import { round } from "lodash";
import { commonFunctions } from "./commonFunctions";

export const getRecommendedHours = (hours: number) => {
  let recommendedHours = 5;
  if (hours > 40) {
    recommendedHours = (hours - 40) * 0.22 + 5;
  }

  return recommendedHours.toFixed(2);
};

export const getCustomerCost = (contractor: IContractor) => {
  const partnerMargin = contractor.margin_percentage / 100;
  const customerCost = contractor.partner_cost / (1 - partnerMargin);

  return customerCost && customerCost > 0
    ? parseFloat(customerCost.toFixed(2))
    : 0;
};

export const getCalculatedHourlyResource = (
  resource: IHourlyResource
): IHourlyResource => {
  const customer_cost = round(resource.hours * resource.hourly_rate, 2);
  const internal_cost = round(resource.hours * resource.hourly_cost, 2);
  const margin = customer_cost - internal_cost;
  const margin_percentage =
    customer_cost !== 0 ? round((margin / customer_cost) * 100, 1) : 0;
  return {
    ...resource,
    margin,
    customer_cost,
    internal_cost,
    margin_percentage,
  };
};

export const getFixedFeeServiceCostSub = (
  jsonConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let serviceCost = 0;
  const engineeringHours = jsonConfig.service_cost.engineering_hours;
  const engineeringHourlyRate = jsonConfig.service_cost.engineering_hourly_rate;
  const PMHours = jsonConfig.service_cost.project_management_hours;
  const PMHourlyRate = jsonConfig.service_cost.project_management_hourly_rate;
  const afterHours = jsonConfig.service_cost.after_hours;
  const afterHourlyRate = jsonConfig.service_cost.after_hours_rate;

  serviceCost =
    (engineeringHours * engineeringHourlyRate +
      PMHours * PMHourlyRate +
      afterHours * afterHourlyRate) *
    (1 + jsonConfig.service_cost.default_ps_risk / 100);

  return serviceCost || 0;
};

export const getFixedFeeInternalCostSub = (
  jsonConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let InternalCost = 0;
  const engineeringHours = jsonConfig.service_cost.engineering_hours;

  const PMHours = jsonConfig.service_cost.project_management_hours;
  const afterHours = jsonConfig.service_cost.after_hours || 0;

  InternalCost =
    engineeringHours * docSetting.engineering_hourly_cost +
      PMHours * docSetting.pm_hourly_cost +
      afterHours * docSetting.after_hours_cost || 0;

  return InternalCost;
};

export const getCustomerCostFixedFee = (
  jsonConfig: IJSONConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let customerCost = 0;
  const travelCost = Boolean(
    jsonConfig.service_cost.travels && jsonConfig.service_cost.travels.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.travels.map((el) => el.cost || 0)
        )
      )
    : 0;
  const contractorCost = Boolean(
    jsonConfig.service_cost.contractors &&
      jsonConfig.service_cost.contractors.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.contractors.map((el) => el.customer_cost || 0)
        )
      )
    : 0;
  customerCost =
    travelCost +
    contractorCost +
    getFixedFeeServiceCostSub(jsonConfig, docSetting);

  return roudingOffNext(customerCost);
};

export const getFixedFeeInternalCost = (
  jsonConfig: IJSONConfig,
  docSetting: IDOCSetting
) => {
  if (!docSetting) {
    return 0;
  }
  let customerCost = 0;
  const travelCost = Boolean(
    jsonConfig.service_cost.travels && jsonConfig.service_cost.travels.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.travels.map((el) => el.cost || 0)
        )
      )
    : 0;
  const contractorCost = Boolean(
    jsonConfig.service_cost.contractors &&
      jsonConfig.service_cost.contractors.length
  )
    ? Math.round(
        commonFunctions.arrSum(
          jsonConfig.service_cost.contractors.map((el) => el.partner_cost || 0)
        )
      )
    : 0;
  customerCost =
    travelCost +
    contractorCost +
    getFixedFeeInternalCostSub(jsonConfig, docSetting);
  return roudingOffNext(customerCost);
};

export const getCustomerCostTM = (jsonConfig) => {
  let InternalCost = 0;
  const engineeringHours = jsonConfig.service_cost.engineering_hours;
  const engineeringHourlyRate = jsonConfig.service_cost.engineering_hourly_rate;
  const PMHourlyRate = jsonConfig.service_cost.project_management_hourly_rate;

  const PMHours = jsonConfig.service_cost.project_management_hours;
  const afterHours = jsonConfig.service_cost.after_hours;
  const afterHourlyRate = jsonConfig.service_cost.after_hours_rate;
  InternalCost =
    engineeringHours * engineeringHourlyRate +
    PMHours * PMHourlyRate +
    afterHours * afterHourlyRate;

  return roudingOffNext(InternalCost);
};

export const getInternalCostTM = (jsonConfig, docSetting: IDOCSetting) => {
  let InternalCost = 0;
  if (!docSetting) {
    return 0;
  }
  const engineeringHours = jsonConfig.service_cost.engineering_hours;
  const PMHours = jsonConfig.service_cost.project_management_hours;
  const afterHours = jsonConfig.service_cost.after_hours;
  InternalCost =
    engineeringHours * docSetting.engineering_hourly_cost +
    PMHours * docSetting.pm_hourly_cost +
    afterHours * docSetting.after_hours_cost;

  return roudingOffNext(InternalCost);
};

export const getSowCalculationFields = (
  serviceCost: IServiceCost,
  docSetting: IDOCSetting
): ISoWCalculationFields => {
  const calculateRiskHours = (
    resourceHours: number,
    riskPercent: number
  ): number => Math.ceil((resourceHours * riskPercent) / 100);

  let totalCustomerCost: number = 0,
    totalInternalCost: number = 0,
    totalMargin: number = 0,
    totalMarginPercent: number = 0,
    riskBudgetCustomerCost: number = 0,
    riskBudgetInternalCost: number = 0,
    riskBudgetMargin: number = 0,
    riskBudgetMarginPercent: number = 0,
    contractorCustomerCost: number = 0,
    contractorInternalCost: number = 0,
    contractorMargin: number = 0,
    contractorMarginPercent: number = 0,
    proSerInternalCost: number = 0,
    proSerCustomerCost: number = 0,
    proSerMargin: number = 0,
    proSerMarginPercent: number = 0,
    engRiskHours: number = 0,
    ahRiskHours: number = 0,
    pmRiskHours: number = 0,
    itRiskHours: number = 0,
    hourlyLaborRiskHours: number = 0,
    totalHours: number = 0,
    totalRiskHours: number = 0,
    travelCustomerCost: number = 0,
    travelInternalCost: number = 0,
    travelMargin: number = 0,
    travelMarginPercent: number = 0,
    hourlyLaborCustomerCost: number = 0,
    hourlyLaborInternalCost: number = 0,
    hourlyLaborMargin: number = 0,
    hourlyLaborMarginPercent: number = 0,
    hourlyLaborTotalHours: number = 0,
    hourlyLaborTotalRate: number = 0,
    hourlyLaborRiskCC: number = 0,
    hourlyLaborRiskIC: number = 0;

  // Engineering hours calculation
  let engineeringHoursInternalCost = round(
    serviceCost.engineering_hours * docSetting.engineering_hourly_cost
  );
  let engineeringHoursCustomerCost = round(
    serviceCost.engineering_hours * serviceCost.engineering_hourly_rate
  );
  let engineeringHoursMargin =
    engineeringHoursCustomerCost - engineeringHoursInternalCost;
  let engineeringHoursMarginPercent =
    engineeringHoursCustomerCost !== 0
      ? round((engineeringHoursMargin / engineeringHoursCustomerCost) * 100, 1)
      : 0;

  // Engineering after hours calculation
  let afterHoursInternalCost = round(
    serviceCost.after_hours * docSetting.after_hours_cost
  );
  let afterHoursCustomerCost = round(
    serviceCost.after_hours * serviceCost.after_hours_rate
  );
  let afterHoursMargin = afterHoursCustomerCost - afterHoursInternalCost;
  let afterHoursMarginPercent =
    afterHoursCustomerCost !== 0
      ? round((afterHoursMargin / afterHoursCustomerCost) * 100, 1)
      : 0;

  // Integration Technician calculation
  let integrationTechnicianInternalCost = round(
    serviceCost.integration_technician_hours *
      docSetting.integration_technician_hourly_cost
  );
  let integrationTechnicianCustomerCost = round(
    serviceCost.integration_technician_hours *
      serviceCost.integration_technician_hourly_rate
  );
  let integrationTechnicianMargin =
    integrationTechnicianCustomerCost - integrationTechnicianInternalCost;
  let integrationTechnicianMarginPercent =
    integrationTechnicianCustomerCost !== 0
      ? round(
          (integrationTechnicianMargin / integrationTechnicianCustomerCost) *
            100,
          1
        )
      : 0;

  // Project management hours calculation
  let pmHoursInternalCost = round(
    serviceCost.project_management_hours * docSetting.pm_hourly_cost
  );
  let pmHoursCustomerCost = round(
    serviceCost.project_management_hours *
      serviceCost.project_management_hourly_rate
  );
  let pmHoursMargin = pmHoursCustomerCost - pmHoursInternalCost;
  let pmHoursMarginPercent =
    pmHoursCustomerCost !== 0
      ? round((pmHoursMargin / pmHoursCustomerCost) * 100, 1)
      : 0;

  // Hourly Contract Labor Calculations
  if (serviceCost.hourly_resources) {
    hourlyLaborInternalCost = round(
      commonFunctions.arrSum(
        serviceCost.hourly_resources.map((el) => el.internal_cost)
      )
    );
    hourlyLaborCustomerCost = round(
      commonFunctions.arrSum(
        serviceCost.hourly_resources.map((el) => el.customer_cost)
      )
    );
    hourlyLaborTotalHours = round(
      commonFunctions.arrSum(serviceCost.hourly_resources.map((el) => el.hours))
    );
    hourlyLaborTotalRate = round(
      commonFunctions.arrSum(
        serviceCost.hourly_resources.map((el) => el.hourly_rate)
      )
    );

    hourlyLaborMargin = hourlyLaborCustomerCost - hourlyLaborInternalCost;
    hourlyLaborMarginPercent =
      hourlyLaborCustomerCost !== 0
        ? round((hourlyLaborMargin / hourlyLaborCustomerCost) * 100, 1)
        : 0;
  }

  // Professional Services (All Cost combined)
  proSerCustomerCost =
    engineeringHoursCustomerCost +
    afterHoursCustomerCost +
    pmHoursCustomerCost +
    integrationTechnicianCustomerCost +
    hourlyLaborCustomerCost;
  proSerInternalCost =
    engineeringHoursInternalCost +
    afterHoursInternalCost +
    pmHoursInternalCost +
    integrationTechnicianInternalCost +
    hourlyLaborInternalCost;
  proSerMargin = proSerCustomerCost - proSerInternalCost;
  proSerMarginPercent =
    proSerCustomerCost !== 0
      ? round((proSerMargin / proSerCustomerCost) * 100, 1)
      : 0;

  // Contractors calculations
  contractorInternalCost = Boolean(
    serviceCost.contractors && serviceCost.contractors.length
  )
    ? round(
        commonFunctions.arrSum(
          serviceCost.contractors.map((el) => el.partner_cost || 0)
        )
      )
    : 0;
  contractorCustomerCost = Boolean(
    serviceCost.contractors && serviceCost.contractors.length
  )
    ? round(
        commonFunctions.arrSum(
          serviceCost.contractors.map((el) => el.customer_cost || 0)
        )
      )
    : 0;
  contractorMargin = contractorCustomerCost - contractorInternalCost;
  contractorMarginPercent =
    contractorCustomerCost !== 0
      ? round((contractorMargin / contractorCustomerCost) * 100, 1)
      : 0;

  // Travel Calculations
  travelCustomerCost = Boolean(
    serviceCost.travels && serviceCost.travels.length
  )
    ? round(
        commonFunctions.arrSum(serviceCost.travels.map((el) => el.cost || 0))
      )
    : 0;
  travelInternalCost = travelCustomerCost;

  // Risk Hours Calculations
  engRiskHours = calculateRiskHours(
    serviceCost.engineering_hours,
    serviceCost.default_ps_risk
  );
  ahRiskHours = calculateRiskHours(
    serviceCost.after_hours,
    serviceCost.default_ps_risk
  );
  pmRiskHours = calculateRiskHours(
    serviceCost.project_management_hours,
    serviceCost.default_ps_risk
  );
  itRiskHours = calculateRiskHours(
    serviceCost.integration_technician_hours,
    serviceCost.default_ps_risk
  );
  // Calculate Risk Hours for each Hourly Labor and add them up
  if (serviceCost.hourly_resources) {
    serviceCost.hourly_resources.forEach((hr) => {
      const resourceRiskHours = calculateRiskHours(
        hr.hours,
        serviceCost.default_ps_risk
      );
      hourlyLaborRiskCC += round(hr.hourly_rate * resourceRiskHours, 2);
      hourlyLaborRiskIC += round(hr.hourly_cost * resourceRiskHours, 2);
      hourlyLaborRiskHours += resourceRiskHours;
    });
  }

  // Risk Budget Calculations
  riskBudgetCustomerCost = round(
    serviceCost.engineering_hourly_rate * engRiskHours +
      serviceCost.after_hours_rate * ahRiskHours +
      serviceCost.project_management_hourly_rate * pmRiskHours +
      serviceCost.integration_technician_hourly_rate * itRiskHours +
      hourlyLaborRiskCC
  );

  riskBudgetInternalCost = round(
    docSetting.engineering_hourly_cost * engRiskHours +
      docSetting.after_hours_cost * ahRiskHours +
      docSetting.pm_hourly_cost * pmRiskHours +
      docSetting.integration_technician_hourly_cost * itRiskHours +
      hourlyLaborRiskIC
  );

  riskBudgetMargin = round(riskBudgetCustomerCost - riskBudgetInternalCost);

  riskBudgetMarginPercent =
    riskBudgetCustomerCost !== 0
      ? round((riskBudgetMargin / riskBudgetCustomerCost) * 100, 1)
      : 0;

  // Total calculations
  totalCustomerCost = Number(
    proSerCustomerCost +
      riskBudgetCustomerCost +
      contractorCustomerCost +
      travelCustomerCost
  );
  totalInternalCost =
    proSerInternalCost +
    riskBudgetInternalCost +
    contractorInternalCost +
    travelInternalCost;
  totalMargin = totalCustomerCost - totalInternalCost;
  totalMarginPercent =
    totalCustomerCost !== 0
      ? round((totalMargin / totalCustomerCost) * 100, 1)
      : 0;
  totalRiskHours =
    engRiskHours +
    ahRiskHours +
    pmRiskHours +
    itRiskHours +
    hourlyLaborRiskHours;
  totalHours = Math.round(
    hourlyLaborTotalHours +
      serviceCost.engineering_hours +
      serviceCost.after_hours +
      serviceCost.project_management_hours +
      serviceCost.integration_technician_hours
  );

  return {
    engRiskHours,
    engineeringHoursInternalCost,
    engineeringHoursCustomerCost,
    engineeringHoursMargin,
    engineeringHoursMarginPercent,
    ahRiskHours,
    afterHoursInternalCost,
    afterHoursCustomerCost,
    afterHoursMargin,
    afterHoursMarginPercent,
    itRiskHours,
    integrationTechnicianInternalCost,
    integrationTechnicianCustomerCost,
    integrationTechnicianMargin,
    integrationTechnicianMarginPercent,
    pmRiskHours,
    pmHoursInternalCost,
    pmHoursCustomerCost,
    pmHoursMargin,
    pmHoursMarginPercent,
    riskBudgetInternalCost,
    riskBudgetCustomerCost,
    riskBudgetMargin,
    riskBudgetMarginPercent,
    contractorInternalCost,
    contractorCustomerCost,
    contractorMargin,
    contractorMarginPercent,
    proSerCustomerCost,
    proSerInternalCost,
    proSerMargin,
    proSerMarginPercent,
    hourlyLaborTotalRate,
    hourlyLaborRiskHours,
    hourlyLaborTotalHours,
    hourlyLaborCustomerCost,
    hourlyLaborInternalCost,
    hourlyLaborMargin,
    hourlyLaborMarginPercent,
    totalHours,
    totalRiskHours,
    totalCustomerCost,
    totalInternalCost,
    totalMargin,
    totalMarginPercent,
    travelCustomerCost,
    travelInternalCost,
    travelMargin,
    travelMarginPercent,
  };
};

export const roudingOffNext = (value: number, placeValue: number = 10) => {
  let roundingValue = 0;
  roundingValue =
    value && value > 0 ? Math.ceil(value / placeValue) * placeValue : 0;
  return roundingValue.toFixed(2);
};
