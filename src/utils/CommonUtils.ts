import { isEmpty } from "lodash";
import forge from "node-forge";

export const isValidVal = (value: string) => {
  return value &&
    value !== "null" &&
    value !== null &&
    value !== undefined &&
    value !== "undefined" &&
    value !== "" &&
    value.trim() !== ""
    ? true
    : false;
};

export const isValue = (value: any) => {
  return value &&
    value !== "null" &&
    value !== undefined &&
    value !== "undefined" &&
    value !== ""
    ? value
    : "";
};

export const isEmptyObj = (value: any) => {
  return value === "null" ||
    value === null ||
    value === undefined ||
    value === "undefined" ||
    isEmpty(value) ||
    (value && Object.keys(value).length === 0)
    ? true
    : false;
};

export const hexToRgb = (hex: string) =>
  hex &&
  hex
    .replace(
      /^#?([a-f\d])([a-f\d])([a-f\d])$/i,
      (m, r, g, b) => "#" + r + r + g + g + b + b
    )
    .substring(1)
    .match(/.{2}/g)
    .map((x) => parseInt(x, 16));

export const getConvertedColorWithOpacity = (color: string) => {
  const rgb = hexToRgb(color);
  const colorWithOpacity = rgb && `rgb(${rgb[0]},${rgb[1]},${rgb[2]}, 0.2)`;
  return colorWithOpacity;
};

export const getBackgroundColor = (type) => {
  const colors = ["#164da5", "#550aad", "#c007d0", "#0493ad", "#176f07"];
  return colors[Math.floor(Math.random() * colors.length)];
};

export const getProgressbarColor = (value: number) => {
  let color = "orange";
  if (value > 80) {
    color = "red";
  }
  if (value < 11) {
    color = "green";
  }

  return color;
};

export const encryptTextUsingRSA = (
  text: string,
  publicKeyString: string
): string => {
  const decodedKey: string = forge.util.decode64(publicKeyString);
  const publicKey = forge.pki.publicKeyFromPem(decodedKey);
  // Encrypt the message using RSA-OAEP with SHA-256 hashing
  const encryptedMessage = forge.util.encode64(
    publicKey.encrypt(text, "RSA-OAEP", {
      md: forge.md.sha256.create(),
    })
  );
  return encryptedMessage;
};

export const getDummyEncryptedText = (length: number = 50) =>
  "X".repeat(length);

export const getBackgroundOverdue = (project: any): string => {
  let color = "#176f07";

  if (
    project.expiring_critical_path_items ||
    0 + project.expiring_action_items ||
    0
  ) {
    color = "#dba629";
  }
  if (
    project.overdue_critical_path_items ||
    0 + project.overdue_action_items ||
    0
  ) {
    color = "#db1643";
  }

  return color;
};

export const numberFormatter = (n: number, digits: number) => {
  const num = Math.abs(n);
  const lookup = [
    { value: 1, symbol: "" },
    { value: 1000, symbol: "K" },
    { value: 1000000, symbol: "M" },
    { value: 1000000000, symbol: "B" },
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var item = lookup
    .slice()
    .reverse()
    .find(function(item) {
      return num >= item.value;
    });
  let formated = "0";
  if (item) {
    formated =
      (num / item.value).toFixed(digits).replace(rx, "$1") + item.symbol;
  }
  return n > 0 ? formated : `-${formated}`;
};
