import _, { camelCase } from "lodash";
import showdown from "showdown";

const errorStateHandle = (errorList: object, newState) => {
  Object.keys(errorList).map((key) => {
    if (
      newState.error &&
      (newState.error[key] || newState.error[camelCase(key)])
    ) {
      const stateKey = newState.error[key] ? key : camelCase(key);
      newState.error[stateKey].errorState = "error";
      newState.error[stateKey].errorMessage = errorList[key];
    }
  });
  return newState;
};

const getAddress = (site) => {
  const address = `${site.name} (
    ${_.get(site, "address_line_1", "")}
    ${_.get(site, "address_line_2", "")}
    ${_.get(site, "city", "")}
    ${_.get(site, "state", "")}
    ${_.get(site, "country", "")}
    ${_.get(site, "zip", "")}
  )`;
  return address;
};

const arrSum = (array: number[]): number =>
  array.reduce(
    (sum, num) => sum + (Array.isArray(num) ? arrSum(num) : num * 1),
    0
  );

const flatten = (list) =>
  list.reduce((a, b) => a.concat(Array.isArray(b) ? flatten(b) : b), []);

const isNonEmptyArray = (array) => {
  return array && array.length ? array : [];
};

const getArrFromString = (data: string): string[] => {
  // This function detects the splitter string from the given data string and splits it accordingly
  if (data && data.trim()) {
    let splitValue: string;
    if (data.includes(",")) {
      splitValue = ",";
    } else if (data.includes(" ")) {
      splitValue = " ";
    } else {
      splitValue = "\\s+"; // If no common delimiter is found, split by any whitespace character
    }
    return data.trim().split(new RegExp(splitValue));
  } else return [];
};

const rangeVariables = () => {
  const variablesList = [
    "#{Project Name}",
    "#{Customer Name}",
    "#{Customer Primary Contact First Name}",
    "#{Customer Primary Contact}",
    "#{Project Owner}",
  ];

  const globalVariables = variablesList.map((c) => ({
    id: c,
    display: c,
  }));

  return globalVariables;
};

const emailTemplateOptions = () => {
  const variablesList = [
    "#{Project Name}",
    "#{Customer Name}",
    "#{Customer Primary Contact First Name}",
    "#{Customer Primary Contact}",
    "#{Project Owner}",
    "#{Requestor Name}",
  ];

  const globalVariables = variablesList.map((c) => ({
    id: c,
    display: c,
  }));

  return globalVariables;
};
const emailTemplateVariables = [
  { id: "projectName", value: "{Project Name}" },
  { id: "customerName", value: "{Customer Name}" },
  {
    id: "customerPrimaryContactFirstName",
    value: "{Customer Primary Contact First Name}",
  },
  { id: "customerPrimaryContact", value: "{Customer Primary Contact}" },
  { id: "projectOwner", value: "{Project Owner}" },
];
const emailTemplateVariablesCR = [
  { id: "projectName", value: "{Project Name}" },
  { id: "customerName", value: "{Customer Name}" },
  {
    id: "customerPrimaryContactFirstName",
    value: "{Customer Primary Contact First Name}",
  },
  { id: "customerPrimaryContact", value: "{Customer Primary Contact}" },
  { id: "projectOwner", value: "{Project Owner}" },
  { id: "requestorName", value: "{Requestor Name}" },
];
const markdownVariables: IMarkDownVariables[] = [
  { id: "sowName", value: "{SOW Name}" },
  { id: "customerName", value: "{Customer Name}" },
  { id: "pageBreak", value: "{Page Break}" },
  { id: "requestorName", value: "{Requestor Name}" },
];
const getKeyByValue = (object, value) => {
  return Object.keys(object).find((key) => object[key] === value);
};

const getErrorState = (msg?: string): IFieldValidation => {
  return {
    errorState: msg ? "error" : "success",
    errorMessage: msg ? msg : "",
  };
};

const getErrorMessage = (error) => {
  try {
    return Object.values(error)
      .map((x, ind) =>
        typeof x === "string"
          ? x
          : `${ind + 1}) ${Object.keys(x)}: ${Object.values(x)} `
      )
      .join("\r\n");
  } catch (e) {
    return JSON.stringify(error);
  }
};

const getSiteById = (sites, id, type) => {
  const site = sites && sites.find((x) => x.site_id === id);
  if (type === "text") {
    return site
      ? `
    ${site.name || ""}
    ${site.address_line_1 || ""} 
    ${site.address_line_2 || ""} 
    ${site.city || ""}
    ${site.state || ""} 
    ${site.country || ""} 
    ${site.zip || ""}`
      : "Site Not Found";
  }
  return site;
};

const getCustomerCRMID = (id: number | string, customers: ICustomerShort[]) => {
  if (id && customers) {
    const result = customers.find((el) => el.id == id);
    return result ? result.crm_id : null;
  }
  return null;
};

const sortList = (itemA, itemB, uniqueKey, sorting) => {
  if (!sorting) {
    return (
      (itemA[uniqueKey] !== undefined && itemB[uniqueKey] !== undefined)
        ? itemA[uniqueKey] - itemB[uniqueKey]
        : 0
    );
  }

  if (itemA.edited || itemB.edited) {
    return true;
  }

  let sortValue = sorting.replace("-", "");
  let valueA = itemA[sortValue];
  let valueB = itemB[sortValue];

  let a = valueA;
  let b = valueB;

  if (sorting.includes("-")) {
    a = valueB;
    b = valueA;
  }

  let r = null;

  switch (sortValue) {
    case "due_date":
    case "created_on":
      const c = new Date(b).getTime();
      const d = new Date(a).getTime();
      r = d - c;
      break;

    case "impact":
      r = a - b;
      break;

    default:
      if (typeof a === "string") {
        r = a && a.localeCompare(b);
      } else {
        r = a - b;
      }
      break;
  }

  if (r === 0) {
    r =
      (itemA[uniqueKey] !== undefined && itemB[uniqueKey] !== undefined)
        ? itemA[uniqueKey] - itemB[uniqueKey]
        : 0;
  }

  return r;
};


const isEditorEmpty = (htmlString?: string): boolean => {
  if (typeof htmlString === "string") {
    let htmlValue = htmlString.trim();
    const emptyHtmlRegex = /^((((<[\w\s="]+>)+[ \n(<br>)]*(<\/\w+>)+)+)|<br>|\s)+$/;
    const match = htmlValue.match(emptyHtmlRegex);
    return !htmlValue || (match && match[0] === htmlValue);
  } else {
    return true;
  }
};

const convertToMarkdown = (htmlString: string): string => {
  if (htmlString) {
    const converter = new showdown.Converter();

    let modifiedString: string;
    // Remove image, underline, span, sub and super script, strikethrough and table tag
    // Table can be integrated if using thead and tbody (if future requirement)
    modifiedString = htmlString.replace(
      /(<img (.|\s)*?>)|(<\/?u>)|(<\/?s>)|(<\/?sub>)|(<\/?sup>)|(<\/?span.*?>)|(<table(.|\s)*?<\/table>)/g,
      ""
    );
    // Replace pre with code tag
    modifiedString = modifiedString.replace(/<pre.*>/g, "<code>");
    modifiedString = modifiedString.replace(/<\/pre>/g, "</code>");

    let finalString = converter.makeMarkdown(modifiedString);
    // Remove the <br> and <!-- --> (list end) tags
    return finalString.replace(/(\s?<br>\s?)|(\s?<!-- -->\s?)/g, "");
  } else return "";
};

export const commonFunctions = {
  errorStateHandle,
  getAddress,
  arrSum,
  isNonEmptyArray,
  flatten,
  rangeVariables,
  getCustomerCRMID,
  emailTemplateVariables,
  emailTemplateVariablesCR,
  markdownVariables,
  getKeyByValue,
  getErrorMessage,
  getErrorState,
  getSiteById,
  sortList,
  isEditorEmpty,
  convertToMarkdown,
  emailTemplateOptions,
  getArrFromString,
};
