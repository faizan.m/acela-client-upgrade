import { isEmail, isURL } from 'validator';

export default class AppValidators {
  static isValidUrl(url: string): boolean {
    return isURL(url);
  }

  static isValidEmail(email: string): boolean {
    return isEmail(email);
  }

  static isAlphaWithSpaces(str: string) {
    return /^[a-zA-Z ]*$/.test(str);
  }
  static isValidName(str: string) {
    return /^(?:[A-Za-z]+)(?:[A-Za-z0-9 _]*)$/.test(str);
  }

  static isDateInMMDDYYYY(str: string) {
    return /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/.test(str);
  }

  static isValidPositiveFloat(str: string) {
    return /^(?:[0-9]\d*|0)?(?:\.\d+)?$/.test(str);
  }

  static integerNumber(str: any) {
    return /^\d+$/.test(str);
  }

  
  static noSpaceAllowed(str: any) {
    return /^\S*$/.test(str);
  }

  static isValidPassword(str: string) {
    //Password should contain one or more lowercase, uppercase,
    // numeric and special character. Length should be minimum 8 character.
    // tslint:disable-next-line:max-line-length
    return /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/.test(
      str
    );
  }

  static isValidRuleVarible(str: string) {
    return /^@{{(.*?)}}$/.test(str);
  }
  static isPhoneNumber(str: string) {
    const phoneRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/; // tslint:disable-line

    return phoneRegex.test(str);
  }
  public isValidUsername(str: string) {
    return /^[a-zA-Z0-9_.@-]{6,40}$/.test(str);
  }

  public isValidPhoneNumber(str: string) {
    const phoneRegex = /^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/; // tslint:disable-line

    return phoneRegex.test(str);
  }

  public isValidLatitude(latitude: number) {
    return latitude >= -90 && latitude <= 90;
  }

  public isValidLongitude(longitude: number) {
    return longitude >= -180 && longitude <= 180;
  }

  public isValidMapZoomLevel(mapZoomLevel: number) {
    return mapZoomLevel >= 0 && mapZoomLevel <= 22;
  }
}
