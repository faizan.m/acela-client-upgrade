import { round } from "lodash";

export const getCustomerCost = (product: IAgreementProduct | IPAX8Product): string => {
  let cust_cost = 0;
  const internal_cost = Number(product.internal_cost);
  const margin = Number(product.margin);
  if (product && internal_cost) {
    let marginPercentage = margin / 100;
    if (marginPercentage === 1) marginPercentage = 0.9999; // For avoiding division by zero
    cust_cost = internal_cost / (1 - marginPercentage);
  }
  return cust_cost !== 0 ? Number(cust_cost).toFixed(2) : "0";
};

export const calculateCosts = (
  productBundle: IProductBundle,
  discount: number = 0
): void => {
  const billingMultiplier = getBillingMultiplier(productBundle);

  productBundle.products.forEach((element: IAgreementProduct) => {
    element.customer_cost = Number(getCustomerCost(element));
  });

  // Ideally, we should only check for is_checked, but for existing templates which
  // did not have selected_product attribute, it was set to null on backend
  if (productBundle.is_checked && productBundle.selected_product !== null) {
    const selected_product: IAgreementProduct =
      productBundle.products[productBundle.selected_product];

    productBundle.customer_cost_total = round(
      selected_product.customer_cost *
        selected_product.quantity *
        billingMultiplier,
      2
    );

    productBundle.internal_cost_total = round(
      selected_product.internal_cost *
        selected_product.quantity *
        billingMultiplier,
      2
    );

    productBundle.margin_total = round(
      productBundle.customer_cost_total - productBundle.internal_cost_total,
      2
    );

    productBundle.discount = discount
      ? round((productBundle.margin_total * discount) / 100, 2)
      : 0;
  } else {
    productBundle.customer_cost_total = 0;
    productBundle.internal_cost_total = 0;
    productBundle.margin_total = 0;
    productBundle.discount = 0;
  }
};

export const getBillingMultiplier = (product: IProductBundle): number => {
  let billingMultiplier = product.billing_option === "Annually" ? 12 : 1;
  return billingMultiplier;
};
