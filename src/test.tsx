import React, { useState } from "react";

function Test() {
  const [count, setCount] = useState(0);

  return (
    <div>
      <h1>Vite + React</h1>
      <div className="card">
        <button onClick={() => setCount((count) => count + 1)}>
          count is {count}
        </button>
        <p>
          Edit <code>src/Test.tsx</code> and save to test HMR
        </p>
      </div>
    </div>
  );
}

export default Test;
