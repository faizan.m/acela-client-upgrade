import React from "react";
import { Link } from "react-router-dom";
import { iconsPath } from "../../utils/icons";
import Input from "../Input/input";
import "./style.scss";

interface ISideMenuProps {
  isCollapsed: boolean;
  toggleCollapseState: () => void;
  logout: TLogout;
  sideMenuOptions: ISideMenuOption[];
  location: any;
  logoUrl: string;
  company_url: string;
  enable_url: boolean;
}
interface ISideMenuState {
  searchString: string;
}

export default class SideMenu extends React.Component<
  ISideMenuProps,
  ISideMenuState
> {
  constructor(props: ISideMenuProps) {
    super(props);
    this.state = {
      searchString: "",
    };
  }

  logout = () => {
    this.props.logout();
  };

  componentDidMount() {
    this.props.sideMenuOptions.map((x, index) => {
      this.props.sideMenuOptions[
        index
      ].showChild = this.props.location.pathname.includes(
        x.campare.toLocaleLowerCase()
      )
        ? true
        : false;
    });
  }

  componentDidUpdate(prevProps: ISideMenuProps) {
    if (this.props.sideMenuOptions !== prevProps.sideMenuOptions) {
      this.props.sideMenuOptions.map((x, index) => {
        this.props.sideMenuOptions[
          index
        ].showChild = prevProps.location.pathname.includes(
          x.campare.toLocaleLowerCase()
        )
          ? true
          : false;
      });
    }
  }

  onParentClick = (option: ISideMenuOption) => {
    if (!option.showChild && option.childs && option.childs.length > 0) {
      const index = this.props.sideMenuOptions.findIndex(
        (o) => o.name === option.name
      );
      this.props.sideMenuOptions[index].showChild = true;
    } else {
      if (option.name === "Client 360" && !this.props.isCollapsed) {
        this.props.toggleCollapseState();
      }
      this.props.sideMenuOptions.map((x) => (x.showChild = false));
    }
  };

  openNewTab = () => {
    if (this.props.company_url && this.props.enable_url) {
      window.open(this.props.company_url);
    }
  };
  render() {
    return (
      <div className="side-menu">
        <div
          className={`side-menu__collapse-toggle ${
            this.props.isCollapsed ? "collapsed" : "open"
          }`}
          onClick={this.props.toggleCollapseState}
        >
          <button />
        </div>
        {!this.props.isCollapsed && (
          <Input
            field={{
              value: this.state.searchString,
              label: "",
              type: "SEARCH",
            }}
            width={12}
            name="searchString"
            onChange={(e) => {
              this.setState({ searchString: e.target.value });
            }}
            // onBlur={() => {
            //   if (this.state.searchString.trim() === '') {
            //     this.setState({ showSearch: false })
            //   }
            // }}
            placeholder="Search"
            className="search  search-sidemenu"
            autoFocus={true}
          />
        )}
        <div className="side-menu__options">
          {this.props.sideMenuOptions
            //search on parent
            .filter(
              (option) =>
                [
                  option.childs &&
                    option.childs
                      .map((c) => c.search && c.search.join())
                      .join(),
                  option.name,
                ]
                  .join()
                  .toLowerCase()
                  .indexOf(this.state.searchString.toLowerCase()) !== -1
            )
            .map((option) => (
              <div key={Math.random()} className="option-parent">
                <Link
                  to={`${option.path}`}
                  key={option.path}
                  className={`side-menu__option ${
                    this.props.location.pathname.includes(option.path)
                      ? "side-menu__option--active"
                      : ""
                  }
                `}
                  onClick={() => this.onParentClick(option)}
                  title={`${option.name}`}
                >
                  <div
                    className="icon-side-menu"
                    style={{
                      WebkitMask: `url(${
                        -this.props.location.pathname.includes(option.path)
                          ? option.icon
                          : option.icon
                      }) no-repeat center`,
                      mask: `url(${
                        this.props.location.pathname.includes(option.path)
                          ? option.icon
                          : option.icon
                      }) no-repeat center`,
                    }}
                  ></div>
                  <span>{`${option.name}`}</span>
                  {option.childs &&
                    option.childs.length > 0 &&
                    option.showChild === true && (
                      <img
                        className="arrow-side-menu"
                        alt=""
                        src={"/assets/icons/down-arrow.png"}
                      />
                    )}
                  {option.childs &&
                    option.childs.length > 0 &&
                    option.showChild === false && (
                      <img
                        className="arrow-side-menu"
                        alt=""
                        src={iconsPath.right_arr}
                      />
                    )}
                </Link>
                {(option.showChild === true ||
                  this.state.searchString.length > 1) &&
                  option.childs &&
                  option.childs.length > 0 && (
                    <div className="child-options">
                      {option.childs
                        .filter((op) =>
                          this.state.searchString.length > 1
                            ? op.search &&
                              op.search
                                .join()
                                .toLowerCase()
                                .indexOf(
                                  this.state.searchString.toLowerCase()
                                ) !== -1
                            : true
                        )
                        .filter((f) => !f.defaultHide)
                        .map((childOption) => (
                          <Link
                            to={`${childOption.path}`}
                            key={childOption.name}
                            className={
                              this.props.location.pathname.includes(
                                childOption.path
                              )
                                ? `child-option child-option--active`
                                : "child-option"
                            }
                            title={`${childOption.name}`}
                          >
                            <span className="child-option-text">
                              {`${childOption.name}`}
                            </span>
                          </Link>
                        ))}
                    </div>
                  )}
              </div>
            ))}
        </div>
        <div className="side-menu__fixed-options" onClick={() => this.logout()}>
          <div
            className="icon-side-menu"
            style={{
              WebkitMask: `url(${iconsPath.logout}) no-repeat center`,
              mask: `url(${iconsPath.logout}) no-repeat center`,
            }}
          ></div>
          <span>Logout</span>
        </div>
      </div>
    );
  }
}
