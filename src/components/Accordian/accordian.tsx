import React, { useEffect, useState } from 'react';
import './accordian.scss';
import { Collapse } from 'react-bootstrap';

interface IAccordianProps {
  label: any;
  children: any;
  isOpen?: boolean;
  statusClass?: string;
  parentClass?: string;
  background?: string;
}

const Accordian: React.FC<IAccordianProps> = ({ label, ...props }) => {
  const [isOpen, setIsOpen] = useState(props.isOpen);
  useEffect(() => {
    setIsOpen(props.isOpen);
  }, [props.isOpen]);
  return (
    <div className={`accordian-component ${props.parentClass || ''}`} >
      <div
        className={`header-box
        ${props.statusClass}
        ${isOpen ? "field--not-collapsed" : "field--collapsed"}`}
        aria-controls={`collapsable-item-${label}`}
        aria-expanded={isOpen}
        onClick={() => setIsOpen(!isOpen)}
      >
        <div className="action-collapse right" style={{backgroundColor: props.background}} >
          <img
            className={`arrow-play ${!isOpen? 'point-right' : 'point-bottom'}`}
            alt=""
            src={'/assets/icons/arrow-angle-pointing-to-left.svg'}
          /> 
        </div>
        <div style={{color: props.background}} className={`collapsable-headings`}>{label}</div>

      </div>
      {
        isOpen &&
        <Collapse in={isOpen}>
          <div className="accordian-content" id={`collapsable-item ${label}`}>{props.children}</div>
        </Collapse>
      }

    </div>
  );
};

export default Accordian;
