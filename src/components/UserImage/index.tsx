import React from 'react';
import './style.scss';

const UsershortInfo : React.FC<any> = ( {name, url, className}) => {
    return (
        <div className={`${className} user-image-component`}>
            <div
                style={{
                    backgroundImage: `url(${url
                        ? `${url}`
                        : '/assets/icons/user-filled-shape.svg'
                        })`,
                }}
                className="pm-image-circle"
            />
            <div className="name">{name}
            </div>
        </div>
    )
}
export default UsershortInfo;