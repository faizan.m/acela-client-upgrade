import React, { useRef, useState, useEffect, ReactElement } from "react";
import "./style.scss";

interface IHorizontalTabSliderProps {
  className?: string;
  defaultTab?: string;
  scrollOffset?: number;
  children?: ReactElement<ISingleTabProps>[];
}

interface ISingleTabProps {
  title: string;
  hide?: boolean;
  children: JSX.Element;
}

export const Tab: React.FC<ISingleTabProps> = (props) => {
  return props.children;
};

const HorizontalTabSlider: React.FC<IHorizontalTabSliderProps> = (props) => {
  const ref = useRef<HTMLDivElement>(null);
  const defaultScrollOffset: number = 100;
  const [currentTab, setCurrentTab] = useState<string>(
    props.children[0].props.title
  );

  useEffect(() => {
    if (
      props.defaultTab &&
      props.children.map((el) => el.props.title).includes(props.defaultTab)
    )
      setCurrentTab(props.defaultTab);
  }, [props.defaultTab]);

  const tabsList: string[] = props.children
    .filter((tab) => !tab.props.hide)
    .map(
      (tabComponent: ReactElement<ISingleTabProps>) => tabComponent.props.title
    );
  const [leftMost, setLeftMost] = useState<boolean>(false);
  const [rightMost, setRightMost] = useState<boolean>(false);

  useEffect(() => {
    if (ref.current.offsetWidth === ref.current.scrollWidth) {
      setLeftMost(true);
      setRightMost(true);
    } else {
      setLeftMost(true);
      setRightMost(false);
    }
  }, []);

  function scroll(offset: number) {
    let scrollWidth = ref.current.scrollWidth;
    let offsetWidth = ref.current.offsetWidth;
    if (ref.current.scrollLeft + offset <= 0) {
      setLeftMost(true);
      setRightMost(false);
    } else if (ref.current.scrollLeft + offset >= scrollWidth - offsetWidth) {
      setLeftMost(false);
      setRightMost(true);
    } else {
      setLeftMost(false);
      setRightMost(false);
    }
    ref.current.scrollLeft += offset;
  }

  return (
    <>
      <div
        className={
          "horizontal-tabs-container" +
          (props.className ? " " + props.className : "")
        }
      >
        {!leftMost && (
          <button
            className="tab-arrow hzntl-tab-left-btn"
            onClick={() =>
              scroll(
                props.scrollOffset ? -props.scrollOffset : -defaultScrollOffset
              )
            }
          >
            <img src={"/assets/new-icons/left-arrow.svg"} alt="left arrow" />
          </button>
        )}
        <div className="horizontal-tabs" ref={ref}>
          {tabsList.map((tabName: string, idx: number) => {
            return (
              <div
                key={idx}
                className={
                  "tab-link" +
                  (currentTab === tabName ? " tab-link__active" : "")
                }
                onClick={() => setCurrentTab(tabName)}
              >
                {tabName}
              </div>
            );
          })}
        </div>
        {!rightMost && (
          <button
            className="tab-arrow hzntl-tab-right-btn"
            onClick={() =>
              scroll(
                props.scrollOffset ? props.scrollOffset : defaultScrollOffset
              )
            }
          >
            <img src={"/assets/new-icons/right-arrow.svg"} alt="right arrow" />
          </button>
        )}
      </div>
      {props.children.find(
        (tabComponent: ReactElement<ISingleTabProps>) =>
          tabComponent.props.title === currentTab
      )}
    </>
  );
};

export default HorizontalTabSlider;
