import * as React from "react";
import { Modal } from "react-bootstrap";

import "./style.scss";

interface IModalBaseProps {
  show: boolean;
  onClose: (e: any) => void;
  closeOnEscape?: boolean;
  closeOnOverlayClick?: boolean;
  titleElement?: string | JSX.Element;
  previewHTML?: string;
  className?: string;
}

export default class HTMLViewer extends React.Component<IModalBaseProps, {}> {
  public static defaultProps: IModalBaseProps = {
    show: false,
    onClose: null,
    closeOnEscape: true,
    closeOnOverlayClick: false,
    titleElement: "REPLACE TITLE",
    previewHTML: "REPLACE BODY",
    className: ""
  };
  renderPreview = () => {
    if (!this.props.previewHTML) {
      return <div />;
    }

    return (
      <div
        className="preview-html-body"
        dangerouslySetInnerHTML={{ __html: this.props.previewHTML }}
      />
    );
  };
  
  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.onClose}
        backdrop={this.props.closeOnOverlayClick ? true : "static"}
        keyboard={this.props.closeOnEscape}
        className={`${this.props.className} view-html-preview`}
      >
        <Modal.Header className="view-html-header" closeButton>
          {this.props.titleElement}
        </Modal.Header>
        <Modal.Body>{this.renderPreview()}</Modal.Body>
      </Modal>
    );
  }
}
