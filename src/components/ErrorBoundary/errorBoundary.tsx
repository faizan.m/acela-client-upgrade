import * as React from "react";
import SquareButton from "../Button/button";
import "./errorBoundary.scss";

interface ErrorBoundaryProps {
  children: React.ReactNode;
  customClass?: string;
  text?: string;
}

interface ErrorBoundaryState {
  hasError: boolean;
}

export default class ErrorBoundary extends React.Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  render() {
    if (this.state.hasError) {
      return (
        <div
          className={
            "error-boundary-container" +
            `${this.props.customClass ? " " + this.props.customClass : ""}`
          }
        >
          <h2>
            {this.props.text ? this.props.text : "Oops, something went wrong!"}
          </h2>
          <div className="eb-button-container">
            <SquareButton
              onClick={() => location.reload()}
              content={
                <span>
                  <img
                    alt="reload"
                    src="/assets/icons/reload.png"
                    className="eb-reload-img"
                  />
                  Reload
                </span>
              }
              bsStyle={"primary"}
              className="reload-btn"
            />
          </div>
        </div>
      );
    }

    return this.props.children;
  }
}
