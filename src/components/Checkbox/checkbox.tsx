import * as React from "react";
import "./style.scss";

interface ICheckboxProps {
  isChecked: boolean;
  name: string;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  className?: string;
  disabled?: boolean;
  title?: string;
  children?: React.ReactNode;
}

export default class Checkbox extends React.Component<ICheckboxProps, {}> {
  static defaultProps: ICheckboxProps = {
    name: "checkbox",
    isChecked: false,
    onChange: (e) => null,
    disabled: false,
  };

  render() {
    return (
      <span
        title={this.props.title ? this.props.title : undefined}
        className={`checkbox-holder ${this.props.className}`}
      >
        <input
          name={this.props.name}
          className="checkbox__input"
          type="checkbox"
          checked={this.props.isChecked}
          onChange={this.props.onChange}
          disabled={this.props.disabled}
          onClick={(evt) => evt.stopPropagation()}
        />
        <label className="checkbox__content">{this.props.children}</label>
      </span>
    );
  }
}
