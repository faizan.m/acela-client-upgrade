import React from 'react';

import './style.scss';

// import SquareButton from '../Button/button';
//TODO : Define state type

interface ImageUploadProps {
  onUpdate?: () => void;
}

class ImageUpload extends React.Component<ImageUploadProps, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      files: [],
      imagePreviewUrls: [],
    };
  }

  handleImageChange = (e: any) => {
    const images = Array.from(e.target.files);
    images.map((file: any, i) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        this.setState({
          files: [...this.state.files, file],
          imagePreviewUrls: [...this.state.imagePreviewUrls, reader.result],
        });
      };
      reader.readAsDataURL(file);
    });
  };

  removeImage = index => {
    this.setState({
      files: [
        ...this.state.files.slice(0, index),
        ...this.state.files.slice(index + 1),
      ],
      imagePreviewUrls: [
        ...this.state.imagePreviewUrls.slice(0, index),
        ...this.state.imagePreviewUrls.slice(index + 1),
      ],
    });
  };

  render() {
    const { imagePreviewUrls } = this.state;

    return (
      <div className="attachments">
        <div className="attachments__previews">
          {imagePreviewUrls &&
            imagePreviewUrls.map((preview, i) => {
              return (
                <div key={i}>
                  <img
                    src="/assets/icons/delete.png"
                    className="attachments__icon__remove"
                    onClick={() => this.removeImage(i)}
                  />
                  <div
                    className="attachments__preview"
                    style={{ backgroundImage: `url(${preview})` }}
                  />
                </div>
              );
            })}
        </div>
        <label className="square-btn  btn btn-primary">
          <img
            className="attachments__icon__upload"
            src="/assets/icons/cloud.png"
          />
          Upload New
          <input type="file" multiple onChange={this.handleImageChange} />
        </label>
      </div>
    );
  }
}

export default ImageUpload;
