// import React, { useState } from "react";
// import { Button } from "react-bootstrap";
// import { OverlayTrigger, Tooltip } from "react-bootstrap/lib"; (New Component)
// import SquareButton from "../Button/button";
import "./style.scss";

interface SmallConfirmationBoxProps {
  setRef?: any;
  children?: JSX.Element;
  elementToCLick?: JSX.Element;
  showButton?: boolean;
  title?: string;
  text?: string;
  fullText?: string;
  confirmText?: string;
  onClickOk: () => void;
  className?: string;
  wrapperClass?: string;
}

const SmallConfirmationBox: React.FC<SmallConfirmationBoxProps> = (props) => {
  // const [show, setShow] = useState<boolean>(false);

  // const confirm = (
  //   <Tooltip
  //     ref={props.setRef}
  //     placement="right"
  //     className={`show in`}
  //     id="confirmation-box"
  //   >
  //     {props.children || (
  //       <div
  //         className={
  //           "confirm-data-rules" +
  //           (props.wrapperClass ? " " + props.wrapperClass : "")
  //         }
  //       >
  //         <div className="text">
  //           {props.text && `Are you sure, want to delete this ${props.text}? `}
  //           {props.fullText ? props.fullText : ""}
  //         </div>
  //         <div className="actions">
  //           <SquareButton
  //             onClick={(e) => setShow(true)}
  //             content="Cancel"
  //             bsStyle={"default"}
  //             className="confirmation-btn"
  //           />
  //           <SquareButton
  //             onClick={(e) => {
  //               props.onClickOk();
  //               setShow(true);
  //             }}
  //             content={props.confirmText ? props.confirmText : "Delete"}
  //             bsStyle={"danger"}
  //             className="confirmation-btn"
  //           />
  //         </div>
  //       </div>
  //     )}
  //   </Tooltip>
  // );

  return (
    // <OverlayTrigger
    //   rootClose={show}
    //   trigger={["click"]}
    //   overlay={confirm}
    //   placement="top"
    //   onExited={(e) => {
    //     setShow(false);
    //   }}
    // >
    //   <div className={`confirm-help-icon ${props.className}`}>
    //     {props.elementToCLick ? (
    //       props.elementToCLick
    //     ) : props.showButton ? (
    //       <Button
    //         title={props.title ? props.title : ""}
    //         className="delete-confirm-btn"
    //       />
    //     ) : (
    //       <img
    //         title={props.title ? props.title : ""}
    //         className={"d-pointer icon-remove-c"}
    //         alt=""
    //         src={"/assets/icons/cross-sign.svg"}
    //       />
    //     )}
    //   </div>
    // </OverlayTrigger>
    null
  );
};

export default SmallConfirmationBox;
