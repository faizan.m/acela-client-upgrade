import React from "react";
import "./style.scss";

interface ListProps<T> {
  rows: T[];
  loading?: boolean;
  className?: string;
  uniqueKey?: keyof T;
  noDataString?: string;
  currentOrder?: string; // eg. (if key is test, then it could be test or -test)
  columns: IListColumn<T>[];
  onOrderChange?: (ordering: string) => void;
}

const getOrderClass = (field: string, currentOrder: string) => {
  let currentClassName = "";
  switch (currentOrder) {
    case `-${field}`:
      currentClassName = "asc-order";
      break;
    case field:
      currentClassName = "desc-order";
      break;

    default:
      currentClassName = "";
      break;
  }
  return currentClassName;
};

const SimpleList = <T,>({
  rows,
  columns,
  uniqueKey,
  onOrderChange,
  className = "",
  loading = false,
  currentOrder = "",
  noDataString = "",
}: ListProps<T>) => {
  const ListHeader = () => {
    return (
      <div className="list-header">
        {columns.map((x, index) => {
          return (
            <div
              className={
                "cell" +
                (x.className ? ` ${x.className}` : "") +
                (x.ordering
                  ? ` cursor-pointer ${getOrderClass(x.ordering, currentOrder)}`
                  : "")
              }
              onClick={() => {
                if (x.ordering) onOrderChange(x.ordering);
              }}
              key={index}
            >
              {x.name}
            </div>
          );
        })}
      </div>
    );
  };

  const renderListRow = (row: T, index: number) => {
    return (
      <div
        className={`row-panel ${index % 2 !== 0 ? "odd" : "even"}`}
        key={uniqueKey ? String(row[uniqueKey]) : `row-${index}`}
      >
        {columns.map((x, i) => {
          return (
            <div
              className={"cell" + `${x.className ? " " + x.className : ""}`}
              title={x.id && row[x.id] ? String(row[x.id]) : ""}
              key={i}
            >
              {x.Cell ? x.Cell(row, index) : row[x.id]}
            </div>
          );
        })}
      </div>
    );
  };

  return (
    <div
      className={"list-component-wrapper" + (className ? ` ${className}` : "")}
    >
      <ListHeader />
      {loading && <div className="list-loader">Loading...</div>}
      {Boolean(!loading && (!rows || rows.length === 0)) && (
        <div className="list-loader">
          {noDataString ? noDataString : "No data"}
        </div>
      )}
      {Boolean(!loading && rows && rows.length) &&
        rows.map((row, idx) => renderListRow(row, idx))}
    </div>
  );
};

export default SimpleList;
