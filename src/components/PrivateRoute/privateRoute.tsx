import React from 'react';
import { Redirect, Route } from 'react-router-dom';

import { isLoggedIn } from '../../utils/AuthUtil';

const redirectLoginPage = (rest: any) => {
  const redirectUrl = '/login';

  return (
    <Route
      {...rest}
      render={props => {
        return (
          <Redirect
            to={{
              pathname: redirectUrl,
              state: { from: rest.location },
            }}
          />
        );
      }}
    />
  );
};

const PrivateRoute = ({ component: Component, ...rest }) => {
  if (!isLoggedIn()) {
    return redirectLoginPage(rest);
  }

  return (
    <Route
      {...rest}
      render={props => {
        return <Component {...props} />;
      }}
    />
  );
};

export default PrivateRoute;
