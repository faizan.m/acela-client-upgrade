import React from 'react';

import SquareButton from '../Button/button';
import ModalBase from '../ModalBase/modalBase';

import Spinner from '../Spinner';
import './style.scss';

interface IConfirmProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: any;
  isLoading: boolean;
  message?: any;
  title?: string;
  okText?: string;
  cancelText?: string;
  className?: string;
}

export default class ConfirmBox extends React.Component<
  IConfirmProps,
  { count: any }
> {
  constructor(props: IConfirmProps) {
    super(props);
    this.state = {
      count: [],
    };
  }
  onSubmit = () => {
    this.props.onSubmit();
  };

  getFooter = () => {
    return (
      <div
        className={`confirm__actions
        ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content={this.props.cancelText ? this.props.cancelText : 'Cancel'}
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content={this.props.okText ? this.props.okText : 'Confirm'}
          bsStyle={"primary"}
          disabled={this.props.isLoading}
        />
      </div>
    );
  };

  render() {
    const title = this.props.title
      ? this.props.title
      : ' Are you sure, want to delete ?';

    return (
      <ModalBase
        show={this.props.show}
        closeOnEscape={false}
        onClose={this.props.onClose}
        hideCloseButton={this.props.isLoading}
        titleElement={title}
        bodyElement={
          <>
            <div className="loader">
              <Spinner show={this.props.isLoading} />
            </div>
            {this.props.message ? this.props.message : null}
          </>
        }
        footerElement={this.getFooter()}
        className={`confirm ${this.props.className}`}
      />
    );
  }
}
