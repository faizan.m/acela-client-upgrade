import React from "react";
import SquareButton from "../Button/button";
import ModalBase from "../ModalBase/modalBase";

type WarningDialogProps = {
  open: boolean;
  titleText: string;
  contentText: string;
  cancelButtonText: string;
  onClose: () => void;
  onSave: () => void;
  onConfirm: any;
};

function WarningDialog({
  open,
  titleText,
  contentText,
  cancelButtonText,
  onClose,
  onSave,
  onConfirm
}: WarningDialogProps) {
  return (
    <ModalBase
      show={open}
      onClose={onClose}
      titleElement={titleText}
      bodyElement={<>{contentText ? contentText : null}</>}
      footerElement={
        <>
          <SquareButton
            onClick={onConfirm}
            content={cancelButtonText}
            bsStyle={"danger"}
          />
          <SquareButton
            onClick={onSave}
            content={'Save'}
            bsStyle={"primary"}
          />
        </>
      }
      className={`confirm`}
    />
  );
}

export default WarningDialog;
