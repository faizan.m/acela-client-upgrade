import * as React from "react";
import { Modal } from "react-bootstrap";
import "./style.scss";

interface IModalBaseProps {
  show: boolean;
  onClose: (e: any) => void;
  closeOnEscape?: boolean;
  closeOnOverlayClick?: boolean;
  titleElement?: string | JSX.Element;
  bodyElement?: string | JSX.Element;
  footerElement?: string | JSX.Element;
  className?: string;
  hideCloseButton?: boolean;
  backdropClassName?: string;
}

export default class ModalBase extends React.Component<IModalBaseProps, {}> {
  public static defaultProps: IModalBaseProps = {
    show: false,
    onClose: null,
    closeOnEscape: true,
    closeOnOverlayClick: false,
  };

  render() {
    return (
      <Modal
        show={this.props.show}
        onHide={this.props.onClose}
        backdropClassName={this.props.backdropClassName}
        backdrop={this.props.closeOnOverlayClick ? true : "static"}
        keyboard={this.props.closeOnEscape}
        className={this.props.className}
      >
        {this.props.titleElement && (
          <Modal.Header closeButton={!this.props.hideCloseButton}>
            {this.props.titleElement}
          </Modal.Header>
        )}
        {this.props.bodyElement && (
          <Modal.Body>{this.props.bodyElement}</Modal.Body>
        )}
        {this.props.footerElement && (
          <Modal.Footer>{this.props.footerElement}</Modal.Footer>
        )}
      </Modal>
    );
  }
}
