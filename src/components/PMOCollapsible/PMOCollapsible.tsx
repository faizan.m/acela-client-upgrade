import React, { useEffect, useState } from 'react';
import './PMOCollapsible.scss';
import { Collapse } from 'react-bootstrap';

interface ICollapsibleProps {
  label: any;
  children: any;
  isOpen?: boolean;
  statusClass?: string;
  parentClass?: string;
  background?: string;
}

const PMOCollapsible: React.FC<ICollapsibleProps> = ({ label, ...props }) => {
  const [isOpen, setIsOpen] = useState(props.isOpen);
  useEffect(() => {
    setIsOpen(props.isOpen);
  }, [props.isOpen]);
  return (
    <div className={`PMOCollapsible-component ${props.parentClass || ''}`}>
      <div
        className={`label  
        ${props.statusClass}
        ${isOpen ? "field--not-collapsed" : "field--collapsed"}`}
        aria-controls={`collapsable-item-${label}`}
        aria-expanded={isOpen}
        onClick={() => setIsOpen(!isOpen)}
      >
        <div className="action-collapse right">
          {!isOpen ? <img
            className="arrow-play"
            alt=""
            src={'/assets/icons/play-arrow.svg'}
          /> : <img
            className="arrow-play down"
            alt=""
            src={'/assets/icons/play-arrow.svg'}
          />}
        </div>
        <div className={`collapsable-headings`}>{label}</div>

      </div>
      {
        isOpen &&
        <Collapse in={isOpen}>
          <div className='collapsable-body' id={`collapsable-item-${label}`}>{props.children}</div>
        </Collapse>
      }

    </div>
  );
};

export default PMOCollapsible;
