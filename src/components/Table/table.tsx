import * as React from "react";

// import ReactTable from "react-table";
// import checkboxHOC from "react-table/lib/hoc/selectTable";

import "./style.scss";

interface IManualProps {
  manual: boolean;
  pages: number;
  reset?: boolean;
  onFetchData: (params: IServerPaginationParams) => void;
}

interface ITableProps {
  columns: any;
  rows: any;
  onRowClick?: (event: any) => void;
  noDataText?: string;
  rowSelection?: {
    showCheckbox: boolean;
    selectIndex: string;
    onRowsToggle: (rows: any) => void;
    selectedIndexes?: any[];
  };
  pageSize?: number;
  customTopBar?: any;
  className?: string;
  resizable?: boolean;
  manualProps?: IManualProps;
  defaultSorted?: any;
  loading?: any;
}

interface ITableState {
  selectAll: boolean;
  selectedIds: any[];
  page?: number;
  pageSize?: number;
}

// const CheckboxTable = checkboxHOC(ReactTable);

export default class Table extends React.Component<ITableProps, ITableState> {
  static defaultProps: ITableProps = {
    columns: [],
    rows: [],
    onRowClick: () => null,
    rowSelection: {
      showCheckbox: false,
      selectIndex: null,
      onRowsToggle: () => null,
      selectedIndexes: null,
    },
    noDataText: "No Rows Found",
    customTopBar: null,
    className: "",
    resizable: true,
    manualProps: {
      pages: 0,
      manual: false,
      onFetchData: () => null,
    },
    defaultSorted: [],
    loading: false,
  };

  private checkboxTable: any = null;

  constructor(props: ITableProps) {
    super(props);

    this.state = {
      selectAll: false,
      selectedIds: [],
      page: 0,
      pageSize: props.pageSize || 25,
    };
  }

  componentDidUpdate(prevProps: ITableProps) {
    if (this.props.rows !== prevProps.rows) {
      prevProps.manualProps.manual === true
        ? this.setState({
            selectAll: false,
            selectedIds: [],
          })
        : this.setState({
            selectAll: false,
            selectedIds:
              (this.props.rowSelection &&
                this.props.rowSelection.selectedIndexes) ||
              [],
            page: 0,
          });
    }
  }

  toggleSelection = (key, shift, row) => {
    let selectedIds = [...this.state.selectedIds];
    const keyIndex = selectedIds.indexOf(key);

    if (keyIndex >= 0) {
      selectedIds = [
        ...selectedIds.slice(0, keyIndex),
        ...selectedIds.slice(keyIndex + 1),
      ];
    } else {
      selectedIds.push(key);
    }

    this.setState({
      selectedIds,
      selectAll: false,
    });

    this.props.rowSelection.onRowsToggle(selectedIds);
  };

  toggleAll = () => {
    const wrappedInstance = this.checkboxTable.getWrappedInstance();
    const currentRecords = wrappedInstance.getResolvedState().sortedData;
    // const PS = wrappedInstance.getResolvedState().pageSize;
    // const PN = wrappedInstance.getResolvedState().page + 1;
    const selectAll = this.state.selectAll ? false : true;
    const selectedIds = [];
    // const from = PS * (PN - 1);
    // const to = from + (PS - 1);
    // const records = currentRecords.slice(from, to + 1);
    if (selectAll) {
      currentRecords.forEach((item, index) => {
        selectedIds.push(item._original[this.props.rowSelection.selectIndex]);
      });
    }

    this.setState({
      selectedIds,
      selectAll,
    });

    this.props.rowSelection.onRowsToggle(selectedIds);
  };

  getData = () => {
    const data =
      this.props.rows &&
      this.props.rows.map((item, index) => {
        return {
          _id: item[this.props.rowSelection.selectIndex],
          ...item,
        };
      });

    return data;
  };

  onManualFetchData = (state, instance) => {
    // react table indexes from 0,
    // whereas the server pagination
    // index starts from 1.
    const nextPage = state.page + 1;
    const params: IServerPaginationParams = {
      page: nextPage,
      page_size: state.pageSize,
    };
    if (state.sorted.length > 0) {
      const sortBy = state.sorted[0];
      params.ordering = sortBy.desc ? `-${sortBy.id}` : sortBy.id;
    }

    this.props.manualProps.onFetchData(params);
  };

  isSelected = (key) => {
    return this.state.selectedIds.includes(key);
  };

  renderTable() {
    let pageNumber = this.state.page;

    // const commonProps = {
    //   columns: this.props.columns,
    //   defaultPageSize: this.props.pageSize || 25,
    //   noDataText: this.props.noDataText,
    //   minRows: 0,
    //   resizable: this.props.resizable,
    // };
    // const rowSelectionProps = {
    //   isSelected: this.isSelected,
    //   toggleAll: this.toggleAll,
    //   toggleSelection: this.toggleSelection,
    //   selectType: "checkbox",
    //   selectAll: this.state.selectAll,
    // };
    // const manualProps =
    //   this.props.manualProps && this.props.manualProps.manual
    //     ? {
    //         ...this.props.manualProps,
    //         onFetchData: this.onManualFetchData,
    //       }
    //     : {};

    pageNumber =
      this.props.manualProps && this.props.manualProps.reset === true
        ? 0
        : pageNumber;

    // return this.props.rowSelection.showCheckbox ? (
    //   this.props.manualProps.reset ? (
    //     <CheckboxTable
    //       ref={(r) => (this.checkboxTable = r)}
    //       className={`table--checked ${this.props.className}`}
    //       {...commonProps}
    //       {...rowSelectionProps}
    //       {...manualProps}
    //       data={this.getData()}
    //       getTrProps={(state, rowInfo, column, instance) => {
    //         return {
    //           onClick: (e, handleOriginal) => {
    //             return this.props.onRowClick && this.props.onRowClick(rowInfo);
    //           },
    //         };
    //       }}
    //       loading={this.props.loading}
    //       page={pageNumber}
    //       onPageChange={(page) =>
    //         this.setState({
    //           page,
    //         })
    //       }
    //       pageSize={this.props.pageSize}
    //       onPageSizeChange={(e) => {
    //         this.setState({ pageSize: e, page: 0 });
    //       }}
    //       previousText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-left.svg"
    //         />
    //       }
    //       nextText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-right.svg"
    //         />
    //       }
    //     />
    //   ) : (
    //     <CheckboxTable
    //       ref={(r) => (this.checkboxTable = r)}
    //       className={`table--checked ${this.props.className}`}
    //       {...commonProps}
    //       {...rowSelectionProps}
    //       {...manualProps}
    //       data={this.getData()}
    //       getTrProps={(state, rowInfo, column, instance) => {
    //         return {
    //           onClick: (e, handleOriginal) => {
    //             return this.props.onRowClick && this.props.onRowClick(rowInfo);
    //           },
    //         };
    //       }}
    //       loading={this.props.loading}
    //       onPageChange={(page) =>
    //         this.setState({
    //           page,
    //         })
    //       }
    //       pageSize={this.props.pageSize}
    //       onPageSizeChange={(e) => {
    //         this.setState({ pageSize: e, page: 0 });
    //       }}
    //       previousText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-left.svg"
    //         />
    //       }
    //       nextText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-right.svg"
    //         />
    //       }
    //     />
    //   )
    // ) : this.props.manualProps.manual ? (
    //   this.props.manualProps.reset ? (
    //     <ReactTable
    //       className={`${this.props.className}`}
    //       {...commonProps}
    //       {...manualProps}
    //       data={this.props.rows}
    //       page={pageNumber}
    //       getTrProps={(state, rowInfo, column, instance) => {
    //         return {
    //           onClick: (e, handleOriginal) => {
    //             return this.props.onRowClick && this.props.onRowClick(rowInfo);
    //           },
    //         };
    //       }}
    //       defaultSorted={this.props.defaultSorted}
    //       onPageChange={(page) => this.setState({ page })}
    //       pageSize={this.props.pageSize}
    //       loading={this.props.loading}
    //       onPageSizeChange={(e) => {
    //         this.setState({ pageSize: e, page: 0 });
    //       }}
    //       previousText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-left.svg"
    //         />
    //       }
    //       nextText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-right.svg"
    //         />
    //       }
    //     />
    //   ) : (
    //     <ReactTable
    //       className={`${this.props.className}`}
    //       {...commonProps}
    //       {...manualProps}
    //       data={this.props.rows}
    //       getTrProps={(state, rowInfo, column, instance) => {
    //         return {
    //           onClick: (e, handleOriginal) => {
    //             return this.props.onRowClick && this.props.onRowClick(rowInfo);
    //           },
    //         };
    //       }}
    //       defaultSorted={this.props.defaultSorted}
    //       onPageChange={(page) => this.setState({ page })}
    //       pageSize={this.props.pageSize}
    //       loading={this.props.loading}
    //       onPageSizeChange={(e) => {
    //         this.setState({ pageSize: e, page: 0 });
    //       }}
    //       previousText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-left.svg"
    //         />
    //       }
    //       nextText={
    //         <img
    //           className="arrow-table"
    //           alt=""
    //           src="/assets/icons/arrow-point-to-right.svg"
    //         />
    //       }
    //     />
    //   )
    // ) : (
    //   <ReactTable
    //     className={`${this.props.className}`}
    //     {...commonProps}
    //     {...manualProps}
    //     data={this.props.rows}
    //     getTrProps={(state, rowInfo, column, instance) => {
    //       return {
    //         onClick: (e, handleOriginal) => {
    //           return this.props.onRowClick && this.props.onRowClick(rowInfo);
    //         },
    //       };
    //     }}
    //     page={pageNumber}
    //     defaultSorted={this.props.defaultSorted}
    //     onPageChange={(page) => this.setState({ page })}
    //     pageSize={this.state.pageSize}
    //     onPageSizeChange={(e) => {
    //       this.setState({ pageSize: e, page: 0 });
    //     }}
    //     loading={this.props.loading}
    //     previousText={
    //       <img
    //         className="arrow-table"
    //         alt=""
    //         src="/assets/icons/arrow-point-to-left.svg"
    //       />
    //     }
    //     nextText={
    //       <img
    //         className="arrow-table"
    //         alt=""
    //         src="/assets/icons/arrow-point-to-right.svg"
    //       />
    //     }
    //   />
    // );
    return null;
  }
  render() {
    return (
      <div className="table-container">
        {this.props.customTopBar}
        <div className="table-container__table">{this.renderTable()}</div>
      </div>
    );
  }
}
