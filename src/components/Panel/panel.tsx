import React from 'react';
import './style.scss';

const RightPanel: React.FC<any> = (props) => {

  return (
    <div className="custom-right-panel">
      {props.children}
    </div>
  );
};

export default RightPanel;
