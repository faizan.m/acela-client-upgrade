import React from 'react';
import { Button } from 'react-bootstrap';

import './style.scss';

interface IDeleteButtonProps {
  onClick: TButtonOnClick;
  type?: string;
  title?: string;
}

interface IDeleteButtonState {}

export default class DeleteButton extends React.Component<
  IDeleteButtonProps,
  IDeleteButtonState
> {
  render() {
    const { title, onClick, type } = this.props;

    return (
      <Button
        className={`${type ? type : 'delete'}-btn`}
        title={title ? title : 'Delete'}
        onClick={event => onClick(event)}
      />
    );
  }
}
