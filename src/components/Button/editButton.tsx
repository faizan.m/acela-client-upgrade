import React from 'react';
import { Button } from 'react-bootstrap';

import './style.scss';

interface IEditButtonProps {
  onClick: TButtonOnClick;
  title?: string;
}

interface IEditButtonState {}

export default class EditButton extends React.Component<
  IEditButtonProps,
  IEditButtonState
> {
  render() {
    const { onClick, title } = this.props;

    return (
      <Button
        title={title ? title : 'Edit'}
        className="edit-btn"
        onClick={event => onClick(event)}
      />
    );
  }
}
