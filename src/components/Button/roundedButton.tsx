import * as React from 'react';

import './style.scss';

export interface IRoundedButtonProps {
  active: boolean;
  content: any;
  onClick: TButtonOnClick;
  className?: string;
}

export default class Tag extends React.Component<IRoundedButtonProps, {}> {
  render() {
    return (
      <div
        className={`${this.props.className}
        rounded-btn ${this.props.active ? 'active' : ''}`}
        onClick={event => this.props.onClick(event)}
      >
        {this.props.content}
      </div>
    );
  }
}
