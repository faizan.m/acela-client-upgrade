import React from 'react';
import { Badge, Button } from 'react-bootstrap';

import './style.scss';

interface IAttachButtonProps {
  onClick: TButtonOnClick;
  bsStyle?: ButtonStyle;
  count?: number;
}

interface IAttachButtonState {}

export default class AttachButton extends React.Component<
  IAttachButtonProps,
  IAttachButtonState
> {
  static defaultProps = {
    count: 0,
    bsStyle: "default",
    onClick: null,
  };

  render() {
    const { onClick, count } = this.props;

    return (
      <Button
        className="attach-btn"
        onClick={event => onClick(event)}
        bsStyle={count > 0 ? "warning" : "default"}
      >
        {count > 0 && <Badge>{count}</Badge>}
      </Button>
    );
  }
}
