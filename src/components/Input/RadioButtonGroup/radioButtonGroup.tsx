import * as React from 'react';

import './style.scss';

interface IRadioButtonGroupProps {
  name: string;
  value: any;
  onChange: TButtonOnClick;
  onBlur?: TButtonOnClick;
  placeholder?: string;
  disabled?: boolean;
  required?: boolean;
  options: Array<{ value: string; label: string }>;
}

export default class RadioButtonGroup extends React.Component<
  IRadioButtonGroupProps,
  {}
> {
  onChange = e => {
    this.props.onChange({
      target: {
        name: this.props.name,
        value: e.target.value,
      },
    });
  };

  render() {
    return (
      <div className="radio-btn-group">
        {this.props.options.map((option, index) => (
          <div className="radio-btn" key={index}>
            <input
              type="radio"
              name={this.props.name}
              onClick={this.onChange}
              onChange={() => null}
              value={option.value}
              disabled={this.props.disabled}
              checked={option.value === this.props.value}
            />
            <span>{option.label}</span>
          </div>
        ))}
      </div>
    );
  }
}
