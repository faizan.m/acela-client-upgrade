import * as React from "react";
import ReactSelect, { createFilter } from "react-select";

import "./style.scss";

interface ISelectInputProps {
  name: string;
  value: any;
  clearable?: boolean;
  options: IPickListOptions[];
  onChange: TButtonOnClick;
  onBlur?: TButtonOnClick;
  disabled?: boolean;
  placeholder?: string;
  required?: boolean;
  multi?: boolean;
  searchable?: boolean;
  isOptionDisabled?: any;
  loading?: any;
  onInputChange?: any;
}

export default class SelectInput extends React.Component<
  ISelectInputProps,
  {}
> {
  public static defaultProps = {
    name: "",
    value: "",
    clearable: false,
    onChange: null,
    onBlur: null,
    placeholder: "Select Option",
    options: null,
    multi: false,
    disabled: false,
    searchable: true,
    loading: false,
  };

  onChange = (e: IPickListOptions[] | IPickListOptions) => {
    if (this.props.multi) {
      const newValue = (e as IPickListOptions[]).map((option) => option.value);
      this.props.onChange({
        target: { name: this.props.name, value: newValue, label: null },
      });
    } else {
      this.props.onChange({
        target: {
          name: this.props.name,
          value: (e && (e as IPickListOptions).value) || null,
          label: e && (e as IPickListOptions).label,
          e,
        },
      });
    }
  };

  onBlur = (e) => {
    this.props.onBlur && this.props.onBlur(e);
  };

  onInputChange = (e) => {
    this.props.onInputChange && this.props.onInputChange(e);
  };

  render() {
    const {
      name,
      options,
      value,
      clearable,
      placeholder,
      multi,
      disabled,
      searchable,
      loading,
    } = this.props;

    let valueData: IPickListOptions[] = [];
    let newOptions = options ?? [];
    if (multi) {
      const valuesMap = new Set(value.map((el: string | number) => String(el)));
      valueData = newOptions.filter((el) => valuesMap.has(String(el.value)));
    } else {
      valueData = newOptions.filter((el) => el.value == value);
    }

    const customFilter = createFilter({
      ignoreCase: true,
      matchFrom: "any",
    });

    return (
      <ReactSelect
        value={valueData}
        name={name}
        options={newOptions}
        isDisabled={disabled}
        isOptionDisabled={(option: any) => option.disabled === true}
        isClearable={clearable}
        onChange={this.onChange}
        onBlur={this.onBlur}
        placeholder={placeholder}
        isSearchable={searchable}
        isLoading={loading}
        isMulti={multi}
        menuPortalTarget={document.body}
        onInputChange={this.onInputChange}
        filterOption={customFilter}
        className={"Select" + (this.props.multi ? " Select--multi" : "")}
        classNamePrefix="Select"
        // menuIsOpen={true}
      />
    );
  }
}
