import * as React from "react";
import { Col, Row } from "react-bootstrap";
import DateTimePicker from "./DatePicker/datePicker";
import RadioButtonGroup from "./RadioButtonGroup/radioButtonGroup";
import Select from "./Select/select";

import "./style.scss";
interface IInputProps {
  field: IInputField;
  name: string;
  width: number;
  widthMD?: number;
  placeholder?: string;
  onChange: (event: any) => void;
  onBlur?: (event: any) => void;
  handleKeyDown?: (event: any) => void;
  className?: string;
  disabled?: boolean;
  error?: IFieldValidation;
  labelIcon?: string;
  labelTitle?: any;
  accept?: string;
  showDate?: boolean;
  showTime?: boolean;
  hideIcon?: boolean;
  defaultValue?: any;
  multi?: boolean;
  loading?: boolean;
  disablePrevioueDates?: boolean;
  disableFutureDates?: boolean;
  clearable?: boolean;
  minimumValue?: string;
  maximumValue?: string;
  customInput?: any;
  title?: string;
  onLabeIconClick?: any;
  disableTimezone?: boolean;
  autoFocus?: boolean;
  onMouseDown?: (event: any) => void;
  showErrorOnDisabled?: boolean;
  onlyInteger?: boolean;
}

export default class Input extends React.Component<
  IInputProps,
  { showInfo: boolean }
> {
  static defaultProps = {
    name: "",
    width: 12,
    placeholder: "",
    onChange: () => null,
    disabled: false,
    onBlur: () => null,
    error: {
      errorState: "success",
      errorMessage: "",
    },
    multi: false,
    loading: false,
    clearable: false,
    showErrorOnDisabled: false,
  };

  constructor(props: any) {
    super(props);
    this.state = {
      showInfo: false,
    };
  }

  renderField = () => {
    // TODO: add placeholders for all the input types
    // currenly only type=text supported.
    const { name, field, placeholder, multi } = this.props;

    if (field.type === "PICKLIST") {
      let newOptions: any = field.options;

      if (field.options && typeof field.options[0] === "string") {
        newOptions = field.options.map((option) => ({
          value: option,
          label: option,
        }));
      }

      return (
        <Select
          name={name}
          value={field.value}
          disabled={this.props.disabled}
          onChange={(event) => this.props.onChange(event)}
          onBlur={(event) => this.props.onBlur(event)}
          placeholder={placeholder}
          options={newOptions}
          multi={multi}
          loading={this.props.loading}
          clearable={this.props.clearable}
        />
      );
    } else if (field.type === "DATE") {
      return (
        <DateTimePicker
          name={name}
          onChange={(event) => this.props.onChange(event)}
          onBlur={(event) => this.props.onBlur(event)}
          value={
            field.value
              ? this.props.disableTimezone
                ? field.value
                : new Date(field.value)
              : null
          }
          required={field.isRequired}
          placeholder={placeholder}
          showDate={this.props.showDate}
          showTime={this.props.showTime}
          hideIcon={this.props.hideIcon}
          disabled={this.props.disabled}
          defaultValue={this.props.defaultValue}
          disablePrevioueDates={this.props.disablePrevioueDates}
          disableFutureDates={this.props.disableFutureDates}
          disableTimezone={this.props.disableTimezone}
        />
      );
    } else if (field.type === "TEXTAREA") {
      return (
        <textarea
          rows={3}
          name={name}
          onChange={(event) => this.props.onChange(event)}
          onBlur={(event) => this.props.onBlur(event)}
          value={field.value ? field.value : ""}
          required={field.isRequired}
          disabled={this.props.disabled}
          placeholder={placeholder}
          autoFocus={this.props.autoFocus}
        />
      );
    } else if (field.type === "NUMBER") {
      const numberInputOnWheelPreventChange = (e) => {
        // Prevent the input value change
        e.target.blur();

        // Prevent the page/container scrolling
        e.stopPropagation();

        // Refocus immediately, on the next tick (after the current
        // function is done)
        setTimeout(() => {
          e.target.focus();
        }, 0);
      };
      return (
        <input
          type={field.type}
          placeholder={placeholder ? placeholder : "Enter Number"}
          name={name}
          onChange={(event) => this.props.onChange(event)}
          onBlur={(event) => this.props.onBlur(event)}
          value={field.value}
          disabled={this.props.disabled}
          min={this.props.minimumValue}
          max={this.props.maximumValue}
          onKeyDown={this.props.handleKeyDown}
          onWheel={numberInputOnWheelPreventChange}
          onWheelCapture={numberInputOnWheelPreventChange}
          step={this.props.onlyInteger ? 1 : undefined}
        />
      );
    } else if (field.type === "FILE") {
      return (
        <input
          type={field.type}
          name={name}
          onChange={(event) => this.props.onChange(event)}
          //value={field.value ? field.value : ''}
          accept={this.props.accept}
          id={field.id}
        />
      );
    } else if (field.type === "COLOR") {
      return (
        <input
          type={field.type}
          name={name}
          onChange={(event) => this.props.onChange(event)}
          value={field.value ? field.value : ""}
        />
      );
    } else if (field.type === "RADIO") {
      let newOptions: any = field.options;
      if (field.options && typeof field.options[0] === "string") {
        newOptions = field.options.map((option) => ({
          value: option,
          label: option,
        }));
      }

      return (
        <RadioButtonGroup
          name={name}
          onChange={this.props.onChange}
          disabled={this.props.disabled}
          value={field.value}
          options={newOptions}
        />
      );
    } else if (field.type === "CUSTOM") {
      return this.props.customInput;
    } else {
      return (
        <input
          type={field.type}
          placeholder={placeholder ? placeholder : ""}
          name={name}
          onChange={(event) => this.props.onChange(event)}
          onBlur={(event) => this.props.onBlur(event)}
          onKeyDown={this.props.handleKeyDown}
          value={field.value ? field.value : ""}
          disabled={this.props.disabled}
          autoFocus={this.props.autoFocus}
          onMouseDown={this.props.onMouseDown}
          className="inpt"
        />
      );
    }
  };

  render() {
    const {
      width,
      widthMD,
      field,
      className,
      labelIcon,
      labelTitle,
      title,
    } = this.props;
    const elementWidth = width === 13 ? undefined : width;
    const elementWidthMD =
      field.type === "TEXTAREA" ? widthMD : widthMD;
    const { errorState, errorMessage } = this.props.error;
    let containerClassName =
      errorState === "error"
        ? "field-section field-section--error"
        : "field-section";
    containerClassName = field.isRequired
      ? `${containerClassName} field-section--required`
      : containerClassName;

    return (
      <Col
        className={`${containerClassName} ${className ? className : ""} ${
          this.props.disabled ? "disabled" : ""
        }`}
        xs={elementWidth}
        md={elementWidthMD ? elementWidthMD : elementWidth}
      >
        <Row className="field__label">
          <label
            className="field__label-label"
            title={
              field.label && typeof field.label === "string" ? field.label : ""
            }
          >
            {field.label}
          </label>
          {labelIcon && labelTitle && (
            <div
              className={`tooltip-help-icon ${
                this.state.showInfo ? "tooltip-tail" : ""
              }`}
              onClick={() => {
                this.setState({ showInfo: !this.state.showInfo });
              }}
            >
              <img
                alt=""
                className={"d-pointer rule-info"}
                src={`/assets/new-icons/${labelIcon}.svg`}
                title={labelTitle}
              />
            </div>
          )}
          <span className="field__label-required" />
        </Row>
        {this.state.showInfo && (
          <Row className="tooltip-input-body">
            <img
              className="cross-sign"
              alt=""
              src="/assets/new-icons/cross_sign.svg"
              onClick={() => {
                this.setState({ showInfo: !this.state.showInfo });
              }}
            />
            {labelTitle}
          </Row>
        )}
        <Row className="field__input" title={title}>
          {this.renderField()}
        </Row>
        {(!this.props.disabled || this.props.showErrorOnDisabled) && (
          <span
            className={
              errorState === "warning"
                ? "field__warning"
                : "field__error"
            }
          >
            {errorState === "error" ||
            errorState === "warning"
              ? errorMessage
              : ""}
          </span>
        )}
      </Col>
    );
  }
}
