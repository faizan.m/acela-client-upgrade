import * as React from 'react';
import DateTime from 'react-datetime';

import moment, { Moment } from 'moment';
import './style.scss';

interface ISelectInputProps {
  name: string;
  value: any;
  onChange: TButtonOnClick;
  onBlur: TButtonOnClick;
  placeholder?: string;
  valid?: any;
  disabled?: boolean;
  required?: boolean;
  showDate: boolean;
  showTime: boolean;
  hideIcon: boolean;
  defaultValue?: any;
  readOnly?: boolean;
  disablePrevioueDates?: boolean;
  disableFutureDates?: boolean;
  disableTimezone?: boolean;
}

export default class DateTimePicker extends React.Component<
  ISelectInputProps,
  {showError: boolean}
> {
  static defaultProps = {
    name: '',
    value: null,
    placeholder: '',
    disabled: false,
    valid: null,
    required: false,
    showDate: true,
    showTime: true,
    defaultValue: '',
    readOnly: false,
    hideIcon: false,
  };
  constructor(props: any) {
    super(props);
    this.state = {
      showError: false
    };
  }
  onChange = momentObj => {
    if (momentObj._isValid) {
      this.props.onChange({
        target: {
          name: this.props.name,
          value: momentObj,
        },
      });
      this.setState({showError: false})
    }
  };
  onBlur = momentObj => {
    if (!momentObj._isValid && momentObj !== "") {
      this.setState({showError: true})
      this.props.onChange({
        target: {
          name: this.props.name,
          value: moment(new Date()),
        },
      });
    }
  };

  // TODO: add renderInput after the type has been added to react date time.
  renderInput = (props, openCalendar, closeCalendar) => {
    const clear = () => {
      props.onChange({ target: { value: '' } });
    };

    return (
      <div>
        <input {...props} readonly="readonly" />
        <button onClick={openCalendar}>open calendar</button>
        <button onClick={closeCalendar}>close calendar</button>
        <button onClick={clear}>clear</button>
      </div>
    );
  };

  render() {
    const {
      name,
      value,
      placeholder,
      showDate,
      showTime,
      hideIcon,
      disabled,
      required,
      defaultValue,
      readOnly,
      disablePrevioueDates,
      disableFutureDates,
      disableTimezone,
    } = this.props;
    const today = moment();
    const yesterday = moment().subtract(1, 'day');
    const valid = (current: Moment) => {
      return disablePrevioueDates
        ? current.isAfter(yesterday)
        : disableFutureDates
        ? current.isBefore(today)
        : true;
    };
    const utcTime = value;
    const offset = moment(value && value._d ? value._d : value).utcOffset();

    return (
      <>
      <DateTime
        inputProps={{ 
          name,
          placeholder,
          disabled,
          required,
          autoComplete: 'none',
          readOnly,
        }}
        value={disableTimezone ? moment.utc(utcTime) : moment.utc(utcTime).utcOffset(offset)}
        isValidDate={disablePrevioueDates || disableFutureDates ? valid : null}
        onChange={e => this.onChange(e)}
        onBlur={e=> this.onBlur(e)}
        closeOnSelect={true}
        timeFormat={showTime ? 'hh:mm A' : false}
        dateFormat={showDate ? 'MM-DD-YYYY' : false}
        defaultValue={defaultValue}
        className={hideIcon ? 'icon-hide' : 'show'}
      />
      {
        this.state.showError && <div className="invalid-date-picker-selection"> Invalid Date selection</div>
      }
      </>
    );
  }
}
