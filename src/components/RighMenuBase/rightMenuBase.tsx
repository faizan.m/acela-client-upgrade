import * as React from 'react';
import './style.scss';

interface IRightMenuProps {
  show: boolean;
  onClose: (e: any) => void;
  titleElement?: any;
  bodyElement?: any;
  footerElement?: any;
  className?: string;
}

export default class RightMenu extends React.Component<IRightMenuProps, {}> {
  public static defaultProps: IRightMenuProps = {
    show: false,
    onClose: null,
    titleElement: 'REPLACE TITLE',
    bodyElement: 'REPLACE BODY',
    footerElement: 'REPLACE FOOTER',
  };

  render() {
    return (
      <div className="right-menu">
        <div className={`sidebar  ${this.props.className} ${this.props.show ? 'sidebar-open' : 'sidebar-close'}`} >

          <div className="title">
          <a className="closebtn" onClick={this.props.onClose}> Back</a>

            {this.props.titleElement}
          </div>
          <div className="body">{this.props.bodyElement}</div>
          <div className="footer">{this.props.footerElement}</div>
        </div>
      </div>
    );
  }
}
