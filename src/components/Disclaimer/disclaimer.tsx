import React from 'react';
import { connect } from 'react-redux';
import SquareButton from '../Button/button';

interface IDisclaimerProps extends ICommonProps {
  configStatus: IConfigStatus;
}
interface IDisclaimerState {}

class Disclaimer extends React.Component<IDisclaimerProps, IDisclaimerState> {
  click = () => {
    this.props.history.push(`/setting/erp`);
  };

  clickManufacturer = () => {
    this.props.history.push(`/setting/api-credentials`);
  };

  render() {
    return (
      <div className="disclaimer">
        <h4>
          Settings has not been configured. Please contact the site
          administrator.
        </h4>
        <div style={{ display: 'flex' }}>
          {(!this.props.configStatus.crm_authentication_configured ||
            !this.props.configStatus.crm_board_mapping_configured ||
            !this.props.configStatus.crm_device_categories_configured) && (
            <SquareButton
              content="Go to ERP Setting >> Click here"
              onClick={this.click}
              bsStyle={"primary"}
            />
          )}

          {this.props.configStatus.crm_authentication_configured &&
            this.props.configStatus.crm_board_mapping_configured &&
            this.props.configStatus.crm_device_categories_configured &&
            !this.props.configStatus.device_manufacturer_api_configured && (
              <SquareButton
                content="Go to Manufacturer Setting >> Click here"
                onClick={this.clickManufacturer}
                bsStyle={"primary"}
              />
            )}
        </div>
      </div>
    );
  }
}

//export default Disclaimer;

const mapStateToProps = (state: IReduxStore) => ({
  configStatus: state.providerIntegration.configStatus,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Disclaimer);
