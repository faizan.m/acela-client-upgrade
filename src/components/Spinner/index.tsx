import React, { Component } from 'react';

import loader from './loading.gif';

import './style.scss';

interface ISpinnerProps {
  show: boolean;
  className?: string;
}

class Spinner extends Component<ISpinnerProps> {
  render() {
    if (this.props.show) {
      return (
        <div className={`spinner ${this.props.className}`}>
          <img src={loader} />
        </div>
      );
    } else {
      return null;
    }
  }
}

export default Spinner;
