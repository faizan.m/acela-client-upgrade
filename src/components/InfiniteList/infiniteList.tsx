import { cloneDeep, debounce, isEqual } from "lodash";
import React from "react";
// import TagsInput from "react-tagsinput"; (New Component)
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import { fetchInfiniteList } from "../../actions/orderTracking";
import Checkbox from "../Checkbox/checkbox";
import Input from "../Input/input";
import Spinner from "../Spinner";
import "./infinite.scss";

interface IInfiniteListCheckbox {
  key: string;
  value: boolean;
  name: string;
  trueValue?: string;
  falseValue?: string;
}

interface IInfiniteListProps<T> {
  url: string;
  columns: any | IColumnInfinite<T>[];
  showSearch?: boolean;
  showIncludeExclude?: boolean;
  checkBoxfilters?: IInfiniteListCheckbox[];
  radioBoxFilter?: any;
  filterFunction?: Function;
  id?: string;
  height?: string;
  refreshKey?: string | number;
  filters?: object;
  className?: string;
  fetchInfiniteList: (
    url: string,
    params?: IScrollPaginationFilters
  ) => Promise<any>;
}

interface IInfiniteListState<T> {
  rows: T[];
  pagination: any;
  inputValue: string;
  noData: boolean;
  loading: boolean;
  isIncludeMode: boolean;
  checkBoxfilters: IInfiniteListCheckbox[];
  radioBoxFilter: any;
}

class InfiniteListing<T> extends React.Component<
  IInfiniteListProps<T>,
  IInfiniteListState<T>
> {
  constructor(props: IInfiniteListProps<T>) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 1000);
  }

  getEmptyState = () => ({
    rows: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: "",
      search: "",
      include: [],
      exclude: [],
      count: 0,
    },
    inputValue: "",
    noData: false,
    loading: true,
    checkBoxfilters: [],
    isIncludeMode: true,
    radioBoxFilter: [],
  });

  componentDidMount() {
    if (this.state.rows && this.state.rows.length === 0) {
      this.setState(
        {
          checkBoxfilters: this.props.checkBoxfilters || [],
          radioBoxFilter: this.props.radioBoxFilter,
        },
        () => this.fetchMoreData(true)
      );
    }
  }
  componentDidUpdate(prevProps: IInfiniteListProps<T>) {
    if (
      prevProps.refreshKey &&
      prevProps.refreshKey !== this.props.refreshKey
    ) {
      this.fetchMoreData(true);
    }
    if (prevProps.filters && !isEqual(this.props.filters, prevProps.filters)) {
      this.fetchMoreData(true);
    }
  }

  onSearchStringChange = (e) => {
    const search = e.target.value;
    this.setState(
      (prevState) => ({
        rows: [],
        pagination: {
          ...prevState.pagination,
          currentPage: 0,
          nextPage: 1,
        },
        inputValue: search,
      }),
      () => {
        this.updateMessage(search);
      }
    );
  };

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };

  handleTagChange = (tags: string[], include: boolean) => {
    const key: string = include ? "include" : "exclude";
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          [key]: tags,
        },
      }),
      () => this.fetchMoreData(true)
    );
  };

  handleChangeIncExcRadio = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      isIncludeMode: e.target.value === "Include",
    });
  };

  fetchMoreData = (clearData: boolean = false) => {
    let newParams = cloneDeep(this.state.pagination);
    this.setState({
      loading: true,
    });

    if (clearData) {
      newParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
        include: this.state.pagination.include,
        exclude: this.state.pagination.exclude,
      };
    }
    if (newParams.nextPage !== null) {
      const filter = this.props.filters ? { ...this.props.filters } : {};
      this.state.checkBoxfilters.map((x) => {
        filter[x.key] =
          x.trueValue || x.falseValue
            ? x.value
              ? x.trueValue
              : x.falseValue
            : x.value;
      });
      if (
        this.props.radioBoxFilter &&
        this.state.radioBoxFilter &&
        this.state.radioBoxFilter.value !== ""
      ) {
        filter[this.props.radioBoxFilter.key] = this.state.radioBoxFilter.value;
      }
      if (this.state.pagination.include.length)
        newParams.include = newParams.include.join(",");
      else delete newParams.include;
      if (this.state.pagination.exclude.length)
        newParams.exclude = newParams.exclude.join(",");
      else delete newParams.exclude;
      newParams = {
        ...newParams,
        ...filter,
      };

      this.props.fetchInfiniteList(this.props.url, newParams).then((action) => {
        if (action.response && action.response.results) {
          let rows = [];
          let fetchedData = action.response.results;
          if (this.props.filterFunction) {
            fetchedData = fetchedData.filter(this.props.filterFunction);
          }
          if (clearData) {
            rows = [...fetchedData];
          } else {
            rows = [...this.state.rows, ...fetchedData];
          }
          const newPagination = {
            ...this.state.pagination,
            currentPage: action.response.links.page_number,
            nextPage: action.response.links.next_page_number,
            page_size: this.state.pagination.page_size,
            ordering: this.state.pagination.ordering,
            search: this.state.pagination.search,
            count: action.response.count,
          };

          this.setState({
            pagination: newPagination,
            rows,
            noData: rows.length === 0 ? true : false,
            loading: false,
          });
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  fetchByOrder = (field: string) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  getOrderClass = (field: string) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  rowListingRows = (row: T, index: number) => {
    return (
      <div
        className={`row-panel ${index % 2 !== 0 ? "odd" : "even"}`}
        key={`row-${index}`}
      >
        {this.props.columns.map((x, i) => {
          return (
            <div
              className={"cell" + `${x.className ? " " + x.className : ""}`}
              title={row[x.id]}
              key={i}
            >
              {x.Cell ? x.Cell(row) : row[x.id]}
            </div>
          );
        })}
      </div>
    );
  };

  handleChangeCheckBox = (
    event: React.ChangeEvent<HTMLInputElement>,
    i: number
  ) => {
    const targetValue = event.target.checked;
    const newState = cloneDeep(this.state);
    newState.checkBoxfilters[i].value = targetValue;
    this.setState(newState);
    this.updateMessage(this.state.pagination.search);
  };

  handleChangeRadioBox = (event: React.ChangeEvent<HTMLInputElement>) => {
    const targetValue = event.target.value;
    this.setState(
      (prevState) => ({
        rows: [],
        radioBoxFilter: {
          ...prevState.radioBoxFilter,
          value: targetValue,
        },
        pagination: {
          ...prevState.pagination,
          currentPage: 0,
          nextPage: 1,
        },
      }),
      () => {
        this.updateMessage(this.state.pagination.search);
      }
    );
  };
  render() {
    const columns = this.props.columns;
    const noFilters: boolean =
      this.state.pagination.include.length +
        this.state.pagination.exclude.length ===
      0;
    return (
      <div className={`infinite-list-component ${this.props.className}`}>
        <div className="top-action">
          {this.props.showSearch && (
            <Input
              field={{
                value: this.state.inputValue,
                label: "",
                type: "SEARCH",
              }}
              width={10}
              name="searchString"
              onChange={this.onSearchStringChange}
              placeholder="Search"
              className={`custom-search custom-search-outside ${
                this.props.showIncludeExclude ? "search-before-incl-excl" : ""
              }`}
            />
          )}
          {/* this.props.showIncludeExclude && (
            <div className="include-exclude-infinite">
              <TagsInput
                value={
                  this.state.isIncludeMode
                    ? this.state.pagination.include
                    : this.state.pagination.exclude
                }
                onChange={(tags: string[]) =>
                  this.handleTagChange(tags, this.state.isIncludeMode)
                }
                inputProps={{
                  className: "react-tagsinput-input incl-excl-input",
                  placeholder: `Enter text to ${
                    this.state.isIncludeMode ? "include" : "exclude"
                  }`,
                }}
                className="list-header-tag"
                addOnBlur={true}
              />
              <Input
                field={{
                  options: ["Include", "Exclude"],
                  label: null,
                  type: "RADIO",
                  value: this.state.isIncludeMode ? "Include" : "Exclude",
                }}
                width={12}
                className="incl-excl-radio"
                name={`include_exclude_radio`}
                onChange={this.handleChangeIncExcRadio}
              />
              <div className="search-total-sep" />
            </div>
          )} (New Component) */}
          <div className="total-count">
            <span>Total : </span>
            {this.state.pagination.count || 0}
          </div>
          {this.props.showIncludeExclude && (
            <div className="infinite-list-filters">
              <div className="filters-h3">
                <img
                  alt=""
                  className="filter-img"
                  src="/assets/icons/filter.png"
                />
                <span>Applied Filters:</span>
              </div>
              {noFilters ? (
                <div className="no-filter-applied">No Filter Applied</div>
              ) : (
                <div className="tiles">
                  {this.state.pagination.include.map(
                    (el: string, idx: number) => (
                      <div key={idx} className="filter-tile green-tile">
                        {el}
                        <img
                          className={"d-pointer icon-remove"}
                          alt=""
                          src={"/assets/icons/cross-sign.svg"}
                          onClick={() => {
                            this.handleTagChange(
                              this.state.pagination.include.filter(
                                (el: string, index: number) => index !== idx
                              ),
                              true
                            );
                          }}
                        />
                      </div>
                    )
                  )}
                  {this.state.pagination.exclude.map(
                    (el: string, idx: number) => (
                      <div key={idx} className="filter-tile red-tile">
                        {el}
                        <img
                          className={"d-pointer icon-remove"}
                          alt=""
                          src={"/assets/icons/cross-sign.svg"}
                          onClick={() => {
                            this.handleTagChange(
                              this.state.pagination.exclude.filter(
                                (el: string, index: number) => index !== idx
                              ),
                              false
                            );
                          }}
                        />
                      </div>
                    )
                  )}
                </div>
              )}
            </div>
          )}
          <div className="check-box-filters">
            {this.state.checkBoxfilters &&
              this.state.checkBoxfilters.map((x, i) => {
                return (
                  <Checkbox
                    isChecked={x.value}
                    name="loa"
                    onChange={(e) => this.handleChangeCheckBox(e, i)}
                    key={i}
                  >
                    {x.name}
                  </Checkbox>
                );
              })}
          </div>
          {this.state.radioBoxFilter && this.props.radioBoxFilter && (
            <div className="radio-box-filters">
              <Input
                field={{
                  value: this.state.radioBoxFilter.value,
                  label: `${this.props.radioBoxFilter.name}`,
                  type: "RADIO",
                  isRequired: false,
                  options: this.props.radioBoxFilter.options,
                }}
                width={12}
                name={`${this.props.radioBoxFilter.name}`}
                className="input-radio-infi"
                onChange={this.handleChangeRadioBox}
              />
            </div>
          )}
        </div>

        <div className={`header`}>
          {columns.map((x, index) => {
            return (
              <div
                className={
                  "cell" +
                  `${x.className ? " " + x.className : ""}` +
                  `${
                    x.ordering
                      ? " cursor-pointer " + this.getOrderClass(x.ordering)
                      : ""
                  }`
                }
                onClick={() => {
                  if (x.ordering) {
                    this.fetchByOrder(x.ordering);
                  }
                }}
                key={index}
              >
                {x.name}
              </div>
            );
          })}
        </div>
        <div
          id={this.props.id}
          style={{
            position: "relative",
            height: this.props.height || "72vh",
            overflow: "auto",
          }}
        >
          <InfiniteScroll
            dataLength={this.state.rows.length}
            next={this.fetchMoreData}
            hasMore={this.state.pagination.nextPage ? true : false}
            loader={<h4 className="no-data"> Loading...</h4>}
            scrollableTarget={this.props.id}
          >
            {this.state.rows.map((row: T, index: number) =>
              this.rowListingRows(row, index)
            )}
            {!this.state.loading &&
              this.state.pagination.nextPage &&
              this.state.rows.length > 0 && (
                <div
                  className="load-more"
                  onClick={() => {
                    this.fetchMoreData(false);
                  }}
                >
                  Load more data...
                </div>
              )}
          </InfiniteScroll>
          {this.state.noData && this.state.rows.length === 0 && (
            <div className="no-data-inf">No data available</div>
          )}
          {this.state.loading && (
            <div className="loader-list">
              <Spinner show={true} />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchInfiniteList: (url: string, params?: IScrollPaginationFilters) =>
    dispatch(fetchInfiniteList(url, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InfiniteListing);
