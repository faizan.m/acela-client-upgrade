import React from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
// import ReactDiffViewer from "react-diff-viewer"; (New Component)
import { cloneDeep, debounce, isEqual } from "lodash";

import Spinner from "../Spinner";
import SquareButton from "../Button/button";
import BlinkingLoader from "../Loading/BlinkingLoader";

import { fetchInfiniteList } from "../../actions/orderTracking";
import { utcToLocalInLongFormat } from "../../utils/CalendarUtil";
import { getFieldNames } from "../../utils/fieldName";
import _ from "lodash";

import "./infinite.scss";

interface InfiniteHistoryProps<T> {
  url: string;
  id?: string;
  prohibited: any;
  height?: string;
  refreshKey?: string | number;
  className?: string;
  fetchInfiniteList: (
    url: string,
    params?: IScrollPaginationFilters
  ) => Promise<any>;
  previewDoc: any;
  checkoutSOW: any;
}

interface InfiniteHistoryState<T> {
  list: any;
  rows: T[];
  pagination: any;
  inputValue: string;
  noData: boolean;
  loading: boolean;
  isIncludeMode: boolean;
  isCollapsed: boolean[];
}

class InfiniteHistory<T> extends React.Component<
  InfiniteHistoryProps<T>,
  InfiniteHistoryState<T>
> {
  constructor(props: InfiniteHistoryProps<T>) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 1000);
  }

  getEmptyState = () => ({
    list: [],
    rows: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 6,
      ordering: "",
      search: "",
      include: [],
      exclude: [],
      count: 0,
    },
    inputValue: "",
    noData: false,
    loading: true,
    checkBoxfilters: [],
    isIncludeMode: true,
    radioBoxFilter: [],
    isCollapsed: [],
  });

  componentDidMount() {
    if (this.state.rows && this.state.rows.length === 0) {
      this.fetchMoreData(true);
    }
  }
  componentDidUpdate(prevProps: InfiniteHistoryProps<T>) {
    if (
      prevProps.refreshKey &&
      prevProps.refreshKey !== this.props.refreshKey
    ) {
      this.fetchMoreData(true);
    }
  }

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };

  fetchMoreData = (clearData: boolean = false) => {
    let newParams = cloneDeep(this.state.pagination);
    this.setState({
      loading: true,
    });

    if (clearData) {
      newParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
        include: this.state.pagination.include,
        exclude: this.state.pagination.exclude,
      };
    }
    if (newParams.nextPage !== null) {
      if (this.state.pagination.include.length)
        newParams.include = newParams.include.join(",");
      else delete newParams.include;
      if (this.state.pagination.exclude.length)
        newParams.exclude = newParams.exclude.join(",");
      else delete newParams.exclude;
      newParams = {
        ...newParams,
      };

      this.props.fetchInfiniteList(this.props.url, newParams).then((action) => {
        if (action.response && action.response.results) {
          let rows = [];
          let fetchedData = action.response.results;

          if (clearData) {
            rows = [...fetchedData];
          } else {
            rows = [...this.state.rows, ...fetchedData];
          }
          const newPagination = {
            ...this.state.pagination,
            currentPage: action.response.links.page_number,
            nextPage: action.response.links.next_page_number,
            page_size: this.state.pagination.page_size,
            ordering: this.state.pagination.ordering,
            search: this.state.pagination.search,
            count: action.response.count,
          };

          this.setState({
            pagination: newPagination,
            rows,
            noData: rows.length === 0 ? true : false,
            loading: false,
          });
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  getVersion = (revision: any) => {
    const version =
      typeof revision.meta_data === "object"
        ? revision.meta_data.version
        : "N.A.";

    return version ? version : "N.A.";
  };

  checker = (value) => {
    for (var i = 0; i < this.props.prohibited.length; i++) {
      if (value.indexOf(this.props.prohibited[i]) > -1) {
        return false;
      }
    }
    return true;
  };

  getValueByField = (value, field) => {
    let currentValue = this.stringifyObject(value);

    if (typeof value === "object") {
      switch (true) {
        case field.indexOf("travels") > -1:
          currentValue = this.formatTravelField(value);
          break;
        case field.indexOf("contractors") > -1:
          currentValue = this.formatContractorsField(value);
          break;
        case field.indexOf("implementation") > -1:
          currentValue = this.formatImplementationField(value);
          break;
        case field.indexOf("phases") > -1:
          currentValue = this.formatPhasesField(value);
          break;
        case field.indexOf("hourly_resources") > -1:
          currentValue = this.formatHourlyResourcesField(value);
          break;
        case field.indexOf("milestones") > -1:
          currentValue = this.milestoneFields(value);
          break;
        default:
          break;
      }
    }

    currentValue = this.filterIMGString(currentValue);
    return currentValue;
  };

  stringifyObject = (value) => {
    return typeof value === "object" ? JSON.stringify(value, null, 4) : value;
  };

  formatTravelField = (value) => {
    return `cost : ${value.cost || 0}\ndescription : ${value.description ||
      "N.A."}`;
  };

  formatContractorsField = (value) => {
    return `Name : ${value.name || "N.A."},
  Customer Cost : ${value.customer_cost || 0},
  Margin Percentage : ${value.margin_percentage || "0"}%,
  Partner cost : ${value.partner_cost}`;
  };

  formatImplementationField = (value) => {
    return Object.keys(value)
      .map((v) => `${value[v].label} :${value[v].value}`)
      .join(", ");
  };

  formatPhasesField = (value) => {
    return `${value.name || "N.A."},
  Hours : ${value.hours || 0},
  Resource : ${value.resource_name || "0"}`;
  };

  formatHourlyResourcesField = (value) => {
    return `Customer Cost :${value.customer_cost || "N.A."},
  Internal Cost :${value.internal_cost || "N.A."},
  override : ${value.override},
  Hours : ${value.hours || 0},
  Cost/hr : ${value.hourly_cost || 0},
  Rate/hr : ${value.hourly_rate || 0},
  Resource : ${value.resource_name},
  Des. : ${value.resource_description},
  Margin : ${value.margin || "0"},
  Margin % : ${value.margin_percentage || "0"}%`;
  };

  milestoneFields = (value) => {
    return `Name : ${value.name || "N.A."},
    Terms : ${value.terms || "N.A."},
  Percentage : ${value.milestone_percent || "0"}%`;
  };

  filterIMGString = (inputString) => {
    const startSubstring = "data:image/png;base64";
    const endCharacter = ")";
    if (typeof inputString === "string") {
      const startIndex = inputString.indexOf(startSubstring);
      const endIndex = inputString.indexOf(endCharacter, startIndex);

      if (startIndex !== -1 && endIndex !== -1) {
        const filteredString =
          inputString.substring(0, startIndex) +
          inputString.substring(endIndex + 1);
        return filteredString;
      }
    }

    return inputString;
  };

  toggleCollapsedState = (boardIndex) => {
    this.setState((prevState) => {
      const updatedIsCollapsed = [...prevState.isCollapsed];
      updatedIsCollapsed[boardIndex] = !updatedIsCollapsed[boardIndex];
      return { isCollapsed: updatedIsCollapsed };
    });
  };

  renderHistoryContainer = (history: any, boardIndex: number) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];

    return (
      <div
        key={boardIndex}
        className={`collapsable-section  ${
          isCollapsed
            ? "collapsable-section--collapsed"
            : "collapsable-section--not-collapsed"
        }`}
      >
        <div
          className="col-md-12 collapsable-heading"
          onClick={() => this.toggleCollapsedState(boardIndex)}
        >
          <div className="left col-md-9">
            <div className="name">
              {" "}
              <span>User: </span>
              {history.revision.user}
            </div>
            <div className="version">
              {" "}
              <span>version: </span>
              {this.getVersion(history.revision)}
            </div>
            <div className="date">
              {" "}
              <span>Updated On: </span>
              {utcToLocalInLongFormat(history.revision.date_created)}
            </div>
          </div>

          <div className="right col-md-3">
            <SquareButton
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                this.props.previewDoc(history.snapshot_data);
              }}
              content="Preview"
              title="View this SOW version "
              bsStyle={"primary"}
              className={"history-buttons"}
            />
            <SquareButton
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                this.props.checkoutSOW(history.snapshot_data);
              }}
              content="Edit"
              title="Edit this SOW version "
              bsStyle={"default"}
              className={"history-buttons"}
            />
          </div>
        </div>
        <div className="collapsable-contents">
          {history.revision &&
            _.get(history, "revision.meta_data.description") && (
              <div className="no-data">
                {" "}
                <span>Version Description : </span>
                {_.get(history, "revision.meta_data.description")}
              </div>
            )}
          {history.diff === null ||
            !history.diff ||
            (isEqual(history.diff, {}) && (
              <div className="no-data"> No diff data</div>
            ))}
          {history.diff && history.diff.values_changed && (
            <>
              <div className="heading-version">
                <span> {` Previous Version `} </span>
                <span>{` Version ${this.getVersion(history.revision)}`}</span>
              </div>
              {Object.keys(history.diff.values_changed)
                .sort((a, b) => (a !== b ? (a < b ? -1 : 1) : 0))
                .map((v, i) => {
                  // const version = history.diff.values_changed[v];

                  if (!this.checker(v)) {
                    return "";
                  }
                  return (
                    <div key={i} className="diff-view-section">
                      <span className="heading">{getFieldNames(v)}</span>
                      {/* <ReactDiffViewer
                        oldValue={`${this.getValueByField(
                          version.old_value,
                          v
                        )}`}
                        newValue={`${this.getValueByField(
                          version.new_value,
                          v
                        )}`}
                        splitView={true}
                        disableWordDiff={version.diff ? false : true}
                      /> */}
                    </div>
                  );
                })}
            </>
          )}
          {history.diff && history.diff.iterable_item_added && (
            <div className={"col-md-6 row"}>
              <div className="heading-version">
                <span> {`Added`} </span>
              </div>

              {history.diff &&
                history.diff.iterable_item_added &&
                Object.keys(history.diff.iterable_item_added).map((v, i) => {
                  const version = history.diff.iterable_item_added[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span>{" "}
                        {this.getValueByField(version, v)}
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
          {history.diff && history.diff.iterable_item_removed && (
            <div className={"col-md-6 row removed-section"}>
              <div className="heading-version">
                <span> {`Removed`} </span>
              </div>
              {history.diff &&
                history.diff.iterable_item_removed &&
                Object.keys(history.diff.iterable_item_removed).map((v, i) => {
                  const version = history.diff.iterable_item_removed[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span>{" "}
                        {this.getValueByField(version, v)}
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
        </div>
      </div>
    );
  };

  render() {
    return (
      <div
        className={`infinite-list-component history-ui ${this.props.className}`}
      >
        <div className="top-action">
          {this.state.rows.length > 0 && (
            <div className="total-count">
              <span>Total : </span>
              {this.state.pagination.count || 0}
            </div>
          )}
        </div>
        <div
          id={this.props.id}
          style={{
            position: "relative",
            height: this.props.height || "72vh",
            overflow: "auto",
          }}
        >
          <InfiniteScroll
            dataLength={this.state.rows.length}
            next={this.fetchMoreData}
            hasMore={this.state.pagination.nextPage ? true : false}
            loader={
              <div className="loader-list">
                <Spinner show={true} />
              </div>
            }
            scrollableTarget={this.props.id}
          >
            {this.state.rows.map((row: T, index: number) =>
              this.renderHistoryContainer(row, index)
            )}
            {!this.state.loading &&
              this.state.pagination.nextPage &&
              this.state.rows.length > 0 && (
                <div
                  className="load-more"
                  onClick={() => {
                    this.fetchMoreData(false);
                  }}
                >
                  Load more data
                </div>
              )}
          </InfiniteScroll>
          {this.state.noData && this.state.rows.length === 0 && (
            <div className="no-data-inf">No data available</div>
          )}

          <BlinkingLoader show={this.state.loading} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchInfiniteList: (url: string, params?: IScrollPaginationFilters) =>
    dispatch(fetchInfiniteList(url, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(InfiniteHistory);
