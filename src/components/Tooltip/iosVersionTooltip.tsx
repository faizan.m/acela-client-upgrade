import React from 'react';
 import './style.scss';

const TooltipCustom: React.FC<any> = (props) => {

  const tooltip = (
    <div className="in show" id="tooltip-right">
      {props.children || 
      <div className="tooltip-data-rules">
      <div>  IOS Version Filtering</div>
      <div>  Lets you filter configurations based on version and applies the rule. Accepts multiple version comma separated </div>
      <div>  We can use following symbols for setting the version filtering
              <br/> &nbsp; &nbsp;● {'>'}= | greater than equal to - transaltes to newer or given version number
              <br/> &nbsp; &nbsp;● {`<=`} | less than equal to - transaltes to given version or lesser than it
              <br/> &nbsp; &nbsp;● {'>'} | greater than - transaltes to newer versions after the given input version.
              <br/> &nbsp; &nbsp;{`● < |`} less than - transaltes to versions lower than the given input version.
        </div>  
      <br/>
      <div>  Examples:
          <br/> &nbsp; &nbsp;1. Catalyst 9300 switches:  {'>'}=16.12.4, 16.9.6
          <br/> &nbsp; &nbsp;2. Catalyst 9300 switches: 12.09.1
          <br/> &nbsp; &nbsp;3. Nexus Switches: {'>'}7.3(6), {`<=`}7.0(5)
          <br/> &nbsp; &nbsp;4. Nexus Switches: {`<`}2.0.9
          <br/> &nbsp; &nbsp;5. 3850 switches: {`<=`}03.06.05, {'>'}=03.06.05bE
      </div>
      <br/>
      <div>  Explanation:
          <br/> &nbsp; &nbsp;1. In the first e.g. the versions specified are for Cisco switches, the rule will filter down configurations whose versions are
          <br/> &nbsp; &nbsp; (i) newer than or equal to 16.12.4 or
          <br/> &nbsp; &nbsp; (ii) equal to 16.9.6
          <br/> &nbsp; &nbsp;
          <br/> &nbsp; &nbsp;2. In the second e.g. the versions specified are for Cisco switches, the rule will apply exactly to configurations having
          <br/> &nbsp; &nbsp; (i) versions equal to 12.09.1
          <br/> &nbsp; &nbsp;
          <br/> &nbsp; &nbsp;3. For this e.g. Nexus Switches: {'>'}= 7.3(6), {`<=`}7.0(5). rule is applicable on configurations having version
          <br/> &nbsp; &nbsp; (i) greater than or equal to 7.3(6)
          <br/> &nbsp; &nbsp; (ii) lower than or equal to 7.0(5)
          <br/> &nbsp; &nbsp;
          <br/> &nbsp; &nbsp;4. For this e.g. Nexus Switches: {`<`}2.0.9. rule is applicable on configurations having version
          <br/> &nbsp; &nbsp; (ii) lower than 2.0.9
          <br/> &nbsp; &nbsp;
          <br/> &nbsp; &nbsp;5. In the last e.g. 3850 switches: {`<=`}03.06.05, {'>'}03.06.05bE the rule will apply to configurations having the following versions 
          <br/> &nbsp; &nbsp; (i) versions older than or equal to 03.06.05
          <br/> &nbsp; &nbsp; (ii) versions newer than 03.06.05bE
      </div>
    </div>}
    </div>
  );

  return tooltip;
};

export default TooltipCustom;
