import React from 'react';
import './style.scss';

const TooltipCustom: React.FC<any> = (props) => {

  const tooltip = (
    <>
      {props.children || 
      <div className="tooltip-data-rules">
      <div>  Creates an exception in a rule</div>
      <div>  Please enter valid exceptions with variable value replaced accordingly.</div>
      <div>  Example:
              <br/> &nbsp; &nbsp;Rule: logging host * 
              <br/> &nbsp; &nbsp;Exceptions: logging host 10.10.10.10
              <br/> &nbsp; &nbsp;Fail if found is checked
        </div>  
      <div>  Explanation for the above rule validation:
          <br/> &nbsp; &nbsp;According to the above configured rule, it is valid only if 
          <br/> &nbsp; &nbsp;there was no line with logging host found in the configuration
          <br/> &nbsp; &nbsp;But because of the exception set, it'll check if any of the lines found
          <br/> &nbsp; &nbsp;from the config were in the present in the listed exception, 
          <br/> &nbsp; &nbsp;if yes, then rule fails.</div>
    </div>}
    </>
  );


  return tooltip;
};

export default TooltipCustom;
