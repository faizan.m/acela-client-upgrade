import React from "react";
import "./style.scss";

interface BlinkingLoaderProps {
  show: boolean;
}

const BlinkingLoader: React.FC<BlinkingLoaderProps> = ({ show }) => {
  return show ? (
    <div className="center-container">
      <div className="loading-dots">
        <span className="loading-dot"></span>
        <span className="loading-dot"></span>
        <span className="loading-dot"></span>
      </div>
    </div>
  ) : null;
};

export default BlinkingLoader;
