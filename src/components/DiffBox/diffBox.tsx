import React from "react";
// import ReactDiffViewer, { ReactDiffViewerProps } from "react-diff-viewer"; (New Component)
import "./diffBox.scss";

interface DiffBoxProps {
  heading?: string;
  customWrapper?: string;
  // diffViewerProps: ReactDiffViewerProps;
  diffViewerProps: any;
}

const DiffBox: React.FC<DiffBoxProps> = ({
  heading,
  customWrapper,
  diffViewerProps,
}) => {
  return (
    <div
      className={
        "diff-view-section" + (customWrapper ? " " + customWrapper : "")
      }
    >
      {heading && <span className="heading">{heading}</span>}
      {/* <ReactDiffViewer {...diffViewerProps} /> */}
    </div>
  );
};

export default DiffBox;
