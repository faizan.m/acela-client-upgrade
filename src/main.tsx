import React from "react";
import ReactDOM from "react-dom/client";
import App from "./app";
// import Test from "./test";

// ReactDOM.createRoot(document.getElementById("root")!).render(<Test />);
ReactDOM.createRoot(document.getElementById("root")!).render(<App />);
