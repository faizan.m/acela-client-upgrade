// import { createBrowserHistory } from "history";
import { configureStore } from "@reduxjs/toolkit";
// import { applyMiddleware, compose, createStore as _createStore } from "redux";
import { reducers as combinedReducers } from "../reducers";

import ApiMiddleware from "../middleware/ApiMiddleware";
import {
  LoginMiddleware,
  LogOutMiddleware,
} from "../middleware/AuthMiddleware";

// export const history = createBrowserHistory();

const middleware = [LoginMiddleware, LogOutMiddleware, ApiMiddleware];

const store = configureStore({
  reducer: combinedReducers,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(middleware),
});

export default store;
