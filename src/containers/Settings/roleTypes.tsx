import React, { Component } from "react";
import { connect } from "react-redux";
import "./style.scss";
import Input from "../../components/Input/input";
import cloneDeep from "lodash/cloneDeep";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import {
  createRoleType,
  deleteRoleType,
  getRoleTypes,
  ROLE_TYPE_FAILURE,
  ROLE_TYPE_SUCCESS,
} from "../../actions/setting";
import { commonFunctions } from "../../utils/commonFunctions";
import SquareButton from "../../components/Button/button";

interface IIRoleTypesState {
  role: string;
  roleList: any[];
  error: {
    role: IFieldValidation;
  };
}

interface IIRoleTypesProps extends ICommonProps {
  getRoleTypes?: any;
  createRoleType?: any;
  deleteRoleType?: any;
  callbackFn: any;
}

interface IPassedProps {
  callbackFn?: any;
}

class RoleTypes extends Component<IIRoleTypesProps, IIRoleTypesState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    role: "",
    error: {
      role: { ...RoleTypes.emptyErrorState },
    },
    roleList: [],
  });

  componentDidMount() {
    this.getRoleTypes();
  }

  getRoleTypes = async () => {
    const mapping: any = await this.props.getRoleTypes();
    this.setState({ roleList: mapping.response });
  };

  onNewOptionClick = (e) => {
    const name = this.state.role;
    const newState = cloneDeep(this.state);
    const allTitles = newState.roleList && newState.roleList.map((p) => p.role);
    if (!allTitles.includes(name)) {
      this.props.createRoleType({ role: name }).then((action) => {
        if (action.type === ROLE_TYPE_SUCCESS) {
          newState.roleList.push({ role: name, id: action.response.id });
          (newState.role as string) = "";
          (newState.error.role.errorMessage as string) = "";
          (newState.error.role.errorState as any) = "success";
          this.setState(newState);
          this.props.callbackFn();
        }
        if (action.type === ROLE_TYPE_FAILURE) {
          this.setValidationErrors(action.errorList.data);
        }
      });
    }
  };

  deleteRoleType = (data, index) => {
    const newState = cloneDeep(this.state);
    this.props.deleteRoleType({ id: data.id }).then((action) => {
      if (action.type === ROLE_TYPE_SUCCESS) {
        newState.roleList.splice(index, 1);
        this.props.callbackFn();
      }
      this.setState(newState);
    });
  };
  setValidationErrors = (errorList) => {
    const newState: IIRoleTypesState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };
  render() {
    return (
      <div className="role-types">
        <div className="add-role-type">
          <Input
            field={{
              label: "",
              type: "TEXT",
              value: this.state.role && this.state.role,
              isRequired: false,
            }}
            width={4}
            multi={true}
            name="role"
            onChange={(e) =>
              this.setState({
                role: e.target.value,
              })
            }
            placeholder={`Enter Type `}
            error={this.state.error.role}
            handleKeyDown={(e) => {
              if (e.key === "Enter") {
                this.onNewOptionClick(e);
              }
            }}
          />
          {this.state.role && this.state.role.trim() && (
            <SquareButton
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Add</span>
                </>
              }
              className="add-btn"
              bsStyle={"link"}
              onClick={(e) => this.onNewOptionClick(e)}
            />
          )}
          {this.state.role && this.state.role.trim() && (
            <SquareButton
              content="Cancel"
              bsStyle={"default"}
              onClick={() => {
                this.setState({ role: "" });
              }}
            />
          )}
        </div>
        <div className="roles-all">
          <div className="role-row">
            {this.state.roleList.length === 0 && (
              <div className="type no-data">No Role Type available</div>
            )}
            {this.state.roleList &&
              this.state.roleList.map((contact, i) => {
                return (
                  <div className="type" key={i}>
                    {contact.role}
                    <SmallConfirmationBox
                      className="remove"
                      onClickOk={() => this.deleteRoleType(contact, i)}
                      text={"Role Type and associated Mapping"}
                    />
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore, ownProps: IPassedProps) => ({
  callbackFn: ownProps.callbackFn,
});

const mapDispatchToProps = (dispatch: any) => ({
  getRoleTypes: () => dispatch(getRoleTypes()),
  createRoleType: (data: any) => dispatch(createRoleType(data)),
  deleteRoleType: (data: any) => dispatch(deleteRoleType(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RoleTypes);
