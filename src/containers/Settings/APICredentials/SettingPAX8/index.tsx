import { cloneDeep } from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  fetchVendorList,
  getPAX8APICredentials,
  GET_VENDOR_LIST_SUCCESS,
  savePAX8APICredentials,
  testPAX8APICredentials,
  TEST_PAX8_CRED_FAILURE,
  TEST_PAX8_CRED_SUCCESS,
} from "../../../../actions/setting";
import SquareButton from "../../../../components/Button/button";
import Input from "../../../../components/Input/input";
import Spinner from "../../../../components/Spinner";
import {
  encryptTextUsingRSA,
  getDummyEncryptedText,
} from "../../../../utils/CommonUtils";

interface IPAX8APIState {
  client_id: string;
  client_secret: string;
  vendors: string[];
  prev_client_id: string;
  error: {
    client_id: IFieldValidation;
    client_secret: IFieldValidation;
  };
  showNotification: boolean;
  isVerificationSuccess: boolean;
  isEdit: boolean;
  loading: boolean;
  newSettings: boolean;
  vendorsList: any[];
}

interface IPAX8APIProps extends ICommonProps {
  publicKey: string;
  isFetching: boolean;
  pax8Credentials: IPAX8Credentials;
  settings_error: boolean;
  addErrorMessage: TShowErrorMessage;
  fetchVendorList: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  getPAX8APICredentials: () => Promise<any>;
  testPAX8APICredentials: (data: IPAX8Credentials) => Promise<any>;
  savePAX8APICredentials: (
    data: IPAX8Credentials,
    method: "put" | "post"
  ) => Promise<any>;
}

class PAX8Credential extends Component<IPAX8APIProps, IPAX8APIState> {
  static emptyState: IPAX8APIState = {
    client_id: "",
    client_secret: "",
    vendors: [],
    newSettings: false,
    error: {
      client_id: {
        errorState: "success",
        errorMessage: "",
      },
      client_secret: {
        errorState: "success",
        errorMessage: "",
      },
    },
    showNotification: false,
    isVerificationSuccess: false,
    isEdit: false,
    loading: true,
    vendorsList: [],
    prev_client_id: "",
  };

  constructor(props: any) {
    super(props);
    this.state = cloneDeep(PAX8Credential.emptyState);
  }
  componentDidMount() {
    this.props.getPAX8APICredentials();
    this.fetchVendorList();
  }

  componentDidUpdate(prevProps: IPAX8APIProps) {
    if (prevProps.pax8Credentials !== this.props.pax8Credentials) {
      this.setState({
        loading: false,
        client_id: this.props.pax8Credentials.client_id,
        client_secret: getDummyEncryptedText(),
        vendors: this.props.pax8Credentials.vendors,
        prev_client_id: this.props.pax8Credentials.client_id,
      });
    }
    if (this.props.settings_error !== prevProps.settings_error) {
      this.setState({ loading: false, newSettings: true });
    }
  }

  fetchVendorList = () => {
    this.props.fetchVendorList().then((action) => {
      if (action.type === GET_VENDOR_LIST_SUCCESS && action.response) {
        const vendorsList = action.response.map((c) => ({
          value: c.vendor_name,
          label: c.vendor_name,
          disabled: false,
        }));
        this.setState({ vendorsList });
      }
    });
  };

  renderNotification = () => {
    const { showNotification, isVerificationSuccess } = this.state;
    const onNotificationClick = () =>
      this.setState({
        showNotification: false,
      });

    return (
      showNotification && (
        <div
          onClick={onNotificationClick}
          className={`settings__notification ${
            isVerificationSuccess
              ? "settings__notification--success"
              : "settings__notification--fail"
          }`}
        >
          {isVerificationSuccess ? "Test success. Please Save." : "Test Failed"}
        </div>
      )
    );
  };

  isValid = () => {
    const error = cloneDeep(PAX8Credential.emptyState.error);
    let isValid = true;

    if (!this.state.client_id || this.state.client_id.trim() === "") {
      error.client_id.errorState = "error";
      error.client_id.errorMessage = "Enter valid client Id";
      isValid = false;
    }

    if (!this.state.client_secret || this.state.client_secret.trim() === "") {
      error.client_secret.errorState = "error";
      error.client_secret.errorMessage = "Enter valid client Secret";
      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  onTestAdd = () => {
    if (!this.isValid()) {
      return null;
    }
    this.setState({
      showNotification: false,
      isVerificationSuccess: false,
      loading: true,
    });
    const payload = {
      client_id: this.state.client_id,
      client_secret: encryptTextUsingRSA(
        this.state.client_secret,
        this.props.publicKey
      ),
    };
    this.props
      .testPAX8APICredentials(payload as IPAX8Credentials)
      .then((action) => {
        if (action.type === TEST_PAX8_CRED_SUCCESS) {
          const verified = action.response.is_valid;

          this.setState({
            showNotification: true,
            isVerificationSuccess: verified,
            isEdit: !verified,
          });
        }
        this.setState({ loading: false });
      });
  };

  savePAX8Setting = () => {
    if (this.isValid()) {
      this.setState({
        showNotification: false,
        isVerificationSuccess: false,
        loading: true,
      });
      const payload = {
        client_id: this.state.client_id,
        client_secret: encryptTextUsingRSA(
          this.state.client_secret,
          this.props.publicKey
        ),
        vendors: this.state.vendors,
      };
      this.props
        .savePAX8APICredentials(
          payload,
          this.state.newSettings ? "post" : "put"
        )
        .then((action) => {
          if (action.type === TEST_PAX8_CRED_SUCCESS) {
            this.props.addSuccessMessage(
              `PAX8 Credentials ${
                this.state.newSettings ? "Saved" : "Updated"
              } Successfully!`
            );
            this.setState({
              isEdit: false,
              newSettings: false,
              prev_client_id: this.state.client_id,
            });
          } else if (action.type === TEST_PAX8_CRED_FAILURE) {
            this.props.addErrorMessage("Error saving PAX8 Credentials");
          }
        })
        .finally(() => this.setState({ loading: false }));
    }
  };

  setEditStateAdd = () => {
    this.setState({
      showNotification: false,
      isEdit: true,
      isVerificationSuccess: false,
      client_secret: "",
    });
  };

  onCancelEdit = () => {
    this.setState({
      isEdit: false,
      client_id: this.state.prev_client_id,
      client_secret: getDummyEncryptedText(),
    });
  };

  render() {
    return (
      <div className="api-credentials">
        <h3>PAX8 API Credentials</h3>
        {this.props.isFetching ||
          (this.state.loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          ))}
        {this.renderNotification()}
        <div className="pax8-api-credentials-form">
          <Input
            field={{
              label: "Client Id",
              type: "TEXT",
              value: this.state.client_id,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Client Id"
            name="client_id"
            className="client_id-select"
            error={this.state.error.client_id}
            onChange={(e) => this.setState({ client_id: e.target.value })}
            disabled={!this.state.isEdit}
          />
          <Input
            field={{
              label: "Client Secret",
              type: "PASSWORD",
              value: this.state.client_secret,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Client Secret"
            name="client_secret"
            className="client_secret-select"
            error={this.state.error.client_secret}
            onChange={(e) => this.setState({ client_secret: e.target.value })}
            disabled={!this.state.isEdit}
          />
          <Input
            field={{
              label: "Vendor",
              type: "PICKLIST",
              value: this.state.vendors,
              options: this.state.vendorsList,
              isRequired: false,
            }}
            width={6}
            multi={true}
            name="vendors"
            onChange={(e) => this.setState({ vendors: e.target.value })}
            placeholder={`Select`}
          />
        </div>
        <div className="credentials-body-actions">
          {!this.state.isVerificationSuccess && this.state.isEdit && (
            <SquareButton
              onClick={this.onTestAdd}
              content="Test"
              bsStyle={"primary"}
            />
          )}
          {!this.state.isEdit && this.state.isVerificationSuccess && (
            <SquareButton
              onClick={this.savePAX8Setting}
              content={this.state.newSettings ? "Save" : "Update"}
              bsStyle={"primary"}
            />
          )}
          {!this.state.isEdit && (
            <SquareButton
              content="Edit"
              onClick={this.setEditStateAdd}
              bsStyle={"default"}
            />
          )}
          {this.state.isEdit && (
            <SquareButton
              content="Cancel"
              onClick={this.onCancelEdit}
              bsStyle={"default"}
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  publicKey: state.setting.publicKey,
  isFetching: state.setting.isFetching,
  pax8Credentials: state.setting.pax8Credentials,
  settings_error: state.setting.error,
});

const mapDispatchToProps = (dispatch: any) => ({
  testPAX8APICredentials: (data: IPAX8Credentials) =>
    dispatch(testPAX8APICredentials(data)),
  savePAX8APICredentials: (data: IPAX8Credentials, method: "put" | "post") =>
    dispatch(savePAX8APICredentials(data, method)),
  getPAX8APICredentials: () => dispatch(getPAX8APICredentials()),
  fetchVendorList: () => dispatch(fetchVendorList()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PAX8Credential);
