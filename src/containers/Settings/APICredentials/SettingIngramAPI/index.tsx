import { cloneDeep } from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import { addSuccessMessage } from "../../../../actions/appState";
import {
  getIngramAPICredentials,
  saveIngramAPICredentials,
  testIngramAPICredentials,
  TEST_INGRAM_CRED_SUCCESS,
} from "../../../../actions/setting";
import SquareButton from "../../../../components/Button/button";
import Input from "../../../../components/Input/input";
import Spinner from "../../../../components/Spinner";
import {
  encryptTextUsingRSA,
  getDummyEncryptedText,
} from "../../../../utils/CommonUtils";

interface IIngramAPIState {
  client_id: string;
  client_secret: string;
  ingram_customer_id: string;
  prev_client_id: string;
  error: {
    client_id: IFieldValidation;
    client_secret: IFieldValidation;
    ingram_customer_id: IFieldValidation;
  };
  showNotification: boolean;
  isVerificationSuccess: boolean;
  isEdit: boolean;
  loading: boolean;
}

interface IIngramAPIProps extends ICommonProps {
  publicKey: string;
  isFetching: boolean;
  ingramCredentials: IIngramCredentials;
  addSuccessMessage: TShowSuccessMessage;
  getIngramAPICredentials: () => Promise<any>;
  testIngramAPICredentials: (data: IIngramCredentials) => Promise<any>;
  saveIngramAPICredentials: (data: IIngramCredentials) => Promise<any>;
}

class IngramCredential extends Component<IIngramAPIProps, IIngramAPIState> {
  static emptyState: IIngramAPIState = {
    client_id: "",
    client_secret: "",
    ingram_customer_id: "",
    error: {
      client_id: {
        errorState: "success",
        errorMessage: "",
      },
      client_secret: {
        errorState: "success",
        errorMessage: "",
      },
      ingram_customer_id: {
        errorState: "success",
        errorMessage: "",
      },
    },
    showNotification: false,
    isVerificationSuccess: false,
    isEdit: false,
    loading: false,
    prev_client_id: "",
  };

  constructor(props: any) {
    super(props);
    this.state = cloneDeep(IngramCredential.emptyState);
  }
  componentDidMount() {
    this.props.getIngramAPICredentials();
  }

  componentDidUpdate(prevProps: IIngramAPIProps) {
    if (
      this.props.ingramCredentials &&
      prevProps.ingramCredentials !== this.props.ingramCredentials
    ) {
      this.setState({
        client_id: this.props.ingramCredentials.client_id,
        prev_client_id: this.props.ingramCredentials.client_id,
        client_secret: getDummyEncryptedText(),
        ingram_customer_id: this.props.ingramCredentials.ingram_customer_id,
      });
    }
  }

  renderNotification = () => {
    const { showNotification, isVerificationSuccess } = this.state;
    const onNotificationClick = () =>
      this.setState({
        showNotification: false,
      });

    return (
      showNotification && (
        <div
          onClick={onNotificationClick}
          className={`settings__notification ${
            isVerificationSuccess
              ? "settings__notification--success"
              : "settings__notification--fail"
          }`}
        >
          {isVerificationSuccess ? "Test success. Please Save." : "Test Failed"}
        </div>
      )
    );
  };

  isValid = () => {
    const error = cloneDeep(IngramCredential.emptyState.error);
    let isValid = true;

    if (!this.state.client_id || this.state.client_id.trim() === "") {
      error.client_id.errorState = "error";
      error.client_id.errorMessage = "Enter valid client Id";
      isValid = false;
    }

    if (!this.state.client_secret || this.state.client_secret.trim() === "") {
      error.client_secret.errorState = "error";
      error.client_secret.errorMessage = "Enter valid client Secret";
      isValid = false;
    }

    if (
      !this.state.ingram_customer_id ||
      this.state.ingram_customer_id.trim() === ""
    ) {
      error.ingram_customer_id.errorState = "error";
      error.ingram_customer_id.errorMessage = "Enter valid Customer Number";
      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  onTestAdd = () => {
    if (!this.isValid()) {
      return null;
    }
    this.setState({
      showNotification: false,
      isVerificationSuccess: false,
      loading: true,
    });
    const payload = {
      client_id: this.state.client_id,
      client_secret: encryptTextUsingRSA(
        this.state.client_secret,
        this.props.publicKey
      ),
      ingram_customer_id: this.state.ingram_customer_id,
      customer_number: this.state.ingram_customer_id,
    };
    this.props.testIngramAPICredentials(payload).then((action) => {
      if (action.type === TEST_INGRAM_CRED_SUCCESS) {
        const verified = action.response.is_valid;

        this.setState({
          showNotification: true,
          isVerificationSuccess: verified,
          isEdit: !verified,
        });
        if (verified) {
        }
      }
      this.setState({ loading: false });
    });
  };

  saveIngramSetting = () => {
    if (this.isValid()) {
      this.setState({
        showNotification: false,
        isVerificationSuccess: false,
        loading: true,
      });
      const payload = {
        client_id: this.state.client_id,
        client_secret: encryptTextUsingRSA(
          this.state.client_secret,
          this.props.publicKey
        ),
        ingram_customer_id: this.state.ingram_customer_id,
      };
      this.props.saveIngramAPICredentials(payload).then((action) => {
        if (action.type === TEST_INGRAM_CRED_SUCCESS) {
          this.props.addSuccessMessage("Credentials Saved.");
          this.setState({
            showNotification: false,
            isVerificationSuccess: true,
            isEdit: false,
            prev_client_id: this.state.client_id,
          });
        }
        this.setState({ loading: false });
      });
    }
  };

  setEditStateAdd = () => {
    this.setState({
      showNotification: false,
      isEdit: true,
      isVerificationSuccess: false,
      client_secret: "",
    });
  };

  onCancelEdit = () => {
    this.setState({
      isEdit: false,
      client_id: this.state.prev_client_id,
      client_secret: getDummyEncryptedText(),
    });
  };

  render() {
    return (
      <div className="api-credentials">
        <h3>Ingram API Credentials </h3>
        {this.props.isFetching ||
          (this.state.loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          ))}
        {this.renderNotification()}
        <div className="ingram-api-credentials-form">
          <Input
            field={{
              label: "Client Id",
              type: "TEXT",
              value: this.state.client_id,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Client Id"
            name="client_id"
            className="client_id-select"
            error={this.state.error.client_id}
            onChange={(e) => this.setState({ client_id: e.target.value })}
            disabled={!this.state.isEdit}
          />
          <Input
            field={{
              label: "Client Secret",
              type: "PASSWORD",
              value: this.state.client_secret,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Client Secret"
            name="client_secret"
            className="client_secret-select"
            error={this.state.error.client_secret}
            onChange={(e) => this.setState({ client_secret: e.target.value })}
            disabled={!this.state.isEdit}
          />
          <Input
            field={{
              label: " Customer Number",
              type: "TEXT",
              value: this.state.ingram_customer_id,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Customer Number"
            name="ingram_customer_id"
            className="ingram_customer_id-select"
            onChange={(e) =>
              this.setState({ ingram_customer_id: e.target.value })
            }
            error={this.state.error.ingram_customer_id}
            disabled={!this.state.isEdit}
          />
        </div>
        <div className="credentials-body-actions">
          {!this.state.isVerificationSuccess && this.state.isEdit && (
            <SquareButton
              onClick={this.onTestAdd}
              content="Test"
              bsStyle={"primary"}
            />
          )}
          {!this.state.isEdit && this.state.isVerificationSuccess && (
            <SquareButton
              onClick={this.saveIngramSetting}
              content="Save"
              bsStyle={"primary"}
            />
          )}
          {!this.state.isEdit && (
            <SquareButton
              content="Edit"
              onClick={this.setEditStateAdd}
              bsStyle={"default"}
            />
          )}
          {this.state.isEdit && (
            <SquareButton
              content="Cancel"
              onClick={this.onCancelEdit}
              bsStyle={"default"}
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.setting.isFetching,
  publicKey: state.setting.publicKey,
  ingramCredentials: state.setting.ingramCredentials,
});

const mapDispatchToProps = (dispatch: any) => ({
  testIngramAPICredentials: (data: IIngramCredentials) =>
    dispatch(testIngramAPICredentials(data)),
  saveIngramAPICredentials: (data: IIngramCredentials) =>
    dispatch(saveIngramAPICredentials(data)),
  getIngramAPICredentials: () => dispatch(getIngramAPICredentials()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IngramCredential);
