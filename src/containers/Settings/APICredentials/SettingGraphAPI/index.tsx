import { cloneDeep } from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addErrorMessage,
  addSuccessMessage,
  addWarningMessage,
} from "../../../../actions/appState";
import {
  graphAPICredCRU,
  testGraphAPICredentials,
  GRAPH_CRED_SUCCESS,
  GRAPH_CRED_FAILURE,
  TEST_GRAPH_CRED_SUCCESS,
} from "../../../../actions/setting";
import SquareButton from "../../../../components/Button/button";
import Input from "../../../../components/Input/input";
import Spinner from "../../../../components/Spinner";
import {
  encryptTextUsingRSA,
  getDummyEncryptedText,
} from "../../../../utils/CommonUtils";

interface IGraphAPIState {
  isEdit: boolean;
  loading: boolean;
  prev_client_id: string;
  client_id: string;
  newSettings: boolean;
  client_secret: string;
  showNotification: boolean;
  isVerificationSuccess: boolean;
  error: {
    client_id: IFieldValidation;
    client_secret: IFieldValidation;
  };
}

interface IGraphAPIProps extends ICommonProps {
  publicKey: string;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  addWarningMessage: TShowWarningMessage;
  testGraphAPICredentials: (data: IGraphCredentials) => Promise<any>;
  graphAPICredCRU: (
    method: HTTPMethods,
    data?: IGraphCredentials
  ) => Promise<any>;
}

class GraphCredential extends Component<IGraphAPIProps, IGraphAPIState> {
  static emptyState: IGraphAPIState = {
    isEdit: false,
    client_id: "",
    loading: true,
    client_secret: "",
    prev_client_id: "",
    newSettings: false,
    showNotification: false,
    isVerificationSuccess: false,
    error: {
      client_id: {
        errorState: "success",
        errorMessage: "",
      },
      client_secret: {
        errorState: "success",
        errorMessage: "",
      },
    },
  };

  constructor(props: any) {
    super(props);
    this.state = cloneDeep(GraphCredential.emptyState);
  }

  componentDidMount() {
    this.fetchGraphAPICred();
  }

  fetchGraphAPICred = () => {
    this.setState({ loading: true });
    this.props
      .graphAPICredCRU("get")
      .then((action) => {
        if (action.type === GRAPH_CRED_SUCCESS) {
          this.setState({
            client_id: action.response.client_id,
            client_secret: getDummyEncryptedText(),
            prev_client_id: action.response.client_id,
          });
        } else if (action.type === GRAPH_CRED_FAILURE) {
          this.props.addWarningMessage(
            "Graph API Credentials are not configured!"
          );
          this.setState({ newSettings: true });
        }
      })
      .finally(() => this.setState({ loading: false }));
  };

  renderNotification = () => {
    const { showNotification, isVerificationSuccess } = this.state;
    const onNotificationClick = () =>
      this.setState({
        showNotification: false,
      });

    return (
      showNotification && (
        <div
          onClick={onNotificationClick}
          className={`settings__notification ${
            isVerificationSuccess
              ? "settings__notification--success"
              : "settings__notification--fail"
          }`}
        >
          {isVerificationSuccess ? "Test success. Please Save." : "Test Failed"}
        </div>
      )
    );
  };

  isValid = () => {
    const error = cloneDeep(GraphCredential.emptyState.error);
    let isValid = true;

    if (!this.state.client_id || this.state.client_id.trim() === "") {
      error.client_id.errorState = "error";
      error.client_id.errorMessage = "Enter valid client Id";
      isValid = false;
    }

    if (!this.state.client_secret || this.state.client_secret.trim() === "") {
      error.client_secret.errorState = "error";
      error.client_secret.errorMessage = "Enter valid client Secret";
      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  onTestAdd = () => {
    if (!this.isValid()) {
      return null;
    }
    this.setState({
      showNotification: false,
      isVerificationSuccess: false,
      loading: true,
    });
    const payload = {
      client_id: this.state.client_id,
      client_secret: encryptTextUsingRSA(
        this.state.client_secret,
        this.props.publicKey
      ),
    };
    this.props
      .testGraphAPICredentials(payload as IGraphCredentials)
      .then((action) => {
        if (action.type === TEST_GRAPH_CRED_SUCCESS) {
          const verified = action.response.is_valid;

          this.setState({
            showNotification: true,
            isVerificationSuccess: verified,
            isEdit: !verified,
          });
        }
      })
      .finally(() => this.setState({ loading: false }));
  };

  saveGraphAPISetting = () => {
    if (this.isValid()) {
      this.setState({
        showNotification: false,
        isVerificationSuccess: false,
        loading: true,
      });
      const payload = {
        client_id: this.state.client_id,
        client_secret: encryptTextUsingRSA(
          this.state.client_secret,
          this.props.publicKey
        ),
      };
      this.props
        .graphAPICredCRU(
          this.state.newSettings ? "post" : "put",
          payload
        )
        .then((action) => {
          if (action.type === GRAPH_CRED_SUCCESS) {
            this.props.addSuccessMessage(
              `Graph API Credentials ${
                this.state.newSettings ? "Saved" : "Updated"
              } Successfully!`
            );
            this.setState({
              isEdit: false,
              newSettings: false,
              prev_client_id: this.state.client_id,
            });
          } else if (action.type === GRAPH_CRED_FAILURE) {
            this.props.addErrorMessage("Error saving Graph API Credentials!");
          }
        })
        .finally(() => this.setState({ loading: false }));
    }
  };

  setEditStateAdd = () => {
    this.setState({
      showNotification: false,
      isEdit: true,
      isVerificationSuccess: false,
      client_secret: "",
    });
  };

  onCancelEdit = () => {
    this.setState({
      isEdit: false,
      client_id: this.state.prev_client_id,
      client_secret: getDummyEncryptedText(),
    });
  };

  render() {
    return (
      <div className="api-credentials">
        <h3>Microsoft Graph API Credentials</h3>
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {this.renderNotification()}
        <div className="graph-api-credentials-form">
          <Input
            field={{
              label: "Client Id",
              type: "TEXT",
              value: this.state.client_id,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Client Id"
            name="client_id"
            className="client_id-select"
            error={this.state.error.client_id}
            onChange={(e) => this.setState({ client_id: e.target.value })}
            disabled={!this.state.isEdit}
          />
          <Input
            field={{
              label: "Client Secret",
              type: "PASSWORD",
              value: this.state.client_secret,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Client Secret"
            name="client_secret"
            className="client_secret-select"
            error={this.state.error.client_secret}
            onChange={(e) => this.setState({ client_secret: e.target.value })}
            disabled={!this.state.isEdit}
          />
        </div>
        <div className="credentials-body-actions">
          {!this.state.isVerificationSuccess && this.state.isEdit && (
            <SquareButton
              onClick={this.onTestAdd}
              content="Test"
              bsStyle={"primary"}
            />
          )}
          {!this.state.isEdit && this.state.isVerificationSuccess && (
            <SquareButton
              onClick={this.saveGraphAPISetting}
              content={this.state.newSettings ? "Save" : "Update"}
              bsStyle={"primary"}
            />
          )}
          {!this.state.isEdit && (
            <SquareButton
              content="Edit"
              onClick={this.setEditStateAdd}
              bsStyle={"default"}
            />
          )}
          {this.state.isEdit && (
            <SquareButton
              content="Cancel"
              onClick={this.onCancelEdit}
              bsStyle={"default"}
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  publicKey: state.setting.publicKey,
});

const mapDispatchToProps = (dispatch: any) => ({
  testGraphAPICredentials: (data: IGraphCredentials) =>
    dispatch(testGraphAPICredentials(data)),
  graphAPICredCRU: (method: HTTPMethods, data?: IGraphCredentials) =>
    dispatch(graphAPICredCRU(method, data)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addWarningMessage: (message: string) => dispatch(addWarningMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GraphCredential);
