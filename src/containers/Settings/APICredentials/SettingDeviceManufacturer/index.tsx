import _cloneDeep from "lodash/cloneDeep";
import React, { Component } from "react";
import { connect } from "react-redux";
import { ERROR, SHOW_SUCCESS_MESSAGE } from "../../../../actions/appState";
import {
  FETCH_INTEGRATIONS_CAT_SUCCESS,
  fetchIntegrationsCategories,
  fetchIntegrationsCategoriesTypes,
  TEST_INTEGRATION_SUCCESS,
  testIntegration,
} from "../../../../actions/integration";
import { fetchManufacturers } from "../../../../actions/inventory";
import {
  CREATE_PROVIDER_INTEGRATIONS_FAILURE,
  CREATE_PROVIDER_INTEGRATIONS_SUCCESS,
  createProviderIntegrations,
  editProviderIntegrations,
  fetchAllProviderIntegrationsWithFilter,
  fetchProviderConfigStatus,
  updateProviderIntegration,
} from "../../../../actions/provider/integration";
import SquareButton from "../../../../components/Button/button";
import { fetchEncryptionPublicKey } from "../../../../actions/setting";
import EditButton from "../../../../components/Button/editButton";
import Input from "../../../../components/Input/input";
import SelectInput from "../../../../components/Input/Select/select";
import ModalBase from "../../../../components/ModalBase/modalBase";
import Spinner from "../../../../components/Spinner";
import Table from "../../../../components/Table/table";
import store from "../../../../store";
import { encryptTextUsingRSA } from "../../../../utils/CommonUtils";
import "./style.scss";

interface IDeviceSettingState {
  isFormValid: boolean;
  error: {
    [fieldName: string]: IFieldValidation;
  };
  isFetching: boolean;
  errorList?: any;
  deviceInfoId?: number;
  integrations: any;
  providerIntegration: IProviderIntegration;
  editedItemIndex: any;
  isEdit: boolean;
  isEditPopUp: boolean;
  showNotification: boolean;
  integrationVerified: boolean;
  integrationsCatgories?: any;
  isVerificationSuccess: boolean;
  allProviderIntegrations: any;
  selectedIntegrationFields: any;
  selectedIntegration: any;
  openAddAccount: boolean;
  connectwiseManufacturer: any;
  rows: any;
  CRMId?: number;
}

interface IDeviceSettingProps extends ICommonProps {
  publicKey: string;
  fetchPublicKey: () => Promise<any>;
  fetchIntegrationsCategories: TFetchIntegrationsCategories;
  fetchIntegrationsCategoriesTypes: TFetchIntegrationsCategoriesTypes;
  providerIntegration: IProviderIntegration;
  fetchAllProviderIntegrationsWithFilter: any; // TODO;
  isFetching: number;
  integrations: IIntegrationType[];
  allProviderIntegrations: IProviderIntegration[];
  testIntegration: TTestIntegration;
  createProviderIntegrations: TCreateProviderIntegrations;
  editProviderIntegrations: any;
  fetchManufacturers: TFetchManufacturers;
  manufacturers: any[];
  fetchManufacturerIntegrations: TFetchIntegrationsCategoriesTypes;
  updateProviderIntegration: any; // TODO;
  fetchProviderConfigStatus: any;
}

class SettingDeviceManufacturer extends Component<
  IDeviceSettingProps,
  IDeviceSettingState
> {
  static emptyState: IDeviceSettingState = {
    integrations: [],
    isFormValid: false,
    error: {},
    errorList: null,
    isFetching: true,
    deviceInfoId: null,
    providerIntegration: null,
    showNotification: false,
    integrationVerified: false,
    isVerificationSuccess: false,
    editedItemIndex: null,
    allProviderIntegrations: [],
    isEdit: true,
    selectedIntegrationFields: [],
    selectedIntegration: "",
    connectwiseManufacturer: "",
    openAddAccount: false,
    rows: [],
    isEditPopUp: false,
  };

  constructor(props: any) {
    super(props);
    this.state = _cloneDeep(SettingDeviceManufacturer.emptyState);
  }
  componentDidMount() {
    this.props.fetchPublicKey();
    this.props.fetchManufacturers();
    this.props.fetchIntegrationsCategories().then((action) => {
      if (action.type === FETCH_INTEGRATIONS_CAT_SUCCESS) {
        const deviceInfoId = action.response.findIndex(
          (x) => x.name === "DEVICE_INFO"
        );
        this.setState({ deviceInfoId: action.response[deviceInfoId].id });
        this.props.fetchIntegrationsCategoriesTypes(this.state.deviceInfoId);
      }
    });
  }

  componentDidUpdate(prevProps: IDeviceSettingProps) {
    if (
      this.props.integrations !== prevProps.integrations &&
      this.props.integrations
    ) {
      this.setState({
        integrations: this.props.integrations,
      });
      this.props.fetchAllProviderIntegrationsWithFilter(
        "DEVICE_INFO"
      );
    }
    if (
      this.props.allProviderIntegrations &&
      this.props.allProviderIntegrations !== prevProps.allProviderIntegrations
    ) {
      this.setState({
        allProviderIntegrations: this.props.allProviderIntegrations,
      });
      this.setRows(this.props);
    }
  }

  setRows = (nextProps: IDeviceSettingProps) => {
    const allProviderIntegrations = nextProps.allProviderIntegrations;

    const rows = allProviderIntegrations
      .filter((user) => user.category === "DEVICE_INFO")
      .map((user, index) => ({
        type: user.type,
        authentication_info: user.authentication_info,
        id: user.id,
        index,
      }));

    this.setState({ rows });
  };

  setEditStateAdd = () => {
    this.setState({
      showNotification: false,
      editedItemIndex: null,
      isEdit: true,
      integrationVerified: false,
    });
  };

  addAccount = () => {
    this.setState({
      openAddAccount: !this.state.openAddAccount,
      editedItemIndex: null,
      showNotification: false,
      selectedIntegration: "",
      connectwiseManufacturer: "",
      isEdit: true,
      integrationVerified: false,
      selectedIntegrationFields: [],
      isEditPopUp: false,
    });
  };

  renderNotification = () => {
    const { showNotification, isVerificationSuccess } = this.state;
    const onNotificationClick = () =>
      this.setState({
        showNotification: false,
      });

    if (showNotification) {
      return (
        <div
          onClick={onNotificationClick}
          className={`settings__notification ${
            isVerificationSuccess
              ? "settings__notification--success"
              : "settings__notification--fail"
          }`}
        >
          {isVerificationSuccess ? "Test success. Please Save." : "Test Failed"}
        </div>
      );
    } else {
      return null;
    }
  };

  getManufaturerTypes = () => {
    let types = [];
    if (this.props.integrations) {
      types = this.props.integrations.map((int) => {
        return {
          value: int.name,
          label: int.name,
        };
      });
    }

    return types;
  };

  handleSelectChange = (event: any) => {
    const targetValue = event.target.value;
    const prevIntegrations = this.state.integrations;

    const integrationToRender = prevIntegrations.filter(
      (int) => int.name === targetValue
    );

    const selectedIntegrationFields = integrationToRender[0];
    if (selectedIntegrationFields) {
      Object.keys(selectedIntegrationFields.authentication_config).map(
        (key) => {
          selectedIntegrationFields.authentication_config[key].value = "";
        }
      );
    }

    let manufactureMap = this.state.allProviderIntegrations.filter(
      (user) => user.category === "CRM"
    )[0];

    manufactureMap =
      manufactureMap &&
      manufactureMap.other_config &&
      manufactureMap.other_config.manufacturer_map;
    let connectwiseManufacturer;
    if (manufactureMap) {
      connectwiseManufacturer = Object.keys(manufactureMap).filter(
        (item) => parseInt(item, 10) === prevIntegrations[0].id
      );
    }

    this.setState(() => ({
      selectedIntegrationFields: _cloneDeep(selectedIntegrationFields),
      selectedIntegration: targetValue,
      connectwiseManufacturer: connectwiseManufacturer
        ? manufactureMap[connectwiseManufacturer[0]]
        : "",
    }));
  };

  handleChangeAdd = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const selectedIntegrationFields = this.state.selectedIntegrationFields;
    selectedIntegrationFields.authentication_config[
      targetName
    ].value = targetValue;

    this.setState(() => ({
      selectedIntegrationFields: _cloneDeep(selectedIntegrationFields),
    }));
  };

  isValidAdd = () => {
    const selectedIntegrationFields = this.state.selectedIntegrationFields
      .authentication_config;
    let isValid = true;

    const error = Object.keys(selectedIntegrationFields).reduce(
      (errorObj, fieldName) => {
        const attributes = selectedIntegrationFields[fieldName];

        if (attributes.is_required && attributes.value === "") {
          isValid = false;

          errorObj[fieldName] = {
            errorState: "error",
            errorMessage: `${attributes.label} is required`,
          };
        }

        return errorObj;
      },
      {}
    );
    this.setState({
      error,
    });

    return isValid;
  };

  onTestAdd = () => {
    if (!this.isValidAdd()) {
      return null;
    }
    this.setState({
      integrationVerified: false,
      showNotification: false,
      isVerificationSuccess: false,
      isEdit: false,
    });

    const integration = this.state.selectedIntegrationFields;
    const typeId = integration.id;
    const authenticationConfig = integration.authentication_config;
    const integrationObject: any = Object.keys(authenticationConfig).reduce(
      (accumulator, key) => {
        accumulator[key] = authenticationConfig[key].value;

        return accumulator;
      },
      {}
    ); // tslint:disable-line

    integrationObject.client_secret = encryptTextUsingRSA(
      integrationObject.client_secret,
      this.props.publicKey
    );
    this.props
      .testIntegration(this.state.deviceInfoId, typeId, integrationObject)
      .then((action) => {
        if (action.type === TEST_INTEGRATION_SUCCESS) {
          const verified = action.response.verified;

          this.setState({
            integrationVerified: verified,
            showNotification: true,
            isVerificationSuccess: verified,
            isEdit: !verified,
          });
        }
      });
  };

  onSaveAdd = () => {
    if (!this.isValidAdd()) {
      return null;
    }
    const integration = this.state.selectedIntegrationFields;
    const typeId = integration.id;
    const authenticationConfig = integration.authentication_config;
    const integrationObject: any = Object.keys(authenticationConfig).reduce(
      (accumulator, key) => {
        accumulator[key] = authenticationConfig[key].value;

        return accumulator;
      },
      {}
    ); // tslint:disable-line

    integrationObject.client_secret = encryptTextUsingRSA(
      integrationObject.client_secret,
      this.props.publicKey
    );

    const providerIntegration: any = {
      integration_type: typeId,
      authentication_info: integrationObject,
    };
    if (this.state.isEditPopUp) {
      const allProviderIntegrations = this.props.allProviderIntegrations.filter(
        (user) => user.category === "DEVICE_INFO"
      );
      const providerId = allProviderIntegrations[0].id;
      this.props
        .editProviderIntegrations(providerId, providerIntegration)
        .then((action) => {
          if (action.type === CREATE_PROVIDER_INTEGRATIONS_SUCCESS) {
            store.dispatch({
              type: SHOW_SUCCESS_MESSAGE,
              message: "Account Updated Successfully.",
            });
            this.setState({
              integrationVerified: false,
              showNotification: false,
              isVerificationSuccess: false,
              openAddAccount: false,
              selectedIntegrationFields: [],
              selectedIntegration: "",
              isEdit: true,
            });
            this.props.fetchAllProviderIntegrationsWithFilter(
              "DEVICE_MONITOR"
            );
          } else if (action.type === CREATE_PROVIDER_INTEGRATIONS_FAILURE) {
            store.dispatch({
              type: ERROR,
              errorMessage: action.errorList.data.authentication_info[0],
            });
          }
        });
    } else {
      this.props
        .createProviderIntegrations(providerIntegration)
        .then((action) => {
          if (action.type === CREATE_PROVIDER_INTEGRATIONS_SUCCESS) {
            store.dispatch({
              type: SHOW_SUCCESS_MESSAGE,
              message: "Account added Successfully.",
            });
            this.setState({
              integrationVerified: false,
              showNotification: false,
              isVerificationSuccess: false,
              openAddAccount: false,
              selectedIntegrationFields: [],
              selectedIntegration: "",
              isEdit: true,
            });
            this.props.fetchAllProviderIntegrationsWithFilter(
              "DEVICE_INFO"
            );
            this.props.fetchProviderConfigStatus();
          } else if (action.type === CREATE_PROVIDER_INTEGRATIONS_FAILURE) {
            store.dispatch({
              type: ERROR,
              errorMessage: action.errorList.data.authentication_info[0],
            });
          }
        });
    }
  };

  toggleNewModal = () => {
    this.setState({ openAddAccount: !this.state.openAddAccount });
  };

  renderFooter = () => {
    return (
      <div className="device-manufacturer__body-actions">
        {this.state.isEdit &&
          this.state.selectedIntegration !== "" &&
          !this.state.integrationVerified && (
            <SquareButton
              onClick={() => this.onTestAdd()}
              content="Test"
              bsStyle={"primary"}
            />
          )}
        {!this.state.isEdit && this.state.integrationVerified && (
          <SquareButton
            onClick={() => this.onSaveAdd()}
            content="Save"
            bsStyle={"primary"}
          />
        )}
        {!this.state.isEdit && (
          <SquareButton
            content="Edit"
            onClick={() => this.setEditStateAdd()}
            bsStyle={"primary"}
          />
        )}
        {this.state.openAddAccount && (
          <SquareButton
            content="Cancel"
            onClick={this.addAccount}
            bsStyle={"default"}
          />
        )}
      </div>
    );
  };

  renderAddDeviceIntegration = () => {
    const selectedIntegrationFields = this.state.selectedIntegrationFields
      .authentication_config;

    return (
      <div className="integration-list integration-list__add">
        {this.props.isFetching > 0 && (
          <div className="loader">
            <Spinner show={this.props.isFetching > 0} />
          </div>
        )}

        <div
          className={
            this.state.isEdit
              ? "device-manufacturer__body-fields"
              : "device-manufacturer__body-fields disable-div"
          }
        >
          {this.renderNotification()}
          <div className="manufacturer-from-connectwise">
            <div className="select-div-1">
              <label className="manufacturer-label" htmlFor="">
                Select Manufacturer
              </label>
              <SelectInput
                name="Manufacturer"
                value={this.state.selectedIntegration}
                options={this.getManufaturerTypes()}
                onChange={(event) => this.handleSelectChange(event)}
                disabled={this.state.isEditPopUp}
              />
            </div>
          </div>

          <div className="device-fields-group">
            {selectedIntegrationFields &&
              Object.keys(selectedIntegrationFields).map((key, fieldIndex) => {
                const name = key;
                const attributes = selectedIntegrationFields[key];

                return (
                  <Input
                    key={fieldIndex}
                    field={{
                      label: attributes.label,
                      type: attributes.type,
                      isRequired: attributes.is_required,
                      value: attributes.value,
                      options: attributes.options,
                    }}
                    width={6}
                    disabled={JSON.parse(attributes.disabled) ? true : false}
                    placeholder={`Enter ${attributes.label}`}
                    name={name}
                    onChange={(event) => this.handleChangeAdd(event)}
                    error={this.state.error[name]}
                  />
                );
              })}
          </div>
        </div>
      </div>
    );
  };

  onEditRowClick = (data, event: any) => {
    event.stopPropagation();

    const targetValue = data.type;
    const prevIntegrations = this.state.integrations;

    const integrationToRender = prevIntegrations.filter(
      (int) => int.name === targetValue
    );

    const selectedIntegrationFields = _cloneDeep(integrationToRender[0]);
    const integrations = this.state.integrations;
    const allProviderIntegrations = this.state.allProviderIntegrations.filter(
      (user) => user.category === "DEVICE_INFO"
    );

    if (integrations.length === 0 || allProviderIntegrations.length === 0) {
      return null;
    }

    const integrationFields = selectedIntegrationFields.authentication_config;

    if (allProviderIntegrations.length > 0) {
      Object.keys(integrationFields).map((key) => {
        integrationFields[key].value =
          allProviderIntegrations[data.index].authentication_info[key];
      });
      selectedIntegrationFields.authentication_config.client_secret.value = "";
    }

    this.setState(() => ({
      selectedIntegrationFields: selectedIntegrationFields,
      selectedIntegration: targetValue,
      openAddAccount: true,
      isEditPopUp: true,
    }));
  };

  render() {
    const columns: any = [
      {
        accessor: "type",
        Header: "Type",
        width: 150,
        sortable: false,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "authentication_info",
        Header: "Authentication Info",
        sortable: false,
        Cell: (cell) => (
          <div>
            {Object.keys(cell.value).map((info, index) => {
              return (
                <div key={index}>
                  <label>{info} </label> :
                  {info.includes("secret") ? (
                    <label> &nbsp; * * * * * * * * * * * * * * * *</label>
                  ) : (
                    <label>
                      &nbsp;
                      {cell.value[info]}
                    </label>
                  )}
                </div>
              );
            })}
          </div>
        ),
      },
      {
        accessor: "index",
        Header: "Edit",
        width: 60,
        sortable: false,
        Cell: (cell) => (
          <EditButton onClick={(e) => this.onEditRowClick(cell.original, e)} />
        ),
      },
    ];

    return (
      <div className="device-manufacturer-container">
        <h3>API Integrations</h3>

        <SquareButton
          onClick={() => this.addAccount()}
          content="Add New Device Manufacturer Account"
          bsStyle={"primary"}
          className="add-new-account"
        />

        <ModalBase
          show={this.state.openAddAccount}
          onClose={this.toggleNewModal}
          titleElement={`${
            this.state.isEditPopUp ? "Update" : "Add New"
          } Device Manufacturer Account`}
          bodyElement={
            this.state.openAddAccount && this.renderAddDeviceIntegration()
          }
          footerElement={this.renderFooter()}
          className="add-manufacturer-setting"
        />
        <div className="device-manufacturer-setting-list">
          <Table columns={columns} rows={this.state.rows} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  publicKey: state.setting.publicKey,
  integrations: state.integration.integrations,
  integrationsCatgories: state.integration.integrationsCatgories,
  providerIntegration: state.providerIntegration.providerIntegration,
  isFetching: state.providerIntegration.isFetching,
  allProviderIntegrations: state.providerIntegration.allProviderIntegrations,
  manufacturers: state.inventory.manufacturers,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPublicKey: () => dispatch(fetchEncryptionPublicKey()),
  fetchIntegrationsCategories: () => dispatch(fetchIntegrationsCategories()),
  fetchIntegrationsCategoriesTypes: (id: number) =>
    dispatch(fetchIntegrationsCategoriesTypes(id)),
  fetchAllProviderIntegrationsWithFilter: (type: string) =>
    dispatch(fetchAllProviderIntegrationsWithFilter(type)),
  testIntegration: (
    catId: number,
    typeId: number,
    integration: Array<{ [name: string]: string }>
  ) => dispatch(testIntegration(catId, typeId, integration)),
  createProviderIntegrations: (
    providerIntegration: IProviderIntegrationRequest
  ) => dispatch(createProviderIntegrations(providerIntegration)),
  editProviderIntegrations: (
    id: number,
    providerIntegration: IProviderIntegrationRequest
  ) => dispatch(editProviderIntegrations(id, providerIntegration)),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  updateProviderIntegration: (
    providerId: number,
    providerIntegration: IProviderIntegration
  ) => dispatch(updateProviderIntegration(providerId, providerIntegration)),
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingDeviceManufacturer);
