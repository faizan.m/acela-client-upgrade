import React, { useEffect } from "react";
import { connect } from "react-redux";
import SettingDeviceManufacturer from "./SettingDeviceManufacturer";
import SettingPAX8 from "./SettingPAX8";
import SettingGraphAPI from "./SettingGraphAPI";
import SettingIngramAPI from "./SettingIngramAPI";
import { fetchEncryptionPublicKey } from "../../../actions/setting";
import "./style.scss";

interface APICredentialsProps {
  fetchPublicKey: () => Promise<any>;
}

const APICredentials: React.FC<APICredentialsProps> = (props) => {
  useEffect(() => {
    props.fetchPublicKey();
  }, []);

  return (
    <>
      <SettingDeviceManufacturer />
      <SettingIngramAPI />
      <SettingPAX8 />
      <SettingGraphAPI />
    </>
  );
};

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPublicKey: () => dispatch(fetchEncryptionPublicKey()),
});

export default connect(mapStateToProps, mapDispatchToProps)(APICredentials);
