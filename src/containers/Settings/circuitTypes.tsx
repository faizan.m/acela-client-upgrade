import React from 'react';

import { cloneDeep } from 'lodash';
import { connect } from 'react-redux';
import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import {
  createCircuitTypes,
  deleteCircuitTypes,
  editCircuitTypes,
  fetchCircuitTypes,
} from '../../actions/documentation';
import SquareButton from '../../components/Button/button';
import DeleteButton from '../../components/Button/deleteButton';
import EditButton from '../../components/Button/editButton';
import ConfirmBox from '../../components/ConfirmBox/ConfirmBox';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { fromISOStringToFormattedDate, phoneNumberInFormat } from '../../utils/CalendarUtil';
import './style.scss';

interface ICircuitTypeProps {
  isFetchingTypes: boolean;
  isFetching: boolean;
  circuitTypes: any[];
  addSuccessMessage: any;
  addErrorMessage: any;
  createCircuitTypes: any;
  fetchCircuitTypes: any;
  editCircuitTypes: any;
  deleteCircuitTypes: any;
}

interface ICircuitTypeState {
  circuitTypes: any[];
  name: string;
  circuit_infos: any[];
  warningMessage: any;
  id: number;
  open: boolean;
  isopenConfirm: boolean;
  error: {
    name: IFieldValidation;
  };
}

class CircuitTypes extends React.Component<
  ICircuitTypeProps,
  ICircuitTypeState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  constructor(props: ICircuitTypeProps) {
    super(props);

    this.state = {
      circuitTypes: [],
      open: false,
      circuit_infos: [],
      warningMessage: null,
      isopenConfirm: false,
      name: '',
      id: 0,
      error: {
        name: { ...CircuitTypes.emptyErrorState },
      },
    };
  }
  componentDidMount() {
    this.props.fetchCircuitTypes(true);
  }

  componentDidUpdate(prevProps: ICircuitTypeProps) {
    if (
      this.props.circuitTypes &&
      prevProps.circuitTypes !== this.props.circuitTypes
    ) {
      const circuitTypes = this.props.circuitTypes;
      circuitTypes.map(c => ({
        isEdit: false,
        name: c.name,
        error: '',
        id: c.id,
      }));
      this.setState({ circuitTypes });
    }
  }

  AddNewType = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.name as string) = '';
    (newState.warningMessage as any) = '';
    (newState.id as number) = 0;
    this.setState(newState);
  };

  onEditClick = (type, e) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.name as string) = type.name;
    (newState.id as number) = type.id;
    this.setState(newState);
  };

  onDeleteClick = (type, e) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = false;
    (newState.name as string) = type.name;
    (newState.warningMessage as any) = this.getWarningMessage(
      type.circuit_infos
    );
    (newState.id as number) = type.id;
    (newState.isopenConfirm as boolean) = true;
    this.setState(newState);
  };

  getWarningMessage = circuits => {
    const circuitsAsso =
      circuits &&
      circuits.map(item => {
        return `${item.circuit_id}`;
      });

    const message = (
      <div className="types-delete">
        <p className="warning-message">
          {circuits.length}
          {circuits.length > 1 ? ' circuits' : ' circuit'} will be deleted
          associated with this type.
        </p>
        <div className="body-section">
          <div className="device-details-circuit-info">
            {circuits && circuits.length > 0 && (
              <div className="document">
                <div className="tile-index">
                  <label> #</label>
                </div>
                <div className="tile">
                  <label> circuit Id</label>
                </div>
                <div className="tile">
                  <label> provider</label>
                </div>
                <div className="tile">
                  <label> circuit type</label>
                </div>

                <div className="tile">
                  <label> Phone</label>
                </div>
              </div>
            )}
            {circuits && circuits.length > 0
              ? circuits.map((key, fieldIndex) => {
                  return (
                    <div className="document" key={fieldIndex}>
                      <div className="tile-index">
                        <label />
                        <label>{fieldIndex + 1}</label>
                      </div>
                      <div className="tile">
                        <label />
                        <label>
                          {key.circuit_id ? key.circuit_id : 'N.A.'}
                        </label>
                      </div>
                      <div className="tile">
                        <label />
                        <label>{key.provider ? key.provider : 'N.A.'}</label>
                      </div>
                      <div className="tile">
                        <label />
                        <label>
                          {key.circuit_type ? key.circuit_type : 'N.A.'}
                        </label>
                      </div>
                      <div
                        className="
                                    tile"
                      >
                        <label />
                        <label>
                          {phoneNumberInFormat('',key.contact_phone )}
                        </label>
                      </div>
                    </div>
                  );
                })
              : 'No circuits are associated.'}
          </div>
        </div>
      </div>
    );

    return circuitsAsso && circuitsAsso.length > 0 && message ? message : '';
  };
  onCancelClick = e => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = false;
    (newState.name as string) = '';
    (newState.id as number) = 0;
    (newState.error.name.errorState as any) = "success";
    (newState.error.name.errorMessage as any) = '';
    this.setState(newState);
  };

  handleChange = e => {
    const newState = cloneDeep(this.state);
    (newState.name as string) = e.target.value;
    this.setState(newState);
  };

  onSaveClick = e => {
    if (this.state.id === 0) {
      this.props.createCircuitTypes(this.state.name).then(action => {
        if (action.type === 'CREATE_CIRCUIT_TYPE_SUCCESS') {
          const newState = cloneDeep(this.state);
          (newState.open as boolean) = false;
          (newState.name as string) = '';
          (newState.id as number) = 0;
          this.setState(newState);
          this.props.fetchCircuitTypes(true);
        }
        if (action.type === 'CREATE_CIRCUIT_TYPE_FAILURE') {
          const newState = cloneDeep(this.state);
          (newState.error.name.errorState as any) = "error";
          (newState.error.name
            .errorMessage as any) = action.errorList.data.name;
          this.setState(newState);
        }
      });
    } else {
      this.props
        .editCircuitTypes(this.state.name, this.state.id)
        .then(action => {
          if (action.type === 'CREATE_CIRCUIT_TYPE_SUCCESS') {
            const newState = cloneDeep(this.state);
            (newState.open as boolean) = false;
            (newState.name as string) = '';
            (newState.id as number) = 0;
            this.setState(newState);
            this.props.fetchCircuitTypes(true);
          }
          if (action.type === 'CREATE_CIRCUIT_TYPE_FAILURE') {
            const newState = cloneDeep(this.state);
            newState.error.name = action.errorList.data.name;
            (newState.error.name.errorState as any) = "error";
            (newState.error.name
              .errorMessage as any) = action.errorList.data.name;
            this.setState(newState);
          }
        });
    }
  };

  onClickConfirm = () => {
    this.props.deleteCircuitTypes(this.state.id).then(action => {
      if (action.type === 'CREATE_CIRCUIT_TYPE_SUCCESS') {
        const newState = cloneDeep(this.state);
        (newState.open as boolean) = false;
        (newState.isopenConfirm as boolean) = false;
        (newState.name as string) = '';
        (newState.id as number) = 0;
        this.setState(newState);
        this.props.fetchCircuitTypes(true);
        (newState.warningMessage as any) = null;
      }
      if (action.type === 'CREATE_CIRCUIT_TYPE_FAILURE') {
        const newState = cloneDeep(this.state);
        (newState.error.name.errorState as any) = "error";
        (newState.error.name.errorMessage as any) = action.errorList.data.name;
        this.setState(newState);
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
    });
  };

  renderTerritoriesMapping = () => {
    const types = this.state.circuitTypes;
    const columns: any = [
      {
        accessor: 'id',
        Header: '#',
        width: 110,
        sortable: false,
        Cell: cell => <div> {`${cell.index + 1}`}</div>,
      },
      {
        accessor: 'name',
        Header: 'Name',
        sortable: true,
        Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
      },
      {
        accessor: 'circuit_infos',
        Header: 'Circuit Assocoated',
        sortable: true,
        Cell: cell => (
          <div> {`${cell.value ? cell.value.length : ' N.A.'}`}</div>
        ),
      },
      {
        accessor: 'updated_on',
        Header: 'Updated On',
        id: 'updated_on',
        width: 160,
        sortable: true,
        Cell: cell => (
          <div>
            {' '}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : ' N.A.'
            }`}
          </div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Actions',
        width: 110,
        sortable: false,
        Cell: cell => (
          <div>
            {' '}
            <EditButton onClick={e => this.onEditClick(cell.original, e)} />
            <DeleteButton
              onClick={e => this.onDeleteClick(cell.original, e)}
            />{' '}
          </div>
        ),
      },
    ];

    return (
      <div className="types-mapping__options row ">
        <SquareButton
          onClick={e => this.AddNewType(true)}
          content="Add Circuit Type"
          bsStyle={"primary"}
          className="add-new-btn"
        />
        <ModalBase
          show={this.state.open}
          onClose={e => this.AddNewType(!this.state.open)}
          titleElement={`${this.state.id === 0 ? 'Add' : 'Edit'} Circuit Type`}
          bodyElement={
            <Input
              field={{
                label: 'Name',
                type: "TEXT",
                value: this.state.name,
                isRequired: true,
              }}
              width={12}
              placeholder="Enter name"
              error={this.state.error.name}
              name="name"
              onChange={e => this.handleChange(e)}
            />
          }
          footerElement={
            <div>
              <SquareButton
                content={`Save`}
                bsStyle={"primary"}
                onClick={e => this.onSaveClick(e)}
                className="save"
              />
              <SquareButton
                content={`Cancel`}
                bsStyle={"default"}
                onClick={e => this.onCancelClick(e)}
                className="save"
              />
            </div>
          }
          className="view-contacts"
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
          message={this.state.warningMessage}
        />
        <div className="col-md-12 col-xs-12 mapping-row heading">
          <Table
            columns={columns}
            rows={types}
            className={''}
            loading={this.props.isFetching}
          />
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="circuit-types-mapping">
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingTypes || this.props.isFetching ? true : false
            }
          />
        </div>

        {this.renderTerritoriesMapping()}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetchingTypes: state.documentation.isFetchingTypes,
  isFetching: state.documentation.isFetching,
  circuitTypes: state.documentation.circuitTypes,
});

const mapDispatchToProps = (dispatch: any) => ({
  deleteCircuitTypes: (id: number) => dispatch(deleteCircuitTypes(id)),
  createCircuitTypes: (name: string) => dispatch(createCircuitTypes(name)),
  editCircuitTypes: (name: string, id: number) =>
    dispatch(editCircuitTypes(name, id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchCircuitTypes: (show: boolean) => dispatch(fetchCircuitTypes(show)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CircuitTypes);
