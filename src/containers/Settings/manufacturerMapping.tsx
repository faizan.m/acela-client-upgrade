import React from 'react';

import { cloneDeep } from 'lodash';
import SquareButton from '../../components/Button/button';
import SelectInput from '../../components/Input/Select/select';

interface ImanufacturerMappingProps {
  integrations: IIntegrationType[];
  manufacturers: any[];
  manufacturerMapping: any;
  onSubmit: (selectedCategories: any) => void;
}

interface ImanufacturerMappingState {
  searchQueryField: any;
  manufacturerMapping: any;
  integrations: IIntegrationType[];
  isValid: boolean;
}

export default class ManufacturerMapping extends React.Component<
  ImanufacturerMappingProps,
  ImanufacturerMappingState
> {
  constructor(props: ImanufacturerMappingProps) {
    super(props);
    this.state = {
      manufacturerMapping: [],
      searchQueryField: [],
      integrations: this.props.integrations,
      isValid: true,
    };
  }

  componentDidMount() {
    this.setManufaturerIntegrations();
  }

  setManufaturerIntegrations = () => {
    const newState: ImanufacturerMappingState = cloneDeep(this.state);

    Object.keys(this.props.manufacturerMapping).map(key => {
      let index;
      this.props.integrations.map((res, i) => {
        if (res.id === parseInt(key, 10)) {
          index = i;
        }
      });
      newState.integrations[
        index
      ].device_manu_id = this.props.manufacturerMapping[key];
    });
    this.setState(newState);
  };

  getManufaturerTypes = () => {
    let types = [];
    if (this.props.manufacturers) {
      types = this.props.manufacturers.map(int => {
        return {
          value: int.id,
          label: int.label,
        };
      });
    }

    return types;
  };

  handleChangeField = (event: any, id?: any) => {
    const newState = cloneDeep(this.state);
    let index;
    newState.integrations.map((res, i) => {
      if (res.id === id) {
        index = i;
      }
    });
    newState.integrations[index].device_manu_id = event.target.value;
    this.setState(newState);
  };

  onSubmit = () => {
    const di = {};
    let isValid = true;
    this.state.integrations.forEach(element => {
      di[element.id] = element.device_manu_id;
      if (element && !element.device_manu_id) {
        isValid = false;
        this.setState({ isValid });
      }
    });
    if (isValid) {
      this.props.onSubmit(di);
    }
  };

  renderManufacturerList = () => {
    return (
      <div className="manufacturer-mapping">
        <table className="manufacturer-mapping__table">
          <tbody>
            <tr className="manufacturer-mapping__table-row" />
            <tr className="manufacturer-mapping__table-header">
              <th className="first">Supported Manufacturers</th>
              <th className="middle" />
              <th className="last">Manufacturers from connectwise </th>
              <th className="" />
            </tr>

            {this.state.integrations &&
              this.state.integrations.map((field, index) => (
                <tr key={index} className="manufacturer-mapping__table-row">
                  <td className="first">{field.name}</td>
                  <td className="middle">
                    <img src="/assets/new-icons/oppositearrow.svg" />
                  </td>
                  <td className="last">
                    {
                      <SelectInput
                        name="select"
                        value={field.device_manu_id}
                        onChange={event =>
                          this.handleChangeField(event, field.id)
                        }
                        options={this.getManufaturerTypes()}
                        searchable={true}
                        placeholder="Select field"
                        clearable={false}
                      />
                    }
                  </td>
                  <th className="" />
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    );
  };

  render() {
    return (
      <div className="manufacturer-mapping-container">
        {this.renderManufacturerList()}
        {!this.state.isValid && (
          <div className="manufacturer-error">Please select Manufacturers</div>
        )}
        <SquareButton
          content="Save"
          onClick={this.onSubmit}
          bsStyle={"primary"}
        />
      </div>
    );
  }
}
