import _cloneDeep from "lodash/cloneDeep";
import _set from "lodash/set";
import React from "react";
import { connect } from "react-redux";
import {
  FETCH_INTEGRATIONS_CAT_SUCCESS,
  fetchIntegrationsCategories,
  fetchIntegrationsCategoriesTypes,
  fetchManufacturerIntegrations,
  TEST_INTEGRATION_SUCCESS,
  testIntegration,
} from "../../actions/integration";
import {
  CREATE_PROVIDER_INTEGRATIONS_SUCCESS,
  createProviderIntegrations,
  FETCH_PROVIDER_CONFIG_STATUS_SUCCESS,
  fetchAllProviderIntegrations,
  fetchProviderBoardStatus,
  fetchProviderBoardStatusManaged,
  fetchProviderConfigStatus,
  fetchProviderExtraConfigs,
  fetchProviderIntegrations,
  SYNC_CW_DATA_FAILURE,
  SYNC_CW_DATA_SUCCESS,
  syncCWData,
  syncExternal,
  UPDATE_PROVIDER_INTEGRATIONS_SUCCESS,
  updateProviderIntegration,
  updateEmptySerialNumber,
} from "../../actions/provider/integration";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import BoardMapping from "./boardMapping";
import DeviceMapping from "./deviceMapping";
import {
  addErrorMessage,
  addSuccessMessage,
  addWarningMessage,
} from "../../actions/appState";
import { fetchManufacturers } from "../../actions/inventory";
import { encryptTextUsingRSA, isValidVal } from "../../utils/CommonUtils";
import ProductMapping from "./productMapping";
import SoftwareMapping from "./softwareMapping";
import Synchronize from "./synchronize";
import DeviceFilters from "./deviceFilters";
import EmptySerialMapping from "./emptySerialMapping";
import RoleRateMapping from "./roleRateMapping";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import "./style.scss";
import { fetchEncryptionPublicKey } from "../../actions/setting";

enum PageType {
  INTEGRATION,
  BOARD_MAPPING,
  DEVICE_MAPPING,
  PRODUCTMAPPING,
  MODEL_MAPPING,
  SYNCHRONIZE,
  TERRITORY,
  CIRCUITTYPES,
  ROLEMAPPING,
}

interface ISettingProps extends ICommonProps {
  // from redux
  publicKey: string;
  fetchPublicKey: () => Promise<any>;
  fetchIntegrationsCategoriesTypes: TFetchIntegrationsCategoriesTypes;
  fetchManufacturerIntegrations: TFetchIntegrationsCategoriesTypes;
  fetchProviderConfigStatus: TFetchConfigStatus;
  fetchProviderBoardStatus: any;
  fetchProviderBoardStatusManaged: any;
  createProviderIntegrations: TCreateProviderIntegrations;
  fetchProviderExtraConfigs: TFetchProviderExtraConfigs;
  fetchAllProviderIntegrations: any; // TODO;
  updateProviderIntegration: any; // TODO;
  fetchProviderIntegrations: any; // TODO;
  testIntegration: TTestIntegration;
  integrations: IIntegrationType[];
  configStatus: IConfigStatus;
  boardStatusList: any;
  boardManagedStatusList: any;
  providerIntegration: IProviderIntegration;
  allProviderIntegrations: IProviderIntegration[];
  providerExtraConfigs: IExtraConfig;
  integrationVerified: boolean;
  isFetching: number;
  integrationsCatgories: any;
  fetchIntegrationsCategories: TFetchIntegrationsCategories;
  fetchManufacturers: TFetchManufacturers;
  manufacturers: any[];
  manufacturerIntegrations: any;
  syncCWData: any;
  syncExternal: any;
  addSuccessMessage: TShowSuccessMessage;
  addErrorMessage: TShowErrorMessage;
  addWarningMessage: TShowErrorMessage;
  isFetchingExtraConfig: boolean;
  updateEmptySerialNumber: any;
  isFetchingStatus: boolean;
}

interface ISettingState {
  integrations: IIntegrationType[];
  integrationVerified: boolean;
  currentPage: {
    integrationIndex: number;
    typeIndex: number;
    pageType: PageType;
  };
  error: {
    [fieldName: string]: IFieldValidation;
  };
  showNotification: boolean;
  isVerificationSuccess: boolean;
  extraConfigs: IExtraConfig;
  providerIntegration: IProviderIntegration;
  boardStatusList?: any;
  boardManagedStatusList?: any;
  key?: string;
  managedKey?: string;
  isEdit: boolean;
  CRMId: number;
  loadingEmptySerial: boolean;
  deviceInfoId: number;
}

class Setting extends React.Component<ISettingProps, ISettingState> {
  constructor(props: ISettingProps) {
    super(props);

    this.state = {
      isEdit: false,
      integrations: [],
      integrationVerified: false,
      currentPage: {
        integrationIndex: null,
        typeIndex: null,
        pageType: null,
      },
      error: {},
      showNotification: false,
      loadingEmptySerial: false,
      isVerificationSuccess: false,
      extraConfigs: null,
      providerIntegration: null,
      CRMId: null,
      deviceInfoId: null,
    };
  }

  componentDidMount() {
    this.props.fetchPublicKey();
    this.props.fetchIntegrationsCategories().then((action) => {
      if (action.type === FETCH_INTEGRATIONS_CAT_SUCCESS) {
        const CRM = action.response.findIndex(
          (x) => x.name === "CRM"
        );
        const DEVICE_INFO = action.response.findIndex(
          (x) => x.name === "DEVICE_INFO"
        );
        this.setState({ CRMId: action.response[CRM].id });
        this.props.fetchIntegrationsCategoriesTypes(action.response[CRM].id);
        this.props.fetchManufacturerIntegrations(
          action.response[DEVICE_INFO].id
        );

        const deviceInfoId = action.response.findIndex(
          (x) => x.name === "DEVICE_INFO"
        );
        this.setState({ deviceInfoId: action.response[deviceInfoId].id });
        if (
          this.props.configStatus &&
          !this.props.configStatus.crm_authentication_configured &&
          this.props.integrations
        ) {
          this.setState({
            integrations: this.props.integrations,
            currentPage: {
              integrationIndex: 0,
              typeIndex: 0,
              pageType: PageType.INTEGRATION,
            },
          });
        } else if (
          this.props.configStatus &&
          this.props.configStatus.crm_authentication_configured
        ) {
          this.props.fetchAllProviderIntegrations();
          this.setState({ isEdit: true });
        } else {
          this.setState({ isEdit: false });
        }
      }
    });
  }

  componentDidUpdate(prevProps: ISettingProps) {
    const {
      configStatus,
      integrations,
      providerIntegration,
      allProviderIntegrations,
      providerExtraConfigs,
      boardStatusList,
      boardManagedStatusList,
    } = this.props;

    if (configStatus !== prevProps.configStatus) {
      if (configStatus.crm_authentication_configured) {
        this.props.fetchAllProviderIntegrations();
        this.setState({ isEdit: true });
      }
    }

    if (integrations && integrations !== prevProps.integrations) {
      this.setState({
        integrations,
        currentPage: {
          integrationIndex: 0,
          typeIndex: 0,
          pageType: PageType.INTEGRATION,
        },
      });
    }

    if (!prevProps.providerIntegration && providerIntegration) {
      const providerId = providerIntegration.id;
      this.props.fetchProviderExtraConfigs(providerId);
    }

    if (
      providerIntegration !== prevProps.providerIntegration &&
      (configStatus.crm_board_mapping_configured ||
        configStatus.crm_authentication_configured)
    ) {
      this.setState({ providerIntegration });
    }

    if (
      allProviderIntegrations !== prevProps.allProviderIntegrations &&
      allProviderIntegrations
    ) {
      this.props.fetchManufacturers();
      const provider = allProviderIntegrations.filter(
        (int) => int.type === "CONNECTWISE"
      );
      if (provider.length > 0) {
        this.props.fetchProviderIntegrations(provider[0].id);
      }
    }

    if (
      providerExtraConfigs !== prevProps.providerExtraConfigs &&
      configStatus.crm_authentication_configured &&
      configStatus.crm_board_mapping_configured &&
      configStatus.crm_device_categories_configured
    ) {
      this.setState({
        extraConfigs: providerExtraConfigs,
        currentPage: {
          integrationIndex: this.state.currentPage.integrationIndex,
          typeIndex: this.state.currentPage.typeIndex,
          pageType: this.state.currentPage.pageType,
        },
      });
    } else if (
      providerExtraConfigs !== prevProps.providerExtraConfigs &&
      !configStatus.crm_authentication_configured &&
      !configStatus.crm_board_mapping_configured &&
      !configStatus.crm_device_categories_configured
    ) {
      this.setState({
        extraConfigs: providerExtraConfigs,
        currentPage: {
          integrationIndex: this.state.currentPage.integrationIndex,
          typeIndex: this.state.currentPage.typeIndex,
          pageType: this.state.currentPage.pageType,
        },
      });
    } else if (
      providerExtraConfigs !== prevProps.providerExtraConfigs &&
      !configStatus.crm_authentication_configured
    ) {
      this.setState({
        extraConfigs: providerExtraConfigs,
        currentPage: {
          integrationIndex: 0,
          typeIndex: 0,
          pageType: PageType.INTEGRATION,
        },
      });
    } else if (
      providerExtraConfigs !== prevProps.providerExtraConfigs &&
      !configStatus.crm_board_mapping_configured
    ) {
      this.setState({
        extraConfigs: providerExtraConfigs,
        currentPage: {
          integrationIndex: null,
          typeIndex: null,
          pageType: PageType.BOARD_MAPPING,
        },
      });
    } else if (
      providerExtraConfigs !== prevProps.providerExtraConfigs &&
      !configStatus.crm_device_categories_configured
    ) {
      this.setState({
        extraConfigs: providerExtraConfigs,
        currentPage: {
          integrationIndex: null,
          typeIndex: null,
          pageType: PageType.DEVICE_MAPPING,
        },
      });
    }

    if (
      prevProps.providerExtraConfigs &&
      boardStatusList !== prevProps.boardStatusList
    ) {
      const prevMapping = prevProps.providerExtraConfigs;
      const x = this.state.key.replace(".board", "");
      _set(
        prevMapping,
        `board_mapping.${x}.closed_status.options`,
        boardStatusList
      );
      _set(
        prevMapping,
        `board_mapping.${x}.open_status.options`,
        boardStatusList
      );
      _set(
        prevMapping,
        `board_mapping.${x}.ticket_closed_status.options`,
        boardStatusList
      );
      _set(
        prevMapping,
        `board_mapping.${x}.smartnet_complete_status.options`,
        boardStatusList
      );
      this.setState({ extraConfigs: prevMapping });
    }

    if (
      prevProps.providerExtraConfigs &&
      boardManagedStatusList !== prevProps.boardManagedStatusList
    ) {
      const prevMapping = prevProps.providerExtraConfigs;
      const x = this.state.managedKey.replace(".board", "");
      _set(
        prevMapping,
        `board_mapping.${x}.closed_status.options`,
        boardManagedStatusList
      );
      _set(
        prevMapping,
        `board_mapping.${x}.open_status.options`,
        boardManagedStatusList
      );
      _set(
        prevMapping,
        `board_mapping.${x}.ticket_closed_status.options`,
        boardManagedStatusList
      );
      _set(
        prevMapping,
        `board_mapping.${x}.smartnet_complete_status.options`,
        boardManagedStatusList
      );
      this.setState({ extraConfigs: prevMapping });
    }
  }

  handleChange = (integrationIndex: number, typeIndex: number, event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    const prevIntegrations = this.state.integrations;
    const integrationToChange =
      prevIntegrations[integrationIndex].authentication_config;
    integrationToChange[targetName].value = targetValue;
    const integrations = _cloneDeep(prevIntegrations);

    this.setState((prevState) => ({
      integrations,
    }));
  };

  changePage = (
    integrationIndex: number,
    typeIndex: number,
    pageType: PageType
  ) => {
    if (
      (!this.props.configStatus.crm_authentication_configured &&
        pageType === PageType.BOARD_MAPPING) ||
      (!this.props.configStatus.crm_board_mapping_configured &&
        pageType === PageType.DEVICE_MAPPING)
    ) {
      return null;
    }

    this.setState({
      currentPage: {
        integrationIndex,
        typeIndex,
        pageType,
      },
    });
  };

  isValid = () => {
    const integrationIndex = this.state.currentPage.integrationIndex;
    const integration = this.state.integrations[integrationIndex];
    const authenticationConfig = integration.authentication_config;
    let isValid = true;

    const error = Object.keys(authenticationConfig).reduce(
      (errorObj, fieldName) => {
        const attributes = authenticationConfig[fieldName];

        if (attributes.is_required && !isValidVal(attributes.value)) {
          isValid = false;

          errorObj[fieldName] = {
            errorState: "error",
            errorMessage: `${attributes.label} is required`,
          };
        }

        return errorObj;
      },
      {}
    );

    this.setState({
      error,
    });

    return isValid;
  };

  // test and submit
  onTest = () => {
    if (!this.isValid()) {
      return null;
    }

    const integrationIndex = this.state.currentPage.integrationIndex;
    const integration = this.state.integrations[integrationIndex];
    const integrationId = integration.id;
    const authenticationConfig = integration.authentication_config;
    const integrationObject: any = Object.keys(authenticationConfig).reduce(
      (accumulator, key) => {
        accumulator[key] = authenticationConfig[key].value;

        return accumulator;
      },
      {}
    ); // tslint:disable-line

    integrationObject.private_key = encryptTextUsingRSA(
      integrationObject.private_key,
      this.props.publicKey
    );

    this.props
      .testIntegration(this.state.CRMId, integrationId, integrationObject)
      .then((action) => {
        if (action.type === TEST_INTEGRATION_SUCCESS) {
          const verified = action.response.verified;

          this.setState({
            integrationVerified: verified,
            showNotification: true,
            isVerificationSuccess: verified,
            isEdit: verified,
          });
        }
      });
  };

  onSave = () => {
    if (!this.isValid()) {
      return null;
    }
    const integrationIndex = this.state.currentPage.integrationIndex;
    const integration = this.state.integrations[integrationIndex];
    const integrationId = integration.id;
    const authenticationConfig = integration.authentication_config;
    const authenticationInfo: any = Object.keys(authenticationConfig).reduce(
      (accumulator, key) => {
        accumulator[key] = authenticationConfig[key].value;

        return accumulator;
      },
      {}
    ); // tslint:disable-line

    authenticationInfo.private_key = encryptTextUsingRSA(
      authenticationInfo.private_key,
      this.props.publicKey
    );

    const providerIntegration: any = {
      integration_type: integrationId,
      authentication_info: authenticationInfo,
    };

    if (this.props.configStatus.crm_authentication_configured) {
      const providerId = this.props.providerIntegration.id;
      const boardMapping = this.getSavedBoardMappings();
      const deviceCategories = this.getSavedDeviceMappings();
      const providerIntegrationObject: any = {
        // TODO: static now, change it later
        authentication_info: authenticationInfo,
        integration_type: 1,
        other_config: {
          manufacturer_mapping: deviceCategories,
          subscription_mapping: this.getSavedSoftwareMappings(),
          board_mapping: boardMapping,
          device_filters: this.getSavedDeviceFilters(),
          empty_serial_mapping: this.getEmptySerialMappings(),
        },
      };

      this.props
        .updateProviderIntegration(providerId, providerIntegrationObject)
        .then((action) => {
          if (action.type === UPDATE_PROVIDER_INTEGRATIONS_SUCCESS) {
            this.setState({
              isEdit: false,
            });
            this.props.fetchProviderConfigStatus().then((a) => {
              if (a.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS) {
                this.setState({
                  showNotification: false,
                  isVerificationSuccess: null,
                  isEdit: false,
                });
                this.changePage(null, null, PageType.BOARD_MAPPING);
              }
            });
          }
        });
    } else {
      this.props
        .createProviderIntegrations(providerIntegration)
        .then((action) => {
          if (action.type === CREATE_PROVIDER_INTEGRATIONS_SUCCESS) {
            this.setState({
              showNotification: false,
              isVerificationSuccess: null,
              isEdit: false,
            });

            this.props.fetchProviderConfigStatus().then((a) => {
              if (a.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS) {
                this.changePage(null, null, PageType.BOARD_MAPPING);
              }
            });
          }
        });
    }
  };

  onBoardMappingSave = (boardMapping: any) => {
    const providerId = this.props.providerIntegration.id;
    const deviceCategories = this.getSavedDeviceMappings();
    const providerIntegrationObject: any = {
      // TODO: static now, change it later
      integration_type: 1,
      other_config: {
        board_mapping: boardMapping,
        subscription_mapping: this.getSavedSoftwareMappings(),
        manufacturer_mapping: deviceCategories,
        device_filters: this.getSavedDeviceFilters(),
        empty_serial_mapping: this.getEmptySerialMappings(),
      },
    };

    this.props
      .updateProviderIntegration(providerId, providerIntegrationObject)
      .then((action) => {
        if (action.type === UPDATE_PROVIDER_INTEGRATIONS_SUCCESS) {
          this.props.fetchProviderConfigStatus().then((a) => {
            if (a.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS) {
              this.changePage(null, null, PageType.DEVICE_MAPPING);
            }
          });
        }
      });
  };

  onDeviceMappingSave = (savedDeviceCategories: number[]) => {
    const providerId = this.props.providerIntegration.id;
    const boardMapping = this.getSavedBoardMappings();
    const providerIntegrationObject: any = {
      // TODO: static now, change it later
      integration_type: 1,
      other_config: {
        manufacturer_mapping: savedDeviceCategories,
        subscription_mapping: this.getSavedSoftwareMappings(),
        board_mapping: boardMapping,
        device_filters: this.getSavedDeviceFilters(),
        empty_serial_mapping: this.getEmptySerialMappings(),
      },
    };

    this.props
      .updateProviderIntegration(providerId, providerIntegrationObject)
      .then((action) => {
        if (action.type === UPDATE_PROVIDER_INTEGRATIONS_SUCCESS) {
          this.props.fetchProviderConfigStatus().then((a) => {
            if (a.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS) {
              this.props.addSuccessMessage("Device mapping updated.");
            }
          });
        }
      });
  };

  onSoftwareMappingSave = (savedDeviceCategories: any[]) => {
    const providerId = this.props.providerIntegration.id;
    const boardMapping = this.getSavedBoardMappings();
    const providerIntegrationObject: any = {
      // TODO: static now, change it later
      integration_type: 1,
      other_config: {
        manufacturer_mapping: this.getSavedDeviceMappings(),
        subscription_mapping: savedDeviceCategories,
        board_mapping: boardMapping,
        device_filters: this.getSavedDeviceFilters(),
        empty_serial_mapping: this.getEmptySerialMappings(),
      },
    };
    this.props
      .updateProviderIntegration(providerId, providerIntegrationObject)
      .then((action) => {
        if (action.type === UPDATE_PROVIDER_INTEGRATIONS_SUCCESS) {
          this.props.fetchProviderConfigStatus().then((a) => {
            if (a.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS) {
              this.props.addSuccessMessage("Software mapping updated.");
            }
          });
        }
      });
  };

  onDeviceFilterSave = (device_filters: any[]) => {
    const providerId = this.props.providerIntegration.id;
    const boardMapping = this.getSavedBoardMappings();
    const providerIntegrationObject: any = {
      integration_type: 1,
      other_config: {
        manufacturer_mapping: this.getSavedDeviceMappings(),
        subscription_mapping: this.getSavedSoftwareMappings,
        board_mapping: boardMapping,
        device_filters,
        empty_serial_mapping: this.getEmptySerialMappings(),
      },
    };
    this.props
      .updateProviderIntegration(providerId, providerIntegrationObject)
      .then((action) => {
        if (action.type === UPDATE_PROVIDER_INTEGRATIONS_SUCCESS) {
          this.props.fetchProviderConfigStatus().then((a) => {
            if (a.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS) {
              this.props.addSuccessMessage("Device Filters updated.");
            }
          });
        }
      });
  };

  onEmptySerialMappingSave = (data: any[]) => {
    const providerId = this.props.providerIntegration.id;
    this.setState({ loadingEmptySerial: true });
    this.props
      .updateEmptySerialNumber(providerId, {
        empty_serial_mapping: {
          device_category_ids: data,
        },
      })
      .then((action) => {
        if (action.type === "UPDATE_EMPTY_SN_SUCCESS") {
          this.props.fetchAllProviderIntegrations();
          this.setState({ loadingEmptySerial: false });
          this.props.addSuccessMessage("Empty serial category  updated.");
        }
      });
  };

  getSavedBoardMappings = () => {
    if (
      this.props.configStatus &&
      this.props.configStatus.crm_board_mapping_configured &&
      this.state.providerIntegration.other_config
    ) {
      return this.state.providerIntegration &&
        this.state.providerIntegration.other_config
        ? this.state.providerIntegration.other_config.board_mapping
        : null;
    }

    return null;
  };

  getBoardMapping = (id: number, key: string) => {
    this.setState({ key });
    this.props.fetchProviderBoardStatus(id);
  };

  getBoardMappingManaged = (id: number, key: string) => {
    this.setState({ managedKey: key });
    this.props.fetchProviderBoardStatusManaged(id);
  };

  getSavedDeviceMappings = () => {
    if (
      this.props.configStatus &&
      this.props.configStatus.crm_device_categories_configured
    ) {
      return this.state.providerIntegration &&
        this.state.providerIntegration.other_config
        ? this.state.providerIntegration.other_config.manufacturer_mapping
        : null;
    }

    return null;
  };

  getSavedSoftwareMappings = () => {
    if (
      this.props.configStatus &&
      this.props.configStatus.crm_device_categories_configured
    ) {
      return this.state.providerIntegration &&
        this.state.providerIntegration.other_config
        ? this.state.providerIntegration.other_config.subscription_mapping
        : null;
    }

    return null;
  };

  getSavedDeviceFilters = () => {
    if (
      this.props.configStatus &&
      this.props.configStatus.crm_device_categories_configured
    ) {
      return this.state.providerIntegration &&
        this.state.providerIntegration.other_config
        ? this.state.providerIntegration.other_config.device_filters
        : null;
    }

    return null;
  };
  getEmptySerialMappings = () => {
    const empty_serial_mapping =
      this.state.providerIntegration &&
      this.state.providerIntegration.other_config &&
      this.state.providerIntegration.other_config.empty_serial_mapping;

    return empty_serial_mapping || "";
  };

  setEditState = () => {
    this.setState({
      showNotification: false,
      isVerificationSuccess: false,
      isEdit: !this.state.isEdit,
    });
  };

  setViewState = () => {
    this.setState({
      showNotification: false,
      isVerificationSuccess: false,
      isEdit: !this.state.isEdit,
      error: {},
    });
  };

  renderIntegration = (integrationIndex: number, typeIndex: number) => {
    const integrations = this.state.integrations;
    if (!integrations || integrationIndex === null || typeIndex === null) {
      return null;
    }

    const integrationToRender = integrations[integrationIndex];

    const integrationFields = integrationToRender.authentication_config;

    if (
      this.state.providerIntegration &&
      this.state.providerIntegration.authentication_info &&
      this.state.isEdit &&
      !this.state.isVerificationSuccess
    ) {
      Object.keys(integrationFields).map((key, fieldIndex) => {
        integrationFields[
          key
        ].value = this.state.providerIntegration.authentication_info[key];
      });
    }

    return (
      <div className="cw-authentication-container">
        <div className="loader">
          <Spinner
            show={this.props.isFetching > 0 || this.props.isFetchingExtraConfig}
          />
        </div>
        <div
          className={`settings__body ${
            this.props.isFetching > 0 ? `loading` : ``
          }`}
        >
          <div
            className={
              "cw-auth-input" + (this.state.isEdit ? " disable-div" : "")
            }
          >
            {Object.keys(integrationFields).map((key, fieldIndex) => {
              const name = key;
              const attributes = integrationFields[key];

              return (
                <Input
                  key={fieldIndex}
                  field={{
                    label: attributes.label,
                    type: attributes.type,
                    isRequired: attributes.is_required,
                    value: attributes.value,
                    options: attributes.options,
                  }}
                  width={6}
                  disabled={
                    this.props.configStatus &&
                    this.props.configStatus.crm_authentication_configured &&
                    JSON.parse(attributes.disabled)
                      ? true
                      : false
                  }
                  placeholder={`Enter ${attributes.label}`}
                  name={name}
                  onChange={(event) =>
                    this.handleChange(integrationIndex, typeIndex, event)
                  }
                  error={this.state.error[name]}
                  labelIcon="info"
                  labelTitle={attributes.help_text}
                  onLabeIconClick={(e) => {
                    window.open(attributes.help_text);
                  }}
                />
              );
            })}
          </div>
          <div className="settings__body-actions">
            {!this.state.isEdit && (
              <>
                <SquareButton
                  onClick={this.onTest}
                  content="Test"
                  bsStyle={"default"}
                />
                <div id="settings-separator" />
              </>
            )}
            {this.state.isEdit && this.state.integrationVerified && (
              <SquareButton
                onClick={this.onSave}
                content="Save"
                bsStyle={"primary"}
              />
            )}
            {this.state.isEdit && (
              <SquareButton
                content="Edit"
                onClick={this.setEditState}
                bsStyle={"primary"}
              />
            )}
            {!this.state.isEdit && (
              <SquareButton
                content="Cancel"
                onClick={this.setViewState}
                bsStyle={"default"}
              />
            )}
          </div>
        </div>
      </div>
    );
  };

  renderTopBar = () => {
    const integrations = this.state.integrations;
    const currentPage = this.state.currentPage;

    if (!integrations) {
      return null;
    }

    return (
      <div className="settings__header col-md-2">
        {integrations.map((integrationType, integrationIndex) => (
          <div
            key={integrationIndex}
            className={`settings__header-link ${
              currentPage.pageType === PageType.INTEGRATION &&
              integrationIndex === currentPage.integrationIndex
                ? "settings__header-link--active"
                : ""
            }`}
            onClick={(e) =>
              this.changePage(integrationIndex, 0, PageType.INTEGRATION)
            }
          >
            CW Authentication
            <span className="required-tabs" />
          </div>
        ))}
        {this.props.providerExtraConfigs && (
          <div
            className={`settings__header-link ${
              currentPage.pageType === PageType.BOARD_MAPPING
                ? "settings__header-link--active"
                : ""
            }`}
            onClick={(e) => this.changePage(null, null, PageType.BOARD_MAPPING)}
          >
            Field Mapping
            <span className="required-tabs" />
          </div>
        )}
        {this.props.providerExtraConfigs && (
          <div
            className={`settings__header-link ${
              currentPage.pageType === PageType.DEVICE_MAPPING
                ? "settings__header-link--active"
                : ""
            }`}
            onClick={(e) =>
              this.changePage(null, null, PageType.DEVICE_MAPPING)
            }
          >
            Categories Mapping & Filters
            <span className="required-tabs" />
          </div>
        )}
        {this.props.providerExtraConfigs && (
          <>
            <div
              className={`settings__header-link ${
                currentPage.pageType === PageType.PRODUCTMAPPING
                  ? "settings__header-link--active"
                  : ""
              }`}
              onClick={(e) =>
                this.changePage(null, null, PageType.PRODUCTMAPPING)
              }
            >
              Subscription Product Mapping
            </div>
          </>
        )}
        {this.props.providerExtraConfigs && (
          <div
            className={`settings__header-link ${
              currentPage.pageType === PageType.SYNCHRONIZE
                ? "settings__header-link--active"
                : ""
            }`}
            onClick={(e) => this.changePage(null, null, PageType.SYNCHRONIZE)}
          >
            Synchronize
          </div>
        )}
        {this.props.providerExtraConfigs && (
          <div
            className={`settings__header-link ${
              currentPage.pageType === PageType.ROLEMAPPING
                ? "settings__header-link--active"
                : ""
            }`}
            onClick={(e) => this.changePage(null, null, PageType.ROLEMAPPING)}
          >
            Role Rate Mapping
          </div>
        )}
      </div>
    );
  };

  renderNotification = () => {
    const { showNotification, isVerificationSuccess } = this.state;
    const onNotificationClick = (e) =>
      this.setState({
        showNotification: false,
      });

    if (showNotification) {
      return (
        <div
          onClick={onNotificationClick}
          className={`settings__notification ${
            isVerificationSuccess
              ? "settings__notification--success"
              : "settings__notification--fail"
          }`}
        >
          {isVerificationSuccess
            ? "Test success. Please Save to continue."
            : "Test Failed"}
        </div>
      );
    } else {
      return null;
    }
  };

  onSynchronizeNowConnectwise = () => {
    this.props.syncCWData().then((action) => {
      if (action.type === SYNC_CW_DATA_SUCCESS) {
        this.props.addSuccessMessage("Connectwise Sync has started!");
      } else if (action.type === SYNC_CW_DATA_FAILURE) {
        this.props.addErrorMessage("Error on Sync.");
      }
    });
  };

  onSynchronizeNowExternal = () => {
    this.props.syncExternal().then((action) => {
      if (action.type === SYNC_CW_DATA_SUCCESS) {
        this.props.addSuccessMessage("External Sync has started!");
      } else if (action.type === SYNC_CW_DATA_FAILURE) {
        this.props.addErrorMessage("Error on Sync.");
      }
    });
  };

  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="settings col-md-12">
        {this.renderTopBar()}
        <div className="setting-tab-body col-md-10">
          {currentPage.pageType === PageType.INTEGRATION &&
            this.renderNotification()}
          {currentPage.pageType === PageType.INTEGRATION &&
            this.renderIntegration(
              currentPage.integrationIndex,
              currentPage.typeIndex
            )}
          {currentPage.pageType === PageType.BOARD_MAPPING && (
            <BoardMapping
              boardData={
                this.props.providerExtraConfigs &&
                this.props.providerExtraConfigs.board_mapping
              }
              onSave={this.onBoardMappingSave}
              boardSavedMappings={this.getSavedBoardMappings()}
              fetchProviderBoardStatus={this.getBoardMapping}
              fetchProviderBoardStatusManaged={this.getBoardMappingManaged}
              addWarningMessage={this.props.addWarningMessage}
              isFetchingStatus={this.props.isFetchingStatus}
            />
          )}
          {currentPage.pageType === PageType.DEVICE_MAPPING && (
            <HorizontalTabSlider>
              <Tab title="Device Category Mapping">
                <DeviceMapping
                  deviceCategories={
                    this.props.providerExtraConfigs &&
                    this.props.providerExtraConfigs.device_categories
                  }
                  savedDeviceCategories={this.getSavedDeviceMappings()}
                  manufacturers={this.props.manufacturers}
                  onSubmit={this.onDeviceMappingSave}
                  integrations={this.props.manufacturerIntegrations}
                />
              </Tab>
              <Tab title="Software Category Mapping">
                <SoftwareMapping
                  deviceCategories={
                    this.props.providerExtraConfigs &&
                    this.props.providerExtraConfigs.device_categories
                  }
                  savedDeviceCategories={this.getSavedSoftwareMappings()}
                  manufacturers={this.props.manufacturers}
                  onSubmit={this.onSoftwareMappingSave}
                  integrations={this.props.manufacturerIntegrations}
                />
              </Tab>

              <Tab title="Device Exclusion Filter">
                <DeviceFilters
                  deviceFilters={this.getSavedDeviceFilters()}
                  onSubmit={this.onDeviceFilterSave}
                />
              </Tab>
              <Tab title="Empty Serial Mapping">
                <EmptySerialMapping
                  deviceCategories={
                    this.props.providerExtraConfigs &&
                    this.props.providerExtraConfigs.device_categories
                  }
                  loading={
                    this.state.loadingEmptySerial || this.props.isFetching > 0
                  }
                  device_category_ids={
                    this.state.providerIntegration &&
                    this.state.providerIntegration.other_config &&
                    this.state.providerIntegration.other_config
                      .empty_serial_mapping &&
                    this.state.providerIntegration.other_config
                      .empty_serial_mapping.device_category_ids
                  }
                  onSubmit={this.onEmptySerialMappingSave}
                />
              </Tab>
            </HorizontalTabSlider>
          )}
          {currentPage.pageType === PageType.SYNCHRONIZE && (
            <Synchronize
              {...this.props}
              syncNowConnectwise={this.onSynchronizeNowConnectwise}
              syncNowExternal={this.onSynchronizeNowExternal}
              configStatus={this.props.configStatus}
              periodicSync={
                this.props.providerIntegration.other_config
                  ? this.props.providerIntegration.other_config
                      .cw_device_data_periodic_sync_enabled
                  : null
              }
            />
          )}
          {currentPage.pageType === PageType.PRODUCTMAPPING && (
            <ProductMapping />
          )}
          {currentPage.pageType === PageType.ROLEMAPPING && (
            <>
              <RoleRateMapping url={"pmo/settings/project-rate-mappings"} />
            </>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  publicKey: state.setting.publicKey,
  integrations: state.integration.integrations,
  integrationVerified: state.integration.integrationVerified,
  providerIntegration: state.providerIntegration.providerIntegration,
  allProviderIntegrations: state.providerIntegration.allProviderIntegrations,
  providerExtraConfigs: state.providerIntegration.providerExtraConfigs,
  configStatus: state.providerIntegration.configStatus,
  boardStatusList: state.providerIntegration.boardStatusList,
  boardManagedStatusList: state.providerIntegration.boardManagedStatusList,
  isFetchingStatus: state.providerIntegration.isFetchingStatus,
  isFetching: state.providerIntegration.isFetching,
  integrationsCatgories: state.integration.integrationsCatgories,
  manufacturers: state.inventory.manufacturers,
  manufacturerIntegrations: state.integration.manufacturerIntegrations,
  isFetchingExtraConfig: state.providerIntegration.isFetchingExtraConfig,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPublicKey: () => dispatch(fetchEncryptionPublicKey()),
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
  fetchProviderBoardStatus: (id: number) =>
    dispatch(fetchProviderBoardStatus(id)),
  fetchProviderBoardStatusManaged: (id: number) =>
    dispatch(fetchProviderBoardStatusManaged(id)),
  fetchIntegrationsCategoriesTypes: (id: number) =>
    dispatch(fetchIntegrationsCategoriesTypes(id)),
  testIntegration: (
    catId: number,
    typeId: number,
    integration: Array<{ [name: string]: string }>
  ) => dispatch(testIntegration(catId, typeId, integration)),
  createProviderIntegrations: (
    providerIntegration: IProviderIntegrationRequest
  ) => dispatch(createProviderIntegrations(providerIntegration)),
  fetchProviderExtraConfigs: (providerId: number) =>
    dispatch(fetchProviderExtraConfigs(providerId)),
  fetchAllProviderIntegrations: () => dispatch(fetchAllProviderIntegrations()),
  fetchProviderIntegrations: (providerId: number) =>
    dispatch(fetchProviderIntegrations(providerId)),
  updateProviderIntegration: (
    providerId: number,
    providerIntegration: IProviderIntegration
  ) => dispatch(updateProviderIntegration(providerId, providerIntegration)),
  fetchIntegrationsCategories: () => dispatch(fetchIntegrationsCategories()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchManufacturerIntegrations: (id: number) =>
    dispatch(fetchManufacturerIntegrations(id)),
  syncCWData: () => dispatch(syncCWData()),
  syncExternal: () => dispatch(syncExternal()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addWarningMessage: (message: string) => dispatch(addWarningMessage(message)),
  updateEmptySerialNumber: (providerId: number, providerIntegration: any) =>
    dispatch(updateEmptySerialNumber(providerId, providerIntegration)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Setting);
