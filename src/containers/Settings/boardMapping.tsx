import React from "react";

import _get from "lodash/get";
import _set from "lodash/set";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import "./style.scss";

interface IBoardMappingProps {
  boardData: IFieldMapping;
  boardSavedMappings: any;
  isFetchingStatus: boolean;
  onSave: (boardMapping: any) => void;
  addWarningMessage: TShowWarningMessage;
  fetchProviderBoardStatus: (id: number, key: string) => void;
  fetchProviderBoardStatusManaged: (id: number, key: string) => void;
}

interface IBoardMappingState {
  isEdit: boolean;
  isValid: boolean;
  boardMapping: any;
  showError: boolean;
  showNotification: boolean;
}

class BoardMapping extends React.Component<
  IBoardMappingProps,
  IBoardMappingState
> {
  constructor(props: IBoardMappingProps) {
    super(props);

    this.state = {
      isEdit: true,
      isValid: false,
      showError: false,
      boardMapping: null,
      showNotification: true,
    };
  }

  componentDidMount() {
    if (this.props.boardData) {
      const boardMapping = this.getBoardMappingInitState(this.props); // tslint:disable-line
      let idManaged = _get(
        boardMapping,
        `Service Board Mapping.Managed Services.board.options`
      );
      let id = _get(
        boardMapping,
        `Service Board Mapping.Professional Services.board.options`
      );
      if (id && Object.keys(id)[0] !== undefined) {
        id = Object.keys(id)[0];
        this.props.fetchProviderBoardStatus(
          id,
          "Service Board Mapping.Professional Services.board"
        );
      }
      let idSmartNet = _get(
        boardMapping,
        `Service Board Mapping.Smartnet Services.board.options`
      );
      setTimeout(() => {
        if (idSmartNet && Object.keys(idSmartNet)[0] !== undefined) {
          idSmartNet = Object.keys(idSmartNet)[0];
          this.props.fetchProviderBoardStatus(
            idSmartNet,
            "Service Board Mapping.Smartnet Services.board"
          );
        } // tslint:disable-next-line:align
      }, 3000);

      if (idManaged && Object.keys(idManaged)[0] !== undefined) {
        idManaged = Object.keys(idManaged)[0];
        this.props.fetchProviderBoardStatusManaged(
          idManaged,
          "Service Board Mapping.Managed Services.board"
        );
      }
      this.setState({
        boardMapping,
        isEdit: !this.props.boardSavedMappings,
        showNotification: !this.props.boardSavedMappings,
      });
    }
  }

  componentDidUpdate(prevProps: IBoardMappingProps) {
    if (prevProps.boardSavedMappings !== this.props.boardSavedMappings) {
      const boardMapping = this.getBoardMappingInitState(this.props); // tslint:disable-line
      this.setState({
        boardMapping,
        isEdit: !this.props.boardSavedMappings,
        showNotification: !this.props.boardSavedMappings,
      });
    }
  }

  // init board mapping
  getBoardMappingInitState = (props: IBoardMappingProps) => {
    const boardData = props.boardData;
    if (!boardData) {
      return null;
    }

    return Object.keys(boardData).reduce((accumulator, key, index) => {
      const mappingData = this.props.boardData[key];
      accumulator[key] = this.getMappingInitState(key, mappingData, props);
      return accumulator;
    }, {}); // tslint:disable-line
  };

  getMappingInitState = (
    mappingDataKey: string,
    mappingData: IBoardMappingObj,
    props: IBoardMappingProps
  ) => {
    const hasOptions =
      Object.keys(mappingData).findIndex((key) => key === "options") !== -1;

    if (hasOptions) {
      if (props.boardSavedMappings) {
        const optionsArray = _get(props.boardSavedMappings, mappingDataKey);

        if (!Array.isArray(optionsArray)) {
          return { value: optionsArray };
        }

        const optionsMapped = optionsArray
          ? {
              options: optionsArray.reduce((initOptions, id) => {
                initOptions[id] = true;

                return initOptions;
              }, {}), // tslint:disable-line
            }
          : { options: {} };

        return optionsMapped;
      } else {
        return { options: {} };
      }
    } else {
      return Object.keys(mappingData).reduce((accumulator, key, index) => {
        const nestedData = mappingData[key];

        if (
          nestedData.type === "RADIOBOX" &&
          nestedData.options.length === 0 &&
          key !== "ticket_closed_status" &&
          key !== "smartnet_complete_status"
        ) {
          accumulator[key] = { options: {} };
        } else {
          accumulator[key] = this.getMappingInitState(
            `${mappingDataKey}.${key}`,
            nestedData,
            props
          );
        }

        return accumulator;
      }, {}); // tslint:disable-line
    }
  };

  // get save data
  getBoardSaveData = () => {
    if (!this.state.boardMapping) {
      return null;
    }

    return Object.keys(this.state.boardMapping).reduce(
      (accumulator, key, index) => {
        const mappingData = this.state.boardMapping[key];
        accumulator[key] =
          key === "Custom Fields"
            ? this.getSaveData(key, mappingData, true)
            : this.getSaveData(key, mappingData, false);

        return accumulator;
      },
      {}
    );
  };

  getSaveData = (mappingDataKey: string, mappingData: any, custom: boolean) => {
    const hasOptions =
      Object.keys(mappingData).findIndex((key) => key === "options") !== -1;
    if (custom) {
      return Object.keys(mappingData).reduce((accumulator, key, index) => {
        accumulator[key] = mappingData[key].value;

        return accumulator;
      }, {}); // tslint:disable-line
    } else {
      if (hasOptions) {
        // set empty options
        const options = mappingData.options || {};
        const savedOptions = Object.keys(options)
          .filter((key) => options[key] === true)
          .map((filteredOption) => parseInt(filteredOption, 10));

        return savedOptions;
      } else {
        return Object.keys(mappingData).reduce((accumulator, key, index) => {
          const nestedData = mappingData[key];

          accumulator[key] =
            nestedData && this.getSaveData(key, nestedData, false);

          return accumulator;
        }, {}); // tslint:disable-line
      }
    }
  };

  // edit toggles
  cancelEdit = () => {
    this.setState({
      isEdit: false,
      boardMapping: this.getBoardMappingInitState(this.props),
      showError: false,
    });
  };

  onSave = () => {
    if (this.checkValidaBoards()) {
      const saveData = this.getBoardSaveData();

      this.props.onSave(saveData);
    }
  };

  checkValidaBoards = () => {
    const saveData = this.getBoardSaveData();
    const boardData = this.props.boardData;
    let isValid = true;

    if (saveData) {
      Object.keys(boardData).map((key, index) => {
        const boardContainerData = boardData[key];
        {
          Object.keys(boardContainerData).map((k, i) => {
            const hasOptions =
              Object.keys(boardContainerData[k]).findIndex(
                (v) => v === "options"
              ) !== -1;
            if (boardContainerData[k].type !== "TEXTBOX") {
              if (hasOptions) {
                if (boardContainerData[k].is_required) {
                  if (saveData[key] && saveData[key][k]) {
                    if (saveData[key][k].length === 0) {
                      isValid = false;
                    }
                  } else {
                    isValid = false;
                  }
                }
              } else {
                {
                  Object.keys(boardContainerData[k]).map((l, j) => {
                    if (boardContainerData[k][l].is_required) {
                      if (
                        saveData[key] &&
                        saveData[key][k] &&
                        saveData[key][k][l]
                      ) {
                        if (saveData[key][k][l].length === 0) {
                          isValid = false;
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  });
                }
              }
            } else {
              if (boardContainerData[k].is_required) {
                if (saveData[key] && saveData[key][k]) {
                  if (saveData[key][k].length === 0) {
                    isValid = false;
                  }
                } else {
                  isValid = false;
                }
              }
            }
          });
        }
      });
    } else {
      isValid = false;
    }

    this.setState({ isValid, showError: !isValid });
    if (!isValid) {
      this.props.addWarningMessage("Please complete all required fields!");
    }
    return isValid;
  };

  onRadioBoxChanged = (event: any, key: string) => {
    if (
      key === "Service Board Mapping.Professional Services.board" ||
      key === "Service Board Mapping.Smartnet Services.board" ||
      key === "Service Board Mapping.Managed Services.board"
    ) {
      this.props.fetchProviderBoardStatus(Number(event.target.value), key);
    }
    const prevBoardMapping = this.state.boardMapping;
    const newOptionsForKey = {
      [event.target.value]: true,
    };
    _set(prevBoardMapping, key + ".options", newOptionsForKey);
    const getkey = key.replace(".board", "");
    _set(prevBoardMapping, getkey + ".closed_status.options", null);
    _set(prevBoardMapping, getkey + ".open_status.options", null);
    _set(prevBoardMapping, getkey + ".ticket_closed_status.options", null);
    _set(prevBoardMapping, getkey + ".smartnet_complete_status.options", null);

    this.setState({
      boardMapping: { ...prevBoardMapping },
    });
  };

  onMultiSelectChanged = (event: any, key: string) => {
    if (
      key === "Service Board Mapping.Professional Services.board" ||
      key === "Service Board Mapping.Smartnet Services.board" ||
      key === "Service Board Mapping.Managed Services.board"
    ) {
      this.props.fetchProviderBoardStatus(event.target.value, key);
    }
    const prevBoardMapping = this.state.boardMapping;
    const newOptionsForKey = {};
    event.target.value.map((x) => {
      newOptionsForKey[x] = true;
    });
    _set(prevBoardMapping, key + ".options", newOptionsForKey);
    const getkey = key.replace(".board", "");
    _set(prevBoardMapping, getkey + ".closed_status.options", null);
    _set(prevBoardMapping, getkey + ".open_status.options", null);
    _set(prevBoardMapping, getkey + ".ticket_closed_status.options", null);
    _set(prevBoardMapping, getkey + ".smartnet_complete_status.options", null);

    this.setState({
      boardMapping: { ...prevBoardMapping },
    });
  };

  onInputChanged = (event: any, key) => {
    const prevBoardMapping = this.state.boardMapping;
    _set(prevBoardMapping, key + ".value", event.target.value);
    this.setState({
      boardMapping: { ...prevBoardMapping },
    });
  };

  getvalue = (concatenattedKey: string, isMulti: boolean) => {
    let valueSelected = _get(
      this.state.boardMapping,
      `${concatenattedKey}.options`
    );
    if (valueSelected) {
      valueSelected = isMulti
        ? Object.keys(valueSelected)
        : Object.keys(valueSelected)[0];
    }

    return isMulti ? valueSelected : parseInt(valueSelected, 10);
  };

  getInputValue = (concatenattedKey: string) => {
    const valueSelected = _get(
      this.state.boardMapping,
      `${concatenattedKey}.value`
    );

    return valueSelected;
  };

  // render methods
  renderMapping = (
    mappingDataKey: string,
    mappingData: any,
    concatenattedKey: string
  ) => {
    const keys = Object.keys(mappingData);
    const hasOptions = keys.findIndex((key) => key === "options") !== -1;
    if (hasOptions) {
      const title = mappingData.label;
      const options = mappingData.options;
      const type = mappingData.type;
      if (type === "CHECKBOXES" && Array.isArray(options)) {
        return (
          <Input
            field={{
              value: this.getvalue(concatenattedKey, true),
              label: `${title}`,
              type: "PICKLIST",
              isRequired: mappingData.is_required,
              options: options.map((role) => ({
                value: role.id,
                label: role.label,
              })),
            }}
            disabled={!this.state.isEdit}
            error={
              Boolean(
                this.getvalue(concatenattedKey, true) &&
                  this.getvalue(concatenattedKey, true).length === 0 &&
                  mappingData.is_required &&
                  this.state.showError
              )
                ? {
                    errorState: "error",
                    errorMessage: "Required",
                  }
                : undefined
            }
            multi={true}
            width={6}
            name={concatenattedKey}
            onChange={(e) => this.onMultiSelectChanged(e, concatenattedKey)}
          />
        );
      } else if (type === "RADIOBOX" && Array.isArray(options)) {
        return (
          <Input
            field={{
              value: this.getvalue(concatenattedKey, false),
              label: `${title}`,
              type: "PICKLIST",
              isRequired: mappingData.is_required,
              options: options.map((role: IDLabelObject) => ({
                value: role.id,
                label: role.label,
              })),
            }}
            disabled={!this.state.isEdit}
            error={
              Boolean(
                this.getvalue(concatenattedKey, true) === undefined &&
                  mappingData.is_required &&
                  this.state.showError
              )
                ? {
                    errorState: "error",
                    errorMessage: "Required",
                  }
                : undefined
            }
            width={6}
            clearable={false}
            name={concatenattedKey}
            onChange={(e) => this.onRadioBoxChanged(e, concatenattedKey)}
          />
        );
      } else if (type === "TEXTBOX" && options.length === 0) {
        return (
          <div key={mappingDataKey} className="board-mapping-input-text">
            <Input
              field={{
                value: this.getInputValue(concatenattedKey),
                label: title,
                type: "TEXT",
                isRequired: mappingData.is_required,
              }}
              disabled={!this.state.isEdit}
              width={12}
              name={concatenattedKey}
              onChange={(e) => this.onInputChanged(e, concatenattedKey)}
            />
          </div>
        );
      } else {
        return false;
      }
    } else {
      return (
        <div
          key={mappingDataKey}
          className="board-mapping__options-container--nested"
        >
          <h3>{mappingDataKey}</h3>
          <div className="board-mapping__container-inner">
            {keys.map((key, index) => {
              if (index % 2 === 0) {
                const nestedData = mappingData[key];
                // If last odd element
                if (index + 1 === Object.keys(mappingData).length) {
                  return (
                    <div className="inner-row" key={index}>
                      {this.renderMapping(
                        key,
                        nestedData,
                        `${concatenattedKey}.${key}`
                      )}
                    </div>
                  );
                } else {
                  // Access the data at odd index
                  const nextData = mappingData[keys[index + 1]];
                  return (
                    <div className="inner-row" key={index}>
                      {this.renderMapping(
                        key,
                        nestedData,
                        `${concatenattedKey}.${key}`
                      )}
                      {this.renderMapping(
                        keys[index + 1],
                        nextData,
                        `${concatenattedKey}.${keys[index + 1]}`
                      )}
                    </div>
                  );
                }
              } else {
                return null;
              }
            })}
          </div>
        </div>
      );
    }
  };

  renderBoardContainer = (title: string, data: any) => {
    return (
      <div
        className={`board-mapping__container-contents col-md-12 ${
          title === "Service Board Mapping" ? "services" : "mapping"
        }`}
      >
        {Object.keys(data).map((key, index) => {
          const mappingData = data[key];

          return this.renderMapping(key, mappingData, `${title}.${key}`);
        })}
      </div>
    );
  };

  renderActionBar = () => {
    const isEdit = this.state.isEdit;
    const setEditState = () =>
      this.setState({
        isEdit: true,
      });

    return (
      <div className="board-mapping__actions">
        {!isEdit ? (
          <SquareButton
            content="Edit"
            onClick={setEditState}
            bsStyle={"primary"}
          />
        ) : (
          [
            <SquareButton
              key={2}
              content="Cancel"
              onClick={this.cancelEdit}
              bsStyle={"default"}
            />,
            <SquareButton
              key={3}
              content="Save"
              onClick={this.onSave}
              bsStyle={"primary"}
            />,
          ]
        )}
      </div>
    );
  };

  renderNotification = () => {
    const onNotificationClick = (e) =>
      this.setState({
        showNotification: false,
      });

    if (this.state.showNotification) {
      return (
        <div
          onClick={onNotificationClick}
          className="board-mapping__notification"
        >
          Please fill the board mapping to proceed.
        </div>
      );
    } else {
      return null;
    }
  };

  render() {
    const boardData = this.props.boardData;

    const isEdit = this.state.isEdit;

    return (
      <div
        className={`board-mapping ${!isEdit ? "board-mapping--disabled" : ""}`}
      >
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingStatus ||
              this.props.boardData === null ||
              this.props.boardData === undefined
                ? true
                : false
            }
          />
        </div>
        {this.renderNotification()}
        {boardData && (
          <HorizontalTabSlider className={"field-mapping-tabs"}>
            {Object.keys(boardData).map((boardKey, index) => {
              return (
                <Tab key={index} title={boardKey}>
                  {this.renderBoardContainer(boardKey, boardData[boardKey])}
                </Tab>
              );
            })}
          </HorizontalTabSlider>
        )}
        {this.renderActionBar()}
      </div>
    );
  }
}

export default BoardMapping;
