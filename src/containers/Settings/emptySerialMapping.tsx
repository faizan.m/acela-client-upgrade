import React from "react";

import SquareButton from "../../components/Button/button";
import Select from "../../components/Input/Select/select";
import Spinner from "../../components/Spinner";

interface IEmptySerialMappingProps {
  deviceCategories: IDeviceCategory[];
  onSubmit: any;
  device_category_ids: number[];
  loading: boolean;
}

interface IEmptySerialMappingState {
  device_category_ids: number[];
}

export default class EmptySerialMapping extends React.Component<
  IEmptySerialMappingProps,
  IEmptySerialMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: IEmptySerialMappingProps) {
    super(props);
    this.state = {
      device_category_ids: props.device_category_ids,
    };
  }
  onSubmit = () => {
    this.props.onSubmit(this.state.device_category_ids);
  };

  deviceCategoryList = () => {
    let deviceCategories = this.props.deviceCategories
      ? this.props.deviceCategories.map((category) => ({
          value: category.category_id,
          label: category.category_name,
          disabled: false,
        }))
      : [];
    return deviceCategories;
  };

  renderCategoryMapping = () => {
    return (
      <div className="device-mapping__options">
        <div className="col-md-12 mapping-row">
          <div className="field-section default-category col-md-8 col-xs-8">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Category
              </label>
              <span className="field__label-required" />
            </div>
            <div className={``}>
              <Select
                name="device_category_id"
                value={this.state.device_category_ids}
                onChange={(e) =>
                  this.setState({ device_category_ids: e.target.value })
                }
                options={this.deviceCategoryList()}
                multi={true}
                searchable={true}
                placeholder="Select Category"
              />
            </div>
          </div>
          <div className="field-section  col-md-4 col-xs-4" />

          <div className="field-section  col-md-1 col-xs-1" />
        </div>
      </div>
    );
  };
  isLoading = () => {
    return this.props.deviceCategories === null ||
      this.props.deviceCategories === undefined
      ? true
      : false || this.props.loading;
  };
  render() {
    return (
      <div className="category-mapping-container">
        <div className="loader">
          <Spinner show={this.isLoading()} />
        </div>

        {this.renderCategoryMapping()}
        <div className="action-btns">
          <SquareButton
            content="Save"
            onClick={this.onSubmit}
            className="save-mapping"
            bsStyle={"primary"}
            disabled={this.isLoading()}
          />
        </div>
      </div>
    );
  }
}
