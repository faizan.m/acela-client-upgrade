import { cloneDeep } from 'lodash';
import React from 'react';

import { connect } from 'react-redux';
import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import { FETCH_COMM_TYPES_SUCCESS, fetCommunicationTypes, getCommunicationData, saveCommunication, SAVE_COMM_SUCCESS } from '../../actions/setting';
import Select from '../../components/Input/Select/select';
import Spinner from '../../components/Spinner';
import './style.scss';

interface ICommunicationProps {
  addSuccessMessage: any;
  addErrorMessage: any;
  fetCommunicationTypes: any;
  saveCommunication: any;
  getCommunicationData: any;
}

interface ICommunicationState {
  error: any;
  types: any[];
  communication: any[],
  loading: boolean;
  listLoading: boolean;
}

class Communication extends React.Component<
  ICommunicationProps,
  ICommunicationState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  constructor(props: ICommunicationProps) {
    super(props);

    this.state = {
      types: [],
      communication: [],
      error: {},
      loading: false,
      listLoading: false,
    };
  }
  componentDidMount() {
    this.fetCommunicationTypes();
    this.getCommunicationData();
  }

  fetCommunicationTypes = () => {
    this.setState({ listLoading: true });
    this.props.fetCommunicationTypes('get',).then((action) => {
      if (action.type === FETCH_COMM_TYPES_SUCCESS) {
        this.setState({
          listLoading: false,
          types: action.response,
        })
      }
    })
  };

  getCommunicationData = () => {
    this.setState({ loading: true });
    this.props.getCommunicationData().then((action) => {
      if (action.type === SAVE_COMM_SUCCESS) {
        this.setState({
          loading: false,
          communication: Object.entries(action.response).filter(x=>x[0] !== 'last_updated_on'),
        })
      }
    })
  };

  
  saveCommunicationData = () => {
    this.setState({ loading: true });
    const dic ={};
    this.state.communication.filter(x=>x[0] !== 'last_updated_on').map(x=>{
      dic[x[0]] = x[1]
    })
    this.props.saveCommunication(dic).then((action) => {
      if (action.type === SAVE_COMM_SUCCESS) {
        this.setState({
          loading: false,
        })
        this.props.addSuccessMessage('Updated...')
      }
    })
  };

  renderCommunicationTypesMapping = () => {
    const communication = this.state.communication;

    return (
      <div className="terretory-mapping__options row ">
        <div className="col-md-12 col-xs-12 mapping-row heading">
          <div className="terretory-name arrow col-md-4 col-xs-4">
            Communication via
          </div>

          <div className="field-section col-md-3 col-xs-3">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Type
              </label>
            </div>
          </div>
        </div>
        {communication &&
          communication.map((x, index) => (
            <div className="col-md-12 mapping-row" key={index}>
              <div className="terretory-name arrow col-md-4 col-xs-4">
                {x[0].replace(/_/g, ' ')}
              </div>

              <div className="field-section col-md-3 col-xs-3">
                <div
                  className={``}
                >
                  <Select
                    name="Type"
                    value={x[1]}
                    onChange={e => {
                        const newState = cloneDeep(this.state);
                        (newState.communication[index][1] as any) = e.target.value;
                        this.setState(newState,()=>{
                          this.saveCommunicationData()
                        });
                    }}
                    options={this.state.types.map(s => ({
                      value: s.id,
                      label: s.description,
                    }))}
                    multi={false}
                    searchable={true}
                    placeholder="Select Type"
                    loading={this.state.listLoading}
                  />
                </div>
              </div>
            </div>
          ))}
      </div>
    );
  };

  render() {
    return (
      <div className="terretory-mapping">
        <div className="loader">
          <Spinner
            show={
              this.state.loading || this.state.listLoading
            }
          />
        </div>
        <div className="col-md-12 col-x row mapping-row heading">
          <div className="model-heading arrow">
            <div className="heading-text"> Communication Setting</div>
          </div>
        </div>
        {this.renderCommunicationTypesMapping()}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetCommunicationTypes: () => dispatch(fetCommunicationTypes()),
  getCommunicationData: () => dispatch(getCommunicationData()),
  saveCommunication: (data: any) => dispatch(saveCommunication(data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Communication);
