import React from "react";

import { cloneDeep } from "lodash";
import SquareButton from "../../components/Button/button";
import DeleteButton from "../../components/Button/deleteButton";
import Select from "../../components/Input/Select/select";
import Spinner from "../../components/Spinner";

interface IDeviceMappingProps {
  deviceCategories: IDeviceCategory[];
  savedDeviceCategories: number[];
  onSubmit: (selectedCategories: any) => void;
  manufacturers: any[];
  integrations: any[];
}

interface IDeviceMappingState {
  mappingList: ICategoryManufaturerMapping[];
  device_category_id: number;
  error: {
    device_category_id: IFieldValidation;
    device_category_row: IFieldValidation;
  };
}

export default class DeviceMapping extends React.Component<
  IDeviceMappingProps,
  IDeviceMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: IDeviceMappingProps) {
    super(props);
    const mapping =
      this.props.savedDeviceCategories &&
      Object.keys(this.props.savedDeviceCategories);
    const mappingList = [];
    let deviceCategoryId = null;
    if (mapping) {
      mapping.map((data) => {
        if (data !== "default") {
          mappingList.push({
            device_category_id: this.props.savedDeviceCategories[data]
              .device_category_id,
            device_category_name: this.props.savedDeviceCategories[data]
              .device_category_name,
            manufacturer_id: parseInt(data, 10),
            cw_mnf_name: this.props.savedDeviceCategories[data].cw_mnf_name,
            acela_integration_type_id: this.props.savedDeviceCategories[data]
              .acela_integration_type_id,
          });
        } else {
          deviceCategoryId = parseInt(
            this.props.savedDeviceCategories[data].device_category_id,
            10
          );
        }
      });
    }

    this.state = {
      mappingList,
      device_category_id: deviceCategoryId,
      error: {
        device_category_id: { ...DeviceMapping.emptyErrorState },
        device_category_row: { ...DeviceMapping.emptyErrorState },
      },
    };
  }

  addNew = () => {
    const newState = cloneDeep(this.state);
    newState.mappingList.unshift({
      device_category_id: null,
      device_category_name: "",
      manufacturer_id: null,
      cw_mnf_name: "",
      acela_integration_type_id: null,
    });

    this.setState(newState);
  };

  onSubmit = () => {
    const mappedObject = {};
    const d = "default";
    if (this.isValid()) {
      this.state.mappingList.map((data, i) => {
        mappedObject[data.manufacturer_id] = data;
      });
      mappedObject[d] = { device_category_id: this.state.device_category_id };
      this.props.onSubmit(mappedObject);
    }
  };

  handledeviceCategoryChange = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.mappingList[index][e.target.name] = e.target.value;
    if (e.target.name === "device_category_id") {
      this.props.deviceCategories.map((category) => {
        if (e.target.value === category.category_id) {
          newState.mappingList[index].device_category_name =
            category.category_name;
        }
      });
    } else if (e.target.name === "manufacturer_id") {
      this.props.manufacturers.map((data) => {
        if (e.target.value === data.id) {
          newState.mappingList[index].cw_mnf_name = data.label;
        }
      });
    }
    this.setState(newState);
  };

  handleChangeDefault = (e) => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  onDeleteRowClick = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.mappingList.splice(index, 1);
    this.setState(newState);
  };

  deviceCategoryList = () => {
    let deviceCategories = this.props.deviceCategories
      ? this.props.deviceCategories.map((category) => ({
          value: category.category_id,
          label: category.category_name,
          disabled: false,
        }))
      : [];

    deviceCategories = deviceCategories.map((data) => ({
      value: data.value,
      label: data.label,
      disabled:
        !!this.state.mappingList.find(
          (e) => data.value === e.device_category_id
        ) || data.value === this.state.device_category_id,
    }));

    return deviceCategories;
  };

  manufacturers = () => {
    let manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((item) => ({
          value: item.id,
          label: item.label,
          disabled: false,
        }))
      : [];

    manufacturers = manufacturers.map((data) => ({
      value: data.value,
      label: data.label,
      disabled: !!this.state.mappingList.find(
        (e) => data.value === e.manufacturer_id
      ),
    }));

    return manufacturers;
  };

  integrations = () => {
    const integrations = this.props.integrations
      ? this.props.integrations.map((e) => ({
          value: e.id,
          label: e.name,
          disabled: false,
        }))
      : [];

    return integrations;
  };

  isValid = () => {
    const error = {
      device_category_id: { ...DeviceMapping.emptyErrorState },
      device_category_row: { ...DeviceMapping.emptyErrorState },
    };
    let isValid = true;

    if (!this.state.device_category_id) {
      error.device_category_id.errorState = "error";
      error.device_category_id.errorMessage = "Please select default category.";

      isValid = false;
    }

    if (this.state.mappingList && this.state.mappingList.length > 0) {
      this.state.mappingList.map((e) => {
        if (!e.device_category_id || !e.manufacturer_id) {
          error.device_category_row.errorState = "error";
          // tslint:disable-next-line:max-line-length
          error.device_category_row.errorMessage = `Please select required fields.`;
          isValid = false;
        }
      });
    }
    this.setState({
      error,
    });

    return isValid;
  };

  renderCategoryManufaturerMapping = () => {
    const mappingList = this.state.mappingList;

    return (
      <div className="device-mapping__options">
        <div className="col-md-12 mapping-row">
          <div className="field-section default-category col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Default Device Category
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.device_category_id.errorMessage
                  ? `error-input`
                  : ""
              }`}
            >
              <Select
                name="device_category_id"
                value={this.state.device_category_id}
                onChange={(e) => this.handleChangeDefault(e)}
                options={this.deviceCategoryList()}
                multi={false}
                searchable={true}
                placeholder="Select Category"
              />
            </div>
            {this.state.error.device_category_id.errorMessage && (
              <div className="select-manufacturer-error">
                {this.state.error.device_category_id.errorMessage}
              </div>
            )}
          </div>
          <SquareButton
            content={
              <>
                <span className="add-plus">+</span>
                <span className="add-text">Add</span>
              </>
            }
            onClick={this.addNew}
            bsStyle={"link"}
            className="add-new-mapping add-btn"
          />
        </div>

        {mappingList &&
          mappingList.map((category, index) => (
            <div className="col-md-12 mapping-row" key={index}>
              <div className="category-select-fields">
                <div className="field-section arrow col-md-4 col-xs-4">
                  <div className="field__label row">
                    <label className="field__label-label" title="">
                      Select Manufacturer
                    </label>
                    <span className="field__label-required" />
                  </div>
                  <div>
                    <Select
                      name="manufacturer_id"
                      value={this.state.mappingList[index].manufacturer_id}
                      isOptionDisabled={(option) => option.disabled === true}
                      onChange={(e) =>
                        this.handledeviceCategoryChange(e, index)
                      }
                      options={this.manufacturers()}
                      multi={false}
                      searchable={true}
                      placeholder="Select Manufacturer"
                    />
                  </div>
                </div>
                <div className="field-section arrow  col-md-4 col-xs-4">
                  <div className="field__label row">
                    <label className="field__label-label" title="">
                      Select Acela Integration Type
                    </label>
                  </div>
                  <div>
                    <Select
                      name="acela_integration_type_id"
                      value={
                        this.state.mappingList[index].acela_integration_type_id
                      }
                      onChange={(e) =>
                        this.handledeviceCategoryChange(e, index)
                      }
                      options={this.integrations().concat([
                        {
                          value: "",
                          label: "None",
                          disabled: false,
                        },
                      ])}
                      multi={false}
                      searchable={true}
                      placeholder="Select Acela Integration Type"
                    />
                  </div>
                </div>
                <div className="field-section col-md-4 col-xs-4">
                  <div className="field__label row">
                    <label className="field__label-label" title="">
                      Select Device Category
                    </label>
                    <span className="field__label-required" />
                  </div>
                  <div>
                    <Select
                      name="device_category_id"
                      value={this.state.mappingList[index].device_category_id}
                      onChange={(e) =>
                        this.handledeviceCategoryChange(e, index)
                      }
                      options={this.deviceCategoryList()}
                      isOptionDisabled={(option) => option.disabled === true}
                      multi={false}
                      searchable={true}
                      placeholder="Select Category"
                    />
                  </div>
                </div>
              </div>
              <DeleteButton onClick={(e) => this.onDeleteRowClick(e, index)} />
            </div>
          ))}
        {this.state.mappingList &&
          this.state.mappingList.length > 0 &&
          this.state.error.device_category_row.errorMessage && (
            <div className="select-manufacturer-error error-for-row">
              {this.state.error.device_category_row.errorMessage}
            </div>
          )}
      </div>
    );
  };

  render() {
    return (
      <div className="category-mapping-container">
        <div className="loader">
          <Spinner
            show={
              this.props.deviceCategories === null ||
              this.props.deviceCategories === undefined
                ? true
                : false
            }
          />
        </div>

        {this.renderCategoryManufaturerMapping()}
        <div className="action-btns">
          <SquareButton
            content="Save"
            onClick={this.onSubmit}
            className="save-mapping"
            bsStyle={"primary"}
          />
        </div>
      </div>
    );
  }
}
