import React from "react";

import { cloneDeep, debounce } from "lodash";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import {
  addModelMapping,
  deleteModelMapping,
  fetchModelMappings,
  saveModelMapping,
  UPDATE_MODELS_SUCCESS,
  UPDATE_MODELS_FAILURE,
} from "../../actions/subscription";
import SquareButton from "../../components/Button/button";
import DeleteButton from "../../components/Button/deleteButton";
import EditButton from "../../components/Button/editButton";
import Input from "../../components/Input/input";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import { commonFunctions } from "../../utils/commonFunctions";
import IconButton from "../../components/Button/iconButton";
import "../../commonStyles/inputTagMapping.scss";
import "./style.scss";

interface IModelMappingProps {
  modelMappings: any;
  isFetching: any;
  fetchModelMappings: any;
  saveModelMapping: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  addModelMapping: any;
  deleteModelMapping: any;
}

interface IModelMappingState {
  modelMappings: any[];
  openCisco: boolean;
  openNonCisco: boolean;
  type: string;
  model_ids: string[];
  non_cisco: boolean;
  id: number;
  deleteId?: number;
  error: {
    model_ids: IFieldValidation;
    type: IFieldValidation;
  };
  isopenConfirm: boolean;
}

class ModelMappintg extends React.Component<
  IModelMappingProps,
  IModelMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: IModelMappingProps) {
    super(props);
    this.state = this.getEmptyState();
    this.onSaveClick = debounce(this.onSaveClick.bind(this), 500);
    this.onClickConfirm = debounce(this.onClickConfirm.bind(this), 500);
  }

  getEmptyState = () => ({
    modelMappings: [],
    type: "",
    model_ids: [],
    non_cisco: false,
    id: 0,
    error: {
      model_ids: { ...ModelMappintg.emptyErrorState },
      type: { ...ModelMappintg.emptyErrorState },
    },
    isopenConfirm: false,
    openCisco: true,
    openNonCisco: true,
  });
  componentDidMount() {
    if (!this.props.modelMappings) {
      this.props.fetchModelMappings();
    } else {
      this.setState({ modelMappings: this.props.modelMappings });
    }
  }

  componentDidUpdate(prevProps: IModelMappingProps) {
    if (
      this.props.modelMappings &&
      prevProps.modelMappings !== this.props.modelMappings
    ) {
      this.setState({ modelMappings: this.props.modelMappings });
    }
  }

  handleChange = (e) => {
    const newState = cloneDeep(this.state);
    (newState[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  handleChangeModel = (e) => {
    const newState = cloneDeep(this.state);
    (newState.model_ids as any) = e;
    this.setState(newState);
  };

  onSaveClick = () => {
    if (this.state.id) {
      this.props
        .saveModelMapping(this.state.id, this.state.type, this.state.model_ids)
        .then((action) => {
          if (action.type === UPDATE_MODELS_SUCCESS) {
            this.resetState();
            this.props.addSuccessMessage(
              `Model Type (${action.response.type}) updated successfully!`
            );
            this.props.fetchModelMappings(true);
          }
          if (action.type === UPDATE_MODELS_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
        });
    } else {
      this.props
        .addModelMapping(this.state.type, this.state.model_ids)
        .then((action) => {
          if (action.type === UPDATE_MODELS_SUCCESS) {
            this.resetState();
            this.props.addSuccessMessage(
              `Model Type (${action.response.type}) saved successfully!`
            );
            this.props.fetchModelMappings(true);
          }
          if (action.type === UPDATE_MODELS_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
        });
    }
  };

  resetState = () => {
    const newState = cloneDeep(this.state);
    (newState.type as string) = "";
    (newState.model_ids as any) = [];
    (newState.non_cisco as boolean) = false;
    (newState.id as number) = 0;
    (newState.deleteId as number) = undefined;
    (newState.error.model_ids.errorState as any) = "success";
    (newState.error.model_ids.errorMessage as any) = "";
    (newState.error.type.errorMessage as any) = "";
    (newState.error.type.errorState as any) = "success";
    this.setState(newState);
  };

  addNewModelMapping = () => {
    // Make sure that unsaved new models are not available
    if (!this.state.modelMappings.find((el) => el.id === 0)) {
      this.setState(
        {
          modelMappings: [
            { type: "", model_ids: [], id: 0 },
            ...this.state.modelMappings,
          ],
        },
        () => this.resetState()
      );
    }
  };

  setValidationErrors = (errorList) => {
    const newState: IModelMappingState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  onEditClick = (model) => {
    const newState = cloneDeep(this.state);
    (newState.non_cisco as boolean) = model.non_cisco;
    (newState.type as string) = model.type;
    (newState.id as number) = model.id;
    (newState.model_ids as any) = model.model_ids || [];
    this.setState(newState);
  };

  handleChangeTypes = (event: any, i) => {
    const newState = cloneDeep(this.state);
    const list = event.filter(
      (value, index, self) => self.indexOf(value) === index
    );
    newState.modelMappings[i].model_ids = list;
    this.setState(newState);
  };

  onDeleteRowClick(model: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      deleteId: model.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteModelMapping(this.state.deleteId).then((action) => {
      if (action.type === UPDATE_MODELS_SUCCESS) {
        this.props.fetchModelMappings();
        this.props.addSuccessMessage("Model deleted successfully!");
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      deleteId: undefined,
      id: 0,
    });
  };

  removeNonSavedElement = () => {
    this.setState({
      modelMappings: this.state.modelMappings.filter((el) => el.id !== 0),
    });
  };

  renderMapping = () => {
    const modelMappings = this.state.modelMappings;

    return (
      <div className="erp-ipt-tag-mapping__options">
        <div className="erp-ipt-tag-heading">
          <SquareButton
            content={
              <>
                <span className="add-plus">+</span>
                <span className="add-text">Add</span>
              </>
            }
            bsStyle={"link"}
            onClick={this.addNewModelMapping}
            className="add-new-sp-mapping add-btn"
          />
        </div>
        {Boolean(
          this.props.isFetching || (modelMappings && modelMappings.length > 0)
        ) ? (
          <div className="input-tag-mapping-table erp-ipt-tag-mapping-table">
            <div className="it-mapping-table-header">
              <div className="it-row-col-1">Type</div>
              <div className="it-row-col-2">Models</div>
            </div>
            <div className="it-mapping-table-rows">
              {modelMappings.map((model, index) =>
                Boolean(this.state.id === model.id) ? (
                  <div className={`it-table-row it-row-edit-mode`} key={index}>
                    <div className="it-row-col-1">
                      <Input
                        field={{
                          label: "Type",
                          type: "TEXT",
                          value: this.state.type,
                        }}
                        width={12}
                        name="type"
                        onChange={this.handleChange}
                        error={this.state.error.type}
                        placeholder={`Enter type`}
                        disabled={this.state.id ? true : false}
                      />
                      {Boolean(this.state.id) && (
                        <span className="tag-error"></span>
                      )}
                    </div>
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={this.state.model_ids}
                        onChange={(e) => this.handleChangeModel(e)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "Enter model type",
                        }}
                        addOnBlur={true}
                        onlyUnique={true}
                      /> */}
                      <span className="tag-error">
                        {this.state.error.model_ids.errorState ===
                        "error"
                          ? this.state.error.model_ids.errorMessage
                          : ""}
                      </span>
                    </div>
                    <div className="it-table-mapping-actions">
                      {this.state.model_ids.length > 0 && (
                        <IconButton
                          newIcon={true}
                          icon={"save_disk.svg"}
                          title="Save Model Mapping"
                          className="it-save-mapping"
                          onClick={this.onSaveClick}
                        />
                      )}
                      <IconButton
                        newIcon={true}
                        icon={"cross_sign.svg"}
                        title="Cancel"
                        className="it-cancel-mapping"
                        onClick={() =>
                          model.id !== 0
                            ? this.resetState()
                            : this.removeNonSavedElement()
                        }
                      />
                    </div>
                  </div>
                ) : (
                  <div
                    className={`it-table-row it-row-display-mode`}
                    key={index}
                  >
                    <div className="it-row-col-1">{model.type}</div>
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={model.model_ids || []}
                        onChange={(e) => this.handleChangeTypes(e, index)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "",
                        }}
                        disabled={true}
                      /> */}
                    </div>
                    <div className="it-table-mapping-actions">
                      <EditButton
                        title="Update model"
                        onClick={(e) => this.onEditClick(model)}
                      />
                      {model.id !== 0 && (
                        <DeleteButton
                          onClick={(e) => this.onDeleteRowClick(model, e)}
                        />
                      )}
                    </div>
                  </div>
                )
              )}
            </div>
          </div>
        ) : (
          <div className="no-mapping-data">
            No model type mappings configured!
          </div>
        )}
      </div>
    );
  };

  render() {
    return (
      <div className="model-mapping-container">
        <div className="erp-ipt-tag-mapping">
          <div className="loader">
            <Spinner show={this.props.isFetching ? true : false} />
          </div>

          {this.renderMapping()}
        </div>
        <ModalBase
          className="erp-ipt-tag-mapping-delete"
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          titleElement={`You're about to delete model mapping`}
          bodyElement={`The model mapping will be permanently remove and you won't be able to retrieve it again`}
          footerElement={
            <>
              <SquareButton
                onClick={this.toggleConfirmOpen}
                content="Cancel"
                bsStyle={"default"}
              />
              <SquareButton
                onClick={this.onClickConfirm}
                content="Delete"
                bsStyle={"primary"}
              />
            </>
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  modelMappings: state.subscription.modelMappings,
  isFetching: state.subscription.isFetchingModelList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchModelMappings: () => dispatch(fetchModelMappings()),
  saveModelMapping: (id: any, type: string, mapping: any) =>
    dispatch(saveModelMapping(id, type, mapping)),
  addModelMapping: (type: string, mapping: any) =>
    dispatch(addModelMapping(type, mapping)),
  deleteModelMapping: (id: any) => dispatch(deleteModelMapping(id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ModelMappintg);
