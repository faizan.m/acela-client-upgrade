import React from "react";

import { cloneDeep, debounce } from "lodash";
import Input from "../../components/Input/input";
import { connect } from "react-redux";
import {
  deleteActivityStatus,
  editActivityStatus,
  getActivityStatusList,
  NEW_ACTIVITY_STATUS_FAILURE,
  NEW_ACTIVITY_STATUS_SUCCESS,
  postActivityStatus,
} from "../../actions/pmo";
import Spinner from "../../components/Spinner";
import { getRoleTypes } from "../../actions/setting";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import RoleTypes from "./roleTypes";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";

interface IRoleRateMappingProps {
  getRoleRateMappingList: any;
  postRoleRateMapping: any;
  editRoleRateMapping: any;
  deleteRoleRateMapping: any;
  milestone?: boolean;
  url?: string;
  purchaseOrderSetting: any;
  getRoleTypes: any;
}

interface IRoleRateMappingState {
  statusList: any[];
  open: boolean;
  loading: boolean;
  roleTypeList: any[];
}

class RoleRateMapping extends React.Component<
  IRoleRateMappingProps,
  IRoleRateMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  private debouncedFetch;

  constructor(props: IRoleRateMappingProps) {
    super(props);

    this.state = {
      statusList: [],
      open: true,
      loading: false,
      roleTypeList: [],
    };
    this.debouncedFetch = debounce(this.passObjectToParent, 1000);
  }

  componentDidMount() {
    this.getRoleTypes();
    this.getRoleRateMapping();
  }

  getRoleTypes = async () => {
    const mapping: any = await this.props.getRoleTypes();
    this.setState({ roleTypeList: mapping.response });
  };

  getRoleRateMapping = () => {
    this.setState({ loading: true });
    this.props.getRoleRateMappingList(this.props.url).then((action) => {
      this.setState({ loading: false, statusList: action.response });
    });
  };

  addNew = () => {
    const newState = cloneDeep(this.state);
    newState.statusList.push({
      role_name: "",
      hourly_cost: 0,
      role_type: null,
      role_nameError: "",
      hourly_costError: "",
      is_default: true,
    });

    this.setState(newState);
  };

  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    if (rowData.id) {
      this.props
        .deleteRoleRateMapping(rowData, this.props.url)
        .then((action) => {
          if (action.type === NEW_ACTIVITY_STATUS_SUCCESS) {
            newState.statusList.splice(index, 1);
          }
          if (action.type === NEW_ACTIVITY_STATUS_FAILURE) {
            newState.statusList[index].role_nameError = `Couldn't delete`;
          }
          this.setState(newState);
        });
    } else {
      newState.statusList.splice(index, 1);
      this.setState(newState);
    }
  };

  onSaveRowClick = (e, index, rowData) => {
    const newState = cloneDeep(this.state);
    if (rowData.id) {
      this.props.editRoleRateMapping(rowData, this.props.url).then((action) => {
        if (action.type === NEW_ACTIVITY_STATUS_SUCCESS) {
          newState.statusList[index].edited = false;
          newState.statusList[index].role_nameError = "";
          newState.statusList[index].hourly_costError = "";
        }
        if (action.type === NEW_ACTIVITY_STATUS_FAILURE) {
          newState.statusList[index].edited = true;
          newState.statusList[index].role_nameError =
            action.errorList.data.role_name &&
            action.errorList.data.role_name.join(" ");
          newState.statusList[index].hourly_costError =
            action.errorList.data.hourly_cost &&
            action.errorList.data.hourly_cost.join(" ");
        }
        this.setState(newState);
      });
    } else {
      this.props.postRoleRateMapping(rowData, this.props.url).then((action) => {
        if (action.type === NEW_ACTIVITY_STATUS_SUCCESS) {
          newState.statusList[index].edited = false;
          newState.statusList[index].role_nameError = "";
          newState.statusList[index].id = action.response.id;
        }
        if (action.type === NEW_ACTIVITY_STATUS_FAILURE) {
          newState.statusList[index].edited = true;
          newState.statusList[index].role_nameError =
            action.errorList.data.role_name &&
            action.errorList.data.role_name.join(" ");
          newState.statusList[index].hourly_costError =
            action.errorList.data.hourly_cost &&
            action.errorList.data.hourly_cost.join(" ");
        }
        this.setState(newState);
      });
    }
  };

  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].edited = true;
    newState.statusList[index][e.target.name] = e.target.value;
    this.setState(newState);
    this.debouncedFetch(newState.statusList);
  };

  passObjectToParent = (list) => {
    const mappedObject = {};
    list.map((data, i) => {
      mappedObject[data.name] = data;
    });
  };

  isValid = () => {
    const error = {
      subscription_category_id: { ...RoleRateMapping.emptyErrorState },
      device_category_row: { ...RoleRateMapping.emptyErrorState },
    };
    let isValid = true;

    if (this.state.statusList && this.state.statusList.length > 0) {
      this.state.statusList.map((e) => {
        if (!e.connectwise_shipper || !e.name) {
          error.device_category_row.errorState = "error";
          // tslint:disable-next-line:max-line-length
          error.device_category_row.errorMessage = `Please enter required fields.`;
          isValid = false;
        }
      });
    }

    return isValid;
  };

  callbackFn = () => {
    this.getRoleTypes();
    this.getRoleRateMapping();
  };

  renderRateRoleMapping = () => {
    const statusList = this.state.statusList || [];
    return (
      <section className="role-rate-mapping">
        {statusList && statusList.length > 0 && !this.state.loading && (
          <div className="rrm-row rrm-heading ">
            <div className="rr-col">Role</div>
            <div className="rr-col">Type</div>
            <div className="rr-col">Hourly Cost</div>
          </div>
        )}
        {statusList.length === 0 && this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {statusList.length === 0 && !this.state.loading && (
          <div className="no-status col-md-6">No Mapping available</div>
        )}
        {statusList &&
          !this.state.loading &&
          statusList.map((row, index) => (
            <div className="rrm-row" key={index}>
              <div className="rr-col">
                <Input
                  field={{
                    label: "",
                    type: "TEXT",
                    value: row.role_name,
                    isRequired: false,
                  }}
                  width={12}
                  labelIcon={"info"}
                  name="role_name"
                  onChange={(e) => this.handleChangeList(e, index)}
                  placeholder={`Enter Role`}
                  error={
                    row.role_nameError && {
                      errorState: "error",
                      errorMessage: row.role_nameError,
                    }
                  }
                />
              </div>
              <div className="rr-col">
                <Input
                  field={{
                    label: "",
                    type: "PICKLIST",
                    value: row.role_type,
                    isRequired: false,
                    options: this.state.roleTypeList.map((d) => ({
                      value: d.id,
                      label: d.role,
                      disabled: false,
                    })),
                  }}
                  width={12}
                  labelIcon={"info"}
                  name="role_type"
                  onChange={(e) => this.handleChangeList(e, index)}
                  placeholder={`Select Type`}
                />
              </div>
              <div className="rr-col">
                <Input
                  field={{
                    label: "",
                    type: "NUMBER",
                    value: row.hourly_cost,
                    isRequired: false,
                  }}
                  width={12}
                  labelIcon={"info"}
                  name="hourly_cost"
                  onChange={(e) => this.handleChangeList(e, index)}
                  placeholder={`Enter Hourly cost in $`}
                  error={
                    row.hourly_costError && {
                      errorState: "error",
                      errorMessage: row.hourly_costError,
                    }
                  }
                  handleKeyDown={(e) => {
                    if (e.key === "Enter") {
                      this.onSaveRowClick(e, index, row);
                    }
                  }}
                />
              </div>

              <div className="rr-icon-col">
                {Boolean(
                  row.edited &&
                    row.role_name &&
                    row.role_type &&
                    row.hourly_cost
                ) && (
                  <img
                    className="saved"
                    alt=""
                    src={"/assets/icons/tick-blue.svg"}
                    onClick={(e) => this.onSaveRowClick(e, index, row)}
                  />
                )}
              </div>
              <div className="rr-icon-col">
                <SmallConfirmationBox
                  className="remove"
                  onClickOk={() => this.onDeleteRowClick(index, row)}
                  text={"Mapping"}
                  showButton={true}
                />
              </div>
            </div>
          ))}
        <div onClick={this.addNew} className="add-new-role">
          <span className="add-plus">+</span>
          <span className="add-text">Add Role</span>
        </div>
      </section>
    );
  };

  render() {
    return (
      <>
        <HorizontalTabSlider>
          <Tab title="Rate Role Mapping">{this.renderRateRoleMapping()}</Tab>
          <Tab title="Role Type">
            <RoleTypes callbackFn={this.callbackFn} />
          </Tab>
        </HorizontalTabSlider>
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getRoleRateMappingList: (url: string) => dispatch(getActivityStatusList(url)),
  postRoleRateMapping: (data: any, url: string) =>
    dispatch(postActivityStatus(data, url)),
  editRoleRateMapping: (data: any, url: string) =>
    dispatch(editActivityStatus(data, url)),
  deleteRoleRateMapping: (data: any, url: string) =>
    dispatch(deleteActivityStatus(data, url)),
  getRoleTypes: () => dispatch(getRoleTypes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(RoleRateMapping);
