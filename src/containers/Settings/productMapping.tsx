import React from "react";

import { cloneDeep } from "lodash";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import {
  addProductMapping,
  deleteProductMapping,
  fetchProductMappings,
  saveProductMapping,
  fetchSubscriptionServiceProviderSettings,
  saveSubscriptionServiceProviderSettings,
  updateSubscriptionServiceProviderSettings,
  deleteSubscriptionServiceProviderSettings,
  UPDATE_PRODUCTS_SUCCESS,
  UPDATE_PRODUCTS_FAILURE,
  SERVICE_PROVIDER_SETTINGS_SUCCESS,
  SERVICE_PROVIDER_SETTINGS_FAILURE,
} from "../../actions/subscription";
import SquareButton from "../../components/Button/button";
import DeleteButton from "../../components/Button/deleteButton";
import EditButton from "../../components/Button/editButton";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import { commonFunctions } from "../../utils/commonFunctions";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import IconButton from "../../components/Button/iconButton";
import ModalBase from "../../components/ModalBase/modalBase";
import "../../commonStyles/inputTagMapping.scss";

interface IProductMappingProps {
  productMappings: any;
  isFetching: any;
  fetchProductMappings: any;
  saveProductMapping: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  addProductMapping: any;
  deleteProductMapping: any;
  fetchSubscriptionServiceProviderSettings: any;
  saveSubscriptionServiceProviderSettings: any;
  updateSubscriptionServiceProviderSettings: any;
  deleteSubscriptionServiceProviderSettings: any;
}

type mappingType = "product_type" | "service_provider";

interface IProductMappingState {
  loading: boolean;
  deleteType: mappingType;
  serviceProviderPrefixMappings: any[];
  serviceProviderName: string;
  prefixes: any[];
  productMappings: any[];
  currentEdit: mappingType;
  name: string;
  products: string[];
  id: number;
  deleteId?: number;
  error: {
    products: IFieldValidation;
    name: IFieldValidation;
    serviceProviderName: IFieldValidation;
    providerPrefix: IFieldValidation;
  };
  isopenConfirm: boolean;
}

class ProductMappintg extends React.Component<
  IProductMappingProps,
  IProductMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: IProductMappingProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    deleteType: "product_type" as mappingType,
    loading: false,
    serviceProviderPrefixMappings: [],
    productMappings: [],
    currentEdit: "product_type" as mappingType,
    type: 0,
    name: "",
    products: [],
    id: 0,
    serviceProviderName: "",
    prefixes: [],
    error: {
      products: { ...ProductMappintg.emptyErrorState },
      name: { ...ProductMappintg.emptyErrorState },
      serviceProviderName: { ...ProductMappintg.emptyErrorState },
      providerPrefix: { ...ProductMappintg.emptyErrorState },
    },
    isopenConfirm: false,
  });
  componentDidMount() {
    if (!this.props.productMappings) {
      this.props.fetchProductMappings();
    } else {
      this.setState({ productMappings: this.props.productMappings });
    }
    this.fetchServiceMapping();
  }

  componentDidUpdate(prevProps: IProductMappingProps) {
    if (
      this.props.productMappings &&
      prevProps.productMappings !== this.props.productMappings
    ) {
      this.setState({ productMappings: this.props.productMappings });
    }
  }

  fetchServiceMapping = () => {
    this.setState({ loading: true });
    this.props.fetchSubscriptionServiceProviderSettings().then((action) => {
      if (action.type === SERVICE_PROVIDER_SETTINGS_SUCCESS) {
        this.setState({ serviceProviderPrefixMappings: action.response });
      }
      this.setState({ loading: false });
    });
  };

  handleChange = (e) => {
    const newState = cloneDeep(this.state);
    (newState[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  handleChangeProduct = (e) => {
    const newState = cloneDeep(this.state);
    (newState.products as any) = e;
    this.setState(newState);
  };

  handleChangePrefixes = (e) => {
    const newState = cloneDeep(this.state);
    (newState.prefixes as any) = e;
    this.setState(newState);
  };

  addNewProductMapping = () => {
    // Make sure that unsaved new products are not available
    if (!this.state.productMappings.find((el) => el.id === 0)) {
      this.setState(
        {
          productMappings: [
            { name: "", products: [], id: 0 },
            ...this.state.productMappings,
          ],
        },
        () => this.resetState("product_type")
      );
    }
  };

  addNewServiceMapping = () => {
    // Make sure that unsaved new service mappings are not available
    if (!this.state.serviceProviderPrefixMappings.find((el) => el.id === 0)) {
      this.setState(
        {
          serviceProviderPrefixMappings: [
            { service_provider_name: "", provider_prefix: [], id: 0 },
            ...this.state.serviceProviderPrefixMappings,
          ],
        },
        () => this.resetState("service_provider")
      );
    }
  };

  onSaveClick = () => {
    if (this.state.id !== 0) {
      this.props
        .saveProductMapping(this.state.id, this.state.name, this.state.products)
        .then((action) => {
          if (action.type === UPDATE_PRODUCTS_SUCCESS) {
            this.resetState();
            this.props.addSuccessMessage(`Product Type updated successfully`);
            this.props.fetchProductMappings(true);
          }
          if (action.type === UPDATE_PRODUCTS_FAILURE) {
            this.props.addErrorMessage("Error saving product type mapping!");
            this.setValidationErrors(action.errorList.data);
          }
        });
    } else {
      this.props
        .addProductMapping(this.state.name, this.state.products)
        .then((action) => {
          if (action.type === UPDATE_PRODUCTS_SUCCESS) {
            this.resetState();
            this.props.addSuccessMessage(
              `Product Type (${action.response.name}) saved successfully`
            );
            this.props.fetchProductMappings(true);
          }
          if (action.type === UPDATE_PRODUCTS_FAILURE) {
            this.props.addErrorMessage("Error updating product type mapping!");
            this.setValidationErrors(action.errorList.data);
          }
        });
    }
  };

  onPrefixSaveClick = () => {
    const { id, serviceProviderName, prefixes } = this.state;
    const {
      saveSubscriptionServiceProviderSettings,
      updateSubscriptionServiceProviderSettings,
    } = this.props;

    const data = {
      service_provider_name: serviceProviderName,
      provider_prefix: prefixes,
    };

    if (this.state.id !== 0) {
      updateSubscriptionServiceProviderSettings(id, data).then((action) => {
        if (action.type === SERVICE_PROVIDER_SETTINGS_SUCCESS) {
          this.resetState();
          this.props.addSuccessMessage(
            `Service Provider (${action.response.service_provider_name}) updated successfully`
          );
          this.fetchServiceMapping();
        }
        if (action.type === SERVICE_PROVIDER_SETTINGS_FAILURE) {
          this.props.addErrorMessage("Error updating service provider!");
          this.setValidationErrors(action.errorList.data);
        }
      });
    } else {
      saveSubscriptionServiceProviderSettings(data).then((action) => {
        if (action.type === SERVICE_PROVIDER_SETTINGS_SUCCESS) {
          this.resetState();
          this.fetchServiceMapping();
          this.props.addSuccessMessage(
            `Service Provider (${action.response.service_provider_name}) saved successfully`
          );
        }
        if (action.type === SERVICE_PROVIDER_SETTINGS_FAILURE) {
          this.props.addErrorMessage("Error saving service provider!");
          this.setValidationErrors(action.errorList.data);
        }
      });
    }
  };

  resetState = (modal_name: mappingType = "product_type") => {
    const newState = cloneDeep(this.state);
    (newState.currentEdit as mappingType) = modal_name;
    (newState.deleteType as mappingType) = "product_type";
    (newState.serviceProviderName as string) = "";
    (newState.prefixes as any) = [];
    (newState.name as string) = "";
    (newState.products as any) = [];
    (newState.id as number) = 0;
    (newState.deleteId as number) = undefined;
    (newState.error.products.errorState as any) = "success";
    (newState.error.products.errorMessage as any) = "";
    (newState.error.name.errorMessage as any) = "";
    (newState.error.name.errorState as any) = "success";
    (newState.error.serviceProviderName.errorMessage as any) = "";
    (newState.error.serviceProviderName
      .errorState as any) = "success";
    (newState.error.providerPrefix
      .errorState as any) = "success";
    (newState.error.providerPrefix.errorMessage as any) = "";
    this.setState(newState);
  };

  setValidationErrors = (errorList) => {
    const newState: IProductMappingState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  onEditClick = (product) => {
    const newState = cloneDeep(this.state);
    (newState.currentEdit as mappingType) = "product_type";
    (newState.name as string) = product.name;
    (newState.id as number) = product.id;
    (newState.products as any) =
      (product.products && product.products.map((s) => s.name)) || [];
    this.setState(newState);
  };

  onServiceEditClick = (service) => {
    const newState = cloneDeep(this.state);
    (newState.serviceProviderName as string) = service.service_provider_name;
    (newState.currentEdit as mappingType) = "service_provider";
    (newState.id as number) = service.id;
    (newState.prefixes as any) =
      (service.provider_prefix && service.provider_prefix) || [];
    this.setState(newState);
  };

  handleChangeTypes = (event: any, i) => {
    const newState = cloneDeep(this.state);
    const list = event.filter(
      (value, index, self) => self.indexOf(value) === index
    );
    newState.productMappings[i].products = list;
    this.setState(newState);
  };

  removeNonSavedElement = (type: mappingType) => {
    if (type === "product_type") {
      this.setState({
        productMappings: this.state.productMappings.filter((el) => el.id !== 0),
      });
    } else {
      this.setState({
        serviceProviderPrefixMappings: this.state.serviceProviderPrefixMappings.filter(
          (el) => el.id !== 0
        ),
      });
    }
  };

  onDeleteRowClick(product: any, e: any, type: mappingType): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      deleteId: product.id,
      deleteType: type,
    });
  }

  onClickConfirm = () => {
    const { deleteType } = this.state;
    if (deleteType === "product_type") {
      this.props.deleteProductMapping(this.state.deleteId).then((action) => {
        if (action.type === UPDATE_PRODUCTS_SUCCESS) {
          this.props.addSuccessMessage(
            "Product Type Mapping deleted successfully!"
          );
          this.props.fetchProductMappings();
          this.toggleConfirmOpen();
        }
      });
    }

    if (deleteType === "service_provider") {
      this.props
        .deleteSubscriptionServiceProviderSettings(this.state.deleteId)
        .then((action) => {
          if (action.type === SERVICE_PROVIDER_SETTINGS_SUCCESS) {
            this.props.addSuccessMessage(
              "Service Provider Prefix Mapping deleted successfully!"
            );
            this.fetchServiceMapping();
            this.toggleConfirmOpen();
          }
        });
    }
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      deleteId: undefined,
      id: 0,
    });
  };

  renderMapping = () => {
    const productMappings = this.state.productMappings;

    return (
      <div className="erp-ipt-tag-mapping__options">
        <div className="erp-ipt-tag-heading">
          <SquareButton
            content={
              <>
                <span className="add-plus">+</span>
                <span className="add-text">Add</span>
              </>
            }
            bsStyle={"link"}
            onClick={this.addNewProductMapping}
            className="add-new-sp-mapping add-btn"
          />
        </div>
        {this.props.isFetching ||
        (productMappings && productMappings.length > 0) ? (
          <div className="input-tag-mapping-table erp-ipt-tag-mapping-table">
            <div className="it-mapping-table-header">
              <div className="it-row-col-1">Type</div>
              <div className="it-row-col-2">Products</div>
            </div>
            <div className="it-mapping-table-rows">
              {productMappings.map((product, index) =>
                Boolean(
                  this.state.currentEdit === "product_type" &&
                    this.state.id === product.id
                ) ? (
                  <div className={`it-table-row it-row-edit-mode`} key={index}>
                    <div className="it-row-col-1">
                      <Input
                        field={{
                          label: "",
                          type: "TEXT",
                          value: this.state.name,
                        }}
                        width={12}
                        name="name"
                        onChange={this.handleChange}
                        error={this.state.error.name}
                        placeholder={`Enter name`}
                      />
                    </div>
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={this.state.products}
                        onChange={(e) => this.handleChangeProduct(e)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "Enter product name",
                        }}
                        addOnBlur={true}
                        onlyUnique={true}
                      /> */}
                      <span className="tag-error">
                        {this.state.error.products.errorState ===
                        "error"
                          ? this.state.error.products.errorMessage
                          : ""}
                      </span>
                    </div>
                    <div className="it-table-mapping-actions">
                      <IconButton
                        newIcon={true}
                        icon={"save_disk.svg"}
                        title="Save Product Mapping"
                        className="it-save-mapping"
                        onClick={this.onSaveClick}
                      />
                      <IconButton
                        newIcon={true}
                        icon={"cross_sign.svg"}
                        title="Cancel"
                        className="it-cancel-mapping"
                        onClick={() =>
                          product.id !== 0
                            ? this.resetState()
                            : this.removeNonSavedElement("product_type")
                        }
                      />
                    </div>
                  </div>
                ) : (
                  <div
                    className={`it-table-row it-row-display-mode`}
                    key={index}
                  >
                    <div className="it-row-col-1">{product.name}</div>
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={product.products.map((d) => d.name) || []}
                        onChange={(e) => this.handleChangeTypes(e, index)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "",
                        }}
                        disabled={true}
                      /> */}
                    </div>
                    <div className="it-table-mapping-actions">
                      <EditButton
                        title="Update product"
                        onClick={(e) => this.onEditClick(product)}
                      />
                      {product.id !== 0 && (
                        <DeleteButton
                          onClick={(e) =>
                            this.onDeleteRowClick(product, e, "product_type")
                          }
                        />
                      )}
                    </div>
                  </div>
                )
              )}
            </div>
          </div>
        ) : (
          <div className="no-mapping-data">No product mappings configured!</div>
        )}
      </div>
    );
  };

  renderDefaultServiceProviderSettings = () => {
    const { serviceProviderPrefixMappings } = this.state;

    return (
      <div className="erp-ipt-tag-mapping__options">
        <div className="loader">
          <Spinner show={this.state.loading} />
        </div>
        <div className="erp-ipt-tag-heading">
          <SquareButton
            content={
              <>
                <span className="add-plus">+</span>
                <span className="add-text">Add</span>
              </>
            }
            bsStyle={"link"}
            onClick={this.addNewServiceMapping}
            className="add-new-sp-mapping add-btn"
          />
        </div>
        {this.state.loading ||
        (serviceProviderPrefixMappings &&
          serviceProviderPrefixMappings.length > 0) ? (
          <div className="input-tag-mapping-table erp-ipt-tag-mapping-table">
            <div className="it-mapping-table-header">
              <div className="it-row-col-1">Service Provider</div>
              <div className="it-row-col-2">Prefix</div>
            </div>
            <div className="it-mapping-table-rows">
              {serviceProviderPrefixMappings.map((service, index) =>
                Boolean(
                  this.state.currentEdit === "service_provider" &&
                    this.state.id === service.id
                ) ? (
                  <div className={`it-table-row it-row-edit-mode`} key={index}>
                    <div className="it-row-col-1">
                      <Input
                        field={{
                          label: "",
                          type: "TEXT",
                          value: this.state.serviceProviderName,
                        }}
                        width={12}
                        name="serviceProviderName"
                        onChange={this.handleChange}
                        error={this.state.error.serviceProviderName}
                        placeholder={`Enter name`}
                      />
                    </div>
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={this.state.prefixes}
                        onChange={(e) => this.handleChangePrefixes(e)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "Enter prefixes",
                        }}
                        addOnBlur={true}
                        onlyUnique={true}
                      /> */}
                      <span className="tag-error">
                        {this.state.error.providerPrefix.errorState ===
                        "error"
                          ? this.state.error.providerPrefix.errorMessage
                          : ""}
                      </span>
                    </div>
                    <div className="it-table-mapping-actions">
                      {this.state.prefixes.length > 0 && (
                        <IconButton
                          newIcon={true}
                          icon={"save_disk.svg"}
                          title="Save Product Mapping"
                          className="it-save-mapping"
                          onClick={this.onPrefixSaveClick}
                        />
                      )}
                      <IconButton
                        newIcon={true}
                        icon={"cross_sign.svg"}
                        title="Cancel"
                        className="it-cancel-mapping"
                        onClick={() =>
                          service.id !== 0
                            ? this.resetState("service_provider")
                            : this.removeNonSavedElement("service_provider")
                        }
                      />
                    </div>
                  </div>
                ) : (
                  <div
                    className={`it-table-row it-row-display-mode`}
                    key={index}
                  >
                    <div className="it-row-col-1">
                      {service.service_provider_name}
                    </div>
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={service.provider_prefix || []}
                        onChange={(e) => this.handleChangeTypes(e, index)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "",
                        }}
                        disabled={true}
                      /> */}
                    </div>
                    <div className="it-table-mapping-actions">
                      <EditButton
                        title="Update product"
                        onClick={(e) => this.onServiceEditClick(service)}
                      />
                      {service.id !== 0 && (
                        <DeleteButton
                          onClick={(e) =>
                            this.onDeleteRowClick(
                              service,
                              e,
                              "service_provider"
                            )
                          }
                        />
                      )}
                    </div>
                  </div>
                )
              )}
            </div>
          </div>
        ) : (
          <div className="no-mapping-data">
            No service provider prefixes configured!
          </div>
        )}
      </div>
    );
  };

  render() {
    return (
      <div className="erp-ipt-tag-mapping">
        <div className="loader">
          <Spinner show={this.props.isFetching ? true : false} />
        </div>
        <HorizontalTabSlider>
          <Tab title={"Product Types"}>{this.renderMapping()}</Tab>
          <Tab title={"Service Provider Prefix Mapping"}>
            {this.renderDefaultServiceProviderSettings()}
          </Tab>
        </HorizontalTabSlider>
        <ModalBase
          className="erp-ipt-tag-mapping-delete"
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          titleElement={`You're about to delete ${
            this.state.deleteType === "product_type"
              ? "product type"
              : "provider mapping"
          }`}
          bodyElement={`The ${
            this.state.deleteType === "product_type"
              ? "product type"
              : "provider mapping"
          } will be permanently remove and you won't be able to retrieve it again`}
          footerElement={
            <>
              <SquareButton
                onClick={this.toggleConfirmOpen}
                content="Cancel"
                bsStyle={"default"}
              />
              <SquareButton
                onClick={this.onClickConfirm}
                content="Delete"
                bsStyle={"primary"}
              />
            </>
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  productMappings: state.subscription.productMappings,
  isFetching: state.subscription.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProductMappings: () => dispatch(fetchProductMappings()),
  saveProductMapping: (id: any, name: string, mapping: any) =>
    dispatch(saveProductMapping(id, name, mapping)),
  addProductMapping: (name: string, mapping: any) =>
    dispatch(addProductMapping(name, mapping)),
  deleteProductMapping: (id: any) => dispatch(deleteProductMapping(id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchSubscriptionServiceProviderSettings: () =>
    dispatch(fetchSubscriptionServiceProviderSettings()),
  saveSubscriptionServiceProviderSettings: (data: any) =>
    dispatch(saveSubscriptionServiceProviderSettings(data)),
  updateSubscriptionServiceProviderSettings: (id: any, data: any) =>
    dispatch(updateSubscriptionServiceProviderSettings(id, data)),
  deleteSubscriptionServiceProviderSettings: (id: any) =>
    dispatch(deleteSubscriptionServiceProviderSettings(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductMappintg);
