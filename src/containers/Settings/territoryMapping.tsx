import React from "react";

import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import {
  fetchTerritories,
  fetchTerritoryMembers,
  SAVE_MANAGER_FAILURE,
  SAVE_MANAGER_SUCCESS,
  saveTerritoryManager,
} from "../../actions/provider/integration";
import Select from "../../components/Input/Select/select";
import Spinner from "../../components/Spinner";
import './style.scss';

interface ITerritoryMappingProps {
  terretories: any;
  isFetchingTerretories: any;
  terretoryMembers: any;
  isFetchingMembers: any;
  fetchTerritories: any;
  fetchTerritoryMembers: any;
  saveTerritoryManager: any;
  addSuccessMessage: any;
  addErrorMessage: any;
}

interface ITerritoryMappingState {
  error: any;
  terretories: any[];
}

class TerritoryMapping extends React.Component<
  ITerritoryMappingProps,
  ITerritoryMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: ITerritoryMappingProps) {
    super(props);

    this.state = {
      terretories: [],
      error: {},
    };
  }
  componentDidMount() {
    if (!this.props.terretories) {
      this.props.fetchTerritories();
    } else {
      this.setState({ terretories: this.props.terretories });
    }
    if (!this.props.terretoryMembers) {
      this.props.fetchTerritoryMembers();
    }
  }

  componentDidUpdate(prevProps: ITerritoryMappingProps) {
    if (
      this.props.terretories &&
      prevProps.terretories !== this.props.terretories
    ) {
      this.setState({ terretories: this.props.terretories });
    }
  }

  handleyChange = (e, index, id) => {
    const newState = cloneDeep(this.state);
    newState.terretories[index].manager_id = e.target.value;
    this.setState(newState);
    this.props.saveTerritoryManager(e.target.value, id).then((action) => {
      if (action.type === SAVE_MANAGER_SUCCESS) {
        this.props.fetchTerritories();
        this.props.addSuccessMessage("Manager updated");
      }
      if (action.type === SAVE_MANAGER_FAILURE) {
        this.props.addErrorMessage("Manager Failed To update");
      }
    });
  };

  terretoryMembersList = () => {
    const terretoryMembers = this.props.terretoryMembers
      ? this.props.terretoryMembers.map((c) => ({
          value: c.id,
          label: `${c.first_name} ${c.last_name}`,
          disabled: false,
        }))
      : [];

    return terretoryMembers;
  };

  renderTerritoriesMapping = () => {
    const terretories = this.state.terretories;

    return (
      <div className="terretory-mapping__options row ">
        <div className="mapping-row heading">
          <div className="terretory-name col-md-4 col-xs-4">
            Territory
          </div>

          <div className="field-section col-md-3 col-xs-3">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Manager
              </label>
            </div>
          </div>
        </div>
        {terretories &&
          terretories.map((terretory, index) => (
            <div className="mapping-row" key={index}>
              <div className="terretory-name arrow col-md-4 col-xs-4">
                {terretory.name}
              </div>

              <div className="field-section col-md-3 col-xs-3">
                <div
                  className={`${!terretory.manager_id ? `error-input` : ""}`}
                >
                  <Select
                    name="manufacturer_id"
                    value={terretory.manager_id}
                    onChange={(e) => this.handleyChange(e, index, terretory.id)}
                    options={this.terretoryMembersList()}
                    multi={false}
                    searchable={true}
                    placeholder="Select Manager"
                  />
                </div>
                {!terretory.manager_id && (
                  <div className="select-manufacturer-error">
                    Please select manager
                  </div>
                )}
              </div>
            </div>
          ))}
      </div>
    );
  };

  render() {
    return (
      <div className="terretory-mapping">
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingMembers || this.props.isFetchingTerretories
                ? true
                : false
            }
          />
        </div>
        <div className="col-md-12 col-x row mapping-row heading">
          <div className="model-heading arrow">
            <div className="heading-text">Territory Mapping</div>
          </div>
        </div>
        {this.renderTerritoriesMapping()}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  terretories: state.integration.terretories,
  isFetchingTerretories: state.integration.isFetchingTerretories,
  terretoryMembers: state.integration.terretoryMembers,
  isFetchingMembers: state.integration.isFetchingMembers,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTerritories: () => dispatch(fetchTerritories()),
  fetchTerritoryMembers: () => dispatch(fetchTerritoryMembers()),
  saveTerritoryManager: (managerId: any, id: any) =>
    dispatch(saveTerritoryManager(managerId, id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TerritoryMapping);
