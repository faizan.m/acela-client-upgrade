import { debounce, isEmpty } from "lodash";
import moment from "moment";
import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import {
  fetchSalesActivity,
  SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
  getSalesActivityStatusSetting,
  SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
  getSalesActivityPrioritySetting,
  UPDATE_SALES_ACTIVITY_SUCCESS,
  updateSalesActivity,
  getSalesActivityCustomView,
  postSalesActivityCustomView,
  deleteSalesActivityCustomView,
  SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS,
} from "../../actions/sales";
import { fetchProviderUsers } from '../../actions/provider/user';
import {
  fetchCustomersShort,
  FETCH_CUSTOMERS_SHORT_SUCCESS,
} from "../../actions/customer";
import Input from "../../components/Input/input";
import PMOCollapsible from "../../components/PMOCollapsible";
// import SquareButton from '../../components/Button/button';
import { fromISOStringToFormattedDate, toFormattedDate } from "../../utils/CalendarUtil";
import {
  getConvertedColorWithOpacity,
  getBackgroundColor,
} from "../../utils/CommonUtils";
import {
  getQuoteStageList,
  getQuoteTypeList,
} from '../../actions/sow';
import Filters from "./filters";

import "./style.scss";
import Spinner from "../../components/Spinner";

interface IProjectProps extends ICommonProps {
  getProjectManagers: any;
  fetchCustomersShort: any;
  getProjectTypes: any;
  fetchProjects: any;
  fetching: boolean;
  rules: any;
  additionalSetting: IAdditionSettingPMO;
  GetAdditionalSetting: any;
  checkSettings: any;
  closedDateOption: any;
  providerUsers: any;
  fetchProviderUsers: any;
  isFetchingUsers: boolean;
  getSalesActivityStatusSetting: any;
  getSalesActivityPrioritySetting: any;
  updateSalesActivity: any;
  getSalesActivityCustomView: any;
  postSalesActivityCustomView: any;
  deleteSalesActivityCustomView: any;
  getQuoteStageList: any;
  getQuoteTypeList: any;
  fetchQuoteDashboardListingPU: any;
}

interface IProjectState {
  list: any;
  currentPage: {
    pageType: PageType;
  };
  projects: any[];
  pagination: IScrollPaginationFilters;
  inputValue: string;
  noData: boolean;
  showAllActivities?: boolean;
  groupBy: string;
  showSearch: boolean;
  showGroupBy: boolean;
  loading: boolean;
  showFiltersPop: boolean;
  selectedDueDate: any;
  selectedDueDateOption: any;
  showAllCompletedProjects: any;
  assignedToOptions: any;
  createdByOptions: any;
  customerOptions: any;
  statusOptions: any;
  priorityOptions: any;
  activeAssignedToFilter: any;
  activeCreatedByFilter: any;
  activeCustomerFilter: any;
  activeStatusFilter: any;
  activePriorityFilter: any;
  activeTypeOfWorkFilter: any;
  showRowActions: boolean;
  rowEditId: any;
  message: string;
  messageId: any;
  isFetchingCustomerContacts: boolean;
  fetchingStatusSettings: boolean;
  fetchingPrioritySettings: boolean;
  statusSettingOptions: any
  prioritySettingOptions: any;
  fetchingCustomers: boolean;
  customerStr: string;
  projectManagerStr: string;
  statusStr: string;
  priorityStr: string;
  assignedToStr: string;
  createdByStr: string;
  activeTypeOfWorkStr: string;
  closedStatues: any;
  openStatues: any;
  showDefaultView: boolean;
  fetchingCustomView: boolean;
  customViewObject: any;
}

enum PageType {
  Projects,
  AllProjects,
}
class MyProjects extends React.Component<any, IProjectState> {
  constructor(props: IProjectProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 2000);
  }

  typeOfActivityOptions = [
    {
      label: "Statement of work",
      value: "Statement of work"
    },
    {
      label: "Hardware BoM",
      value: "Hardware BoM"
    },
    {
      label: "Research and Respond",
      value: "Research and Respond"
    }
  ];

  getEmptyState = () => ({
    list: [],
    currentPage: {
      pageType: PageType.Projects,
    },
    projects: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: "",
      search: "",
      type: "",
      customer: "",
      manager: "",
      status: "",
      priority: "",
      created_by: "",
      due_date_before: "",
      due_date_after: "",
      showSearch: false
    },
    inputValue: "",
    noData: false,
    showSearch: false,
    showGroupBy: false,
    showAllActivities: false,
    loading: true,
    groupBy: "",
    showFiltersPop: false,
    customerOptions: [],
    fetchingCustomers: false,
    selectedDueDate: undefined,
    selectedDueDateOption: undefined,
    showAllCompletedProjects: undefined,
    assignedToOptions: [],
    createdByOptions: [],
    statusOptions: [],
    priorityOptions: [],
    activeAssignedToFilter: [],
    activeCreatedByFilter: [],
    activeCustomerFilter: [],
    activeStatusFilter: [],
    activePriorityFilter: [],
    activeTypeOfWorkFilter: [],
    showRowActions: false,
    rowEditId: undefined,
    message: "",
    messageId: undefined,
    isFetchingCustomerContacts: false,
    fetchingStatusSettings: false,
    fetchingPrioritySettings: false,
    statusSettingOptions: [],
    prioritySettingOptions: [],
    customerStr: "",
    projectManagerStr: "",
    statusStr: "",
    priorityStr: "",
    assignedToStr: "",
    createdByStr: "",
    activeTypeOfWorkStr: "",
    closedStatues: [],
    openStatues: [],
    showDefaultView: true,
    fetchingCustomView: false,
    customViewObject: {},
  });

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const showAllActivities = query.get("showAllActivities") === "true" ? true : false;

    this.props.fetchProviderUsers({ pagination: false });
    this.getAllCustomers();
    this.getStatusSettings();
    this.getPrioritySettings();
    this.getCustomViews();
    this.props.getQuoteStageList();
    this.props.getQuoteTypeList();

    this.setState({ showAllActivities });
    if (this.state.projects && this.state.projects.length === 0) {
      this.getAllCustomers();
      this.fetchMoreData(true);
      if (this.props.location.state) {
        const { messageId, message } = this.props.location.state
        this.setState({ message, messageId });

        setTimeout(() => {
          this.setState({ message: "", messageId: undefined });
        }, 15000);
      }
    }
  }

  getCustomViews = () => {
    this.setState({ fetchingCustomView: true });
    this.props.getSalesActivityCustomView().then((action) => {
      if (action.type === SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS) {
        this.setState({
          customViewObject: action.response.length > 0 && action.response[0].custom_activity_view,
        })
      }
    })
  };

  createCustomView = () => {
    this.setState({ loading: true });

    const customViewObj = {
      ...this.state.pagination
    }

    customViewObj.showGroupBy = this.state.showGroupBy;
    customViewObj.groupBy = this.state.groupBy;

    customViewObj.showSearch = this.state.showSearch;

    (customViewObj as any).selectedDueDate = this.state.selectedDueDate;
    (customViewObj as any).selectedDueDateOption = this.state.selectedDueDateOption;
    (customViewObj as any).showAllCompletedProjects = this.state.showAllCompletedProjects;
    (customViewObj as any).activeCustomerFilter = this.state.activeCustomerFilter;
    (customViewObj as any).activeAssignedToFilter = this.state.activeAssignedToFilter;
    (customViewObj as any).activeCreatedByFilter = this.state.activeCreatedByFilter;
    (customViewObj as any).activeStatusFilter = this.state.activeStatusFilter;
    (customViewObj as any).activePriorityFilter = this.state.activePriorityFilter;
    (customViewObj as any).activeTypeOfWorkFilter = this.state.activeTypeOfWorkFilter;
    (customViewObj as any).customerStr = this.state.customerStr;
    (customViewObj as any).statusStr = this.state.statusStr;
    (customViewObj as any).priorityStr = this.state.priorityStr;
    (customViewObj as any).assignedToStr = this.state.assignedToStr;
    (customViewObj as any).createdByStr = this.state.createdByStr;
    (customViewObj as any).activeTypeOfWorkStr = this.state.activeTypeOfWorkStr;

    this.props.postSalesActivityCustomView({ custom_activity_view: customViewObj }).then((action) => {
      if (action.type === SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS) {
        this.setState({
          customViewObject: action.response.custom_activity_view,
        }, () => this.fetchMoreData(true))
      }
    })

    this.setState({ loading: false });
  };

  getStatusSettings = () => {
    this.setState({ fetchingStatusSettings: true });
    this.props.getSalesActivityStatusSetting().then((action) => {
      if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
        const statusSettingOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));

        const closedStatues = action.response.filter((val) => {
          return val.is_closed
        }).map((obj) => {
          return obj.id;
        });

        const openStatues = action.response.filter((val) => {
          return !val.is_closed
        }).map((obj) => {
          return obj.id;
        });

        this.setState({
          statusSettingOptions,
          closedStatues,
          openStatues,
          statusOptions: action.response,
          // pagination: {
          //   ...this.state.pagination,
          //   status: openStatues.join(",")
          // }
        });
      }
      this.setState({ fetchingStatusSettings: false });
    });
  }

  getPrioritySettings = () => {
    this.setState({ fetchingPrioritySettings: true });
    this.props.getSalesActivityPrioritySetting().then((action) => {
      if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS) {
        const prioritySettingOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));
        this.setState({ prioritySettingOptions });
      }
      this.setState({ fetchingPrioritySettings: false });
    });
  }

  getAllCustomers = () => {
    this.setState({ fetchingCustomers: true });
    this.props.fetchCustomersShort().then((action) => {
      if (action.type === FETCH_CUSTOMERS_SHORT_SUCCESS) {
        const customerOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.name}`,
        }));
        this.setState({ customerOptions });
      }
      this.setState({ fetchingCustomers: false });
    });
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
    this.updateMessage(search);
  };

  updateMessage = (search) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
      customViewObject: {
        ...prevState.customViewObject,
        search,
      }
    }), () => this.fetchMoreData(true));
  };

  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });

    const query = new URLSearchParams(this.props.location.search);
    const showAllActivities = query.get("showAllActivities") === "true" ? true : false;

    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
        customer: this.state.pagination.customer,
        assigned_to: this.state.pagination.assigned_to,
        created_by: this.state.pagination.created_by,
        status: this.state.pagination.status,
        priority: this.state.pagination.priority,
        due_date_after: this.state.pagination.due_date_after,
        due_date_before: this.state.pagination.due_date_before,
        activity_type: this.state.pagination.activity_type,
      };
    }
    if (prevParams.nextPage !== null) {
      prevParams = {
        ...prevParams,
        customer: this.state.activeCustomerFilter.join(),
        // status: this.state.showAllCompletedProjects ? "" : "",
        assigned_to: this.state.pagination.assigned_to,
        created_by: this.state.pagination.created_by,
        status: this.state.pagination.status,
        priority: this.state.pagination.priority,
        due_date_after: this.state.pagination.due_date_after,
        due_date_before: this.state.pagination.due_date_before,
        activity_type: this.state.pagination.activity_type,
      };

      if (!this.state.showDefaultView) {
        prevParams = {
          ...prevParams,
          ordering: this.state.customViewObject.ordering,
          search: this.state.customViewObject.search,
          customer: this.state.customViewObject.customer,
          assigned_to: this.state.customViewObject.assigned_to,
          created_by: this.state.customViewObject.created_by,
          status: this.state.customViewObject.status,
          priority: this.state.customViewObject.priority,
          due_date_after: this.state.customViewObject.due_date_after,
          due_date_before: this.state.customViewObject.due_date_before,
          activity_type: this.state.pagination.activity_type,
        }
      }

      if (!showAllActivities) {
        prevParams = {
          ...prevParams,
          assigned_to: `${this.props.user ? this.props.user.id : ""}`,
        };
      } else {
        prevParams = {
          ...prevParams,
          assigned_to: this.state.pagination.assigned_to,
        };
      }

      if (this.state.showAllCompletedProjects) {
        prevParams = {
          ...prevParams,
          status: this.state.closedStatues.join(",")
        }
      }

      this.props.fetchSalesActivity(prevParams).then((action) => {
        if (action.response) {
          let projects = [];
          if (clearData) {
            projects = [...action.response.results];
          } else {
            projects = [...this.state.projects, ...action.response.results];
          }
          const newPagination = {
            currentPage: action.response.links.page_number,
            nextPage: action.response.links.next_page_number,
            page_size: this.state.pagination.page_size,
            ordering: this.state.pagination.ordering,
            search: this.state.pagination.search,
            customer: this.state.pagination.customer,
            assigned_to: this.state.pagination.assigned_to,
            created_by: this.state.pagination.created_by,
            status: this.state.pagination.status,
            priority: this.state.pagination.priority,
            due_date_after: this.state.pagination.due_date_after,
            due_date_before: this.state.pagination.due_date_before,
            activity_type: this.state.pagination.activity_type,
          };

          this.setState({
            pagination: newPagination,
            projects,
            noData: projects.length === 0 ? true : false,
            loading: false,
          });
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  fetchByOrder = (field) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true);
      }
    );
  };
  getOrderClass = (field) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  changePage = (show: boolean) => {
    const newstate = this.getEmptyState();
    newstate.showAllActivities = show;
    newstate.customerOptions = this.state.customerOptions;
    newstate.statusSettingOptions = this.state.statusSettingOptions;
    newstate.prioritySettingOptions = this.state.prioritySettingOptions;
    newstate.statusOptions = this.state.statusOptions;
    newstate.priorityOptions = this.state.priorityOptions;
    newstate.customViewObject = this.state.customViewObject;

    newstate.showDefaultView = this.state.showDefaultView;

    // (newstate as any).pagination.status = this.state.openStatues.join(",");

    if (!this.state.showDefaultView) {
      newstate.pagination = {
        ...this.state.customViewObject
      }
      newstate.showSearch = this.state.customViewObject.showSearch;
      newstate.showGroupBy = this.state.customViewObject.showGroupBy;
      newstate.groupBy = this.state.customViewObject.groupBy;
      newstate.inputValue = this.state.customViewObject.search;

      newstate.selectedDueDate = this.state.customViewObject.selectedDueDate;
      newstate.selectedDueDateOption = this.state.customViewObject.selectedDueDateOption;
      newstate.showAllCompletedProjects = this.state.customViewObject.showAllCompletedProjects;
      newstate.activeCustomerFilter = this.state.customViewObject.activeCustomerFilter;
      newstate.activeAssignedToFilter = this.state.customViewObject.activeAssignedToFilter;
      newstate.activeCreatedByFilter = this.state.customViewObject.activeCreatedByFilter;
      newstate.activeStatusFilter = this.state.customViewObject.activeStatusFilter;
      newstate.activePriorityFilter = this.state.customViewObject.activePriorityFilter;
      newstate.activeTypeOfWorkStr = this.state.customViewObject.activeTypeOfWorkStr;
      newstate.customerStr = this.state.customViewObject.customerStr;
      newstate.statusStr = this.state.customViewObject.statusStr;
      newstate.priorityStr = this.state.customViewObject.priorityStr;
      newstate.assignedToStr = this.state.customViewObject.assignedToStr;
      newstate.createdByStr = this.state.customViewObject.createdByStr;
      newstate.activeTypeOfWorkStr = this.state.customViewObject.activeTypeOfWorkStr;
    }

    this.props.history.push(`?showAllActivities=${show}`);
    this.setState(newstate, () => {
      this.fetchMoreData(true);
    });
  };
  goToDetails = (project) => {
    if (project) {
      // go to edit page
      this.props.history.push(
        `sales-activity/${project.id}/edit?showAllActivities=${this.state.showAllActivities}`,
        project
      );
    } else {
      // go to create page
      this.props.history.push(
        `sales-activity/create?showAllActivities=${this.state.showAllActivities}`
      );
    }
  };
  renderTopBar = () => {
    const showAllActivities = this.state.showAllActivities;

    return (
      <div className="sales-activity-action-tabs">
        <div
          className={`sales-activity-action-tabs-link ${!showAllActivities ? "--active" : ""
            }`}
          onClick={() => this.changePage(false)}
        >
          My Activities
        </div>
        <div
          className={`sales-activity-action-tabs-link ${showAllActivities ? "--active" : ""
            }`}
          onClick={() => this.changePage(true)}
        >
          All Activities
        </div>
      </div>
    );
  };
  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };

  applyFilters = (filters) => {
    let due_date_before, due_date_after;

    if (filters.selectedDueDateOption) {
      if (filters.selectedDueDateOption === "thisWeek" || filters.selectedDueDateOption === "thisMonth") {
        const filterType = filters.selectedDueDateOption === "thisWeek" ? "isoWeek" : "month"
        due_date_before = moment().clone().endOf(filterType);
        due_date_after = due_date_before.clone().startOf(filterType);
      }

      if (filters.selectedDueDateOption === "before") {
        due_date_before = filters.selectedDueDate;
        due_date_after = moment("1970-01-01", 'YYYY-MM-DD');
      }

      if (filters.selectedDueDateOption === "after") {
        due_date_after = filters.selectedDueDate;
      }
    }

    this.setState(
      {
        selectedDueDate: filters.selectedDueDate,
        selectedDueDateOption: filters.selectedDueDateOption,
        showAllCompletedProjects: filters.showAllCompletedProjects,
        activeTypeOfWorkFilter: filters.typeOfWork,
        activeCustomerFilter: filters.customer,
        activeAssignedToFilter: filters.assignedTo,
        activeCreatedByFilter: filters.createdBy,
        activeStatusFilter: filters.status,
        activePriorityFilter: filters.priority,
        customerStr: filters.customerStr,
        activeTypeOfWorkStr: filters.activeTypeOfWorkStr,
        // projectManagerStr: "",
        statusStr: filters.statusStr,
        priorityStr: filters.priorityStr,
        assignedToStr: filters.assignedToStr,
        createdByStr: filters.createdByStr,
        pagination: {
          ...this.state.pagination,
          customer: filters.customer.join(","),
          activity_type: filters.typeOfWork.join(","),
          // manager: filters.projectManagerStr,
          status: filters.status.join(","),
          priority: filters.priority.join(","),
          assigned_to: filters.assignedTo.join(","),
          created_by: filters.createdBy.join(","),
          due_date_after: due_date_after && due_date_after.format('YYYY-MM-DD'),
          due_date_before: due_date_before && due_date_before.format('YYYY-MM-DD'),
        },
      },
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  removeFilter = (filterType: string) => {
    if (filterType === "showAllCompletedProjects") {
      this.setState(
        {
          showAllCompletedProjects: false,
          customViewObject: {
            ...this.state.customViewObject,
            showAllCompletedProjects: false
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "customer") {
      this.setState(
        {
          activeCustomerFilter: [],
          customerStr: "",
          pagination: {
            ...this.state.pagination,
            customer: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            activeCustomerFilter: [],
            customerStr: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "assignedTo") {
      this.setState(
        {
          activeAssignedToFilter: [],
          assignedToStr: "",
          pagination: {
            ...this.state.pagination,
            assigned_to: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            assigned_to: "",
            activeAssignedToFilter: [],
            assignedToStr: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "createdBy") {
      this.setState(
        {
          activeCreatedByFilter: [],
          createdByStr: "",
          pagination: {
            ...this.state.pagination,
            created_by: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            created_by: "",
            activeCreatedByFilter: [],
            createdByStr: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "status") {
      this.setState(
        {
          activeStatusFilter: [],
          statusStr: "",
          pagination: {
            ...this.state.pagination,
            status: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            status: "",
            activeStatusFilter: [],
            statusStr: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "priority") {
      this.setState(
        {
          activePriorityFilter: [],
          priorityStr: "",
          pagination: {
            ...this.state.pagination,
            priority: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            activePriorityFilter: [],
            priorityStr: "",
            priority: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    // if (filterType === "projectManager") {
    //   this.setState(
    //     {
    //       activeProjectManagerFilter: [],
    //       pagination: {
    //         ...this.state.pagination,
    //         manager: "",
    //       },
    //     },
    //     () => {
    //       this.fetchMoreData(true);
    //     }
    //   );
    // }

    if (filterType === "dueDateOption") {
      this.setState(
        {
          selectedDueDate: undefined,
          selectedDueDateOption: undefined,
          pagination: {
            ...this.state.pagination,
            due_date_after: "",
            due_date_before: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            selectedDueDate: undefined,
            selectedDueDateOption: undefined,
            due_date_after: "",
            due_date_before: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "activityType") {
      this.setState(
        {
          activeTypeOfWorkFilter: [],
          activeTypeOfWorkStr: "",
          pagination: {
            ...this.state.pagination,
            activity_type: "",
          },
          customViewObject: {
            ...this.state.customViewObject,
            activeTypeOfWorkFilter: [],
            activeTypeOfWorkStr: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }
  };

  clearFilters = () => {
    this.setState(
      {
        showAllCompletedProjects: false,
        selectedDueDate: undefined,
        selectedDueDateOption: undefined,
        showFiltersPop: false,
        activeCustomerFilter: [],
        activeAssignedToFilter: [],
        activeCreatedByFilter: [],
        activeStatusFilter: [],
        activePriorityFilter: [],
        activeTypeOfWorkFilter: [],
        customerStr: "",
        assignedToStr: "",
        createdByStr: "",
        statusStr: "",
        priorityStr: "",
        activeTypeOfWorkStr: "",
        pagination: {
          ...this.state.pagination,
          type: "",
          customer: "",
          manager: "",
          due_date_after: "",
          due_date_before: "",
          status: "",
          priority: "",
          assigned_to: "",
          created_by: "",
          activity_type: ""
        },
        customViewObject: {
          ...this.state.customViewObject,
          showAllCompletedProjects: false,
          selectedDueDate: undefined,
          selectedDueDateOption: undefined,
          activeCustomerFilter: [],
          activeAssignedToFilter: [],
          activeCreatedByFilter: [],
          activeStatusFilter: [],
          activePriorityFilter: [],
          activeTypeOfWorkFilter: [],
          customerStr: "",
          assignedToStr: "",
          createdByStr: "",
          statusStr: "",
          priorityStr: "",
          type: "",
          customer: "",
          manager: "",
          due_date_after: "",
          due_date_before: "",
          status: "",
          priority: "",
          assigned_to: "",
          created_by: "",
          activeTypeOfWorkStr: ""
        }
      },
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  onEditClick = (e, activity) => {
    e.stopPropagation();
    this.goToDetails(activity);
  };

  onCloseActivityClick = (e, activity) => {
    e.stopPropagation();

    const data = { ...activity };

    const defaultCloseSetting = this.state.statusOptions.filter((obj) => {
      return obj.is_default_closed_status;
    })[0];

    data.status = defaultCloseSetting.id;
    data.priority = data.priority.id;
    data.customer = data.customer.id;
    data.assigned_to = data.assigned_to.id;
    data.created_by = data.created_by.id;

    this.props.updateSalesActivity(data, data.id).then((action) => {
      if (action.type === UPDATE_SALES_ACTIVITY_SUCCESS) {
        setTimeout(() => {
        this.fetchMoreData(true);
      },3000);
        this.setState({ message: "Update Successfully", messageId: data.id });
    
        setTimeout(() => {
          this.setState({ message: "", messageId: undefined });
        }, 15000);
      }
    });

    this.setState({
      showRowActions: false,
      rowEditId: undefined,
    }) 

  };

  projectListingRows = (project, index) => {
    return (
      <div
        className={`row-panel ${this.state.showAllActivities ? "column-11" : "column-10-sales-activity"} `}
        key={project.id}
      >
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-status status color-preview" title={project.priority.title}>
          <div
            style={{
              backgroundColor: project.priority.color,
              borderColor: project.priority.color,
            }}
            className="left-column ellipsis-no-width"
          />
          <div
            style={{
              background: `${getConvertedColorWithOpacity(
                project.priority.color
              )}`,
            }}
            className="ellipsis-no-width"
          >
            {project.priority.title}
          </div>
        </div>
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-status sales-status status color-preview" title={project.status && project.status.title}>
          <div
            style={{
              backgroundColor: project.status && project.status.color,
              borderColor: project.status && project.status.color,
            }}
            className="left-column"
          />
          <div
            style={{
              background: `${getConvertedColorWithOpacity(
                project.status && project.status.color
              )}`,
            }}
            className="ellipsis-no-width"
          >
            {project.status && project.status.title}
          </div>
        </div>
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-project-name">
          <div className="type ellipsis-no-width" title={project.customer.name}>
            {project.customer.name}
          </div>
        </div>
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-project-name">
          <div className="type ellipsis-no-width" title={project.customer}>
            {project.customer.account_manager_name || "-"}
          </div>
        </div>
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-project-name">
          <div className="type ellipsis-no-width" title={project.description}>
            {project.description}
          </div>
        </div>
        {this.state.showAllActivities && (
          <div onClick={(e) => this.onEditClick(e, project)} className="manager">
            <div
              style={{
                backgroundImage: `url(${project.assigned_to.profile_pic
                    ? `${project.assigned_to.profile_pic}`
                    : "/assets/icons/user-filled-shape.svg"
                  })`,
              }}
              className="pm-image-circle"
            />
            <div className="user-name" title={project.assigned_to.name}>
              {project.assigned_to.name}
            </div>
          </div>
        )}
        <div onClick={(e) => this.onEditClick(e, project)} className="manager">
          <div
            style={{
              backgroundImage: `url(${project.created_by.profile_pic
                  ? `${project.created_by.profile_pic}`
                  : "/assets/icons/user-filled-shape.svg"
                })`,
            }}
            className="pm-image-circle"
          />
          <div className="user-name" title={project.created_by.name}>
            {project.created_by.name}
          </div>
        </div>
        <div
          className="sales-activity-project-name due-date"
          onClick={(e) => this.onEditClick(e, project)}
          style={{
            background: moment().isAfter(project.due_date) && "rgba(255, 0, 0, 0.2)"
          }}
        >
          {toFormattedDate(
            project.due_date,
            "MMM DD, YYYY"
          )}
        </div>
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-project-age">
          {project.age}
        </div>
        <div onClick={(e) => this.onEditClick(e, project)} className="sales-activity-project-name">
          {fromISOStringToFormattedDate(
            project.updated_on,
            "MMM DD, YYYY"
          ) || "-"}
        </div>
        <div className="ellipse-actions" onClick={() => {
                let showPopUp = false;
                let editedRow = project.id

                if (this.state.rowEditId === project.id) editedRow = undefined; showPopUp = false;

                if (this.state.rowEditId !== project.id) editedRow = project.id; showPopUp = true;

                this.setState({
                  showRowActions: showPopUp,
                  rowEditId: editedRow
                })
              }}>
          {this.state.messageId !== project.id && Array(3).fill(0).map((_, i) =>
            <div
              className="ellipse-item"
              style={{
                backgroundColor: this.state.showRowActions && this.state.rowEditId === project.id && "#41a2dd"
              }}
              key={i}
            />
          )}
          {
            this.state.message !== "" && this.state.messageId === project.id && (
              <div className="action-status-message-sales">
                <div className="message-text">{this.state.message}</div>
              </div>
            )
          }
          {
            this.state.showRowActions && this.state.rowEditId === project.id && (
              <div className="rowellipse-actions" >
                <div
                  className="edit-activity"
                  onClick={(e) => this.onEditClick(e, project)}
                >
                  Edit
                </div>
                <div
                  className="close-activity"
                  onClick={(e) => this.onCloseActivityClick(e, project)}
                >
                  Close Activity
                </div>
              </div>
            )
          }
        </div>
      </div>
    );
  };

  renderFilters = () => {
    return (
      <div className="top-bar-active-filter">
        {this.state.showAllCompletedProjects && (
          <div className="applied-filter">
            Show Completed Projects:
            <span className="filters">{`True`}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("showAllCompletedProjects")}
            />
          </div>
        )}
        {this.state.activeTypeOfWorkFilter &&
          this.state.activeTypeOfWorkFilter.length > 0 && (
            <div className="applied-filter">
              Activity Type:
              <span className="filters">{this.state.activeTypeOfWorkStr}</span>
              <img
                className="top-filter-delete"
                alt=""
                src={"/assets/icons/close.png"}
                onClick={() => this.removeFilter("activityType")}
              />
            </div>
          )}
        {this.state.activeCustomerFilter.length > 0 && (
          <div className="applied-filter">
            Customer:
            <span className="filters">{this.state.customerStr}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("customer")}
            />
          </div>
        )}
        {this.state.activeAssignedToFilter.length > 0 && (
          <div className="applied-filter">
            Assigned To:
            <span className="filters">{this.state.assignedToStr}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("assignedTo")}
            />
          </div>
        )}
        {this.state.activeCreatedByFilter.length > 0 && (
          <div className="applied-filter">
            Created By:
            <span className="filters">{this.state.createdByStr}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("createdBy")}
            />
          </div>
        )}
        {this.state.activePriorityFilter.length > 0 && (
          <div className="applied-filter">
            Priority:
            <span className="filters">{this.state.priorityStr}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("priority")}
            />
          </div>
        )}
        {this.state.activeStatusFilter.length > 0 && (
          <div className="applied-filter">
            Status:
            <span className="filters">{this.state.statusStr}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("status")}
            />
          </div>
        )}
        {this.state.selectedDueDateOption && (
          <div className="applied-filter">
            Due Date:
            <span className="filters">
              {["before", "olderThan"].includes(
                this.state.selectedDueDateOption
              )
                ? `Before - ${this.state.pagination.due_date_before}`
                : ["after"].includes(this.state.selectedDueDateOption)
                ? `After - ${this.state.pagination.due_date_after}`
                : `Between ${this.state.pagination.due_date_after} to ${this.state.pagination.due_date_before}`}
            </span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("dueDateOption")}
            />
          </div>
        )}
        <div className="clear-action" onClick={() => this.clearFilters()}>
          Clear All
        </div>
      </div>
    );
  };

  render() {
    let groupedByInvalid = this.groupBy(
      this.state.projects,
      this.state.groupBy
    );

    const groupByOptions = ["assigned_to__first_name", "created_by__name", "customer__account_manager_name"];

    if (groupByOptions.includes(this.state.groupBy)) {
      if ("assigned_to__first_name" === this.state.groupBy) {
        groupedByInvalid = this.state.projects.reduce((a, c) => {
          a[c.assigned_to.name] = a[c.assigned_to.name] || [];
          a[c.assigned_to.name].push(c)
          return a;
        }, Object.create(null));
      }

      if ("created_by__name" === this.state.groupBy) {
        groupedByInvalid = this.state.projects.reduce((a, c) => {
          a[c.created_by.name] = a[c.created_by.name] || [];
          a[c.created_by.name].push(c)
          return a;
        }, Object.create(null));
      }

      if ("customer__account_manager_name" === this.state.groupBy) {
        groupedByInvalid = this.state.projects.reduce((a, c) => {
          a[c.customer.account_manager_name] = a[c.customer.account_manager_name] || [];
          a[c.customer.account_manager_name].push(c)
          return a;
        }, Object.create(null));
      }
    }

    return (
      <div className="sales-activity-management sales-activity-list-panel">
        <div className="header-actions">
          <div className="left">{this.renderTopBar()}</div>
          <div className="right">
            <div
              className="action-btn filter"
              onClick={() =>
                this.setState({ showFiltersPop: !this.state.showFiltersPop })
              }
            >
              <img
                alt=""
                className="img-icon"
                src="/assets/icons/filter-filled-tool-symbol.svg"
              />
              Filter
            </div>
            {this.state.showFiltersPop && (
              <Filters
                show={this.state.showFiltersPop}
                selectedDueDate={this.state.selectedDueDate}
                selectedDueDateOption={this.state.selectedDueDateOption}
                showAllCompletedProjects={this.state.showAllCompletedProjects}
                assignedToOptions={this.props.providerUsers}
                createdByOptions={this.props.providerUsers}
                isFetchingUsers={this.props.isFetchingUsers}
                customerOptions={this.state.customerOptions}
                typeOfActivityOptions={this.typeOfActivityOptions}
                isFetchingCustomers={this.state.fetchingCustomers}
                statusOptions={this.state.statusSettingOptions}
                isFetchingStatusOptions={this.state.fetchingStatusSettings}
                priorityOptions={this.state.prioritySettingOptions}
                isFetchingPriorityOptions={this.state.fetchingCustomers}
                setPopUpState={(show) =>
                  this.setState({ showFiltersPop: show })
                }
                activeAssignedToFilter={this.state.activeAssignedToFilter}
                activeCreatedByFilter={this.state.activeCreatedByFilter}
                activeCustomerFilter={this.state.activeCustomerFilter}
                activeStatusFilter={this.state.activeStatusFilter}
                activePriorityFilter={this.state.activePriorityFilter}
                activeTypeOfWorkFilter={this.state.activeTypeOfWorkFilter}
                applyFilters={this.applyFilters}
              />
            )}
            |
            {this.state.showSearch && (
              <Input
                field={{
                  value: this.state.inputValue,
                  label: "",
                  type: "SEARCH",
                }}
                width={10}
                name="searchString"
                onChange={this.onSearchStringChange}
                onBlur={() => {
                  if (this.state.inputValue.trim() === "") {
                    this.setState({ showSearch: false });
                  }
                }}
                placeholder="Search"
                className="search custom-search"
              />
            )}
            {!this.state.showSearch && (
              <div
                className="action-btn"
                onClick={(e) =>
                  this.setState({ showSearch: true, inputValue: "" })
                }
              >
                <img
                  alt=""
                  className="img-icon"
                  src="/assets/icons/search.svg"
                />
                Search
              </div>
            )}
            |
            {!this.state.showGroupBy && (
              <div
                className="action-btn-group"
                onClick={(e) =>
                  this.setState({ showGroupBy: true, groupBy: "" })
                }
              >
                <img
                  alt=""
                  className="img-icon"
                  src="/assets/icons/folder.svg"
                />
                Group By
              </div>
            )}
            {this.state.showGroupBy && (
              <Input
                field={{
                  label: "",
                  type: "PICKLIST",
                  value: this.state.groupBy,
                  isRequired: false,
                  options: [
                    { label: "Account Manager", value: "customer__account_manager_name" },
                    { label: "Assigned To", value: "assigned_to__first_name" },
                    { label: "Assigned By", value: "created_by__name" },
                    { label: "Due Date", value: "due_date" },
                  ],
                }}
                width={10}
                labelIcon={"info"}
                name="status"
                onChange={(e) =>
                  this.setState({
                    groupBy: e.target.value,
                    customViewObject: {
                      ...this.state.customViewObject,
                      groupBy: e.target.value
                    }
                  }, () => {
                    this.fetchByOrder(e.target.value);
                  })
                }
                placeholder={`Group By`}
                className="group-by"
                clearable={true}
                onBlur={() => {
                  if (!this.state.groupBy) {
                    this.setState({ showGroupBy: false });
                  }
                }}
              />
            )}
            {/* <SquareButton
                content="+"
                onClick={e => this.goToDetails(undefined)}
                className="add-new-activity"
                bsStyle={"primary"}
                title="Add new activity"
                disabled={false}
            /> */}
            <div onClick={e => this.goToDetails(undefined)} className="add-new-activity">
              +
            </div>
          </div>
        </div>
        <div className="sales-activity-list">
          {(this.state.activeAssignedToFilter.length > 0 ||
            this.state.activeCustomerFilter.length > 0 ||
            this.state.activeCreatedByFilter.length > 0 ||
            this.state.activePriorityFilter.length > 0 ||
            this.state.activeStatusFilter.length > 0 ||
            this.state.activeTypeOfWorkFilter.length > 0 ||
            this.state.showAllCompletedProjects ||
            this.state.selectedDueDateOption) && (
              <div className="applied-filters-row">
                <div>
                  Filters:
                  {this.renderFilters()}
                </div>
              </div>
            )}
          <div className="custom-view-action">
            View:
            <div
              className={`default-view ${this.state.showDefaultView && 'active-view'}`}
              onClick={(e) => {
                this.setState({
                  showDefaultView: true,
                  pagination: {
                    ...this.getEmptyState().pagination,
                  },
                  showSearch: this.getEmptyState().showSearch,
                  inputValue: this.getEmptyState().inputValue,
                  showGroupBy: this.getEmptyState().showGroupBy,
                  groupBy: this.getEmptyState().groupBy
                }, () => this.fetchMoreData(true))
              }}
            >
              Default
            </div>
            |
            {!this.state.customViewObject ?
              <div
                className={`custom-view ${!this.state.showDefaultView && 'active-view'}`}
                onClick={(e) => {
                  this.createCustomView();
                  this.setState({ showDefaultView: false })
                }}
              >
                Save as Custom View
              </div>
              :
              <>
                <div
                  className={`default-view ${!this.state.showDefaultView && 'active-view'}`}
                  onClick={(e) => {
                    this.setState({
                      showDefaultView: false,
                      pagination: {
                        ...this.state.pagination,
                        ...this.state.customViewObject
                      },
                      selectedDueDate: this.state.customViewObject.selectedDueDate,
                      selectedDueDateOption: this.state.customViewObject.selectedDueDateOption,
                      showAllCompletedProjects: this.state.customViewObject.showAllCompletedProjects,
                      activeCustomerFilter: this.state.customViewObject.activeCustomerFilter,
                      activeAssignedToFilter: this.state.customViewObject.activeAssignedToFilter,
                      activeCreatedByFilter: this.state.customViewObject.activeCreatedByFilter,
                      activeStatusFilter: this.state.customViewObject.activeStatusFilter,
                      activePriorityFilter: this.state.customViewObject.activePriorityFilter,
                      activeTypeOfWorkFilter: this.state.customViewObject.activeTypeOfWorkFilter,
                      activeTypeOfWorkStr: this.state.customViewObject.activeTypeOfWorkStr,
                      customerStr: this.state.customViewObject.customerStr,
                      statusStr: this.state.customViewObject.statusStr,
                      priorityStr: this.state.customViewObject.priorityStr,
                      assignedToStr: this.state.customViewObject.assignedToStr,
                      createdByStr: this.state.customViewObject.createdByStr,
                      inputValue: this.state.customViewObject.search,
                      showSearch: this.state.customViewObject.showSearch,
                      showGroupBy: this.state.customViewObject.showGroupBy,
                      groupBy: this.state.customViewObject.groupBy
                    }, () => this.fetchMoreData(true))
                  }}
                >
                  Custom
                </div>
                <div
                  className={`custom-view`}
                  onClick={(e) => {
                    this.setState({ showDefaultView: false });
                    this.createCustomView();
                    // this.setState({
                    //   pagination: {
                    //     ...this.state.pagination,
                    //     ...this.state.customViewObject
                    //   },
                    //   inputValue: this.state.customViewObject.search,
                    //   showSearch: this.state.customViewObject.showSearch,
                    //   showGroupBy: this.state.customViewObject.showGroupBy,
                    //   groupBy: this.state.customViewObject.groupBy
                    // }, () => this.fetchMoreData(true))
                  }}
                >
                  Update Custom
                </div>
              </>
            }
          </div>
          <div
            className={`header ${this.state.showAllActivities ? "column-11" : "column-10-sales-activity"
              }`}
          >
            {/* <div className="title" /> */}
            <div
              className={`sales-activity-title ${this.getOrderClass("priority__rank")}`}
              onClick={() => this.fetchByOrder("priority__rank")}
            >
              Priority
            </div>
            <div
              className={`sales-activity-title ${this.getOrderClass("status__title")}`}
              onClick={() => this.fetchByOrder("status__title")}
            >
              Status
            </div>
            <div
              className={`sales-activity-title ${this.getOrderClass(
                "customer__name"
              )}`}
              onClick={() => this.fetchByOrder("customer__name")}
            >
              {" "}
              Customer
            </div>
            <div
              className={`sales-activity-title ${this.getOrderClass(
                "customer__account_manager_name"
              )}`}
              onClick={() => this.fetchByOrder("customer__account_manager_name")}
            >
              {" "}
              Account Manager
            </div>
            <div
              className={`sales-activity-title ${this.getOrderClass("title")}`}
            >
              {" "}
              Description
            </div>
            {this.state.showAllActivities && (
              <div
                className={`sales-activity-title ${this.getOrderClass(
                  "assigned_to__first_name"
                )}`}
                title={"Assigned To"}
                onClick={() => this.fetchByOrder("assigned_to__first_name")}
              >
                {" "}
                Assigned To
              </div>
            )}
            <div className={`sales-activity-title ${this.getOrderClass("created_by")}`} title={"Created By"}
              onClick={() => this.fetchByOrder("created_by")}
            >
              {" "}
              Created By
            </div>
            <div
              className={`sales-activity-title ${this.getOrderClass("due_date")}`}
              onClick={() => this.fetchByOrder("due_date")}
            >
              {" "}
              Due Date
            </div>
            <div
              className={`sales-activity-title ${this.getOrderClass("age")}`}
              onClick={() => this.fetchByOrder("age")}
            >
              {" "}
              Age
            </div>
            <div
              className={`sales-activity-title  ${this.getOrderClass("updated_on")}`}
              onClick={() => this.fetchByOrder("updated_on")}
            >
              Last Update
            </div>
            <div
              className={`actions  ${this.getOrderClass("last_action_date")}`}
            // onClick={() => this.fetchByOrder("last_action_date")}
            />
          </div>
          <div id="scrollableDiv" style={{ position: "relative", height: "72vh", overflow: "auto" }}>
            <InfiniteScroll
              dataLength={this.state.projects.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
            >
              {!this.state.groupBy &&
                this.state.projects.map((project, index) =>
                  this.projectListingRows(project, index)
                )}
              {this.state.groupBy &&
                groupedByInvalid &&
                !isEmpty(groupedByInvalid) &&
                Object.keys(groupedByInvalid).map((data, i) => (
                  <div key={i} className="voilation-statistics col-md-12">
                    <PMOCollapsible
                      background={getBackgroundColor(
                        data
                      )}
                      label={
                        data !== "null" && data || "Activites"
                      }
                      isOpen={true}
                    >
                      {groupedByInvalid[data].map((project, index) =>
                        this.projectListingRows(project, index)
                      )}
                    </PMOCollapsible>
                  </div>
                ))}
              {!this.state.loading &&
                this.state.pagination.nextPage &&
                this.state.projects.length > 0 && (
                  <div
                    className="load-more"
                    onClick={() => {
                      this.fetchMoreData(false);
                    }}
                  >
                    load more data...
                  </div>
                )}
            </InfiniteScroll>
            {this.state.noData && this.state.projects.length === 0 && (
              <div className="no-data">No Activites Available</div>
            )}
            {this.state.loading && (
              <div className="loader">
                <Spinner show={true} />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetchingUsers: state.providerUser.isFetchingUsers,
  rules: state.configuration.rules,
  isFetching: state.configuration.isFetching,
  additionalSetting: state.pmo.additionalSetting,
  user: state.profile.user,
  providerUsers: state.providerUser.providerUsers,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStageList: () => dispatch(getQuoteStageList()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  fetchSalesActivity: (params?: IScrollPaginationFilters) =>
    dispatch(fetchSalesActivity(params)),
  fetchCustomersShort: () => dispatch(fetchCustomersShort()),
  getSalesActivityPrioritySetting: () => dispatch(getSalesActivityPrioritySetting()),
  getSalesActivityStatusSetting: () => dispatch(getSalesActivityStatusSetting()),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  updateSalesActivity: (data: any, id: number) =>
    dispatch(updateSalesActivity(data, id)),

  getSalesActivityCustomView: () => dispatch(getSalesActivityCustomView()),
  postSalesActivityCustomView: (data: any) =>
    dispatch(postSalesActivityCustomView(data)),
  deleteSalesActivityCustomView: (id: number) =>
    dispatch(deleteSalesActivityCustomView(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyProjects);
