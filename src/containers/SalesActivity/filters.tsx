import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import SquareButton from '../../components/Button/button';
import Checkbox from '../../components/Checkbox/checkbox';
import SelectInput from '../../components/Input/Select/select';
import Input from '../../components/Input/input';

import './style.scss';

interface IProjectFiltersProps {
    show: boolean;
    setPopUpState: any;
    assignedToOptions?: any;
    createdByOptions?: any;
    customerOptions?: any;
    typeOfActivityOptions?: any;
    selectedDueDateOption: any;
    selectedDueDate: any;
    statusOptions?: any;
    priorityOptions?: any;
    isFetchingUsers: boolean;
    isFetchingCustomers: boolean;
    isFetchingStatusOptions: boolean;
    isFetchingPriorityOptions: boolean;
    showAllCompletedProjects: boolean;

    activeCustomerFilter?: any;

    activeAssignedToFilter?: any;
    activeCreatedByFilter?: any;
    activeStatusFilter?: any;
    activePriorityFilter?: any;
    activeTypeOfWorkFilter?: any;

    applyFilters: any;
}

interface IProjectFiltersState {
    list: any;
    showAllCompletedProjects: any;
    activeFilterOption: any;
    fetchingCustomers: any;
    fetchingAssignedTo: any;
    fetchingCreatedBy: any;
    
    selectedAssignedTo: any;
    selectedCreatedBy: any;
    selectedCustomer: any;

    selectedAssignedToArr: any;
    selectedCreatedByArr: any;
    selectedCustomerArr: any;

    selectedDueDateOption: any;
    selectedDueDate: any;

    selectedStatus: any;
    selectedStatusArr: any;

    selectedPriority: any;
    selectedPriorityArr: any;

    selectedTypeOfActivity: any;
    selectedTypeOfActivityArr: any;
}

class ProjectFilters extends React.Component<
    IProjectFiltersProps,
    IProjectFiltersState
> {
    constructor(props: IProjectFiltersProps) {
        super(props);
        this.state = this.getEmptyState();
    }

    getEmptyState = () => ({
        list: [],
        showAllCompletedProjects: false,
        activeFilterOption: "assignedTo",
        fetchingCustomers: false,
        fetchingAssignedTo: false,
        fetchingCreatedBy: false,
        
        selectedAssignedTo: undefined,
        selectedCreatedBy: undefined,
        selectedCustomer: undefined,

        selectedAssignedToArr: [],
        selectedCreatedByArr: [],
        selectedCustomerArr: [],
        selectedStatusArr: [],
        selectedPriorityArr: [],
        selectedTypeOfActivityArr: [],

        selectedDueDateOption: undefined,
        selectedDueDate: undefined,
        selectedStatus: undefined,
        selectedPriority: undefined,
        selectedTypeOfActivity: undefined
    });

    componentDidMount() {
        this.setState({
            selectedDueDate: this.props.selectedDueDate,
            selectedDueDateOption: this.props.selectedDueDateOption,
            showAllCompletedProjects: this.props.showAllCompletedProjects,
            selectedAssignedToArr: [...this.props.activeAssignedToFilter],
            selectedCreatedByArr: [...this.props.activeCreatedByFilter],
            selectedCustomerArr: [...this.props.activeCustomerFilter],
            selectedStatusArr: [...this.props.activeStatusFilter],
            selectedPriorityArr: [...this.props.activePriorityFilter],
            selectedTypeOfActivityArr: [...this.props.activeTypeOfWorkFilter],
        });
    }

    handleChange = (event: any) => {
        const newState: IProjectFiltersState = cloneDeep(this.state);        
        (newState[event.target.name] as string) = event.target.value;
        this.setState(newState);
    };

    onApplyClick = (e: any) => {
        e.preventDefault();
        this.props.setPopUpState(false);
        
        const assignedToStr = [];
        this.state.selectedAssignedToArr.forEach((value) => {
            this.props.assignedToOptions.forEach((obj) => {
                if (obj.id === value) {
                    assignedToStr.push(`${obj.first_name} ${obj.last_name}`)
                }
            });
        });

        const createdByStr = [];
        this.state.selectedCreatedByArr.forEach((value) => {
            this.props.createdByOptions.forEach((obj) => {
                if (obj.id === value) {
                    createdByStr.push(`${obj.first_name} ${obj.last_name}`)
                }
            });
        });

        const customerStr = [];
        this.state.selectedCustomerArr.forEach((value) => {
            this.props.customerOptions.forEach((obj) => {
                if (obj.value === value) {
                    customerStr.push(obj.label)
                }
            });
        });

        const statusStr = [];
        this.state.selectedStatusArr.forEach((value) => {
            this.props.statusOptions.forEach((obj) => {
                if (obj.value === value) {
                    statusStr.push(obj.label)
                }
            });
        });

        const priorityStr = [];
        this.state.selectedPriorityArr.forEach((value) => {
            this.props.priorityOptions.forEach((obj) => {
                if (obj.value === value) {
                    priorityStr.push(obj.label)
                }
            });
        });

        const typeOfWorkStr = [];
        this.state.selectedTypeOfActivityArr.forEach((value) => {
            this.props.typeOfActivityOptions.forEach((obj) => {
                if (obj.value === value) {
                    typeOfWorkStr.push(obj.label)
                }
            });
        });

        // const projectManagerStr = [];
        // this.state.selectedProjectManagerArr.forEach((value) => {
        //     this.props.projectManagerOptions.forEach((obj) => {
        //         if (obj.value === value) {
        //             projectManagerStr.push(obj.label)
        //         }
        //     });
        // });

        this.props.applyFilters({
            selectedDueDate: this.state.selectedDueDate,
            selectedDueDateOption: this.state.selectedDueDateOption,
            showAllCompletedProjects: this.state.showAllCompletedProjects,
            customer: this.state.selectedCustomerArr,
            customerStr: customerStr.join(),

            assignedTo: this.state.selectedAssignedToArr,
            assignedToStr: assignedToStr.join(),

            createdBy: this.state.selectedCreatedByArr,
            createdByStr: createdByStr.join(),

            status: this.state.selectedStatusArr,
            statusStr: statusStr.join(),

            priority: this.state.selectedPriorityArr,
            priorityStr: priorityStr.join(),

            typeOfWork: this.state.selectedTypeOfActivityArr,
            activeTypeOfWorkStr: typeOfWorkStr.join()
        });
    };

    onCancelClick = (e: any) => {
        e.preventDefault();
        this.props.setPopUpState(false);
    }

    onFiltersClose = () => {

    }

    handleChangeCheckBox = (e: any) => {
        const newState = cloneDeep(this.state);

        (newState[e.target.name] as any) = e.target.checked;
        this.setState(newState);
    };

    getCustomerUserOptions = () => {
        const users = this.props.customerOptions
          ? this.props.customerOptions.map(t => ({
            value: t.id,
            label: `${t.first_name} ${t.last_name}`,
            disabled: false,
          }))
          : [];
    
        return users;
    };

    handleAssignedToChange = (e: any, type: string) => { 
        this.setState(prevState => ({
            selectedAssignedTo: e.target.value,
            selectedAssignedToArr: [ ...prevState.selectedAssignedToArr, e.target.value ],
        }));
    }

    handleProjectCustomerChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedCustomer: e.target.value,
            selectedCustomerArr: [ ...prevState.selectedCustomerArr, e.target.value ],
        }));
    }

    handleCreatedByChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedCreatedBy: e.target.value,
            selectedCreatedByArr: [ ...prevState.selectedCreatedByArr, e.target.value ],
        }));
    }

    handleStatusChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedStatus: e.target.value,
            selectedStatusArr: [ ...prevState.selectedStatusArr, e.target.value ],
        }));
    }

    handlePriorityChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedPriority: e.target.value,
            selectedPriorityArr: [ ...prevState.selectedPriorityArr, e.target.value ],
        }));
    }

    handleTypeOfActivityChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedTypeOfActivity: e.target.value,
            selectedTypeOfActivityArr: [ ...prevState.selectedTypeOfActivityArr, e.target.value ],
        }));
    }

    removeFilter = (filter: any, type: string) => {
        if (type === "assignedTo") {
            this.setState({
                selectedAssignedTo: undefined,
                selectedAssignedToArr: this.state.selectedAssignedToArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "customer") {
            this.setState({
                selectedCustomer: undefined,
                selectedCustomerArr: this.state.selectedCustomerArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "createdBy") {
            this.setState({
                selectedCreatedBy: undefined,
                selectedCreatedByArr: this.state.selectedCreatedByArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "status") {
            this.setState({
                selectedStatus: undefined,
                selectedStatusArr: this.state.selectedStatusArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "priority") {
            this.setState({
                selectedPriority: undefined,
                selectedPriorityArr: this.state.selectedPriorityArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "activityType") {
            this.setState({
                selectedTypeOfActivity: undefined,
                selectedTypeOfActivityArr: this.state.selectedTypeOfActivityArr.filter((val) => { 
                    return val !== filter
            })});
        }
    }

    getAssignedToVal = (assignedTo) => {
        const type = this.props.assignedToOptions.find(obj => obj.id == assignedTo)
        if (type) {
            return `${type.first_name} ${type.last_name}`
        }
        return ""
    };

    getCreatedByVal = (createdBy) => {
        const type = this.props.createdByOptions.find(obj => obj.id == createdBy)
        if (type) {
            return `${type.first_name} ${type.last_name}`
        }
        return ""
    };

    getCustomerVal = (customer) => {
        const type = this.props.customerOptions.find(obj => obj.value == customer)
        if (type) {
            return type.label
        }
        return ""
    }

    getStatusVal = (status) => {
        const type = this.props.statusOptions.find(obj => obj.value == status)
        if (type) {
            return type.label
        }
        return ""
    }

    getPriorityVal = (priority) => {
        const type = this.props.priorityOptions.find(obj => obj.value == priority)
        if (type) {
            return type.label
        }
        return ""
    }

    getTypeOfActivityVal = (activityType) => {
        const type = this.props.typeOfActivityOptions.find(obj => obj.value == activityType)
        if (type) {
            return type.label
        }
        return ""
    }

    getProjectManagerVal = (manager) => {
        // const type = this.props.projectManagerOptions.find(obj => obj.value == manager)
        // if (type) {
        //     return type.label
        // }
        return ""
    }

    render() {

        return (
            <div className="sales-activity-filters-container-sales">
                <div className="header">
                    <div className="filter-heading">Filter By</div>
                    <Checkbox
                        isChecked={this.state.showAllCompletedProjects}
                        name="showAllCompletedProjects"
                        onChange={e => this.handleChangeCheckBox(e)}
                    >
                        Show All Completed Activities
                    </Checkbox>
                </div>
                <div className="body-container">
                    <div className="filter-left">
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "assignedTo" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'assignedTo'})}
                        >
                            <span className="filter-title">Assigned To</span>
                                {
                                    this.state.selectedAssignedToArr.length > 0 &&
                                        <div className="selection-count">
                                            {`${this.state.selectedAssignedToArr.length} Selected`}
                                        </div>
                                }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "createdBy" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'createdBy'})}
                        >
                            <span className="filter-title">Created By</span>
                            {
                                this.state.selectedCreatedByArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedCreatedByArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "customer" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'customer'})}
                        >
                            <span className="filter-title">Customer</span>
                            {
                                this.state.selectedCustomerArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedCustomerArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "dueDate" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'dueDate'})}
                        >
                            <span className="filter-title">Due Date</span>
                            {
                                this.state.selectedDueDate && 
                                    <div className="selection-count">
                                        {`Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "status" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'status'})}
                        >
                            <span className="filter-title">Status</span>
                            {
                                this.state.selectedStatusArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedStatusArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "priority" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'priority'})}
                        >
                            <span className="filter-title">Priority</span>
                            {
                                this.state.selectedPriorityArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedPriorityArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "activityType" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'activityType'})}
                        >
                            <span className="filter-title">Activity Type</span>
                            {
                                this.state.selectedTypeOfActivityArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedTypeOfActivityArr.length} Selected`}
                                    </div>
                            }
                        </div>
                    </div>
                    <div className="filter-right">
                        {
                            this.state.activeFilterOption === "priority" ? 
                                this.renderPriorityFilter()
                            :
                            this.state.activeFilterOption === "status" ? 
                                this.renderStatusFilter()
                            :
                            this.state.activeFilterOption === "dueDate" ? 
                                this.renderDueDateFilter()
                            :
                            this.state.activeFilterOption === "customer" ? 
                                this.renderProjectCustomerFilter()
                            :
                            this.state.activeFilterOption === "createdBy" ? 
                                this.renderCreatedByFilter()
                            :
                            this.state.activeFilterOption === "activityType" ? 
                                this.renderActivityTypeFilter()
                            :
                                this.renderAssignedToFilter()
                        }
                    </div>
                </div>
                <div className="action-footer">
                    <SquareButton
                        content="Cancel"
                        bsStyle={"default"}
                        className="contact-cancel"
                        onClick={(e) => this.onCancelClick(e)}
                    />
                    <SquareButton
                        content="Apply"
                        bsStyle={"primary"}
                        className="contact-save"
                        onClick={(e) => this.onApplyClick(e)}
                    />
                </div>
            </div>
        );
    }

    renderActivityTypeFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="activityType"
                    value={this.state.selectedAssignedTo}
                    onChange={e => this.handleTypeOfActivityChange(e, "activityType")}
                    options={this.props.typeOfActivityOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose type"
                    disabled={false}
                    loading={false}
                />
                {
                    this.state.selectedTypeOfActivityArr.map((val) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getTypeOfActivityVal(val)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(val, "activityType")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderAssignedToFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="assignedTo"
                    value={this.state.selectedAssignedTo}
                    onChange={e => this.handleAssignedToChange(e, "assignedTo")}
                    options={this.props.createdByOptions && this.props.assignedToOptions.map((data) => ({
                        value: data.id,
                        label: `${data.first_name} ${data.last_name}`,
                    }))}
                    multi={false}
                    searchable={true}
                    placeholder="Choose User"
                    disabled={false}
                    loading={this.props.isFetchingUsers}
                />
                {
                    this.state.selectedAssignedToArr.map((val) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getAssignedToVal(val)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(val, "assignedTo")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderCreatedByFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="createdBy"
                    value={this.state.selectedCreatedBy}
                    onChange={e => this.handleCreatedByChange(e, "createdBy")}
                    options={this.props.createdByOptions && this.props.createdByOptions.map((data) => ({
                        value: data.id,
                        label: `${data.first_name} ${data.last_name}`,
                    }))}
                    multi={false}
                    searchable={true}
                    placeholder="Choose User"
                    disabled={false}
                    loading={this.props.isFetchingUsers}
                />
                {
                    this.state.selectedCreatedByArr.map((val) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getCreatedByVal(val)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(val, "createdBy")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderProjectCustomerFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectCustomer"
                    value={this.state.selectedCustomer}
                    onChange={e => this.handleProjectCustomerChange(e, "customer")}
                    options={this.props.customerOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Customer"
                    disabled={false}
                    loading={this.props.isFetchingCustomers}
                />
                {
                    this.state.selectedCustomerArr.map((customer) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getCustomerVal(customer)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(customer, "customer")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderStatusFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="status"
                    value={this.state.selectedStatus}
                    onChange={e => this.handleStatusChange(e, "status")}
                    options={this.props.statusOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Select Status"
                    disabled={false}
                    loading={this.props.isFetchingStatusOptions}
                />
                {
                    this.state.selectedStatusArr.map((status) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getStatusVal(status)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(status, "status")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderPriorityFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="priority"
                    value={this.state.selectedPriority}
                    onChange={e => this.handlePriorityChange(e, "priority")}
                    options={this.props.priorityOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Select Priority"
                    disabled={false}
                    loading={this.props.isFetchingPriorityOptions}
                />
                {
                    this.state.selectedPriorityArr.map((priority) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getPriorityVal(priority)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(priority, "priority")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderDueDateFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="selectedDueDateOption"
                    value={this.state.selectedDueDateOption}
                    onChange={e => this.handleChange(e)}
                    options={[
                        {
                            value: "thisWeek",
                            label: `This Week`,
                            disabled: false,
                        },
                        {
                            value: "thisMonth",
                            label: `This Month`,
                            disabled: false,
                        },
                        {
                            value: "after",
                            label: "After",
                            disabled: false,
                        },
                        {
                            value: "before",
                            label: "Before",
                            disabled: false,
                        }
                    ]}
                    multi={false}
                    searchable={false}
                    placeholder="Choose Date Period"
                    disabled={false}
                />
                {
                    (this.state.selectedDueDateOption === "after" ||
                    this.state.selectedDueDateOption === "before") &&

                    <Input
                        field={{
                            value: this.state.selectedDueDate,
                            label: '',
                            type: "DATE",
                            isRequired: false,
                        }}
                        width={15}
                        name="selectedDueDate"
                        onChange={e => this.handleChange(e)}
                        placeholder="Choose Date"
                        showTime={false}
                        showDate={true}
                        disablePrevioueDates={false}
                    />
                }
            </div>
        )
    }
}

const mapStateToProps = (state: IReduxStore) => ({
});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectFilters);
