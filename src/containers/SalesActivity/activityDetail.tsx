import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import { cloneDeep } from "lodash";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../actions/documentation";
import {
  postActivityStatus,
  updateSalesActivity,
  getSalesActivityStatusSetting,
  getSalesActivityPrioritySetting,
  CREATE_SALES_ACTIVITY_FAILURE,
  UPDATE_SALES_ACTIVITY_SUCCESS,
  UPDATE_SALES_ACTIVITY_FAILURE,
  CREATE_SALES_ACTIVITY_SUCCESS,
  SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
  SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
} from "../../actions/sales";
import {
  createQuote,
  getQuoteTypeList,
  CREATE_QUOTES_SUCCESS,
} from "../../actions/sow";
import { fetchQuoteDashboardListingPU } from "../../actions/dashboard";
import {
  getQuoteAllStages,
  GET_QUOTE_STAGES_SUCCESS,
} from "../../actions/setting";
import { addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Checkbox from "../../components/Checkbox/checkbox";
import AddQuote from "../SOW/Sow/addQuote";
import CustomerUserNew from "../SOW/Sow/addUser";
import { commonFunctions } from "../../utils/commonFunctions";
import "./style.scss";

interface IActivityDetailProps extends ICommonProps {
  user: ISuperUser;
  providerUsers: any;
  quoteList: IQuote[];
  quoteFetching: boolean;
  isFetchingUsers: boolean;
  qTypeList: IDLabelObject[];
  customers: ICustomerShort[];
  isFetchingQTypeList: boolean;
  isFetchingQStageList: boolean;
  getQuoteStages: () => Promise<any>;
  getQuoteTypeList: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  getSalesActivityStatusSetting: () => Promise<any>;
  createQuote: (id: number, q: any) => Promise<any>;
  fetchAllCustomerUsers: (id: number) => Promise<any>;
  getSalesActivityPrioritySetting: () => Promise<any>;
  postActivityStatus: (data: ISalesActivity) => Promise<any>;
  updateSalesActivity: (data: ISalesActivity, id: number) => Promise<any>;
  fetchQuoteDashboardListing: (id: any, openOnly?: boolean) => Promise<any>;
}

interface IActivityDetailState {
  data: ISalesActivity;
  quote: IQuote;
  loading: boolean;
  quote_id: number;
  errorList?: object;
  disableSaveClick: boolean;
  isCreateUserModal: boolean;
  stages: IPickListOptions[];
  account_manager_name: string;
  fetchingStatusSettings: boolean;
  fetchingCustomersUsers: boolean;
  customerUsers: IPickListOptions[];
  fetchingPrioritySettings: boolean;
  isCreateOpportunityModal: boolean;
  isFetchingCustomerContacts: boolean;
  statusSettingOptions: IPickListOptions[];
  prioritySettingOptions: IPickListOptions[];
  error: {
    notes: IFieldValidation;
    status: IFieldValidation;
    priority: IFieldValidation;
    customer: IFieldValidation;
    due_date: IFieldValidation;
    description: IFieldValidation;
    assigned_to: IFieldValidation;
    activity_type: IFieldValidation;
    customer_contact: IFieldValidation;
  };
}

class ActivityDetail extends React.Component<
  IActivityDetailProps,
  IActivityDetailState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IActivityDetailProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    stages: [],
    quote: {} as IQuote,
    loading: false,
    customerUsers: [],
    quote_id: undefined,
    disableSaveClick: false,
    statusSettingOptions: [],
    isCreateUserModal: false,
    account_manager_name: "-",
    prioritySettingOptions: [],
    fetchingStatusSettings: false,
    fetchingCustomersUsers: false,
    fetchingPrioritySettings: false,
    isCreateOpportunityModal: false,
    isFetchingCustomerContacts: false,
    data: {
      notes: "",
      status: null,
      customer: null,
      due_date: "",
      priority: null,
      description: "",
      assigned_to: "",
      activity_type: [],
      customer_contact: "",
      opportunity_id: null,
    },
    error: {
      notes: { ...ActivityDetail.emptyErrorState },
      status: { ...ActivityDetail.emptyErrorState },
      priority: { ...ActivityDetail.emptyErrorState },
      customer: { ...ActivityDetail.emptyErrorState },
      due_date: { ...ActivityDetail.emptyErrorState },
      description: { ...ActivityDetail.emptyErrorState },
      assigned_to: { ...ActivityDetail.emptyErrorState },
      activity_type: { ...ActivityDetail.emptyErrorState },
      opportunity_id: { ...ActivityDetail.emptyErrorState },
      customer_contact: { ...ActivityDetail.emptyErrorState },
    },
  });

  componentDidMount() {
    const id = this.props.match.params.id;

    if (id) {
      const activityObj = { ...this.props.location.state };
      activityObj.priority = this.props.location.state.priority.id;
      activityObj.status = this.props.location.state.status.id;
      activityObj.customer = this.props.location.state.customer.id;
      activityObj.assigned_to = this.props.location.state.assigned_to.id;
      activityObj.customer_contact = this.props.location.state.customer_contact;
      activityObj.due_date = this.props.location.state.due_date;
      activityObj.opportunity_id = this.props.location.state.opportunity_id;

      this.setState({
        data: activityObj,
        quote_id: this.props.location.state.opportunity_id,
        account_manager_name: this.props.location.state.customer
          .account_manager_name,
      });

      this.getAllCustomerUsers(this.props.location.state.customer.id);
      this.props.fetchQuoteDashboardListing(
        this.props.location.state.customer.id,
        true
      );
    } else {
      this.setState({
        data: {
          ...this.state.data,
          assigned_to: this.props.user ? this.props.user.id : "",
          due_date: moment().add(7, "days"),
        },
      });
    }
    this.getStatusSettings();
    this.getPrioritySettings();
    this.props.getQuoteTypeList();
    this.props.getQuoteStages().then((action) => {
      if (action.type === GET_QUOTE_STAGES_SUCCESS) {
        this.setState({
          stages: action.response.map((stage) => ({
            value: stage.id,
            label: stage.label,
          })),
        });
      }
    });
  }

  getStatusSettings = () => {
    this.setState({ fetchingStatusSettings: true });
    this.props.getSalesActivityStatusSetting().then((action) => {
      if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
        const statusSettingOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));

        this.setState({ statusSettingOptions });

        const id = this.props.match.params.id;

        if (!id) {
          const defaultStatusId = action.response
            .filter((val) => {
              return val.is_default;
            })
            .map((obj) => {
              return obj.id;
            });

          this.setState({
            statusSettingOptions,
            data: {
              ...this.state.data,
              status: defaultStatusId.length > 0 ? defaultStatusId[0] : "",
            },
          });
        }
      }
      this.setState({ fetchingStatusSettings: false });
    });
  };

  getPrioritySettings = () => {
    this.setState({ fetchingPrioritySettings: true });
    this.props.getSalesActivityPrioritySetting().then((action) => {
      if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS) {
        const prioritySettingOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));

        const id = this.props.match.params.id;
        this.setState({ prioritySettingOptions });

        if (!id) {
          const defaultPriorityId = action.response
            .filter((val) => {
              return val.is_default;
            })
            .map((obj) => {
              return obj.id;
            });

          this.setState({
            data: {
              ...this.state.data,
              priority:
                defaultPriorityId.length > 0 ? defaultPriorityId[0] : "",
            },
          });
        }
      }
      this.setState({ fetchingPrioritySettings: false });
    });
  };

  getCustomerOptions = () => {
    return this.props.customers
      ? this.props.customers.map((data: ICustomerShort) => ({
          value: data.id,
          label: `${data.name}`,
          crm_id: data.crm_id,
          account_manager_name: data.account_manager_name,
        }))
      : [];
  };

  getAllCustomerUsers = (customerID) => {
    this.setState({ fetchingCustomersUsers: true });
    this.props.fetchAllCustomerUsers(customerID).then((action) => {
      if (action.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const customerUsers = action.response.map((data) => ({
          value: data.id,
          label: `${data.first_name} ${data.last_name}`,
        }));
        this.setState({ customerUsers });
      }
      this.setState({ fetchingCustomersUsers: false });
    });
  };

  handleDataChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.data[event.target.name] = event.target.value;
    newState.error[event.target.name].errorState = "success";
    newState.error[event.target.name].errorMessage = "";
    this.setState(newState);
  };

  handleCustomerChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    const customerObj = this.getCustomerOptions().filter(
      (customer) => String(event.target.value) === String(customer.value)
    );

    newState.data[event.target.name] = event.target.value;
    (newState as any).fetchingCustomersUsers = true;
    newState.error[event.target.name].errorState = "success";
    newState.error[event.target.name].errorMessage = "";

    (newState as any).account_manager_name =
      customerObj.length > 0 && customerObj[0].account_manager_name;
    this.getAllCustomerUsers(event.target.value);
    this.props.fetchQuoteDashboardListing(event.target.value, true);
    this.setState(newState);
  };

  handleActivityTypeChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    type: string
  ) => {
    const isChecked = e.target.checked;

    let updatedActivityType = [...this.state.data.activity_type];

    if (isChecked) {
      updatedActivityType = [...updatedActivityType, type];
    } else {
      updatedActivityType = updatedActivityType.filter((value) => {
        return value !== type;
      });
    }

    this.setState({
      data: {
        ...this.state.data,
        activity_type: updatedActivityType,
      },
    });
  };

  setValidationErrors = (errorList: object) => {
    const newState: IActivityDetailState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  validateForm() {
    const newState: IActivityDetailState = cloneDeep(this.state);
    let isValid = true;

    if (
      !this.state.data.description ||
      this.state.data.description.trim().length === 0
    ) {
      newState.error.description.errorState = "error";
      newState.error.description.errorMessage = "Description cannot be empty";
      isValid = false;
    }

    if (!this.state.data.priority) {
      newState.error.priority.errorState = "error";
      newState.error.priority.errorMessage = "Please select a priority";
      isValid = false;
    }

    if (!this.state.data.status) {
      newState.error.status.errorState = "error";
      newState.error.status.errorMessage = "Please select a status";
      isValid = false;
    }

    if (!this.state.data.customer) {
      newState.error.customer.errorState = "error";
      newState.error.customer.errorMessage = "Please select a customer";
      isValid = false;
    }

    if (!this.state.data.assigned_to) {
      newState.error.assigned_to.errorState = "error";
      newState.error.assigned_to.errorMessage = "Please select an Assignee";
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  onSaveClick = () => {
    // save activity
    this.setState({ loading: true, disableSaveClick: true });

    const { data } = this.state;

    if (!this.validateForm()) return;

    if (data.id) {
      if (typeof data.due_date !== "string")
        data.due_date = data.due_date.format("YYYY-MM-DD");

      this.props
        .updateSalesActivity(this.state.data, data.id)
        .then((action) => {
          if (action.type === UPDATE_SALES_ACTIVITY_SUCCESS) {
            const query = new URLSearchParams(this.props.location.search);
            const showAllActivities = query.get("showAllActivities") === "true";
            this.props.history.push(
              `/sales-activity?showAllActivities=${showAllActivities}`,
              { message: "Update Successfully", messageId: data.id }
            );
          }
          if (action.type === UPDATE_SALES_ACTIVITY_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
          this.setState({ loading: false, disableSaveClick: false });
        });
    } else {
      data.due_date = data.due_date.format("YYYY-MM-DD");
      this.props.postActivityStatus(this.state.data).then((action) => {
        if (action.type === CREATE_SALES_ACTIVITY_SUCCESS) {
          const query = new URLSearchParams(this.props.location.search);
          const showAllActivities = query.get("showAllActivities") === "true";
          this.props.history.push(
            `/sales-activity?showAllActivities=${showAllActivities}`,
            { message: "New", messageId: action.response.id }
          );
        }
        if (action.type === CREATE_SALES_ACTIVITY_FAILURE) {
          this.setValidationErrors(action.errorList.data);
        }
        this.setState({ loading: false, disableSaveClick: false });
      });
    }
  };

  toggleCreateUserModal = () => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
  };

  closeUserModal = (action: object) => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
    if (action === null) this.getAllCustomerUsers(this.state.data.customer);
  };

  toggleCreateOpportunityModal = () => {
    this.setState((prevState) => ({
      isCreateOpportunityModal: !prevState.isCreateOpportunityModal,
    }));
  };

  handleChangeQuotes = (e) => {
    if (e.target.value === 0) {
      this.toggleCreateOpportunityModal();
    } else {
      const newState = cloneDeep(this.state);
      newState[e.target.name] = e.target.value;
      newState.data.opportunity_id = e.target.value;
      this.setState(newState);
    }
  };

  createQuote = (data: any) => {
    this.setState({ loading: true });
    const customerId = this.state.data.customer;
    const customerObj = this.getCustomerOptions().filter(
      (customer) => customerId === customer.value
    );

    if (customerId && customerObj.length > 0) {
      data.customer_id = customerId;
      (data.user_id as string) = this.state.data.customer_contact;
      this.props
        .createQuote(customerObj[0].crm_id, data)
        .then((action) => {
          if (action.type === CREATE_QUOTES_SUCCESS) {
            this.setState({
              isCreateOpportunityModal: false,
            });
            this.props.fetchQuoteDashboardListing(customerId, true);
          } else {
            this.setState({
              isCreateOpportunityModal: true,
              errorList: action.errorList.data,
              quote: data,
            });
          }
          this.setState({ loading: false });
        })
        .catch(() => {
          this.setState({ loading: false });
        });
    }
  };

  getOpportunityOptions = () => {
    const quoteList = this.props.quoteList
      ? this.props.quoteList.map((t) => ({
          value: t.id,
          label: `${t.name} (${t.stage_name})`,
          disabled: false,
        }))
      : [];

    return quoteList;
  };

  render() {
    const activityId = this.props.match.params.id;
    return (
      <div className="sales-activity-details activity">
        {this.state.isCreateUserModal && (
          <CustomerUserNew
            isVisible={this.state.isCreateUserModal}
            close={this.closeUserModal}
            customerId={this.state.data.customer}
          />
        )}
        <AddQuote
          show={this.state.isCreateOpportunityModal}
          onClose={this.toggleCreateOpportunityModal}
          onSubmit={this.createQuote}
          types={this.props.qTypeList}
          stages={this.state.stages}
          isLoading={this.state.loading}
          errorList={this.state.errorList}
          quote={this.state.quote}
        />
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div className="header">
          <div className="left">
            <div className="name">
              {activityId ? "Edit Activity" : "New Activity"}
            </div>
          </div>
          <div className="right">
            <SquareButton
              content="Save"
              bsStyle={"primary"}
              onClick={(e) => this.onSaveClick()}
              disabled={this.state.disableSaveClick}
            />
            <SquareButton
              content="Cancel"
              bsStyle={"default"}
              onClick={() => {
                const query = new URLSearchParams(this.props.location.search);
                const showAllActivities =
                  query.get("showAllActivities") === "true";
                this.props.history.push(
                  `/sales-activity?showAllActivities=${showAllActivities}`
                );
              }}
            />
          </div>
        </div>
        <div className="first-row">
          <div className="action-second status-actions-row">
            <div className="sales-activity-action-component">
              <Input
                field={{
                  label: "Description",
                  type: "TEXT",
                  value: this.state.data.description,
                  isRequired: true,
                }}
                width={10}
                labelIcon={"info"}
                name="description"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Enter description`}
                error={this.state.error.description}
              />
              <Input
                field={{
                  label: "Priority",
                  type: "PICKLIST",
                  value: this.state.data.priority,
                  options: this.state.prioritySettingOptions,
                  isRequired: true,
                }}
                className="priority-input"
                width={8}
                multi={false}
                name="priority"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select`}
                loading={this.state.fetchingPrioritySettings}
                error={this.state.error.priority}
              />
              <Input
                field={{
                  label: "Customer",
                  type: "PICKLIST",
                  value: this.state.data.customer,
                  options: this.getCustomerOptions(),
                  isRequired: true,
                }}
                className="customer-input"
                width={8}
                multi={false}
                name="customer"
                onChange={(e) => this.handleCustomerChange(e)}
                placeholder={`Select`}
                error={this.state.error.customer}
              />
              <div className="customer-contact-sales-activity">
                <Input
                  field={{
                    label: "Customer Contact",
                    type: "PICKLIST",
                    value: this.state.data.customer_contact,
                    options: this.state.customerUsers,
                    isRequired: false,
                  }}
                  className="customer-contact-input"
                  width={8}
                  multi={false}
                  name="customer_contact"
                  onChange={(e) => this.handleDataChange(e)}
                  placeholder={`Select Customer Contact`}
                  loading={this.state.fetchingCustomersUsers}
                  error={this.state.error.customer_contact}
                />
                <SquareButton
                  content="+"
                  onClick={(e) => this.toggleCreateUserModal()}
                  className="add-new-option-sow-sales-activity"
                  bsStyle={"primary"}
                  title="Add New User"
                  disabled={!this.state.data.customer}
                />
              </div>
              <div className="customer-contact-sales-activity">
                <Input
                  field={{
                    label: "Select Opportunity",
                    type: "PICKLIST",
                    value: this.state.quote_id,
                    options: this.getOpportunityOptions(),
                    isRequired: false,
                  }}
                  loading={this.props.quoteFetching}
                  width={8}
                  className="customer-contact-input"
                  name="quote_id"
                  onChange={(e) => this.handleChangeQuotes(e)}
                  multi={false}
                  placeholder="Select Opportunity"
                  disabled={false}
                  error={{
                    errorState: "success",
                    errorMessage: "",
                  }}
                />
                <SquareButton
                  content="+"
                  onClick={(e) => this.toggleCreateOpportunityModal()}
                  className="add-new-option-sow-sales-activity"
                  bsStyle={"primary"}
                  title="Add New Opportunity"
                  disabled={
                    !this.state.data.customer ||
                    !this.state.data.customer_contact
                  }
                />
              </div>
              <Input
                className=""
                field={{
                  label: "",
                  type: "TEXTAREA",
                  value: this.state.data.notes,
                  isRequired: false,
                }}
                width={12}
                name="notes"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Enter Note`}
                // error={this.state.error.note}
              />
            </div>
          </div>
          <div className="status-overall-accordians">
            <div className="details-box">
              <Input
                field={{
                  label: "Status",
                  type: "PICKLIST",
                  value: this.state.data.status,
                  options: this.state.statusSettingOptions,
                  isRequired: true,
                }}
                width={8}
                multi={false}
                name="status"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select`}
                loading={this.state.fetchingStatusSettings}
                error={this.state.error.status}
              />
            </div>
            <div className="details-box">
              <Input
                field={{
                  label: "Due Date",
                  type: "DATE",
                  value: this.state.data.due_date,
                  isRequired: true,
                }}
                className="due-date-input"
                width={4}
                labelIcon={"info"}
                name="due_date"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select Date`}
                showTime={false}
                disablePrevioueDates={false}
                disabled={false}
                error={this.state.error.due_date}
                disableTimezone={true}
              />
            </div>
            <div className="details-box">
              <Input
                field={{
                  label: "Assigned To",
                  type: "PICKLIST",
                  value:
                    this.state.data.assigned_to === ""
                      ? !activityId && this.props.user && this.props.user.id
                      : this.state.data.assigned_to,
                  options:
                    this.props.providerUsers &&
                    this.props.providerUsers.map((data) => ({
                      value: data.id,
                      label: `${data.first_name} ${data.last_name}`,
                    })),
                  isRequired: true,
                }}
                width={8}
                multi={false}
                name="assigned_to"
                onChange={(e) => this.handleDataChange(e)}
                placeholder={`Select`}
                loading={this.props.isFetchingUsers}
                error={this.state.error.assigned_to}
              />
            </div>
            <div className="details-box">
              <div className="label-details">Account Manager</div>
              <div className="label-content">
                {this.state.account_manager_name || "-"}
              </div>
            </div>
            <div className="details-box">
              <div className="header-label">Types of Activity</div>
              <Checkbox
                className="activity-type-option"
                isChecked={this.state.data.activity_type.includes(
                  "Statement of work"
                )}
                name="activityTypeCheck"
                onChange={(e) =>
                  this.handleActivityTypeChange(e, "Statement of work")
                }
              >
                Statement of Work
              </Checkbox>
              <Checkbox
                className="activity-type-option"
                isChecked={this.state.data.activity_type.includes(
                  "Hardware BoM"
                )}
                name="activityTypeCheck"
                onChange={(e) =>
                  this.handleActivityTypeChange(e, "Hardware BoM")
                }
              >
                Hardware BoM
              </Checkbox>
              <Checkbox
                className="activity-type-option"
                isChecked={this.state.data.activity_type.includes(
                  "Research and Respond"
                )}
                name="activityTypeCheck"
                onChange={(e) =>
                  this.handleActivityTypeChange(e, "Research and Respond")
                }
              >
                Research and Respond
              </Checkbox>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  qTypeList: state.sow.qTypeList,
  quoteList: state.dashboard.quoteList,
  customers: state.customer.customersShort,
  quoteFetching: state.dashboard.quoteFetching,
  providerUsers: state.providerUser.providerUsers,
  isFetchingQTypeList: state.sow.isFetchingQTypeList,
  isFetchingUsers: state.providerUser.isFetchingUsers,
  isFetchingQStageList: state.sow.isFetchingQStageList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStages: () => dispatch(getQuoteAllStages()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  createQuote: (id: number, q: any) => dispatch(createQuote(id, q)),
  fetchAllCustomerUsers: (id: number) => dispatch(fetchAllCustomerUsers(id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  getSalesActivityStatusSetting: () =>
    dispatch(getSalesActivityStatusSetting()),
  getSalesActivityPrioritySetting: () =>
    dispatch(getSalesActivityPrioritySetting()),
  postActivityStatus: (data: ISalesActivity) =>
    dispatch(postActivityStatus(data)),
  updateSalesActivity: (data: ISalesActivity, id: number) =>
    dispatch(updateSalesActivity(data, id)),
  fetchQuoteDashboardListing: (id: any, openOnly?: boolean) =>
    dispatch(fetchQuoteDashboardListingPU(id, openOnly)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityDetail);
