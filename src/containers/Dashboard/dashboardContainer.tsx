import React from "react";
import { Doughnut } from "react-chartjs-2";
import { connect } from "react-redux";
import {
  downloadAttachment,
  DOWNLOAD_DOCUMENT_FAILURE,
  DOWNLOAD_DOCUMENT_SUCCESS,
  fetchCustomerActivityDashboard,
  fetchLifeCycleDashboardData,
  fetchLifeCycleDashboardDataCustomer,
  fetchOrdersDashboardData,
  fetchOrdersDashboardDataCustomer,
  fetchQuoteDashboardListing,
  fetchServiceDashboardData,
  fetchServiceDashboardDataCustomer,
  fetchSuperUserDashboardData,
  getAttachment,
  GET_DOCUMENT_SUCCESS,
} from "../../actions/dashboard";

import { fetchUserProfile } from "../../actions/profile";
import Spinner from "../../components/Spinner";
import {
  Chart,
  Legend,
  Tooltip,
  ArcElement,
  DoughnutController,
} from "chart.js";
import {
  fetchProviderConfigStatus,
  FETCH_PROVIDER_CONFIG_STATUS_SUCCESS,
} from "../../actions/provider/integration";
import IconButton from "../../components/Button/iconButton";
import Input from "../../components/Input/input";
import Table from "../../components/Table/table";
import { utcToLocalInLongFormat } from "../../utils/CalendarUtil";
import { saveBlob } from "../../utils/download";
import { searchInFields } from "../../utils/searchListUtils";
import OrderTRCustomerDashboard from "./CustomerOrderTrackingDashboard";
import "./style.scss";
import { isEqual } from "lodash";

Chart.register(Legend, Tooltip, ArcElement, DoughnutController);

interface IDashboardProps extends ICommonProps {
  service: Iservice;
  lifecycle: Ilifecycle;
  orders: Iorders;
  superUsers: ISupeUserDashboard;
  fetchServiceDashboardData: TfetchServiceDashboardData;
  fetchLifeCycleDashboardData: TfetchLifeCycleDashboardData;
  fetchServiceDashboardDataCustomer: TfetchServiceDashboardDataCustomer;
  fetchLifeCycleDashboardDataCustomer: TfetchLifeCycleDashboardDataCustomer;
  fetchOrdersDashboardData: TfetchOrdersDashboardData;
  fetchOrdersDashboardDataCustomer: TfetchOrdersDashboardDataCustomer;
  fetchSuperUserDashboardData: TfetchSuperUserDashboardData;
  user: ISuperUser;
  fetchUserProfile: TFetchUserProfile;
  isServiceFetching: boolean;
  isLifeCycleFetching: boolean;
  isOrderFetching: boolean;
  isSuperUserFetching: boolean;
  fetchProviderConfigStatus: any;
  quoteList: IQuote[];
  fetchQuoteDashboardListing: any;
  quoteFetching: boolean;
  quoteAction: boolean;
  getAttachment: any;
  downloadAttachment: any;
  userActivity: any[];
  userActivityFetching: boolean;
  fetchCustomerActivityDashboard: any;
}

interface IDashboardState {
  service: Iservice;
  lifecycle: Ilifecycle;
  orders: Iorders;
  superUsers: ISupeUserDashboard;
  quoteList: any[];
  userActivity: any[];
  downloadingIds: number[];
  errorIds: number[];
  searchString: string;
}

class DashBoard extends React.Component<IDashboardProps, IDashboardState> {
  constructor(props: IDashboardProps) {
    super(props);
    this.state = {
      service: {
        open_ms_ticket: 0,
        open_projects: 0,
        open_ps_ticket: 0,
      },
      lifecycle: {
        expired_support: 0,
        expiring_support: 0,
        end_of_life: 0,
        supported_devices: 0,
      },
      orders: {
        closed: 0,
        open: 0,
        open_quotes: 0,
      },
      superUsers: {
        providers_count: 0,
        top_five_providers_by_customer_count: [],
        top_five_providers_by_users_activity: [],
      },
      quoteList: [],
      userActivity: [],
      downloadingIds: [],
      errorIds: [],
      searchString: "",
    };
  }

  componentDidMount() {
    if (!this.props.user) {
      this.props.fetchUserProfile();
    }

    if (this.props.service) {
      this.setState({
        service: this.props.service,
      });
    }
    if (this.props.lifecycle) {
      this.setState({
        lifecycle: this.props.lifecycle,
      });
    }
    if (this.props.orders) {
      this.setState({
        orders: this.props.orders,
      });
    }
    if (this.props.superUsers) {
      this.setState({
        superUsers: this.props.superUsers,
      });
    }
    if (this.props.quoteList) {
      this.setState({
        quoteList: this.props.quoteList,
      });
    }
    if (this.props.userActivity) {
      const userActivity = this.getRows(this.props, "");
      this.setState({
        userActivity,
      });
    }
    if (this.props.user) {
      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "provider" &&
        (!this.props.service || !this.props.orders || !this.props.lifecycle)
      ) {
        this.props.fetchProviderConfigStatus().then((action) => {
          if (
            action.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS &&
            action.response &&
            action.response.crm_authentication_configured &&
            action.response.crm_board_mapping_configured &&
            action.response.crm_device_categories_configured
          ) {
            this.props.fetchServiceDashboardData();
            this.props.fetchLifeCycleDashboardData();
            this.props.fetchOrdersDashboardData();
            this.props.fetchCustomerActivityDashboard();
          }
        });
      } else if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "customer" &&
        (!this.props.service || this.props.orders || !this.props.lifecycle)
      ) {
        this.props.fetchServiceDashboardDataCustomer();
        this.props.fetchLifeCycleDashboardDataCustomer();
        this.props.fetchOrdersDashboardDataCustomer();
        this.props.fetchQuoteDashboardListing();
      } else if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "accelavar" &&
        !this.props.superUsers
      ) {
        this.props.fetchSuperUserDashboardData();
      }
    }
  }

  static getDerivedStateFromProps(
    nextProps: IDashboardProps,
    prevState: IDashboardState
  ) {
    if (
      nextProps.superUsers &&
      !isEqual(nextProps.superUsers, prevState.superUsers)
    ) {
      return {
        superUsers: nextProps.superUsers,
      };
    }
    if (nextProps.service && !isEqual(nextProps.service, prevState.service)) {
      return {
        service: nextProps.service,
      };
    }
    if (
      nextProps.lifecycle &&
      !isEqual(nextProps.lifecycle, prevState.lifecycle)
    ) {
      return {
        lifecycle: nextProps.lifecycle,
      };
    }
    if (
      nextProps.quoteList &&
      !isEqual(nextProps.quoteList, prevState.quoteList)
    ) {
      return {
        quoteList: nextProps.quoteList,
      };
    }
    if (nextProps.orders && !isEqual(nextProps.orders, prevState.orders)) {
      return {
        orders: nextProps.orders,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps: IDashboardProps) {
    if (
      this.props.userActivity &&
      this.props.userActivity !== prevProps.userActivity
    ) {
      const userActivity = this.getRows(this.props, "");
      this.setState({
        userActivity,
      });
    }
    if (this.props.user && this.props.user !== prevProps.user) {
      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "provider" &&
        !prevProps.service
      ) {
        prevProps.fetchProviderConfigStatus().then((action) => {
          if (
            action.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS &&
            action.response &&
            action.response.crm_authentication_configured &&
            action.response.crm_board_mapping_configured &&
            action.response.crm_device_categories_configured
          ) {
            this.props.fetchServiceDashboardData();
            this.props.fetchLifeCycleDashboardData();
            this.props.fetchOrdersDashboardData();
            this.props.fetchCustomerActivityDashboard();
          }
        });
      } else if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "customer" &&
        !this.props.service
      ) {
        this.props.fetchServiceDashboardDataCustomer();
        this.props.fetchLifeCycleDashboardDataCustomer();
        this.props.fetchOrdersDashboardDataCustomer();
        this.props.fetchQuoteDashboardListing();
      } else if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "accelavar" &&
        !this.props.superUsers
      ) {
        this.props.fetchSuperUserDashboardData();
      }
    }
  }
  renderTopBar = () => {
    return (
      <div className={"header-quote"}>
        {" "}
        <div
          className={`graph-heading ${
            this.props.quoteFetching ? "loading" : ""
          }`}
        >
          Open Quotes
        </div>{" "}
      </div>
    );
  };

  renderTopBarActivity = () => {
    return (
      <div className={"header-quote"}>
        {" "}
        <div
          className={`graph-heading ${
            this.props.userActivityFetching ? "loading" : ""
          }`}
        >
          <div id="customer-user-activity"> Customer Users Activity</div>
          <Input
            field={{
              label: "",
              type: "SEARCH",
              value: this.state.searchString,
              isRequired: false,
            }}
            width={4}
            placeholder="Search"
            name="searchString"
            onChange={this.handleChange}
            className="dashboard__search search"
          />
        </div>{" "}
      </div>
    );
  };

  getRows = (nextProps: IDashboardProps, searchString?: string) => {
    let userActivities = nextProps.userActivity;
    const search = searchString ? searchString : this.state.searchString;

    if (search && search.length > 0) {
      userActivities =
        userActivities &&
        userActivities.filter((row) => searchInFields(row, search, ["name"]));
    }
    const rows: any =
      userActivities &&
      userActivities.map((activity, index) => ({
        name: activity.name,
        count_last_90_days: activity.count_last_90_days,
        total_active_users: activity.total_active_users,
        user_last_login: activity.user_last_login,
        user_name: activity.user_name,
        index,
      }));

    return rows;
  };

  handleChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };

  handleRows = () => {
    const userActivity = this.getRows(this.props, this.state.searchString);

    this.setState({ userActivity });
  };

  downloadAttachment = (loadingId) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, loadingId],
    });

    this.props.getAttachment(loadingId).then((action) => {
      if (action.type === GET_DOCUMENT_SUCCESS) {
        if (action.response && action.response.id) {
          this.getStream(action.response, loadingId);
        }
        if (action.response && action.response.length === 0) {
          this.setState({
            downloadingIds: this.state.downloadingIds.filter(
              (id) => loadingId !== id
            ),
            errorIds: [...this.state.errorIds, loadingId],
          });
        }
      }
    });
  };

  getLoadingMarkUp = (cell) => {
    if (this.state.downloadingIds.includes(cell.original.id)) {
      return (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Downloading File"
        />
      );
    } else {
      if (this.state.errorIds.includes(cell.original.id)) {
        return "No Attachment";
      } else {
        return (
          <IconButton
            icon="download-button.png"
            onClick={() => {
              this.downloadAttachment(cell.original.id);
            }}
          />
        );
      }
    }
  };

  getErrorsMarkUp = (cell) => {
    if (this.state.errorIds.includes(cell.original.id)) {
      return <div>no attachment </div>;
    } else {
      return "";
    }
  };

  getStream = (document: ITicketDocument, loadingId) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, loadingId],
    });

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.downloadAttachment(document.id).then((action) => {
        if (action.type === DOWNLOAD_DOCUMENT_SUCCESS) {
          saveBlob(document.file_name, action.response);
          this.setState({
            downloadingIds: this.state.downloadingIds.filter(
              (id) => loadingId !== id
            ),
          });
        }
        if (action.type === DOWNLOAD_DOCUMENT_FAILURE) {
          this.setState({
            downloadingIds: this.state.downloadingIds.filter(
              (id) => loadingId !== id
            ),
          });
        }
      });
    }
  };
  getProcurmentLabel = () => {
    const procurmentLabel = [`Completed Orders`, `Orders in Process`];
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      return [`Completed Orders`, `Orders in Process`, "Open Quotes"];
    }

    return procurmentLabel;
  };

  getTotal = (array: number[]) => {
    let total = 0;
    total = array.reduce((a, b) => (a || 0) + (b || 0));

    return total ? total : null;
  };
  render() {
    const options = {
      plugins: {
        datalabels: {
          display: false,
        },
        legend: {
          position: "bottom" as "bottom",
          labels: {
            boxWidth: 13,
            font: { size: 11 },
            boxRadius: 50,
          },
        },
      },
      maintainAspectRatio: false,
      layout: {
        padding: {
          left: 10,
          right: 10,
          top: 10,
          bottom: 10,
        },
      },
      cutout: "80%",
    };
    const columns: any = [
      {
        accessor: "name",
        Header: "Name",
      },
      {
        accessor: "shipping_contact_name",
        Header: "Requester",
        width: 150,
        Cell: (id) => (
          <div className="pl-15">{id.value ? id.value : "N.A."}</div>
        ),
      },
      {
        accessor: "status_name",
        Header: "Status",
        width: 150,
        Cell: (id) => <div className="pl-15">{id.value}</div>,
      },
      {
        accessor: "id",
        Header: "Attachment",
        width: 150,
        sortable: false,
        Cell: (cell) => (
          <div className="loader-download">{this.getLoadingMarkUp(cell)}</div>
        ),
      },
    ];

    const activityColumns: any = [
      {
        accessor: "name",
        Header: "Customer Name",
      },
      {
        accessor: "user_name",
        Header: "Last Login User",
        Cell: (id) => (
          <div className="pl-15">{id.value ? id.value : "N.A."}</div>
        ),
      },
      {
        accessor: "user_last_login",
        Header: "Last Login Date & Time",
        width: 200,
        Cell: (id) => (
          <div className="pl-15">
            {id.value ? utcToLocalInLongFormat(id.value) : "N.A."}
          </div>
        ),
      },
      {
        accessor: "total_active_users",
        Header: "Total Active Users",
        width: 150,
        sortable: false,
        Cell: (id) => (
          <div className="pl-15">{id.value ? id.value : "N.A."}</div>
        ),
      },
      {
        accessor: "count_last_90_days",
        Header: "Last 90 days Active Users",
        width: 180,
        sortable: false,
        Cell: (id) => (
          <div className="pl-15">{id.value ? id.value : "N.A."}</div>
        ),
      },
    ];

    return (
      <div>
        {this.props.user &&
          this.props.user.type &&
          (this.props.user.type === "provider" ||
            this.props.user.type === "customer") && (
            <div className="dashboard">
              <div className="col-md-4 graph-box">
                <div
                  className={`graph-heading ${
                    this.props.isOrderFetching ? "loading" : ""
                  }`}
                >
                  Procurement
                </div>
                <div className={this.props.isOrderFetching ? "loader" : ""}>
                  <Spinner show={this.props.isOrderFetching} />
                </div>
                <div
                  className={`dashboard-graph-img ${
                    this.props.isOrderFetching ? "loading" : ""
                  }`}
                >
                  <Doughnut
                    data={{
                      labels: this.getProcurmentLabel(),
                      datasets: [
                        {
                          data: [
                            this.state.orders.closed,
                            this.state.orders.open,
                            this.state.orders.open_quotes,
                          ],
                          backgroundColor: ["#f0b65c", "#9e3d9a", "#7f7fb1"],
                          hoverBackgroundColor: [
                            "#f0b65c",
                            "#9e3d9a",
                            "#7f7fb1",
                          ],
                          borderWidth: 1,
                        },
                      ],
                    }}
                    options={options}
                  />
                  <div className="donut-inner">
                    <span>
                      {this.getTotal([
                        this.state.orders.closed,
                        this.state.orders.open,
                        this.state.orders.open_quotes,
                      ])}
                    </span>
                  </div>
                </div>
                <div
                  className={`graph-footer  ${
                    this.props.isOrderFetching ? "loading" : ""
                  }`}
                >
                  <div className="">{this.state.orders.closed}</div>
                  <div className="">{this.state.orders.open}</div>
                  {this.props.user &&
                    this.props.user.type &&
                    this.props.user.type === "customer" && (
                      <div className="">{this.state.orders.open_quotes}</div>
                    )}
                </div>
              </div>

              <div className="col-md-4 graph-box-services">
                <div
                  className={`graph-heading  ${
                    this.props.isServiceFetching ? "loading" : ""
                  }`}
                >
                  Services
                </div>
                <div className={this.props.isServiceFetching ? "loader" : ""}>
                  <Spinner show={this.props.isServiceFetching} />
                </div>
                <div
                  className={`dashboard-graph-img ${
                    this.props.isServiceFetching ? "loading" : ""
                  }`}
                >
                  <Doughnut
                    data={{
                      labels: [
                        // `Open PS Ticket`,
                        `Open MS Ticket`,
                        `Open Projects`,
                      ],
                      datasets: [
                        {
                          data: [
                            // this.state.service.open_ps_ticket,
                            this.state.service.open_ms_ticket,
                            this.state.service.open_projects,
                          ],
                          backgroundColor: ["#f0b65c", "#9e3d9a", "#7f7fb1"],
                          hoverBackgroundColor: [
                            "#f0b65c",
                            "#9e3d9a",
                            "#7f7fb1",
                          ],
                          borderWidth: 1,
                        },
                      ],
                    }}
                    options={options}
                  />
                  <div className="donut-inner">
                    <span>
                      {this.getTotal([
                        // this.state.service.open_ps_ticket,
                        this.state.service.open_ms_ticket,
                        this.state.service.open_projects,
                      ])}
                    </span>
                  </div>
                </div>
                <div
                  className={`graph-footer ${
                    this.props.isServiceFetching ? "loading" : ""
                  }`}
                >
                  {/* <div className="">{this.state.service.open_ps_ticket}</div> */}
                  <div className="">{this.state.service.open_ms_ticket}</div>
                  <div className="">{this.state.service.open_projects}</div>
                </div>
              </div>
              <div className="col-md-4 graph-box">
                <div
                  className={`graph-heading ${
                    this.props.isLifeCycleFetching ? "loading" : ""
                  }`}
                >
                  Lifecycle Management
                </div>
                <div className={this.props.isLifeCycleFetching ? "loader" : ""}>
                  <Spinner show={this.props.isLifeCycleFetching} />
                </div>
                <div
                  className={`dashboard-graph-img ${
                    this.props.isLifeCycleFetching ? "loading" : ""
                  }`}
                >
                  <Doughnut
                    data={{
                      labels: [
                        `End of life`,
                        `Expiring Support`,
                        `Expired support`,
                        `Supported devices`,
                      ],
                      datasets: [
                        {
                          data: [
                            this.state.lifecycle.end_of_life,
                            this.state.lifecycle.expiring_support,
                            this.state.lifecycle.expired_support,
                            this.state.lifecycle.supported_devices,
                          ],
                          backgroundColor: [
                            "#f0b65c",
                            "#9e3d9a",
                            "#7f7fb1",
                            "#56a799",
                          ],
                          hoverBackgroundColor: [
                            "#f0b65c",
                            "#9e3d9a",
                            "#7f7fb1",
                            "#56a799",
                          ],
                          borderWidth: 1,
                        },
                      ],
                    }}
                    options={options}
                  />
                  <div className="donut-inner">
                    <span>
                      {this.getTotal([
                        this.state.lifecycle.end_of_life,
                        this.state.lifecycle.expiring_support,
                        this.state.lifecycle.expired_support,
                        this.state.lifecycle.supported_devices,
                      ])}
                    </span>
                  </div>
                </div>
                <div
                  className={`graph-footer ${
                    this.props.isLifeCycleFetching ? "loading" : ""
                  }`}
                >
                  <div className="">{this.state.lifecycle.end_of_life}</div>
                  <div className="">
                    {this.state.lifecycle.expiring_support}
                  </div>
                  <div className="">{this.state.lifecycle.expired_support}</div>
                  <div className="">
                    {this.state.lifecycle.supported_devices}
                  </div>
                </div>
              </div>
            </div>
          )}
        <div className={this.props.quoteFetching ? "loader" : ""}>
          <Spinner show={this.props.quoteFetching} />
        </div>

        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === "provider" && (
            <div className="col-md-12 activity-list dashboard">
              <div className={this.props.userActivityFetching ? "loader" : ""}>
                <Spinner show={this.props.userActivityFetching} />
              </div>
              <Table
                columns={activityColumns}
                customTopBar={this.renderTopBarActivity()}
                rows={this.state.userActivity || []}
                className={`${
                  this.props.userActivityFetching ? "loading" : ""
                }`}
                loading={this.props.userActivityFetching}
              />
            </div>
          )}
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === "customer" && (
            <div className="col-md-12 quota-list dashboard">
              <OrderTRCustomerDashboard
                {...this.props}
                table={
                  <Table
                    columns={columns}
                    customTopBar={this.renderTopBar()}
                    rows={this.state.quoteList}
                    className={`${this.props.quoteFetching ? "loading" : ""}`}
                    loading={this.props.quoteFetching}
                  />
                }
              />
            </div>
          )}
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === "accelavar" && (
            <div className="dashboard-super-user">
              <h3>Dashboard</h3>
              <div className="loader">
                <Spinner show={this.props.isSuperUserFetching} />
              </div>
              <div
                className={`${this.props.isSuperUserFetching ? "loading" : ""}`}
              >
                <div className="col-md-4">
                  <div className="graph-heading">
                    Numbers of Providers
                    <div className="small">
                      (Based on Enabled status of Providers)
                    </div>
                  </div>
                  <div className="number-of-provider col-md-12">
                    {this.state.superUsers.providers_count}
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="graph-heading">
                    Top 5 Providers
                    <div className="small">(Based on customer count)</div>
                  </div>
                  <div className="projects dashboard-box col-md-12">
                    {this.state.superUsers &&
                      // tslint:disable-next-line:max-line-length
                      this.state.superUsers.top_five_providers_by_customer_count.map(
                        (data, index) => (
                          <div key={index} className="name">
                            {data.name} <span>({data.customers_count})</span>
                          </div>
                        )
                      )}
                  </div>
                </div>
                <div className="col-md-4">
                  <div className="graph-heading">
                    Top 5 Providers
                    <div className="small">(Based on activity login)</div>
                  </div>
                  <div className="projects dashboard-box col-md-12">
                    {this.state.superUsers &&
                      // tslint:disable-next-line:max-line-length
                      this.state.superUsers.top_five_providers_by_users_activity.map(
                        (data, index) => (
                          <div key={index} className="name">
                            {data.name} <span>({data.users_count})</span>
                          </div>
                        )
                      )}
                  </div>
                </div>
              </div>
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  service: state.dashboard.service,
  lifecycle: state.dashboard.lifecycle,
  user: state.profile.user,
  orders: state.dashboard.orders,
  superUsers: state.dashboard.superUsers,
  isServiceFetching: state.dashboard.isServiceFetching,
  isLifeCycleFetching: state.dashboard.isLifeCycleFetching,
  isOrderFetching: state.dashboard.isOrderFetching,
  isSuperUserFetching: state.dashboard.isSuperUserFetching,
  quoteList: state.dashboard.quoteList,
  quoteFetching: state.dashboard.quoteFetching,
  quoteAction: state.dashboard.quoteAction,
  userActivityFetching: state.dashboard.userActivityFetching,
  userActivity: state.dashboard.userActivity,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchUserProfile: () => dispatch(fetchUserProfile()),
  fetchServiceDashboardData: () => dispatch(fetchServiceDashboardData()),
  fetchLifeCycleDashboardData: () => dispatch(fetchLifeCycleDashboardData()),
  fetchOrdersDashboardData: () => dispatch(fetchOrdersDashboardData()),
  fetchOrdersDashboardDataCustomer: () =>
    dispatch(fetchOrdersDashboardDataCustomer()),
  fetchServiceDashboardDataCustomer: () =>
    dispatch(fetchServiceDashboardDataCustomer()),
  fetchLifeCycleDashboardDataCustomer: () =>
    dispatch(fetchLifeCycleDashboardDataCustomer()),
  fetchSuperUserDashboardData: () => dispatch(fetchSuperUserDashboardData()),
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
  fetchQuoteDashboardListing: () => dispatch(fetchQuoteDashboardListing()),
  getAttachment: (id: any) => dispatch(getAttachment(id)),
  downloadAttachment: (id: any) => dispatch(downloadAttachment(id)),
  fetchCustomerActivityDashboard: () =>
    dispatch(fetchCustomerActivityDashboard()),
});

export default connect(mapStateToProps, mapDispatchToProps)(DashBoard);
