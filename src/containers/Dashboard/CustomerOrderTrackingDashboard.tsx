import "chartjs-plugin-datalabels";
import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";
import { addSuccessMessage } from "../../actions/appState";

import {
  fetchCustomerProfile,
  FETCH_CUSTOMER_PROFILE_SUCCESS,
} from "../../actions/customerUser";
import { fetchSitesCustomersUser } from "../../actions/inventory";
import InfiniteListing from "../../components/InfiniteList/infiniteList";
import RightMenu from "../../components/RighMenuBase/rightMenuBase";
import { getUserProfile } from "../../utils/AuthUtil";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import { commonFunctions } from "../../utils/commonFunctions";
import "../../commonStyles/tabs.scss";
import "./style.scss";

enum PageType {
  Order,
  OpenQuote,
}
interface IDashboardProps extends ICommonProps {
  addSuccessMessage: any;
  user: any;
  customerId: any;
  table: any;
  fetchSitesCustomersUser: any;
  sites: any;
  fetchCustomerProfile: any;
  quoteFetching: boolean;
}

interface IDashboardState {
  searchString: string;
  openPOsDistributors: any[];
  orderEstimatedShipDate: any[];
  lastCustomerUpdate: any[];
  orderShippedNotReceived: any[];
  loadingOpenPO: boolean;
  loadingTWO: boolean;
  loadingTHREE: boolean;
  loadingFOUR: boolean;
  loading: boolean;
  rows: any;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  loggedInUserOnly: boolean;
  showCusOT: boolean;
  show: boolean;
  currentPage: {
    pageType: PageType;
  };
  selectedPO: any;
}

class OrderTRCustomerDashboard extends React.Component<
  IDashboardProps,
  IDashboardState
> {
  constructor(props: IDashboardProps) {
    super(props);
    this.state = this.getEmptyState();
  }
  private debouncedFetch;

  priorityColorMap = [
    "#4ba2c1",
    "#e55b7a",
    "#fac64d",
    "#5b9950",
    "#ba89f2",
    "#cdd5e1",
  ];

  estShipppedDatePriorityColorMap = [
    "#5b9950",
    "#fac64d",
    "#4ba2c1",
    "#e55b7a",
    "#ba89f2",
    "#cdd5e1",
  ];

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Order,
    },
    rows: [],
    searchString: "",
    openPOsDistributors: [{ label: "", value: 0, filter: false }],
    orderEstimatedShipDate: [],
    lastCustomerUpdate: [],
    orderShippedNotReceived: [],
    loadingOpenPO: false,
    loadingTWO: false,
    loadingTHREE: false,
    loadingFOUR: false,
    loading: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: { page_size: 25 },
    },
    reset: false,
    show: false,
    loggedInUserOnly: false,
    selectedPO: null,
    showCusOT: false,
  });

  componentDidMount() {
    const UserProfile: IUserProfile = getUserProfile();
    if (UserProfile.scopes.type === "CUSTOMER") {
      this.props.fetchCustomerProfile().then((action) => {
        if (action.type === FETCH_CUSTOMER_PROFILE_SUCCESS) {
          const showCusOT = action.response.customer_order_visibility_allowed;
          this.setState({
            showCusOT,
            currentPage: {
              pageType: showCusOT ? PageType.Order : PageType.OpenQuote,
            },
          });
        }
      });
    }
    this.props.fetchSitesCustomersUser();
  }

  getFilterParams = (list) => {
    const data = list.filter((x) => x.filter);
    const derivedData = data[0];
    return derivedData;
  };

  createFilterParams = () => {
    const orderShippedNotReceived = this.getFilterParams(
      this.state.orderShippedNotReceived
    );
    const orderEstimatedShipDate = this.getFilterParams(
      this.state.orderEstimatedShipDate
    );
    const lastCustomerUpdate = this.getFilterParams(
      this.state.lastCustomerUpdate
    );

    return {
      ...orderShippedNotReceived,
      ...orderEstimatedShipDate,
      ...lastCustomerUpdate,
    };
  };

  onClose = () => {
    this.setState((prevState) => ({
      show: !prevState.show,
    }));
  };
  getBody = () => {
    const selectedPO = this.state.selectedPO;
    if (!selectedPO) {
      return "";
    }
    return (
      <div className="line-items-body">
        <div className="details-line-tems">
          <div className="row-fields">
            <span> Order Date : </span>
            {fromISOStringToFormattedDate(
              selectedPO.order_date,
              "MMM DD, YYYY"
            )}
          </div>
          <div className="row-fields">
            <span>Customer PO : </span>
            {selectedPO.customer_po_number}
          </div>
          <div
            className="row-fields"
            title={commonFunctions.getSiteById(
              this.props.sites,
              selectedPO.site_crm_id,
              "text"
            )}
          >
            <span>Ship to Location : </span>
            {selectedPO.site_name}
          </div>
          <div className="row-fields">
            <span> Order Name :</span> {selectedPO.opportunity_name}
          </div>
        </div>
        <InfiniteListing
          id="line-items"
          key={selectedPO.id}
          url={`customers/sales-orders/${selectedPO.id}/product-items`}
          columns={[
            {
              name: "Product ID",
              ordering: "product_id",
              className: "",
              id: "product_id",
              Cell: (d) => (
                <div className={`po`} title={""} onClick={(e) => {}}>
                  {d.product_id}
                </div>
              ),
            },
            {
              name: "Product Description",
              ordering: "description",
              className: "",
              id: "description",
              Cell: (d) => (
                <div className={`po`} title={d.description}>
                  {d.description}
                </div>
              ),
            },
            {
              name: "Qty",
              ordering: "quantity",
              className: "width-10",
              id: "quantity",
              Cell: (d) => (
                <div className={`po`} title={d.quantity}>
                  {d.quantity}
                </div>
              ),
            },
            {
              name: "Ship Status",
              ordering: "status",
              className: "",
              id: "status",
              Cell: (d) => (
                <div className={`status-icon-rules`}>{d.status}</div>
              ),
            },
          ]}
          className="failed-service-tickets"
          showSearch={true}
        />
      </div>
    );
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
      pageSize: this.state.pagination.params.page_size,
    });
  };

  appliedFilterLength = () => {
    return (
      this.state.openPOsDistributors.filter((x) => x.filter).length +
      this.state.orderEstimatedShipDate.filter((x) => x.filter).length +
      this.state.orderShippedNotReceived.filter((x) => x.filter).length +
      this.state.lastCustomerUpdate.filter((x) => x.filter).length
    );
  };

  filtersTiles = (list, name) => {
    return (
      <>
        {list.map((x, ind) => {
          if (!x.filter) {
            return false;
          }
          return (
            <div className="filter-tile">
              {x.label}{" "}
              <img
                className={"d-pointer icon-remove"}
                alt=""
                src={"/assets/icons/cross-sign.svg"}
                onClick={(e) => {
                  const newstate = cloneDeep(this.state);
                  const data = newstate[name];
                  data[ind].filter = false;
                  (newstate[name] as any) = data;
                  const pagination = this.getEmptyState().pagination;
                  (newstate.pagination as any) = pagination;
                  this.setState(newstate, () => {
                    this.debouncedFetch({
                      page: 1,
                      pageSize: this.state.pagination.params.page_size,
                    });
                  });
                }}
              />
            </div>
          );
        })}
      </>
    );
  };

  tabs = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="tab__header">
        {this.state.showCusOT && (
          <div
            className={`tab__header-link ${
              currentPage.pageType === PageType.Order
                ? "tab__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.Order)}
          >
            Order
          </div>
        )}

        <div
          className={`tab__header-link ${
            currentPage.pageType === PageType.OpenQuote
              ? "tab__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.OpenQuote)}
        >
          {this.props.quoteFetching ? "Loading..." : "Open Quote"}
        </div>
      </div>
    );
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  render() {
    const currentPage = this.state.currentPage;
    return (
      <div className="customer-ot-dashboard tab">
        {this.tabs()}

        {currentPage.pageType === PageType.Order && this.state.showCusOT && (
          <>
            {this.props.user && (
              <InfiniteListing
                id="orderTracking"
                url={`customers/sales-orders`}
                columns={[
                  {
                    name: "Customer PO",
                    ordering: "customer_po_number",
                    className: "width-15",
                    id: "customer_po_number",
                    Cell: (d) => (
                      <div
                        className={`po-underline`}
                        title={d.internal_notes}
                        onClick={(e) => {
                          this.setState({ show: true, selectedPO: d });
                        }}
                      >
                        {d.customer_po_number || "-"}
                      </div>
                    ),
                  },
                  {
                    name: "Order Date",
                    ordering: "order_date",
                    className: "width-10",
                    id: "order_date",
                    Cell: (d) => (
                      <div className={`po`}>
                        {fromISOStringToFormattedDate(
                          d.order_date,
                          "MMM DD, YYYY"
                        )}
                      </div>
                    ),
                  },
                  {
                    name: "Order Name",
                    ordering: "opportunity_name",
                    className: "",
                    id: "opportunity_name",
                    Cell: (d) => (
                      <div className={`po`} title={d.opportunity_name}>
                        {d.opportunity_name}
                      </div>
                    ),
                  },
                  {
                    name: "Ship to Location",
                    ordering: "site_name",
                    className: "",
                    id: "site_name",
                    Cell: (d) => (
                      <div
                        className={`po`}
                        title={commonFunctions.getSiteById(
                          this.props.sites,
                          d.site_crm_id,
                          "text"
                        )}
                      >
                        {d.site_name}
                      </div>
                    ),
                  },
                  {
                    name: "Ship Status",
                    ordering: "status",
                    id: "status",
                    Cell: (d) => (
                      <div className={`status-icon-rules`}>{d.status}</div>
                    ),
                  },
                ]}
                radioBoxFilter={{
                  options: [
                    {
                      label: "All",
                      value: "",
                    },
                    {
                      label: "Open",
                      value: "False",
                    },
                    {
                      label: "Delivered",
                      value: "True",
                    },
                  ],
                  name: "Show Orders :",
                  key: "is_closed",
                  value: "",
                }}
                className="failed-service-tickets"
                showSearch={true}
              />
            )}
          </>
        )}
        {currentPage.pageType === PageType.OpenQuote && <>{this.props.table}</>}
        {this.state.show && (
          <RightMenu
            show={this.state.show}
            onClose={this.onClose}
            titleElement={
              <div className="title-details">
                {" "}
                {this.state.selectedPO.customer_po_number} Details{" "}
              </div>
            }
            bodyElement={this.getBody()}
            footerElement={null}
            className="po-line-items-Details"
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customerId: state.customer.customerId,
  sites: state.inventory.sites,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSitesCustomersUser: () => dispatch(fetchSitesCustomersUser()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchCustomerProfile: () => dispatch(fetchCustomerProfile()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTRCustomerDashboard);
