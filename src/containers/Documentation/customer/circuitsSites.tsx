import { cloneDeep } from "lodash";
import React from "react";
import moment from "moment";
import { connect } from "react-redux";

import AppValidators from "../../../utils/validator";
import { addErrorMessage } from "../../../actions/appState";
import {
  ADD_CIRCUIT_INFO_FAILURE,
  ADD_CIRCUIT_INFO_SUCCESS,
  addCircuitInfoCU,
  CREATE_CIRCUIT_TYPE_SUCCESS,
  createCircuitTypes,
  editCircuitInfoCU,
  fetchCircuitInfoCU,
  fetchCircuitTypes,
  fetchDevicesShortCU,
} from "../../../actions/documentation";
import {
  ADD_SITE_FAILURE,
  ADD_SITE_SUCCESS,
  addSiteCU,
  editSiteCU,
  FETCH_SINGLE_DEVICE_FAILURE,
  FETCH_SINGLE_DEVICE_SUCCESS,
  FETCH_SITES_SUCCESS,
  fetchCountriesAndStates,
  fetchExistingDeviceAssociationCU,
  fetchManufacturers,
  fetchSingleDevice,
  fetchSitesCustomersUser,
} from "../../../actions/inventory";
import SquareButton from "../../../components/Button/button";
import EditButton from "../../../components/Button/editButton";
import Checkbox from "../../../components/Checkbox/checkbox";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import { isValue } from "../../../utils/CommonUtils";
import { allowPermission } from "../../../utils/permissions";
import ViewDevice from "../../InventoryManagement/viewDevice";
import CircuitFilter from "../filterCircuites";
import FilterSite from "../filterSites";
import { searchInFields } from "../../../utils/searchListUtils";
import {
  fromISOStringToFormattedDate,
  phoneNumberInFormat,
} from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";
import "../Provider/style.scss";

enum PageType {
  Site,
  Circuit,
}

interface ICircuitsSitesProps extends ICommonProps {
  user: ISuperUser;
  isFetching: boolean;
  site: ISite;
  customerId: number;
  fetchCustomerUsers: any;
  fetchSites: any;
  sites: any;
  countries: any;
  addSite: (newSite: any) => any;
  editSite: (newSite: any) => any;
  fetchCountriesAndStates: any;
  isFetchingSite: any;
  fetchCircuitInfo: any;
  circuitInfoList: any;
  circuitTypes: any[];
  fetchCircuitTypes: any;
  addCircuitInfo: any;
  editCircuitInfo: any;
  deleteCircuitInfo: any;
  manufacturers: any[];
  fetchManufacturers: TFetchManufacturers;
  fetchSingleDevice: TFetchSingleDevice;
  deviceDetails: IDevice;
  isDeviceFetching: boolean;
  addErrorMessage: TShowErrorMessage;
  resetManufacturerApiRetryCount: any;
  fetchExistingDeviceAssociation: any;
  existingAssociationList: any;
  fetchDevicesShort: any;
  deviceList: any[];
  isFetchingDevices: boolean;
  createCircuitTypes: any;
  isFetchingTypes: boolean;
  downloadCircuitReportCU: any;
  reportDownloading: boolean;
}

interface ICircuitsSitesState {
  attachment: any;
  currentPage: {
    pageType: PageType;
  };
  openModalCircuit: boolean;
  openViewModalCircuit: boolean;
  site: ISite;
  rows: ISite[];
  circuitRows: ICircuitInfo[];
  ShowCircuitRows: ICircuitInfo[];
  circuitInfo: ICircuitInfo;
  isEditPopUp: boolean;
  isCreateSiteModal: boolean;
  isViewSiteModal: boolean;
  filtersites: ISiteFilters;
  isFilterSiteModalOpen: boolean;
  siteLabelIds: { [id: number]: string };
  isFilterCircuitModalOpen: boolean;
  typesLabelIds: { [id: number]: string };
  isExistLabelIds: { [id: number]: string };
  filterCircuites: ICircuitFilters;

  error: {
    name: IFieldValidation;
    address_line_1: IFieldValidation;
    address_line_2: IFieldValidation;
    city: IFieldValidation;
    state: IFieldValidation;
    country_id: IFieldValidation;
    zip: IFieldValidation;
    phone_number: IFieldValidation;
  };
  errorCircuit: {
    site: IFieldValidation;
    circuit_type: IFieldValidation;
    provider: IFieldValidation;
    circuit_id: IFieldValidation;
    email: IFieldValidation;
    loa: IFieldValidation;
    contact_phone: IFieldValidation;
    attachment: IFieldValidation;
    device_crm_id: IFieldValidation;
    ip_address: IFieldValidation;
    bandwidth: IFieldValidation;
    account_number: IFieldValidation;
  };
  circuitInfoList: any;
  id: any;
  isopenConfirm: boolean;
  searchString: string;
  searchStringCircuit: string;
  isViewDeviceModalOpen: boolean;
  deviceDetails?: IDevice;
  reset: boolean;
}

class CircuitsSitesCustomer extends React.Component<
  ICircuitsSitesProps,
  ICircuitsSitesState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: ICircuitsSitesProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    attachment: null,
    searchString: "",
    searchStringCircuit: "",
    currentPage: {
      pageType: PageType.Circuit,
    },
    openModalCircuit: false,
    openViewModalCircuit: false,
    rows: [],
    circuitRows: [],
    ShowCircuitRows: [],
    isEditPopUp: false,
    isCreateSiteModal: false,
    isViewSiteModal: false,
    circuitInfo: {
      site: "",
      circuit_type: "",
      provider: "",
      circuit_id: "",
      loa: false,
      contract_end_date: "",
      email: "",
      note: "",
      phone_number: "",
      bandwidth: "",
      account_number: null,
      bandwidth_frequency: "GB",
    },
    site: {
      name: "",
      address_line_1: "",
      address_line_2: "",
      city: "",
      state: "",
      zip: "",
    },
    filtersites: {
      site: [],
      type: [],
      is_exist: [],
    },
    filterCircuites: {
      site: [],
    },
    isFilterSiteModalOpen: false,
    isFilterCircuitModalOpen: false,
    siteLabelIds: {},
    isExistLabelIds: {
      EXIST: "EXIST",
      NOTEXIST: "NOTEXIST",
    },
    typesLabelIds: {},
    error: {
      name: { ...CircuitsSitesCustomer.emptyErrorState },
      address_line_1: { ...CircuitsSitesCustomer.emptyErrorState },
      address_line_2: { ...CircuitsSitesCustomer.emptyErrorState },
      city: { ...CircuitsSitesCustomer.emptyErrorState },
      state: { ...CircuitsSitesCustomer.emptyErrorState },
      country_id: { ...CircuitsSitesCustomer.emptyErrorState },
      zip: { ...CircuitsSitesCustomer.emptyErrorState },
      phone_number: { ...CircuitsSitesCustomer.emptyErrorState },
    },
    errorCircuit: {
      site: { ...CircuitsSitesCustomer.emptyErrorState },
      circuit_type: { ...CircuitsSitesCustomer.emptyErrorState },
      provider: { ...CircuitsSitesCustomer.emptyErrorState },
      circuit_id: { ...CircuitsSitesCustomer.emptyErrorState },
      email: { ...CircuitsSitesCustomer.emptyErrorState },
      contact_phone: { ...CircuitsSitesCustomer.emptyErrorState },
      attachment: { ...CircuitsSitesCustomer.emptyErrorState },
      device_crm_id: { ...CircuitsSitesCustomer.emptyErrorState },
      ip_address: { ...CircuitsSitesCustomer.emptyErrorState },
      bandwidth: { ...CircuitsSitesCustomer.emptyErrorState },
      account_number: { ...CircuitsSitesCustomer.emptyErrorState },
      loa: { ...CircuitsSitesCustomer.emptyErrorState },
    },
    sites: [],
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    circuitInfoList: [],
    id: "",
    isopenConfirm: false,
    isViewDeviceModalOpen: false,
    reset: false,
  });
  componentDidMount() {
    this.props.fetchCountriesAndStates();
    this.props.fetchCircuitTypes();
    this.props.fetchManufacturers();
    this.props.fetchSites().then((action) => {
      if (action.type === FETCH_SITES_SUCCESS) {
        this.props.fetchCircuitInfo();
      }
    });
    this.props.fetchDevicesShort();
  }

  componentDidUpdate(prevProps: ICircuitsSitesProps) {
    const { sites, circuitInfoList, deviceDetails, circuitTypes } = this.props;
    const {
      searchString,
      filtersites,
      searchStringCircuit,
      filterCircuites,
    } = this.state;

    if (sites !== prevProps.sites && sites) {
      const rows = this.getRows(this.props, searchString, filtersites);
      this.setState({ rows });
    }

    if (circuitInfoList !== prevProps.circuitInfoList && circuitInfoList) {
      const circuitRows = this.getRowsCircuit(
        this.props,
        searchStringCircuit,
        filterCircuites
      );
      this.setState({ circuitRows });
    }

    if (deviceDetails !== prevProps.deviceDetails) {
      this.setState({ deviceDetails });
    }

    if (sites !== prevProps.sites) {
      this.setLabelsSites(this.props);
    }

    if (circuitTypes !== prevProps.circuitTypes) {
      this.setLabelsTypes(this.props);
    }
  }

  getRowsCircuit = (
    nextProps: ICircuitsSitesProps,
    searchString: string,
    filters: ICircuitFilters
  ) => {
    const search = searchString ? searchString : this.state.searchString;

    const circuitInfos = nextProps.circuitInfoList;

    const circuitRows: any[] = circuitInfos.map((info, index) => ({
      id: info.id,
      cw_site_name:
        this.props.sites &&
        this.props.sites.filter((row) => row.site_id === info.cw_site_id) &&
        this.props.sites.filter((row) => row.site_id === info.cw_site_id)
          .length > 0 &&
        this.props.sites.filter((row) => row.site_id === info.cw_site_id)[0]
          .name,
      cw_site_id: info.cw_site_id,
      contact_phone: info.contact_phone,
      email: info.email,
      note: info.note,
      provider: info.provider,
      circuit_id: info.circuit_id,
      circuit_type_name: info.circuit_type,
      circuit_type: info.circuit_type_id,
      device_crm_id: info.device_crm_id,
      ip_address: info.ip_address,
      loa: info.loa,
      attachment: info.attachment,
      bandwidth: info.bandwidth && info.bandwidth.split(" ")[0],
      account_number: info.account_number,
      bandwidth_frequency:
        (info.bandwidth && info.bandwidth.split(" ")[1]) || "GB",
      contract_end_date: info.contract_end_date,
      index,
    }));
    let rows = circuitRows;

    if (filters) {
      // Add filter for type once included.
      const filterSites = filters.site;
      const filterTypes = filters.type;
      const filterExist = filters.is_exist;

      rows =
        rows &&
        rows.filter((c) => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
          const hasFilteredSite =
            filterSites.length > 0 ? filterSites.includes(c.cw_site_id) : true;
          const hasFilteredType =
            filterTypes && filterTypes.length > 0
              ? filterTypes.includes(JSON.parse(c.circuit_type))
              : true;
          const hasFilteredExist =
            filterExist && filterExist.length > 0
              ? filterExist.includes(c && c.circuit_id ? "EXIST" : "NOTEXIST")
              : true;

          return hasFilteredSite && hasFilteredType && hasFilteredExist;
        });
    }
    if (search && search.length > 0) {
      rows = rows.filter((row) =>
        searchInFields(row, search, [
          "cw_site_name",
          "circuit_type_name",
          "circuit_id",
          "provider",
        ])
      );
    }
    rows =
      rows &&
      rows.map((info, index) => ({
        id: info.id,
        cw_site_name:
          this.props.sites &&
          this.props.sites.filter((row) => row.site_id === info.cw_site_id) &&
          this.props.sites.filter((row) => row.site_id === info.cw_site_id)
            .length > 0 &&
          this.props.sites.filter((row) => row.site_id === info.cw_site_id)[0]
            .name,
        cw_site_id: info.cw_site_id,
        contact_phone: info.contact_phone,
        email: info.email,
        note: info.note,
        provider: info.provider,
        circuit_id: info.circuit_id,
        circuit_type_name: info.circuit_type_name,
        circuit_type: info.circuit_type,
        device_crm_id: info.device_crm_id,
        ip_address: info.ip_address,
        loa: info.loa,
        attachment: info.attachment,
        bandwidth: info.bandwidth && info.bandwidth.split(" ")[0],
        account_number: info.account_number,
        bandwidth_frequency:
          (info.bandwidth && info.bandwidth.split(" ")[1]) || "GB",
        contract_end_date: info.contract_end_date,
        index,
      }));

    return rows;
  };

  setLabelsSites = (props) => {
    if (props.sites !== this.props.sites) {
      const siteLabelIds = props.sites.reduce((labelIds, site) => {
        labelIds[site.site_id] = site.name;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        siteLabelIds,
      });
    }
  };
  setLabelsTypes = (props) => {
    if (props.circuitTypes !== this.props.circuitTypes) {
      const typesLabelIds = props.circuitTypes.reduce((labelIds, type) => {
        labelIds[type.id] = type.name;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        typesLabelIds,
      });
    }
  };
  getRows = (
    nextProps: ICircuitsSitesProps,
    searchString: string,
    filters: ISiteFilters
  ) => {
    const search = searchString ? searchString : this.state.searchString;

    let rows = nextProps.sites;

    if (filters) {
      // Add filter for type once included.
      const filterSites = filters.site;

      rows =
        rows &&
        rows.filter((config) => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
          const hasFilteredSite =
            filterSites.length > 0
              ? filterSites.includes(config.site_id)
              : true;

          return hasFilteredSite;
        });
    }
    if (search && search.length > 0) {
      rows = rows.filter((row) =>
        searchInFields(row, search, ["name", "address_line_1", "city", "state"])
      );
    }
    rows =
      rows &&
      rows.map((note, index) => ({
        address_line_1: note.address_line_1,
        address_line_2: note.address_line_2,
        city: note.city,
        country: note.country,
        name: note.name,
        site_id: note.site_id,
        state: note.state,
        phone_number: note.phone_number,
        device_id: note.device_id,
        zip: note.zip,
        index,
      }));

    return rows;
  };

  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filtersites
    );

    this.setState({ rows });
  };

  handleRowsCircuits = () => {
    const circuitRows = this.getRowsCircuit(
      this.props,
      this.state.searchStringCircuit,
      this.state.filterCircuites
    );

    this.setState({ circuitRows });
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="documentation__header">
        {
          <div
            className={`documentation__header-link ${
              currentPage.pageType === PageType.Circuit
                ? "documentation__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.Circuit)}
          >
            Circuits
          </div>
        }
        {
          <div
            className={`documentation__header-link ${
              currentPage.pageType === PageType.Site
                ? "documentation__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.Site)}
          >
            Sites
          </div>
        }
      </div>
    );
  };

  toggleNewModal = () => {
    this.setState({
      circuitInfo: this.getEmptyState().circuitInfo,
      errorCircuit: this.getEmptyState().errorCircuit,
      openModalCircuit: !this.state.openModalCircuit,
      isEditPopUp: false,
    });
  };

  toggleViewModalCircuit = () => {
    this.setState({
      circuitInfo: this.getEmptyState().circuitInfo,
      errorCircuit: this.getEmptyState().errorCircuit,
      openViewModalCircuit: !this.state.openViewModalCircuit,
    });
  };

  onEditRowClick(original: any, e: any, index: number): any {
    e.stopPropagation();
    let countrySelected = [];
    if (this.props.countries && this.props.countries.length > 0) {
      countrySelected = this.props.countries.filter(
        (row) => row.country === original.country
      );
    }
    this.setState({
      isCreateSiteModal: !this.state.isCreateSiteModal,
      site: {
        ...this.state.rows[index],
        country: countrySelected.length > 0 && countrySelected[0].country,
        country_id: countrySelected.length > 0 && countrySelected[0].country_id,
      },
      isEditPopUp: true,
    });
  }

  onViewRowClick = (rowInfo) => {
    this.setState({
      isViewSiteModal: !this.state.isViewSiteModal,
      site: {
        ...rowInfo.original,
      },
    });
  };

  onEditRowClickCircuit(original: any, e: any, index: number): any {
    e.stopPropagation();
    this.setState({
      openModalCircuit: !this.state.openModalCircuit,
      circuitInfo: {
        ...this.state.circuitRows[index],
      },
      isEditPopUp: true,
    });
  }
  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      site: {
        ...prevState.site,
        [targetName]: targetValue,
      },
    }));
  };

  handleChangeCircuitType = (event: any) => {
    const targetValue = event.value;

    this.setState((prevState) => ({
      circuitInfo: {
        ...prevState.circuitInfo,
        circuit_type: targetValue,
      },
    }));
  };

  handleChangeCircuit = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      circuitInfo: {
        ...prevState.circuitInfo,
        [targetName]: targetValue,
      },
    }));
  };
  handleChangeSiteName = (event: any) => {
    const targetValue = event.target.value;
    const name = this.props.sites.filter((row) => row.site_id === targetValue);
    this.setState((prevState) => ({
      circuitInfo: {
        ...prevState.circuitInfo,
        cw_site_name: name[0].name,
        cw_site_id: targetValue,
      },
    }));
  };
  getCountries = () => {
    if (this.props.countries && this.props.countries.length > 0) {
      return this.props.countries.map((country) => ({
        value: country.country_id,
        label: country.country,
      }));
    } else {
      return [];
    }
  };

  getStates = () => {
    const countries = this.props.countries;
    if (countries && this.state.site.country_id) {
      const country = countries.find(
        (c) => c.country_id === this.state.site.country_id
      );

      return (
        country.states &&
        country.states.map((state) => ({
          value: state.state_identifier,
          label: state.state,
        }))
      );
    }

    return [];
  };
  toggleCreateSiteModal = () => {
    this.setState((prevState) => ({
      site: this.getEmptyState().site,
      error: this.getEmptyState().error,
      isCreateSiteModal: !prevState.isCreateSiteModal,
      isEditPopUp: false,
    }));
  };

  toggleViewSiteModal = () => {
    this.setState((prevState) => ({
      isViewSiteModal: !prevState.isViewSiteModal,
      site: this.getEmptyState().site,
    }));
  };

  renderAddSite = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching || this.props.isFetchingSite} />
        </div>
        <div
          className={`add-site__body ${
            this.props.isFetching || this.props.isFetchingSite ? `loading` : ""
          }`}
        >
          <Input
            field={{
              label: "Name",
              type: "TEXT",
              value: this.state.site.name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Site Name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Address Line 1",
              type: "TEXT",
              value: this.state.site.address_line_1,
            }}
            width={6}
            placeholder="Enter Address Line 1"
            error={this.state.error.address_line_1}
            name="address_line_1"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Address Line 2",
              type: "TEXT",
              value: this.state.site.address_line_2,
            }}
            width={6}
            placeholder="Enter Address Line 2"
            error={this.state.error.address_line_2}
            name="address_line_2"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Phone Number",
              type: "TEXT",
              value: this.state.site.phone_number,
            }}
            error={this.state.error.phone_number}
            width={6}
            placeholder="Enter Phone Number"
            name="phone_number"
            onChange={this.handleChange}
          />

          <Input
            field={{
              value: this.state.site.country_id,
              label: "Country",
              type: "PICKLIST",
              isRequired: true,
              options: this.getCountries(),
            }}
            width={6}
            name="country_id"
            onChange={this.handleChange}
            placeholder="Select country"
            error={this.state.error.country_id}
          />
          <Input
            field={{
              value: this.state.site.state,
              label: "State",
              type: "PICKLIST",
              isRequired: false,
              options: this.getStates(),
            }}
            width={6}
            name="state"
            onChange={this.handleChange}
            placeholder="Select State"
            error={this.state.error.state}
          />
          <Input
            field={{
              label: "City",
              type: "TEXT",
              value: this.state.site.city,
            }}
            width={6}
            placeholder="Enter City"
            error={this.state.error.city}
            name="city"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Zip Code",
              type: "TEXT",
              value: this.state.site.zip,
            }}
            width={6}
            placeholder="Enter Zip Code"
            error={this.state.error.zip}
            name="zip"
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  };

  renderViewSite = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching || this.props.isFetchingSite} />
        </div>
        <div
          className={`add-site__body ${
            this.props.isFetching || this.props.isFetchingSite ? `loading` : ""
          }`}
        >
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Name
              </label>
            </div>
            <div>{this.state.site.name || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Address line 1
              </label>
            </div>
            <div>{this.state.site.address_line_1 || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Address line 2
              </label>
            </div>
            <div>{this.state.site.address_line_2 || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Phone number
              </label>
            </div>
            <div>{phoneNumberInFormat("", this.state.site.phone_number)}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Country
              </label>
            </div>
            <div>{this.state.site.country || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                State
              </label>
            </div>
            <div>{this.state.site.state || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                City
              </label>
            </div>
            <div>{this.state.site.city || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Zip
              </label>
            </div>
            <div>{this.state.site.zip || "N.A."}</div>
          </div>
        </div>
      </div>
    );
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.site.name || this.state.site.name.length === 0) {
      error.name.errorState = "error";
      error.name.errorMessage = "Enter a valid site name";

      isValid = false;
    }

    if (!this.state.site.country_id) {
      error.country_id.errorState = "error";
      error.country_id.errorMessage = "Select country ";

      isValid = false;
    }

    if (this.getStates().length !== 0 && !this.state.site.state) {
      error.state.errorState = "error";
      error.state.errorMessage = "Select State";

      isValid = false;
    }
    if (
      this.state.site.phone_number &&
      !AppValidators.isPhoneNumber(this.state.site.phone_number)
    ) {
      error.phone_number.errorState = "error";
      error.phone_number.errorMessage = "Enter a valid phone number";

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };
  onSiteAdd = () => {
    if (this.isValid()) {
      const site = this.state.site;
      if (!site.state_id) {
        delete site.state_id;
      }
      if (site.site_id) {
        this.props.editSite(site).then((action) => {
          if (action.type === ADD_SITE_SUCCESS) {
            this.props.fetchSites();
            this.setState({ isCreateSiteModal: false });
          }
          if (action.type === ADD_SITE_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
        });
      } else {
        this.props.addSite(site).then((action) => {
          if (action.type === ADD_SITE_SUCCESS) {
            this.props.fetchSites();
            this.setState({ isCreateSiteModal: false });
          }
          if (action.type === ADD_SITE_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
        });
      }
    }
  };

  setValidationErrors = (errorList) => {
    const newState: ICircuitsSitesState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isFetching ? `loading` : ""}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.toggleCreateSiteModal}
        />
        <SquareButton
          content={`${this.state.isEditPopUp ? "Update" : "Add"}`}
          bsStyle={"primary"}
          onClick={this.onSiteAdd}
          disabled={this.props.isFetchingSite}
        />
      </div>
    );
  };

  onNewOptionClick = (option) => {
    const { className, ...newOption } = option;
    const name = newOption.label;
    this.props.createCircuitTypes(name).then((action) => {
      if (action.type === CREATE_CIRCUIT_TYPE_SUCCESS) {
        this.props.fetchCircuitTypes();
      }
    });
  };
  renderAddCircuit = () => {
    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];

    const circuitTypes = this.props.circuitTypes
      ? this.props.circuitTypes.map((t) => ({
          value: t.id,
          label: t.name,
          disabled: false,
        }))
      : [];
    const devices = this.props.deviceList.map((info, index) => ({
      device_name: info.device_name,
      cw_site_name:
        (this.props.sites &&
          this.props.sites.filter((row) => row.site_id === info.site_id) &&
          this.props.sites.filter((row) => row.site_id === info.site_id)
            .length > 0 &&
          this.props.sites.filter((row) => row.site_id === info.site_id)[0]
            .name) ||
        "N.A.",
      serial_number: info.serial_number,
      id: info.id,
      index,
    }));

    const deviceList = devices
      ? devices.map((d) => ({
          value: d.id,
          label: `${d.device_name} (${d.serial_number}) (${d.cw_site_name})`,
          disabled: false,
        }))
      : [];

    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching || this.props.isFetchingSite} />
        </div>
        <div
          className={`add-circuit__body ${
            this.props.isFetching || this.props.isFetchingSite ? `loading` : ""
          }`}
        >
          <div
            className="select-type field-section
             col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Device
              </label>
            </div>
            <div
              className={`${
                this.state.errorCircuit.device_crm_id.errorMessage
                  ? `error-input`
                  : ""
              }`}
            >
              <SelectInput
                name="device_crm_id"
                value={this.state.circuitInfo.device_crm_id}
                onChange={this.handleChangeCircuit}
                options={deviceList}
                searchable={true}
                placeholder="Select Device"
                clearable={false}
              />
            </div>
            {this.state.errorCircuit.device_crm_id.errorMessage && (
              <div className="select-type-error">
                {this.state.errorCircuit.device_crm_id.errorMessage}
              </div>
            )}
          </div>

          <div
            className="select-type field-section
             col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Site
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.errorCircuit.site.errorMessage ? `error-input` : ""
              }`}
            >
              <SelectInput
                name="cw_site_id"
                value={this.state.circuitInfo.cw_site_id}
                onChange={this.handleChangeSiteName}
                options={sites}
                searchable={true}
                placeholder="Select site"
                clearable={false}
              />
            </div>
            {this.state.errorCircuit.site.errorMessage && (
              <div className="select-type-error">
                {this.state.errorCircuit.site.errorMessage}
              </div>
            )}
          </div>
          <Input
            field={{
              label: "Circuit ID",
              type: "TEXT",
              value: this.state.circuitInfo.circuit_id,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Circuit ID"
            error={this.state.errorCircuit.circuit_id}
            name="circuit_id"
            onChange={this.handleChangeCircuit}
          />
          <div
            className="select-type field-section
             col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Circuit Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.errorCircuit.circuit_type.errorMessage
                  ? `error-input`
                  : ""
              }`}
            >
              <SelectInput
                name="circuit_type"
                value={this.state.circuitInfo.circuit_type}
                onChange={this.handleChangeCircuit}
                options={circuitTypes}
                searchable={true}
                placeholder="Select Circuit Type"
                clearable={false}
              />
            </div>
            {this.state.errorCircuit.circuit_type.errorMessage && (
              <div className="select-type-error">
                {this.state.errorCircuit.circuit_type.errorMessage}
              </div>
            )}
          </div>
          <Input
            field={{
              label: "Provider",
              type: "TEXT",
              value: this.state.circuitInfo.provider,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter provider "
            error={this.state.errorCircuit.provider}
            name="provider"
            onChange={this.handleChangeCircuit}
          />
          <Input
            field={{
              label: "IP Address Space",
              type: "TEXT",
              value: this.state.circuitInfo.ip_address,
              isRequired: false,
            }}
            labelIcon="info"
            // tslint:disable-next-line: max-line-length
            labelTitle="Provider IP ex.10.0.0.1 and subnet ex.10.0.0.1/32"
            width={6}
            placeholder="Enter IP address Space "
            error={this.state.errorCircuit.ip_address}
            name="ip_address"
            onChange={this.handleChangeCircuit}
          />

          <Input
            field={{
              label: "Provider Phone Number",
              type: "TEXT",
              value: this.state.circuitInfo.contact_phone,
              isRequired: false,
            }}
            width={6}
            placeholder="Enter Provider Phone Number"
            error={this.state.errorCircuit.contact_phone}
            name="contact_phone"
            onChange={this.handleChangeCircuit}
          />
          <Input
            field={{
              label: "Provider Email",
              type: "TEXT",
              value: this.state.circuitInfo.email,
              isRequired: false,
            }}
            error={this.state.errorCircuit.email}
            width={6}
            placeholder="Enter email"
            name="email"
            onChange={this.handleChangeCircuit}
          />
          <div
            className="band-width-section
            field-section col-lg-6 col-md-6"
          >
            <Input
              field={{
                label: "Bandwidth",
                type: "TEXT",
                value: this.state.circuitInfo.bandwidth,
                isRequired: false,
              }}
              width={8}
              placeholder="Enter Bandwidth"
              error={this.state.errorCircuit.bandwidth}
              name="bandwidth"
              onChange={this.handleChangeCircuit}
            />
            <Input
              field={{
                value: this.state.circuitInfo.bandwidth_frequency,
                label: "Frequency",
                type: "PICKLIST",
                isRequired: false,
                options: [
                  {
                    value: "GB",
                    label: "GB",
                  },
                  {
                    value: "MB",
                    label: "MB",
                  },
                ],
              }}
              width={4}
              name="bandwidth_frequency"
              onChange={this.handleChangeCircuit}
              placeholder="Select"
            />
          </div>
          <Input
            field={{
              label: "Account Number",
              type: "TEXT",
              value: this.state.circuitInfo.account_number,
              isRequired: false,
            }}
            width={6}
            placeholder="Enter Account Number"
            error={this.state.errorCircuit.account_number}
            name="account_number"
            onChange={this.handleChangeCircuit}
          />
          <Input
            field={{
              value: this.state.circuitInfo.note,
              label: "Notes",
              type: "TEXTAREA",
              isRequired: false,
            }}
            width={12}
            name="note"
            onChange={this.handleChangeCircuit}
            placeholder="Enter notes"
          />
          <Input
            field={{
              value: this.state.circuitInfo.contract_end_date,
              label: "Contract End Date",
              type: "DATE",
              isRequired: false,
            }}
            width={6}
            name="contract_end_date"
            onChange={this.handleChangeCircuit}
            placeholder="Select Date"
            showTime={false}
            showDate={true}
          />
          <div className="field-section lao-section col-md-6 col-xs-6">
            <Checkbox
              isChecked={this.state.circuitInfo.loa}
              name="loa"
              onChange={(e) => this.handleChangeType(e)}
            >
              Letter of Authorization
            </Checkbox>
            {this.state.circuitInfo.loa && (
              <Input
                field={{
                  label: "",
                  type: "FILE",
                  value: `${
                    this.state.attachment ? this.state.attachment : ""
                  }`,
                  id: "attachment",
                }}
                error={this.state.errorCircuit.attachment}
                width={12}
                name="attachment"
                accept="image/png, image/jpg, image/jpeg"
                onChange={this.handleFile}
                className="custom-file-input  lao-check"
              />
            )}
            {this.state.errorCircuit.loa.errorMessage && (
              <div className="select-file-error">
                {this.state.errorCircuit.loa.errorMessage}
              </div>
            )}
          </div>

          {this.state.circuitInfo.loa && this.state.circuitInfo.attachment && (
            // tslint:disable-next-line:max-line-length
            <div className="view-lao field-section lao-check col-md-6 col-xs-6">
              <SquareButton
                content={`View LOA`}
                bsStyle={"primary"}
                onClick={this.onClickDownload}
                className="view-lao-btn"
              />
            </div>
          )}
        </div>
      </div>
    );
  };

  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    if (
      attachment &&
      attachment.type &&
      ["application/pdf", "image/png", "image/jpg", "image/jpeg"].includes(
        attachment.type
      )
    ) {
      const errorCircuit = this.getEmptyState().errorCircuit;

      errorCircuit.attachment.errorState = "success";
      errorCircuit.attachment.errorMessage = "";
      this.setState({ attachment, errorCircuit });
    } else {
      const errorCircuit = this.getEmptyState().errorCircuit;

      errorCircuit.attachment.errorState = "error";
      errorCircuit.attachment.errorMessage =
        "Please select Images and PDF file only.";

      this.setState({ attachment, errorCircuit });
    }
  };

  onClickDownload = () => {
    window.open(this.state.circuitInfo.attachment, "letter od authorisation");
  };

  renderViewCircuit = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching || this.props.isFetchingSite} />
        </div>
        <div
          className={`add-circuit__body ${
            this.props.isFetching || this.props.isFetchingSite ? `loading` : ""
          }`}
        >
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Site
              </label>
            </div>
            <div>{this.state.circuitInfo.cw_site_name || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Circuit Type
              </label>
            </div>
            <div>{this.state.circuitInfo.circuit_type_name || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                IP Address
              </label>
            </div>
            <div>{this.state.circuitInfo.ip_address || "N.A."}</div>
          </div>

          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Provider
              </label>
            </div>
            <div>{this.state.circuitInfo.provider || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Circuit ID
              </label>
            </div>
            <div>{this.state.circuitInfo.circuit_id || "Unknown"}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Contact Phone
              </label>
            </div>
            <div>{this.state.circuitInfo.contact_phone || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Provider Email
              </label>
            </div>
            <div>{this.state.circuitInfo.email || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Bandwidth
              </label>
            </div>
            <div>
              {this.state.circuitInfo.bandwidth
                ? `${this.state.circuitInfo.bandwidth}
           ${this.state.circuitInfo.bandwidth_frequency}`
                : "N.A."}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Account Number
              </label>
            </div>
            <div>{this.state.circuitInfo.account_number || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Notes
              </label>
            </div>
            <div>{this.state.circuitInfo.note || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Contract End Date
              </label>
            </div>
            <div>
              {this.state.circuitInfo.contract_end_date
                ? moment(this.state.circuitInfo.contract_end_date).format(
                    "MM/DD/YYYY"
                  )
                : "N.A."}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Letter of Authorization
              </label>
            </div>
            <div> {this.state.circuitInfo.loa ? "Yes" : "No"}</div>
            {this.state.circuitInfo.loa && this.state.circuitInfo.attachment && (
              // tslint:disable-next-line:max-line-length
              <div className="only-view-lao field-section lao-check col-md-6 col-xs-6">
                <SquareButton
                  content={`View LOA`}
                  bsStyle={"primary"}
                  onClick={this.onClickDownload}
                  className=""
                />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };
  handleChangeType = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.checked;

    this.setState((prevState) => ({
      circuitInfo: {
        ...prevState.circuitInfo,
        [targetName]: targetValue,
      },
    }));
  };

  isValidCircuit = () => {
    const errorCircuit = this.getEmptyState().errorCircuit;
    let isValid = true;

    if (!this.state.circuitInfo.cw_site_id) {
      errorCircuit.site.errorState = "error";
      errorCircuit.site.errorMessage = "Please select site";

      isValid = false;
    }

    if (!this.state.circuitInfo.circuit_type) {
      errorCircuit.circuit_type.errorState = "error";
      errorCircuit.circuit_type.errorMessage = "Please select type";

      isValid = false;
    }
    if (!this.state.circuitInfo.provider) {
      errorCircuit.provider.errorState = "error";
      errorCircuit.provider.errorMessage = "Please enter provider ";

      isValid = false;
    }

    if (!this.state.circuitInfo.circuit_id) {
      errorCircuit.circuit_id.errorState = "error";
      errorCircuit.circuit_id.errorMessage = "Please enter circuit ID ";

      isValid = false;
    }
    if (
      this.state.circuitInfo.contact_phone &&
      !AppValidators.isPhoneNumber(this.state.circuitInfo.contact_phone)
    ) {
      errorCircuit.contact_phone.errorState = "error";
      errorCircuit.contact_phone.errorMessage = "Enter a valid phone number";

      isValid = false;
    }
    if (
      this.state.circuitInfo.email &&
      !AppValidators.isValidEmail(this.state.circuitInfo.email)
    ) {
      errorCircuit.email.errorState = "error";
      errorCircuit.email.errorMessage = "Enter a valid email";

      isValid = false;
    }

    if (this.state.circuitInfo.loa && !this.state.attachment) {
      errorCircuit.loa.errorState = "error";
      errorCircuit.loa.errorMessage = "select an attachment";

      isValid = false;
    }
    if (
      this.state.attachment &&
      ["application/pdf", "image/png", "image/jpg", "image/jpeg"].indexOf(
        this.state.attachment.type,
        0
      ) === -1
    ) {
      errorCircuit.attachment.errorState = "error";
      errorCircuit.attachment.errorMessage =
        "Please select Images and PDF file only.";
      isValid = false;
    }

    this.setState({
      errorCircuit,
    });

    return isValid;
  };
  onCircuitAdd = () => {
    if (this.isValidCircuit()) {
      const data = new FormData();
      if (this.state.attachment) {
        data.append("attachment", this.state.attachment);
      }
      data.append("id", isValue(this.state.circuitInfo.id));
      data.append("circuit_id", isValue(this.state.circuitInfo.circuit_id));
      data.append("circuit_type", isValue(this.state.circuitInfo.circuit_type));
      data.append(
        "contact_phone",
        isValue(this.state.circuitInfo.contact_phone)
      );
      data.append(
        "contract_end_date",
        isValue(this.state.circuitInfo.contract_end_date) &&
          fromISOStringToFormattedDate(
            this.state.circuitInfo.contract_end_date,
            "YYYY-MM-DDThh:mm"
          )
      );
      data.append("device_crm_id", this.state.circuitInfo.device_crm_id || "");
      data.append("ip_address", this.state.circuitInfo.ip_address || "");
      data.append("cw_site_id", this.state.circuitInfo.cw_site_id);
      data.append("cw_site_name", this.state.circuitInfo.cw_site_name);
      data.append("email", isValue(this.state.circuitInfo.email));
      data.append("loa", this.state.circuitInfo.loa.toString());
      data.append("note", isValue(this.state.circuitInfo.note));
      data.append("provider", this.state.circuitInfo.provider);
      data.append("site", this.state.circuitInfo.site);
      data.append(
        "account_number",
        this.state.circuitInfo.account_number || ""
      );
      if (this.state.circuitInfo && this.state.circuitInfo.bandwidth) {
        data.append(
          "bandwidth",
          `${this.state.circuitInfo.bandwidth} ${this.state.circuitInfo.bandwidth_frequency}`
        );
      } else {
        data.append("bandwidth", ``);
      }
      if (this.state.circuitInfo.id) {
        this.props
          .editCircuitInfo(data, this.state.circuitInfo.id)
          .then((action) => {
            if (action.type === ADD_CIRCUIT_INFO_SUCCESS) {
              this.props.fetchCircuitInfo();
              this.setState({ attachment: null, openModalCircuit: false });
            }
            if (action.type === ADD_CIRCUIT_INFO_FAILURE) {
              this.setValidationErrorsCircuit(action.errorList.data);
            }
          });
      } else {
        this.props.addCircuitInfo(data).then((action) => {
          if (action.type === ADD_CIRCUIT_INFO_SUCCESS) {
            this.props.fetchCircuitInfo();
            this.setState({ attachment: null, openModalCircuit: false });
          }
          if (action.type === ADD_CIRCUIT_INFO_FAILURE) {
            this.setValidationErrorsCircuit(action.errorList.data);
          }
        });
      }
    }
  };
  onViewRowClickCircuit = (rowInfo) => {
    this.setState({
      openViewModalCircuit: !this.state.openViewModalCircuit,
      circuitInfo: {
        ...rowInfo.original,
      },
    });
  };

  setValidationErrorsCircuit = (errorList) => {
    const newState: ICircuitsSitesState = cloneDeep(this.state);

    Object.keys(errorList).map((key) => {
      newState.errorCircuit[key].errorState = "error";
      newState.errorCircuit[key].errorMessage = errorList[key];
    });
    this.setState(newState);
  };

  renderFooterCircuit = () => {
    return (
      <div className={`${this.props.isFetching ? `loading` : ""}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.toggleNewModal}
        />
        <SquareButton
          content={`${this.state.isEditPopUp ? "Update" : "Add"}`}
          bsStyle={"primary"}
          onClick={this.onCircuitAdd}
        />
      </div>
    );
  };

  handleChangeSearch = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };

  handleChangeSearchCircuit = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchStringCircuit: searchString }, () => {
      this.handleRowsCircuits();
    });
  };

  toggleViewDeviceModal = () => {
    this.setState((prevState) => ({
      isViewDeviceModalOpen: !prevState.isViewDeviceModalOpen,
    }));
  };

  openURL = (event: any, url) => {
    event.stopPropagation();
    if (url) {
      window.open(url);
    }
  };

  onViewDevice = (original, e) => {
    e.stopPropagation();
    if (allowPermission("view_device")) {
      this.props.fetchSingleDevice(original.device_crm_id).then((action) => {
        if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
          this.props.fetchExistingDeviceAssociation(original.device_crm_id);
          this.setState(() => ({
            isViewDeviceModalOpen: true,
          }));
        } else if (action.type === FETCH_SINGLE_DEVICE_FAILURE) {
          this.props.addErrorMessage(
            action.errorList && action.errorList.data.message
          );
        }
      });
    }
  };

  toggleFilterSiteModal = () => {
    this.setState((prevState) => ({
      isFilterSiteModalOpen: !prevState.isFilterSiteModalOpen,
    }));
  };

  onFiltersSiteUpdate = (filters: ISiteFilters) => {
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState({
      rows,
      filtersites: filters,
      isFilterSiteModalOpen: false,
    });
  };
  renderFilters = () => {
    const { filtersites, siteLabelIds } = this.state;
    const shouldRenderFilters = filtersites
      ? filtersites.site && filtersites.site.length > 0
      : false;

    return shouldRenderFilters ? (
      <div className="sites__filters">
        <label className="applied-filter">Applied Filters: </label>
        {Object.keys(filtersites).map((filterName, filterIndex) => {
          const filterValues = filtersites[filterName];
          let labelIds = {};
          if (filterName === "site") {
            labelIds = siteLabelIds;
          }

          if (filterValues.length > 0) {
            return filterValues.map((id, valueIndex) => (
              <div
                key={`${filterIndex}.${valueIndex}`}
                className="sites__filter"
              >
                <label>{labelIds[id] ? labelIds[id] : "N.A."}</label>
                <span
                  onClick={() => this.deleteFilter(filterName, valueIndex)}
                />
              </div>
            ));
          }
        })}
      </div>
    ) : null;
  };

  deleteFilter = (filterName: string, attributeIndex: number) => {
    const prevFilters = this.state.filtersites;
    const filters = {
      ...prevFilters,
      [filterName]: [
        ...prevFilters[filterName].slice(0, attributeIndex),
        ...prevFilters[filterName].slice(attributeIndex + 1),
      ],
    };
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState(() => ({
      filtersites: filters,
      rows,
    }));
  };

  toggleFilterCircuitModal = () => {
    this.setState((prevState) => ({
      isFilterCircuitModalOpen: !prevState.isFilterCircuitModalOpen,
    }));
  };

  onFiltersCircuitUpdate = (filters: ICircuitFilters) => {
    const rows = this.getRowsCircuit(
      this.props,
      this.state.searchStringCircuit,
      filters
    );

    this.setState({
      circuitRows: rows,
      filterCircuites: filters,
      isFilterCircuitModalOpen: false,
    });
  };
  renderFiltersCircuit = () => {
    const {
      filterCircuites,
      siteLabelIds,
      typesLabelIds,
      isExistLabelIds,
    } = this.state;
    const shouldRenderFilters = filterCircuites
      ? (filterCircuites.site && filterCircuites.site.length > 0) ||
        (filterCircuites.type && filterCircuites.type.length > 0) ||
        (filterCircuites.is_exist && filterCircuites.is_exist.length > 0)
      : false;

    return shouldRenderFilters ? (
      <div className="sites__filters">
        <label className="applied-filter">Applied Filters: </label>
        {Object.keys(filterCircuites).map((filterName, filterIndex) => {
          const filterValues = filterCircuites[filterName];
          let labelIds = {};
          if (filterName === "site") {
            labelIds = siteLabelIds;
          } else if (filterName === "type") {
            labelIds = typesLabelIds;
          } else if (filterName === "is_exist") {
            labelIds = isExistLabelIds;
          }

          if (filterValues.length > 0) {
            return filterValues.map((id, valueIndex) => (
              <div
                key={`${filterIndex}.${valueIndex}`}
                className="sites__filter"
              >
                <label>{labelIds[id] ? labelIds[id] : "N.A."}</label>
                <span
                  onClick={() =>
                    this.deleteFilterCircuit(filterName, valueIndex)
                  }
                />
              </div>
            ));
          }
        })}
      </div>
    ) : null;
  };

  deleteFilterCircuit = (filterName: string, attributeIndex: number) => {
    const prevFilters = this.state.filterCircuites;
    const filters = {
      ...prevFilters,
      [filterName]: [
        ...prevFilters[filterName].slice(0, attributeIndex),
        ...prevFilters[filterName].slice(attributeIndex + 1),
      ],
    };
    const rows = this.getRowsCircuit(
      this.props,
      this.state.searchStringCircuit,
      filters
    );

    this.setState(() => ({
      filterCircuites: filters,
      circuitRows: rows,
    }));
  };
  render() {
    const currentPage = this.state.currentPage;
    const columnsCircuit: any = [
      {
        accessor: "cw_site_name",
        id: "cw_site_name",
        Header: "Site",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " Unknown"}`}</div>,
      },
      {
        accessor: "circuit_type_name",
        id: "circuit_type__name",
        Header: "Circuit Type",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "circuit_id",
        Header: "Circuit ID",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "provider",
        Header: "Provider",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "contact_phone",
        Header: "Provider Phone",
        sortable: true,
        Cell: (cell) => <div> {`${phoneNumberInFormat("", cell.value)}`}</div>,
      },
      {
        accessor: "index",
        Header: "Associated Device",
        width: 160,
        sortable: false,
        Cell: (cell) => (
          <div>
            {cell.original && cell.original.device_crm_id && (
              <SquareButton
                onClick={(e) => this.onViewDevice(cell.original, e)}
                content="View Device"
                bsStyle={"primary"}
                disabled={!cell.original.device_crm_id}
              />
            )}
            {cell.original &&
              !cell.original.device_crm_id &&
              "No Device Associated"}
          </div>
        ),
      },
      {
        accessor: "index",
        Header: "Edit",
        width: 90,
        sortable: false,
        Cell: (cell) => (
          <EditButton
            onClick={(e) =>
              this.onEditRowClickCircuit(cell.original, e, cell.index)
            }
          />
        ),
      },
    ];
    const columns: any = [
      {
        accessor: "name",
        Header: "Site",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "address_line_1",
        Header: "Address",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "city",
        Header: "City",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "state",
        Header: "State",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
    ];

    return (
      <div className="provider-documentation">
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === "customer" && (
            <div className="documentation">
              <div
                className={`loader  ${
                  this.props.isFetching ||
                  this.props.isFetchingSite ||
                  this.props.reportDownloading
                    ? `loading`
                    : ""
                }`}
              >
                <Spinner
                  show={
                    this.props.isFetching ||
                    this.props.isFetchingSite ||
                    this.props.reportDownloading
                  }
                />
              </div>
              {this.renderTopBar()}
              {currentPage.pageType === PageType.Site && (
                <div className="documentation-container">
                  <div className="search-add  header-panel">
                    <Input
                      field={{
                        value: this.state.searchString,
                        label: "",
                        type: "SEARCH",
                      }}
                      width={5}
                      name="searchString"
                      onChange={this.handleChangeSearch}
                      placeholder="Search"
                      className="actions-search search"
                    />
                    <div className="right-btn-container">
                      <SquareButton
                        onClick={this.toggleFilterSiteModal}
                        content={
                          <span>
                            <img alt="" src="/assets/icons/filter.png" />
                            Filters
                          </span>
                        }
                        disabled={
                          !this.props.sites ||
                          (this.props.sites && this.props.sites.length === 0)
                        }
                        bsStyle={"primary"}
                        className="header-buttons export-filter"
                      />
                      <SquareButton
                        onClick={() => this.toggleCreateSiteModal()}
                        content="Add New Site "
                        bsStyle={"primary"}
                        className="add-new site-circuit-btn"
                      />
                    </div>
                  </div>
                  {this.renderFilters()}
                  <div className="notes-list">
                    <Table
                      columns={columns}
                      rows={this.state.rows}
                      className={""}
                      onRowClick={this.onViewRowClick}
                      loading={this.props.isFetchingSite}
                    />
                  </div>
                  <ModalBase
                    show={this.state.isCreateSiteModal}
                    onClose={this.toggleCreateSiteModal}
                    titleElement={`${
                      this.state.isEditPopUp ? "Update" : "Add New"
                    } Site`}
                    bodyElement={
                      this.state.isCreateSiteModal && this.renderAddSite()
                    }
                    footerElement={this.renderFooter()}
                    className="add-customer-note"
                  />
                  <ModalBase
                    show={this.state.isViewSiteModal}
                    onClose={this.toggleViewSiteModal}
                    titleElement={`View Site`}
                    bodyElement={
                      this.state.isViewSiteModal && this.renderViewSite()
                    }
                    footerElement={
                      <SquareButton
                        content="Close"
                        bsStyle={"default"}
                        onClick={this.toggleViewSiteModal}
                      />
                    }
                    className="view-contacts"
                  />
                  <FilterSite
                    show={this.state.isFilterSiteModalOpen}
                    onClose={this.toggleFilterSiteModal}
                    onSubmit={this.onFiltersSiteUpdate}
                    sites={this.props.sites}
                    prevFilters={this.state.filtersites}
                  />
                </div>
              )}
              {currentPage.pageType === PageType.Circuit && (
                <div className="documentation-container">
                  <div className="search-add  header-panel">
                    <Input
                      field={{
                        value: this.state.searchStringCircuit,
                        label: "",
                        type: "SEARCH",
                      }}
                      width={6}
                      name="searchString"
                      onChange={this.handleChangeSearchCircuit}
                      placeholder="Search"
                      className="actions-search search"
                    />
                    <div className="right-btn-container">
                      <SquareButton
                        onClick={this.toggleFilterCircuitModal}
                        content={
                          <span>
                            <img alt="" src="/assets/icons/filter.png" />
                            Filters
                          </span>
                        }
                        disabled={
                          !this.props.circuitInfoList ||
                          (this.props.circuitInfoList &&
                            this.props.circuitInfoList.length === 0)
                        }
                        bsStyle={"primary"}
                        className="header-buttons export-filter"
                      />
                      <SquareButton
                        onClick={() => this.toggleNewModal()}
                        content="Add New Circuit "
                        bsStyle={"primary"}
                        className="add-new site-circuit-btn"
                      />
                    </div>
                  </div>
                  {this.renderFiltersCircuit()}
                  <ModalBase
                    show={this.state.openModalCircuit}
                    onClose={this.toggleNewModal}
                    titleElement={`${
                      this.state.isEditPopUp ? "Update" : "Add New"
                    } Circuit Info`}
                    bodyElement={
                      this.state.openModalCircuit && this.renderAddCircuit()
                    }
                    footerElement={this.renderFooterCircuit()}
                    className="add-circuit-info"
                  />
                  <ModalBase
                    show={this.state.openViewModalCircuit}
                    onClose={this.toggleViewModalCircuit}
                    titleElement={`View Circuit Info`}
                    bodyElement={
                      this.state.openViewModalCircuit &&
                      this.renderViewCircuit()
                    }
                    footerElement={
                      <SquareButton
                        content="Close"
                        bsStyle={"default"}
                        onClick={this.toggleViewModalCircuit}
                      />
                    }
                    className="view-contacts"
                  />
                  <div className="notes-list">
                    <Table
                      columns={columnsCircuit}
                      rows={this.state.circuitRows}
                      className={""}
                      loading={this.props.isFetching}
                      onRowClick={this.onViewRowClickCircuit}
                    />
                    <ViewDevice
                      {...this.props}
                      show={this.state.isViewDeviceModalOpen}
                      onClose={this.toggleViewDeviceModal}
                      manufacturers={this.props.manufacturers}
                      sites={this.props.sites}
                      fetchSites={this.props.fetchSites}
                      deviceDetails={this.state.deviceDetails}
                      openURL={this.openURL}
                      isFetching={this.props.isDeviceFetching}
                      resetRetry={this.props.resetManufacturerApiRetryCount}
                      userType={this.props.user && this.props.user.type}
                      existingAssociationList={
                        this.props.existingAssociationList
                      }
                    />
                    <CircuitFilter
                      show={this.state.isFilterCircuitModalOpen}
                      onClose={this.toggleFilterCircuitModal}
                      onSubmit={this.onFiltersCircuitUpdate}
                      sites={this.props.sites}
                      types={this.props.circuitTypes}
                      prevFilters={this.state.filterCircuites}
                    />
                  </div>
                </div>
              )}
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  isFetching: state.documentation.isFetching,
  isFetchingSite: state.inventory.isFetching,
  customerId: state.customer.customerId,
  sites: state.inventory.sites,
  countries: state.inventory.countries,
  circuitInfoList: state.documentation.circuitInfoList,
  circuitTypes: state.documentation.circuitTypes,
  manufacturers: state.inventory.manufacturers,
  deviceDetails: state.inventory.device,
  isDeviceFetching: state.inventory.isDeviceFetching,
  existingAssociationList: state.inventory.existingAssociationList,
  deviceList: state.documentation.deviceList,
  isFetchingDevices: state.documentation.isFetchingDevices,
  isFetchingTypes: state.documentation.isFetchingTypes,
  reportDownloading: state.report.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSites: () => dispatch(fetchSitesCustomersUser()),
  addSite: (newSite: any) => dispatch(addSiteCU(newSite)),
  editSite: (newSite: any) => dispatch(editSiteCU(newSite)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
  fetchCircuitInfo: (params?: IServerPaginationParams) =>
    dispatch(fetchCircuitInfoCU(params)),
  fetchCircuitTypes: () => dispatch(fetchCircuitTypes()),
  createCircuitTypes: (name: string) => dispatch(createCircuitTypes(name)),
  addCircuitInfo: (circuitInfo: any) => dispatch(addCircuitInfoCU(circuitInfo)),
  editCircuitInfo: (circuitInfo: any, id: any) =>
    dispatch(editCircuitInfoCU(circuitInfo, id)),
  fetchSingleDevice: (id: number) => dispatch(fetchSingleDevice(id)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchExistingDeviceAssociation: (deviceId: number) =>
    dispatch(fetchExistingDeviceAssociationCU(deviceId)),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchDevicesShort: () => dispatch(fetchDevicesShortCU()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CircuitsSitesCustomer);
