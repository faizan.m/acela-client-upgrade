import cloneDeep from 'lodash/cloneDeep';
import React from 'react';
import { connect } from 'react-redux';
import Spinner from '../../../components/Spinner';
import Validator from '../../../utils/validator';

import {
  DOWNLOAD_FILE_SUCCESS,
  downloadFilefromDropBoxCU,
  FETCH_PATH_LISTING_SUCCESS,
  fetchPathListingCU,
} from '../../../actions/documentation';
import '../Provider/style.scss';

interface IStorageServiceCUProps {
  isFetching: boolean;
  fetchPathListing: any;
  downloadFilefromDropBox: any;
  documentsList: any[];
  user: ISuperUser;
}

interface IStorageServiceCUState {
  data: any;
  lastPath: string;
  showBackArrow: boolean;
  fullPath: string;
}

//TODO: Remove update state durung render error
class StorageServiceCU extends React.Component<
  IStorageServiceCUProps,
  IStorageServiceCUState
> {
  static validator = new Validator();
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  static emptyState: IStorageServiceCUState = {
    lastPath: '',
    data: null,
    showBackArrow: false,
    fullPath: '',
  };

  constructor(props: IStorageServiceCUProps) {
    super(props);

    this.state = cloneDeep(StorageServiceCU.emptyState);
  }

  componentDidMount() {
    this.props.fetchPathListing();
    this.setState({
      showBackArrow: false,
    });
  }

  getBody = () => {
    const documentsList = Object.keys(this.props.documentsList || {});
    const data =
      documentsList &&
      documentsList.length > 0 &&
      this.props.documentsList[documentsList[0]];
    const currentPath =
      data && data.value.substr(0, data.value.lastIndexOf('/'));

    const backPath =
      currentPath && currentPath.substr(0, currentPath.lastIndexOf('/'));

    return (
      <div className="profile__details">
        <div className="profile__details-header">
          <h5>Storage Service </h5>
          <div className="docuentaion-documents">
            <div className="docuentaion-breadcrumbs">
              {this.state.fullPath && (
                <div
                  className="root"
                  onClick={() => {
                    this.openFolder('');
                  }}
                >
                  <img
                    alt=""
                    src="/assets/icons/root-directory.png"
                    title="root folder"
                  />{' '}
                  /
                </div>
              )}
              {this.state.fullPath &&
                this.state.fullPath.split('/').map((key, fieldIndex) => {
                  return (
                    <div className="document" key={fieldIndex}>
                      <div
                        className="folder"
                        onClick={() => {
                          this.openFolder(
                            this.state.fullPath
                              .split('/')
                              .slice(0, fieldIndex + 1)
                              .join('/')
                          );
                        }}
                      >
                        {key} /
                      </div>
                    </div>
                  );
                })}
            </div>

            {this.state.showBackArrow && (
              <div
                className="back"
                onClick={() => {
                  this.openFolder(backPath || '');
                }}
              >
                <img alt="" src="/assets/icons/back.png" />
              </div>
            )}
            {Object.keys(this.props.documentsList).map((key, fieldIndex) => {
              const attributes = this.props.documentsList[key];

              return (
                <div className="document" key={fieldIndex}>
                  {attributes.type === 'Folder' ? (
                    <div
                      className="folder"
                      onClick={() => {
                        this.openFolder(attributes.value);
                      }}
                    >
                      <img alt="" src="/assets/icons/folder.png" />

                      {attributes.value.substr(
                        attributes.value.lastIndexOf('/') + 1
                      )}
                    </div>
                  ) : (
                    <div
                      className="file"
                      onClick={() => {
                        this.downloadFile(attributes.value);
                      }}
                    >
                      <img alt="" src="/assets/icons/file.png" />
                      {attributes.value.substr(
                        attributes.value.lastIndexOf('/') + 1
                      )}
                    </div>
                  )}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
  };

  openFolder = value => {
    this.props.fetchPathListing(value).then(action => {
      if (action.type === FETCH_PATH_LISTING_SUCCESS) {
        this.setState({
          showBackArrow: true,
          lastPath: value.substr(0, value.lastIndexOf('/')),
        });
        if (value === '') {
          this.setState({
            showBackArrow: false,
          });
        }
      }
    });
    this.setState({
      fullPath: value,
      lastPath: value.substr(0, value.lastIndexOf('/')),
    });
  };

  downloadFile = value => {
    this.props.downloadFilefromDropBox(value).then(action => {
      if (action.type === DOWNLOAD_FILE_SUCCESS) {
        if (action.response && action.response !== '') {
          const url = action.response.file_path;
          const link = document.createElement('a');
          link.href = url;
          link.target = '_blank';
          link.setAttribute('download', action.response.file_name);
          document.body.appendChild(link);
          link.click();
        }
      }
    });
  };

  render() {
    return (
      <div className="profile">
        <div className="profile__header">
          <div className="loader">
            <Spinner show={this.props.isFetching} />
          </div>
          <div className="profile__Body">{this.getBody()}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.documentation.isFetching,
  user: state.profile.user,
  documentsList: state.documentation.documentsList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPathListing: (folderName?: string) =>
    dispatch(fetchPathListingCU(folderName)),
  downloadFilefromDropBox: (fileName: string) =>
    dispatch(downloadFilefromDropBoxCU(fileName)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StorageServiceCU);
