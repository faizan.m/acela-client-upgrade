import React from "react";

import SquareButton from "../../components/Button/button";
import Select from "../../components/Input/Select/select";
import ModalBase from "../../components/ModalBase/modalBase";

interface ICircuitFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: ICircuitFilters) => void;
  prevFilters: ICircuitFilters;
  sites: any[];
  types: any[];
}

interface ICircuitFilterState {
  filters: ICircuitFilters;
}

export default class CircuitFilter extends React.Component<
  ICircuitFilterProps,
  ICircuitFilterState
> {
  constructor(props: ICircuitFilterProps) {
    super(props);

    this.state = {
      filters: {
        site: [],
        type: [],
        is_exist: [],
      },
    };
  }

  componentDidUpdate(prevProps: ICircuitFilterProps) {
    const { show, prevFilters } = this.props;
    if (show !== prevProps.show && show) {
      this.setState({
        filters: prevFilters ? prevFilters : {},
      });
    }
  }

  onClose = (e) => {
    this.props.onClose(e);
  };

  onSubmit = (e) => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  getBody = () => {
    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];
    const types = this.props.types
      ? this.props.types.map((type, index) => ({
          value: type.id,
          label: type.name,
        }))
      : [];
    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        {" "}
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Site</label>
            <div className="field__input">
              <Select
                name="site"
                value={filters.site}
                onChange={this.onFilterChange}
                options={sites}
                multi={true}
                placeholder="Select Site"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Circuit ID</label>
            <div className="field__input">
              <Select
                name="is_exist"
                value={filters.is_exist}
                onChange={this.onFilterChange}
                options={[
                  { value: "EXIST", label: "Exist" },
                  { value: "NOTEXIST", label: "Not Exist" },
                ]}
                multi={true}
                placeholder="Select"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Type</label>
            <div className="field__input">
              <Select
                name="type"
                value={filters.type}
                onChange={this.onFilterChange}
                options={types}
                multi={true}
                placeholder="Select Type"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
