import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";
import {
  ADD_CUSTOMER_NOTE_FAILURE,
  ADD_CUSTOMER_NOTE_SUCCESS,
  ADD_NETWORK_NOTE_SUCCESS,
  addCustomerNote,
  editCustomerNote,
  editNetworkNote,
  fetchCustomerNotes,
  fetchNoteTypes,
  getNetworkNote,
} from "../../../actions/documentation";
import SquareButton from "../../../components/Button/button";
import EditButton from "../../../components/Button/editButton";
import Checkbox from "../../../components/Checkbox/checkbox";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";
import ShowNote from "../../Renewal/showNote";
import "./style.scss";

enum PageType {
  CustomerNotes,
  NetworkNotes,
}

interface ICustomerInformationProps extends ICommonProps {
  user: ISuperUser;
  isFetching: boolean;
  customerNotes: ICustomerNote[];
  noteType: any;
  fetchNoteTypes: any;
  fetchCustomerNotes: any;
  customerId: number;
  addCustomerNote: any;
  editCustomerNote: any;
  getNetworkNote: any;
  editNetworkNote: any;
  networkNote: string;
}

interface ICustomerInformationState {
  currentPage: {
    pageType: PageType;
  };
  isFetching: boolean;
  openModal: boolean;
  customerNotes: ICustomerNote[];
  customerNote: ICustomerNote;
  rows: ICustomerNote[];
  isEditPopUp: boolean;
  error: {
    text: IFieldValidation;
    note_type_id: IFieldValidation;
    flagged: IFieldValidation;
  };
  isNoteDetailVisible: boolean;
  noteDetails: string;
  networkNote: string;
}

class CustomerInformation extends React.Component<
  ICustomerInformationProps,
  ICustomerInformationState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: ICustomerInformationProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.CustomerNotes,
    },
    isFetching: false,
    openModal: false,
    customerNotes: [],
    rows: [],
    isEditPopUp: false,
    customerNote: {
      text: "",
      note_type_id: null,
      flagged: false,
    },
    isNoteDetailVisible: false,
    noteDetails: "",
    networkNote: "",

    error: {
      text: { ...CustomerInformation.emptyErrorState },
      note_type_id: { ...CustomerInformation.emptyErrorState },
      flagged: { ...CustomerInformation.emptyErrorState },
    },
  });
  componentDidMount() {
    this.props.fetchNoteTypes();
    if (this.props.customerId) {
      this.props.fetchCustomerNotes(this.props.customerId);
      this.props.getNetworkNote(this.props.customerId);
    }
    if (this.props.networkNote) {
      this.setState({ networkNote: this.props.networkNote });
    }
  }

  componentDidUpdate(prevProps: ICustomerInformationProps) {
    const { customerId, customerNotes, networkNote } = this.props;

    if (customerNotes !== prevProps.customerNotes) {
      this.setState({ customerNotes });
      this.setRows(this.props);
    }

    if (customerId !== prevProps.customerId) {
      this.setState({ networkNote: "" });
      this.props.fetchCustomerNotes(customerId);
      this.props.getNetworkNote(customerId);
    }

    if (networkNote !== prevProps.networkNote) {
      this.setState({ networkNote });
    }
  }

  setRows = (nextProps: ICustomerInformationProps) => {
    const customerNotes = nextProps.customerNotes;

    const rows = customerNotes.map((note, index) => ({
      text: note.text,
      last_updated: note.last_updated,
      updated_by: note.updated_by,
      note_type_name: note.note_type_name,
      id: note.id,
      index,
    }));

    this.setState({ rows });
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="documentation__header">
        {
          <div
            className={`documentation__header-link ${
              currentPage.pageType === PageType.CustomerNotes
                ? "documentation__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.CustomerNotes)}
          >
            Customer Notes
          </div>
        }
        {
          <div
            className={`documentation__header-link ${
              currentPage.pageType === PageType.NetworkNotes
                ? "documentation__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.NetworkNotes)}
          >
            Network Notes
          </div>
        }
      </div>
    );
  };

  toggleNewModal = () => {
    this.setState({
      openModal: !this.state.openModal,
      customerNote: this.getEmptyState().customerNote,
      isEditPopUp: false,
      error: this.getEmptyState().error,
    });
  };

  onEditRowClick(original: any, e: any): any {
    event.stopPropagation();
    this.setState({
      openModal: !this.state.openModal,
      customerNote: this.state.customerNotes[original.index],
      isEditPopUp: true,
    });
  }

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      customerNote: {
        ...prevState.customerNote,
        [targetName]: targetValue,
      },
    }));
  };

  handleChangeNetworkNote = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState({
      [targetName]: targetValue,
    });
  };

  handleChangeType = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.checked;

    this.setState((prevState) => ({
      customerNote: {
        ...prevState.customerNote,
        [targetName]: targetValue,
      },
    }));
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.customerNote.text ||
      this.state.customerNote.text.trim().length === 0
    ) {
      error.text.errorState = "error";
      error.text.errorMessage = "Enter a valid note";

      isValid = false;
    }

    if (!this.state.customerNote.note_type_id) {
      error.note_type_id.errorState = "error";
      error.note_type_id.errorMessage = "Please select note type";

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };
  renderAdd = () => {
    const noteType = this.props.noteType
      ? this.props.noteType.map((site) => ({
          value: site.note_type_id,
          label: site.note_type_name,
        }))
      : [];

    return (
      <div className="add">
        <div className="loader">
          <Spinner show={this.state.isFetching} />
        </div>
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Select Note Type
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${
              this.state.error.note_type_id.errorMessage ? `error-input` : ""
            }`}
          >
            <SelectInput
              name="note_type_id"
              value={this.state.customerNote.note_type_id}
              onChange={this.handleChange}
              options={noteType}
              searchable={true}
              placeholder="Select Note Type"
              clearable={false}
            />
          </div>
          {this.state.error.note_type_id.errorMessage && (
            <div className="select-type-error">
              {this.state.error.note_type_id.errorMessage}
            </div>
          )}
        </div>
        <div className="field-section flagged">
          <Checkbox
            isChecked={this.state.customerNote.flagged}
            name="flagged"
            onChange={(e) => this.handleChangeType(e)}
          >
            Flagged
          </Checkbox>
        </div>

        <Input
          field={{
            label: "Note",
            type: "TEXTAREA",
            isRequired: true,
            value: this.state.customerNote.text,
          }}
          width={12}
          error={this.state.error.text}
          name="text"
          onChange={this.handleChange}
          className="new__body__input"
        />
      </div>
    );
  };

  onSubmit = (e) => {
    if (this.isValid()) {
      this.setState({ isFetching: true });
      if (!this.state.customerNote.id) {
        this.props
          .addCustomerNote(this.props.customerId, this.state.customerNote)
          .then((action) => {
            if (action.type === ADD_CUSTOMER_NOTE_SUCCESS) {
              this.toggleNewModal();
              this.props.fetchCustomerNotes(this.props.customerId);
              this.setState({ isFetching: false });
            }
            if (action.type === ADD_CUSTOMER_NOTE_FAILURE) {
              this.setValidationErrors(action.errorList.data);
              this.setState({ isFetching: false });
            }
          });
      } else {
        this.props
          .editCustomerNote(this.props.customerId, this.state.customerNote)
          .then((action) => {
            if (action.type === ADD_CUSTOMER_NOTE_SUCCESS) {
              this.toggleNewModal();
              this.props.fetchCustomerNotes(this.props.customerId);
              this.setState({ isFetching: false });
            }
            if (action.type === ADD_CUSTOMER_NOTE_FAILURE) {
              this.setValidationErrors(action.errorList.data);
              this.setState({ isFetching: false });
            }
          });
      }
    }
  };

  saveNetworkNote = (e) => {
    this.props
      .editNetworkNote(this.props.customerId, this.state.networkNote)
      .then((action) => {
        if (action.type === ADD_NETWORK_NOTE_SUCCESS) {
          this.props.getNetworkNote(this.props.customerId);
        }
      });
  };
  setValidationErrors = (errorList) => {
    const newState: ICustomerInformationState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isFetching ? `loading` : ""}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.toggleNewModal}
        />
        <SquareButton
          content={`${this.state.isEditPopUp ? "Update" : "Add"}`}
          bsStyle={"primary"}
          onClick={this.onSubmit}
        />
      </div>
    );
  };

  openNoteDetails = (note: string) => {
    this.setState({
      isNoteDetailVisible: true,
      noteDetails: note,
    });
  };

  closeNoteDetails = () => {
    this.setState({
      isNoteDetailVisible: false,
      noteDetails: "",
    });
  };

  render() {
    const currentPage = this.state.currentPage;
    const columns: any = [
      {
        accessor: "text",
        Header: "Text",
        sortable: true,
        Cell: (cell) => <div>{getMarkUp(cell.value)}</div>,
      },
      {
        accessor: "note_type_name",
        Header: "Note Type",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "last_updated",
        Header: "Last Updated",
        sortable: true,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "updated_by",
        Header: "Updated By",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "index",
        Header: "Edit",
        width: 60,
        sortable: false,
        Cell: (cell) => (
          <EditButton onClick={(e) => this.onEditRowClick(cell.original, e)} />
        ),
      },
    ];

    const getMarkUp = (note) => {
      if (note) {
        if (note.length > 12) {
          return (
            <p>
              {note.slice(0, 12)}..&nbsp;
              <a onClick={() => this.openNoteDetails(note)}>Read More</a>
            </p>
          );
        } else {
          return <p>{note}</p>;
        }
      } else {
        return ``;
      }
    };

    return (
      <div
        className={`provider-documentation ${
          !this.props.customerId ? "provider-documentation-disable" : ""
        }`}
      >
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === "provider" && (
            <div className="documentation">
              <div className="loader">
                <Spinner show={this.props.isFetching} />
              </div>
              {this.renderTopBar()}
              {currentPage.pageType === PageType.CustomerNotes && (
                <div className="documentation-container">
                  {/* <h3>Device Manufacturer Accounts</h3> */}

                  <SquareButton
                    onClick={() => this.toggleNewModal()}
                    content="Add New Customer Notes "
                    bsStyle={"primary"}
                    className="add-new"
                  />

                  <ModalBase
                    show={this.state.openModal}
                    onClose={this.toggleNewModal}
                    titleElement={`${
                      this.state.isEditPopUp ? "Update" : "Add New"
                    } Customer Notes`}
                    bodyElement={this.state.openModal && this.renderAdd()}
                    footerElement={this.renderFooter()}
                    className="add-customer-note"
                  />
                  <div className="notes-list">
                    <Table
                      columns={columns}
                      rows={this.state.rows}
                      loading={this.props.isFetching}
                    />
                  </div>
                </div>
              )}
              {currentPage.pageType === PageType.NetworkNotes && (
                <div>
                  <Input
                    field={{
                      label: "Network Note",
                      type: "TEXTAREA",
                      isRequired: true,
                      value: this.state.networkNote,
                    }}
                    width={12}
                    name="networkNote"
                    onChange={this.handleChangeNetworkNote}
                    className="network-note"
                  />
                  <SquareButton
                    content="Save"
                    bsStyle={"primary"}
                    onClick={this.saveNetworkNote}
                    disabled={this.props.networkNote === this.state.networkNote}
                    className="save-network-note"
                  />
                </div>
              )}
              <ShowNote
                title="Note"
                onClose={this.closeNoteDetails}
                show={this.state.isNoteDetailVisible}
                note={this.state.noteDetails}
              />
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  noteType: state.documentation.noteType,
  customerNotes: state.documentation.customerNotes,
  networkNote: state.documentation.networkNote,
  isFetching: state.documentation.isFetching,
  customerId: state.customer.customerId,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchNoteTypes: () => dispatch(fetchNoteTypes()),
  fetchCustomerNotes: (customerId: string) =>
    dispatch(fetchCustomerNotes(customerId)),
  addCustomerNote: (customerId: number, customerNote: ICustomerNote) =>
    dispatch(addCustomerNote(customerId, customerNote)),
  editCustomerNote: (customerId: number, customerNote: ICustomerNote) =>
    dispatch(editCustomerNote(customerId, customerNote)),
  getNetworkNote: (customerId: number) => dispatch(getNetworkNote(customerId)),
  editNetworkNote: (customerId: number, networkNote: string) =>
    dispatch(editNetworkNote(customerId, networkNote)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerInformation);
