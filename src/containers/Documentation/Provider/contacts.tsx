import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import { addSuccessMessage } from "../../../actions/appState";
import {
  ADD_ESCALATION_FAILURE,
  ADD_ESCALATION_SUCCESS,
  ADD_NETWORK_NOTE_SUCCESS,
  addCustomerEscalation,
  addCustomerEscalationCU,
  DELETE_ESCALATION_SUCCESS,
  deleteCustomerEscalation,
  deleteCustomerEscalationCU,
  editCustomerEscalation,
  editCustomerEscalationCU,
  editDistributionList,
  editDistributionListCU,
  fetchAllCustomerUsers,
  fetchAllCustomerUsersCU,
  fetchCustomerEscalation,
  fetchCustomerEscalationCU,
  fetchEscalationFields,
  fetchNoteTypes,
  getNetworkNote,
  getNetworkNoteCU,
} from "../../../actions/documentation";
import {
  fetchSites,
  fetchSitesCustomersUser,
} from "../../../actions/inventory";
import SquareButton from "../../../components/Button/button";
import DeleteButton from "../../../components/Button/deleteButton";
import EditButton from "../../../components/Button/editButton";
import Checkbox from "../../../components/Checkbox/checkbox";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import { phoneNumberInFormat } from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";
import AppValidators from "../../../utils/validator";
import "./style.scss";

enum PageType {
  Escalations,
  distribution,
}

interface IContactsProps extends ICommonProps {
  user: ISuperUser;
  isFetching: boolean;
  customerEscalations: ICustomerEscalation[];
  fetchNoteTypes: any;
  fetchCustomerescalation: any;
  customerId: number;
  addCustomerEscalation: any;
  editCustomerEscalation: any;
  getNetworkNote: any;
  editDistributionList: any;
  networkNote: string;
  distributionList: IDistributionList;
  fetchEscalationFields: any;
  escalationFields: IEscalationFields;
  fetchAllCustomerUsers: any;
  users: any;
  fetchSites: any;
  deleteEscalation: any;
  sites: any;
  fetchCustomerescalationCU: any;
  addCustomerEscalationCU: any;
  editCustomerEscalationCU: any;
  getNetworkNoteCU: any;
  editDistributionListCU: any;
  fetchSitesCustomersUser: any;
  deleteEscalationCU: any;
  fetchAllCustomerUsersCU: any;
  addSuccessMessage: any;
}

interface IContactsState {
  currentPage: {
    pageType: PageType;
  };
  openModal: boolean;
  openViewModal: boolean;
  customerEscalations: ICustomerEscalation[];
  customerEscalation: ICustomerEscalation;
  rows: ICustomerEscalation[];
  isEditPopUp: boolean;
  error: {
    user: IFieldValidation;
    escalation_priority: IFieldValidation;
    escalation_type: IFieldValidation;
  };
  errorDistributionList: {
    alerts_and_notifications: IFieldValidation;
    monitoring_alerts: IFieldValidation;
    scheduled_reports: IFieldValidation;
    staffing_changes: IFieldValidation;
  };
  isNoteDetailVisible: boolean;
  noteDetails: string;
  networkNote: string;
  distributionList: IDistributionList;
  isValidDistributionList: boolean;
  selectedUser?: any;
  isopenConfirm: boolean;
  id: string;
}

class Contacts extends React.Component<IContactsProps, IContactsState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: IContactsProps) {
    super(props);
    this.state = this.getEmptyState();
    this.onViewRowClick = this.onViewRowClick.bind(this);
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Escalations,
    },
    openModal: false,
    openViewModal: false,
    customerEscalations: [],
    rows: [],
    isEditPopUp: false,
    customerEscalation: {
      escalation_priority: "",
      escalation_type: "",
      user: "",
      contact: "",
      authorize_changes: false,
      authorize_charges: false,
    },
    isNoteDetailVisible: false,
    noteDetails: "",
    networkNote: "",
    distributionList: {
      alerts_and_notifications: [],
      monitoring_alerts: [],
      scheduled_reports: [],
      staffing_changes: [],
    },
    isValidDistributionList: true,
    isopenConfirm: false,
    id: "",
    error: {
      user: { ...Contacts.emptyErrorState },
      escalation_priority: { ...Contacts.emptyErrorState },
      escalation_type: { ...Contacts.emptyErrorState },
    },
    errorDistributionList: {
      alerts_and_notifications: { ...Contacts.emptyErrorState },
      monitoring_alerts: { ...Contacts.emptyErrorState },
      scheduled_reports: { ...Contacts.emptyErrorState },
      staffing_changes: { ...Contacts.emptyErrorState },
    },
  });
  componentDidMount() {
    this.props.fetchEscalationFields();

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "provider"
    ) {
      if (this.props.customerId) {
        this.props.fetchCustomerescalation(this.props.customerId);
        this.props.getNetworkNote(this.props.customerId);
        this.props.fetchAllCustomerUsers(this.props.customerId);
        this.props.fetchSites(this.props.customerId);
      }
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.fetchCustomerescalationCU();
      this.props.getNetworkNoteCU();
      this.props.fetchAllCustomerUsersCU();
      this.props.fetchSitesCustomersUser();
    }

    if (this.props.distributionList) {
      this.setState({ distributionList: this.props.distributionList });
    }
  }

  componentDidUpdate(prevProps: IContactsProps) {
    const {
      customerId,
      customerEscalations,
      distributionList,
      user,
    } = this.props;

    if (
      customerEscalations !== prevProps.customerEscalations &&
      customerEscalations
    ) {
      this.setState({
        customerEscalations,
      });
      this.setRows(this.props);
    }
    if (customerId !== prevProps.customerId) {
      this.setState({
        networkNote: "",
        distributionList: this.getEmptyState().distributionList,
      });
      this.props.fetchCustomerescalation(customerId);
      this.props.getNetworkNote(customerId);
      this.props.fetchAllCustomerUsers(customerId);
      this.props.fetchSites(customerId);
    }
    if (distributionList !== prevProps.distributionList) {
      this.setState({ distributionList });
    }

    if (user !== prevProps.user) {
      if (user && user.type && user.type === "customer") {
        this.props.fetchCustomerescalationCU();
        this.props.getNetworkNoteCU();
        this.props.fetchAllCustomerUsersCU();
        this.props.fetchSitesCustomersUser();
      }
    }
  }

  setRows = (nextProps: IContactsProps) => {
    const customerEscalations = nextProps.customerEscalations;

    const rows =
      customerEscalations &&
      customerEscalations.map((note, index) => ({
        escalation_priority: note.escalation_priority,
        escalation_type: note.escalation_type,
        contact: `${note.user.first_name} ${note.user.last_name}`,
        authorize_changes: note.authorize_changes,
        authorize_charges: note.authorize_charges,
        userId: note.user.id,
        cw_site_id: note.cw_site_id,
        cell_phone: note.user.profile.cell_phone_number,
        country_code: note.user.profile.country_code,
        email: note.user.email,
        id: note.id,
        index,
      }));

    this.setState({ rows });
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="documentation__header">
        {
          <div
            className={`documentation__header-link ${
              currentPage.pageType === PageType.Escalations
                ? "documentation__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.Escalations)}
          >
            Customer Escalation
          </div>
        }
        {
          <div
            className={`documentation__header-link ${
              currentPage.pageType === PageType.distribution
                ? "documentation__header-link--active"
                : ""
            }`}
            onClick={() => this.changePage(PageType.distribution)}
          >
            Customer Distribution List
          </div>
        }
      </div>
    );
  };

  toggleNewModal = () => {
    this.setState({
      openModal: !this.state.openModal,
      customerEscalation: this.getEmptyState().customerEscalation,
      isEditPopUp: false,
    });
  };

  toggleViewModal = () => {
    this.setState({
      openViewModal: !this.state.openViewModal,
      customerEscalation: this.getEmptyState().customerEscalation,
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
    });
  };

  onEditRowClick(original: any, e: any): any {
    e.stopPropagation();
    const user = this.state.customerEscalations[original.index].user;
    this.setState({
      openModal: !this.state.openModal,
      customerEscalation: {
        ...this.state.customerEscalations[original.index],
        userId: user.id,
        contact: `${user.first_name} ${user.last_name}`,
        role: user.role_display_name,
        email: user.email,
        location: user.address,
        cell_phone: user.profile.cell_phone_number,
        work_phone: user.profile.office_phone,
      },
      isEditPopUp: true,
    });
  }

  onViewRowClick = (rowInfo) => {
    //event.stopPropagation();
    const userData: any =
      this.state.customerEscalations &&
      this.state.customerEscalations.filter(
        (row) => row.id === rowInfo.row._original.id
      )[0];

    this.setState({
      openViewModal: !this.state.openViewModal,
      customerEscalation: {
        ...userData,
        userId: userData.user.id,
        contact: `${userData.user.first_name} ${userData.user.last_name}`,
        role: userData.user.role_display_name,
        email: userData.user.email,
        location: userData.user.address,
        cell_phone: userData.user.profile.cell_phone_number,
        country_code: userData.user.profile.country_code,
        work_phone: userData.user.profile.office_phone,
      },
    });
  };

  onDeleteRowClick(original: any, e: any): any {
    e.stopPropagation();
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.id,
    });
  }

  onClickConfirm = () => {
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.deleteEscalationCU(this.state.id).then((action) => {
        if (action.type === DELETE_ESCALATION_SUCCESS) {
          this.props.fetchCustomerescalationCU();
          this.toggleConfirmOpen();
        }
      });
    }

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "provider"
    ) {
      this.props
        .deleteEscalation(this.props.customerId, this.state.id)
        .then((action) => {
          if (action.type === DELETE_ESCALATION_SUCCESS) {
            this.props.fetchCustomerescalation(this.props.customerId);
            this.toggleConfirmOpen();
          }
        });
    }
  };

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      customerEscalation: {
        ...prevState.customerEscalation,
        [targetName]: targetValue,
      },
    }));
  };

  handleChangeDistributionList = (event: any, name) => {
    const errorDistributionList = this.getEmptyState().errorDistributionList;
    let distributionList = event;

    distributionList.forEach((val) => {
      if (!AppValidators.isValidEmail(val)) {
        errorDistributionList[name].errorMessage = "Please enter valid email";
        errorDistributionList[name].errorState = "error";
      }
    });
    distributionList = distributionList.filter(
      (value, index, self) => self.indexOf(value) === index
    );

    this.setState((prevState) => ({
      distributionList: {
        ...prevState.distributionList,
        [name]: distributionList,
      },
      errorDistributionList,
    }));
  };

  handleChangeType = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.checked;

    this.setState((prevState) => ({
      customerEscalation: {
        ...prevState.customerEscalation,
        [targetName]: targetValue,
      },
    }));
  };

  handleChangeUser = (event: any) => {
    const targetValue = event.target.value;
    const user = this.props.users[
      this.props.users.findIndex((x) => x.id === targetValue)
    ];

    this.setState((prevState) => ({
      customerEscalation: {
        ...prevState.customerEscalation,
        userId: targetValue,
        contact: `${user.first_name} ${user.last_name}`,
        role: user.role_display_name,
        email: user.email,
        location: user.address,
        cell_phone: user.profile.cell_phone_number,
        work_phone: user.profile.office_phone,
      },
    }));
  };
  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.customerEscalation.userId) {
      error.user.errorState = "error";
      error.user.errorMessage = "Please select user";

      isValid = false;
    }

    if (!this.state.customerEscalation.escalation_priority) {
      error.escalation_priority.errorState = "error";
      error.escalation_priority.errorMessage =
        "Please select escalation priority type";

      isValid = false;
    }
    if (!this.state.customerEscalation.escalation_type) {
      error.escalation_type.errorState = "error";
      error.escalation_type.errorMessage = "Please select escalation type";

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };
  renderAdd = () => {
    const escalationTypes = this.props.escalationFields.escalation_types
      ? Object.keys(this.props.escalationFields.escalation_types).map(
          (site) => ({
            value: site,
            label: site,
          })
        )
      : [];

    const escalationPriorityTypes = this.props.escalationFields
      .escalation_priority_types
      ? Object.keys(this.props.escalationFields.escalation_priority_types).map(
          (site) => ({
            value: site,
            label: site,
          })
        )
      : [];

    const customerUsers = this.props.users
      ? this.props.users.map((user) => ({
          value: user.id,
          label: `${user.first_name} ${user.last_name}`,
        }))
      : [];

    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];

    return (
      <div className="add-escalation">
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              User
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${
              this.state.error.user.errorMessage ? `error-input` : ""
            }`}
          >
            {
              <SelectInput
                name="userId"
                value={this.state.customerEscalation.userId}
                onChange={this.handleChangeUser}
                options={customerUsers}
                searchable={true}
                placeholder="Select User"
                clearable={false}
                disabled={this.state.isEditPopUp}
              />
            }
          </div>
          {this.state.error.user.errorMessage && (
            <div className="select-type-error">
              {this.state.error.user.errorMessage}
            </div>
          )}
        </div>
        {!this.state.isEditPopUp && (
          <div
            className="select-type field-section
             col-md-6 col-xs-6"
          >
            {this.state.isEditPopUp && <div>ss</div>}
            <div className="field__label row">
              <label className="field__label-label" title="">
                Escalation Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.escalation_type.errorMessage
                  ? `error-input`
                  : ""
              }`}
            >
              <SelectInput
                name="escalation_type"
                value={this.state.customerEscalation.escalation_type}
                onChange={this.handleChange}
                options={escalationTypes}
                searchable={true}
                placeholder="Select Escalation Type"
                clearable={false}
              />
            </div>
            {this.state.error.escalation_type.errorMessage && (
              <div className="select-type-error">
                {this.state.error.escalation_type.errorMessage}
              </div>
            )}
          </div>
        )}
        {this.state.isEditPopUp && (
          <div
            className="select-type field-section
             col-md-6 col-xs-6"
          />
        )}
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Escalation Priority
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${
              this.state.error.escalation_priority.errorMessage
                ? `error-input`
                : ""
            }`}
          >
            <SelectInput
              name="escalation_priority"
              value={this.state.customerEscalation.escalation_priority}
              onChange={this.handleChange}
              options={escalationPriorityTypes}
              searchable={true}
              placeholder="Select Escalation Priority"
              clearable={false}
            />
          </div>
          {this.state.error.escalation_priority.errorMessage && (
            <div className="select-type-error">
              {this.state.error.escalation_priority.errorMessage}
            </div>
          )}
        </div>
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Location
            </label>
          </div>
          <div>
            <SelectInput
              name="cw_site_id"
              value={this.state.customerEscalation.cw_site_id}
              onChange={this.handleChange}
              options={sites}
              searchable={true}
              placeholder="Select site"
              clearable={false}
            />
          </div>
        </div>
        <div className="field-section  col-md-6 col-xs-6">
          <Checkbox
            isChecked={this.state.customerEscalation.authorize_changes}
            name="authorize_changes"
            onChange={(e) => this.handleChangeType(e)}
          >
            Authorize Changes?
          </Checkbox>
        </div>

        <div className="field-section  col-md-6 col-xs-6">
          <Checkbox
            isChecked={this.state.customerEscalation.authorize_charges}
            name="authorize_charges"
            onChange={(e) => this.handleChangeType(e)}
          >
            Authorize Charges?
          </Checkbox>
        </div>
        {this.state.customerEscalation.contact && (
          <Input
            field={{
              label: "Contact",
              type: "TEXT",
              isRequired: false,
              value: this.state.customerEscalation.contact,
            }}
            width={6}
            name="firstName"
            onChange={this.handleChange}
            className="new__body__input"
            disabled={true}
          />
        )}

        {this.state.customerEscalation.email && (
          <Input
            field={{
              label: "Email",
              type: "TEXT",
              isRequired: false,
              value: this.state.customerEscalation.email,
            }}
            width={6}
            name="firstName"
            onChange={this.handleChange}
            className="new__body__input"
            disabled={true}
          />
        )}

        {this.state.customerEscalation.role && (
          <Input
            field={{
              label: "Role",
              type: "TEXT",
              isRequired: false,
              value: this.state.customerEscalation.role,
            }}
            width={6}
            name="firstName"
            onChange={this.handleChange}
            className="new__body__input"
            disabled={true}
          />
        )}

        {this.state.customerEscalation.cell_phone && (
          <Input
            field={{
              label: "Cell phone",
              type: "TEXT",
              isRequired: false,
              value: this.state.customerEscalation.cell_phone,
            }}
            width={6}
            name="firstName"
            onChange={this.handleChange}
            className="new__body__input"
            disabled={true}
          />
        )}

        {this.state.customerEscalation.work_phone && (
          <Input
            field={{
              label: "Work phone",
              type: "TEXT",
              isRequired: false,
              value: this.state.customerEscalation.work_phone,
            }}
            width={6}
            name="firstName"
            onChange={this.handleChange}
            className="new__body__input"
            disabled={true}
          />
        )}
      </div>
    );
  };

  renderView = () => {
    const sites =
      this.props.sites &&
      this.props.sites.filter(
        (i) => i.site_id === this.state.customerEscalation.cw_site_id
      );

    return (
      <div className="add-escalation">
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              User
            </label>
          </div>
          <div>{this.state.customerEscalation.contact}</div>
        </div>
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          {this.state.isEditPopUp && <div>ss</div>}
          <div className="field__label row">
            <label className="field__label-label" title="">
              Escalation Type
            </label>
          </div>
          <div>{this.state.customerEscalation.escalation_type}</div>
        </div>

        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Escalation Priority
            </label>
          </div>
          <div>{this.state.customerEscalation.escalation_priority}</div>
        </div>
        <div
          className="select-type field-section
             col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Location
            </label>
          </div>
          <div>{sites && sites.length > 0 && sites[0].name}</div>
        </div>
        <div
          className="field-section
            col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Authorize Changes?
            </label>
          </div>
          <div>
            {this.state.customerEscalation.authorize_changes ? "Yes" : "No"}
          </div>
        </div>
        <div
          className="field-section
            col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Authorize Charges?
            </label>
          </div>
          <div>
            {this.state.customerEscalation.authorize_charges ? "Yes" : "No"}
          </div>
        </div>
        {this.state.customerEscalation.email && (
          <div
            className="field-section
            col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Email
              </label>
            </div>
            <div>{this.state.customerEscalation.email}</div>
          </div>
        )}

        {this.state.customerEscalation.role && (
          <div
            className="field-section
            col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Role
              </label>
            </div>
            <div>{this.state.customerEscalation.role}</div>
          </div>
        )}

        {this.state.customerEscalation.cell_phone && (
          <div
            className="field-section
            col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Cell phone
              </label>
            </div>
            <div>
              {phoneNumberInFormat(
                "",
                this.state.customerEscalation.cell_phone
              )}
            </div>
          </div>
        )}

        {this.state.customerEscalation.work_phone && (
          <div
            className="field-section
            col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Work phone
              </label>
            </div>
            <div>
              {phoneNumberInFormat(
                "",
                this.state.customerEscalation.work_phone
              )}
            </div>
          </div>
        )}
      </div>
    );
  };

  onSubmit = (e) => {
    if (this.isValid()) {
      if (!this.state.customerEscalation.id) {
        if (
          this.props.user &&
          this.props.user.type &&
          this.props.user.type === "provider"
        ) {
          this.props
            .addCustomerEscalation(this.props.customerId, {
              ...this.state.customerEscalation,
              user: this.state.customerEscalation.userId,
            })
            .then((action) => {
              if (action.type === ADD_ESCALATION_SUCCESS) {
                this.toggleNewModal();
                this.props.fetchCustomerescalation(this.props.customerId);
              }
              if (action.type === ADD_ESCALATION_FAILURE) {
                this.setValidationErrors(action.errorList.data);
              }
            });
        }

        if (
          this.props.user &&
          this.props.user.type &&
          this.props.user.type === "customer"
        ) {
          this.props
            .addCustomerEscalationCU({
              ...this.state.customerEscalation,
              user: this.state.customerEscalation.userId,
            })
            .then((action) => {
              if (action.type === ADD_ESCALATION_SUCCESS) {
                this.toggleNewModal();
                this.props.fetchCustomerescalationCU();
              }
              if (action.type === ADD_ESCALATION_FAILURE) {
                this.setValidationErrors(action.errorList.data);
              }
            });
        }
      } else {
        if (
          this.props.user &&
          this.props.user.type &&
          this.props.user.type === "provider"
        ) {
          this.props
            .editCustomerEscalation(
              this.props.customerId,
              {
                ...this.state.customerEscalation,
                user: this.state.customerEscalation.user.id,
              },
              this.state.customerEscalation.id
            )
            .then((action) => {
              if (action.type === ADD_ESCALATION_SUCCESS) {
                this.toggleNewModal();
                this.props.fetchCustomerescalation(this.props.customerId);
              }
              if (action.type === ADD_ESCALATION_FAILURE) {
                this.setValidationErrors(action.errorList.data);
              }
            });
        }
        if (
          this.props.user &&
          this.props.user.type &&
          this.props.user.type === "customer"
        ) {
          this.props
            .editCustomerEscalationCU(
              {
                ...this.state.customerEscalation,
                user: this.state.customerEscalation.user.id,
              },
              this.state.customerEscalation.id
            )
            .then((action) => {
              if (action.type === ADD_ESCALATION_SUCCESS) {
                this.toggleNewModal();
                this.props.fetchCustomerescalationCU();
              }
              if (action.type === ADD_ESCALATION_FAILURE) {
                this.setValidationErrors(action.errorList.data);
              }
            });
        }
      }
    }
  };

  validateForm = () => {
    const errorDistributionList = this.getEmptyState().errorDistributionList;

    let isValid = true;

    Object.keys(this.state.distributionList).map((key) => {
      this.state.distributionList[key].forEach((val) => {
        if (!AppValidators.isValidEmail(val)) {
          errorDistributionList[key].errorMessage = "Please enter valid email";
          errorDistributionList[key].errorState = "error";
          isValid = false;
        }
      });
    });

    this.setState((prevState) => ({
      errorDistributionList,
    }));

    return isValid;
  };

  saveNetworkNote = (e) => {
    if (this.validateForm()) {
      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "provider"
      ) {
        this.props
          .editDistributionList(
            this.props.customerId,
            this.state.distributionList
          )
          .then((action) => {
            if (action.type === ADD_NETWORK_NOTE_SUCCESS) {
              this.props.getNetworkNote(this.props.customerId);
              this.props.addSuccessMessage("Email updated successfully");
            }
          });
      }

      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "customer"
      ) {
        this.props
          .editDistributionListCU(this.state.distributionList)
          .then((action) => {
            if (action.type === ADD_NETWORK_NOTE_SUCCESS) {
              this.props.getNetworkNoteCU();
              this.props.addSuccessMessage("Email updated successfully");
            }
          });
      }
    }
  };
  setValidationErrors = (errorList) => {
    const newState: IContactsState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isFetching ? `loading` : ""}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.toggleNewModal}
        />
        <SquareButton
          content={`${this.state.isEditPopUp ? "Update" : "Add"}`}
          bsStyle={"primary"}
          disabled={this.props.isFetching}
          onClick={this.onSubmit}
        />
      </div>
    );
  };

  render() {
    const currentPage = this.state.currentPage;
    const columns: any = [
      {
        accessor: "escalation_priority",
        id: "escalation_priority",
        Header: "Escalation Priority",
        width: 150,
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "contact",
        Header: "Contact",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "cell_phone",
        Header: "Cell Phone",
        width: 120,
        sortable: true,
        Cell: (cell) => (
          <div>
            {" "}
            {`${phoneNumberInFormat(cell.original.country_code, cell.value)}`}
          </div>
        ),
      },
      {
        accessor: "email",
        Header: "Email",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "index",
        Header: "Action",
        width: 100,
        sortable: false,
        Cell: (cell) => (
          <div>
            <EditButton
              onClick={(e) => this.onEditRowClick(cell.original, e)}
            />
            {this.props.user &&
              this.props.user.type &&
              this.props.user.type === "provider" && (
                <DeleteButton
                  onClick={(e) => this.onDeleteRowClick(cell.original, e)}
                />
              )}
          </div>
        ),
      },
    ];

    return (
      <div
        className={`provider-documentation ${
          this.props.user &&
          this.props.user.type &&
          this.props.user.type === "provider" &&
          !this.props.customerId
            ? "provider-documentation-disable"
            : ""
        }`}
      >
        <div className="documentation">
          <div className="loader">
            <Spinner show={this.props.isFetching} />
          </div>
          {this.renderTopBar()}
          {currentPage.pageType === PageType.Escalations && (
            <div className="documentation-container">
              <SquareButton
                onClick={() => this.toggleNewModal()}
                content="Add New Customer Escalation "
                bsStyle={"primary"}
                className="add-new-escalation"
              />

              <ModalBase
                show={this.state.openModal}
                onClose={this.toggleNewModal}
                titleElement={`${
                  this.state.isEditPopUp ? "Update" : "Add New"
                } Customer Escalation`}
                bodyElement={this.state.openModal && this.renderAdd()}
                footerElement={this.renderFooter()}
                className="add-customer-note"
              />

              <ModalBase
                show={this.state.openViewModal}
                onClose={this.toggleViewModal}
                titleElement={`View Customer Escalation`}
                bodyElement={this.state.openViewModal && this.renderView()}
                footerElement={
                  <SquareButton
                    content="Close"
                    bsStyle={"default"}
                    onClick={this.toggleViewModal}
                  />
                }
                className="view-contacts"
              />
              <div className="notes-list">
                <Table
                  columns={columns}
                  rows={this.state.rows.filter(
                    (row) => row.escalation_type === "During business hours"
                  )}
                  customTopBar={
                    <h4 className="table-heading">During Business Hours</h4>
                  }
                  className={""}
                  defaultSorted={[
                    {
                      id: "escalation_priority",
                      desc: false,
                    },
                  ]}
                  onRowClick={this.onViewRowClick}
                  loading={this.props.isFetching}
                />
              </div>
              <div className="notes-list outside-escalation">
                <Table
                  columns={columns}
                  rows={this.state.rows.filter(
                    (row) => row.escalation_type === "Outside of business hours"
                  )}
                  customTopBar={
                    <h4 className="table-heading">
                      Outside Normal Business Hours
                    </h4>
                  }
                  defaultSorted={[
                    {
                      id: "escalation_priority",
                      desc: false,
                    },
                  ]}
                  className={""}
                  onRowClick={this.onViewRowClick}
                  loading={this.props.isFetching}
                />
              </div>
            </div>
          )}
          {currentPage.pageType === PageType.distribution && (
            <div className="distribution-list">
              <div
                className="email-tag field-section
             col-md-9 col-xs-9"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Alerts and Notifications
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.errorDistributionList.alerts_and_notifications
                      .errorMessage
                      ? `error-input`
                      : ""
                  }`}
                >
                  {/* <TagsInput
                    value={this.state.distributionList.alerts_and_notifications}
                    onChange={(e) =>
                      this.handleChangeDistributionList(
                        e,
                        "alerts_and_notifications"
                      )
                    }
                    inputProps={{
                      className: "react-tagsinput-input",
                      placeholder: "Enter Email",
                    }}
                    addOnBlur={true}
                  /> */}
                </div>
                {this.state.errorDistributionList.alerts_and_notifications
                  .errorMessage && (
                  <div className="multiple-email-eror">
                    {
                      this.state.errorDistributionList.alerts_and_notifications
                        .errorMessage
                    }
                  </div>
                )}
              </div>
              <div
                className="email-tag field-section
             col-md-9 col-xs-9"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Monitoring Alerts
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.errorDistributionList.monitoring_alerts
                      .errorMessage
                      ? `error-input`
                      : ""
                  }`}
                >
                  {/* <TagsInput
                    value={this.state.distributionList.monitoring_alerts}
                    onChange={(e) =>
                      this.handleChangeDistributionList(e, "monitoring_alerts")
                    }
                    inputProps={{
                      className: "react-tagsinput-input",
                      placeholder: "Enter Email",
                    }}
                    addOnBlur={true}
                  /> */}
                </div>
                {this.state.errorDistributionList.monitoring_alerts
                  .errorMessage && (
                  <div className="multiple-email-eror">
                    {
                      this.state.errorDistributionList.monitoring_alerts
                        .errorMessage
                    }
                  </div>
                )}
              </div>

              <div
                className="email-tag field-section
             col-md-9 col-xs-9"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Scheduled Reports
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.errorDistributionList.scheduled_reports
                      .errorMessage
                      ? `error-input`
                      : ""
                  }`}
                >
                  {/* <TagsInput
                    value={this.state.distributionList.scheduled_reports}
                    onChange={(e) =>
                      this.handleChangeDistributionList(e, "scheduled_reports")
                    }
                    inputProps={{
                      className: "react-tagsinput-input",
                      placeholder: "Enter Email",
                    }}
                    addOnBlur={true}
                  /> */}
                </div>
                {this.state.errorDistributionList.scheduled_reports
                  .errorMessage && (
                  <div className="multiple-email-eror">
                    {
                      this.state.errorDistributionList.scheduled_reports
                        .errorMessage
                    }
                  </div>
                )}
              </div>

              <div
                className="email-tag field-section
             col-md-9 col-xs-9"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Staffing Changes
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.errorDistributionList.staffing_changes
                      .errorMessage
                      ? `error-input`
                      : ""
                  }`}
                >
                  {/* <TagsInput
                    value={this.state.distributionList.staffing_changes}
                    onChange={(e) =>
                      this.handleChangeDistributionList(e, "staffing_changes")
                    }
                    inputProps={{
                      className: "react-tagsinput-input",
                      placeholder: "Enter Email",
                    }}
                    addOnBlur={true}
                  /> */}
                </div>
                {this.state.errorDistributionList.staffing_changes
                  .errorMessage && (
                  <div className="multiple-email-eror">
                    {
                      this.state.errorDistributionList.staffing_changes
                        .errorMessage
                    }
                  </div>
                )}
              </div>
              <SquareButton
                content="Save"
                bsStyle={"primary"}
                onClick={this.saveNetworkNote}
                className="distribution-list__save"
                disabled={
                  this.state.distributionList === this.props.distributionList
                }
              />
            </div>
          )}

          <ConfirmBox
            show={this.state.isopenConfirm}
            onClose={this.toggleConfirmOpen}
            onSubmit={this.onClickConfirm}
            isLoading={this.props.isFetching}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customerEscalations: state.documentation.customerEscalations,
  networkNote: state.documentation.networkNote,
  isFetching: state.documentation.isFetching,
  customerId: state.customer.customerId,
  distributionList: state.documentation.distributionList,
  escalationFields: state.documentation.escalationFields,
  users: state.documentation.users,
  sites: state.inventory.sites,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchNoteTypes: () => dispatch(fetchNoteTypes()),
  fetchEscalationFields: () => dispatch(fetchEscalationFields()),
  fetchCustomerescalation: (customerId: number) =>
    dispatch(fetchCustomerEscalation(customerId)),
  addCustomerEscalation: (
    customerId: number,
    customerEscalation: ICustomerEscalation
  ) => dispatch(addCustomerEscalation(customerId, customerEscalation)),
  editCustomerEscalation: (
    customerId: number,
    customerEscalation: ICustomerEscalation,
    id: number
  ) => dispatch(editCustomerEscalation(customerId, customerEscalation, id)),
  getNetworkNote: (customerId: number) => dispatch(getNetworkNote(customerId)),
  editDistributionList: (customerId: number, networkNote: string) =>
    dispatch(editDistributionList(customerId, networkNote)),
  fetchAllCustomerUsers: (customerId: any) =>
    dispatch(fetchAllCustomerUsers(customerId)),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  deleteEscalation: (customerId: any, id: number) =>
    dispatch(deleteCustomerEscalation(customerId, id)),

  fetchCustomerescalationCU: () => dispatch(fetchCustomerEscalationCU()),
  addCustomerEscalationCU: (customerEscalation: ICustomerEscalation) =>
    dispatch(addCustomerEscalationCU(customerEscalation)),
  editCustomerEscalationCU: (
    customerEscalation: ICustomerEscalation,
    id: number
  ) => dispatch(editCustomerEscalationCU(customerEscalation, id)),
  getNetworkNoteCU: () => dispatch(getNetworkNoteCU()),
  editDistributionListCU: (networkNote: string) =>
    dispatch(editDistributionListCU(networkNote)),
  fetchSitesCustomersUser: () => dispatch(fetchSitesCustomersUser()),
  deleteEscalationCU: (id: number) => dispatch(deleteCustomerEscalationCU(id)),
  fetchAllCustomerUsersCU: () => dispatch(fetchAllCustomerUsersCU()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
