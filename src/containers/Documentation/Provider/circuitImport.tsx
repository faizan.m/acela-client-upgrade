import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";

import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import { importCircuits } from "../../../actions/documentation";
import {
  addSite,
  BATCH_CREATE_SUCCESS,
  fetchCountriesAndStates,
  fetchDateFormat,
  fetchSites,
  fetchStatuses,
  fetchTaskStatus,
  UPLOAD_DEVICE_FILE_SUCCESS,
  uploadDeviceFile,
} from "../../../actions/inventory";
import { fetchSubscriptions } from "../../../actions/subscription";
import BackButton from "../../../components/BackLink";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import SelectInput from "../../../components/Input/Select/select";
import Spinner from "../../../components/Spinner";
import "./style.scss";

const fields: any[] = [
  {
    count: 0,
    label: "Circuit ID",
    value: "circuit_id",
    isRequired: true,
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 1,
    label: "Provider",
    value: "provider",
    isRequired: true,
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 2,
    label: "IP Address Space",
    value: "ip_address",
    isDate: false,
    isRequired: false,
    isCheckBox: false,
  },
  {
    count: 3,
    label: "Provider Phone",
    value: "contact_phone",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
  },

  {
    count: 4,
    label: "Provider Email",
    value: "email",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 5,
    label: "Bandwidth",
    value: "bandwidth",
    isRequired: false,
    message: "",
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 6,
    label: "Bandwidth Measurement",
    value: "bandwidth_measurement",
    message: "",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
  },
  {
    count: 7,
    label: "Account Number",
    value: "account_number",
    isRequired: false,
    isDate: false,
  },
  {
    count: 8,
    label: "Notes",
    value: "note",
    isRequired: false,
    isDate: false,
  },
  {
    count: 9,
    label: "Site",
    value: "cw_site_id",
    isRequired: true,
    isDate: false,
  },
  {
    count: 10,
    label: "Circuit Type",
    value: "circuit_type",
    isRequired: true,
    isDate: false,
    isCheckBox: false,
  },
];

interface ICircuitMappingProps extends ICommonProps {
  xlsxData: any;
  sites: ISite[];
  fetchSites: (customerId: number) => any;
  customerId: number;
  addSite: (customerId: number, newSite: any) => any;
  importCircuits: (customerId: number, deviceInfo: any) => any;
  deviceSuccess: ICircuitCreateRes;
  customersShort: ICustomerShort[];
  countries: any;
  fetchCountriesAndStates: any;
  isPostingBatch: boolean;
  fetchSubscriptions: any;
  fetchDateFormat: any;
  dateFormats: any;
  xlsxFile: any;
  uploadDeviceFile: (fileReq: any, name: string) => Promise<any>;
  addErrorMessage: any;
  fetchTaskStatus: any;
  addSuccessMessage: any;
  fetchStatuses: any;
  statusList: any[];
  circuitTypes: any[];
}
interface ICircuitMappingState {
  searchQueryField: any;
  searchQueryAddress: any;
  checkboxList: any;
  rows: any[];
  isAddressMappingOpen: boolean;
  xlsxData: any;
  columns: any[];
  deviceList: any;
  addressList: any[];
  deviceTypes: any[];
  statusList: any[];
  statusMapped: any[];
  statusErrors: any[];
  statusInclude: any[];
  minStatusError: string;
  sites: any[];
  customerId?: number;
  isCreateSiteModal: boolean;
  deviceSuccess: ICircuitCreateRes;
  isPostingSite: boolean;
  isopenConfirm: boolean;
  errors: string[];
  manufacturer_id: number;
  error: {
    manufacturer_id: IFieldValidation;
  };
  isSuccess: boolean;
  dateFormat: any;
  isopenFilterPrototype: boolean;
  taskId: any;
  showSiteMappingError: boolean;
  uploading: boolean;
}

class CircuitMapping extends React.Component<
  ICircuitMappingProps,
  ICircuitMappingState
> {
  constructor(props: ICircuitMappingProps) {
    super(props);

    this.state = {
      rows: [],
      searchQueryField: [],
      searchQueryAddress: [],
      checkboxList: [],
      dateFormat: [],
      isAddressMappingOpen: false,
      isCreateSiteModal: false,
      xlsxData: null,
      columns: [],
      deviceList: [],
      statusList: [],
      statusMapped: [],
      statusInclude: [],
      addressList: [],
      deviceTypes: [],
      sites: [],
      deviceSuccess: {
        total_requested_item_count: 0,
        failed_item_count: 0,
        created_item_count: 0,
        updated_count: 0,
        error_file: {
          file_name: "",
          file_path: "",
        },
      },
      isPostingSite: false,
      isopenConfirm: false,
      isopenFilterPrototype: false,
      manufacturer_id: null,
      errors: ["", "", "", "", "", "", "", "", "", ""],
      error: {
        manufacturer_id: {
          errorState: "success",
          errorMessage: "",
        },
      },
      isSuccess: false,
      taskId: null,
      showSiteMappingError: false,
      statusErrors: [],
      minStatusError: "",
      uploading: false,
    };
  }

  componentDidMount() {
    this.props.fetchCountriesAndStates();
    this.props.fetchDateFormat();
    if (this.props.statusList.length === 0) {
      this.props.fetchStatuses();
    }

    if (!this.props.xlsxFile) {
      this.props.history.push("/documentation/circuits");
    }
    if (this.props.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
  }

  componentDidUpdate(prevProps: ICircuitMappingProps) {
    const { deviceSuccess } = this.props;
    if (deviceSuccess !== prevProps.deviceSuccess) {
      this.setState({ deviceSuccess });
    }
  }

  setColumns = (nextProps: ICircuitMappingProps) => {
    const xlsxDataResponce = nextProps.xlsxData;
    const columns = [];
    let index = 0;
    let tempKey = "";
    for (const key in xlsxDataResponce[0]) {
      if (xlsxDataResponce[0].hasOwnProperty(key)) {
        if (key.includes("EMPTY")) {
          tempKey = `column_${index + 1}`;
        }
        columns.push({ key: tempKey !== "" ? tempKey : key, value: key });
        index++;
        tempKey = "";
      }
    }
    this.setState({ columns });
  };

  setaddressList = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const addressList = [];
    xlsxDataResponce.map((data) => {
      addressList.push(data[this.state.searchQueryField[9]]);
    });
    const onlyUniqueCustomer = addressList.filter(this.onlyUnique);
    if (
      onlyUniqueCustomer &&
      onlyUniqueCustomer.length > 0 &&
      onlyUniqueCustomer[onlyUniqueCustomer.length - 1].trim() === ""
    ) {
      onlyUniqueCustomer.pop();
    }
    const searchQueryAddress = [];
    onlyUniqueCustomer.map((c, index) => {
      const isPresent = this.props.sites.filter(
        (customer) =>
          customer.name.toUpperCase().trim() === c.toUpperCase().trim()
      );
      if (isPresent.length > 0) {
        searchQueryAddress[index] = c;
      }
    });

    this.setState({
      addressList: onlyUniqueCustomer,
      searchQueryAddress,
    });
  };

  setStatusList = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const statusList = [];
    xlsxDataResponce.map((data) => {
      statusList.push(data[this.state.searchQueryField[10]]);
    });
    const onlyUniquestatus = statusList.filter(this.onlyUnique);
    if (
      onlyUniquestatus &&
      onlyUniquestatus.length > 0 &&
      onlyUniquestatus[onlyUniquestatus.length - 1].trim() === ""
    ) {
      onlyUniquestatus.pop();
    }
    const statusMapped = [];
    const statusInclude = [];
    onlyUniquestatus.map((c, index) => {
      const isPresent = this.props.circuitTypes.filter(
        (customer) => customer.name === c.trim()
      );
      if (isPresent.length > 0) {
        statusMapped[index] = c.trim();
      }
      statusInclude[index] = true;
    });

    this.setState({
      statusList: onlyUniquestatus,
      statusInclude,
      statusMapped,
    });
  };

  onlyUnique = (value, index, self) => {
    return value && self.indexOf(value) === index;
  };

  toggleAddressMapping = () => {
    this.setState((prevState) => ({
      isAddressMappingOpen: !prevState.isAddressMappingOpen,
    }));
  };

  getFieldsOptions = () => {
    if (this.state.columns.length > 0) {
      return this.state.columns.map((role) => ({
        value: role.value,
        label: role.key,
      }));
    } else {
      return [];
    }
  };

  getDateFormatOptions = () => {
    const formats = [];
    if (this.props.dateFormats) {
      Object.keys(this.props.dateFormats).map((key) => {
        formats.push({
          value: this.props.dateFormats[key],
          label: key,
        });
      });

      return formats;
    } else {
      return [];
    }
  };

  getStatusOptions = () => {
    if (this.props.circuitTypes && this.props.circuitTypes.length > 0) {
      return this.props.circuitTypes.map((site) => ({
        value: site.name,
        label: site.name,
      }));
    } else {
      return [];
    }
  };

  onClickCancel = () => {
    this.props.history.push("/documentation/circuits");
  };

  handleChangeField = (event: any, index?: any) => {
    const searchQueryField = this.state.searchQueryField;
    searchQueryField[index] = event.target.value;

    this.setState({
      searchQueryField,
    });
    if (event.target.name === "circuit_type") {
      this.setStatusList();
    }
  };

  handleChangeDateFormat = (event: any, index?: any) => {
    const dateFormat = this.state.dateFormat;
    dateFormat[index] = event.target.value;

    this.setState({
      dateFormat,
    });
  };

  handleChangeStatus = (event: any, index?: any) => {
    const statusMapped = this.state.statusMapped;
    statusMapped[index] = event.target.value;

    this.setState({
      statusMapped,
    });
  };

  onClickImport = (event: any, index?: any) => {
    if (this.validateForm()) {
      this.setaddressList();
      this.toggleAddressMapping();
    }
  };

  validateForm = () => {
    let isValid = true;
    const newState = cloneDeep(this.state);
    fields.map((field, index) => {
      newState.errors[index] = "";
    });
    (newState.minStatusError as string) = "";
    fields.map((field, index) => {
      if (field.isRequired && !this.state.searchQueryField[index]) {
        newState.errors[index] = `${field.label} is required`;
        isValid = false;
      }
    });
    if (newState.searchQueryField[10]) {
      newState.statusList.map((field, index) => {
        newState.statusErrors[index] = ``;
      });
      newState.statusList.map((field, index) => {
        if (!this.state.statusMapped[index]) {
          newState.statusErrors[index] = `Required`;
          isValid = false;
        }
      });
    }
    if (this.state.searchQueryField[5] && !this.state.searchQueryField[6]) {
      newState.errors[6] = `Bandwidth Measurement	 is required`;
      isValid = false;
    }
    this.setState(newState);

    return isValid;
  };

  validateCustomerMapping = () => {
    let isValid = true;
    this.setState({ showSiteMappingError: false });

    for (const value of this.state.searchQueryAddress) {
      if (!value) {
        isValid = false;
        this.setState({ showSiteMappingError: true });
      }
    }
    if (this.state.searchQueryAddress.length === 0) {
      this.setState({ showSiteMappingError: true });
      isValid = false;
    }

    return isValid;
  };

  getCircuitTypeId = (name: string) => {
    const customerData = this.props.circuitTypes.filter(
      (customer) =>
        customer.name.toUpperCase().trim() === name.toUpperCase().trim()
    );

    return customerData[0].id;
  };

  onClickImportSave = (event: any, index?: any) => {
    const addressSiteObject = {};

    if (this.validateCustomerMapping()) {
      this.setState({ uploading: true, showSiteMappingError: false });

      this.state.searchQueryAddress.map((name, i) => {
        addressSiteObject[this.state.addressList[i]] = name;
      });
      const fieldMappings = {};

      fields.map((field, j) => {
        fieldMappings[field.value] = this.state.searchQueryField[j];
        if (this.state.searchQueryField[j] && this.state.dateFormat[j]) {
          fieldMappings[`${field.value}_format`] = this.state.dateFormat[j];
        }
      });
      const statusMappings = {};
      this.state.statusInclude.map((is, j) => {
        if (is) {
          statusMappings[this.state.statusList[j]] = this.getCircuitTypeId(
            this.state.statusMapped[j]
          );
        }
      });
      const deviceInfo = {
        file_path: this.props.xlsxFile && this.props.xlsxFile.name,
        field_mappings: fieldMappings,
        site_mapping: addressSiteObject ? addressSiteObject : null,
        circuit_type_mapping: statusMappings ? statusMappings : null,
      };

      if (this.props.xlsxFile) {
        const data = new FormData();
        data.append("filename", this.props.xlsxFile);
        this.props
          .uploadDeviceFile(
            data,
            this.props.xlsxFile && this.props.xlsxFile.name
          )
          .then((action) => {
            if (action.type === UPLOAD_DEVICE_FILE_SUCCESS) {
              deviceInfo.file_path = action.response;
              this.props
                .importCircuits(this.props.customerId, deviceInfo)
                .then((a) => {
                  if (a.type === BATCH_CREATE_SUCCESS) {
                    if (a.response) {
                      this.setState({
                        isSuccess: true,
                        deviceSuccess: a.response,
                      });
                    }
                    this.props.addSuccessMessage("Bulk import completed.");
                  }
                });
            } else {
              this.setState({ uploading: false });
            }
          });
      }
    }
  };

  getFieldMapping = () => {
    return (
      <div className="field-mapping  subscription-mapping-import">
        <h3>Circuit Field Mapping</h3>

        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-row" />
            <tr className="field-mapping__table-header">
              <th className="first">Map Field</th>
              <th className="middle" />
              <th className="last"> Sheet Columns </th>
              <th className="" />
            </tr>

            {fields.map((field, index) => (
              <tr key={index} className="field-mapping__table-row">
                {field.isCheckBox && (
                  <td className="first">
                    <Checkbox
                      isChecked={this.state.checkboxList[index]}
                      name="option"
                      onChange={(e) => this.onChecboxChanged(e, index)}
                    >
                      {field.header}
                    </Checkbox>
                  </td>
                )}

                <td className="first">
                  {((field.isCheckBox && this.state.checkboxList[index]) ||
                    !field.isCheckBox) &&
                    field.label}

                  {field.message ? (
                    <div className="field-mapping__label-message">
                      {field.message}
                    </div>
                  ) : null}
                  {field.isRequired && (
                    <span className="field-mapping__label-required" />
                  )}
                  {field.isCheckBox && this.state.checkboxList[index] && (
                    <img
                      className="opposite-arrow"
                      src="/assets/new-icons/oppositearrow.svg"
                    />
                  )}
                  {field.message ? (
                    <div className="field-mapping__label-message">
                      {field.message}
                    </div>
                  ) : null}
                  {this.state.errors[index] ? (
                    <span className="field-mapping__label-error">
                      {this.state.errors[index]}
                    </span>
                  ) : null}
                </td>
                {!field.isCheckBox && (
                  <td className="middle">
                    <img src="/assets/icons/oppositearrow.svg" />
                  </td>
                )}
                {((field.isCheckBox && this.state.checkboxList[index]) ||
                  !field.isCheckBox) && (
                  <td className="sheet-column">
                    <SelectInput
                      name={field.value}
                      value={this.state.searchQueryField[index]}
                      onChange={(event) => this.handleChangeField(event, index)}
                      options={this.getFieldsOptions()}
                      searchable={true}
                      placeholder="Select field"
                      clearable={false}
                    />
                  </td>
                )}
                {field.isDate && (
                  <td className="last">
                    <SelectInput
                      name="select"
                      value={this.state.dateFormat[index]}
                      onChange={(event) =>
                        this.handleChangeDateFormat(event, index)
                      }
                      options={this.getDateFormatOptions()}
                      searchable={true}
                      placeholder="Select Date Format"
                      clearable={false}
                      disabled={!this.state.searchQueryField[index]}
                    />
                  </td>
                )}

                <th className="" />
                <th className="" />
                <th className="" />
              </tr>
            ))}
          </tbody>
        </table>
        {this.state.statusList && this.state.statusList.length > 0 && (
          <table className="field-mapping__table">
            <tbody>
              <tr className="field-mapping__table-row" />
              <tr className="field-mapping__table-header">
                <th className="first">Circuit Types from Sheet</th>
                <th className="middle" />
                <th className="last">Circuit Types to Map</th>
                <th className="" />
              </tr>

              {this.state.statusList &&
                this.state.statusList.map((field, index) => (
                  <tr key={index} className="field-mapping__table-row">
                    <td className="first">
                      {field}
                      {this.state.statusErrors[index] ? (
                        <span className="field-mapping__label-error">
                          {this.state.statusErrors[index]}
                        </span>
                      ) : null}
                    </td>
                    <td className="middle">
                      <img src="/assets/new-icons/oppositearrow.svg" />
                    </td>
                    <td className="last">
                      {
                        <SelectInput
                          name="select"
                          value={this.state.statusMapped[index]}
                          onChange={(event) =>
                            this.handleChangeStatus(event, index)
                          }
                          options={this.getStatusOptions()}
                          searchable={true}
                          placeholder="Select Status"
                          clearable={false}
                          disabled={!this.state.statusInclude[index]}
                        />
                      }
                    </td>
                    <th className="" />
                  </tr>
                ))}
            </tbody>
          </table>
        )}
        {this.state.statusList && this.state.minStatusError && (
          <div className="error-customer_mapping">
            {this.state.minStatusError}
          </div>
        )}
        <div className="field-mapping__footer">
          <SquareButton
            onClick={this.onClickCancel}
            content="Cancel"
            bsStyle={"default"}
            className="import-button-1"
          />
          <SquareButton
            onClick={this.onClickImport}
            content="Next"
            bsStyle={"primary"}
            className="import-button-1"
          />
        </div>
      </div>
    );
  };

  onChecboxChanged = (event: any, index?: any) => {
    const checkboxList = this.state.checkboxList;
    const searchQueryField = this.state.searchQueryField;
    let deviceTypes = this.state.deviceTypes;
    if (index === 9) {
      deviceTypes = [];
    }
    if (index === 8) {
      deviceTypes = deviceTypes.map((item, i) => {
        if (item.include) {
          item.ignore_zero_dollar_items = event.target.checked;
        }

        return item;
      });
    }
    checkboxList[index] = event.target.checked;

    searchQueryField[index] = "";
    const errors = this.state.errors;
    errors[8] = "";
    errors[9] = "";
    this.setState({
      checkboxList,
      searchQueryField,
      errors,
      deviceTypes,
    });
  };

  toggleConfirmOpen = () => {
    this.setState((prevState) => ({
      isopenConfirm: !prevState.isopenConfirm,
    }));
  };

  getSitesOptions = () => {
    if (this.props.sites && this.props.sites.length > 0) {
      return this.props.sites.map((site) => ({
        value: site.site_id,
        label: site.name,
      }));
    } else {
      return [];
    }
  };
  handleChangeAddress = (event: any, index?: any) => {
    const searchQueryAddress = this.state.searchQueryAddress;
    searchQueryAddress[index] = event.target.value;

    this.setState({
      searchQueryAddress,
    });
  };

  getAddressMapping = () => {
    return (
      <div className="field-mapping customer-mapping">
        {this.state.uploading ? "" : <h3>Address Mapping</h3>}
        {this.props.isPostingBatch || this.state.uploading ? (
          <Spinner show={true} />
        ) : (
          <div>
            <table className="field-mapping__table">
              <tbody>
                <tr className="field-mapping__table-row" />
                <tr className="field-mapping__table-header">
                  <th className="first">Address</th>
                  <th className="middle" />
                  <th className="last">Site</th>
                  <th className="" />
                </tr>

                {this.state.addressList &&
                  this.state.addressList.map((field, index) => (
                    <tr key={index} className="field-mapping__table-row">
                      <td className="first">{field}</td>
                      <td className="middle">
                        <img src="/assets/new-icons/oppositearrow.svg" />
                      </td>
                      <td className="last">
                        {
                          <SelectInput
                            name="select"
                            value={this.state.searchQueryAddress[index]}
                            onChange={(event) =>
                              this.handleChangeAddress(event, index)
                            }
                            options={this.getSitesOptions()}
                            searchable={true}
                            placeholder="Select field"
                            clearable={false}
                          />
                        }
                      </td>
                      <th className="" />
                    </tr>
                  ))}
              </tbody>
            </table>
            {this.state.showSiteMappingError && (
              <div className="error-customer_mapping">
                Please select all sites
              </div>
            )}
            <div className="field-mapping__footer">
              <SquareButton
                onClick={this.toggleAddressMapping}
                content="Back"
                bsStyle={"default"}
                className="import-button-1"
              />
              <SquareButton
                onClick={this.onClickImportSave}
                content="Import"
                bsStyle={"primary"}
                className="import-button-1"
              />
            </div>
          </div>
        )}
      </div>
    );
  };
  downloadReport = (response) => {
    if (response && response !== "") {
      const url = response.file_path;
      const link = document.createElement("a");
      link.href = url;
      link.target = "_blank";
      link.setAttribute("download", response.file_name);
      document.body.appendChild(link);
      link.click();
    }
  };
  render() {
    return (
      <div className="mapping">
        {!this.state.isAddressMappingOpen && this.getFieldMapping()}
        {this.state.isAddressMappingOpen &&
          !this.props.isPostingBatch &&
          this.state.deviceSuccess.total_requested_item_count === 0 &&
          !this.state.isSuccess &&
          this.getAddressMapping()}
        {this.props.isPostingBatch ? <Spinner show={true} /> : null}
        {(this.state.isSuccess ||
          (this.state.deviceSuccess &&
            this.state.deviceSuccess.total_requested_item_count > 0) ||
          this.state.deviceSuccess.failed_item_count !== 0 ||
          this.state.deviceSuccess.created_item_count !== 0 ||
          this.state.deviceSuccess.updated_count !== 0) && (
          <div>
            <BackButton
              path="/documentation/circuits"
              name="Back to Circuit Listing"
            />
            <div className="device-responce">
              <h3>Status :</h3>
              <div className="row ">
                Total circuit :
                <span>
                  {" "}
                  {this.state.deviceSuccess.total_requested_item_count || 0}
                </span>
              </div>
              <div className="row success">
                Circuit created :
                <span>{this.state.deviceSuccess.created_item_count || 0}</span>
              </div>
              <div className="row success">
                Updated circuit :
                <span>{this.state.deviceSuccess.updated_count || 0}</span>
              </div>
              <div className="row error">
                Failed to create circuit :
                <span>{this.state.deviceSuccess.failed_item_count || 0}</span>
              </div>
              {this.state.deviceSuccess.error_file &&
                this.state.deviceSuccess.error_file.file_path && (
                  <div className="row error warning-list">
                    Download error report :{" "}
                    <img
                      onClick={(e) =>
                        this.downloadReport(this.state.deviceSuccess.error_file)
                      }
                      alt=""
                      className="import-error-btn"
                      src="/assets/icons/download.png"
                    />
                  </div>
                )}
            </div>
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  xlsxData: state.inventory.xlsxData,
  sites: state.inventory.sites,
  customerId: state.customer.customerId,
  deviceSuccess: state.inventory.deviceSuccess,
  customersShort: state.customer.customersShort,
  countries: state.inventory.countries,
  isPostingBatch: state.inventory.isPostingBatch,
  dateFormats: state.inventory.dateFormats,
  xlsxFile: state.inventory.xlsxFile,
  statusList: state.inventory.statusList,
  circuitTypes: state.documentation.circuitTypes,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  addSite: (customerId: number, newSite: any) =>
    dispatch(addSite(customerId, newSite)),
  importCircuits: (customerId: number, devices: any[]) =>
    dispatch(importCircuits(customerId, devices)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
  fetchSubscriptions: (id: number, show: boolean) =>
    dispatch(fetchSubscriptions(id)),
  fetchDateFormat: () => dispatch(fetchDateFormat()),
  uploadDeviceFile: (fileReq: any, name: string) =>
    dispatch(uploadDeviceFile(fileReq, name)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  fetchStatuses: () => dispatch(fetchStatuses()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CircuitMapping);
