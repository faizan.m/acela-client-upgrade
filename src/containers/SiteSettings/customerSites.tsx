import React, { useCallback, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  customSiteCRUD,
  CUSTOM_SITE_SUCCESS,
  CUSTOM_SITE_FAILURE,
} from "../../actions/inventory";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import CreateSite from "../CreateNew/createSite";
import EditButton from "../../components/Button/editButton";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import InfiniteListing from "../../components/InfiniteList/infiniteList";
import Spinner from "../../components/Spinner";

interface CustomerSitesProps {
  customers: ICustomerShort[];
  isFetchingCustomers: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  customSiteCRUD: (
    method: HTTPMethods,
    customerId: number,
    data?: ISite
  ) => Promise<any>;
}

const CustomerSites: React.FC<CustomerSitesProps> = (props) => {
  const [editSite, setEditSite] = useState<ISite>();
  const [loading, setLoading] = useState<boolean>(false);
  const [customer, setCustomer] = useState<number>(null);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [refreshKey, setRefreshKey] = useState<number>(Math.random());

  const customerOptions: IPickListOptions[] = useMemo(
    () =>
      props.customers
        ? props.customers.map((customer) => ({
            value: customer.id,
            label: customer.name,
          }))
        : [],
    []
  );

  const handleAddSite = (saved: boolean = false) => {
    if (saved === true) {
      props.addSuccessMessage("Custom site created successfully!");
      setRefreshKey(Math.random());
    }
    setShowModal(false);
    setEditSite(undefined);
  };

  const handleCustomerSiteUpdate = (site: ISite) => {
    setLoading(true);
    setShowModal(false);
    setEditSite(undefined);
    props
      .customSiteCRUD("put", customer, site)
      .then((action) => {
        if (action.type === CUSTOM_SITE_SUCCESS) {
          setRefreshKey(Math.random());
          props.addSuccessMessage("Custom site updated successfully!");
        } else if (action.type === CUSTOM_SITE_FAILURE) {
          props.addErrorMessage("Error updating sustom site!");
        }
      })
      .finally(() => setLoading(false));
  };

  const onClickEdit = (site: ISite) => {
    setEditSite({
      ...site,
      state_id: site.state_crm_id,
      country_id: site.country_crm_id,
    });
    setShowModal(true);
  };

  const onClickDelete = (site: ISite) => {
    setLoading(true);
    props
      .customSiteCRUD("delete", customer, site)
      .then((action) => {
        if (action.type === CUSTOM_SITE_SUCCESS) {
          setRefreshKey(Math.random());
          props.addSuccessMessage("Custom site deleted successfully!");
        } else if (action.type === CUSTOM_SITE_FAILURE) {
          props.addErrorMessage("Error deleting custom site!");
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChangeCustomer = (event: { target: { value: number } }) => {
    const customerId: number = Number(event.target.value);
    setCustomer(customerId);
  };

  const renderSelectCustomer = useCallback(() => {
    return (
      <div className="add-customer-block">
        <Input
          field={{
            label: "Customer",
            type: "PICKLIST",
            value: customer,
            options: customerOptions,
            isRequired: false,
          }}
          width={6}
          multi={false}
          name="customer"
          labelIcon="info"
          labelTitle={"Please select a customer to modify its partner sites"}
          onChange={(e) => handleChangeCustomer(e)}
          placeholder={`Select Customer`}
        />
      </div>
    );
  }, [customer, customerOptions]);

  const columns: IColumnInfinite<ISite>[] = [
    {
      id: "name",
      name: "Site",
      ordering: "name",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.name}</div>
      ),
    },
    {
      id: "business_unit",
      name: "Business Unit",
      ordering: "business_unit",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">
          {row.business_unit ? row.business_unit : "-"}
        </div>
      ),
    },
    {
      id: "address_line_1",
      name: "Address",
      ordering: "address_line_1",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.address_line_1}</div>
      ),
    },
    {
      id: "city",
      name: "City",
      ordering: "city",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.city ? row.city : "-"}</div>
      ),
    },
    {
      id: "state",
      name: "State",
      ordering: "state",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.state ? row.state : "-"}</div>
      ),
    },
    {
      id: "is_custom_site",
      name: "Custom Site",
      ordering: "is_custom_site",
      className: "icon-col",
      Cell: (row: ISite) =>
        row.is_custom_site && (
          <img
            className="custom-site-icon"
            alt="Custom Site Check"
            src="/assets/icons/active-right.svg"
          />
        ),
    },
    {
      name: "",
      className: "configured-sites-actions",
      Cell: (row: ISite) =>
        !loading &&
        row.is_custom_site && (
          <>
            <EditButton title="Edit Site" onClick={() => onClickEdit(row)} />
            <SmallConfirmationBox
              className="remove"
              showButton={true}
              onClickOk={() => onClickDelete(row)}
              text={"Site"}
              title="Remove Site"
            />
          </>
        ),
    },
  ];

  return (
    <>
      {renderSelectCustomer()}
      <div className="sites-listing-container">
        <Spinner show={loading} className="sites-list-loader" />
        {customer ? (
          <>
            <SquareButton
              onClick={() => setShowModal(true)}
              title={
                Boolean(customer)
                  ? "Create new site for the selected customer"
                  : "Please select a customer to create site"
              }
              content={"Create Custom Site"}
              bsStyle={"primary"}
              className="site-setting-add-site"
              disabled={!Boolean(customer)}
            />
            <InfiniteListing
              showSearch={true}
              refreshKey={refreshKey}
              url={`providers/fire-report/customers/${customer}/custom-sites?all_sites=true`}
              id="siteSettingsList"
              className="sites-list custom-sites-list"
              columns={columns}
            />
          </>
        ) : (
          <div className="no-data">Please select a customer</div>
        )}
      </div>
      {showModal && (
        <CreateSite
          site={editSite}
          isCustom={true}
          customerId={customer}
          isVisible={showModal}
          close={handleAddSite}
          onUpdate={handleCustomerSiteUpdate}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
  isFetchingCustomers: state.customer.isFetchingCustomers,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  customSiteCRUD: (method: HTTPMethods, customerId: number, data?: ISite) =>
    dispatch(customSiteCRUD(method, customerId, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerSites);
