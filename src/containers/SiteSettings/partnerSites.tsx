import React, { useState } from "react";
import { connect } from "react-redux";
import {
  fireReportPartnerSitesCRUD,
  FIRE_REPORT_SITES_SUCCESS,
} from "../../actions/setting";
import {
  fetchTaskStatusConfig,
  CONFIG_TASK_STATUS_SUCCESS,
} from "../../actions/configuration";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import SquareButton from "../../components/Button/button";
import EditButton from "../../components/Button/editButton";
import InfiniteListing from "../../components/InfiniteList/infiniteList";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import CreateSite from "../CreateNew/createSite";

interface ConfiguredSitesProps {
  isTaskPolling: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchTaskStatus: (id: number) => Promise<any>;
  partnerSitesCRUD: (method: HTTPMethods, site?: ISite) => Promise<any>;
}

const ConfiguredSites: React.FC<ConfiguredSitesProps> = (props) => {
  const [editSite, setEditSite] = useState<ISite>();
  const [showModal, setShowModal] = useState<boolean>(false);
  const [refreshKey, setRefreshKey] = useState<number>(Math.random());

  const fetchTaskStatus = (taskId: number, type: string) => {
    if (taskId) {
      props.fetchTaskStatus(taskId).then((action) => {
        if (action.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (action.response.status === "SUCCESS") {
            props.addSuccessMessage(`Partner Sites' ${type} task completed!`);
          }
          if (action.response.status === "PENDING") {
            setTimeout(() => fetchTaskStatus(taskId, type), 3000);
          }
          if (action.response.status === "FAILURE") {
            props.addErrorMessage(`Partner Sites' ${type} task failed!`);
          }
        }
      });
    }
  };

  const handleCUDPartnerSite = (site: ISite, deleteMode?: boolean) => {
    const editMode: boolean = Boolean(editSite);
    const CUDType: string = deleteMode
      ? "deletion"
      : editMode
      ? "updation"
      : "creation";
    const method: HTTPMethods = deleteMode
      ? "delete"
      : editMode
      ? "put"
      : "post";

    props.partnerSitesCRUD(method, site).then((action) => {
      if (action.type === FIRE_REPORT_SITES_SUCCESS) {
        const taskId = action.response.task_id;
        setRefreshKey(Math.random());
        fetchTaskStatus(taskId, CUDType);
        if (!deleteMode) {
          closeModal();
        }
        props.addSuccessMessage(`Partner Sites' ${CUDType} task started!`);
      }
    });
  };

  const closeModal = () => {
    setShowModal(false);
    setEditSite(undefined);
  };

  const onClickEdit = (site: ISite) => {
    const sitePayload: ISite = {
      ...site,
      country_id: site.country_crm_id,
      state_id: site.state_crm_id,
    };
    delete sitePayload.country_crm_id;
    delete sitePayload.state_crm_id;
    setEditSite(sitePayload);
    setShowModal(true);
  };

  const columns: IColumnInfinite<ISite>[] = [
    {
      id: "name",
      name: "Site",
      ordering: "name",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.name}</div>
      ),
    },
    {
      id: "address_line_1",
      name: "Address",
      ordering: "address_line_1",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.address_line_1}</div>
      ),
    },
    {
      id: "city",
      name: "City",
      ordering: "city",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.city ? row.city : "-"}</div>
      ),
    },
    {
      id: "state",
      name: "State",
      ordering: "state",
      className: "hide-overflow-width",
      Cell: (row: ISite) => (
        <div className="type ellipsis-text">{row.state ? row.state : "-"}</div>
      ),
    },
    {
      name: "",
      className: "configured-sites-actions",
      Cell: (site: ISite) =>
        !props.isTaskPolling && (
          <>
            <EditButton
              title="Edit Partner Site"
              onClick={() => onClickEdit(site)}
            />
            <SmallConfirmationBox
              className="remove"
              showButton={true}
              onClickOk={() => handleCUDPartnerSite(site, true)}
              text={"Partner Site"}
              title="Remove Partner Site"
            />
          </>
        ),
    },
  ];

  return (
    <>
      {props.isTaskPolling && (
        <div className="task-status">Partner Site task is in progress</div>
      )}
      <div className="sites-listing-container">
        <SquareButton
          onClick={() => setShowModal(true)}
          content={"Add New Site"}
          bsStyle={"primary"}
          className="site-setting-add-site"
          disabled={props.isTaskPolling}
        />
        <InfiniteListing
          showSearch={true}
          refreshKey={refreshKey}
          url={`providers/fire-report/partner-sites`}
          id="siteSettingsList"
          className="sites-list"
          columns={columns}
        />
      </div>
      {showModal && (
        <CreateSite
          site={editSite}
          isCustom={false}
          close={closeModal}
          isVisible={showModal}
          handleEditPartnerSite={handleCUDPartnerSite}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  isTaskPolling: state.configuration.isTaskPolling,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  partnerSitesCRUD: (method: HTTPMethods, site?: ISite) =>
    dispatch(fireReportPartnerSitesCRUD(method, site)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfiguredSites);
