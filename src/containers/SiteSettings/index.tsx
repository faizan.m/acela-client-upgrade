import React from "react";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import CustomerSites from "./customerSites";
import PartnerSites from "./partnerSites";
import FireReportSettings from "./fireReportSettings";
import "./style.scss";
import SiteSyncErrors from "./siteSyncErrors";

const SiteSettings: React.FC = () => {
  return (
    <div className="site-settings-wrapper">
      <h3>Site Settings</h3>
      <div className="site-settings-container">
        <HorizontalTabSlider>
          <Tab title={"All Partner Sites"}>
            <PartnerSites />
          </Tab>
          <Tab title={"Customer Sites"}>
            <CustomerSites />
          </Tab>
          <Tab title={"FIRE Report"}>
            <FireReportSettings />
          </Tab>
          <Tab title={"Site Sync Errors"}>
            <SiteSyncErrors />
          </Tab>
        </HorizontalTabSlider>
      </div>
    </div>
  );
};

export default SiteSettings;
