import React from "react";
import InfiniteListing from "../../components/InfiniteList/infiniteList";

interface ISiteSyncError {
  operation: string;
  customer_name: string;
  customer_crm_id: number;
  site_crm_id: number;
  site_name: string;
  site_payload: ISite;
  errors: {
    id: number;
    name: string;
  }[];
}

const columns: IColumnInfinite<ISiteSyncError>[] = [
  {
    name: "Operation",
    className: "width-20",
    id: "operation",
  },
  {
    name: "Customer",
    ordering: "customer_name",
    className: "width-30",
    id: "customer_name",
  },
  {
    name: "Site",
    ordering: "site_name",
    id: "site_name",
    Cell: (site) => {
      const siteName = site.site_name ? site.site_name : site.site_payload.name;
      return siteName;
    },
  },
];

const SiteSyncErrors = () => {
  return (
    <InfiniteListing
      id="SiteSyncErrors"
      url={"providers/fire-report/partner-sites/sync-errors"}
      columns={columns}
      className="site-sync-errors-listing"
      showSearch={true}
      height={"auto"}
    />
  );
};

export default SiteSyncErrors;
