import { cloneDeep } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  EDIT_PROVIDER_PROFILE_FAILURE,
  EDIT_PROVIDER_PROFILE_SUCCESS,
  FETCH_PROVIDER_PROFILE_SUCCESS,
  fetchProviderProfile,
  updateProviderProfile,
  fetchTimezones,
} from '../../actions/provider';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import AppValidators from '../../utils/validator';

// import ColorPicker from 'rc-color-picker'; (New Component)
import { SHOW_SUCCESS_MESSAGE } from '../../actions/appState';
import store from '../../store';

import Checkbox from '../../components/Checkbox/checkbox';
import './style.scss';
import { getConvertedColorWithOpacity, isValidVal } from '../../utils/CommonUtils';
import { commonFunctions } from '../../utils/commonFunctions';
import { Prompt } from 'react-router-dom';
interface IErpState {
  url_alias: string;
  ticket_note: string;
  support_contact: string;
  colour_code: string;
  logo_url: string;
  company_url: string;
  enable_url: boolean;
  logo: any;
  default_colour_code: string;
  customer_instance_id: string;
  isFormValid: boolean;
  timezone: string;
  error: {
    url_alias: IFieldValidation;
    ticket_note: IFieldValidation;
    support_contact: IFieldValidation;
    customer_instance_id: IFieldValidation;
    logo: IFieldValidation;
    company_url: IFieldValidation;
    timezone: IFieldValidation;
  };
  isFetching: boolean;
  errorList?: any;
  unsaved: boolean;
}

interface IErpProps extends ICommonProps {
  fetchProviderProfile: TFetchProviderProfile;
  updateProviderProfile: TEditProviderProfile;
  user: ISuperUser;
  timezones: any;
  fetchTimezones: any;
  customers:any;
}

class SettingAlias extends Component<IErpProps, IErpState> {
  static emptyState: IErpState = {
    url_alias: '',
    ticket_note: '',
    support_contact: '',
    colour_code: '',
    default_colour_code: '',
    company_url: '',
    enable_url: false,
    isFormValid: false,
    unsaved: false,
    logo: null,
    logo_url: '',
    customer_instance_id: '',
    timezone: "America/Los_Angeles",
    error: {
      url_alias: {
        errorState: "success",
        errorMessage: '',
      },
      ticket_note: {
        errorState: "success",
        errorMessage: '',
      },
      support_contact: {
        errorState: "success",
        errorMessage: '',
      },
      logo: {
        errorState: "success",
        errorMessage: '',
      },
      company_url: {
        errorState: "success",
        errorMessage: '',
      },
      customer_instance_id: {
        errorState: "success",
        errorMessage: '',
      },
      timezone: {
        errorState: "success",
        errorMessage: '',
      },
    },
    errorList: null,
    isFetching: true,
  };

  constructor(props: any) {
    super(props);
    this.state = cloneDeep(SettingAlias.emptyState);
  }

  componentDidMount() {
    this.props
      .fetchProviderProfile()
      .then(action => {
        if (action.type === FETCH_PROVIDER_PROFILE_SUCCESS) {
          const response: IProviderProfile = action.response;
          this.setState({
            url_alias: response.url_alias === null ? `` : response.url_alias,
            ticket_note:
              response.ticket_note === null ? `` : response.ticket_note,
            support_contact:
              response.support_contact === null ? `` : response.support_contact,
            colour_code:
              response.colour_code === null ? `` : response.colour_code,
            default_colour_code:
              response.colour_code === null ? `` : response.colour_code,
            logo_url: response.logo === null ? `` : response.logo,
            company_url:
              response.company_url === null ? `` : response.company_url,
            timezone:
              response.timezone === null ? `` : response.timezone,
            enable_url: response.enable_url,
            isFetching: false,
            customer_instance_id:
              response.customer_instance_id === null
                ? ``
                : response.customer_instance_id,
          });
        }
      })
      .catch(() => {
        this.setState({ isFetching: false });
      });
    if (!this.props.timezones || (this.props.timezones && this.props.timezones.length === 0)) {
      this.props.fetchTimezones();
    }
  }

  componentWillUnmount() {
    this.resetColor();
  }

  handleChange = (e: any): void => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    (newState.unsaved as boolean)= true;
    this.setState(newState);
  };

  handleChangeCheckBox = (event: any) => {
    const targetValue = event.target.checked;
    const newState = cloneDeep(this.state);
    newState[event.target.name] = targetValue;
    (newState.unsaved as boolean)= true;
    this.setState(newState);
  };

  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const logo = files[0];
    this.setState({ logo, unsaved: true });
  };
  getTimezones = () => {
    const timezones = this.props.timezones
      ? this.props.timezones.map(t => ({
        value: t.timezone,
        label: t.display_name,
        disabled: false,
      }))
      : [];

    return timezones;
  };

  validateForm() {
    this.clearValidationStatus();
    const newState: IErpState = cloneDeep(this.state);
    let isValid = true;

    if (
      this.state.url_alias &&
      !AppValidators.isValidUrl(this.state.url_alias)
    ) {
      newState.error.url_alias.errorState = "error";
      newState.error.url_alias.errorMessage = 'Invalid URL';
      isValid = false;
    }
    if (
      this.state.support_contact &&
      !AppValidators.isValidEmail(this.state.support_contact)
    ) {
      newState.error.support_contact.errorState = "error";
      newState.error.support_contact.errorMessage = 'Enter a valid email';

      isValid = false;
    }

    if (!this.state.customer_instance_id) {
      newState.error.customer_instance_id.errorState = "error";
      newState.error.customer_instance_id.errorMessage =
        'Please select customer';
      isValid = false;
    }
    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  }

  handleSubmit = () => {
    if (this.validateForm()) {
      this.setState({ isFetching: true });
      const data = new FormData();
      if (this.state.logo) {
        data.append('logo', this.state.logo);
      }
      data.append('url_alias', this.state.url_alias);
      data.append('ticket_note', this.state.ticket_note);
      data.append('support_contact', this.state.support_contact);
      data.append('colour_code', this.state.colour_code);
      data.append('company_url', this.state.company_url);
      data.append('timezone', this.state.timezone);
      data.append('customer_instance_id', this.state.customer_instance_id);
      data.append(
        'enable_url',
        this.state.company_url && this.state.enable_url ? 'true' : 'false'
      );

      this.props.updateProviderProfile(data).then(action => {
        if (action.type === EDIT_PROVIDER_PROFILE_SUCCESS) {
          this.clearValidationStatus();
          store.dispatch({
            type: SHOW_SUCCESS_MESSAGE,
            message: 'Updated Successfully.',
          });
          const response: IProviderProfile = action.response;

          this.setState({
            url_alias: response.url_alias === null ? `` : response.url_alias,
            ticket_note:
              response.ticket_note === null ? `` : response.ticket_note,
            support_contact:
              response.support_contact === null ? `` : response.support_contact,
            colour_code:
              response.colour_code === null ? `` : response.colour_code,
            default_colour_code:
              response.colour_code === null ? `` : response.colour_code,
            timezone:
              response.timezone === null ? `` : response.timezone,
            logo_url: response.logo === null ? `` : response.logo,
            logo: null,
            isFetching: false,
            unsaved: false
          });
        } else if (action.type === EDIT_PROVIDER_PROFILE_FAILURE) {
          this.clearValidationStatus();
          this.setState({
            errorList: action.errorList.data,
            isFetching: false,
          });
          this.setValidationErrors(this.state.errorList);
        } else {
          this.setState({ isFetching: false });
        }
      });
    }
  };

  clearValidationStatus() {
    const cleanState = cloneDeep(SettingAlias.emptyState.error);

    const newState: IErpState = cloneDeep(this.state);
    newState.error = cleanState;

    this.setState(newState);
  }

  setValidationErrors = errorList => {
    let newState: IErpState = cloneDeep(this.state);
    newState = commonFunctions.errorStateHandle(errorList,newState)
    newState.isFormValid = false;
    this.setState(newState);
  }

  onCancel = () => {
    this.resetColor();
    this.props.history.push('/');
  };

  handleChangeColor = (e: any) => {
    if (e.color) {
      this.setState({ colour_code: e.color });
      document.documentElement.style.setProperty('--primary', e.color);
      document.documentElement.style.setProperty(
        "--primary-light",
        getConvertedColorWithOpacity(e.color)
      );
      this.setState({unsaved: true})
    }
  };
  resetColor = () => {
    if (this.state.default_colour_code) {
      document.documentElement.style.setProperty(
        '--primary',
        this.state.default_colour_code
      );
      document.documentElement.style.setProperty(
        "--primary-light",
        getConvertedColorWithOpacity(this.state.default_colour_code)
      );
      this.setState({ colour_code: this.state.default_colour_code });
    } else {
      document.documentElement.style.removeProperty('--primary');
    }
  };
  getCustomerOptions = () => {
    return this.props.customers
      ? this.props.customers.map((cust, index) => ({
        value: cust.id,
        label: cust.name,
      }))
      : [];
  };


  render() {
    return (
      <div className="erp-container">
        <h3>General Settings</h3>
        <div className="loader">
          <Spinner show={this.state.isFetching} />
        </div>
        <div className="general-setting">
          <div className={`erp ${this.state.isFetching ? 'loading' : ''}`}>
            <Input
              field={{
                label: 'URL Alias ',
                type: "TEXT",
                value: `${this.state.url_alias}`,
              }}
              error={this.state.error.url_alias}
              width={6}
              name="url_alias"
              placeholder="www.lookingpoint.com"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: 'Ticket Note ',
                type: "TEXT",
                value: `${this.state.ticket_note}`,
              }}
              error={this.state.error.ticket_note}
              width={6}
              name="ticket_note"
              placeholder="Lookingpoint"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: 'Support Contact',
                type: "TEXT",
                value: `${this.state.support_contact}`,
              }}
              error={this.state.error.support_contact}
              width={6}
              name="support_contact"
              placeholder="Enter Support Contact"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: 'Timezone',
                type: "PICKLIST",
                isRequired: false,
                value: this.state.timezone,
                options: this.getTimezones()
              }}
              error={this.state.error.timezone}
              width={6}
              placeholder="Select timezone"
              name="timezone"
              onChange={event => this.handleChange(event)}
            />
            <Input
              field={{
                label: 'Change Logo',
                type: "FILE",
                value: `${this.state.logo ? this.state.logo : ''}`,
                id: 'logo',
              }}
              error={this.state.error.logo}
              width={this.state.logo ? 6 : 3}
              name="logo"
              accept="image/png, image/jpg, image/jpeg"
              onChange={this.handleFile}
              className="custom-file-input"
            />
            {
              this.state.logo ? '' :
                <>
                  <div
                    className={`current-logo col-md-3`}
                    style={{
                      backgroundImage: `url(${isValidVal(this.state.logo_url) &&
                        `${this.state.logo_url}`
                        })`,
                    }} />
                </>
            }
            {this.props.user &&
              this.props.user.type &&
              this.props.user.type === 'provider' &&
              this.props.user.role === 'owner' && (
                <Input
                  field={{
                    label: 'Customer',
                    type: "PICKLIST",
                    value: `${this.state.customer_instance_id}`,
                    options: this.getCustomerOptions(),
                  }}
                  error={this.state.error.customer_instance_id}
                  width={6}
                  name="customer_instance_id"
                  placeholder="Select Customer"
                  onChange={this.handleChange}
                />)}
            <div className="erp-theme-setting col-md-6 col-xs-6">
              <div className="field-section  col-md-3 col-xs-3">
                <div className="field__label row">
                  <label className="field__label-label" title="Change Logo">
                    Select Theme Color
                </label>
                </div>
                <div className="field__input row">
                  {/* <ColorPicker
                    color={this.state.colour_code}
                    onChange={this.handleChangeColor}
                    placement="topLeft"
                    defaultColor="#FF8413"
                    enableAlpha={false}
                  /> */}
                </div>
              </div>
              <SquareButton
                content="Reset"
                bsStyle={"default"}
                onClick={this.resetColor}
                className="reset-btn"
              />
            </div>
            <div className="company-url col-md-12 col-xs-12">
              <Input
                field={{
                  label: 'Company URL',
                  type: "TEXT",
                  value: `${this.state.company_url}`,
                }}
                error={this.state.error.company_url}
                width={6}
                name="company_url"
                placeholder="Enter Company URL"
                onChange={this.handleChange}
              />
              <Checkbox
                isChecked={this.state.enable_url}
                name="enable_url"
                onChange={e => this.handleChangeCheckBox(e)}
                disabled={!this.state.company_url}
              >
                Enable URL
            </Checkbox>
            </div>

          </div>
          <div className="erp__actions">
            <SquareButton
              content="Save"
              bsStyle={"primary"}
              onClick={this.handleSubmit}
            />
            <SquareButton
              content="Cancel"
              bsStyle={"default"}
              onClick={this.onCancel}
              className="erp-container__save"
            />
          </div>
        </div>
        <Prompt when={this.state.unsaved} message="Are you sure? Changes not saved!!!"/>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  timezones: state.provider.timezones,
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProviderProfile: () => dispatch(fetchProviderProfile()),
  updateProviderProfile: (req: IProviderProfile) =>
    dispatch(updateProviderProfile(req)),
  fetchTimezones: () => dispatch(fetchTimezones()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingAlias);
