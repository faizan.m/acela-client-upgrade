import React, { useEffect, useMemo, useState } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  client360SettingCRU,
  CLIENT_360_SETTINGS_CRU_FAILURE,
  CLIENT_360_SETTINGS_CRU_SUCCESS,
  GET_PO_STATUS_SUCCESS,
  getPurchaseOrderStatusList,
} from "../../actions/setting";
import { addSuccessMessage } from "../../actions/appState";
import { getQuoteStatusesList } from "../../actions/sow";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Spinner from "../../components/Spinner";
import { commonFunctions } from "../../utils/commonFunctions";

interface Client360SettingProps {
  isFetchingQStatuses: boolean;
  qStatusList: IDLabelObject[];
  addSuccessMessage: TShowSuccessMessage;
  getQuoteStatusesList: () => Promise<any>;
  getPurchaseOrderStatusList: () => Promise<any>;
  client360SettingCRU: (
    request: HTTPMethods,
    data?: IClient360Settings
  ) => Promise<any>;
}

const Client360Setting: React.FC<Client360SettingProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<IErrorValidation>({});
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [fetchingPOs, setFetchingPOs] = useState<boolean>(false);
  const [poStatusList, setPOStatusList] = useState<IPickListOptions[]>([]);
  const [settings, setSettings] = useState<IClient360Settings>({
    won_opp_status_crm_ids: [],
    open_po_status_crm_ids: [],
  });

  const quoteStatusOptions: IPickListOptions[] = useMemo(
    () =>
      props.qStatusList
        ? props.qStatusList.map((el) => ({
            value: el.id,
            label: el.label,
          }))
        : [],
    [props.qStatusList]
  );

  useEffect(() => {
    getPOStatusList();
    fetchClient360Settings();
    props.getQuoteStatusesList();
  }, []);

  const getPOStatusList = () => {
    setFetchingPOs(true);
    props
      .getPurchaseOrderStatusList()
      .then((action) => {
        if (action.type === GET_PO_STATUS_SUCCESS) {
          const poStatusOptions = action.response
            ? action.response.map((c) => ({
                value: c.id,
                label: c.name,
              }))
            : [];
          setPOStatusList(poStatusOptions);
        }
      })
      .finally(() => setFetchingPOs(false));
  };

  const handleChangeSetting = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSettings((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const fetchClient360Settings = () => {
    setLoading(true);
    props
      .client360SettingCRU("get")
      .then((action) => {
        if (action.type === CLIENT_360_SETTINGS_CRU_SUCCESS) {
          setSettings(action.response);
        } else if (action.type === CLIENT_360_SETTINGS_CRU_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const validate = (): boolean => {
    let isValid = true;
    const errors: IErrorValidation = {};
    const keys = ["won_opp_status_crm_ids", "open_po_status_crm_ids"];
    keys.forEach((key) => {
      if (!settings[key].length) {
        isValid = false;
        errors[key] = commonFunctions.getErrorState("This field is required");
      } else errors[key] = commonFunctions.getErrorState();
    });
    setError(errors);
    return isValid;
  };

  const onSubmit = () => {
    if (validate()) {
      setLoading(true);
      props
        .client360SettingCRU(
          firstSave ? "post" : "put",
          settings
        )
        .then((action) => {
          if (action.type === CLIENT_360_SETTINGS_CRU_SUCCESS) {
            props.addSuccessMessage(
              `Client 360 Settings ${
                firstSave ? "Saved" : "Updated"
              } Successfully!`
            );
            setFirstSave(false);
          }
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <div className="client-360-setting-container">
      <Spinner show={loading} className="client-setting-spinner" />
      <div className="setting-row">
        <h3>Open Opportunities Chart Settings</h3>
        <div className="col-md-12 client-setting-note">
          {`These settings can be configured in `}
          <Link to="/erp-setting/primary" className="client-360-settings-link">
            {`ERP Settings > ERP Primary > Field Mapping > Opportunities`}
          </Link>
        </div>
      </div>
      <div className="setting-row">
        <h3>Open Activities Settings</h3>
        <div className="col-md-12 client-setting-note">
          {`These settings can be configured in `}
          <Link to="/setting/sow?tab=Sales Activity" className="client-360-settings-link">
            {`Settings > Sales Settings > Sales Activity > Status Setting`}
          </Link>
          <br />
          Open Activities lists those activities with status not marked as Is
          Closed Status
        </div>
      </div>
      <div className="setting-row">
        <h3>Active Projects Settings</h3>
        <div className="col-md-12 client-setting-note">
          {`These settings can be configured in `}
          <Link
            to="/setting/project-setting?tab=General"
            className="client-360-settings-link"
          >
            {`Settings > Project Setting > General > Project Overall Status Mapping`}
          </Link>
          <br />
          Active Projects lists those projects with status marked as Is Open
          Status
        </div>
      </div>
      <div className="setting-row">
        <h3>Managed Tickets Settings</h3>
        <div className="col-md-12 client-setting-note">
          {"These settings can be configured in "}
          <Link to="/erp-setting/primary" className="client-360-settings-link">
            {`ERP Settings > ERP Primary > Field Mapping > Service Board Mapping`}
          </Link>
        </div>
      </div>
      <div className="setting-row">
        <h3>Won Opportunities in Last 90 Days Settings</h3>
        <div className="col-md-12">
          <Input
            field={{
              label: "WON Opportunity Status",
              type: "PICKLIST",
              value: settings.won_opp_status_crm_ids,
              options: quoteStatusOptions,
              isRequired: true,
            }}
            className="client-360-select"
            width={8}
            multi={true}
            placeholder={`Select Opportunity Status`}
            name="won_opp_status_crm_ids"
            onChange={handleChangeSetting}
            labelIcon="info"
            labelTitle={
              "Opportunities with these statuses will be shown in the Won Opportunities in Last 90 Days listing"
            }
            loading={props.isFetchingQStatuses}
            error={error.won_opp_status_crm_ids}
          />
        </div>
      </div>
      <div className="setting-row">
        <h3>Open Orders Settings</h3>
        <div className="col-md-12">
          <Input
            field={{
              label: "Purchase Order Statuses",
              type: "PICKLIST",
              value: settings.open_po_status_crm_ids,
              options: poStatusList,
              isRequired: true,
            }}
            className="client-360-select"
            width={8}
            multi={true}
            placeholder={`Select PO Status`}
            name="open_po_status_crm_ids"
            onChange={handleChangeSetting}
            labelIcon="info"
            labelTitle={
              "Purchase Orders with these statuses will be shown in the Open Purchase Orders Chart"
            }
            loading={fetchingPOs}
            error={error.open_po_status_crm_ids}
          />{" "}
        </div>
      </div>
      <SquareButton
        onClick={onSubmit}
        content={firstSave ? "Save" : "Update"}
        bsStyle={"primary"}
        className="client-360-settings-save"
        disabled={loading}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  qStatusList: state.sow.qStatusList,
  isFetchingQStatuses: state.sow.isFetchingQStatusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStatusesList: () => dispatch(getQuoteStatusesList()),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  getPurchaseOrderStatusList: () => dispatch(getPurchaseOrderStatusList()),
  client360SettingCRU: (request: HTTPMethods, data?: IClient360Settings) =>
    dispatch(client360SettingCRU(request, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Client360Setting);
