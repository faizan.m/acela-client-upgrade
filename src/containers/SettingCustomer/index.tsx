import _cloneDeep from "lodash/cloneDeep";
import React, { Component } from "react";
import { connect } from "react-redux";
import {
  FETCH_STATUS_PO_SUCCESS,
  getDataCommon,
  GET_DATA_SUCCESS,
  salesOrderStatusSetting,
} from "../../actions/orderTracking";
import { addSuccessMessage, addErrorMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Client360Settings from "./client360Settings";
import "./style.scss";

enum PageType {
  Mapping,
  Client360,
}

interface IISettingCustomerState {
  settings: {
    in_progress_status: number;
    waiting_on_smartnet_status: number;
    closed_statuses: number[];
  };
  currentPage: {
    pageType: PageType;
  };
  submitted: boolean;
  loadingList: boolean;
  loading: boolean;
  statusList: any[];
}

interface IISettingCustomerProps extends ICommonProps {
  getDataCommon: any;
  salesOrderStatusSetting: any;
  addSuccessMessage: any;
  addErrorMessage: any;
}

class SettingCollector extends Component<
  IISettingCustomerProps,
  IISettingCustomerState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Mapping,
    },
    settings: {
      in_progress_status: 0,
      waiting_on_smartnet_status: 0,
      closed_statuses: [],
    },
    submitted: false,
    loading: false,
    loadingList: false,
    statusList: [],
  });

  


  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const defaultTab = query.get("tab");
    if (defaultTab) {
      this.changePage(PageType[defaultTab]);
    }
    this.salesOrderStatusSetting();
  }

  salesOrderStatusSetting = () => {
    this.setState({ loading: true });
    this.props.salesOrderStatusSetting("get").then((action) => {
      if (action.type === FETCH_STATUS_PO_SUCCESS) {
        const settings = action.response;
        this.setState({ settings });
      }
      this.setState({ loading: false });
      this.salesOrderStatusList();
    });
  };

  salesOrderStatusList = () => {
    this.setState({ loadingList: true });
    this.props
      .getDataCommon(
        "providers/workflows/smartnet-receiving/sales-order-statuses",
        "get"
      )
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          const statusList = action.response;
          statusList.map((x) => (x.selected = false));
          this.setState({ statusList });
        }
        this.setState({ loadingList: false });
      });
  };

  handleChange = (event: any) => {
    const newState = _cloneDeep(this.state);
    newState.settings[event.target.name] = event.target.value;
    this.setState(newState);
  };

  OnSave = () => {
    this.setState({ loading: true });
    if (true) {
      const data = this.state.settings;
      this.props.salesOrderStatusSetting("put", data).then((action) => {
        if (action.type === FETCH_STATUS_PO_SUCCESS) {
          this.setState({ submitted: false, loading: false });
          this.props.addSuccessMessage("Updated");
        } else {
          this.props.addErrorMessage("Error while update");
        }
      });
    }
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="voilence__header">
        <div
          className={`voilence__header-link ${
            currentPage.pageType === PageType.Mapping
              ? "voilence__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Mapping)}
        >
          Operations Status Setting
        </div>
        <div
          className={`voilence__header-link ${
            currentPage.pageType === PageType.Client360
              ? "voilence__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Client360)}
        >
          Client 360 Setting
        </div>
      </div>
    );
  };

  checkNotSelectedStatus = (id) => {
    let list = [
      this.state.settings.in_progress_status,
      this.state.settings.waiting_on_smartnet_status,
      ...this.state.settings.closed_statuses,
    ];
    const selected = list.includes(id);
    return selected ? true : false;
  };
  getStatusList = () => {
    let list = [];

    list = this.state.statusList.map((role) => ({
      value: role.id,
      label: role.label,
      disabled: this.checkNotSelectedStatus(role.id),
    }));

    return list;
  };
  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="customer-setting-wrapper">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <h3>Customer Settings</h3>
        <div className="customer-setting">
          {this.renderTopBar()}
          {currentPage.pageType === PageType.Mapping && (
            <div className="customer-setting-container">
              <div className="selection-fields">
                <Input
                  field={{
                    label: "In Progress Status",
                    type: "PICKLIST",
                    value: this.state.settings.in_progress_status,
                    options: this.getStatusList(),
                    isRequired: true,
                  }}
                  width={4}
                  clearable={true}
                  name="in_progress_status"
                  onChange={(e) => this.handleChange(e)}
                  loading={this.state.loadingList}
                  placeholder={`Select Status`}
                />
                <Input
                  field={{
                    label: "Waiting on smartnet Status",
                    type: "PICKLIST",
                    value: this.state.settings.waiting_on_smartnet_status,
                    options: this.getStatusList(),
                    isRequired: true,
                  }}
                  clearable={true}
                  width={4}
                  name="waiting_on_smartnet_status"
                  onChange={(e) => this.handleChange(e)}
                  loading={this.state.loadingList}
                  placeholder={`Select Status`}
                />
                <Input
                  field={{
                    label: "Close Statuses",
                    type: "PICKLIST",
                    value: this.state.settings.closed_statuses,
                    options: this.getStatusList(),
                    isRequired: true,
                  }}
                  width={4}
                  multi={true}
                  clearable={true}
                  name="closed_statuses"
                  onChange={(e) => this.handleChange(e)}
                  loading={this.state.loadingList}
                  placeholder={`Select Status`}
                />
              </div>
              <SquareButton
                onClick={() => this.OnSave()}
                content="Save"
                bsStyle={"primary"}
                className="save-button-customer-setting"
                disabled={this.state.loading}
              />
            </div>
          )}
          {currentPage.pageType === PageType.Client360 && <Client360Settings />}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getDataCommon: (url) => dispatch(getDataCommon(url)),
  salesOrderStatusSetting: (method: string, data) =>
    dispatch(salesOrderStatusSetting(method, data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingCollector);
