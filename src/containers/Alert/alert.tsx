import * as React from "react";
// import NotificationSystem, {
//   Notification,
//   Style,
// } from "react-notification-system";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { removeError } from "../../actions/appState";
import "./style.scss";

interface IAlertProps extends ICommonProps {
  errors: IHash;
  infoMessage: IHash;
  warnings: IHash;
  successMessage: IHash;
  removeError: (errorKey: string) => void;
}

// class Alert extends React.Component<IAlertProps, {}> {
//   notificationSystem: NotificationSystem;

//   componentDidUpdate(prevProps: IAlertProps) {
//     if (prevProps.errors !== this.props.errors) {
//       Object.keys(this.props.errors).map((errorKey) => {
//         if (!Array.from(Object.keys(prevProps.errors)).includes(errorKey)) {
//           this.addNotification({
//             errorKey,
//             children: (
//               <div className="notification-message-container">
//                 <img
//                   className="notification-img"
//                   src="/assets/new-icons/error.svg"
//                   alt="error-icon"
//                 />
//                 <div className="notification-message-acela">
//                   {this.props.errors[errorKey]}
//                 </div>
//               </div>
//             ),
//           });
//         }
//       });
//     }
//     if (prevProps.warnings !== this.props.warnings) {
//       Object.keys(this.props.warnings).map((errorKey) => {
//         if (!Array.from(Object.keys(prevProps.warnings)).includes(errorKey)) {
//           this.addNotification({
//             errorKey,
//             type: "warning",
//             children: (
//               <div className="notification-message-container">
//                 <img
//                   className="notification-img"
//                   src="/assets/new-icons/warning_notification.svg"
//                   alt="warning-icon"
//                 />
//                 <div className="notification-message-acela">
//                   {this.props.warnings[errorKey]}
//                 </div>
//               </div>
//             ),
//           });
//         }
//       });
//     }
//     if (prevProps.successMessage !== this.props.successMessage) {
//       Object.keys(this.props.successMessage).map((errorKey) => {
//         if (
//           !Array.from(Object.keys(prevProps.successMessage)).includes(errorKey)
//         ) {
//           this.addNotification({
//             errorKey,
//             type: "success",
//             children: (
//               <div className="notification-message-container">
//                 <img
//                   className="notification-img"
//                   src="/assets/new-icons/success.svg"
//                   alt="success-icon"
//                 />
//                 <div className="notification-message-acela">
//                   {this.props.successMessage[errorKey]}
//                 </div>
//               </div>
//             ),
//           });
//         }
//       });
//     }
//     if (prevProps.infoMessage !== this.props.infoMessage) {
//       Object.keys(this.props.infoMessage).map((errorKey) => {
//         if (
//           !Array.from(Object.keys(prevProps.infoMessage)).includes(errorKey)
//           ) {
//           this.addNotification({
//             errorKey,
//             type: "info",
//             children: (
//               <div className="notification-message-container">
//                 <img
//                   className="notification-img"
//                   src="/assets/new-icons/info_alert.svg"
//                   alt="info-icon"
//                 />
//                 <div className="notification-message-acela">
//                   {this.props.infoMessage[errorKey]}
//                 </div>
//               </div>
//             ),
//           });
//         }
//       });
//     }
//   }

//   removeNotification = (uidOrNotification: string): void => {
//     this.props.removeError(uidOrNotification);
//     if (this.notificationSystem) {
//       this.notificationSystem.removeNotification(uidOrNotification);
//     }
//   };

//   addNotification = (error: IHash) => {
//     this.notificationSystem.addNotification({
//       uid: error.errorKey,
//       level: error.type ? error.type : "error",
//       autoDismiss: 10,
//       position: "tr",
//       dismissible: "button",
//       children: error.children,
//       onRemove: (notification: Notification) => {
//         this.removeNotification(notification.uid as string);
//       },
//     });
//   };

//   render() {
//     const NotificationStyle: Style = {
//       Containers: {
//         DefaultStyle: {
//           fontFamily: "inherit",
//           position: "fixed",
//           width: "100%",
//           padding: "0 10px 10px 10px",
//           zIndex: 111111,
//           WebkitBoxSizing: "border-box",
//           MozBoxSizing: "border-box",
//           boxSizing: "border-box",
//           height: "fit-content",
//           display: "flex",
//           alignItems: "center",
//           flexDirection: "column",
//         },
//         bl: {
//           top: "auto",
//           bottom: "0px",
//           left: "0px",
//           right: "auto",
//         },
//       },
//       NotificationItem: {
//         DefaultStyle: {
//           position: "relative",
//           width: "fit-content",
//           cursor: "pointer",
//           margin: "10px 0px 0px",
//           padding: "10px",
//           opacity: "1",
//           zIndex: "10053",
//           top: "auto",
//           bottom: "0px",
//           left: "0px",
//           right: "0px",
//           maxWidth: "500px",
//           color: "#333",
//           border: "0",
//           borderRadius: "5px",
//         },
//         error: {
//           backgroundColor: "#F6A7A3",
//           fontWeight: 700,
//         },
//         warning: {
//           backgroundColor: "#FFD38A",
//           fontWeight: 700,
//         },
//         success: {
//           backgroundColor: "#A8F1C6",
//           fontWeight: 700,
//         },
//         info: {
//           backgroundColor: "#5CD3E9",
//           fontWeight: 700,
//         },
//       },
//       Dismiss: {
//         DefaultStyle: {
//           cursor: "pointer",
//           fontSize: "30px",
//           position: "absolute",
//           top: "30%",
//           right: "5px",
//           lineHeight: "15px",
//           backgroundColor: "transparent",
//           color: "#333",
//           borderRadius: 0,
//           width: "unset",
//           height: "unset",
//           fontWeight: "lighter",
//           textAlign: "unset",
//         },
//       },
//     };

//     return (
//       <div>
//         <NotificationSystem
//           ref={(r) => (this.notificationSystem = r)}
//           style={NotificationStyle}
//           allowHTML={true}
//         />
//       </div>
//     );
//   }
// }
const Alert: React.FC<IAlertProps> = () => {
  return <></>;
};

const mapStateToProps = (state: IReduxStore) => ({
  errors: state.appState.errors,
  warnings: state.appState.warnings,
  infoMessage: state.appState.infoMessage,
  successMessage: state.appState.successMessage,
});

const mapDispatchToProps = (dispatch: any) => ({
  removeError: (errorKey: string) => dispatch(removeError(errorKey)),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Alert));
