import React from "react";

import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";

interface IAddSiteFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newSite: ISite) => void;
  countries: any;
  isPosting: boolean;
  clear?: boolean;
}

interface IAddSiteFormState {
  site: ISite;
  error: {
    name: IFieldValidation;
    address_line_1: IFieldValidation;
    address_line_2: IFieldValidation;
    city: IFieldValidation;
    state_id: IFieldValidation;
    country_id: IFieldValidation;
    zip: IFieldValidation;
  };
}

export default class AddSiteForm extends React.Component<
  IAddSiteFormProps,
  IAddSiteFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IAddSiteFormProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    site: {
      name: "",
      address_line_1: "",
      address_line_2: "",
      city: "",
      zip: "",
    },
    error: {
      name: { ...AddSiteForm.emptyErrorState },
      address_line_1: { ...AddSiteForm.emptyErrorState },
      address_line_2: { ...AddSiteForm.emptyErrorState },
      city: { ...AddSiteForm.emptyErrorState },
      state_id: { ...AddSiteForm.emptyErrorState },
      country_id: { ...AddSiteForm.emptyErrorState },
      zip: { ...AddSiteForm.emptyErrorState },
    },
    sites: [],
  });
  componentDidUpdate(prevProps: Readonly<IAddSiteFormProps>, prevState: Readonly<IAddSiteFormState>, snapshot?: any): void {
    if (prevProps.clear !== this.props.clear && this.props.clear) {
      this.setState(this.getEmptyState());
    }
  }
  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState((prevState) => ({
      site: {
        ...prevState.site,
        [targetName]: targetValue,
      },
    }));
  };

  onClose = (e) => {
    this.props.onClose(e);
    this.setState(this.getEmptyState());
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.site.name || this.state.site.name.length === 0) {
      error.name.errorState = "error";
      error.name.errorMessage = "Enter a valid site name";

      isValid = false;
    }

    if (!this.state.site.country_id) {
      error.country_id.errorState = "error";
      error.country_id.errorMessage = "Select country ";

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = (e) => {
    if (this.isValid()) {
      this.props.onSubmit(this.state.site);
    }
  };

  getCountries = () => {
    if (this.props.countries && this.props.countries.length > 0) {
      return this.props.countries.map((country) => ({
        value: country.country_id,
        label: country.country,
      }));
    } else {
      return [];
    }
  };

  getStates = () => {
    const countries = this.props.countries;
    if (countries && this.state.site.country_id) {
      const country = countries.find(
        (c) => c.country_id === this.state.site.country_id
      );

      return (
        country.states &&
        country.states.map((state) => ({
          value: state.state_id,
          label: state.state,
        }))
      );
    }

    return [];
  };

  getTitle = () => {
    return "Create Site";
  };

  getBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isPosting} />
        </div>
        <div
          className={`add-site__body ${this.props.isPosting ? `loading` : ""}`}
        >
          <Input
            field={{
              label: "Name",
              type: "TEXT",
              value: this.state.site.name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter Site Name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Address Line 1",
              type: "TEXT",
              value: this.state.site.address_line_1,
            }}
            width={6}
            placeholder="Enter Address Line 1"
            error={this.state.error.address_line_1}
            name="address_line_1"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Address Line 2",
              type: "TEXT",
              value: this.state.site.address_line_2,
            }}
            width={6}
            placeholder="Enter Address Line 2"
            error={this.state.error.address_line_2}
            name="address_line_2"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "City",
              type: "TEXT",
              value: this.state.site.city,
            }}
            width={6}
            placeholder="Enter City"
            error={this.state.error.city}
            name="city"
            onChange={this.handleChange}
          />
          <Input
            field={{
              value: this.state.site.country_id,
              label: "Country",
              type: "PICKLIST",
              isRequired: true,
              options: this.getCountries(),
            }}
            width={6}
            name="country_id"
            onChange={this.handleChange}
            placeholder="Select country"
            error={this.state.error.country_id}
          />
          <Input
            field={{
              value: this.state.site.state_id,
              label: "State",
              type: "PICKLIST",
              isRequired: false,
              options: this.getStates(),
            }}
            width={6}
            name="state_id"
            onChange={this.handleChange}
            placeholder="Select State"
            error={this.state.error.state_id}
          />

          <Input
            field={{
              label: "Zip Code",
              type: "TEXT",
              value: this.state.site.zip,
            }}
            width={6}
            placeholder="Enter Zip Code"
            error={this.state.error.zip}
            name="zip"
            onChange={this.handleChange}
          />
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-site__footer
      ${this.props.isPosting ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Add"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-site"
      />
    );
  }
}
