import React from "react";

import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import Select from "../../components/Input/Select/select";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import "./style.scss";

interface IDeviceProps {
  show: boolean;
  totalSelected: number;
  onClose: (e: any) => void;
  onUpdateCategory: any;
  onContractUpdate: any;
  onStatusUpdate: any;
  onUpdateSites: any;
  onUpdateManufaturer: any;
  isLoading: any;
  deviceCategoryList: any;
  sites: any[];
  manufacturers: any[];
  userType: any;
}

interface IDeviceState {
  isConfirm: boolean;
  deviceCategory: string;
  type: string;
  error: {
    type: IFieldValidation;
    category: IFieldValidation;
    manufacturer: IFieldValidation;
    site: IFieldValidation;
    contract: IFieldValidation;
  };
  contractNo: string;
  status: number;
  site: any;
  manufacturer: any;
  showBulk: boolean;
}

export default class BulkUpdate extends React.Component<
  IDeviceProps,
  IDeviceState
> {
  constructor(props: IDeviceProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    isConfirm: false,
    type: "",
    deviceCategory: "",
    contractNo: "",
    error: {
      type: {
        errorMessage: "",
        errorState: null,
      },
      category: {
        errorMessage: "",
        errorState: null,
      },
      site: {
        errorMessage: "",
        errorState: null,
      },
      manufacturer: {
        errorMessage: "",
        errorState: null,
      },
      contract: {
        errorMessage: "",
        errorState: null,
      },
    },
    status: 1,
    site: "",
    manufacturer: "",
    showBulk: false,
  });

  componentDidUpdate(prevProps: IDeviceProps) {
    if (this.props.show !== prevProps.show) {
      this.setState({
        isConfirm: false,
        type: "",
        status: 1,
        deviceCategory: "",
        contractNo: "",
        site: "",
        manufacturer: "",
      });
    }
  }

  onClose = (e) => {
    this.setState({
      isConfirm: false,
      type: "",
      status: 1,
      deviceCategory: "",
      contractNo: "",
      site: "",
      manufacturer: "",
    });
    this.props.onClose(e);
  };

  isValid = () => {
    const error = this.getEmptyState().error;

    let isValid = true;

    if (!this.state.type) {
      error.type.errorState = "error";
      error.type.errorMessage = "Please select field.";

      isValid = false;
    }

    if (this.state.type === "category") {
      if (!this.state.deviceCategory) {
        error.category.errorState = "error";
        error.category.errorMessage = "Please select category.";
        isValid = false;
      }
    }

    if (this.state.type === "contract") {
      if (!this.state.contractNo) {
        error.contract.errorState = "error";
        error.contract.errorMessage = "Contract Number cannot be empty.";

        isValid = false;
      }
    }

    if (this.state.type === "site") {
      if (!this.state.site) {
        error.site.errorState = "error";
        error.site.errorMessage = "Please select site.";
        isValid = false;
      }
    }

    if (this.state.type === "manufacturer") {
      if (!this.state.manufacturer) {
        error.manufacturer.errorState = "error";
        error.manufacturer.errorMessage = "Please select manufacturer.";

        isValid = false;
      }
    }
    this.setState({
      error,
    });

    return isValid;
  };

  handleIsActiveChange = (event: any) => {
    const targetValue = event.target.value;

    this.setState({ status: parseInt(targetValue, 10) });
  };

  onSubmit = (e) => {
    this.setState({ showBulk: true });
    if (this.state.type === "category") {
      this.props.onUpdateCategory(this.state.deviceCategory);
    }
    if (this.state.type === "contract") {
      this.props.onContractUpdate(this.state.contractNo);
    }
    if (this.state.type === "status") {
      this.props.onStatusUpdate(this.state.status);
    }
    if (this.state.type === "site") {
      this.props.onUpdateSites(this.state.site);
    }
    if (this.state.type === "manufacturer") {
      this.props.onUpdateManufaturer(this.state.manufacturer);
    }

    setTimeout(() => {
      this.setState({ showBulk: false, isConfirm: false });
      this.onClose(e);
      // tslint:disable-next-line:align
    }, 5000);
  };

  onUpdateClick = () => {
    if (this.isValid()) {
      this.setState((prevState) => ({
        isConfirm: !prevState.isConfirm,
      }));
    }
  };

  handleTypeChange = (e) => {
    const type = e.target.value;

    this.setState({
      type,
    });
  };

  handleSiteChange = (e) => {
    const site = e.target.value;

    this.setState({
      site,
    });
  };
  handleManufacturerChange = (e) => {
    const manufacturer = e.target.value;

    this.setState({
      manufacturer,
    });
  };
  getTitle = () => {
    return "Bulk Update";
  };

  handledeviceCategoryChange = (e) => {
    const deviceCategory = e.target.value;

    this.setState({
      deviceCategory,
    });
  };

  getBody = () => {
    const deviceCategoryList = this.props.deviceCategoryList
      ? this.props.deviceCategoryList.map((category) => ({
          value: category.category_id,
          label: category.category_name,
        }))
      : [];
    const sites = this.props.sites
      ? this.props.sites.map((site) => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((manufacturer) => ({
          value: manufacturer.id,
          label: manufacturer.label,
        }))
      : [];
    let updateFileds = [
      {
        label: "Category",
        value: "category",
      },
      {
        label: "Status",
        value: "status",
      },
      {
        label: "Contract No.",
        value: "contract",
      },
      {
        label: "Site",
        value: "site",
      },
      {
        label: "Manufacturer",
        value: "manufacturer",
      },
    ];

    if (this.props.userType === "customer") {
      updateFileds = [
        {
          label: "Status",
          value: "status",
        },
        {
          label: "Site",
          value: "site",
        },
        {
          label: "Manufacturer",
          value: "manufacturer",
        },
      ];
    }

    return (
      <div className="update-contract__body">
        <div className="loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div className="row col-md-12 col-xs-12 ">
          <div className="field-section  col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="Serial No">
                Select field for Bulk Update
              </label>
            </div>
            <div
              className={`${
                this.state.error.type.errorMessage ? `field__error` : ""
              }`}
            >
              <Select
                name="type"
                value={this.state.type}
                onChange={this.handleTypeChange}
                options={updateFileds}
                multi={false}
                searchable={true}
                placeholder="Select Field"
              />
            </div>
            {this.state.error.type.errorMessage && (
              <div className="field__error">
                {this.state.error.type.errorMessage}
              </div>
            )}
          </div>
        </div>
        {this.state.type === "category" && (
          <div className="row col-md-12 col-xs-12 category">
            <div className="field-section  col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="Serial No">
                  Select Category
                </label>
              </div>
              <div
                className={`${
                  this.state.error.category.errorMessage ? `field__error` : ""
                }`}
              >
                <Select
                  name="deviceCategories"
                  value={this.state.deviceCategory}
                  onChange={this.handledeviceCategoryChange}
                  options={deviceCategoryList}
                  multi={false}
                  searchable={true}
                  placeholder="Select Category"
                />
              </div>
              {this.state.error.category.errorMessage && (
                <div className="field__error">
                  {this.state.error.category.errorMessage}
                </div>
              )}
            </div>
            {this.state.isConfirm && this.getConfirmationCategoryBody()}
          </div>
        )}
        {this.state.type === "status" && (
          <div className="row col-md-12 col-xs-12 category">
            <Input
              field={{
                value: this.state.status,
                label: "Status",
                type: "RADIO",
                isRequired: false,
                options: [
                  { value: 1, label: "In service" },
                  { value: 2, label: "Decommissioned" },
                ],
              }}
              width={8}
              name="status_id"
              className="support-status"
              onChange={this.handleIsActiveChange}
            />
            {this.state.isConfirm && this.getConfirmationBodyStatus()}
          </div>
        )}
        {this.state.type === "contract" && (
          <div className="row col-md-12 col-xs-12 category">
            <Input
              field={{
                label: "Update Contract No.",
                type: "TEXT",
                value: this.state.contractNo,
                isRequired: true,
              }}
              width={6}
              placeholder="Enter Contract Number"
              name="contractNo"
              error={this.state.error.contract}
              onChange={this.handleContractNoChange}
            />
            {this.state.isConfirm && this.getConfirmationBodyContract()}
          </div>
        )}
        {this.state.type === "manufacturer" && (
          <div className="row col-md-12 col-xs-12 category">
            <div className="field-section  col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="Serial No">
                  Select Manufacturer
                </label>
              </div>
              <div
                className={`${
                  this.state.error.category.errorMessage ? `field__error` : ""
                }`}
              >
                <Select
                  name="manufacturer"
                  value={this.state.manufacturer}
                  onChange={this.handleManufacturerChange}
                  options={manufacturers}
                  multi={false}
                  searchable={true}
                  placeholder="Select Manufacturer"
                />
              </div>
              {this.state.error.manufacturer.errorMessage && (
                <div className="field__error">
                  {this.state.error.manufacturer.errorMessage}
                </div>
              )}
            </div>
            {this.state.isConfirm && this.getConfirmationBodyStatus()}
          </div>
        )}
        {this.state.type === "site" && (
          <div className="row col-md-12 col-xs-12 category">
            <div className="field-section  col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="Serial No">
                  Select Site
                </label>
              </div>
              <div
                className={`${
                  this.state.error.site.errorMessage ? `field__error` : ""
                }`}
              >
                <Select
                  name="site"
                  value={this.state.site}
                  onChange={this.handleSiteChange}
                  options={sites}
                  multi={false}
                  searchable={true}
                  placeholder="Select Site"
                />
              </div>
              {this.state.error.site.errorMessage && (
                <div className="field__error">
                  {this.state.error.site.errorMessage}
                </div>
              )}
            </div>
            {this.state.isConfirm && this.getConfirmationBodyStatus()}
          </div>
        )}
        {this.state.isConfirm && this.state.showBulk && this.showBulkStarted()}
      </div>
    );
  };
  handleContractNoChange = (e) => {
    const contractNo = e.target.value;

    this.setState({
      contractNo,
    });
  };
  getConfirmationBodyContract = () => {
    const totalRecords = this.props.totalSelected;

    return (
      <div className="confirm-message">
        {`You are about to update
             ${totalRecords} records, would you like
          to proceed?`}
      </div>
    );
  };

  getConfirmationBodyStatus = () => {
    const totalRecords = this.props.totalSelected;

    return (
      <div className="confirm-message">
        {`You are about to update
             ${totalRecords} records, would you like
          to proceed?`}
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="update-contract__footer">
        <SquareButton
          onClick={this.onClose}
          content={this.state.isConfirm ? "No" : "Cancel"}
          bsStyle={"default"}
        />

        {!this.state.isConfirm && (
          <SquareButton
            onClick={this.onUpdateClick}
            content="Update"
            bsStyle={"primary"}
          />
        )}

        {this.state.isConfirm && (
          <SquareButton
            onClick={this.onSubmit}
            content="Yes"
            bsStyle={"primary"}
          />
        )}
      </div>
    );
  };

  getConfirmationCategoryBody = () => {
    const totalRecords = this.props.totalSelected;

    return (
      <div className="confirm-message">
        <div className="text">
          {" "}
          <img src="/assets/icons/warning.png" />
          <div>
            {" "}
            <p>
              {" "}
              Updating category will revoke renewal and circuit associations.
            </p>
            {`You are about to update
             ${totalRecords} records, would you like
          to proceed?`}
          </div>
        </div>
      </div>
    );
  };

  showBulkStarted = () => {
    return (
      <div className="bulk-message">
        <div className="text">
          {" "}
          <div>
            {" "}
            <p> Bulk update has been initiated.</p>
          </div>
        </div>
      </div>
    );
  };
  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="update-contract"
        />
      </div>
    );
  }
}
