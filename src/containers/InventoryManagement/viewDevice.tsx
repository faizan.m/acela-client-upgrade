import _, { cloneDeep } from "lodash";
import React from "react";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import {
  fromISOStringToDateTimeString,
  fromSecondsToReadable,
  phoneNumberInFormat,
  toFormattedDate,
} from "../../utils/CalendarUtil";
import RenewalHistory from "./renewalHistory";

interface IViewDeviceFormProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  manufacturers: any[];
  sites: any[];
  fetchSites: (customerId: number) => any;
  deviceDetails: any;
  openURL: any;
  isFetching: boolean;
  resetRetry: any;
  userType: string;
  existingAssociationList?: any;
}

interface IViewDeviceFormState {
  sites: any[];
  device: IDevice;
  open: boolean;
  openMigrationInfo: boolean;
  openSoftware: boolean;
  openCircuitInfo: boolean;
  openMigrationDetails: boolean;
  openServiceInfo: boolean;
}

export default class ViewDevice extends React.Component<
  IViewDeviceFormProps,
  IViewDeviceFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IViewDeviceFormProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    device: {
      device_name: "",
      category_id: null,
      serial_number: null,
      site_id: null,
      manufacturer_id: null,
      notes: "",
      monitoring_data: {
        created_by: "",
        is_managed: false,
        last_updated_by: "",
        logicmonitor: null,
        solarwinds: null,
      },
      non_cisco: false,
      renewal_history_is_present: false,
    },
    sites: [],
    open: false,
    openMigrationInfo: false,
    openSoftware: false,
    openMigrationDetails: false,
    openCircuitInfo: false,
    openServiceInfo: false,
  });

  componentDidUpdate(prevProps: IViewDeviceFormProps) {
    const { deviceDetails } = this.props;
    if (deviceDetails && prevProps.deviceDetails !== deviceDetails) {
      this.setState({
        ...cloneDeep(this.getEmptyState()),
        device: deviceDetails,
      });
    }
  }

  onClose = (e) => {
    this.props.onClose(e);
    this.setState({
      ...cloneDeep(this.getEmptyState()),
    });
  };

  resetRetry = () => {
    this.setState((prevState) => ({
      device: {
        ...prevState.device,
        max_retry_reached: false,
        retry_count: 0,
      },
    }));
    this.props.resetRetry(this.state.device.serial_number);
  };
  renderCiscoDevice = () => {
    const deviceDetails = this.state.device;

    return (
      <div className="device-details-modal__body">
        <div className="device-details-modal__body-field">
          <label>Serial #</label>
          <label>
            {deviceDetails.serial_number
              ? deviceDetails.serial_number
              : "Unknown"}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Contract</label>
          <label>
            {deviceDetails.service_contract_number
              ? deviceDetails.service_contract_number
              : deviceDetails.expiration_date
              ? "Unknown"
              : "Not Covered"}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>manufacturer</label>
          <label>
            {deviceDetails.manufacturer_name
              ? deviceDetails.manufacturer_name
              : "Unknown"}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Model</label>
          <label>
            {deviceDetails.model_number
              ? deviceDetails.model_number
              : "Unknown"}
          </label>
        </div>
        <div className="device-details-modal__body-field ">
          <label>Contract Status</label>
          <label>
            {deviceDetails.contract_status !== "N.A." ? (
              deviceDetails.contract_status
            ) : deviceDetails.expiration_date ? (
              deviceDetails.expiration_date
            ) : (
              <span className="contract-status-no-covered">Not Covered</span>
            )}
          </label>
        </div>
        <div className="device-details-modal__body-field details-name">
          <label>Device Name</label>
          <label>
            {this.state.device.device_name
              ? this.state.device.device_name
              : "Unknown"}
          </label>
        </div>
        <div className="device-details-modal__body-field details-location">
          <label>Location / Site</label>
          <label>
            {this.state.device.name ? this.state.device.name : "Not selected"}
          </label>
        </div>
        <div className="device-details-modal__body-field details-notes">
          <label>Description</label>
          <label className="note-description">
            {this.state.device.item_description
              ? this.state.device.item_description
              : "Unknown"}
          </label>
        </div>
        {this.props.userType === "provider" && (
          <div className="device-details-modal__body-field details-notes">
            <label> Internal Notes</label>
            <label className="note-description">
              {this.state.device.notes ? this.state.device.notes : "Unknown"}
            </label>
          </div>
        )}

        <div className="device-details-modal__body-field">
          <label>
            {" "}
            {`${this.props.userType === "provider" ? "Customer" : ""} `}Notes
          </label>
          <label className="note-description">
            {deviceDetails.customer_notes ? deviceDetails.customer_notes : ""}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Asset Tag</label>
          <label>
            {deviceDetails.asset_tag ? deviceDetails.asset_tag : ""}
          </label>
        </div>
      </div>
    );
  };

  renderCiscoDeviceDates = () => {
    const deviceDetails = this.state.device;

    return (
      <>
        <div className="device-detail__header">
          <h5>Device Details</h5>
        </div>
        <div className="device-details-modal__body">
          <div className="device-details-modal__body-field">
            <label>Is Managed</label>
            <label>
              {_.get(deviceDetails, "monitoring_data.is_managed", false)
                ? "Yes"
                : "No"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>OS version</label>
            <label className="note-description">
              {_.get(deviceDetails, "monitoring_data.is_managed")
                ? _.get(
                    deviceDetails,
                    `monitoring_data.${
                      _.get(
                        deviceDetails,
                        `monitoring_data.solarwinds.os_version`,
                        false
                      )
                        ? "solarwinds"
                        : "logicmonitor"
                    }.os_version`
                  )
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>Purchased</label>
            <label>
              {deviceDetails.purchase_date
                ? toFormattedDate(deviceDetails.purchase_date)
                : "Unknown"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>End of Service attach</label>
            <label>
              {deviceDetails.EOS_date
                ? toFormattedDate(deviceDetails.EOSA_date)
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>last day of support</label>
            <label>
              {deviceDetails.LDOS_date
                ? toFormattedDate(deviceDetails.LDOS_date)
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>End Of Life</label>
            <label>
              {deviceDetails.EOL_date
                ? toFormattedDate(deviceDetails.EOL_date)
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>end of sale </label>
            <label>
              {deviceDetails.EOS_date
                ? toFormattedDate(deviceDetails.EOS_date)
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>last day of sw support</label>
            <label>
              {deviceDetails.EOSWS_date
                ? toFormattedDate(deviceDetails.EOSWS_date)
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>end of security update</label>
            <label>
              {deviceDetails.EOSU_date
                ? toFormattedDate(deviceDetails.EOSU_date)
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>Expiration</label>
            <label className="label-expiration-date">
              {deviceDetails.expiration_date
                ? toFormattedDate(deviceDetails.expiration_date)
                : this.state.device.updated_on
                ? "Not Available"
                : "Not Covered"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>Contract Start Date</label>
            <label>
              {deviceDetails.installation_date
                ? toFormattedDate(deviceDetails.installation_date)
                : "Not Available"}
            </label>
          </div>
        </div>
      </>
    );
  };

  getCollapsableCiscoDevice = () => {
    return (
      <div className="device-fields col-md-12 row">
        <div
          className={`field ${
            this.state.openSoftware ? "field--collapsed" : ""
          }`}
          key={6}
        >
          <div
            className={`section-heading
                    ${
                      !this.state.device.release_date &&
                      !this.state.device.image_name &&
                      !this.state.device.suggested_software_update &&
                      !this.state.device.image_download_url
                        ? "disable-heading"
                        : ""
                    }`}
            onClick={() =>
              this.setState({
                openSoftware: !this.state.openSoftware,
              })
            }
          >
            Software suggestion Info
            {!this.state.device.release_date &&
            !this.state.device.image_name &&
            !this.state.device.suggested_software_update &&
            !this.state.device.image_download_url ? (
              ""
            ) : (
              <div className="action-collapse">
                {!this.state.openSoftware ? "+" : "-"}
              </div>
            )}
          </div>
          {this.state.openSoftware && (
            <div className="body-section">
              <div
                className={
                  this.state.device.image_download_url
                    ? "migration-info-tab d-pointer"
                    : "migration-info-tab"
                }
                onClick={(e) =>
                  this.props.openURL(e, this.state.device.image_download_url)
                }
              >
                <label className="heading">
                  {this.state.device.image_download_url && (
                    <label
                      className={
                        this.state.device.image_download_url
                          ? "d-pointer"
                          : "disable-div"
                      }
                    >
                      <img
                        alt=""
                        className={
                          this.state.device.image_download_url
                            ? "d-pointer migration-info"
                            : "disable-div migration-info"
                        }
                        src="/assets/new-icons/info.svg"
                      />
                      View Details
                    </label>
                  )}
                </label>
                {this.state.device.image_download_url && (
                  <div className="device-details-modal__body-field" />
                )}
                <div className="device-details-modal__body">
                  {this.state.device.release_version && (
                    <div className="device-details-modal__body-field">
                      <label>Release Version</label>
                      <label>{this.state.device.release_version}</label>
                    </div>
                  )}
                  {this.state.device.release_date && (
                    <div className="device-details-modal__body-field">
                      <label>Release Date</label>
                      <label>{this.state.device.release_date}</label>
                    </div>
                  )}
                  {this.state.device.suggested_software_update && (
                    <div
                      className="
                          device-details-modal__body-field"
                    >
                      <label>Image Name</label>
                      <label className="info">
                        {this.state.device.suggested_software_update}
                      </label>
                    </div>
                  )}
                </div>
              </div>
              {!this.state.device.release_date &&
              !this.state.device.image_name &&
              !this.state.device.suggested_software_update &&
              !this.state.device.image_download_url ? (
                <label>No software suggestion info. available.</label>
              ) : (
                ""
              )}
            </div>
          )}
        </div>
        <div
          className={`field ${
            this.state.openMigrationDetails ? "field--collapsed" : ""
          }`}
          key={1}
        >
          <div
            className={`section-heading
                ${
                  _.get(this.state.device, "monitoring_data", false)
                    ? ""
                    : "disable-heading"
                }`}
            onClick={() =>
              this.setState({
                openMigrationDetails: !this.state.openMigrationDetails,
              })
            }
          >
            Monitoring Details
            {_.get(this.state.device, "monitoring_data", false) ? (
              <div className="action-collapse">
                {!this.state.openMigrationDetails ? "+" : "-"}
              </div>
            ) : (
              ""
            )}
          </div>
          {this.state.openMigrationDetails && (
            <div className="body-section">
              <div className={"migration-info-tab"}>
                <div className="device-details-modal__body">
                  <div className="device-details-modal__body-field">
                    <label>created by</label>
                    <label>
                      {_.get(
                        this.state.device,
                        "monitoring_data.created_by",
                        "-"
                      )}
                    </label>
                  </div>
                  <div className="device-details-modal__body-field">
                    <label>last updated by</label>
                    <label>
                      {_.get(
                        this.state.device,
                        "monitoring_data.last_updated_by",
                        "-"
                      )}
                    </label>
                  </div>
                  <div className="device-details-modal__body-field">
                    <label>updated on</label>
                    <label>
                      {fromISOStringToDateTimeString(
                        _.get(
                          this.state.device,
                          `monitoring_data.${_.get(
                            this.state.device,
                            `monitoring_data.last_updated_by`
                          )}.updated_on`
                        )
                      )}
                    </label>
                  </div>
                </div>

                <table className="monitoring-data-table">
                  <tbody>
                    <tr>
                      <th></th>
                      <th>Solarwinds</th>
                      <th>logic Monitor</th>
                    </tr>
                    <tr>
                      <th>System name</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.sys_name",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.sys_name",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>IP</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.host_name",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.host_name",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>OS version</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.os_version",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.os_version",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>Host name</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.device_name",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.device_name",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>Updated on</th>
                      <td>
                        {fromISOStringToDateTimeString(
                          _.get(
                            this.state.device,
                            "monitoring_data.solarwinds.updated_on"
                          )
                        )}
                      </td>
                      <td>
                        {fromISOStringToDateTimeString(
                          _.get(
                            this.state.device,
                            "monitoring_data.logicmonitor.updated_on"
                          )
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>Uptime</th>
                      <td>
                        {fromSecondsToReadable(
                          _.get(
                            this.state.device,
                            "monitoring_data.solarwinds.up_time"
                          )
                        )}
                      </td>
                      <td>
                        {fromSecondsToReadable(
                          _.get(
                            this.state.device,
                            "monitoring_data.logicmonitor.up_time"
                          )
                        )}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          )}
        </div>
        <div
          className={`field ${
            this.state.openMigrationInfo ? "field--collapsed" : ""
          }`}
          key={2}
        >
          <div
            className={`section-heading
                    ${
                      !this.state.device.product_bulletin_url &&
                      !this.state.device.migration_product_name &&
                      !this.state.device.migration_product_id &&
                      !this.state.device.migration_info
                        ? "disable-heading"
                        : ""
                    }`}
            onClick={() =>
              this.setState({
                openMigrationInfo: !this.state.openMigrationInfo,
              })
            }
          >
            Migration Info
            {!this.state.device.product_bulletin_url &&
            !this.state.device.migration_product_name &&
            !this.state.device.migration_product_id &&
            !this.state.device.migration_info ? (
              ""
            ) : (
              <div className="action-collapse">
                {!this.state.openMigrationInfo ? "+" : "-"}
              </div>
            )}
          </div>
          {this.state.openMigrationInfo && (
            <div className="body-section">
              <div
                className={
                  this.state.device.product_bulletin_url
                    ? "migration-info-tab d-pointer"
                    : "migration-info-tab"
                }
                onClick={(e) =>
                  this.props.openURL(e, this.state.device.product_bulletin_url)
                }
              >
                <label className="heading">
                  {this.state.device.product_bulletin_url && (
                    <label
                      className={
                        this.state.device.product_bulletin_url
                          ? "d-pointer"
                          : "disable-div"
                      }
                    >
                      <img
                        alt=""
                        className={
                          this.state.device.product_bulletin_url
                            ? "d-pointer migration-info"
                            : "disable-div migration-info"
                        }
                        src="/assets/new-icons/info.svg"
                      />
                      View Details
                    </label>
                  )}
                </label>
                {this.state.device.product_bulletin_url && (
                  <div className="device-details-modal__body-field" />
                )}
                <div className="device-details-modal__body">
                  {this.state.device.migration_product_name && (
                    <div className="device-details-modal__body-field">
                      <label>Name</label>
                      <label>{this.state.device.migration_product_name}</label>
                    </div>
                  )}
                  {this.state.device.migration_product_id && (
                    <div className="device-details-modal__body-field">
                      <label> Product Id</label>
                      <label>{this.state.device.migration_product_id}</label>
                    </div>
                  )}
                  {this.state.device.migration_info && (
                    <div
                      className="
                          device-details-modal__body-field"
                    >
                      <label>Info</label>
                      <label className="info">
                        {this.state.device.migration_info}
                      </label>
                    </div>
                  )}
                </div>
              </div>
              {!this.state.device.product_bulletin_url &&
              !this.state.device.migration_product_name &&
              !this.state.device.migration_product_id &&
              !this.state.device.migration_info ? (
                <label>No migration info. available.</label>
              ) : (
                ""
              )}
            </div>
          )}
        </div>
        <div
          className={`field ${this.state.open ? "field--collapsed" : ""}`}
          key={3}
        >
          <div
            className={`section-heading
                 ${
                   this.state.device &&
                   this.state.device.renewal_history_is_present
                     ? ""
                     : "disable-heading"
                 }`}
            onClick={() =>
              this.setState({
                open: !this.state.open,
              })
            }
          >
            Renewal History
            {this.state.device &&
            this.state.device.renewal_history_is_present ? (
              <div className="action-collapse">
                {!this.state.open ? "+" : "-"}
              </div>
            ) : (
              " "
            )}
          </div>{" "}
          {this.state.open && (
            <div className="body-section">
              <RenewalHistory {...this.props} deviceId={this.state.device.id} />
            </div>
          )}
        </div>
        <div
          className={`field ${
            this.state.openServiceInfo ? "field--collapsed" : ""
          }`}
          key={4}
        >
          <div
            className={`section-heading
                 ${
                   this.state.device.service_sku ||
                   this.state.device.service_level ||
                   this.state.device.service_description
                     ? ""
                     : "disable-heading"
                 }`}
            onClick={() =>
              this.setState({
                openServiceInfo: !this.state.openServiceInfo,
              })
            }
          >
            Service Info
            {(this.state.device.service_sku ||
              this.state.device.service_level ||
              this.state.device.service_description) && (
              <div className="action-collapse">
                {!this.state.openServiceInfo ? "+" : "-"}
              </div>
            )}
          </div>
          {this.state.openServiceInfo &&
            (this.state.device.service_sku ||
              this.state.device.service_level ||
              this.state.device.service_description) && (
              <div className="body-section">
                <div className="device-details-circuit-info">
                  <div className="document">
                    <div
                      className="
                                    tile"
                    >
                      <label> SKU</label>
                      <label>
                        {this.state.device.service_sku
                          ? this.state.device.service_sku
                          : "N.A."}
                      </label>
                    </div>
                    <div
                      className="
                                    tile"
                    >
                      <label> LEVEL</label>
                      <label>
                        {this.state.device.service_level
                          ? this.state.device.service_level
                          : "N.A."}
                      </label>
                    </div>
                    <div
                      className="
                                    tile"
                    >
                      <label> Description</label>
                      <label>
                        {this.state.device.service_description
                          ? this.state.device.service_description
                          : "N.A."}
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            )}
          {this.state.openServiceInfo &&
            !this.state.device.service_sku &&
            !this.state.device.service_level &&
            !this.state.device.service_description && (
              <div className="body-section"> No service info. available. </div>
            )}
        </div>
        <div
          className={`field ${
            this.state.openCircuitInfo ? "field--collapsed" : ""
          }`}
          key={5}
        >
          <div
            className={`section-heading
                          ${
                            !this.props.existingAssociationList ||
                            (this.props.existingAssociationList &&
                              this.props.existingAssociationList.length === 0)
                              ? "disable-heading"
                              : ""
                          }`}
            onClick={() =>
              this.setState({
                openCircuitInfo: !this.state.openCircuitInfo,
              })
            }
          >
            Circuit Info
            {!this.props.existingAssociationList ||
            (this.props.existingAssociationList &&
              this.props.existingAssociationList.length === 0) ? (
              ""
            ) : (
              <div className="action-collapse">
                {!this.state.openCircuitInfo ? "+" : "-"}
              </div>
            )}
          </div>
          {this.state.openCircuitInfo && (
            <div className="body-section">
              <div className="device-details-circuit-info">
                {this.props.existingAssociationList &&
                  this.props.existingAssociationList.length > 0 && (
                    <div className="document">
                      <div className="tile-index">
                        <label> #</label>
                      </div>
                      <div className="tile">
                        <label> circuit Id</label>
                      </div>
                      <div className="tile">
                        <label> provider</label>
                      </div>
                      <div className="tile">
                        <label> circuit type</label>
                      </div>
                      <div className="tile">
                        <label> Phone</label>
                      </div>
                    </div>
                  )}
                {this.props.existingAssociationList &&
                this.props.existingAssociationList.length > 0
                  ? this.props.existingAssociationList.map(
                      (key, fieldIndex) => {
                        return (
                          <div className="document" key={fieldIndex}>
                            <div className="tile-index">
                              <label />
                              <label>{fieldIndex + 1}</label>
                            </div>
                            <div className="tile">
                              <label />
                              <label>
                                {key.circuit_id ? key.circuit_id : "N.A."}
                              </label>
                            </div>
                            <div className="tile">
                              <label />
                              <label>
                                {key.provider ? key.provider : "N.A."}
                              </label>
                            </div>
                            <div className="tile">
                              <label />
                              <label>
                                {key.circuit_type ? key.circuit_type : "N.A."}
                              </label>
                            </div>
                            <div
                              className="
                                    tile"
                            >
                              <label />
                              <label>
                                {phoneNumberInFormat("", key.contact_phone)}
                              </label>
                            </div>
                          </div>
                        );
                      }
                    )
                  : "No circuits are associated."}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };

  getTitle = () => {
    return (
      <div className="device-details">
        View Device Details
        <div className="top-right">
          <div className={`status--${this.state.device.status}`}>
            {this.state.device.status === "Active"
              ? "In Service "
              : "Decommissioned"}
          </div>
          {this.state.device.decommissioned_on ? (
            <div className="decommissioned-on-date">
              {toFormattedDate(
                this.state.device.decommissioned_on
              )}
            </div>
          ) : (
            ""
          )}
        </div>
      </div>
    );
  };

  renderNonCiscoDevice = () => {
    const deviceDetails = this.state.device;

    return (
      <div className="device-details-modal__body">
        <div className="device-details-modal__body-field">
          <label>Serial #</label>
          <label>
            {deviceDetails.serial_number
              ? deviceDetails.serial_number
              : "Unknown"}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>manufacturer</label>
          <label>{_.get(deviceDetails, "manufacturer_name", "Unknown")}</label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Model</label>
          <label>
            {_.get(
              deviceDetails,
              "monitoring_data.logicmonitor.model",
              "Unknown"
            )}
          </label>
        </div>
        <div className="device-details-modal__body-field details-name">
          <label>Device Name</label>
          <label>{_.get(deviceDetails, "device_name", "Unknown")}</label>
        </div>
        <div className="device-details-modal__body-field details-notes">
          <label>Description</label>
          <label className="note-description">
            {_.get(
              deviceDetails,
              "monitoring_data.logicmonitor.description",
              "Unknown"
            )}
          </label>
        </div>
        <div className="device-details-modal__body-field details-location">
          <label>Location / Site</label>
          <label>
            {this.state.device.name ? this.state.device.name : "Not selected"}
          </label>
        </div>
        <div className="device-details-modal__body-field">
          <label>Asset Tag</label>
          <label>
            {deviceDetails.asset_tag ? deviceDetails.asset_tag : ""}
          </label>
        </div>
        {this.props.userType === "provider" && (
          <div className="device-details-modal__body-field details-notes">
            <label> Internal Notes</label>
            <label className="note-description">
              {this.state.device.notes ? this.state.device.notes : "Unknown"}
            </label>
          </div>
        )}

        <div className="device-details-modal__body-field">
          <label>
            {" "}
            {`${this.props.userType === "provider" ? "Customer" : ""} `}Notes
          </label>
          <label className="note-description">
            {deviceDetails.customer_notes ? deviceDetails.customer_notes : ""}
          </label>
        </div>
      </div>
    );
  };

  renderNonCiscoDeviceDates = () => {
    const deviceDetails = this.state.device;

    return (
      <>
        <div className="device-details-modal__body"></div>
        <div className="device-detail__header">
          <h5>Support Information</h5>
        </div>
        <div className="device-details-modal__body">
          <div className="device-details-modal__body-field">
            <label>Is Managed</label>
            <label>
              {_.get(deviceDetails, "monitoring_data.is_managed", false)
                ? "Yes"
                : "No"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>OS version</label>
            <label className="note-description">
              {_.get(deviceDetails, "monitoring_data.is_managed")
                ? _.get(
                    deviceDetails,
                    `monitoring_data.logicmonitor.os_version`
                  )
                : this.state.device.updated_on
                ? "Not Announced"
                : "Not Available"}
            </label>
          </div>
          <div className="device-details-modal__body-field">
            <label>Purchased</label>
            <label>
              {deviceDetails.purchase_date
                ? toFormattedDate(deviceDetails.purchase_date)
                : "Unknown"}
            </label>
          </div>
        </div>
      </>
    );
  };

  getCollapsableNonCiscoDevice = () => {
    return (
      <div className="device-fields col-md-12 row">
        <div
          className={`field ${
            this.state.openMigrationDetails ? "field--collapsed" : ""
          }`}
          key={1}
        >
          <div
            className={`section-heading
                ${
                  _.get(this.state.device, "monitoring_data", false)
                    ? ""
                    : "disable-heading"
                }`}
            onClick={() =>
              this.setState({
                openMigrationDetails: !this.state.openMigrationDetails,
              })
            }
          >
            Monitoring Details
            {_.get(this.state.device, "monitoring_data", false) ? (
              <div className="action-collapse">
                {!this.state.openMigrationDetails ? "+" : "-"}
              </div>
            ) : (
              ""
            )}
          </div>
          {this.state.openMigrationDetails && (
            <div className="body-section">
              <div className={"migration-info-tab"}>
                <div className="device-details-modal__body">
                  <div className="device-details-modal__body-field">
                    <label>created by</label>
                    <label>
                      {_.get(
                        this.state.device,
                        "monitoring_data.created_by",
                        "-"
                      )}
                    </label>
                  </div>
                  <div className="device-details-modal__body-field">
                    <label>last updated by</label>
                    <label>
                      {_.get(
                        this.state.device,
                        "monitoring_data.last_updated_by",
                        "-"
                      )}
                    </label>
                  </div>
                  <div className="device-details-modal__body-field">
                    <label>updated on</label>
                    <label>
                      {fromISOStringToDateTimeString(
                        _.get(
                          this.state.device,
                          `monitoring_data.${_.get(
                            this.state.device,
                            `monitoring_data.last_updated_by`
                          )}.updated_on`
                        )
                      )}
                    </label>
                  </div>
                </div>

                <table className="monitoring-data-table">
                  <tbody>
                    <tr>
                      <th></th>
                      <th>Solarwinds</th>
                      <th>logic Monitor</th>
                    </tr>
                    <tr>
                      <th>System name</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.sys_name",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.sys_name",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>IP</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.host_name",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.host_name",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>OS version</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.os_version",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.os_version",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>Host name</th>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.solarwinds.device_name",
                          "-"
                        )}
                      </td>
                      <td>
                        {_.get(
                          this.state.device,
                          "monitoring_data.logicmonitor.device_name",
                          "-"
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>Updated on</th>
                      <td>
                        {fromISOStringToDateTimeString(
                          _.get(
                            this.state.device,
                            "monitoring_data.solarwinds.updated_on"
                          )
                        )}
                      </td>
                      <td>
                        {fromISOStringToDateTimeString(
                          _.get(
                            this.state.device,
                            "monitoring_data.logicmonitor.updated_on"
                          )
                        )}
                      </td>
                    </tr>
                    <tr>
                      <th>Uptime</th>
                      <td>
                        {fromSecondsToReadable(
                          _.get(
                            this.state.device,
                            "monitoring_data.solarwinds.up_time"
                          )
                        )}
                      </td>
                      <td>
                        {fromSecondsToReadable(
                          _.get(
                            this.state.device,
                            "monitoring_data.logicmonitor.up_time"
                          )
                        )}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          )}
        </div>

        <div
          className={`field ${
            this.state.openCircuitInfo ? "field--collapsed" : ""
          }`}
          key={5}
        >
          <div
            className={`section-heading
                          ${
                            !this.props.existingAssociationList ||
                            (this.props.existingAssociationList &&
                              this.props.existingAssociationList.length === 0)
                              ? "disable-heading"
                              : ""
                          }`}
            onClick={() =>
              this.setState({
                openCircuitInfo: !this.state.openCircuitInfo,
              })
            }
          >
            Circuit Info
            {!this.props.existingAssociationList ||
            (this.props.existingAssociationList &&
              this.props.existingAssociationList.length === 0) ? (
              ""
            ) : (
              <div className="action-collapse">
                {!this.state.openCircuitInfo ? "+" : "-"}
              </div>
            )}
          </div>
          {this.state.openCircuitInfo && (
            <div className="body-section">
              <div className="device-details-circuit-info">
                {this.props.existingAssociationList &&
                  this.props.existingAssociationList.length > 0 && (
                    <div className="document">
                      <div className="tile-index">
                        <label> #</label>
                      </div>
                      <div className="tile">
                        <label> circuit Id</label>
                      </div>
                      <div className="tile">
                        <label> provider</label>
                      </div>
                      <div className="tile">
                        <label> circuit type</label>
                      </div>
                      <div className="tile">
                        <label> Phone</label>
                      </div>
                    </div>
                  )}
                {this.props.existingAssociationList &&
                this.props.existingAssociationList.length > 0
                  ? this.props.existingAssociationList.map(
                      (key, fieldIndex) => {
                        return (
                          <div className="document" key={fieldIndex}>
                            <div className="tile-index">
                              <label />
                              <label>{fieldIndex + 1}</label>
                            </div>
                            <div className="tile">
                              <label />
                              <label>
                                {key.circuit_id ? key.circuit_id : "N.A."}
                              </label>
                            </div>
                            <div className="tile">
                              <label />
                              <label>
                                {key.provider ? key.provider : "N.A."}
                              </label>
                            </div>
                            <div className="tile">
                              <label />
                              <label>
                                {key.circuit_type ? key.circuit_type : "N.A."}
                              </label>
                            </div>
                            <div
                              className="
                                    tile"
                            >
                              <label />
                              <label>
                                {phoneNumberInFormat("", key.contact_phone)}
                              </label>
                            </div>
                          </div>
                        );
                      }
                    )
                  : "No circuits are associated."}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-device__footer
        ${this.props.isFetching ? `loading` : ""}`}
      >
        <div className="updated-on">
          Last Automated Update :{" "}
          {this.state.device.updated_on ? (
            fromISOStringToDateTimeString(this.state.device.updated_on)
          ) : this.state.device.is_serial_number_invalid ? (
            "Serial number is Invalid"
          ) : this.state.device.retry_count === -1 ? (
            "Serial number is Invalid"
          ) : this.state.device.retry_count > 0 &&
            this.state.device.max_retry_reached === false ? (
            "API call made but not able to get data"
          ) : this.state.device.retry_count > 0 &&
            this.state.device.max_retry_reached === true ? (
            <div>
              <div>Max retry limit reached for manufacturer data API.</div>
              <p>
                {" "}
                click here to{" "}
                <SquareButton
                  onClick={() => this.resetRetry()}
                  content="Reset"
                  bsStyle={"default"}
                  className="reset-retry-count"
                />
              </p>
            </div>
          ) : (
            "data fetching is IN PROGESS"
          )}
        </div>
        <SquareButton
          onClick={this.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  renderCiscoDeviceDetails = () => {
    return (
      <>
        {this.renderCiscoDevice()}
        {this.renderCiscoDeviceDates()}
        {this.getCollapsableCiscoDevice()}
      </>
    );
  };
  renderNonCiscoDeviceDetails = () => {
    return (
      <>
        {this.renderNonCiscoDevice()}
        {this.renderNonCiscoDeviceDates()}
        {this.getCollapsableNonCiscoDevice()}
      </>
    );
  };
  getBody = () => {
    return (
      <div className="edit-device">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        {!this.state.device.non_cisco && this.renderCiscoDeviceDetails()}
        {this.state.device.non_cisco === true &&
          this.renderNonCiscoDeviceDetails()}
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-device"
      />
    );
  }
}
