import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";
import { addErrorMessage } from "../../actions/appState";
import {
  bulkSiteCreate,
  BULK_SITE_SUCCESS,
  fetchCountriesAndStates,
  fetchSites
} from "../../actions/inventory";
import SquareButton from "../../components/Button/button";
import DeleteButton from "../../components/Button/deleteButton";
import EditButton from "../../components/Button/editButton";
import Checkbox from "../../components/Checkbox/checkbox";
import SelectInput from "../../components/Input/Select/select";
import RightMenu from "../../components/RighMenuBase/rightMenuBase";
import Spinner from "../../components/Spinner";
import Table from "../../components/Table/table";
import CreateSite from "../../containers/CreateNew/createSite";
import { commonFunctions } from "../../utils/commonFunctions";
import "./style.scss";

const fields: any[] = [
  {
    label: "Name",
    value: "",
    db_name: "name",
    isRequired: true,
    isDate: false,
    isCheckBox: false,
    id: 1,
  },
  {
    label: "Address Line 1",
    db_name: "address_line_1",
    value: "",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
    id: 2,
  },
  {
    label: "Address Line 2",
    db_name: "address_line_2",
    value: "",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
    id: 3,
  },
  {
    label: "Phone Number",
    value: "",
    db_name: "phone_number",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
    id: 4,
  },
  {
    label: "Country",
    value: "",
    db_name: "country_id",
    isRequired: true,
    isDate: false,
    isCheckBox: false,
    id: 5,
  },
  {
    label: "State",
    value: "",
    db_name: "state",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
    id: 6,
  },
  {
    label: "City",
    db_name: "city",
    value: "",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
    id: 7,
  },
  {
    label: "Zip Code",
    db_name: "zip",
    value: "",
    isRequired: false,
    isDate: false,
    isCheckBox: false,
    id: 8,
  },
];

interface ISiteMappingProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: any;
  xlsxData: any;
  searchQueryAddress: any;
  addressList: any;
  addressFieldSelected: any;
  bulkSiteCreate: any;
  customerId: any;
  fetchSites: (customerId: number) => any;
  fetchCountriesAndStates: any;
  countries: any;
  addErrorMessage: any;
  directImport: boolean;
}
interface ISiteMappingState {
  openSections: any;
  xlsxData: any;
  checkboxList: any;
  errors: string[];
  siteFields: any[];
  columns: any[];
  siteList: any[];
  openSiteUpdateModal: boolean;
  loading: boolean;
  openSiteUpdate: boolean;
  site?: ISite;
  index: number;
  error: any;
  addressList: any;
}
const sections = {
  mapping: true,
  list: false,
};
class SiteMapping extends React.Component<
  ISiteMappingProps,
  ISiteMappingState
> {
  constructor(props: ISiteMappingProps) {
    super(props);

    this.state = {
      openSections: sections,
      xlsxData: null,
      checkboxList: [
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
      ],
      errors: [],
      siteFields: fields,
      columns: [],
      siteList: [],
      loading: false,
      openSiteUpdateModal: false,
      openSiteUpdate: false,
      index: null,
      error: null,
      addressList:[],
    };
  }

  componentDidMount() {
    if (this.props.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
    this.props.fetchCountriesAndStates();
    this.props.fetchSites(this.props.customerId);
  }

  setColumns = (nextProps: ISiteMappingProps) => {
    const xlsxDataResponce = nextProps.xlsxData;
    const columns = [];
    let index = 0;
    let tempKey = "";
    for (const key in xlsxDataResponce[0]) {
      if (xlsxDataResponce[0].hasOwnProperty(key)) {
        if (key.includes("EMPTY")) {
          tempKey = `column_${index + 1}`;
        }
        columns.push({ key: tempKey !== "" ? tempKey : key, value: key });
        index++;
        tempKey = "";
      }
    }
    this.setState({ columns });
  };
  onClose = (event?: any) => {
    this.props.onClose(event);
  };

  onChecboxChanged = (event: any, index?: any) => {};

  handleChangeField = (event: any, index?: any) => {
    const newState = cloneDeep(this.state);
    newState.siteFields[index].value = event.target.value;
    this.setState({
      siteFields: newState.siteFields,
    });
  };

  getFieldsOptions = () => {
    if (this.state.columns.length > 0) {
      return this.state.columns.map((role) => ({
        value: role.value,
        label: role.key,
      }));
    } else {
      return [];
    }
  };

  getSitMapping = () => {
    return (
      <div className="field-mapping sites-mapping">
        <h3>Site Mapping</h3>
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-row" />
            <tr className="field-mapping__table-header">
              <th className="first">Map Field</th>
              <th className="middle" />
              <th className="last"> Sheet Columns </th>
              <th className="" />
            </tr>

            {this.state.siteFields.map((field, index) => {
              return (
                <tr key={index} className={`field-mapping__table-row`}>
                  {field.isCheckBox && (
                    <td className="first">
                      <Checkbox
                        isChecked={this.state.checkboxList[index]}
                        name="option"
                        onChange={(e) => this.onChecboxChanged(e, index)}
                      >
                        {field.header}
                      </Checkbox>
                    </td>
                  )}

                  <td className="first">
                    {((field.isCheckBox && this.state.checkboxList[index]) ||
                      !field.isCheckBox) &&
                      field.label}

                    {field.message ? (
                      <div className="field-mapping__label-message">
                        {field.message}
                      </div>
                    ) : null}
                    {field.isRequired && (
                      <span className="field-mapping__label-required" />
                    )}
                    {field.isCheckBox && this.state.checkboxList[index] && (
                      <img
                        className="opposite-arrow"
                        src="/assets/new-icons/oppositearrow.svg"
                      />
                    )}
                    {field.message ? (
                      <div className="field-mapping__label-message">
                        {field.message}
                      </div>
                    ) : null}
                    {this.state.errors[index] ? (
                      <span className="field-mapping__label-error">
                        {this.state.errors[index]}
                      </span>
                    ) : null}
                  </td>
                  {!field.isCheckBox && (
                    <td className="middle">
                      <img src="/assets/icons/oppositearrow.svg" />
                    </td>
                  )}
                  {((field.isCheckBox && this.state.checkboxList[index]) ||
                    !field.isCheckBox) && (
                    <td className="sheet-column">
                      <SelectInput
                        name="select"
                        value={field.value}
                        onChange={(event) =>
                          this.handleChangeField(event, index)
                        }
                        options={this.getFieldsOptions()}
                        searchable={true}
                        placeholder="Select field"
                        clearable={true}
                      />
                    </td>
                  )}
                  <th className="" />
                  <th className="" />
                  <th className="" />
                </tr>
              );
            })}
          </tbody>
        </table>

        <div className="field-mapping__footer">
          <SquareButton
            onClick={this.onClose}
            content="Cancel"
            bsStyle={"default"}
            className="import-button-1"
          />
          <SquareButton
            onClick={this.onClickImport}
            content="Next"
            bsStyle={"primary"}
            className="import-button-1"
          />
        </div>
      </div>
    );
  };

  validateForm = () => {
    let isValid = true;
    const newState = cloneDeep(this.state);
    newState.siteFields.map((field, index) => {
      newState.errors[index] = "";
    });

    newState.siteFields.map((field, index) => {
      const inValidField = !this.state.columns
        .map((f) => f.value)
        .includes(this.state.siteFields[index].value);
      if (
        field.isRequired &&
        (!this.state.siteFields[index].value || inValidField)
      ) {
        newState.errors[index] = `${field.label} is required`;
        isValid = false;
      }
    });

    this.setState(newState);

    return isValid;
  };

  onClickImport = (event: any) => {
    if (this.validateForm()) {
      const addressList = this.props.directImport ? this.getAddressList() : this.props.addressList;
      let addresses = [];
      addressList.map((add, index) => {
        addresses.push({ name: add, id: this.props.searchQueryAddress[index] });
      });
      const siteList = [];
      addresses
        .filter((x) => !x.id)
        .map((add) => {
          siteList.push(this.getSiteObjectByName(add.name));
        });
      this.setState({ siteList }, () => {
        this.toggleMappingOpen("list");
      });
    }
  };

  getAddressList = () => { 
    const xlsxDataResponce = this.state.xlsxData;
    const addressList = [];
    xlsxDataResponce.map(data => {
      addressList.push(
        data[this.state.siteFields[0].value] &&
          data[this.state.siteFields[0].value] !== '-'
          ? data[this.state.siteFields[0].value]
          : 'N.A.'
      );
    });
    const onlyUniqueAddress = addressList.filter(this.onlyUnique);
 
    return onlyUniqueAddress;
  };
 
  onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  toggleMappingOpen = (field) => {
    const openSections = [];
    Object.keys(this.state.openSections).map((f, index) => {
      openSections[f] = false;
    });
    openSections[field] = true;
    this.setState({ openSections });
  };
  getSiteObjectByName = (name) => {
    const data = this.props.xlsxData.filter(
      (data) => data[this.props.directImport ?  this.state.siteFields[0].value : this.props.addressFieldSelected] === name
    )[0];
    const countryN = this.getDataFromFileObject(data,"country_id")
    const stateN = this.getDataFromFileObject(data,"state")
    const countryStateInfo = countryN && this.getCountryStateByName(countryN,stateN);
    const obj = {
      name: this.getDataFromFileObject( data, "name"),
      address_line_1: this.getDataFromFileObject( data, "address_line_1"),
      address_line_2: this.getDataFromFileObject( data, "address_line_2"),
      city: this.getDataFromFileObject( data, "city"),
      phone_number: this.getDataFromFileObject( data, "phone_number"),
      zip: this.getDataFromFileObject( data, "zip"),
      state: countryStateInfo && countryStateInfo.state_id,
      state_id: countryStateInfo && countryStateInfo.state_id || null,
      country_id: countryStateInfo && countryStateInfo.country_id || null,
    };
    return obj;
  };
  getDataFromFileObject =(data, field)=>{
    const fName = this.getMappedFieldName(field);
    const result = data && data[fName];
    return result;
  }
  getCountryStateByName = (country, state) => {
    const countryData = this.props.countries.filter(
      (x) => x.country.toLowerCase() === country.toLowerCase()
    )[0];
    const data =
      countryData && state && 
      countryData.states.filter(
        (s) => s && s.state.toLowerCase() === state.toLowerCase()
      )[0];
    return data || { country_id: countryData ? countryData.country_id : "" };
  };

  getCountryStateById = (country_id, state_id) => {
    const countryData = this.props.countries.filter(
      (x) => x.country_id === country_id
    )[0];
    const data =
      countryData &&
      countryData.states.filter((s) => s && s.state_id === state_id)[0];
    return `${countryData ? countryData.country : ""} ${
      data ? `,${data.state}` : ""
    }`;
  };

  getMappedFieldName = (name) => {
    return this.state.siteFields.filter((x) => x.db_name === name)[0].value;
  };
  onClickCreateSite = () => {
    this.setState({ loading: true });
    this.props
      .bulkSiteCreate(this.props.customerId, this.state.siteList)
      .then((action) => {
        if (action.type === BULK_SITE_SUCCESS) {
          this.props.fetchSites(this.props.customerId);
          this.onClose();
        } else {
          this.props.addErrorMessage("Error occured.");
          this.setState({ error: action.errorList && action.errorList.data });
        }
        this.setState({ loading: false });
      });
  };

  getSiteListUI = () => {
    const columns: any = [
      {
        accessor: "name",
        Header: "Site",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "address_line_1",
        Header: "Address",
        sortable: true,
        Cell: (cell) => (
          <div>
            {`${
              cell.original.address_line_1
                ? `${cell.original.address_line_1}`
                : " N.A."
            },
            ${
              cell.original.address_line_2
                ? `${cell.original.address_line_2}`
                : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "city",
        Header: "City",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "state",
        Header: "Country & State",
        sortable: true,
        Cell: (cell) => (
          <div>
            {" "}
            {this.getCountryStateById(
              cell.original.country_id,
              cell.original.state
            )}
          </div>
        ),
      },
      {
        accessor: "zip",
        Header: "Zip",
        sortable: true,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },

      {
        accessor: "index",
        Header: "Edit",
        width: 60,
        sortable: false,
        Cell: (cell) => (
          <>
            <EditButton
              onClick={(e) => this.onEditRowClick(cell.original, e, cell.index)}
            />
            <DeleteButton
              onClick={(e) =>
                this.onDeleteRowClick(cell.original, e, cell.index)
              }
            />
          </>
        ),
      },
    ];

    return (
      <div className="sites-mapping">
        <div className="header-table">Sites to Auto creation :</div>
        <Table
          columns={columns}
          rows={this.state.siteList}
          className={""}
        />
        {this.state.error && (
          <div className="no-data error-message">
            {commonFunctions.getErrorMessage(this.state.error)}
          </div>
        )}
        <div className="field-mapping__footer">
          <SquareButton
            onClick={(e) => this.toggleMappingOpen("mapping")}
            content="Back"
            bsStyle={"default"}
            className="import-button-1"
          />
          <SquareButton
            onClick={this.onClickCreateSite}
            content="Create"
            bsStyle={"primary"}
            className="import-button-1"
          />
        </div>
      </div>
    );
  };

  onDeleteRowClick = (original: any, e: any, index: number): any => {
    e.stopPropagation();
    const siteList = cloneDeep(this.state.siteList);
    siteList.splice(index, 1);
    this.setState({ siteList });
  };
  onEditRowClick = (original: any, e: any, index: number): any => {
    e.stopPropagation();
    this.setState({
      site: this.state.siteList[index],
      openSiteUpdate: true,
      index,
    });
  };
  getTitle = () => {
    return " ";
  };

  getBody = () => {
    return (
      <div className="note-body">
        <div className="loader">
          {" "}
          <Spinner show={this.state.loading} />
        </div>
        {this.state.openSections["mapping"] === true && this.getSitMapping()}
        {this.state.openSections["list"] === true && this.getSiteListUI()}
      </div>
    );
  };

  closeSiteModal = () => {
    this.setState((prevState) => ({
      openSiteUpdate: !prevState.openSiteUpdate,
    }));
  };
  updateSiteOnIndex = (site) => {
    const newState = cloneDeep(this.state);
    (newState.siteList[this.state.index] as any) = site;
    (newState.openSiteUpdate as boolean) = false;
    (newState.index as number) = null;
    this.setState(newState, () => {
      this.toggleMappingOpen("list");
    });
  };
  render() {
    return (
      <>
        <RightMenu
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={null}
          className="field-mapping auto-address-mapping"
        />
        {this.state.openSiteUpdate && this.state.site && (
          <CreateSite
            isVisible={this.state.openSiteUpdate}
            close={this.closeSiteModal}
            customerId={this.props.customerId}
            site={this.state.site}
            onUpdate={this.updateSiteOnIndex}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customerId: state.customer.customerId,
  countries: state.inventory.countries,
});

const mapDispatchToProps = (dispatch: any) => ({
  bulkSiteCreate: (customerId: number, sites: any[]) =>
    dispatch(bulkSiteCreate(customerId, sites)),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SiteMapping);
