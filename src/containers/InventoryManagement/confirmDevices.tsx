import React from 'react';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';

import './style.scss';

interface IConfirmDeviceProps {
  show: boolean;
  onClose: (e: any) => void;
  deviceList?: any;
  xlsxData?: any;
  onSubmit: any;
}

export default class ConfirmDevices extends React.Component<
  IConfirmDeviceProps
> {
  onSubmit = () => {
    this.props.onSubmit();
  };

  getBody = () => {
    if (this.props.deviceList) {
      return (
        <div className="field-mapping__warning">
          {this.props.xlsxData &&
            this.props.deviceList && (
              <div>
                <div>Total selected devices {this.props.xlsxData}.</div>
                <div>{this.props.deviceList} Valid devices.</div>
                <div>
                  {this.props.xlsxData - this.props.deviceList} Invalid devices.
                </div>
              </div>
            )}
        </div>
      );
    } else {
      return null;
    }
  };

  getFooter = () => {
    return (
      <div className="customer-renew__actions">
        <SquareButton
          onClick={this.props.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Confirm"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    const title = ' Are you sure ?';

    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="import-confirm"
      />
    );
  }
}
