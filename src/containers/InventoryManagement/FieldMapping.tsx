import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import {
  addSite, ADD_SITE_SUCCESS, batchCreateImportData, BATCH_CREATE_SUCCESS, fetchCountriesAndStates,
  fetchDateFormat, fetchDeviceCategories, fetchDevices,
  fetchSites,
  fetchTaskStatus, importSmartNetEnable, TASK_STATUS_FAILURE,
  TASK_STATUS_SUCCESS, uploadDeviceFile, UPLOAD_DEVICE_FILE_SUCCESS
} from '../../actions/inventory';
import { fetchAllProviderIntegrations, FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS } from '../../actions/provider/integration';
import SquareButton from '../../components/Button/button';
import Checkbox from '../../components/Checkbox/checkbox';
import Input from '../../components/Input/input';
import SelectInput from '../../components/Input/Select/select';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { fromISOStringToFormattedDate } from '../../utils/CalendarUtil';
import { commonFunctions } from '../../utils/commonFunctions';
import CreateSite from './addSite';
import ConfirmDevices from './confirmDevices';
import SiteMapping from './siteMapping';
import './style.scss';

const fields: any[] = [
  {
    label: 'Serial Number',
    value: 'serial_number',
    isRequired: false,
    isDate: false,
    isCheckBox: false,
  },
  {
    label: 'Instance Number',
    value: 'instance_number',
    isRequired: false,
    isDate: false,
    isCheckBox: false,
  },
  {
    label: 'Device Name',
    value: 'device_name',
    isRequired: true,
    isDate: false,
    isCheckBox: false,
  },
  {
    label: 'Contract Number',
    value: 'service_contract_number',
    isDate: false,
    isCheckBox: false,
  },
  {
    label: 'Purchase Date',
    value: 'purchase_date',
    message: '',
    isDate: true,
    isCheckBox: false,
  },
  {
    label: 'Contract Start Date',
    value: 'installation_date',
    message: '',
    isDate: true,
    isCheckBox: false,
  },
  {
    label: 'Contract End Date',
    value: 'expiration_date',
    message: '',
    isDate: true,
    isCheckBox: false,
  },
  { label: 'Address', value: 'address', isRequired: true, isDate: false },
  {
    header: 'Ignore Zero $ items ',
    label: 'Service list price ',
    value: 'ignoreZero',
    isRequired: false,
    isDate: false,
    isCheckBox: true,
  },
  {
    header: 'Filter by Product Type ',
    label: 'Product types ',
    value: 'product_types',
    isRequired: false,
    isDate: false,
    isCheckBox: true,
  },
  { label: 'Configuration Type', value: 'configuration_type_name', isRequired: true, isDate: false },
  { label: 'Unique Identifier ', value: 'unique_identifier ', isRequired: true, isDate: false },
];

interface IfieldMappingProps extends ICommonProps {
  xlsxData: any;
  sites: ISite[];
  lastVisitedSite: string;
  fetchSites: (customerId: number) => any;
  customerId: number;
  addSite: (customerId: number, newSite: any) => any;
  batchCreateImportData: (customerId: number, deviceInfo: any) => any;
  deviceSuccess: IDeviceCreateRes;
  customersShort: ICustomerShort[];
  countries: any;
  fetchCountriesAndStates: any;
  isPostingBatch: boolean;
  manufacturers: any[];
  fetchDevices: TFetchDevices;
  fetchDateFormat: any;
  dateFormats: any;
  xlsxFile: any;
  uploadDeviceFile: (fileReq: any, name: string) => Promise<any>;
  addErrorMessage: any;
  fetchTaskStatus: any;
  addSuccessMessage: any;
  importSmartNetEnable: any;
  deviceCategoryList: any;
  fetchDeviceCategories: any;
  fetchAllProviderIntegrations: any;
}
interface IfieldMappingState {
  isopenSiteMapping: boolean;
  searchQueryField: any;
  searchQueryAddress: any;
  checkboxList: any;
  rows: any[];
  isAddressMappingOpen: boolean;
  xlsxData: any;
  columns: any[];
  deviceList: any;
  addressList: any[];
  deviceTypes: any[];
  sites: any[];
  customerId?: number;
  isCreateSiteModal: boolean;
  deviceSuccess: IDeviceCreateRes;
  isPostingSite: boolean;
  isopenConfirm: boolean;
  errors: string[];
  manufacturer_id: number;
  receiving_smartnet: boolean;
  error: {
    manufacturer_id: IFieldValidation;
  };
  isSuccess: boolean;
  dateFormat: any;
  isopenFilterPrototype: boolean;
  taskId: any;
  isConfigured: boolean;
  loading: boolean;
  uploading: boolean;
  show: boolean;
  selectedRows: number[];
  open: any;
  configuration_type: string;
  categoryIds: any[];
  clearSite: boolean;
}

const sections = {
  'fieldMapping': true,
  'addressMapping': false,
  'configTypeMapping': false,
  'poTicketMapping': false,
  'correction': false,
  'emailSetting': false,
  'result': false
};
class FieldMapping extends React.Component<
  IfieldMappingProps,
  IfieldMappingState
> {
  constructor(props: IfieldMappingProps) {
    super(props);

    this.state = {
      open: sections,
      selectedRows: [],
      rows: [],
      searchQueryField: [],
      searchQueryAddress: [],
      checkboxList: [],
      dateFormat: [],
      isAddressMappingOpen: false,
      isCreateSiteModal: false,
      xlsxData: null,
      columns: [],
      deviceList: [],
      addressList: [],
      deviceTypes: [],
      sites: [],
      deviceSuccess: {
        total_request_items_count: 0,
        successfully_updated_items_count: 0,
        successfully_created_items_count: 0,
        failed_updated_items_count: 0,
        failed_created_items_count: 0,
        import_warnings_file_info: {
          file_name: '',
          file_path: '',
        },
      },
      isPostingSite: false,
      isopenConfirm: false,
      isopenSiteMapping: false,
      isopenFilterPrototype: false,
      manufacturer_id: null,
      receiving_smartnet: false,
      errors: ['', '', '', '', '', '', '', '', '', '', '', '', ''],
      error: {
        manufacturer_id: {
          errorState: "success",
          errorMessage: '',
        },
      },
      isSuccess: false,
      taskId: null,
      isConfigured: false,
      loading: true,
      show: false,
      uploading: false,
      configuration_type: '',
      categoryIds: [],
      clearSite: false,
    };
  }

  componentDidMount() {
    if (!this.props.customerId) {
      this.props.history.push(this.props.lastVisitedSite);
    }
    this.props.fetchCountriesAndStates();
    this.props.importSmartNetEnable()
      .then(action => {
        if (action.type === TASK_STATUS_SUCCESS) {
          this.setState({ isConfigured: action.response.is_configured });
        }
        this.setState({ loading: false });
      });
    this.props.fetchDateFormat();
    if (this.props.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
    this.props.fetchDeviceCategories();
    this.props.fetchAllProviderIntegrations().then(action => {
      if (action.type === FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS) {
        const categoryIds = action.response.find(
          int => int.type === "CONNECTWISE"
        ).other_config;
        this.setState({ categoryIds: categoryIds && categoryIds.empty_serial_mapping.device_category_ids });
      }
    });
  }

  componentDidUpdate(prevProps: IfieldMappingProps) {
    if (this.props.xlsxData !== prevProps.xlsxData) {
      this.setState({ xlsxData: this.props.xlsxData });
      this.setColumns(this.props);
    }
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchSites(this.props.customerId);
    }
    if (this.props.deviceSuccess !== prevProps.deviceSuccess) {
      this.setState({ deviceSuccess: this.props.deviceSuccess });
    }
  }  

  setColumns = (nextProps: IfieldMappingProps) => {
    const xlsxDataResponce = nextProps.xlsxData;
    const columns = [];
    let index = 0;
    let tempKey = '';
    for (const key in xlsxDataResponce[0]) {
      if (xlsxDataResponce[0].hasOwnProperty(key)) {
        if (key.includes('EMPTY')) {
          tempKey = `column_${index + 1}`;
        }
        columns.push({ key: tempKey !== '' ? tempKey : key, value: key });
        index++;
        tempKey = '';
      }
    }
    this.setState({ columns });
  };

  setAddressList = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const addressList = [];
    xlsxDataResponce.map(data => {
      addressList.push(
        data[this.state.searchQueryField[7]] &&
          data[this.state.searchQueryField[7]] !== '-'
          ? data[this.state.searchQueryField[7]]
          : 'N.A.'
      );
    });
    const onlyUniqueAddress = addressList.filter(this.onlyUnique);
    const searchQueryAddress = onlyUniqueAddress.map(x=>{
      return this.getSiteIdByName(x)
    });
    this.setState({ addressList: onlyUniqueAddress,searchQueryAddress });
  };
  onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };

  getSiteIdByName =(name)=>{
    const site = this.props.sites && this.props.sites.filter(s=>s.name ===name)[0];
    return site && site.site_id || '';
  }

  toggleAddressMappingOpen = () => {
    this.setState(prevState => ({
      isAddressMappingOpen: !prevState.isAddressMappingOpen,
    }));
  };

  toggleCreateSiteModal = () => {
    this.setState(prevState => ({
      isCreateSiteModal: !prevState.isCreateSiteModal,
    }));
  };

  getFieldsOptions = () => {
    if (this.state.columns.length > 0) {
      return this.state.columns.map(role => ({
        value: role.value,
        label: role.key,
      }));
    } else {
      return [];
    }
  };

  getDateFormatOptions = () => {
    const formats = [];
    if (this.props.dateFormats) {
      Object.keys(this.props.dateFormats).map(key => {
        formats.push({
          value: this.props.dateFormats[key],
          label: key,
        });
      });

      return formats;
    } else {
      return [];
    }
  };

  getSitesOptions = () => {
    if (this.props.sites && this.props.sites.length > 0) {
      return this.props.sites.map(site => ({
        value: site.site_id,
        label: commonFunctions.getAddress(site),
      }));
    } else {
      return [];
    }
  };

  onSiteAdd = (newSite: any) => {
    this.setState({ isPostingSite: true, clearSite: false });
    const customerId = this.props.customerId;
    if (customerId) {
      this.props
        .addSite(customerId, newSite)
        .then(action => {
          if (action.type === ADD_SITE_SUCCESS) {
            this.setState({
              isCreateSiteModal: false,
              clearSite: true,
            });

            this.props.fetchSites(customerId);
          } else if(action.errorList.status === 400 ){
            const msg = commonFunctions.getErrorMessage(action.errorList.data);
            this.props.addErrorMessage(msg)
          }
          this.setState({ isPostingSite: false });
        })
        .catch(() => {
          this.setState({ isPostingSite: false });
        });
    }
  };

  onClickCancel = () => {
    this.props.history.push(this.props.lastVisitedSite);
  };

  handleChangeField = (event: any, index?: any) => {
    const searchQueryField = this.state.searchQueryField;
    searchQueryField[index] = event.target.value;

    this.setState({
      searchQueryField,
    });

    if (index === 9) {
      this.setDeviceTypes();
      //this.toggleFilterPrototype();
    }
  };

  handleChangeDateFormat = (event: any, index?: any) => {
    const dateFormat = this.state.dateFormat;
    dateFormat[index] = event.target.value;

    this.setState({
      dateFormat,
    });
  };

  handleChangeAddress = (event: any, index?: any) => {
    if (event.target.value === '0') {
      this.toggleCreateSiteModal();
    } else {
      const searchQueryAddress = this.state.searchQueryAddress;

        searchQueryAddress[index] = event.target.value;

      this.setState({
        searchQueryAddress,
      });
    }
  };

  checkConfigType = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const configChanges = [];
    xlsxDataResponce.map((data, index) => {
      configChanges.push({
        index,
        serial_number: data[this.state.searchQueryField[0]] || null,
        instance_number: data[this.state.searchQueryField[1]] || null,
        device_name: data[this.state.searchQueryField[2]] || null,
        service_contract_number: data[this.state.searchQueryField[3]] || null,
        purchase_date: data[this.state.searchQueryField[4]] || null,
        purchase_date_format: data[this.state.dateFormat[4]] || null,
        installation_date: data[this.state.searchQueryField[5]] || null,
        installation_date_format: data[this.state.dateFormat[5]] || null,
        expiration_date: data[this.state.searchQueryField[6]] || null,
        expiration_date_format: data[this.state.dateFormat[6]] || null,
        address: data[this.state.searchQueryField[7]] || null,
        device_price: data[this.state.searchQueryField[8]] || null,
        product_type: data[this.state.searchQueryField[9]] || null,
        configuration_type_name: data[this.state.searchQueryField[10]] || null,
        unique_identifier: data[this.state.searchQueryField[11]] || null,
        config_type_object: data[this.state.searchQueryField[10]] ? this.props.deviceCategoryList.find(d => d.category_name === data[this.state.searchQueryField[10]]) || false : false,
      });
    });

    this.setState({ rows: configChanges });
    const valid = configChanges.map(row =>
      this.props.deviceCategoryList.find(d => d.category_name === row.configuration_type_name) || false
    )
    return valid.every(v => v);
  };

  onClickImportFieldMapping = (event: any, index?: any) => {
    if (this.validateForm()) {
      this.setAddressList();
      if (this.checkConfigType() || !this.state.searchQueryField[10]) {
        this.toggleMappingOpen('addressMapping');
      } else {
        this.toggleMappingOpen('configTypeMapping');
      }
    }
  };

  setDeviceTypes = () => {
    const xlsxDataResponce = this.state.xlsxData;
    const deviceTypes = [];
    const uniqueTypes = [];
    let isBlank = false;
    xlsxDataResponce.map(data => {
      deviceTypes.push(data[this.state.searchQueryField[9]]);
    });
    const onlyUniqueTypes = deviceTypes.filter(this.onlyUnique);
    onlyUniqueTypes.map(type => {
      if (type !== '' && type !== '-') {
        uniqueTypes.push({
          product_type: type,
          include: true,
          ignore_zero_dollar_items: this.state.checkboxList[8]
            ? this.state.checkboxList[8]
            : false,
          include_non_serials: false,
        });
      } else {
        isBlank = true;
      }
    });
    if (isBlank) {
      uniqueTypes.push({
        product_type: 'N.A.',
        include: true,
        ignore_zero_dollar_items: this.state.checkboxList[8]
          ? this.state.checkboxList[8]
          : false,
        include_non_serials: false,
      });
    }
    this.setState({ deviceTypes: uniqueTypes });
  };

  validateForm = () => {
    let isValid = true;
    const newState = cloneDeep(this.state);
    fields.map((field, index) => {
      newState.errors[index] = '';
    });
    newState.error.manufacturer_id.errorState = "success";
    newState.error.manufacturer_id.errorMessage = '';

    if (!this.state.manufacturer_id) {
      newState.error.manufacturer_id.errorState = "error";
      newState.error.manufacturer_id.errorMessage =
        'Please select manufacturer ';
      isValid = false;
    }

    if (this.state.checkboxList[8] && !this.state.searchQueryField[8]) {
      newState.errors[8] = 'Please select field';
      isValid = false;
    }

    if (this.state.checkboxList[9] && !this.state.searchQueryField[9]) {
      newState.errors[9] = 'Please select field';
      isValid = false;
    }

    fields.map((field, index) => {
      const inValidField = !this.state.columns.map(f => f.value).includes(this.state.searchQueryField[index]);
      if (field.isRequired && (!this.state.searchQueryField[index] || inValidField)) {
        newState.errors[index] = `${field.label} is required`;
        isValid = false;
      }
      if (
        this.state.searchQueryField[index] &&
        field.isDate &&
        !this.state.dateFormat[index]
      ) {
        newState.errors[index] = `Please select Date format`;
        isValid = false;
      }
    });

    this.setState(newState);

    return isValid;
  };

  onClickImportSave = (event: any, index?: any) => {

    const addressSiteObject = [];
    if (
      this.state.searchQueryAddress.length === this.state.addressList.length
    ) {
      this.setState({ uploading: true });
      this.toggleMappingOpen('result');
      this.state.searchQueryAddress.map((data, i) => {
        addressSiteObject.push({
          site_id: data,
          address: this.state.addressList[i],
        });
      });
      const configuration_types = {};
      if (this.state.searchQueryField[10]) {
        this.state.rows.map(row => {
          const config_type_object = this.props.deviceCategoryList.find(d => d.category_name === row.configuration_type_name);
          configuration_types[row.serial_number || row.unique_identifier] = config_type_object && config_type_object.category_id;
        })
      }

      const deviceInfo = {
        file_name: this.props.xlsxFile && this.props.xlsxFile.name,
        field_mappings: {
          serial_number: this.state.searchQueryField[0]
            ? this.state.searchQueryField[0]
            : null,
          device_name: this.state.searchQueryField[2]
            ? this.state.searchQueryField[2]
            : null,
          service_contract_number: this.state.searchQueryField[3]
            ? this.state.searchQueryField[3]
            : null,
          purchase_date: this.state.searchQueryField[4]
            ? this.state.searchQueryField[4]
            : null,
          purchase_date_format: this.state.dateFormat[4]
            ? this.state.dateFormat[4]
            : null,
          installation_date: this.state.searchQueryField[5]
            ? this.state.searchQueryField[5]
            : null,
          installation_date_format: this.state.dateFormat[5]
            ? this.state.dateFormat[5]
            : null,
          expiration_date: this.state.searchQueryField[6]
            ? this.state.searchQueryField[6]
            : null,
          expiration_date_format: this.state.dateFormat[6]
            ? this.state.dateFormat[6]
            : null,
          address: this.state.searchQueryField[7]
            ? this.state.searchQueryField[7]
            : null,
          device_price: this.state.searchQueryField[8]
            ? this.state.searchQueryField[8]
            : null,
          product_type: this.state.searchQueryField[9]
            ? this.state.searchQueryField[9]
            : null,
          configuration_type: this.state.searchQueryField[10]
            ? this.state.searchQueryField[10]
            : null,
          unique_identifier: this.state.searchQueryField[11]
            ? this.state.searchQueryField[11]
            : null,
          instance_number: this.state.searchQueryField[1]
            ? this.state.searchQueryField[1]
            : null,
        },
        manufacturer_id: this.state.manufacturer_id,
        ignore_zero_dollar_items: this.state.checkboxList[8]
          ? this.state.checkboxList[8]
          : false,
        filter_by_product_type: this.state.checkboxList[9]
          ? this.state.checkboxList[9]
          : false,
        device_type_filter_meta: this.state.deviceTypes
          ? this.state.deviceTypes
          : null,
        address_field_meta: addressSiteObject ? addressSiteObject : null,
        configuration_types,
      };

      if (this.props.xlsxFile) {
        const data = new FormData();
        data.append('filename', this.props.xlsxFile);
        this.props
          .uploadDeviceFile(
            data,
            this.props.xlsxFile && this.props.xlsxFile.name
          )
          .then(action => {
            if (action.type === UPLOAD_DEVICE_FILE_SUCCESS) {
              deviceInfo.file_name = action.response;
              if (this.props.customerId) {
                this.props
                  .batchCreateImportData(this.props.customerId, deviceInfo)
                  .then(a => {
                    if (a.type === BATCH_CREATE_SUCCESS) {
                      //this.props.fetchDevices(this.props.customerId, false);
                      //this.setState({ isSuccess: true });
                      if (a.response && a.response.task_id) {
                        this.setState({ taskId: a.response.task_id, uploading: false });
                        this.fetchTaskStatus();
                      }
                    }
                    // if (a.type === BATCH_CREATE_FAILURE) {
                    //   this.props.fetchDevices(this.props.customerId, false);
                    //this.props.addErrorMessage(a.errorList.data.import_file);
                    // }
                  });
              }
            }
          });
      }
    }
  };

  fetchTaskStatus = () => {
    if (this.state.taskId) {
      this.props.fetchTaskStatus(this.state.taskId).then(a => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage('Bulk import completed.');
            this.props.fetchDevices(this.props.customerId, false);
            this.setState({ isSuccess: true, taskId: '' });

          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage('Bulk import failed.');
            this.setState({ isSuccess: true, taskId: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatus();
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.fetchDevices(this.props.customerId, false);
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState(prevState => ({
      [targetName]: targetValue,
    }));
  };

  onCheckboxChangedReceiving = (event: any) => {
    const active = event.target.checked;
    if (active) {
      this.props.history.push('/inventory-management/field-mapping-smartnet')
    }
  };

  onCheckboxChangedType = (event: any, index: any, column: string) => {
    const deviceTypes = this.state.deviceTypes;
    deviceTypes[index][column] = event.target.checked;
    if (column === 'include' && event.target.checked === false) {
      deviceTypes[index].ignore_zero_dollar_items = false;
      deviceTypes[index].include_non_serials = false;
    }
    this.setState({
      deviceTypes,
    });
  };

  getFieldMapping = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(manufacturer => ({
        value: manufacturer.id,
        label: manufacturer.label,
      }))
      : [];

    return (
      <div className="field-mapping">
        <h3>Field Mapping</h3>
    
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-row">
              <td className="first-manufacturer">

                <Checkbox
                  isChecked={this.state.receiving_smartnet}
                  name="option"
                  onChange={e => this.onCheckboxChangedReceiving(e)}
                  disabled={!this.state.isConfigured}
                >
                  Receiving Smartnet
                </Checkbox>

              </td>
              <td colSpan={2}>
                {
                  !this.state.loading && (!this.state.isConfigured && <div className="error-not-configure">
                    Please complete following settings to enable Receiving Smartnet
                    <div>- <Link to="/setting/smartnet_receiving" className="header-user-options">
                      SmartNet Receiving Settings
                    </Link>
                    </div>
                    <div>- <Link to="/setting/erp" className="header-user-options">
                      Field Mapping - Smartnet Complete Status
                    </Link></div>
                  </div>
                  )
                }
              </td>
              <td className="sheet-column" />
              <td className="last" />
              <th className="" />
            </tr>
            <tr className="field-mapping__table-row">
              <td className="first-manufacturer">
                Select Manufacturer
                <span className="field-mapping__label-required" />
                {this.state.error.manufacturer_id.errorMessage ? (
                  <span className="field-mapping__label-error">
                    {this.state.error.manufacturer_id.errorMessage}
                  </span>
                ) : null}
              </td>
              <td className="middle-manufacturer">
                <img src="/assets/new-icons/oppositearrow.svg" />
              </td>
              <td className="sheet-column">
                {
                  <SelectInput
                    name="manufacturer_id"
                    value={this.state.manufacturer_id}
                    onChange={this.handleChange}
                    options={manufacturers}
                    searchable={true}
                    placeholder="Select Manufacturer"
                    clearable={false}
                  />
                }
              </td>
              <td className="last" />
              <th className="" />
            </tr>
          </tbody>
        </table>
        <table className="field-mapping__table">
          <tbody>
            <tr className="field-mapping__table-header">
              <th className="first">Map Field</th>
              <th className="middle" />
              <th className="last"> Sheet Columns </th>
              <th className="" />
            </tr>

            {fields.map((field, index) => (
              <tr key={index} className="field-mapping__table-row">
                {field.isCheckBox && (
                  <td className="first">
                    <Checkbox
                      isChecked={this.state.checkboxList[index]}
                      name="option"
                      onChange={e => this.onCheckboxChanged(e, index)}
                    >
                      {field.header}
                    </Checkbox>
                  </td>
                )}

                <td className="first">
                  {((field.isCheckBox && this.state.checkboxList[index]) ||
                    !field.isCheckBox) &&
                    field.label}

                  {field.message ? (
                    <div className="field-mapping__label-message">
                      {field.message}
                    </div>
                  ) : null}
                  {field.isRequired && (
                    <span className="field-mapping__label-required" />
                  )}
                  {field.isCheckBox && this.state.checkboxList[index] && (
                    <img
                      className="opposite-arrow"
                      src="/assets/new-icons/oppositearrow.svg"
                    />
                  )}
                  {field.message ? (
                    <div className="field-mapping__label-message">
                      {field.message}
                    </div>
                  ) : null}
                  {this.state.errors[index] ? (
                    <span className="field-mapping__label-error">
                      {this.state.errors[index]}
                    </span>
                  ) : null}
                </td>
                {!field.isCheckBox && (
                  <td className="middle">
                    <img src="/assets/new-icons/oppositearrow.svg" />
                  </td>
                )}
                {((field.isCheckBox && this.state.checkboxList[index]) ||
                  !field.isCheckBox) && (
                    <td className="sheet-column">
                      <SelectInput
                        name="select"
                        value={this.state.searchQueryField[index]}
                        onChange={event => this.handleChangeField(event, index)}
                        options={this.getFieldsOptions()}
                        searchable={true}
                        placeholder="Select field"
                        clearable={true}
                      />
                    </td>
                  )}
                {field.isDate && (
                  <td className="last">
                    <SelectInput
                      name="select"
                      value={this.state.dateFormat[index]}
                      onChange={event =>
                        this.handleChangeDateFormat(event, index)
                      }
                      options={this.getDateFormatOptions()}
                      searchable={true}
                      placeholder="Select Date Format"
                      clearable={false}
                      disabled={!this.state.searchQueryField[index]}
                    />
                  </td>
                )}

                <th className="" />
                <th className="" />
                <th className="" />
              </tr>
            ))}
          </tbody>
        </table>
        {this.state.deviceTypes && this.state.deviceTypes.length > 0 && (
          <table className="field-mapping__table">
            <tbody>
              <tr className="field-mapping__table-row" />
              <tr className="field-mapping__table-header">
                <th className="first">Type</th>
                <th className="first">Import</th>
                {this.state.checkboxList[8] && (
                  <th className="first">Ignore zero $</th>
                )}

                <th className="first">Include Non-serial</th>
                <th className="" />
              </tr>

              {this.state.deviceTypes.map((type, index) => (
                <tr key={index} className="field-mapping__table-row">
                  <td className="device-type">{type.product_type}</td>
                  <td className="first">
                    <Checkbox
                      isChecked={this.state.deviceTypes[index].include}
                      name="option"
                      onChange={e =>
                        this.onCheckboxChangedType(e, index, 'include')
                      }
                    />
                  </td>
                  {this.state.checkboxList[8] && (
                    <td className="first">
                      <Checkbox
                        isChecked={
                          this.state.deviceTypes[index].ignore_zero_dollar_items
                        }
                        name="option"
                        onChange={e =>
                          this.onCheckboxChangedType(
                            e,
                            index,
                            'ignore_zero_dollar_items'
                          )
                        }
                        disabled={!type.include}
                      />
                    </td>
                  )}
                  <td className="first">
                    <Checkbox
                      isChecked={
                        this.state.deviceTypes[index].include_non_serials
                      }
                      name="option"
                      onChange={e =>
                        this.onCheckboxChangedType(
                          e,
                          index,
                          'include_non_serials'
                        )
                      }
                      disabled={!type.include}
                    />
                  </td>
                  <td className="" />
                </tr>
              ))}
            </tbody>
          </table>
        )}

        <div className="field-mapping__footer">
          <SquareButton
            onClick={this.onClickCancel}
            content="Cancel"
            bsStyle={"default"}
            className="import-button-1"
          />
          <SquareButton
            onClick={this.onClickImportFieldMapping}
            content="Next"
            bsStyle={"primary"}
            className="import-button-1"
          />
        </div>
      </div>
    );
  };

  onCheckboxChanged = (event: any, index?: any) => {
    const checkboxList = this.state.checkboxList;
    const searchQueryField = this.state.searchQueryField;
    let deviceTypes = this.state.deviceTypes;
    if (index === 9) {
      deviceTypes = [];
    }
    if (index === 8) {
      deviceTypes = deviceTypes.map((item, i) => {
        if (item.include) {
          item.ignore_zero_dollar_items = event.target.checked;
        }

        return item;
      });
    }
    checkboxList[index] = event.target.checked;

    searchQueryField[index] = '';
    const errors = this.state.errors;
    errors[8] = '';
    errors[9] = '';
    this.setState({
      checkboxList,
      searchQueryField,
      errors,
      deviceTypes,
    });
  };

  toggleConfirmOpen = () => {
    this.setState(prevState => ({
      isopenConfirm: !prevState.isopenConfirm,
    }));
  };
  toggleSiteMapping = () => {
    this.setState(prevState => ({
      isopenSiteMapping: !prevState.isopenSiteMapping,
    }));
  };
  toggleFilterPrototype = () => {
    this.setState(prevState => ({
      isopenFilterPrototype: !prevState.isopenFilterPrototype,
    }));
  };

  onClickConfirm = () => {
    this.toggleAddressMappingOpen();
    this.toggleConfirmOpen();
  };
  addressValidSelection =()=>{
    return this.state.searchQueryAddress.filter(x =>x).length !== this.state.addressList.length
  }
  getAddressMapping = () => {
    return (
      <div className="field-mapping address-mapping">
        <h3>Site Mapping</h3>
    
        {this.props.isPostingBatch ? (
          <Spinner show={true} />
        ) : (
          <div>
            <table className="field-mapping__table">
              <tbody>
                <tr className="field-mapping__table-header">
                  <th className="first">Site</th>
                  <th className="middle" />
                  <th className="last">Saved Sites </th>
                  <th className="" >
                    {
                      this.addressValidSelection() &&
                      <SquareButton
                      onClick={this.toggleSiteMapping}
                      content="Auto Site Creation"
                      bsStyle={"primary"}
                      className="auto-site-creation"
                   />
                    }
                 
           </th>
                </tr>

                {this.state.addressList &&
                  this.state.addressList.map((field, index) => (
                    <tr key={index} className="field-mapping__table-row">
                      <td className="first">{field}</td>
                      <td className="middle">
                        <img src="/assets/new-icons/oppositearrow.svg" />
                      </td>
                      <td className="last">
                        {
                          <SelectInput
                            name="select"
                            value={this.state.searchQueryAddress[index]}
                            onChange={event =>
                              this.handleChangeAddress(event, index)
                            }
                            options={this.getSitesOptions().concat([
                              {
                                value: '0',
                                label: 'Create new site',
                              },
                            ])}
                            searchable={true}
                            placeholder="Select field"
                            clearable={true}
                          />
                        }
                      </td>
                      <th className="" />
                    </tr>
                  ))}
              </tbody>
            </table>
            {this.renderErrorAddressMapping()}
            <div className="field-mapping__footer">
              <SquareButton
                onClick={() => this.toggleMappingOpen('fieldMapping')}
                content="Back"
                bsStyle={"default"}
                className="import-button-1"
              />
              <SquareButton
                onClick={this.onClickImportSave}
                content="Import"
                bsStyle={"primary"}
                className="import-button-1"
                disabled={this.state.searchQueryAddress.filter(x =>x).length !== this.state.addressList.length}
              />
            </div>
          </div>
        )}
      </div>
    );
  };

  downloadReport = response => {
    if (response && response !== '') {
      const url = response.file_path;
      const link = document.createElement('a');
      link.href = url;
      link.target = '_blank';
      link.setAttribute('download', response.file_name);
      document.body.appendChild(link);
      link.click();
    }
  };

  renderErrorAddressMapping = (errorText: string = 'Please map all site name with site.') => {
    return (
      <div className={'purchase-order-top'}>
        {this.state.searchQueryAddress.length !== this.state.addressList.length && errorText}
      </div>
    );
  };

  toggleMappingOpen = field => {
    const open = [];
    Object.keys(this.state.open).map((f, index) => {
      open[f] = false;
    });
    open[field] = true;
    this.setState({ open });
  };


  onRowsToggle = selectedRows => {
    const newState = this.state;

    this.state.rows.map((row, i) => {
      newState.rows[i].selected = false;
    });

    selectedRows.map((id, index) => {
      this.state.rows.map((row, i) => {
        if (row.index === id) {
          newState.rows[i].selected = true;
        }
      })
    });

    this.setState({
      rows: newState.rows,
    }, () => {
      this.setState({
        selectedRows,
      });
    });
  };

  handleChangeType = (event: any, data?: any) => {
    const rows = this.state.rows;
    rows[data.original.index].configuration_type_name = event.target.value;

    this.setState({
      rows,
    });
  };

  renderError = (errorText: string = 'Please select Configuration Type.') => {
    return (
      <div className={'purchase-order-top'}>
        {errorText}
      </div>
    );
  };

  onClickTypeSubmit = () => {
    const valid = this.state.rows.map(row =>
      this.props.deviceCategoryList.find(d => d.category_name === row.configuration_type_name) || false
    )
    if (valid.every(v => v)) {
      this.toggleMappingOpen('addressMapping');
    }
  }

  handleChangeSelectedRows = () => {
    const rows = this.state.rows;
    this.state.selectedRows.map(index => {
      rows[index].configuration_type_name = this.state.configuration_type;
    })

    this.setState({
      rows,
    });
  };

  renderTop = (list) => {
    return (
      <div className="type-filter ">
        <div className="check-show-all">
          <Checkbox
            isChecked={this.state.show}
            name="option"
            onChange={e => { this.setState({ show: !this.state.show, selectedRows: this.state.show ? [] : this.state.selectedRows }) }}
          >
            Show all rows
          </Checkbox>
        </div>
        <div className="apply-type">
          <Input
            field={{
              label: 'Bulk Update Configuration Type',
              type: "PICKLIST",
              value: this.state.configuration_type,
              options: list,
              isRequired: true,
            }}
            width={12}
            name="type"
            onChange={e => this.setState({ configuration_type: e.target.value })}
            placeholder={`Select type`}
          />
          <SquareButton
            onClick={this.handleChangeSelectedRows}
            content="Update selected"
            bsStyle={"primary"}
            className="apply-all"
            disabled={!this.state.configuration_type || this.state.selectedRows.length === 0}
          />
        </div>
      </div>
    )
  }

  getConfigTypeMappping = () => {
    const list = this.props.deviceCategoryList
      ? this.props.deviceCategoryList.map(t => ({
        value: t.category_name,
        label: t.category_name,
        disabled: this.state.searchQueryField[0] ? false : !this.state.categoryIds.includes(t.category_id)
      }))
      : [];
    const columns: any = [
      { accessor: 'device_name', Header: 'Device Name', },
      { accessor: 'serial_number', Header: 'Serial' },
      {
        accessor: 'configuration_type_name',
        Header: 'Configuration Type',
        width: 250,
        sortable: false,
        Cell: cell => (
          <>{
            <div
              className={`select-inner-table
              ${cell.row._original.errorService ? 'required-error' : ' '}
            `}
            >
              <SelectInput
                name={cell.row._original.serial_number}
                value={cell.row._original.configuration_type_name}
                onChange={e => this.handleChangeType(e, cell)}
                options={list}
                multi={false}
                clearable={false}
                placeholder="Select Type"
              />
            </div>
          }
          </>

        ),
      },
      {
        accessor: 'service_contract_number',
        Header: 'Contract Number',
      },
      {
        accessor: 'expiration_date',
        Header: 'End Date',
        id: 'date',
        Cell: date => <div className="pl-15">{fromISOStringToFormattedDate(date.value)}</div>,
      },
    ];
    const devices = this.state.show ? this.state.rows : this.state.rows.filter(row => row.config_type_object === false)
    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: 'index',
      onRowsToggle: this.onRowsToggle,
      selectedIndexes: this.state.selectedRows,
    };
    return (
      <div className="field-mapping po-mapping">
        <h3>Configuration Type Mapping</h3>

        <div>
          <Table
            columns={columns}
            rows={devices}
            customTopBar={this.renderTop(list)}
            rowSelection={rowSelectionProps}
            className={`purchase-order`}
            loading={this.props.isPostingBatch}
            pageSize={devices.length}
          />
          {this.state.selectedRows.length === 0 && this.renderError() ||
            (this.state.rows.filter(row => row.errorService !== '')
              && this.state.rows.filter(row => row.errorService !== '').length > 0 &&
              this.renderError(this.state.rows.filter(row => row.errorService !== '')[0].errorService))}
          <div className="field-mapping__footer">
            <SquareButton
              onClick={e => {
                // this.toggleMappingOpen('fieldMapping')
                const open = [];
                Object.keys(this.state.open).map((f, index) => {
                  open[f] = false;
                });
                open['fieldMapping'] = true;
                this.setState({ open, rows: [] });
              }}
              content="Back"
              bsStyle={"default"}
              className="import-button-1"
            />
            <SquareButton
              onClick={this.onClickTypeSubmit}
              content="Submit"
              bsStyle={"primary"}
              className="import-button-1"
            />
          </div>
        </div>
      </div>
    );
  };

  renderResult = () => {
    return (
      <>
      <div className="import-devices">Device Bulk Import </div>
        {(this.state.isSuccess ||
          (this.state.deviceSuccess.import_warnings &&
            this.state.deviceSuccess.import_warnings.length > 0) ||
          this.state.deviceSuccess.failed_created_items_count !== 0 ||
          this.state.deviceSuccess.failed_updated_items_count !== 0 ||
          this.state.deviceSuccess.successfully_updated_items_count !== 0 ||
          this.state.deviceSuccess.successfully_created_items_count !== 0 ||
          this.state.deviceSuccess.total_request_items_count !== 0) && (
            <div className="device-responce">
              <h3>Status :</h3>
              <div className="row ">
                Total device :
                <span>
                  {' '}
                  {this.state.deviceSuccess.total_request_items_count || 0}
                </span>
              </div>
              <div className="row success">
                Device created :
                <span>
                  {this.state.deviceSuccess.successfully_created_items_count || 0}
                </span>
              </div>
              <div className="row success">
                Updated device :
                <span>
                  {this.state.deviceSuccess.successfully_updated_items_count || 0}
                </span>
              </div>
              <div className="row error">
                Failed to create device :
                <span>
                  {this.state.deviceSuccess.failed_created_items_count || 0}
                </span>
              </div>
              <div className="row error">
                Failed to update device :
                <span>
                  {this.state.deviceSuccess.failed_updated_items_count || 0}
                </span>
              </div>
              {this.state.deviceSuccess.import_warnings &&
                this.state.deviceSuccess.import_warnings.length > 0 && (
                  <div className="row error warning-list">
                    Warnings :
                    {/* {this.state.deviceSuccess.import_warnings
              .map((text, i) => {
                return <div key={i}> 1. {{ text }}</div>;
              })} */}
                    {this.state.deviceSuccess.import_warnings.map(
                      (data, index) => (
                        <div key={index} className="warning-text">
                          {index + 1}. {data}
                        </div>
                      )
                    )}
                  </div>
                )}
              {this.state.deviceSuccess.import_warnings_file_info &&
                this.state.deviceSuccess.import_warnings_file_info.file_path && (
                  <div className="row error warning-list">
                    Download error report :{' '}
                    <img
                      onClick={e =>
                        this.downloadReport(
                          this.state.deviceSuccess.import_warnings_file_info
                        )
                      }
                      alt=""
                      className="import-error-btn"
                      src="/assets/icons/download.png"
                    />
                  </div>
                )}
            </div>
          )}
      </>)
  }

  render() {
    return (
      <div className="mapping">
        {this.props.isPostingBatch || this.state.uploading ? <Spinner show={true} /> : null}
        {this.state.open['fieldMapping'] && this.getFieldMapping()}
        {this.state.open['configTypeMapping'] === true &&
          this.getConfigTypeMappping()}
        {this.state.open['addressMapping'] &&
          this.getAddressMapping()}

        <CreateSite
          show={this.state.isCreateSiteModal}
          onClose={this.toggleCreateSiteModal}
          onSubmit={this.onSiteAdd}
          countries={this.props.countries}
          isPosting={this.state.isPostingSite}
          clear={this.state.clearSite}
        />
        <ConfirmDevices
          deviceList={this.state.deviceList ? this.state.deviceList.length : 0}
          xlsxData={this.state.xlsxData ? this.state.xlsxData.length : 0}
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
        />
        {
          this.state.isopenSiteMapping &&
          <SiteMapping
          xlsxData={this.state.xlsxData}
          searchQueryAddress={this.state.searchQueryAddress}
          addressFieldSelected={this.state.searchQueryField[7]}
          addressList={this.state.addressList}
          show={this.state.isopenSiteMapping}
          onClose={this.toggleSiteMapping}
          onSubmit={this.onClickConfirm}
          directImport={false}
        />
        }
        
       
        {
          this.state.open['result'] && this.renderResult()
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  xlsxData: state.inventory.xlsxData,
  sites: state.inventory.sites,
  customerId: state.customer.customerId,
  deviceSuccess: state.inventory.deviceSuccess,
  customersShort: state.customer.customersShort,
  countries: state.inventory.countries,
  isPostingBatch: state.inventory.isPostingBatch,
  manufacturers: state.inventory.manufacturers,
  dateFormats: state.inventory.dateFormats,
  xlsxFile: state.inventory.xlsxFile,
  lastVisitedSite: state.inventory.lastVisitedSite,
  deviceCategoryList: state.inventory.deviceCategoryList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchDeviceCategories: () => dispatch(fetchDeviceCategories()),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  addSite: (customerId: number, newSite: any) =>
    dispatch(addSite(customerId, newSite)),
  batchCreateImportData: (customerId: number, devices: IDevice[]) =>
    dispatch(batchCreateImportData(customerId, devices)),
  fetchCountriesAndStates: () => dispatch(fetchCountriesAndStates()),
  fetchDevices: (id: number, show: boolean) => dispatch(fetchDevices(id)),
  fetchDateFormat: () => dispatch(fetchDateFormat()),
  uploadDeviceFile: (fileReq: any, name: string) =>
    dispatch(uploadDeviceFile(fileReq, name)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  importSmartNetEnable: (id: number) => dispatch(importSmartNetEnable(id)),
  fetchAllProviderIntegrations: () => dispatch(fetchAllProviderIntegrations()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FieldMapping);
