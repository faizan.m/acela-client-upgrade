import React from 'react';
import { connect } from 'react-redux';

import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import {
  fetchDeviceRenwalHistory,
  fetchDeviceRenwalHistoryCustomer,
  RENEWAL_HISTORY_SUCCESS,
  UNBLOCK_RENEWAL_FAILURE,
  UNBLOCK_RENEWAL_SUCCESS,
  unblockRenewalDevice,
  unblockRenewalDeviceProvider,
} from '../../actions/inventory';
import SquareButton from '../../components/Button/button';
import Spinner from '../../components/Spinner';
import { fromISOStringToFormattedDate } from '../../utils/CalendarUtil';
import './style.scss';

interface IRenewalHistoryProps extends ICommonProps {
  deviceId: number;
  customerId: number;
  fetchDeviceRenwalHistory: any;
  fetchDeviceRenwalHistoryCustomer: any;
  user: ISuperUser;
  unblockRenewalDevice: any;
  unblockRenewalDeviceProvider: any;
  addSuccessMessage: TShowSuccessMessage;
  addErrorMessage: TShowErrorMessage;
}

interface IRenewalHistoryState {
  rows: any;
  success: boolean;
  isLoading: boolean;
  renewalStatus: string;
}

class RenewalHistory extends React.Component<
  IRenewalHistoryProps,
  IRenewalHistoryState
> {
  constructor(props: IRenewalHistoryProps) {
    super(props);

    this.state = {
      rows: [],
      success: false,
      isLoading: false,
      renewalStatus: '',
    };
  }
  componentDidMount() {
    if (this.props.user && this.props.user.type === 'provider') {
      this.setState({
        isLoading: true,
      });
      this.props
        .fetchDeviceRenwalHistory(
          this.props.customerId
            ? this.props.customerId
            : this.props.location.pathname.substr(
                this.props.location.pathname.lastIndexOf('/') + 1
              ),
          this.props.deviceId
        )
        .then(action => {
          if (action.type === RENEWAL_HISTORY_SUCCESS) {
            this.setState({
              rows: action.response.history,
              renewalStatus: action.response.renewal_status,
              success: true,
              isLoading: false,
            });
          }
        });
    } else {
      this.setState({
        isLoading: true,
      });
      this.props
        .fetchDeviceRenwalHistoryCustomer(this.props.deviceId)
        .then(action => {
          if (action.type === RENEWAL_HISTORY_SUCCESS) {
            this.setState({
              rows: action.response.history,
              renewalStatus: action.response.renewal_status,
              success: true,
              isLoading: false,
            });
          }
        });
    }
  }

  onClick = e => {
    this.props.unblockRenewalDevice(this.props.deviceId).then(action => {
      if (action.type === UNBLOCK_RENEWAL_SUCCESS) {
        this.props.addSuccessMessage('Successfull !!!');
        this.props
          .fetchDeviceRenwalHistoryCustomer(this.props.deviceId)
          .then(a => {
            if (a.type === RENEWAL_HISTORY_SUCCESS) {
              this.setState({
                rows: a.response.history,
                renewalStatus: action.response.renewal_status,
                success: true,
                isLoading: false,
              });
            }
          });
      } else if (action.type === UNBLOCK_RENEWAL_FAILURE) {
        this.props.addErrorMessage('Error on Renewal Unblock.');
      }
    });
  };

  onClickUnblockProvider = e => {
    this.props
      .unblockRenewalDeviceProvider(this.props.customerId, this.props.deviceId)
      .then(action => {
        if (action.type === UNBLOCK_RENEWAL_SUCCESS) {
          this.props.addSuccessMessage('Successfull !!!');
          this.props
            .fetchDeviceRenwalHistory(
              this.props.customerId
                ? this.props.customerId
                : this.props.location.pathname.substr(
                    this.props.location.pathname.lastIndexOf('/') + 1
                  ),
              this.props.deviceId
            )
            .then(a => {
              if (a.type === RENEWAL_HISTORY_SUCCESS) {
                this.setState({
                  rows: a.response.history,
                  renewalStatus: a.response.renewal_status,
                  success: true,
                  isLoading: false,
                });
              }
            });
        } else if (action.type === UNBLOCK_RENEWAL_FAILURE) {
          this.props.addErrorMessage('Error on Renewal Unblock.');
        }
      });
  };

  render() {
    return (
      <div className="renewal-history">
        <div className={`${this.state.isLoading ? 'loader modal-loader' : ''}`}>
          <Spinner show={this.state.isLoading} />
        </div>
        {!this.state.isLoading &&
          this.state.rows &&
          this.state.rows.length > 0 && (
            <div>
              {this.props.user &&
                this.props.user.type === 'customer' &&
                (this.state.renewalStatus === 'Hold' ||
                  this.state.renewalStatus === 'Do Not Renew') && (
                  <div className="renewal-status">
                    Renewal Status: {this.state.renewalStatus}
                    <SquareButton
                      onClick={this.onClick}
                      content="Unblock Renewal"
                      bsStyle={"primary"}
                      className="unblock-renewal"
                    />
                  </div>
                )}
              {this.props.user &&
                this.props.user.type === 'provider' &&
                (this.state.renewalStatus === 'Hold' ||
                  this.state.renewalStatus === 'Do Not Renew') && (
                  <div className="renewal-status">
                    Renewal Status: {this.state.renewalStatus}
                    <SquareButton
                      onClick={this.onClickUnblockProvider}
                      content="Unblock Renewal"
                      bsStyle={"primary"}
                      className="unblock-renewal"
                    />
                  </div>
                )}
              <table className="table-renewal-history">
                <tbody>
                  <tr>
                    <th className="action">Action</th>
                    <th className="author">Author</th>
                    <th className="created-on">Create Date</th>
                    <th className="notes">notes</th>
                  </tr>

                  {this.state.rows.map((row, index) => (
                    <tr key={index}>
                      <td className="action">{row.action}</td>
                      <td className="author">{row.author}</td>
                      <td className="created-on">
                        {fromISOStringToFormattedDate(row.created_on)}
                      </td>
                      <td className="note">
                        {row.notes && (
                          <span aria-label={row.notes}>
                            <div className="arrow" />
                            <img alt="" src="/assets/icons/notepad.png" />
                          </span>
                        )}

                        {!row.notes && <span className="blank"> - </span>}
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          )}
        {!this.state.isLoading &&
          this.state.rows &&
          this.state.rows.length === 0 && (
            <div className="no-renewal-history">
              No renewal history available.
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customerId: state.customer.customerId,
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchDeviceRenwalHistory: (customerId: number, deviceId: number) =>
    dispatch(fetchDeviceRenwalHistory(customerId, deviceId)),
  fetchDeviceRenwalHistoryCustomer: (deviceId: number) =>
    dispatch(fetchDeviceRenwalHistoryCustomer(deviceId)),
  unblockRenewalDevice: (deviceId: number) =>
    dispatch(unblockRenewalDevice(deviceId)),
  unblockRenewalDeviceProvider: (customerId: number, deviceId: number) =>
    dispatch(unblockRenewalDeviceProvider(customerId, deviceId)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RenewalHistory);
