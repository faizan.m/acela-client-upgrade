import { cloneDeep } from "lodash";
import React from "react";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import SelectInput from "../../components/Input/Select/select";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import { commonFunctions } from "../../utils/commonFunctions";

interface IAddOpportunityProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newDevice: any) => void;
  stages: any[];
  types: any[];
  isLoading: boolean;
  errorList: any;
  statusesList: any[];
  businessList: any[];
  opportunity: IOpportunity;
  terretoryMembers: any[];
  expirationYear: string;
  currentUserEmail: any;
  smartNetSetting: ISmartNetSetting;
}

interface IOpportunity {
  name: String;
  type_id: number;
  customer_id: String;
  inside_rep_id: string;
  business_unit_id: number;
  close_date: String;
  contact_id: String;
  status_id: number;
}
interface IAddOpportunityState {
  types: any[];
  opportunity: IOpportunity;
  error: {
    stage_id: IFieldValidation;
    type_id: IFieldValidation;
    name: IFieldValidation;
    customer_id: IFieldValidation;
    inside_rep_id: IFieldValidation;
    business_unit_id: IFieldValidation;
    status_id: IFieldValidation;
    close_date: IFieldValidation;
  };
}

export default class AddOpportunity extends React.Component<
  IAddOpportunityProps,
  IAddOpportunityState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IAddOpportunityProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => {
    const smartNetSetting = this.props.smartNetSetting;
    return {
      opportunity: {
        type_id: smartNetSetting.opportunity_type_id,
        name: `WR -SmartNet Renewal-(${this.props.expirationYear})`,
        customer_id: null,
        inside_rep_id: smartNetSetting.opportunity_default_inside_sales_rep,
        business_unit_id: smartNetSetting.opportunity_business_unit_id,
        close_date: null,
        contact_id: null,
        status_id: smartNetSetting.opportunity_status_id,
      },
      error: {
        stage_id: { ...AddOpportunity.emptyErrorState },
        type_id: { ...AddOpportunity.emptyErrorState },
        name: { ...AddOpportunity.emptyErrorState },
        customer_id: { ...AddOpportunity.emptyErrorState },
        inside_rep_id: { ...AddOpportunity.emptyErrorState },
        business_unit_id: { ...AddOpportunity.emptyErrorState },
        status_id: { ...AddOpportunity.emptyErrorState },
        close_date: { ...AddOpportunity.emptyErrorState },
      },
      types: [],
    };
  };

  componentDidUpdate(prevProps: IAddOpportunityProps) {
    const { errorList, opportunity } = this.props;
    if (errorList !== prevProps.errorList) {
      this.setValidationErrors(errorList);
    }
    if (opportunity !== prevProps.opportunity && opportunity) {
      const newOpportunity: IOpportunity = this.getEmptyState().opportunity;
      Object.assign(newOpportunity, opportunity);
      this.setState({ opportunity: newOpportunity });
    }
  }

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const newState = cloneDeep(this.state);
    (newState.opportunity[targetName] as any) = targetValue;
    this.setState(newState);
  };

  onClose = (e) => {
    this.props.onClose(e);
    const newState = this.getEmptyState();
    this.setState(newState);
  };

  setValidationErrors = (errorList) => {
    const newState: IAddOpportunityState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };
  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.opportunity.name ||
      this.state.opportunity.name.trim().length === 0
    ) {
      error.name.errorState = "error";
      error.name.errorMessage = "Enter a valid opportunity name";

      isValid = false;
    } else if (this.state.opportunity.name.length > 300) {
      error.name.errorState = "error";
      error.name.errorMessage =
        "Opportunity name should be less than 300 chars.";

      isValid = false;
    }

    // if (this.state.opportunity && !this.state.opportunity.stage_id) {
    //   error.stage_id.errorState = "error";
    //   error.stage_id.errorMessage = 'Please select stage';

    //   isValid = false;
    // }
    if (this.state.opportunity && !this.state.opportunity.type_id) {
      error.type_id.errorState = "error";
      error.type_id.errorMessage = "Please select type";

      isValid = false;
    }

    if (this.state.opportunity && !this.state.opportunity.status_id) {
      error.status_id.errorState = "error";
      error.status_id.errorMessage = "Please select status";

      isValid = false;
    }
    if (this.state.opportunity && !this.state.opportunity.business_unit_id) {
      error.business_unit_id.errorState = "error";
      error.business_unit_id.errorMessage = "Please select business";

      isValid = false;
    }

    if (this.state.opportunity && !this.state.opportunity.inside_rep_id) {
      error.inside_rep_id.errorState = "error";
      error.inside_rep_id.errorMessage = "Please select user";

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = (e) => {
    if (this.isValid()) {
      this.props.onSubmit(this.state.opportunity);
      const newState = this.getEmptyState();
      this.setState(newState);
    }
  };

  getTitle = () => {
    return "Create New Opportunity";
  };
  getCustomerUserOptions = () => {
    return this.props.terretoryMembers
      ? this.props.terretoryMembers.map((user, index) => ({
          value: user.id,
          label: `${user.first_name} ${user.last_name}`,
        }))
      : [];
  };
  getBody = () => {
    const types = this.props.types
      ? this.props.types.map((site) => ({
          value: site.id,
          label: site.label,
        }))
      : [];

    return (
      <div className="add-opportunity">
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`add-opportunity__body ${
            this.props.isLoading ? `loading` : ""
          }`}
        >
          <Input
            field={{
              label: "Name",
              type: "TEXT",
              value: this.state.opportunity.name,
              isRequired: true,
            }}
            width={6}
            placeholder="Enter name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />

          <div
            className="select-manufacturer
           field-section  col-md-6 col-xs-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.type_id.errorMessage ? `error-input` : ""
              }`}
            >
              <SelectInput
                name="type_id"
                value={this.state.opportunity.type_id}
                onChange={this.handleChange}
                options={types}
                searchable={true}
                placeholder="Select type"
                clearable={false}
              />
            </div>
            {this.state.error.type_id.errorMessage && (
              <div className="field-error">
                {this.state.error.type_id.errorMessage}
              </div>
            )}
          </div>
          <Input
            field={{
              value: this.state.opportunity.status_id,
              label: "Status",
              type: "PICKLIST",
              isRequired: true,
              options: this.props.statusesList,
            }}
            width={6}
            name={"status_id"}
            placeholder="Select Status"
            onChange={(e) => this.handleChange(e)}
            className="select-type"
            error={this.state.error.status_id}
          />
          <Input
            field={{
              value: this.state.opportunity.business_unit_id,
              label: "Select business",
              type: "PICKLIST",
              isRequired: true,
              options: this.props.businessList,
            }}
            width={6}
            name={"business_unit_id"}
            placeholder="Select Business"
            onChange={(e) => this.handleChange(e)}
            className="select-type"
            error={this.state.error.business_unit_id}
          />
          <Input
            field={{
              value: this.state.opportunity.inside_rep_id,
              label: "Inside Sales Representative",
              type: "PICKLIST",
              isRequired: true,
              options: this.getCustomerUserOptions(),
            }}
            width={6}
            name={"inside_rep_id"}
            placeholder="Select User"
            onChange={(e) => this.handleChange(e)}
            className="select-type"
            error={this.state.error.inside_rep_id}
          />

          <div>
            {this.state.error.customer_id.errorMessage && (
              <div className="field-error">
                {this.state.error.customer_id.errorMessage}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-opportunity__footer
      ${this.props.isLoading ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Submit"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-opportunity"
      />
    );
  }
}
