import React from 'react';
import { connect } from 'react-redux';

import IconButton from '../../components/Button/iconButton';
import Table from '../../components/Table/table';
import { saveFileFromURL } from '../../utils/download';
import { getSmartNetSettingsAttachments, addSmartNetSettingsAttachments, deleteSmartNetSettingsAttachments, GET_SMART_NET_ATTACH_FAILURE, GET_SMART_NET_ATTACH_SUCCESS } from '../../actions/setting';
import NewUploadFile from './../ServiceRequest/newUploadFile';
import { getDateTimeWithFormat } from '../../utils/CalendarUtil';
import SquareButton from '../../components/Button/button';
import DeleteButton from '../../components/Button/deleteButton';

interface IAttachmentProps extends ICommonProps {
  attachmentsList?: any;
  getSmartNetSettingsAttachments?: any;
  user?: ISuperUser;
  addSmartNetSettingsAttachments?: any;
  editSmartNetSettingsAttachments?: any;
  deleteSmartNetSettingsAttachments?: any;
  smartNetSetting?: any;
  isFetching?: any;
  isFetchingType?: any;
}

interface IAttachmentState {
  downloadingIds: number[];
  dataFile: any;
  isUploadVisible: boolean;
  uploadFile: any;
}

class Attachments extends React.Component<IAttachmentProps, IAttachmentState> {
  constructor(props: IAttachmentProps) {
    super(props);
    this.state = {
      downloadingIds: [],
      dataFile: null,
      uploadFile: null,
      isUploadVisible: false
    };
  }

  componentDidMount() {
    if (this.props.smartNetSetting && this.props.smartNetSetting.id) {
      this.props.getSmartNetSettingsAttachments();
    }
  }

  componentDidUpdate(prevProps: IAttachmentProps) {
    if (
      this.props.smartNetSetting &&
      this.props.smartNetSetting !== prevProps.smartNetSetting &&
      this.props.smartNetSetting.id
    ) {
      this.props.getSmartNetSettingsAttachments();
    }
  }

  handleFile = (data) => {
    this.props.addSmartNetSettingsAttachments(data)
      .then(action => {
        if (action.type === GET_SMART_NET_ATTACH_SUCCESS) {
          if (this.props.smartNetSetting && this.props.smartNetSetting.id) {
            this.props.getSmartNetSettingsAttachments();
          }
          this.setState({ isUploadVisible: false });
        }
        if (action.type === GET_SMART_NET_ATTACH_FAILURE) {
          this.setState({ isUploadVisible: false });
        }
      });
  };

  onDeleteRowClick = (cell, e) => {
    this.props.deleteSmartNetSettingsAttachments(cell.original.id)
      .then(action => {
        if (action.type === GET_SMART_NET_ATTACH_SUCCESS) {
          this.props.getSmartNetSettingsAttachments();
        }
      });
  };

  openUpload = () => {
    if(this.props.smartNetSetting && this.props.smartNetSetting.id){
      this.setState({ isUploadVisible: true });
    }
  };

  closeUpload = () => {
    // this.getLatest();
    this.setState({ isUploadVisible: false });
  };

  render() {
    const columns: any = [
      {
        accessor: 'name',
        Header: 'Name',
        Cell: cell => <div className="pl-15">{cell.value}</div>,
      },
      {
        accessor: 'created_on',
        Header: 'Attached On',
        Cell: cell => <div>{getDateTimeWithFormat(cell.value)}</div>,

      },
      {
        accessor: 'attachment',
        Header: 'Actions',
        sortable: false,
        Cell: cell => <div>{getLoadingMarkUp(cell)}</div>,
      },
    ];

    const getLoadingMarkUp = cell => {

      return (
        <>
          <IconButton
            icon="download.png"
            onClick={() => {
              saveFileFromURL(cell.value)
            }}
            className="download-attachment"
            title="Download Attachment"
          />
          <DeleteButton onClick={e => this.onDeleteRowClick(cell, e)} />
        </>
      );
    };

    let rows: any[] = [];
    if (this.props.attachmentsList.length > 0) {
      rows = this.props.attachmentsList;
    }

    return (
      <div className="attachment-details">
        <div className="attachment-header">
          <SquareButton
            content={
              <div
                onClick={this.openUpload}
              >
                <img
                  className="attachments__icon__upload"
                  src="/assets/icons/cloud.png"
                />
                Upload Attachment
               </div>
            }
            bsStyle={"primary"}
            onClick={this.openUpload}
            disabled={this.props.smartNetSetting && this.props.smartNetSetting.id ? false : true}
          />
        </div>
        <div className="attachment-body">
          <Table
            columns={columns}
            rows={rows}
            loading={this.props.isFetching || this.props.isFetchingType}
          />
        </div>
        <NewUploadFile
          isVisible={this.state.isUploadVisible}
          close={this.closeUpload}
          onSubmit={this.handleFile}
          loading={this.props.isFetching || this.props.isFetchingType}
          titleField="name"
          fileField="attachment"
        />

      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  attachmentsList: state.setting.attachmentsList,
  isFetching: state.setting.isFetching,
  isFetchingType: state.setting.isFetchingType,
  smartNetSetting: state.setting.smartNetSetting,
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getSmartNetSettingsAttachments: () => dispatch(getSmartNetSettingsAttachments()),
  addSmartNetSettingsAttachments: (attachment: any) => dispatch(addSmartNetSettingsAttachments(attachment)),
  deleteSmartNetSettingsAttachments: (id: any) => dispatch(deleteSmartNetSettingsAttachments(id)),
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(Attachments);
