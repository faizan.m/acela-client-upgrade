import React, { Component } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import { addSuccessMessage, addErrorMessage } from "../../actions/appState";
import {
  getPurchaseOrderStatusList,
  editPurchaseOrderSettings,
  getPurchaseOrderSettings,
  GET_PO_STATUS_SUCCESS
} from '../../actions/setting';
import {
  getBusinessUnits,
  FETCH_BUSINESS_UNITS_SUCCESS,
  getServiceBoards,
  FETCH_SERVICE_BOARDS_SUCCESS,
} from "../../actions/sow";
import {
  fetchOrderTrackingDashboardSettings,
  postOrderTrackingDashboardSettings,
  updateOrderTrackingDashboardSettings,
  ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS,
} from "../../actions/setting";
import {
  fetchCustomFieldsList,
  fetchProviderBoardStatus,
  FETCH_CUSTOM_FS_SUCCESS,
  FETCH_PROVIDER_BOARD_STATUS_SUCCESS,
} from "../../actions/provider/integration";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Spinner from "../../components/Spinner";
import Checkbox from "../../components/Checkbox/checkbox";
import Collapsible from "../../components/Collapsible";

interface IISettingSmartNetState {
  isOpen: boolean;
  loading: boolean;
  po_custom_field_id: number;
  ticket_status_ids: number[];
  selectedBusinessUnit: number;
  selectedServiceBoard: number[];
  poStatusList: IPickListOptions[];
  isFetchingSettingOptions: boolean;
  purchase_order_status_ids: number[];
  customFieldsOption: IPickListOptions[];
  serviceBoardOptions: IPickListOptions[];
  ticketStatusOptions: IPickListOptions[];
  businessUnitsOptions: IPickListOptions[];
  allTicketStatusOptions: IPickListOptions[];
  ticketOptionsMap: {
    [boardId: number]: number[];
  };
  errors: {
    purchase_order_status_ids: IFieldValidation;
    ticket_status_ids: IFieldValidation;
    po_custom_field_id: IFieldValidation;
  };
}

interface IISettingSmartNetProps extends ICommonProps {
  isFetching: boolean;
  fetchOrderTrackingDashboardSettings: () => Promise<any>;
  postOrderTrackingDashboardSettings: (
    data: IOrderTrackingDashboardSetting
  ) => Promise<any>;
  updateOrderTrackingDashboardSettings: (
    data: IOrderTrackingDashboardSetting
  ) => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  addErrorMessage: TShowErrorMessage;
  getBusinessUnits: () => Promise<any>;
  getServiceBoards: () => Promise<any>;
  fetchCustomFieldsList: () => Promise<any>;
  getPurchaseOrderStatusList: () => Promise<any>;
  fetchProviderBoardStatus: (id: number) => Promise<any>;
}

class OrderTrackingDashboardSetting extends Component<
  IISettingSmartNetProps,
  IISettingSmartNetState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    isFetchingSettingOptions: false,
    purchase_order_status_ids: [],
    po_custom_field_id: null,
    ticket_status_ids: [],
    customFieldsOption: [],
    ticketStatusOptions: [],
    allTicketStatusOptions: [],
    ticketOptionsMap: {},
    poStatusList: [],
    loading: false,
    isOpen: false,
    errors: {
      purchase_order_status_ids: {
        ...OrderTrackingDashboardSetting.emptyErrorState,
      },
      ticket_status_ids: { ...OrderTrackingDashboardSetting.emptyErrorState },
      po_custom_field_id: { ...OrderTrackingDashboardSetting.emptyErrorState },
    },
    businessUnitsOptions: [],
    serviceBoardOptions: [],
    selectedServiceBoard: [],
    selectedBusinessUnit: undefined,
  });

  componentDidMount() {
    this.setState({ isFetchingSettingOptions: true });
    this.props.getBusinessUnits().then((action) => {
      if (action.type === FETCH_BUSINESS_UNITS_SUCCESS) {
        const businessUnitsOptions: IPickListOptions[] = action.response.map(
          (data) => ({
            value: data.id,
            label: `${data.label}`,
          })
        );
        this.setState({
          isFetchingSettingOptions: false,
          businessUnitsOptions,
        });
      }
      this.setState({ isFetchingSettingOptions: false });
    });
    this.props.getServiceBoards().then((action) => {
      if (action.type === FETCH_SERVICE_BOARDS_SUCCESS) {
        const serviceBoardOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.label}`,
        }));
        this.setState({ isFetchingSettingOptions: false, serviceBoardOptions });
      }
      this.setState({ isFetchingSettingOptions: false });
    });
    this.fetchOrderTrackingDashboardSettings();
    this.getPOStatusList();
    this.fetchCustomFieldsList();
  }

  getPOStatusList = () => {
    this.setState({ loading: true });
    this.props.getPurchaseOrderStatusList().then((action) => {
      if (action.type === GET_PO_STATUS_SUCCESS) {
        const statusList = action.response;
        const poStatusList = statusList
          ? statusList.map((c) => ({
              value: c.id,
              label: c.name,
            }))
          : [];
        this.setState({ poStatusList, loading: false });
      }
    });
  };

  fetchCustomFieldsList = () => {
    this.setState({ loading: true });
    this.props.fetchCustomFieldsList().then((action) => {
      if (action.type === FETCH_CUSTOM_FS_SUCCESS) {
        const statusList = action.response;
        const customFieldsOption = statusList
          ? statusList.map((c) => ({
              value: c.id,
              label: c.caption,
            }))
          : [];
        this.setState({ customFieldsOption, loading: false });
      }
    });
  };

  fetchOrderTrackingDashboardSettings = () => {
    this.props.fetchOrderTrackingDashboardSettings().then((action) => {
      if (action.type === ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS) {
        const {
          business_unit_id,
          service_boards,
          purchase_order_status_ids,
          ticket_status_ids,
          po_custom_field_id,
        }: IOrderTrackingDashboardSetting = action.response;
        this.setState({
          isFetchingSettingOptions: false,
          selectedBusinessUnit: business_unit_id,
          selectedServiceBoard: service_boards,
          purchase_order_status_ids: purchase_order_status_ids,
          ticket_status_ids: ticket_status_ids,
          po_custom_field_id: po_custom_field_id,
        });
        this.getTicketStatuses(service_boards);
      }
      this.setState({ isFetchingSettingOptions: false });
    });
  };

  getTicketStatuses = (service_board_ids: number[]) => {
    const boardTickekIdsMap = {};
    this.setState({ isFetchingSettingOptions: true });

    service_board_ids.map((board_id) => {
      const boardTickekIds = [];
      this.props.fetchProviderBoardStatus(board_id).then((action) => {
        if (action.type === FETCH_PROVIDER_BOARD_STATUS_SUCCESS) {
          const serviceBoardTickets = [];

          const boardName = this.state.serviceBoardOptions.find(
            (obj) => obj.value == board_id
          );

          action.response.map((data) => {
            const ticket: IPickListOptions = {
              value: data.id,
              label: `${data.label} ${boardName ? `- ${boardName.label}` : ""}`,
            };
            serviceBoardTickets.push(ticket);
            boardTickekIds.push(data.id);
          });
          const ticketStatusOptions: IPickListOptions[] = [
            ...this.state.ticketStatusOptions,
            ...serviceBoardTickets,
          ];
          const allTicketStatusOptions = [...ticketStatusOptions];

          this.setState(
            {
              isFetchingSettingOptions: false,
              ticketStatusOptions,
              allTicketStatusOptions,
            },
            () => {
              boardTickekIdsMap[board_id] = boardTickekIds;
              this.setState({
                ticketOptionsMap: {
                  ...this.state.ticketOptionsMap,
                  ...boardTickekIdsMap,
                },
              });
            }
          );
        }
      });
    });
  };

  saveOrderTrackingDashboardSettings = () => {
    const data = {
      business_unit_id: this.state.selectedBusinessUnit,
      service_boards: this.state.selectedServiceBoard,
      purchase_order_status_ids: this.state.purchase_order_status_ids,
      ticket_status_ids: this.state.ticket_status_ids,
      po_custom_field_id: this.state.po_custom_field_id,
    } as IOrderTrackingDashboardSetting;
    const isValid = this.validateFormOperations();
    if (!isValid) return;

    if (!this.state.selectedBusinessUnit) {
      this.props
        .postOrderTrackingDashboardSettings({
          ...data,
          service_ticket_create_date: null,
        })
        .then((action) => {
          if (action.type === ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS) {
            this.fetchOrderTrackingDashboardSettings();
            this.props.addSuccessMessage(
              "Saved service board settings successfully."
            );
          } else {
            this.props.addErrorMessage(
              "Error occured. Please try again later."
            );
          }
          this.setState({ isFetchingSettingOptions: false });
        });
    } else {
      this.props.updateOrderTrackingDashboardSettings(data).then((action) => {
        if (action.type === ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS) {
          this.fetchOrderTrackingDashboardSettings();
          this.props.addSuccessMessage(
            "Updated service board settings successfully."
          );
        } else {
          this.props.addErrorMessage("Error occured. Please try again later.");
        }
        this.setState({ isFetchingSettingOptions: false });
      });
    }
  };

  validateFormOperations = () => {
    const errors = this.getEmptyState().errors;
    let isValid = true;

    if (
      !this.state.purchase_order_status_ids ||
      this.state.purchase_order_status_ids.length === 0
    ) {
      isValid = false;
      errors.purchase_order_status_ids.errorState = "error";
      errors.purchase_order_status_ids.errorMessage =
        "Please select a purchase order status";
      this.props.addErrorMessage(
        "Please select atleast one purchase order status"
      );
    }

    if (
      !this.state.ticket_status_ids ||
      this.state.ticket_status_ids.length === 0
    ) {
      isValid = false;
      errors.ticket_status_ids.errorState = "error";
      errors.ticket_status_ids.errorMessage = "Please select a ticket status";
      this.props.addErrorMessage("Please select atleast one ticket status");
    }

    this.setState({ errors });

    return isValid;
  };

  updateServiceTicketOptions = (boardId: number, isAdded: boolean) => {
    if (isAdded) {
      const ticketIds = this.state.ticketOptionsMap[boardId] || [];
      if (ticketIds.length > 0) {
        // push data to options
        const newTicketStatusOptions = this.state.allTicketStatusOptions.filter(
          (option) => ticketIds.indexOf(option.value) > -1
        );
        this.setState({
          ticketStatusOptions: [
            ...this.state.ticketStatusOptions,
            ...newTicketStatusOptions,
          ],
        });
      } else {
        // API call for board statuses.
        this.getTicketStatuses([boardId]);
      }
    } else {
      const ticketIds = this.state.ticketOptionsMap[boardId] || [];
      const filteredTicketOptions = this.state.ticketStatusOptions.filter(
        (option) => ticketIds.indexOf(option.value) <= -1
      );
      this.setState({ ticketStatusOptions: filteredTicketOptions });
    }
  };

  onChecboxChanged = (
    e: React.ChangeEvent<HTMLInputElement>,
    optionId: number
  ) => {
    const isChecked = e.target.checked;
    if (isChecked) {
      this.setState({
        selectedServiceBoard: [...this.state.selectedServiceBoard, optionId],
      });
    } else {
      const selectedServiceBoard = this.state.selectedServiceBoard.filter(
        (item) => {
          return item !== optionId;
        }
      );
      this.setState({ selectedServiceBoard });
    }
    this.updateServiceTicketOptions(optionId, isChecked);
  };

  renderORServiceBoardSetting = () => {
    return (
      <div style={{ display: "flex" }}>
        <div className="board-mapping__options">
          <label className="board-mapping__options-title">
            {`Default Service Boards`}
            <span className="field__label-require" />
          </label>
          <div className="board-mapping__options-content">
            {this.state.serviceBoardOptions.map((option, index) => {
              return (
                <div key={index} className="board-mapping__options-option">
                  <Checkbox
                    isChecked={
                      this.state.selectedServiceBoard.indexOf(option.value) > -1
                    }
                    name="option"
                    onChange={(e) => this.onChecboxChanged(e, option.value)}
                  >
                    {option.label}
                  </Checkbox>
                </div>
              );
            })}
          </div>
        </div>
        <div className="board-mapping__options board-mapping-radio">
          <label className="board-mapping__options-title">
            {`Default Business Unit`}
            <span className="field__label-require" />
          </label>
          <div className="board-mapping__options-content board-mapping-radio__options-content">
            <div className="board-mapping__options-option">
              <Input
                field={{
                  value: this.state.selectedBusinessUnit,
                  label: "",
                  type: "RADIO",
                  isRequired: false,
                  options: this.state.businessUnitsOptions,
                }}
                width={12}
                name={"or-settings-service-board"}
                onChange={(e) =>
                  this.setState({
                    selectedBusinessUnit: parseInt(e.target.value),
                  })
                }
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  handleChangeOperations = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;
    this.setState(newState);
  };

  renderPOEmailStatus = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Purchase Order Status",
              type: "PICKLIST",
              value: this.state.purchase_order_status_ids,
              options: this.state.poStatusList,
              isRequired: true,
            }}
            width={6}
            multi={true}
            name="purchase_order_status_ids"
            onChange={(e) => this.handleChangeOperations(e)}
            error={this.state.errors.purchase_order_status_ids}
            placeholder={`Select`}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  };

  renderTicketStatus = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Ticket Status",
              type: "PICKLIST",
              value: this.state.ticket_status_ids,
              options: this.state.ticketStatusOptions,
              isRequired: true,
            }}
            width={6}
            multi={true}
            name="ticket_status_ids"
            onChange={(e) => this.handleChangeOperations(e)}
            error={this.state.errors.ticket_status_ids}
            placeholder={`Select`}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  };

  renderCustomField = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Select the field that captures SO Number",
              type: "PICKLIST",
              value: this.state.po_custom_field_id,
              options: this.state.customFieldsOption,
              isRequired: true,
            }}
            width={6}
            multi={false}
            name="po_custom_field_id"
            onChange={(e) => this.handleChangeOperations(e)}
            error={this.state.errors.po_custom_field_id}
            placeholder={`Select`}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="operations-setting">
        {(this.props.isFetching || this.state.isFetchingSettingOptions) && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <Collapsible label={"Service Board Setting"} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderORServiceBoardSetting()}
          </ul>
        </Collapsible>
        <Collapsible label={"Purchase Order Status"} isOpen={this.state.isOpen}>
          <ul className="namespace-list">{this.renderPOEmailStatus()}</ul>
        </Collapsible>
        <Collapsible label={"Ticket Status"} isOpen={this.state.isOpen}>
          <ul className="namespace-list">{this.renderTicketStatus()}</ul>
        </Collapsible>
        <Collapsible label={"Custom Fields"} isOpen={this.state.isOpen}>
          <ul className="namespace-list">{this.renderCustomField()}</ul>
        </Collapsible>
        <div className="po-setting-save-section">
          <SquareButton
            content="Save"
            bsStyle={"primary"}
            onClick={this.saveOrderTrackingDashboardSettings}
            className="submit-email-data"
            disabled={this.props.isFetching}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.setting.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  getPurchaseOrderStatusList: () => dispatch(getPurchaseOrderStatusList()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  editPurchaseOrderSettings: (data: any) => dispatch(editPurchaseOrderSettings(data)),
  getPurchaseOrderSettings: () => dispatch(getPurchaseOrderSettings()),
  getBusinessUnits: () => dispatch(getBusinessUnits()),
  getServiceBoards: () => dispatch(getServiceBoards()),
  fetchOrderTrackingDashboardSettings: () =>
    dispatch(fetchOrderTrackingDashboardSettings()),
  postOrderTrackingDashboardSettings: (data: IOrderTrackingDashboardSetting) =>
    dispatch(postOrderTrackingDashboardSettings(data)),
  updateOrderTrackingDashboardSettings: (
    data: IOrderTrackingDashboardSetting
  ) => dispatch(updateOrderTrackingDashboardSettings(data)),
  fetchProviderBoardStatus: (id: number) =>
    dispatch(fetchProviderBoardStatus(id)),
  fetchCustomFieldsList: () => dispatch(fetchCustomFieldsList()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTrackingDashboardSetting);
