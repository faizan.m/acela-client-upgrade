import moment from "moment";
import { Moment } from "moment";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import {
  fetchOrderTrackingDashboardSettings,
  ORDER_TRACKING_DASHBOARD_SETTING_FAILURE,
  ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS,
  updateOrderTrackingDashboardSettings,
} from "../../actions/setting";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";

interface SoPoSyncProps {
  fetchOrderTrackingDashboardSettings: () => Promise<any>;
  updateOrderTrackingDashboardSettings: (
    data: IOrderTrackingDashboardSetting
  ) => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  addErrorMessage: TShowErrorMessage;
}

const SoPoSyncSettings: React.FC<SoPoSyncProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [srvcTktSyncDate, setSrvcTktSyncDate] = useState<Moment>(null);

  useEffect(() => {
    fetchServiceTicketDate();
  }, []);

  const fetchServiceTicketDate = () => {
    setLoading(true);
    props
      .fetchOrderTrackingDashboardSettings()
      .then((action) => {
        if (action.type === ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS) {
          const {
            service_ticket_create_date,
          }: IOrderTrackingDashboardSetting = action.response;
          setSrvcTktSyncDate(
            service_ticket_create_date
              ? moment(service_ticket_create_date)
              : null
          );
        } else if (action.type === ORDER_TRACKING_DASHBOARD_SETTING_FAILURE) {
          props.addErrorMessage(
            "Please save the order tracking dashboard settings first!"
          );
        }
      })
      .finally(() => setLoading(false));
  };

  const saveSoPoSettings = () => {
    // In Future, remove the dependency of service_ticket_create_date
    // from order tracking dashboard settings
    setLoading(true);
    const data = {
      service_ticket_create_date: srvcTktSyncDate.format("YYYY-MM-DD"),
    } as IOrderTrackingDashboardSetting;

    props
      .updateOrderTrackingDashboardSettings(data)
      .then((action) => {
        if (action.type === ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS) {
          props.addSuccessMessage(
            "Updated SO PO Data Sync Settings successfully!"
          );
        } else {
          props.addErrorMessage("Error occured. Please try again later.");
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChangeDate = (event: {
    target: {
      name: string;
      value: Moment;
    };
  }) => {
    setSrvcTktSyncDate(event.target.value);
  };

  return (
    <div className="so-po-sync-settings-container">
      <Spinner show={loading} className="so-po-sync-spinner" />
      <div className="col-md-12 row">
        <Input
          field={{
            value: srvcTktSyncDate,
            label: "Sync Service Ticket Created After",
            type: "DATE",
            isRequired: false,
          }}
          width={6}
          name="srvcTktSyncDate"
          onChange={handleChangeDate}
          placeholder="Choose Date"
          showTime={false}
          showDate={true}
          disablePrevioueDates={false}
          disableFutureDates={true}
        />
      </div>
      <SquareButton
        content="Save"
        bsStyle={"primary"}
        onClick={saveSoPoSettings}
        className="submit-so-po-sync"
        disabled={loading}
      />
    </div>
  );
};
const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchOrderTrackingDashboardSettings: () =>
    dispatch(fetchOrderTrackingDashboardSettings()),
  updateOrderTrackingDashboardSettings: (
    data: IOrderTrackingDashboardSetting
  ) => dispatch(updateOrderTrackingDashboardSettings(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SoPoSyncSettings);
