import React, { Component } from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput";(New Component)
import { cloneDeep, isEqual } from "lodash";
import { fetchAllProviderUsers } from "../../actions/provider/user";
import {
  collectionsSettingsCRUD,
  fetchOrderTrackingSettings,
  FETCH_ORDER_TRACKING_SETTING_SUCCESS,
  getBillingStatusesOptions,
  sendCollectionsNoticeEmail,
  getCollectionsEmailPreview,
  GET_COLLECTIONS_PREVIEW_SUCCESS,
  SEND_COLLECTIONS_NOTICE_SUCCESS,
  GET_COLLECTIONS_SETTINGS_FAILURE,
  GET_COLLECTIONS_SETTINGS_SUCCESS,
} from "../../actions/setting";
import {
  uploadImage,
  UPLOAD_IMAGE_FAILURE,
  UPLOAD_IMAGE_SUCCESS,
} from "../../actions/sow";
import { fetchCustomersShort } from "../../actions/customer";
import {
  CONFIG_TASK_STATUS_SUCCESS,
  fetchTaskStatusConfig,
} from "../../actions/configuration";
import {
  addErrorMessage,
  addInfoMessage,
  addSuccessMessage,
  addWarningMessage,
} from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import HTMLViewer from "../../components/HTMLViewer/HTMLViewer";
import ModalBase from "../../components/ModalBase/modalBase";
import { commonFunctions } from "../../utils/commonFunctions";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import Checkbox from "../../components/Checkbox/checkbox";

interface ICollectionsProps {
  collectionsSettingsCRUD: (
    request: "get" | "post" | "put",
    data?: ICollectionsSettings
  ) => Promise<any>;
  providerUsers: ISuperUser[];
  billingStatusOptions: IPickListOptions[];
  customers: ICustomerShort[];
  isFetchingCustomers: boolean;
  fetchCustomers: () => void;
  fetchAllProviderUsers: () => void;
  getBillingStatusesOptions: () => void;
  fetchTaskStatus: (id: number) => Promise<any>;
  getCollectionsEmailPreview: () => Promise<any>;
  sendCollectionsNoticeEmail: (
    crm_ids: number[],
    all_customers: boolean
  ) => Promise<any>;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  addWarningMessage: TShowWarningMessage;
  fetchOrderTrackingSettings: () => Promise<any>;
  uploadImage: (file: any, name: string) => Promise<any>;
}

interface ICollectionsState {
  firstSave: boolean;
  loading: boolean;
  users: number[];
  showModal: boolean;
  previewHTML: string;
  openPreview: boolean;
  sendingEmail: boolean;
  sendToAllCustomers: boolean;
  settings: ICollectionsSettings;
  prevSettings: ICollectionsSettings;
  isFetchingContactTypes: boolean;
  contactTypeOptions: IPickListOptions[];
  error: {
    weekly_send_schedule: IFieldValidation;
    send_to_email: IFieldValidation;
    send_from_email: IFieldValidation;
    approaching_due_date_ageing: IFieldValidation;
    email_body_template: IFieldValidation;
    billing_statuses: IFieldValidation;
    subject: IFieldValidation;
    contact_type_crm_id: IFieldValidation;
  };
}

class Collections extends Component<ICollectionsProps, ICollectionsState> {
  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  static SuccessMessage: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  static EmptySettings: ICollectionsSettings = {
    weekly_send_schedule: [],
    send_to_email: [],
    send_from_email: "",
    approaching_due_date_ageing: 0,
    email_body_template: "",
    billing_statuses: [],
    invoice_exclude_patterns: [],
    subject: "",
    contact_type_crm_id: null,
  };

  getEmptyState = () => ({
    users: [],
    loading: false,
    previewHTML: null,
    showModal: false,
    firstSave: false,
    contactTypeOptions: [],
    sendToAllCustomers: false,
    isFetchingContactTypes: false,
    openPreview: false,
    sendingEmail: false,
    settings: { ...Collections.EmptySettings },
    prevSettings: { ...Collections.EmptySettings },
    error: {
      weekly_send_schedule: { ...Collections.SuccessMessage },
      send_to_email: { ...Collections.SuccessMessage },
      send_from_email: { ...Collections.SuccessMessage },
      approaching_due_date_ageing: { ...Collections.SuccessMessage },
      email_body_template: { ...Collections.SuccessMessage },
      billing_statuses: { ...Collections.SuccessMessage },
      contact_type_crm_id: { ...Collections.SuccessMessage },
      subject: { ...Collections.SuccessMessage },
    },
  });

  componentDidMount() {
    this.setState({ loading: true });
    this.props
      .collectionsSettingsCRUD("get")
      .then((action) => {
        if (action.type === GET_COLLECTIONS_SETTINGS_SUCCESS) {
          let data: ICollectionsSettings = action.response;
          (data.billing_statuses as number[]) = data.billing_statuses.map(
            (el: { billing_status_id: number; billing_status_name: string }) =>
              el.billing_status_id
          );
          this.setState({ settings: data, prevSettings: data });
        } else if (action.type === GET_COLLECTIONS_SETTINGS_FAILURE) {
          this.setState({
            firstSave: true,
          });
        }
      })
      .finally(() => this.setState({ loading: false }));
    this.props.fetchAllProviderUsers();
    this.props.getBillingStatusesOptions();
    if (!this.props.customers) this.props.fetchCustomers();
    this.setState({ isFetchingContactTypes: true }, () => {
      this.props.fetchOrderTrackingSettings().then((action) => {
        if (action.type === FETCH_ORDER_TRACKING_SETTING_SUCCESS) {
          const contactTypeOptions = action.response.map((data) => ({
            value: data.id,
            label: `${data.description}`,
          }));
          this.setState({ isFetchingContactTypes: false, contactTypeOptions });
        }
        this.setState({ isFetchingContactTypes: false });
      });
    });
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      settings: { ...this.state.settings, [e.target.name]: e.target.value },
    });
  };

  handleChangeTags = (arr: string[]) =>
    this.setState({
      settings: {
        ...this.state.settings,
        invoice_exclude_patterns: arr,
      },
    });

  handleChangeMarkdown = (html: string) => {
    this.setState({
      settings: { ...this.state.settings, email_body_template: html },
    });
  };

  handleChangeUser = (e: { target: { value: number[] } }) => {
    this.setState({ users: e.target.value });
  };

  sendEmail = () => {
    this.setState({ sendingEmail: true }, () => {
      this.props
        .sendCollectionsNoticeEmail(
          this.state.users,
          this.state.sendToAllCustomers
        )
        .then((action) => {
          if (action.type === SEND_COLLECTIONS_NOTICE_SUCCESS) {
            const taskId = action.response.task_id;
            this.fetchTaskStatus(
              taskId,
              this.state.users,
              this.state.sendToAllCustomers
            );
            this.setState({
              showModal: false,
              sendToAllCustomers: false,
              users: [],
            });
            this.props.addInfoMessage(
              "We are checking & creating Collection Notices to send Email. It will just take a moment."
            );
          }
        })
        .finally(() => this.setState({ sendingEmail: false }));
    });
  };

  previewEmail = () => {
    this.setState({ loading: true }, () => {
      this.props
        .getCollectionsEmailPreview()
        .then((action) => {
          if (action.type === GET_COLLECTIONS_PREVIEW_SUCCESS) {
            this.setState({
              openPreview: true,
              previewHTML: action.response.html,
            });
          } else {
            this.props.addErrorMessage("Error fetching collections preview");
          }
        })
        .finally(() => this.setState({ loading: false }));
    });
  };

  closePreview = () => {
    this.setState({ openPreview: false, previewHTML: null });
  };

  fetchTaskStatus = (
    taskId: number,
    users: number[],
    allCustomers: boolean
  ) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then((action) => {
        if (action.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (action.response.status === "SUCCESS") {
            const res = action.response.result;
            if (res && res.operation_status !== "SUCCESS") {
              this.props.addErrorMessage(
                `An error occurred while sending emails!`
              );
            }
            const skipped_customers: string[] =
              res && res.payload && res.payload.skipped_customers
                ? res.payload.skipped_customers
                : [];
            if (skipped_customers.length) {
              let names: string = skipped_customers.join(", ");
              if (allCustomers) {
                this.props.addSuccessMessage(
                  "Collections Notice sent to all the customers with Invoices!"
                );
              } else {
                if (skipped_customers.length === users.length) {
                  this.props.addWarningMessage(
                    `Collections Notice not sent to the following customers as Invoices were not found: ${names}!`
                  );
                } else {
                  this.props.addWarningMessage(
                    `Collections Notice not sent to the following customers as Invoices were not found: ${names}!`
                  );
                  this.props.addSuccessMessage(
                    "Collections Notice sent to all the customers with Invoices!"
                  );
                }
              }
            } else {
              this.props.addSuccessMessage(
                `Collections Notice sent to all the selected customers with Invoices!`
              );
            }
          }
          if (action.response.status === "PENDING") {
            setTimeout(
              () => this.fetchTaskStatus(taskId, users, allCustomers),
              3000
            );
          }
          if (action.response.status === "FAILURE") {
            this.props.addErrorMessage(
              `An error occurred while sending emails!`
            );
          }
        }
      });
    }
  };

  onImageUpload = (e) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    this.setState({ loading: true });
    if (
      attachment &&
      attachment.type &&
      ["image/png", "image/jpg", "image/jpeg"].includes(attachment.type)
    ) {
      const data = new FormData();
      if (attachment) {
        data.append("filename", attachment);
        this.props.uploadImage(data, attachment).then((action) => {
          if (action.type === UPLOAD_IMAGE_SUCCESS) {
            this.props.addSuccessMessage("Image Uploaded Successfully!");
            let imageString = `<p><img src="${action.response.file_path}" style="max-width: 100%"></p>`;
            this.setState({
              loading: false,
              settings: {
                ...this.state.settings,
                email_body_template:
                  this.state.settings.email_body_template + imageString,
              },
            });
          }
          if (action.type === UPLOAD_IMAGE_FAILURE) {
            this.props.addErrorMessage("Image Upload Failed!");
            this.setState({ loading: false });
          }
        });
      }
    } else {
      this.setState({ loading: false });
      this.props.addErrorMessage("Invalid image format.");
    }
  };

  validateForm = (): boolean => {
    let isValid = true;
    const error = cloneDeep(this.state.error);
    if (!this.state.settings.send_from_email) {
      error.send_from_email = {
        errorState: "error",
        errorMessage: "This field is required",
      };
      isValid = false;
    }
    if (this.state.settings.send_to_email.length === 0) {
      error.send_to_email = {
        errorState: "error",
        errorMessage: "Please select at least one email address",
      };
      isValid = false;
    }
    if (!this.state.settings.subject.trim()) {
      error.subject = {
        errorState: "error",
        errorMessage: "Please enter a subject",
      };
      isValid = false;
    }
    if (!this.state.settings.contact_type_crm_id) {
      error.contact_type_crm_id = {
        errorState: "error",
        errorMessage: "This field is required",
      };
      isValid = false;
    }
    if (this.state.settings.weekly_send_schedule.length === 0) {
      error.weekly_send_schedule = {
        errorState: "error",
        errorMessage: "Please select at least one day",
      };
      isValid = false;
    }
    if (
      isNaN(parseInt(this.state.settings.approaching_due_date_ageing as any)) ||
      Number(this.state.settings.approaching_due_date_ageing) < 0
    ) {
      error.approaching_due_date_ageing = {
        errorState: "error",
        errorMessage: "Please enter a valid number",
      };
      isValid = false;
    }
    if (this.state.settings.billing_statuses.length === 0) {
      error.billing_statuses = {
        errorState: "error",
        errorMessage: "Please select at least one billing status",
      };
      isValid = false;
    }
    if (
      commonFunctions.isEditorEmpty(this.state.settings.email_body_template)
    ) {
      error.email_body_template = {
        errorState: "error",
        errorMessage: "This field is required",
      };
      isValid = false;
    }
    if (!isValid) this.setState({ error: error });
    return isValid;
  };

  onSubmit = () => {
    if (this.validateForm()) {
      const settings = cloneDeep(this.state.settings);
      settings.approaching_due_date_ageing = Number(
        this.state.settings.approaching_due_date_ageing
      );
      const billingStatusIds = new Set(settings.billing_statuses as number[]);
      settings.billing_statuses = [];
      this.props.billingStatusOptions.forEach((el) => {
        if (billingStatusIds.has(el.value)) {
          settings.billing_statuses.push({
            billing_status_id: el.value,
            billing_status_name: el.label as string,
          });
        }
      });
      this.setState({ loading: true }, () => {
        this.props
          .collectionsSettingsCRUD(
            this.state.firstSave ? "post" : "put",
            settings
          )
          .then((action) => {
            if (action.type === GET_COLLECTIONS_SETTINGS_SUCCESS) {
              this.props.addSuccessMessage(
                (this.state.firstSave ? "Saved" : "Updated") + " Successfully!"
              );
              this.setState({
                firstSave: false,
                prevSettings: cloneDeep(this.state.settings),
              });
            } else if (action.type === GET_COLLECTIONS_SETTINGS_FAILURE) {
              let errors = action.errorList.data;
              let validationErrors = cloneDeep(this.state.error);
              if (!Array.isArray(errors)) {
                Object.keys(errors).forEach((key) => {
                  (validationErrors[key] as IFieldValidation) = {
                    errorState: "error",
                    errorMessage: errors[key],
                  };
                });
                this.setState({ error: validationErrors });
              } else {
                this.props.addErrorMessage("An error occurred.");
              }
            }
          })
          .finally(() => this.setState({ loading: false }));
      });
    }
  };

  onCheckboxChange = (e) => {
    this.setState({
      users: [],
      sendToAllCustomers: e.target.checked,
    });
  };

  getCustomerOptions = (): IPickListOptions[] =>
    this.props.customers
      ? this.props.customers.map((customer) => ({
          value: customer.crm_id,
          label: customer.name,
        }))
      : [];

  closeModal = () =>
    this.setState({ showModal: false, sendToAllCustomers: false, users: [] });

  renderCollectionNoticeModal = () => (
    <ModalBase
      show={true}
      onClose={this.closeModal}
      titleElement={"Send Collections Notices"}
      bodyElement={
        <div className="collection-notice-main">
          <Spinner
            className="send-collections-loader"
            show={this.state.sendingEmail}
          />
          <Checkbox
            isChecked={this.state.sendToAllCustomers}
            name="send-to-all"
            onChange={(e) => this.onCheckboxChange(e)}
            className="send-to-all-checkbox"
          >
            Send to all customers
          </Checkbox>
          {!this.state.sendToAllCustomers && (
            <Input
              field={{
                label: "Customer",
                type: "PICKLIST",
                value: this.state.users,
                options: this.getCustomerOptions(),
                isRequired: true,
              }}
              className="customer-user-select"
              width={12}
              multi={true}
              name="users"
              onChange={this.handleChangeUser}
              placeholder={`Select Customer(s)`}
              loading={this.props.isFetchingCustomers}
            />
          )}
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={"default"}
            onClick={this.closeModal}
            className="cn-btn"
          />
          <SquareButton
            content={`Send Email`}
            bsStyle={"primary"}
            onClick={this.sendEmail}
            className="cn-btn"
            disabled={
              !this.state.sendToAllCustomers && this.state.users.length === 0
            }
          />
        </div>
      }
    />
  );

  render() {
    const providerUsersEmails: IPickListOptions[] = this.props.providerUsers
      ? this.props.providerUsers.map((author: ISuperUser) => ({
          value: author.email,
          label: `${author.first_name} ${author.last_name}`,
        }))
      : [];

    return (
      <>
        {this.state.showModal && this.renderCollectionNoticeModal()}
        <div className="collections-main-container">
          <Spinner className="collections-loader" show={this.state.loading} />
          <HTMLViewer
            show={this.state.openPreview}
            onClose={this.closePreview}
            titleElement={`View Collections Notices Preview`}
            previewHTML={this.state.previewHTML}
          />
          <div className="collections-tr-btns">
            {!this.state.firstSave && (
              <SquareButton
                onClick={this.previewEmail}
                content={"Preview"}
                bsStyle={"primary"}
                className="collections-preview-btn"
                disabled={this.state.loading}
              />
            )}
            <SquareButton
              onClick={() => this.setState({ showModal: true })}
              content="Send Collection Notices"
              bsStyle={"primary"}
              disabled={this.state.loading}
            />
          </div>
          <div className="collections-row">
            <Input
              field={{
                label: "Send from User Email",
                type: "PICKLIST",
                value: this.state.settings.send_from_email,
                options: providerUsersEmails,
                isRequired: true,
              }}
              multi={false}
              width={6}
              placeholder="Select Email"
              error={
                !this.state.settings.send_from_email
                  ? this.state.error.send_from_email
                  : { ...Collections.SuccessMessage }
              }
              name="send_from_email"
              onChange={this.handleChange}
            />
          </div>
          <div className="collections-row">
            <Input
              field={{
                label: "Send to Users",
                type: "PICKLIST",
                value: this.state.settings.send_to_email,
                options: providerUsersEmails,
                isRequired: true,
              }}
              width={6}
              multi={true}
              name="send_to_email"
              onChange={this.handleChange}
              error={
                this.state.settings.send_to_email.length === 0
                  ? this.state.error.send_to_email
                  : { ...Collections.SuccessMessage }
              }
              placeholder={`Select Email`}
            />
          </div>
          <div className="collections-row">
            <Input
              field={{
                label: "Subject",
                type: "TEXT",
                value: this.state.settings.subject,
                isRequired: true,
              }}
              width={8}
              name="subject"
              onChange={this.handleChange}
              error={
                !this.state.settings.subject.trim()
                  ? this.state.error.subject
                  : { ...Collections.SuccessMessage }
              }
              placeholder={`Enter subject for email`}
            />
            <Input
              field={{
                label: "Customer Accounting Type Contact",
                type: "PICKLIST",
                value: this.state.settings.contact_type_crm_id,
                options: this.state.contactTypeOptions,
                isRequired: true,
              }}
              width={4}
              multi={false}
              name="contact_type_crm_id"
              onChange={this.handleChange}
              placeholder={`Select Type`}
              loading={this.state.isFetchingContactTypes}
              error={
                !this.state.settings.contact_type_crm_id
                  ? this.state.error.contact_type_crm_id
                  : { ...Collections.SuccessMessage }
              }
            />
          </div>
          <div className="collections-row">
            <Input
              field={{
                label: "Send Schedule",
                type: "PICKLIST",
                value: this.state.settings.weekly_send_schedule,
                options: [
                  "Sunday",
                  "Monday",
                  "Tuesday",
                  "Wednesday",
                  "Thursday",
                  "Friday",
                  "Saturday",
                ],
                isRequired: true,
              }}
              width={8}
              multi={true}
              name="weekly_send_schedule"
              onChange={this.handleChange}
              error={
                this.state.settings.weekly_send_schedule.length === 0
                  ? this.state.error.weekly_send_schedule
                  : { ...Collections.SuccessMessage }
              }
              placeholder={`Select Day(s)`}
            />
            <Input
              field={{
                label: "Approaching Due Date Aging",
                type: "NUMBER",
                value: this.state.settings.approaching_due_date_ageing,
                isRequired: true,
              }}
              width={4}
              name="approaching_due_date_ageing"
              placeholder="Enter no. of days"
              error={this.state.error.approaching_due_date_ageing}
              onChange={this.handleChange}
            />
          </div>
          <div className="collections-row">
            <div className="field-section col-xs-8">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Invoice Exclude Terms
                </label>
              </div>
              {/* <TagsInput
                value={this.state.settings.invoice_exclude_patterns}
                onChange={this.handleChangeTags}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Invoice Exclude Terms",
                }}
                addOnBlur={true}
              /> */}
            </div>
            <Input
              field={{
                label: "Billing Status",
                type: "PICKLIST",
                value: this.state.settings.billing_statuses,
                options: this.props.billingStatusOptions,
                isRequired: true,
              }}
              width={4}
              multi={true}
              name="billing_statuses"
              onChange={this.handleChange}
              error={
                this.state.settings.billing_statuses.length === 0
                  ? this.state.error.billing_statuses
                  : { ...Collections.SuccessMessage }
              }
              placeholder={`Select Billing Status(es)`}
            />
          </div>
          <div className="collections-row">
            <label className="btn square-btn btn-default upload-btn-container">
              <img
                className="icon-upload"
                src="/assets/icons/photo-camera.svg"
                title="Upload image"
              />
              <input
                type="file"
                name=""
                accept="image/jpg, image/jpeg, image/png"
                className="custom-file-input"
                onChange={(e) => this.onImageUpload(e)}
                onClick={(event: any) => {
                  event.target.value = null;
                }}
              />
            </label>

            <QuillEditorAcela
              onChange={this.handleChangeMarkdown}
              label={"Email Body Template"}
              value={this.state.settings.email_body_template}
              scrollingContainer=".app-body"
              wrapperClass={"collections-email-template"}
              isRequired={true}
              hideTable={true}
              customToolbar={[
                ["bold", "italic", "underline", "strike", "custom-image"], // toggled buttons
                [{ color: [] }, { background: [] }],
                [{ list: "ordered" }, { list: "bullet" }],
                [{ header: [1, 2, 3, 4, 5, 6, false] }],
              ]}
              error={
                commonFunctions.isEditorEmpty(
                  this.state.settings.email_body_template
                )
                  ? this.state.error.email_body_template
                  : { ...Collections.SuccessMessage }
              }
            />
          </div>
          {!isEqual(this.state.prevSettings, this.state.settings) && (
            <SquareButton
              onClick={this.onSubmit}
              content={this.state.firstSave ? "Save" : "Update"}
              className={"collections-save-btn"}
              bsStyle={"primary"}
            />
          )}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
  isFetchingCustomers: state.customer.customersShortFetching,
  providerUsers: state.providerUser.providerUsersAll,
  billingStatusOptions: state.setting.billingStatusOptions,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchCustomers: () => dispatch(fetchCustomersShort()),
  fetchAllProviderUsers: () => dispatch(fetchAllProviderUsers()),
  getBillingStatusesOptions: () => dispatch(getBillingStatusesOptions()),
  collectionsSettingsCRUD: (
    request: "get" | "put" | "post",
    data: ICollectionsSettings
  ) => dispatch(collectionsSettingsCRUD(request, data)),
  sendCollectionsNoticeEmail: (crm_ids: number[], all_customers: boolean) =>
    dispatch(sendCollectionsNoticeEmail(crm_ids, all_customers)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  getCollectionsEmailPreview: () => dispatch(getCollectionsEmailPreview()),
  uploadImage: (file: any, name: string) => dispatch(uploadImage(file, name)),
  addInfoMessage: (str: string) => dispatch(addInfoMessage(str)),
  addErrorMessage: (str: string) => dispatch(addErrorMessage(str)),
  addSuccessMessage: (str: string) => dispatch(addSuccessMessage(str)),
  addWarningMessage: (str: string) => dispatch(addWarningMessage(str)),
  fetchOrderTrackingSettings: () => dispatch(fetchOrderTrackingSettings()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Collections);
