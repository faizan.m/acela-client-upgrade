import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import {
  CONFIG_MAPPING_SUCCESS,
  configMappingCRUD,
} from "../../actions/setting";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import { addSuccessMessage } from "../../actions/appState";
import ModalBase from "../../components/ModalBase/modalBase";
import IconButton from "../../components/Button/iconButton";
import EditButton from "../../components/Button/editButton";
import DeleteButton from "../../components/Button/deleteButton";
import "../../commonStyles/inputTagMapping.scss";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";

interface ConfigMappingModalProps {
  show: boolean;
  configTypes: IDeviceCategory[];
  closeModal: () => void;
  addSuccessMessage: TShowSuccessMessage;
  configMappingCRUD: (
    request: HTTPMethods,
    data?: IConfigMapping
  ) => Promise<any>;
}

const EmptyConfigMapping: IConfigMapping = {
  id: 0,
  name: "",
  config_type_crm_ids: [],
};

interface ErrorInterface {
  name: IFieldValidation;
  config_type_crm_ids: IFieldValidation;
}

const SuccessState: IFieldValidation = {
  errorState: "success",
  errorMessage: "",
};

const ConfigMappingModal: React.FC<ConfigMappingModalProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [openConfirm, setOpenConfirm] = useState<boolean>(false);
  const [configMapping, setConfigMapping] = useState<IConfigMapping[]>([]);
  const [currentMapping, setCurrentMapping] = useState<IConfigMapping>({
    ...EmptyConfigMapping,
  });
  const [error, setError] = useState<ErrorInterface>({
    name: { ...SuccessState },
    config_type_crm_ids: { ...SuccessState },
  });

  useEffect(() => {
    fetchConfigMappings();
  }, []);

  useEffect(() => {
    resetState();
    removeNonSavedElement();
  }, [props.show]);

  const fetchConfigMappings = () => {
    setLoading(true);
    props
      .configMappingCRUD("get")
      .then((action) => {
        if (action.type === CONFIG_MAPPING_SUCCESS) {
          resetState();
          setConfigMapping(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentMapping((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const ConfigTypeOptions: IPickListOptions[] = useMemo(
    () =>
      props.configTypes
        ? props.configTypes.map((el: IDeviceCategory) => ({
            value: el.category_id,
            label: el.category_name,
          }))
        : [],
    [props.configTypes]
  );

  // const ConfigTypeMapping: Map<number, string> = useMemo(() => {
  //   let idNameMapping: Map<number, string> = new Map();
  //   if (props.configTypes) {
  //     props.configTypes.forEach((el) =>
  //       idNameMapping.set(el.category_id, el.category_name)
  //     );
  //   }
  //   return idNameMapping;
  // }, [props.configTypes]);

  const addNewConfigMapping = () => {
    // Make sure that unsaved new products are not available
    if (!configMapping.find((el) => el.id === 0)) {
      setConfigMapping((prevState) => [
        {
          ...EmptyConfigMapping,
        },
        ...prevState,
      ]);
      resetState();
    }
  };

  const resetState = () => {
    setCurrentMapping({ ...EmptyConfigMapping });
    setError({
      name: { ...SuccessState },
      config_type_crm_ids: { ...SuccessState },
    });
  };

  const removeNonSavedElement = () => {
    setConfigMapping((prevState) => {
      return prevState.filter((el) => el.id !== 0);
    });
  };

  const onEditClick = (mapping: IConfigMapping) => {
    resetState();
    removeNonSavedElement();
    setCurrentMapping({ ...mapping });
  };

  const validateData = () => {
    let isValid = true;
    const errors = cloneDeep(error);
    if (!currentMapping.name.trim()) {
      errors.name = {
        errorState: "error",
        errorMessage: "This field is required",
      };
      isValid = false;
    } else errors.name = { ...SuccessState };
    if (!currentMapping.config_type_crm_ids.length) {
      errors.config_type_crm_ids = {
        errorState: "error",
        errorMessage: "This field is required",
      };
      isValid = false;
    } else errors.config_type_crm_ids = { ...SuccessState };
    setError(errors);
    return isValid;
  };

  const onSaveClick = () => {
    if (validateData()) {
      props
        .configMappingCRUD(
          currentMapping.id === 0 ? "post" : "put",
          currentMapping
        )
        .then((action) => {
          if (action.type === CONFIG_MAPPING_SUCCESS) {
            props.addSuccessMessage(
              `Config Mapping ${
                currentMapping.id === 0 ? "Added" : "Updated"
              } Successfully!`
            );
            fetchConfigMappings();
          }
        });
    }
  };

  const toggleConfirmBox = (mapping?: IConfigMapping) => {
    setOpenConfirm((prev) => !prev);
    if (mapping) setCurrentMapping(mapping);
  };

  const deleteMapping = () => {
    setLoading(true);
    props
      .configMappingCRUD("delete", {
        id: currentMapping.id,
      } as IConfigMapping)
      .then((action) => {
        if (action.type === CONFIG_MAPPING_SUCCESS) {
          toggleConfirmBox();
          fetchConfigMappings();
          props.addSuccessMessage("Mapping Deleted Successfull1y");
        }
      })
      .finally(() => setLoading(false));
  };

  const renderMapping = () => {
    return (
      <div className="config-mapping-content">
        <ConfirmBox
          show={openConfirm}
          isLoading={loading}
          onClose={toggleConfirmBox}
          onSubmit={deleteMapping}
          title="You're about to delete this mapping"
          okText="Delete"
          message={
            "The configuration mapping will be permanently removed and you won't be able to retrieve it again"
          }
        />
        <div className="config-mapping-heading">
          <SquareButton
            content={
              <>
                <span className="add-plus">+</span>
                <span className="add-text">Add</span>
              </>
            }
            bsStyle={"link"}
            onClick={addNewConfigMapping}
            className="add-new-config-mapping add-btn"
          />
        </div>
        {loading || configMapping.length > 0 ? (
          <div className="input-tag-mapping-table config-mapping-table">
            <div className="it-mapping-table-header">
              <div className="it-row-col-2">Contract Type</div>
              <div className="it-row-col-1">Friendly Name</div>
            </div>
            <div className="it-mapping-table-rows">
              {configMapping.map((mapping, index) =>
                Boolean(currentMapping.id === mapping.id) ? (
                  <div className={`it-table-row it-row-edit-mode`} key={index}>
                    <div className="it-row-col-2">
                      <Input
                        field={{
                          label: "",
                          type: "PICKLIST",
                          value: currentMapping.config_type_crm_ids,
                          options: ConfigTypeOptions,
                        }}
                        width={12}
                        multi={true}
                        name="config_type_crm_ids"
                        onChange={handleChange}
                        error={error.config_type_crm_ids}
                        className="mapping-config-type"
                        placeholder={`Select Config Type`}
                      />
                    </div>
                    <div className="it-row-col-1">
                      <Input
                        field={{
                          label: "",
                          type: "TEXT",
                          value: currentMapping.name,
                        }}
                        width={12}
                        name="name"
                        onChange={handleChange}
                        error={error.name}
                        className="mapping-friendly-name"
                        placeholder={`Enter Name`}
                      />
                    </div>
                    <div className="it-table-mapping-actions">
                      <IconButton
                        newIcon={true}
                        icon={"save_disk.svg"}
                        title="Save"
                        className="it-save-mapping"
                        onClick={onSaveClick}
                      />
                      <IconButton
                        newIcon={true}
                        icon={"cross_sign.svg"}
                        title="Cancel"
                        className="it-cancel-mapping"
                        onClick={() =>
                          mapping.id !== 0
                            ? resetState()
                            : removeNonSavedElement()
                        }
                      />
                    </div>
                  </div>
                ) : (
                  <div
                    className={`it-table-row it-row-display-mode`}
                    key={index}
                  >
                    <div className="it-row-col-2">
                      {/* <TagsInput
                        value={
                          mapping.config_type_crm_ids.map((id) =>
                            ConfigTypeMapping.get(id)
                          ) || []
                        }
                        onChange={null}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "",
                        }}
                        disabled={true}
                      /> */}
                    </div>
                    <div className="it-row-col-1">{mapping.name}</div>
                    <div className="it-table-mapping-actions">
                      <EditButton
                        title="Edit"
                        onClick={() => onEditClick(mapping)}
                      />
                      {mapping.id !== 0 && (
                        <DeleteButton
                          onClick={() => toggleConfirmBox(mapping)}
                        />
                      )}
                    </div>
                  </div>
                )
              )}
            </div>
          </div>
        ) : (
          <div className="no-mapping-data">No configurations mapped!</div>
        )}
      </div>
    );
  };
  return (
    <ModalBase
      show={props.show}
      onClose={props.closeModal}
      titleElement={"Configuration Mapping"}
      className="config-mapping-modal"
      bodyElement={
        <div className="config-mapping-container">
          <Spinner className="config-mapping-loader" show={loading} />
          {renderMapping()}
        </div>
      }
      footerElement={
        <SquareButton
          content={`Close`}
          bsStyle={"default"}
          onClick={props.closeModal}
          className="cn-btn"
        />
      }
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  configTypes: state.inventory.deviceCategoryList,
});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  configMappingCRUD: (request: HTTPMethods, data?: IConfigMapping) =>
    dispatch(configMappingCRUD(request, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ConfigMappingModal);
