import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  uploadImage,
  UPLOAD_IMAGE_FAILURE,
  UPLOAD_IMAGE_SUCCESS,
} from "../../actions/sow";
import { fetchAllProviderUsers } from "../../actions/provider/user";
import {
  addInfoMessage,
  addErrorMessage,
  addSuccessMessage,
  addWarningMessage,
} from "../../actions/appState";
import {
  fetchStatuses,
  fetchManufacturers,
  fetchDeviceCategories,
} from "../../actions/inventory";
import {
  subscriptionNoticesCRUD,
  fetchOrderTrackingSettings,
  sendSubscriptionsNoticeEmail,
  getSubscriptionsEmailPreview,
  SUBSCRIPTIONS_NOTICE_SUCCESS,
  SUBSCRIPTIONS_NOTICE_FAILURE,
  SEND_SUBSCRIPTIONS_NOTICE_SUCCESS,
  GET_SUBSCRIPTIONS_PREVIEW_SUCCESS,
  FETCH_ORDER_TRACKING_SETTING_SUCCESS,
} from "../../actions/setting";
import {
  fetchTaskStatusConfig,
  CONFIG_TASK_STATUS_SUCCESS,
} from "../../actions/configuration";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import Checkbox from "../../components/Checkbox/checkbox";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import HTMLViewer from "../../components/HTMLViewer/HTMLViewer";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import { commonFunctions } from "../../utils/commonFunctions";
import ConfigMappingModal from "./configMappingModal";

interface SubscriptionNoticeProps {
  statuses: IDeviceStatus[];
  customers: ICustomerShort[];
  isFetchingCustomers: boolean;
  isFetchingOptions: boolean;
  configTypes: IDeviceCategory[];
  manufacturers: IDLabelObject[];
  providerUsers: ISuperUser[];
  fetchStatuses: () => void;
  fetchConfigTypes: () => void;
  fetchManufacturers: () => void;
  fetchAllProviderUsers: () => void;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  addWarningMessage: TShowWarningMessage;
  fetchTaskStatus: (id: number) => Promise<any>;
  fetchOrderTrackingSettings: () => Promise<any>;
  getSubscriptionsEmailPreview: () => Promise<any>;
  uploadImage: (file: any, name: string) => Promise<any>;
  sendSubscriptionsNoticeEmail: (
    crm_ids: number[],
    all_customers: boolean
  ) => Promise<any>;
  subscriptionNoticesCRUD: (
    request: HTTPMethods,
    data?: ISubscriptionNotificationSettings
  ) => Promise<any>;
}

const ExpirationDateOptions: IPickListOptions[] = [
  { value: "EXPIRED", label: "Expired" },
  { value: "NEXT_15_DAYS", label: "0-15 Days" },
  { value: "NEXT_16_TO_89_DAYS", label: "16-89 Days" },
  { value: "NEXT_90_TO_120_DAYS", label: "90-120 Days" },
];

const DayOptions: IPickListOptions[] = [
  { value: "SUNDAY", label: "Sunday" },
  { value: "MONDAY", label: "Monday" },
  { value: "TUESDAY", label: "Tuesday" },
  { value: "WEDNESDAY", label: "Wednesday" },
  { value: "THURSDAY", label: "Thursday" },
  { value: "FRIDAY", label: "Friday" },
  { value: "SATURDAY", label: "Saturday" },
];

const SuccessState: IFieldValidation = {
  errorState: "success",
  errorMessage: "",
};

const SubscriptionNotice: React.FC<SubscriptionNoticeProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<IErrorValidation>({});
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [users, setUsers] = useState<number[]>([]);
  const [sendToAllCustomers, setSendAllCustomers] = useState<boolean>(false);
  const [previewHTML, setPreviewHTML] = useState<string>("");
  const [openPreview, setOpenPreview] = useState<boolean>(false);
  const [sendingEmail, setSendingEmail] = useState<boolean>(false);
  const [showSendModal, setShowSendModal] = useState<boolean>(false);
  const [showConfigModal, setShowConfigModal] = useState<boolean>(false);
  const [contactTypes, setContactTypes] = useState<IPickListOptions[]>([]);
  const [isFetchingContactTypes, setIsFetchingContactType] = useState<boolean>(
    false
  );
  const [settings, setSettings] = useState<ISubscriptionNotificationSettings>({
    receiver_email_ids: [],
    email_subject: "",
    email_body: "",
    renewal_type_contact_crm_id: null,
    weekly_send_schedule: [],
    approaching_expiry_date_range: [],
    sender: "",
    config_type_crm_ids: [],
    config_status_crm_ids: [],
    manufacturer_crm_ids: [],
  });

  useEffect(() => {
    fetchSettings();
    fetchContactTypeOptions();
    props.fetchStatuses();
    props.fetchConfigTypes();
    props.fetchManufacturers();
    props.fetchAllProviderUsers();
  }, []);

  const CustomerOptions: IPickListOptions[] = useMemo(
    () =>
      props.customers
        ? props.customers.map((customer) => ({
            value: customer.crm_id,
            label: customer.name,
          }))
        : [],
    [props.customers]
  );

  const ConfigTypeOptions: IPickListOptions[] = useMemo(
    () =>
      props.configTypes
        ? props.configTypes.map((el: IDeviceCategory) => ({
            value: el.category_id,
            label: el.category_name,
          }))
        : [],
    [props.configTypes]
  );

  const ManufacturerOptions: IPickListOptions[] = useMemo(
    () =>
      props.manufacturers
        ? props.manufacturers.map((el: IDLabelObject) => ({
            value: el.id,
            label: el.label,
          }))
        : [],
    [props.manufacturers]
  );

  const StatusOptions: IPickListOptions[] = useMemo(
    () =>
      props.statuses
        ? props.statuses.map((el: IDeviceStatus) => ({
            value: el.id,
            label: el.description,
          }))
        : [],
    [props.statuses]
  );

  const providerUsersOptions: IPickListOptions[] = useMemo(
    () =>
      props.providerUsers
        ? props.providerUsers.map((author: ISuperUser) => ({
            value: author.id,
            label: `${author.first_name} ${author.last_name}`,
          }))
        : [],
    [props.providerUsers]
  );

  const providerUserEmailOptions: IPickListOptions[] = useMemo(
    () =>
      props.providerUsers
        ? props.providerUsers.map((author: ISuperUser) => ({
            value: author.email,
            label: `${author.first_name} ${author.last_name}`,
          }))
        : [],
    [props.providerUsers]
  );

  const fetchSettings = () => {
    setLoading(true);
    props
      .subscriptionNoticesCRUD("get")
      .then((action) => {
        if (action.type === SUBSCRIPTIONS_NOTICE_SUCCESS) {
          setSettings(action.response);
        } else if (action.type === SUBSCRIPTIONS_NOTICE_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const fetchContactTypeOptions = () => {
    setIsFetchingContactType(true);
    props
      .fetchOrderTrackingSettings()
      .then((action) => {
        if (action.type === FETCH_ORDER_TRACKING_SETTING_SUCCESS) {
          const contactTypeOptions: IPickListOptions[] = action.response.map(
            (data) => ({
              value: data.id,
              label: `${data.description}`,
            })
          );

          setContactTypes(contactTypeOptions);
        }
      })
      .finally(() => setIsFetchingContactType(false));
  };

  const closePreview = () => {
    setOpenPreview(false);
    setPreviewHTML("");
  };

  const toggleConfigModal = () => setShowConfigModal((prev) => !prev);

  const onCheckboxChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUsers([]);
    setSendAllCustomers(e.target.checked);
  };

  const closeSendModal = () => {
    setShowSendModal(false);
    setUsers([]);
    setSendAllCustomers(false);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSettings((prevState) => ({
      ...prevState,
      [e.target.name]: e.target.value,
    }));
  };

  const handleChangeEmailBody = (html: string) => {
    setSettings((prevState) => ({
      ...prevState,
      email_body: html,
    }));
  };

  const handleChangeUser = (e: { target: { value: number[] } }) => {
    setUsers(e.target.value);
  };

  const sendEmail = () => {
    setSendingEmail(true);
    props
      .sendSubscriptionsNoticeEmail(users, sendToAllCustomers)
      .then((action) => {
        if (action.type === SEND_SUBSCRIPTIONS_NOTICE_SUCCESS) {
          const taskId = action.response.task_id;
          fetchTaskStatus(taskId, users, sendToAllCustomers);
          closeSendModal();
          props.addInfoMessage(
            "We are checking the Subscriptions to send Email. It will just take a moment."
          );
        }
      })
      .finally(() => setSendingEmail(false));
  };

  const previewEmail = () => {
    setLoading(true);
    props
      .getSubscriptionsEmailPreview()
      .then((action) => {
        if (action.type === GET_SUBSCRIPTIONS_PREVIEW_SUCCESS) {
          setPreviewHTML(action.response.html);
          setOpenPreview(true);
        } else {
          props.addErrorMessage("Error fetching collections preview");
        }
      })
      .finally(() => setLoading(false));
  };

  const fetchTaskStatus = (
    taskId: number,
    users: number[],
    allCustomers: boolean
  ) => {
    if (taskId) {
      props.fetchTaskStatus(taskId).then((action) => {
        if (action.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (action.response.status === "SUCCESS") {
            const res = action.response.result;
            if (res && res.operation_status !== "SUCCESS") {
              props.addErrorMessage(`An error occurred while sending emails!`);
            }
            const skipped_customers: {
              customer_name: string;
              customer_crm_id: number;
              reason: string;
              email_sent: boolean;
            }[] =
              res && res.payload && res.payload.skipped_customers
                ? res.payload.skipped_customers
                : [];
            if (skipped_customers.length) {
              let names: string = skipped_customers
                .map((el) => el.customer_name)
                .join(", ");
              if (allCustomers) {
                props.addSuccessMessage(
                  "Notice sent to all the customers with expiring subscriptions!"
                );
              } else {
                if (skipped_customers.length === users.length) {
                  props.addWarningMessage(
                    `Notice not sent to the following customers as expiring subscriptions were not found: ${names}`
                  );
                } else {
                  props.addWarningMessage(
                    `Notice not sent to the following customers as expiring subscriptions were not found: ${names}`
                  );
                  props.addSuccessMessage(
                    "Notice sent to all the customers with expiring subscriptions!"
                  );
                }
              }
            } else {
              props.addSuccessMessage(
                `Expiration Notice sent to all the customers with expiring subscriptions!`
              );
            }
          }
          if (action.response.status === "PENDING") {
            setTimeout(
              () => fetchTaskStatus(taskId, users, allCustomers),
              3000
            );
          }
          if (action.response.status === "FAILURE") {
            props.addErrorMessage(`An error occurred while sending emails!`);
          }
        }
      });
    }
  };

  const onImageUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    setLoading(true);
    if (
      attachment &&
      attachment.type &&
      ["image/png", "image/jpg", "image/jpeg"].includes(attachment.type)
    ) {
      const data = new FormData();
      if (attachment) {
        data.append("filename", attachment);
        props
          .uploadImage(data, attachment)
          .then((action) => {
            if (action.type === UPLOAD_IMAGE_SUCCESS) {
              props.addSuccessMessage("Image Uploaded Successfully!");
              let imageString = `<p><img src="${action.response.file_path}" style="max-width: 100%"></p>`;
              setSettings((prevState) => ({
                ...prevState,
                email_body: prevState.email_body + imageString,
              }));
            }
            if (action.type === UPLOAD_IMAGE_FAILURE) {
              props.addErrorMessage("Image Upload Failed!");
            }
          })
          .finally(() => setLoading(false));
      }
    } else {
      props.addErrorMessage("Invalid image format.");
    }
  };

  const validateForm = (): boolean => {
    let isValid = true;
    const errors = cloneDeep(error);
    let arrayFields = [
      "receiver_email_ids",
      "weekly_send_schedule",
      "approaching_expiry_date_range",
    ];
    let simpleFields = [
      "email_subject",
      "renewal_type_contact_crm_id",
      "sender",
    ];

    arrayFields.forEach((key) => {
      if (settings[key].length === 0) {
        errors[key] = {
          errorState: "error",
          errorMessage: "This field is required",
        };
        isValid = false;
      } else {
        errors[key] = { ...SuccessState };
      }
    });
    simpleFields.forEach((key) => {
      if (!settings[key]) {
        errors[key] = {
          errorState: "error",
          errorMessage: "This field is required",
        };
        isValid = false;
      } else errors[key] = { ...SuccessState };
    });
    if (commonFunctions.isEditorEmpty(settings.email_body)) {
      errors.email_body = {
        errorState: "error",
        errorMessage: "This field is required",
      };
      isValid = false;
    } else errors.email_body = { ...SuccessState };
    if (!isValid) setError(errors);
    return isValid;
  };

  const onSubmit = () => {
    if (validateForm()) {
      setLoading(true);
      props
        .subscriptionNoticesCRUD(
          firstSave ? "post" : "put",
          settings
        )
        .then((action) => {
          if (action.type === SUBSCRIPTIONS_NOTICE_SUCCESS) {
            props.addSuccessMessage(
              `Notification Settings ${
                firstSave ? "Saved" : "Updated"
              } Successfully!`
            );
            setFirstSave(false);
          } else if (action.type === SUBSCRIPTIONS_NOTICE_FAILURE) {
            let errors = action.errorList.data;
            let validationErrors = cloneDeep(error);
            if (!Array.isArray(errors)) {
              Object.keys(errors).forEach((key) => {
                (validationErrors[key] as IFieldValidation) = {
                  errorState: "error",
                  errorMessage: errors[key],
                };
              });
              setError(validationErrors);
            } else {
              props.addErrorMessage("An error occurred.");
            }
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const renderExpirationNoticeModal = () => (
    <ModalBase
      show={true}
      onClose={closeSendModal}
      titleElement={"Send Expiration Notices"}
      bodyElement={
        <div className="collection-notice-main">
          <Spinner className="send-collections-loader" show={sendingEmail} />
          <Checkbox
            isChecked={sendToAllCustomers}
            name="send-to-all"
            onChange={onCheckboxChange}
            className="send-to-all-checkbox"
          >
            Send to all customers
          </Checkbox>
          {!sendToAllCustomers && (
            <Input
              field={{
                label: "Customer",
                type: "PICKLIST",
                value: users,
                options: CustomerOptions,
                isRequired: true,
              }}
              className="customer-user-select"
              width={12}
              multi={true}
              name="users"
              onChange={handleChangeUser}
              placeholder={`Select Customer(s)`}
            />
          )}
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={"default"}
            onClick={closeSendModal}
            className="cn-btn"
          />
          <SquareButton
            content={`Send Email`}
            bsStyle={"primary"}
            onClick={sendEmail}
            className="cn-btn"
            disabled={!sendToAllCustomers && users.length === 0}
          />
        </div>
      }
    />
  );

  return (
    <div className="collections-main-container">
      {showSendModal && renderExpirationNoticeModal()}
      <ConfigMappingModal
        show={showConfigModal}
        closeModal={toggleConfigModal}
      />
      <Spinner className="collections-loader" show={loading} />
      <HTMLViewer
        show={openPreview}
        onClose={closePreview}
        titleElement={`View Subscription Notification Preview`}
        previewHTML={previewHTML}
      />
      <div className="collections-tr-btns">
        <SquareButton
          onClick={toggleConfigModal}
          content={"Config Mapping"}
          bsStyle={"primary"}
          className="collections-preview-btn"
          disabled={props.isFetchingOptions || loading}
        />
        {!firstSave && (
          <SquareButton
            onClick={previewEmail}
            content={"Preview"}
            bsStyle={"primary"}
            className="collections-preview-btn"
            disabled={loading}
          />
        )}
        <SquareButton
          onClick={() => setShowSendModal(true)}
          content="Send Expiration Notices"
          bsStyle={"primary"}
          disabled={loading}
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Send from User Email",
            type: "PICKLIST",
            value: settings.sender,
            options: providerUsersOptions,
            isRequired: true,
          }}
          multi={false}
          width={6}
          placeholder="Select Email"
          error={!settings.sender ? error.sender : SuccessState}
          name="sender"
          onChange={handleChange}
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Send to Users",
            type: "PICKLIST",
            value: settings.receiver_email_ids,
            options: providerUserEmailOptions,
            isRequired: true,
          }}
          width={8}
          multi={true}
          name="receiver_email_ids"
          onChange={handleChange}
          error={
            settings.receiver_email_ids.length === 0
              ? error.receiver_email_ids
              : SuccessState
          }
          placeholder={`Select Email`}
        />
      </div>

      <div className="collections-row">
        <Input
          field={{
            label: "Customer Renewals Contact Type",
            type: "PICKLIST",
            value: settings.renewal_type_contact_crm_id,
            options: contactTypes,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="renewal_type_contact_crm_id"
          onChange={handleChange}
          placeholder={`Select Contact Type`}
          loading={isFetchingContactTypes}
          error={
            !settings.renewal_type_contact_crm_id
              ? error.renewal_type_contact_crm_id
              : SuccessState
          }
        />
        <Input
          field={{
            label: "Status",
            type: "PICKLIST",
            value: settings.config_status_crm_ids,
            options: StatusOptions,
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="config_status_crm_ids"
          onChange={handleChange}
          loading={props.isFetchingOptions}
          placeholder={`Select Status`}
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Manufacturers",
            type: "PICKLIST",
            value: settings.manufacturer_crm_ids,
            options: ManufacturerOptions,
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="manufacturer_crm_ids"
          onChange={handleChange}
          placeholder={`Select Manufacturer`}
        />
        <Input
          field={{
            label: "Config Type",
            type: "PICKLIST",
            value: settings.config_type_crm_ids,
            options: ConfigTypeOptions,
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="config_type_crm_ids"
          onChange={handleChange}
          loading={props.isFetchingOptions}
          placeholder={`Select Config Type`}
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Send Schedule",
            type: "PICKLIST",
            value: settings.weekly_send_schedule,
            options: DayOptions,
            isRequired: true,
          }}
          width={6}
          multi={true}
          name="weekly_send_schedule"
          onChange={handleChange}
          error={
            settings.weekly_send_schedule.length === 0
              ? error.weekly_send_schedule
              : SuccessState
          }
          placeholder={`Select Day(s)`}
        />
        <Input
          field={{
            label: "Approaching Expiration Dates",
            type: "PICKLIST",
            value: settings.approaching_expiry_date_range,
            options: ExpirationDateOptions,
            isRequired: true,
          }}
          width={6}
          multi={true}
          name="approaching_expiry_date_range"
          onChange={handleChange}
          error={
            settings.approaching_expiry_date_range.length === 0
              ? error.approaching_expiry_date_range
              : SuccessState
          }
          placeholder={`Select Expiration Date Option`}
        />
      </div>
      <div className="collections-row">
        <Input
          field={{
            label: "Subject",
            type: "TEXT",
            value: settings.email_subject,
            isRequired: true,
          }}
          width={8}
          name="email_subject"
          onChange={handleChange}
          error={
            !settings.email_subject.trim() ? error.email_subject : SuccessState
          }
          placeholder={`Enter subject for email`}
        />
      </div>
      <div className="collections-row">
        <label className="btn square-btn btn-default upload-btn-container">
          <img
            className="icon-upload"
            src="/assets/icons/photo-camera.svg"
            title="Upload image"
          />
          <input
            type="file"
            name=""
            accept="image/jpg, image/jpeg, image/png"
            className="custom-file-input"
            onChange={(e) => onImageUpload(e)}
            onClick={(event: any) => {
              event.target.value = null;
            }}
          />
        </label>

        <QuillEditorAcela
          onChange={handleChangeEmailBody}
          label={"Email Body Template"}
          value={settings.email_body}
          scrollingContainer=".app-body"
          wrapperClass={"collections-email-template"}
          isRequired={true}
          hideTable={true}
          customToolbar={[
            ["bold", "italic", "underline", "strike", "custom-image"], // toggled buttons
            [{ color: [] }, { background: [] }],
            [{ list: "ordered" }, { list: "bullet" }],
            [{ header: [1, 2, 3, 4, 5, 6, false] }],
          ]}
          error={
            commonFunctions.isEditorEmpty(settings.email_body)
              ? error.email_body
              : SuccessState
          }
        />
      </div>
      <SquareButton
        onClick={onSubmit}
        content={firstSave ? "Save" : "Update"}
        className={"collections-save-btn"}
        bsStyle={"primary"}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  statuses: state.inventory.statusList,
  customers: state.customer.customersShort,
  manufacturers: state.inventory.manufacturers,
  configTypes: state.inventory.deviceCategoryList,
  isFetchingOptions: state.inventory.isFetching,
  providerUsers: state.providerUser.providerUsersAll,
  isFetchingCustomers: state.customer.customersShortFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchAllProviderUsers: () => dispatch(fetchAllProviderUsers()),
  subscriptionNoticesCRUD: (
    request: HTTPMethods,
    data?: ISubscriptionNotificationSettings
  ) => dispatch(subscriptionNoticesCRUD(request, data)),
  sendSubscriptionsNoticeEmail: (crm_ids: number[], all_customers: boolean) =>
    dispatch(sendSubscriptionsNoticeEmail(crm_ids, all_customers)),
  getSubscriptionsEmailPreview: () => dispatch(getSubscriptionsEmailPreview()),
  fetchStatuses: () => dispatch(fetchStatuses()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchConfigTypes: () => dispatch(fetchDeviceCategories()),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  fetchOrderTrackingSettings: () => dispatch(fetchOrderTrackingSettings()),
  uploadImage: (file: any, name: string) => dispatch(uploadImage(file, name)),
  addInfoMessage: (str: string) => dispatch(addInfoMessage(str)),
  addErrorMessage: (str: string) => dispatch(addErrorMessage(str)),
  addSuccessMessage: (str: string) => dispatch(addSuccessMessage(str)),
  addWarningMessage: (str: string) => dispatch(addWarningMessage(str)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionNotice);
