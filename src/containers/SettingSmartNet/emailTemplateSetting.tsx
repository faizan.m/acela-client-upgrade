import _cloneDeep from 'lodash/cloneDeep';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { addSuccessMessage, addErrorMessage } from '../../actions/appState';
import {
  getPurchaseOrderStatusList,
  GET_PO_STATUS_SUCCESS,
  editPurchaseOrderSettings,
  getPurchaseOrderSettings,
  GET_P_O_SETTING_SUCCESS,
  getShipmentMethods,
  fetchOrderTrackingSettings,
  FETCH_ORDER_TRACKING_SETTING_SUCCESS,
  getQuoteAllStages,
  GET_QUOTE_STAGES_SUCCESS
} from '../../actions/setting';
import { 
  getServiceBoards,
  getQuoteStatusesList,
  GET_STATUSES_SUCCESS,
  FETCH_SERVICE_BOARDS_SUCCESS,
} from '../../actions/sow';
import {
  fetchProviderBoardStatus,
  FETCH_PROVIDER_BOARD_STATUS_SUCCESS,
} from "../../actions/provider/integration";
import Input from '../../components/Input/input';
import SquareButton from '../../components/Button/button';
import Spinner from '../../components/Spinner';
import { isEmptyObj } from '../../utils/CommonUtils';
import Collapsible from '../../components/Collapsible';
// import TagsInput from 'react-tagsinput'; (New Component)
import CarrierTrackingUrl from './carrierTrackingUrl';
import showdown from 'showdown';
import SimpleMDE from 'react-simplemde-editor';
import SmallConfirmationBox from '../../components/SmallConfirmationBox/confirmation';

interface IISettingSmartNetState {
  purchase_order_status_ids: number[];
  extra_config: IOrderTrackingExtraConfig;
  contact_type_connectwise_id: any;
  service_ticket_board_mapping: IServiceTicketBoardMapping[];
  opportunity_statuses: IOpportunityStatusSetting[];
  opportunity_stages: IOpportunityStageSetting[];
  board_ticket_list: any;
  isFetchingContactTypes: boolean;
  isFetchingBoards: boolean;
  serviceBoardOptions: any[];
  opportunityStatusOptions: any[];
  opportunityStageOptions: any[];
  contactTypeOptions: any[];
  poStatusList: any[];
  loading: boolean;
  loadingServiceTickets: boolean;
  errors: {
    email_intro_markdown: IFieldValidation;
    smartnet_intro_markdown: IFieldValidation;
    ending_paragraph_markdown: IFieldValidation;
    licences: IFieldValidation;
    contracts: IFieldValidation;
    purchase_order_status_ids: IFieldValidation;
    contact_type_connectwise_id: IFieldValidation;
    service_ticket_board_mapping: IFieldValidation;
    received_status_mapping: IFieldValidation;
  }
  isOpen: boolean;
  loadingShipments: any;
  contractsCarrierMapping: any;
  licencesCarrierMapping: any;
}

interface IISettingSmartNetProps extends ICommonProps {
  isFetchingType: boolean;
  isFetching: boolean;
  purchaseOrderSetting: IPurchaseOrderSetting;
  addSuccessMessage: any;
  addErrorMessage: any;
  getPurchaseOrderStatusList: any;
  editPurchaseOrderSettings: any;
  getPurchaseOrderSettings: any;
  shipments: IPickListOptions[];
  isFetchingShipments: boolean;
  getShipmentMethods: any;
  fetchOrderTrackingSettings: any;
  getServiceBoards: any;
  getServiceBoardTickets: any;
  getOpportunityStatuses: any;
  getOpportunityStages: any;
  receivedStatusList: IPickListOptions[];
}

class OrderTrackingEmailSetting extends Component<
  IISettingSmartNetProps,
  IISettingSmartNetState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  static emptyServiceTicketBoard: IServiceTicketBoardMapping = {
    board_id: -1,
    board_name: "",
    ticket_statuses: [],
  };

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    contactTypeOptions: [],
    serviceBoardOptions: [],
    opportunityStatusOptions: [],
    opportunityStageOptions: [],
    isFetchingContactTypes: false,
    isFetchingBoards: false,
    contact_type_connectwise_id: undefined,
    service_ticket_board_mapping: [_cloneDeep(OrderTrackingEmailSetting.emptyServiceTicketBoard)],
    opportunity_statuses: [],
    opportunity_stages: [],
    loadingServiceTickets: false,
    board_ticket_list: {},
    purchase_order_status_ids: [],
    extra_config: {
      email_settings: {
        email_intro: '',
        email_intro_markdown: '',
        smartnet_intro: '',
        smartnet_intro_markdown: '',
        ending_paragraph: '',
        ending_paragraph_markdown: '',
      },
      product_category_mapping: {
        licencesArr: [],
        contractsArr: [],
      },
      part_number_exclusion: [],
      carrier_tracking_url: {},
      received_status_mapping: []
    },
    poStatusList: [],
    loading: false,
    isOpen: false,
    loadingShipments: false,
    errors: {
      email_intro_markdown: { ...OrderTrackingEmailSetting.emptyErrorState },
      smartnet_intro_markdown: { ...OrderTrackingEmailSetting.emptyErrorState },
      ending_paragraph_markdown: { ...OrderTrackingEmailSetting.emptyErrorState },
      licences: { ...OrderTrackingEmailSetting.emptyErrorState },
      contracts: { ...OrderTrackingEmailSetting.emptyErrorState },
      purchase_order_status_ids: { ...OrderTrackingEmailSetting.emptyErrorState },
      contact_type_connectwise_id: { ...OrderTrackingEmailSetting.emptyErrorState },
      service_ticket_board_mapping: { ...OrderTrackingEmailSetting.emptyErrorState },
      received_status_mapping: { ...OrderTrackingEmailSetting.emptyErrorState },
    },
    licencesCarrierMapping: undefined,
    contractsCarrierMapping: undefined
  });

  componentDidMount() {
    this.props.getPurchaseOrderSettings()
    this.getPOStatusList();
    this.props.getShipmentMethods();
    this.setState({ isFetchingContactTypes: true, isFetchingBoards: true });
    this.props.fetchOrderTrackingSettings().then((action) => {
      if (action.type === FETCH_ORDER_TRACKING_SETTING_SUCCESS) {
        const contactTypeOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.description}`,
        }))
        this.setState({ isFetchingContactTypes: false, contactTypeOptions});
      }
      this.setState({ isFetchingContactTypes: false});
    });
    this.props.getServiceBoards().then((action) => {
      if (action.type === FETCH_SERVICE_BOARDS_SUCCESS) {
        const serviceBoardOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.label}`,
        }))
        this.setState({ serviceBoardOptions, isFetchingBoards: false });
      } else {
        this.setState({ isFetchingBoards: false})
      }
    })
    this.props.getOpportunityStatuses().then((action) => {
      if (action.type === GET_STATUSES_SUCCESS) {
        const opportunityStatusOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.label}`,
        }))
        this.setState({ opportunityStatusOptions });
      }
    });
    this.props.getOpportunityStages().then((action) => {
      if (action.type === GET_QUOTE_STAGES_SUCCESS) {
        const opportunityStageOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.label}`,
        }))
        this.setState({ opportunityStageOptions });
      }
    });
  }

  getIntance = instance => {
    instance.togglePreview();
  };
  getPOStatusList = () => {
    this.setState({ loading: true });
    this.props.getPurchaseOrderStatusList().then(action => {
      if (action.type === GET_PO_STATUS_SUCCESS) {
        const statusList = action.response;
        const poStatusList = statusList
          ? statusList.map(c => ({
            value: c.id,
            label: c.name,
          }))
          : [];
        this.setState({ poStatusList, loading: false })
      }
    });
  }

  componentDidUpdate(prevProps: IISettingSmartNetProps) {
    if (
      this.props.purchaseOrderSetting &&
      this.props.purchaseOrderSetting !== prevProps.purchaseOrderSetting
    ) {
      const purchaseOrderSetting: IPurchaseOrderSetting = this.props.purchaseOrderSetting;
      let extra_config: IOrderTrackingExtraConfig = null;
      let contact_type_connectwise_id;
      let service_ticket_board_mapping: IServiceTicketBoardMapping[];
      let licencesCarrierMapping;
      let contractsCarrierMapping;
      let opportunity_stages: IOpportunityStageSetting[];
      let opportunity_statuses: IOpportunityStatusSetting[];
  
      if (purchaseOrderSetting.extra_config && isEmptyObj(purchaseOrderSetting.extra_config)) {
        extra_config = {
          ...this.state.extra_config,
          ...purchaseOrderSetting.default_extra_config,
        };
        contact_type_connectwise_id = purchaseOrderSetting.contact_type_connectwise_id;
        service_ticket_board_mapping = purchaseOrderSetting.ticket_board_and_status_mapping;
        opportunity_stages = purchaseOrderSetting.opportunity_stages;
        opportunity_statuses = purchaseOrderSetting.opportunity_statuses;
      } else {
        extra_config = {
          ...this.state.extra_config,
          ...purchaseOrderSetting.extra_config,
        };
        contact_type_connectwise_id = purchaseOrderSetting.contact_type_connectwise_id;
        service_ticket_board_mapping = purchaseOrderSetting.ticket_board_and_status_mapping;
        opportunity_stages = purchaseOrderSetting.opportunity_stages;
        opportunity_statuses = purchaseOrderSetting.opportunity_statuses;
      }
  
      this.setState(
        {
          purchase_order_status_ids: purchaseOrderSetting.purchase_order_status_ids,
          extra_config,
          licencesCarrierMapping,
          contractsCarrierMapping,
          contact_type_connectwise_id,
          service_ticket_board_mapping,
          opportunity_stages,
          opportunity_statuses
        },
        () =>
          service_ticket_board_mapping.forEach((mapping) => this.fetchBoardStatuses(mapping.board_id))
      );
    }
  }  

  savePurchaseOrderSettings = () => {
    if (this.validateFormOperations()) {
      const convert = new showdown.Converter();
      const extra_config = this.state.extra_config;

      extra_config.email_settings = {
        "email_intro": convert.makeHtml(this.state.extra_config.email_settings.email_intro_markdown),
        "ending_paragraph": convert.makeHtml(this.state.extra_config.email_settings.ending_paragraph_markdown),
        "smartnet_intro": convert.makeHtml(this.state.extra_config.email_settings.smartnet_intro_markdown),
        "email_intro_markdown": this.state.extra_config.email_settings.email_intro_markdown,
        "smartnet_intro_markdown": this.state.extra_config.email_settings.smartnet_intro_markdown,
        "ending_paragraph_markdown": this.state.extra_config.email_settings.ending_paragraph_markdown,
      }

      const data = {
        extra_config,
        opportunity_statuses: this.state.opportunity_statuses,
        opportunity_stages: this.state.opportunity_stages,
        purchase_order_status_ids: this.state.purchase_order_status_ids,
        contact_type_connectwise_id: this.state.contact_type_connectwise_id,
        ticket_board_and_status_mapping: this.state.service_ticket_board_mapping
      }

      this.props.editPurchaseOrderSettings(data)
        .then(action => {
          if (action.type === GET_P_O_SETTING_SUCCESS) {
            this.props.addSuccessMessage('Operations Setting Updated.')
          }
        });
    }
  };


  validateFormOperations = () => {
    const errors = this.getEmptyState().errors;
    let isValid = true;


    if (!this.state.purchase_order_status_ids || this.state.purchase_order_status_ids.length === 0) {
      errors.purchase_order_status_ids.errorState = "error";
      errors.purchase_order_status_ids.errorMessage = 'Please select purchase order  statuses';

      isValid = false;
    }

    const unique_boards = new Set(this.state.service_ticket_board_mapping.map(el => el.board_id));
    if(this.state.service_ticket_board_mapping.find(el => el.board_id === -1)) {
      errors.service_ticket_board_mapping.errorState = "error";
      errors.service_ticket_board_mapping.errorMessage = 'All fields are required';
      isValid = false;
    } else if (unique_boards.size !== this.state.service_ticket_board_mapping.length){
      errors.service_ticket_board_mapping.errorState = "error";
      errors.service_ticket_board_mapping.errorMessage = 'No two boards should be same';      
      isValid = false;
    } else {
      errors.service_ticket_board_mapping = {...OrderTrackingEmailSetting.emptyErrorState}
    }

    const unique_statuses = new Set(
      this.state.extra_config.received_status_mapping.map(
        (el) => el.received_status_id
      )
    );
    if (
      this.state.extra_config.received_status_mapping.find(
        (el) =>
          el.received_status_id === -1 || el.mapped_status.trim().length === 0
      )
    ) {
      errors.received_status_mapping.errorState = "error";
      errors.received_status_mapping.errorMessage = "All fields are required";
      isValid = false;
    } else if (
      unique_statuses.size !==
      this.state.extra_config.received_status_mapping.length
    ) {
      errors.received_status_mapping.errorState = "error";
      errors.received_status_mapping.errorMessage =
        "No two received statuses should be same";
      isValid = false;
    } else {
      errors.received_status_mapping = {
        ...OrderTrackingEmailSetting.emptyErrorState,
      };
    }

    this.setState({ errors, isOpen: !isValid });

    return isValid;
  };


  renderPOEmailStatus = () => {

    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label: 'Email Template Purchase order status ',
              type: "PICKLIST",
              value: this.state.purchase_order_status_ids,
              options: this.state.poStatusList,
              isRequired: true,
            }}
            width={6}
            multi={true}
            name="purchase_order_status_ids"
            onChange={e => this.handleChangeOperations(e)}
            error={this.state.errors.purchase_order_status_ids}
            placeholder={`Select`}
            loading={this.state.loading}
          />
        </div>
      </div>
    );
  };

  handleChangeSowMarkDown = (event: any, name: string) => {
    const newState = _cloneDeep(this.state);
    newState.extra_config.email_settings[name] = event;
    this.setState(newState);
  };

  handleChangeCarrierMapping = (event: any) => {
    const newState = _cloneDeep(this.state);
    newState.extra_config.product_category_mapping[event.target.name].carrier = event.target.value
    this.setState(newState);
  };

  renderOTEmailSetting = () => {

    return (
      <div className="snt-email col-md-12">
        <div className="field-section  col-md-6 col-xs-12">
          <div className="field__label row">
            <label className="field__label-label" title="">
              Email Intro
                    </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${this.state.errors.email_intro_markdown.errorMessage
              ? `error-input`
              : ''
              }`}
          >
            <SimpleMDE
              onChange={e =>
                this.handleChangeSowMarkDown(e, 'email_intro_markdown')
              }
              options={{
                placeholder: '',
                autofocus: true,
                spellChecker: true,
                hideIcons: [
                  'guide',
                  'heading',
                  'fullscreen',
                  'side-by-side',
                  'image',
                ],
              }}
              value={this.state.extra_config.email_settings.email_intro_markdown}
              className={`${'markdown-editor-input'}`}
              getMdeInstance={this.getIntance}
            />
          </div>
          {this.state.errors.email_intro_markdown.errorMessage && (
            <div className="field__error">
              {this.state.errors.email_intro_markdown.errorMessage}
            </div>
          )}
        </div>
        <div className="field-section  col-md-6 col-xs-12">
          <div className="field__label row">
            <label className="field__label-label" title="">
              Smartnet Intro
                    </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${this.state.errors.smartnet_intro_markdown.errorMessage
              ? `error-input`
              : ''
              }`}
          >
            <SimpleMDE
              onChange={e =>
                this.handleChangeSowMarkDown(e, 'smartnet_intro_markdown')
              }
              options={{
                placeholder: '',
                autofocus: true,
                spellChecker: true,
                hideIcons: [
                  'guide',
                  'heading',
                  'fullscreen',
                  'side-by-side',
                  'image',
                ],
              }}
              value={this.state.extra_config.email_settings.smartnet_intro_markdown}
              className={`${'markdown-editor-input'}`}
              getMdeInstance={this.getIntance}
            />
          </div>
          {this.state.errors.smartnet_intro_markdown.errorMessage && (
            <div className="field__error">
              {this.state.errors.smartnet_intro_markdown.errorMessage}
            </div>
          )}
        </div>
        <div className="field-section  col-md-12 col-xs-12">
          <div className="field__label row">
            <label className="field__label-label" title="">
              Ending Paragraph
                    </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${this.state.errors.ending_paragraph_markdown.errorMessage
              ? `error-input`
              : ''
              }`}
          >
            <SimpleMDE
              onChange={e =>
                this.handleChangeSowMarkDown(e, 'ending_paragraph_markdown')
              }
              options={{
                placeholder: '',
                autofocus: true,
                spellChecker: true,
                hideIcons: [
                  'guide',
                  'heading',
                  'fullscreen',
                  'side-by-side',
                  'image',
                ],
              }}
              value={this.state.extra_config.email_settings.ending_paragraph_markdown}
              className={`${'markdown-editor-input'}`}
              getMdeInstance={this.getIntance}
            />
          </div>
          {this.state.errors.ending_paragraph_markdown.errorMessage && (
            <div className="field__error">
              {this.state.errors.ending_paragraph_markdown.errorMessage}
            </div>
          )}
        </div>
      </div>
    );
  };


  renderContractsCarrierTracking = () => {
    
    return (
      <div className="tac-email col-md-12">
        <div
          className="email-tag field-section
             col-md-6 col-xs-12"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Contracts
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${this.state.errors.contracts
              .errorMessage
              ? `error-input`
              : ''
              }`}
          >
            {/* <TagsInput
              value={
                (this.state.extra_config.product_category_mapping.contracts &&
                  this.state.extra_config.product_category_mapping.contracts.sub_strings) || []}
              onChange={e =>
                this.handleChangeCategoryMapping(e, 'contracts')
              }
              inputProps={{
                className: 'react-tagsinput-input',
                placeholder: 'Enter',
              }}
              addOnBlur={true}
            /> */}
          </div>
          {this.state.errors.contracts
            .errorMessage && (
              <div className="multiple-email-eror">
                {
                  this.state.errors.contracts
                    .errorMessage
                }
              </div>
            )}
        </div>
        <div className="email-tag field-section col-md-6 col-xs-12">
            <Input
              field={{
                label: 'Carrier Mapping',
                type: "TEXT",
                value: this.state.extra_config.product_category_mapping.contracts && 
                  this.state.extra_config.product_category_mapping.contracts.carrier,
                isRequired: true,
              }}
              width={12}
              placeholder="Enter Name"
              className="or-carrier-map"
              // error={this.state.name}
              name="contracts"
              onChange={ e => this.handleChangeCarrierMapping(e)}
            />
        </div>
      </div>
    );
  };

  renderLicensesCarrierTracking = () => {
    return (
      <>
      <div className="tac-email col-md-12">
        <div
          className="email-tag field-section
             col-md-6 col-xs-12"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Licences
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${this.state.errors.licences.errorMessage ? `error-input` : '' }`}
          >
            {/* <TagsInput
              value={
                (this.state.extra_config.product_category_mapping.licences &&
                  this.state.extra_config.product_category_mapping.licences.sub_strings) || []}
              onChange={e =>
                this.handleChangeCategoryMapping(e, 'licences')
              }
              inputProps={{
                className: 'react-tagsinput-input',
                placeholder: 'Enter',
              }}
              addOnBlur={true}
            /> */}
          </div>
          {this.state.errors.contracts
            .errorMessage && (
              <div className="multiple-email-eror">
                {this.state.errors.contracts.errorMessage}
              </div>
            )}
        </div>
        <div className="email-tag field-section col-md-6 col-xs-12">
            <Input
              field={{
                label: 'Carrier Mapping',
                type: "TEXT",
                value: this.state.extra_config.product_category_mapping.licences && 
                  this.state.extra_config.product_category_mapping.licences.carrier,
                isRequired: true,
              }}
              width={12}
              placeholder="Enter Name"
              className="or-carrier-map"
              // error={this.state.name}
              name="licences"
              onChange={ e => this.handleChangeCarrierMapping(e)}
            />
        </div>
      </div>
    </>
    );
  };


  renderContactTypeMapping = () => {
    return (
      <Input
        field={{
            label: 'Contact Type',
            type: "PICKLIST",
            value: this.state.contact_type_connectwise_id,
            options: this.state.contactTypeOptions,
            isRequired: false,
        }}
        className="or-contact-type"
        width={8}
        multi={false}
        name="contact_type_connectwise_id"
        onChange={this.handleChangeOperations}
        placeholder={`Select Type`}
        loading={this.state.isFetchingContactTypes}
        error={this.state.errors.contact_type_connectwise_id}
      />
    )
  };

  renderOpportunityStatuses = () => {
    return (
      <Input
        field={{
            label: 'Opportunity Statuses',
            type: "PICKLIST",
            value: this.state.opportunity_statuses.map((el) => el.status_id),
            options: this.state.opportunityStatusOptions,
            isRequired: false,
        }}
        className="or-contact-type"
        width={8}
        multi={true}
        name="opportunity_status"
        onChange={this.handleChangeOpportunityStatus}
        placeholder={`Select Opportunity Status`}
      />
    )
  };

  renderOpportunityStages = () => {
    return (
      <Input
        field={{
            label: 'Opportunity Stages',
            type: "PICKLIST",
            value: this.state.opportunity_stages.map(el => el.stage_id),
            options: this.state.opportunityStageOptions,
            isRequired: false,
        }}
        className="or-contact-type"
        width={8}
        multi={true}
        name="opportunity_stage"
        onChange={this.handleChangeOpportunityStage}
        placeholder={`Select Opportunity Stages`}
      />
    )
  };

  renderServiceTicketBoardMapping = () => {
    return (
      <div className="service-ticket-board-mapping">
        {this.state.service_ticket_board_mapping.map(
          (service_ticket_board, idx) => (
            <div className="board-ticket-mapper" key={idx}>
              <Input
                field={{
                  label: "Board",
                  type: "PICKLIST",
                  value: service_ticket_board.board_id,
                  options: this.state.serviceBoardOptions,
                  isRequired: true,
                }}
                className="or-contact-type"
                width={6}
                multi={false}
                name="board"
                onChange={(e) => this.handleChangeServiceTicketBoard(e, idx)}
                placeholder={`Select Board`}
                loading={this.state.isFetchingBoards}
              />
              {service_ticket_board.board_id in
                this.state.board_ticket_list && (
                <Input
                  field={{
                    label: "Service Ticket Status",
                    type: "PICKLIST",
                    value: service_ticket_board.ticket_statuses.map(
                      (ticket) => ticket.ticket_status_id
                    ),
                    options: this.getTicketOptions(
                      service_ticket_board.board_id
                    ),
                    isRequired: true,
                  }}
                  className="or-contact-type"
                  width={6}
                  multi={true}
                  name="service_ticket"
                  onChange={(e) =>
                    this.handleChangeTicketStatuses(
                      e,
                      idx,
                      service_ticket_board.board_id
                    )
                  }
                  placeholder={`Select Service Tickets for Board`}
                />
              )}
              {
                idx !== 0 && 
                <SmallConfirmationBox
                    className='remove-mapping-row'
                    onClickOk={() => this.onDeleteServiceRowClick(idx)}
                    text={'Mapping'}
                />
              }
            </div>
          )
        )}
        <div className="add-new-board-mapping" onClick={this.addNewServiceTicketMapping}>
          Add New Mapping
        </div>
        {
          this.state.errors.service_ticket_board_mapping.errorState === "error" &&
          <div id="mapping-error">
            {this.state.errors.service_ticket_board_mapping.errorMessage}
          </div>
        }
      </div>
    );
  };

  renderPartNumberExclusion = () => {
    return (
      <div className="part-number-exclusion-container">
        {/* <TagsInput
          value={
            this.state.extra_config.part_number_exclusion
          }
          onChange={e =>
            this.handleChangePartExclusions(e)
          }
          inputProps={{
            className: 'react-tagsinput-input',
            placeholder: 'Enter Part Number',
          }}
          addOnBlur={true}
        /> */}
      </div>
    );
  };

  renderReceivedStatusMapping = () => {
    return (
      <div className="service-ticket-board-mapping">
        {this.state.extra_config.received_status_mapping.map(
          (status, idx) => (
            <div className="board-ticket-mapper" key={idx}>
              <Input
                field={{
                  label: "Received Status Name",
                  type: "PICKLIST",
                  value: status.received_status_id,
                  options: this.props.receivedStatusList,
                  isRequired: true,
                }}
                className="or-contact-type"
                width={6}
                multi={false}
                name="received_status_id"
                onChange={(e) => this.handleChangeReceivedStatusMapping(e, idx)}
                placeholder={`Select Received Status`}
                loading={this.state.loading}
              />
                <Input
                  field={{
                    label: "Mapped Status Name",
                    type: "TEXT",
                    value: status.mapped_status,
                    isRequired: true,
                  }}
                  className="or-contact-type"
                  width={6}
                  name="mapped_status"
                  onChange={(e) => this.handleChangeReceivedStatusMapping(e, idx)}
                  placeholder={`Enter display name for received status`}
                />
              {
                this.state.extra_config.received_status_mapping.length > 1 && 
                <SmallConfirmationBox
                    className='remove-mapping-row'
                    onClickOk={() => this.onDeleteReceivedRowClick(idx)}
                    text={'Mapping'}
                />
              }
            </div>
          )
        )}
        <div className="add-new-board-mapping" onClick={this.addNewReceivedStatusMapping}>
          Add New Mapping
        </div>
        {
          this.state.errors.received_status_mapping.errorState === "error" &&
          <div id="mapping-error">
            {this.state.errors.received_status_mapping.errorMessage}
          </div>
        }
      </div>
    );
  }

  handleChangeCategoryMapping = (event: any, name) => {
    let distributionList = event;
    distributionList = distributionList.filter(
      (value, index, self) => self.indexOf(value) === index
    );
    const newState = _cloneDeep(this.state);

    newState.extra_config.product_category_mapping[name].sub_strings = distributionList;
    this.setState(newState);
  };

  handleChangePartExclusions = (value: string[]) => {
    this.setState({
      extra_config: {
        ...this.state.extra_config,
        part_number_exclusion: value.map(s => s.toLowerCase()),
      },
    });
  };
  
  handleChangeOperations = (event: any) => {
    const newState = _cloneDeep(this.state);
    newState[event.target.name] = event.target.value;
    this.setState(newState);
  };

  // Service Ticket Board Mapping Function
  handleChangeTicketStatuses = (e: any, idx: number, board_id: number) => {
    const tickets_array = e.target.value;
    const newState = _cloneDeep(this.state);
    newState.service_ticket_board_mapping[
      idx
    ].ticket_statuses = tickets_array.map((ticket_id) => ({
      ticket_status_id: ticket_id,
      ticket_status_name: this.state.board_ticket_list[board_id][ticket_id]
        .label,
    }));
    this.setState(newState);
  };

  getTicketOptions = (board_id: number): IPickListOptions[] => {
    return Object.values(this.state.board_ticket_list[board_id]).map(
      (el: any) => ({
        value: el.id,
        label: el.label,
      })
    );
  };

  fetchBoardStatuses = (board_id: number) => {
    this.setState({ loadingServiceTickets: true });
    this.props.getServiceBoardTickets(board_id).then((action) => {
      if (action.type === FETCH_PROVIDER_BOARD_STATUS_SUCCESS) {
        let ticket_list = {};
        action.response.forEach((ticket) => {
          ticket_list[ticket.id] = ticket;
        });
        this.setState(
          {
            loadingServiceTickets: false,
            board_ticket_list: {
              ...this.state.board_ticket_list,
              [board_id]: ticket_list
            }
          }
        );
      } else {
        this.setState({ loadingServiceTickets: false });
      }
    });
  };

  handleChangeServiceTicketBoard = (event: any, idx: number) => {
    const newState = _cloneDeep(this.state);
    newState.service_ticket_board_mapping[idx].board_id = event.target.value;
    newState.service_ticket_board_mapping[idx].board_name = event.target.label;
    newState.service_ticket_board_mapping[idx].ticket_statuses = [];
    this.setState(newState, () => this.fetchBoardStatuses(event.target.value));
  };

  handleChangeReceivedStatusMapping = (e: any, idx: number) => {
    const newState = _cloneDeep(this.state);
    newState.extra_config.received_status_mapping[idx][e.target.name] = e.target.value;
    this.setState(newState);
  }

  addNewServiceTicketMapping = () => {
    this.setState({
      service_ticket_board_mapping: [
        ...this.state.service_ticket_board_mapping,
        _cloneDeep(OrderTrackingEmailSetting.emptyServiceTicketBoard),
      ],
    });
  };

  addNewReceivedStatusMapping = () => {
    this.setState({
      extra_config: {
        ...this.state.extra_config,
        received_status_mapping: [
          ...this.state.extra_config.received_status_mapping,
          { received_status_id: -1, mapped_status: ""}
        ]
      }
    });
  };

  

  onDeleteServiceRowClick = (idx: number) => {
    this.setState({
      service_ticket_board_mapping: [
        ...this.state.service_ticket_board_mapping.slice(0, idx),
        ...this.state.service_ticket_board_mapping.slice(idx + 1),
      ],
    });
  };

  onDeleteReceivedRowClick = (idx: number) => {
    const newState = _cloneDeep(this.state);
    newState.extra_config.received_status_mapping = [
      ...this.state.extra_config.received_status_mapping.slice(0, idx),
      ...this.state.extra_config.received_status_mapping.slice(idx + 1)
    ];
    this.setState(newState);
  }

  handleChangeOpportunityStatus = (e: any) => {
    const opportunity_status_ids = new Set(e.target.value);
    let result = [];
    this.state.opportunityStatusOptions.forEach((op_status) => {
      if (opportunity_status_ids.has(op_status.value)) {
        result.push({
          status_id: op_status.value,
          status_name: op_status.label,
        });
      }
    });
    this.setState({ opportunity_statuses: result });
  };

  handleChangeOpportunityStage = (e: any) => {
    const opportunity_stage_ids = new Set(e.target.value);
    let result = [];
    this.state.opportunityStageOptions.forEach((op_stage) => {
      if (opportunity_stage_ids.has(op_stage.value)) {
        result.push({
          stage_id: op_stage.value,
          stage_name: op_stage.label,
        });
      }
    });
    this.setState({ opportunity_stages: result });
  };

  handleChangeCarrierURL = (obj) => {
    const newState = _cloneDeep(this.state);
    newState.extra_config.carrier_tracking_url = obj;
    this.setState(newState);
  };
  render() {
    return (
      <div className="operations-setting">
        {(this.props.isFetching || this.props.isFetchingType || this.state.loadingServiceTickets) && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <Collapsible label={'Purchase order status'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderPOEmailStatus()}
          </ul>
        </Collapsible>
        <Collapsible label={'Email Settings'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderOTEmailSetting()}
          </ul>
        </Collapsible>
        <Collapsible label={'product category mapping'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderContractsCarrierTracking()}
            {this.renderLicensesCarrierTracking()}
          </ul>
        </Collapsible>
        <Collapsible label={'Contact Type Mapping'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderContactTypeMapping()}
          </ul>
        </Collapsible>
        <Collapsible label={'Opportunity Statuses'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderOpportunityStatuses()}
          </ul>
        </Collapsible>
        <Collapsible label={'Opportunity Stages'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
          {this.renderOpportunityStages()}
          </ul>
        </Collapsible>
        <Collapsible label={'Service Ticket Status Board Mapping'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderServiceTicketBoardMapping()}
          </ul>
        </Collapsible>
        <Collapsible label={'Part Number Exclusion'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderPartNumberExclusion()}
          </ul>
        </Collapsible>
        <Collapsible label={'Received Status Mapping'} isOpen={this.state.isOpen}>
          <ul className="namespace-list">
            {this.renderReceivedStatusMapping()}
          </ul>
        </Collapsible>
        <CarrierTrackingUrl
          onSubmit={this.handleChangeCarrierURL}
          UrlList={this.state.extra_config.carrier_tracking_url}
          shipments={this.props.shipments}
          loadingShipments={this.props.isFetchingShipments}
        />
        <div className="po-setting-save-section">
          <SquareButton
            content="Save"
            bsStyle={"primary"}
            onClick={this.savePurchaseOrderSettings}
            className="submit-email-data"
            disabled={this.props.isFetching}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  shipments: state.setting.shipments,
  isFetching: state.setting.isFetching,
  isFetchingType: state.setting.isFetchingType,
  isFetchingShipments: state.setting.isFetchingShipments,
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
  receivedStatusList: state.setting.receivedStatusList
});

const mapDispatchToProps = (dispatch: any) => ({
  getPurchaseOrderStatusList: () => dispatch(getPurchaseOrderStatusList()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  editPurchaseOrderSettings: (data: any) => dispatch(editPurchaseOrderSettings(data)),
  getPurchaseOrderSettings: () => dispatch(getPurchaseOrderSettings()),
  getShipmentMethods: () => dispatch(getShipmentMethods()),
  fetchOrderTrackingSettings: () => dispatch(fetchOrderTrackingSettings()),
  getServiceBoards: () => dispatch(getServiceBoards()),
  getServiceBoardTickets: (boardId) => dispatch(fetchProviderBoardStatus(boardId)),
  getOpportunityStatuses: () => dispatch(getQuoteStatusesList()),
  getOpportunityStages: () => dispatch(getQuoteAllStages())
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTrackingEmailSetting);
