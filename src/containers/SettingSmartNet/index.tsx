import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import showdown from "showdown";
import SimpleMDE from "react-simplemde-editor";
import validator from "validator";
import _cloneDeep from "lodash/cloneDeep";
import {
  getSmartNetSettings,
  getSmartNetSettingsAttachments,
  editSmartNetSettings,
  getShipmentMethods,
  getPurchaseOrderStatusList,
  GET_PO_STATUS_SUCCESS,
  GET_SMART_NET_SETTING_SUCCESS,
  sendTestEmailSmartNet,
  TEST_EMAIL_SNT_SUCCESS,
  editPurchaseOrderSettings,
  getPurchaseOrderSettings,
  getRecievingStatuses,
  getMSAgentAssociation,
  GET_MS_AGENT_ACC_SUCCESS,
  postMSAgentAssociation,
} from "../../actions/setting";
import {
  getQuoteBusinessList,
  GET_BUSINESS_SUCCESS,
  getQuoteStatusesList,
  GET_STATUSES_SUCCESS,
  getQuoteTypeList,
} from "../../actions/sow";
import { fetchTerritoryMembers } from "../../actions/provider/integration";
import { addSuccessMessage, addErrorMessage } from "../../actions/appState";
import Attachments from "./attachments";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import Collapsible from "../../components/Collapsible";
import SquareButton from "../../components/Button/button";
import { fetchProviderUsers } from "../../actions/provider/user";
import SubscriptionNotice from "./subscriptionNotice";
import OrderTrackingEmailSetting from "./emailTemplateSetting";
import OrderTrackingDashboardSetting from "./orderTrackingDashboardSetting";
import WeeklyOrderTracking from "./weeklyOrderTracking";
import Collections from "./collections";
import SoPoSync from "./soPoSync";
import "../../commonStyles/collapsable.scss";
import "./style.scss";

interface IISettingSmartNetState {
  settings: ISmartNetSetting;
  error: {
    email_body: IFieldValidation;
    email_subject: IFieldValidation;
    order_status_alias_email: IFieldValidation;
    tac_email_body: IFieldValidation;
    tac_email_subject: IFieldValidation;
    tac_email: IFieldValidation;
    smartnet_receiving_ticket_closing_note: IFieldValidation;
    shipment_method_id: IFieldValidation;
    purchase_order_open_statuses: IFieldValidation;
    email_notifications_from_email: IFieldValidation;
    opportunity_default_inside_sales_rep: IFieldValidation;
    opportunity_business_unit_id: IFieldValidation;
    opportunity_type_id: IFieldValidation;
    opportunity_status_id: IFieldValidation;
    email_body_markdown: IFieldValidation;
    partial_receive_status: IFieldValidation;
    ms_agent_association: IFieldValidation;
  };
  submitted: boolean;
  open: boolean;
  openTAC: boolean;
  uploadFile: any;
  poStatusList: IPickListOptions[];
  businessList: IPickListOptions[];
  statusesList: IPickListOptions[];
  emailLabel: string;
  isOpen: boolean;
  recieving_status_options: IPickListOptions[];
  ms_agent_association: {
    provider: number;
    user: any[];
  };
  ms_agent_change: boolean;
}

interface IISettingSmartNetProps extends ICommonProps {
  isFetching: boolean;
  attachmentsList: any;
  terretoryMembers: any;
  isFetchingType: boolean;
  isFetchingShipments: boolean;
  qTypeList: IDLabelObject[];
  providerUsers: ISuperUser[];
  shipments: IPickListOptions[];
  receivedStatusList: IPickListOptions[];
  smartNetSetting: ISmartNetSetting;
  purchaseOrderSetting: IPurchaseOrderSetting;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getQuoteTypeList: () => Promise<any>;
  getShipmentMethods: () => Promise<any>;
  getSmartNetSettings: () => Promise<any>;
  getQuoteStatusesList: () => Promise<any>;
  getQuoteBusinessList: () => Promise<any>;
  fetchTerritoryMembers: () => Promise<any>;
  getPurchaseOrderStatusList: () => Promise<any>;
  editSmartNetSettings: (data: any) => Promise<any>;
  getSmartNetSettingsAttachments: () => Promise<any>;
  sendTestEmailSmartNet: (data: any) => Promise<any>;
  editPurchaseOrderSettings: () => Promise<any>;
  getPurchaseOrderSettings: () => Promise<any>;
  getRecievingStatuses: () => Promise<any>;
  getMSAgentAssociation: () => Promise<any>;
  fetchProviderUsers: (params?: IServerPaginationParams) => Promise<any>;
  postMSAgentAssociation: (data: any, method: string) => Promise<any>;
}

class SettingSmartNet extends Component<
  IISettingSmartNetProps,
  IISettingSmartNetState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    settings: {
      email_body: "",
      email_subject: "",
      order_status_alias_email: "",
      tac_email: "",
      tac_email_subject: "",
      tac_email_body: "",
      smartnet_receiving_ticket_closing_note: "",
      shipment_method_id: null,
      purchase_order_open_statuses: [],
      email_notifications_from_email: "",
      opportunity_default_inside_sales_rep: "",
      opportunity_business_unit_id: null,
      opportunity_type_id: null,
      opportunity_status_id: null,
      email_body_markdown: "",
      partial_receive_status: "",
    },
    error: {
      email_body: { ...SettingSmartNet.emptyErrorState },
      email_subject: { ...SettingSmartNet.emptyErrorState },
      order_status_alias_email: { ...SettingSmartNet.emptyErrorState },
      tac_email_body: { ...SettingSmartNet.emptyErrorState },
      tac_email_subject: { ...SettingSmartNet.emptyErrorState },
      tac_email: { ...SettingSmartNet.emptyErrorState },
      smartnet_receiving_ticket_closing_note: {
        ...SettingSmartNet.emptyErrorState,
      },
      shipment_method_id: { ...SettingSmartNet.emptyErrorState },
      purchase_order_open_statuses: { ...SettingSmartNet.emptyErrorState },
      email_notifications_from_email: { ...SettingSmartNet.emptyErrorState },
      opportunity_default_inside_sales_rep: {
        ...SettingSmartNet.emptyErrorState,
      },
      opportunity_business_unit_id: { ...SettingSmartNet.emptyErrorState },
      opportunity_type_id: { ...SettingSmartNet.emptyErrorState },
      opportunity_status_id: { ...SettingSmartNet.emptyErrorState },
      email_body_markdown: { ...SettingSmartNet.emptyErrorState },
      partial_receive_status: { ...SettingSmartNet.emptyErrorState },
      ms_agent_association: { ...SettingSmartNet.emptyErrorState },
    },
    submitted: false,
    open: true,
    openTAC: true,
    isUploadVisible: false,
    uploadFile: null,
    shipments: [],
    poStatusList: [],
    businessList: [],
    statusesList: [],
    emailLabel: "Send Email",
    isOpen: false,
    recieving_status_options: [],
    ms_agent_association: {
      provider: null,
      user: [],
    },
    ms_agent_change: false,
  });

  componentDidMount() {
    this.props.getSmartNetSettings();
    this.props.getPurchaseOrderSettings();
    this.props.getShipmentMethods();
    this.getPOStatusList();
    this.props.fetchTerritoryMembers();
    this.props.getQuoteTypeList();
    this.getQuoteStatusesList();
    this.getQuoteBusinessList();
    this.props.getRecievingStatuses();
    this.getMSAgentAssociation();
    this.props.fetchProviderUsers({ pagination: false });
  }
  getIntance = (instance) => {
    // You can now store and manipulate the simplemde instance.
    instance.togglePreview();
  };

  getMSAgentAssociation = () => {
    this.props.getMSAgentAssociation().then((action) => {
      if (action.type === GET_MS_AGENT_ACC_SUCCESS) {
        this.setState({ ms_agent_association: action.response });
      }
    });
  };

  getPOStatusList = () => {
    this.props.getPurchaseOrderStatusList().then((action) => {
      if (action.type === GET_PO_STATUS_SUCCESS) {
        const statusList = action.response;
        const poStatusList = statusList
          ? statusList.map((c) => ({
              value: c.id,
              label: c.name,
            }))
          : [];
        this.setState({ poStatusList });
      }
    });
  };

  getQuoteBusinessList = () => {
    this.props.getQuoteBusinessList().then((action) => {
      if (action.type === GET_BUSINESS_SUCCESS) {
        const poTicketsRaw = action.response;
        const businessList = poTicketsRaw
          ? poTicketsRaw.map((c) => ({
              value: c.id,
              label: ` ${c.label}`,
            }))
          : [];
        this.setState({ businessList });
      }
    });
  };

  getQuoteStatusesList = () => {
    this.props.getQuoteStatusesList().then((action) => {
      if (action.type === GET_STATUSES_SUCCESS) {
        const poTicketsRaw = action.response;
        const statusesList = poTicketsRaw
          ? poTicketsRaw.map((c) => ({
              value: c.id,
              label: `${c.label}`,
            }))
          : [];
        this.setState({ statusesList });
      }
    });
  };

  componentDidUpdate(prevProps: IISettingSmartNetProps) {
    if (
      this.props.smartNetSetting &&
      this.props.smartNetSetting !== prevProps.smartNetSetting
    ) {
      const smartNetSetting: ISmartNetSetting = _.cloneDeep(this.props.smartNetSetting);
      smartNetSetting.email_body = smartNetSetting.email_body_markdown;
      this.setState({
        settings: smartNetSetting,
      });
    }
    if (
      this.props.receivedStatusList &&
      this.props.receivedStatusList !== prevProps.receivedStatusList
    ) {
      this.setState({ recieving_status_options: this.props.receivedStatusList });
    }
  }
  
  handleChange = (event: any) => {
    const newState = _cloneDeep(this.state);
    newState.settings[event.target.name] = event.target.value;
    this.setState(newState);
  };
  handleChangeSowMarkDown = (event: any) => {
    const newState = _cloneDeep(this.state);
    newState.settings.email_body = event;
    this.setState(newState);
  };

  sendTestEmailSmartNet = () => {
    if (this.sntEmailvalidateForm()) {
      const convert = new showdown.Converter();
      const payload = {
        to_email: this.state.settings.order_status_alias_email,
        email_body: convert.makeHtml(this.state.settings.email_body),
      };
      this.setState({ emailLabel: "Sending..." });
      this.props.sendTestEmailSmartNet(payload).then((action) => {
        if (action.type === TEST_EMAIL_SNT_SUCCESS) {
          this.props.addSuccessMessage("Email delivered.");
          this.setState({ emailLabel: "Send Email" });
        }
      });
    }
  };

  saveSmartNetSettings = () => {
    this.setState({ isOpen: false });
    if (this.validateForm()) {
      let settings = { ...this.state.settings };
      const convert = new showdown.Converter();
      settings.email_body_markdown = settings.email_body;
      settings.email_body = convert.makeHtml(this.state.settings.email_body);

      this.props.editSmartNetSettings(settings).then((action) => {
        if (action.type === GET_SMART_NET_SETTING_SUCCESS) {
          this.props.addSuccessMessage("Smartnet Setting Updated.");
        }
      });
    }
  };

  validateForm = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.settings.email_body ||
      this.state.settings.email_body.trim().length === 0
    ) {
      error.email_body.errorState = "error";
      error.email_body.errorMessage = "SNT Email body  cannot be empty";
      isValid = false;
    }
    if (!this.state.settings.order_status_alias_email) {
      error.order_status_alias_email.errorState = "error";
      error.order_status_alias_email.errorMessage = "Enter a valid email";

      isValid = false;
    }
    if (
      this.state.settings.order_status_alias_email &&
      !validator.isEmail(this.state.settings.order_status_alias_email)
    ) {
      error.order_status_alias_email.errorState = "error";
      error.order_status_alias_email.errorMessage = "Enter a valid email";

      isValid = false;
    }
    if (
      !this.state.settings.tac_email_subject ||
      this.state.settings.tac_email_subject.trim().length === 0
    ) {
      error.tac_email_subject.errorState = "error";
      error.tac_email_subject.errorMessage =
        "TAC Email Subject  cannot be empty";
      isValid = false;
    }
    if (!this.state.settings.tac_email) {
      error.tac_email.errorState = "error";
      error.tac_email.errorMessage = "Enter a valid email";

      isValid = false;
    }
    if (
      this.state.settings.tac_email &&
      !validator.isEmail(this.state.settings.tac_email)
    ) {
      error.tac_email.errorState = "error";
      error.tac_email.errorMessage = "Enter a valid email";

      isValid = false;
    }

    if (
      !this.state.settings.smartnet_receiving_ticket_closing_note ||
      this.state.settings.smartnet_receiving_ticket_closing_note.trim()
        .length === 0
    ) {
      error.smartnet_receiving_ticket_closing_note.errorState =
        "error";
      error.smartnet_receiving_ticket_closing_note.errorMessage =
        "Note  cannot be empty";
      isValid = false;
    }
    if (!this.state.settings.shipment_method_id) {
      error.shipment_method_id.errorState = "error";
      error.shipment_method_id.errorMessage = "Please select shipment method";

      isValid = false;
    }

    if (
      !this.state.settings.purchase_order_open_statuses ||
      this.state.settings.purchase_order_open_statuses.length === 0
    ) {
      error.purchase_order_open_statuses.errorState = "error";
      error.purchase_order_open_statuses.errorMessage =
        "Please select purchase order open statuses";

      isValid = false;
    }

    if (!this.state.settings.email_notifications_from_email) {
      error.email_notifications_from_email.errorState = "error";
      error.email_notifications_from_email.errorMessage = "Enter a valid email";

      isValid = false;
    }
    if (
      this.state.settings.email_notifications_from_email &&
      !validator.isEmail(this.state.settings.email_notifications_from_email)
    ) {
      error.email_notifications_from_email.errorState = "error";
      error.email_notifications_from_email.errorMessage = "Enter a valid email";

      isValid = false;
    }
    if (!this.state.settings.opportunity_type_id) {
      error.opportunity_type_id.errorState = "error";
      error.opportunity_type_id.errorMessage = "Please select opportunity type";

      isValid = false;
    }

    if (!this.state.settings.opportunity_business_unit_id) {
      error.opportunity_business_unit_id.errorState = "error";
      error.opportunity_business_unit_id.errorMessage =
        "Please select opportunity business";

      isValid = false;
    }
    if (!this.state.settings.opportunity_status_id) {
      error.opportunity_status_id.errorState = "error";
      error.opportunity_status_id.errorMessage =
        "Please select opportunity status";

      isValid = false;
    }
    if (!this.state.settings.opportunity_default_inside_sales_rep) {
      error.opportunity_default_inside_sales_rep.errorState =
        "error";
      error.opportunity_default_inside_sales_rep.errorMessage =
        "Please select opportunity inside sales rep";

      isValid = false;
    }

    this.setState({ error, isOpen: !isValid });

    return isValid;
  };

  sntEmailvalidateForm = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.settings.email_body ||
      this.state.settings.email_body.trim().length === 0
    ) {
      error.email_body.errorState = "error";
      error.email_body.errorMessage = "SNT Email body  cannot be empty";
      isValid = false;
    }
    if (!this.state.settings.order_status_alias_email) {
      error.order_status_alias_email.errorState = "error";
      error.order_status_alias_email.errorMessage = "Enter a valid email";

      isValid = false;
    }
    if (
      this.state.settings.order_status_alias_email &&
      !validator.isEmail(this.state.settings.order_status_alias_email)
    ) {
      error.order_status_alias_email.errorState = "error";
      error.order_status_alias_email.errorMessage = "Enter a valid email";

      isValid = false;
    }

    this.setState({ error });

    return isValid;
  };

  getCustomerUserOptions = () => {
    return this.props.terretoryMembers
      ? this.props.terretoryMembers.map((user) => ({
          value: user.id,
          label: `${_.get(user, "first_name", "")}${_.get(
            user,
            "last_name",
            ""
          )}`,
        }))
      : [];
  };
  renderSmartNetEmailSection = () => {
    return (
      <div className="snt-email col-md-12">
        <div className={`col-md-12 heading`}>SNT Email Settings</div>
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: "Order status alias email",
              type: "TEXT",
              isRequired: true,
              value: this.state.settings.order_status_alias_email,
            }}
            width={12}
            name="order_status_alias_email"
            onChange={this.handleChange}
            error={this.state.error.order_status_alias_email}
            placeholder="Enter Email"
          />
          <div className="field-section  col-md-12 col-xs-12">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Email Body
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.email_body.errorMessage ? `error-input` : ""
              }`}
            >
              <SimpleMDE
                onChange={(e) => this.handleChangeSowMarkDown(e)}
                options={{
                  placeholder: "",
                  autofocus: true,
                  spellChecker: true,
                  hideIcons: [
                    "guide",
                    "heading",
                    "fullscreen",
                    "side-by-side",
                    "image",
                  ],
                }}
                value={this.state.settings.email_body}
                className={`${"markdown-editor-input"}`}
                getMdeInstance={this.getIntance}
              />
            </div>
            {this.state.error.email_body.errorMessage && (
              <div className="field__error">
                {this.state.error.email_body.errorMessage}
              </div>
            )}
          </div>
          <SquareButton
            content={this.state.emailLabel}
            bsStyle={"primary"}
            onClick={this.sendTestEmailSmartNet}
            className="submit-email-data"
            disabled={this.props.isFetching}
          />
        </div>
      </div>
    );
  };

  renderTACEmailSection = () => {
    return (
      <div className="tac-email col-md-12">
        <div className={`col-md-12 heading `}>TAC Email Settings</div>
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: "TAC email",
              type: "TEXT",
              isRequired: true,
              value: this.state.settings.tac_email,
            }}
            width={12}
            name="tac_email"
            onChange={this.handleChange}
            error={this.state.error.tac_email}
            placeholder="Enter Email"
          />
          <Input
            field={{
              label: "TAC Email  Subject",
              type: "TEXTAREA",
              isRequired: true,
              value: this.state.settings.tac_email_subject,
            }}
            width={12}
            name="tac_email_subject"
            onChange={this.handleChange}
            error={this.state.error.tac_email_subject}
            placeholder="Enter Email Subject"
          />
        </div>
      </div>
    );
  };

  renderTicketNoteSection = () => {
    return (
      <div className="tac-email col-md-12">
        <div className={`col-md-12 heading `}>Ticket Closing Note Settings</div>
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: "Smartnet Receiving Ticket Closing Note",
              type: "TEXTAREA",
              isRequired: true,
              value: this.state.settings.smartnet_receiving_ticket_closing_note,
            }}
            width={12}
            name="smartnet_receiving_ticket_closing_note"
            onChange={this.handleChange}
            error={this.state.error.smartnet_receiving_ticket_closing_note}
          />
        </div>
      </div>
    );
  };

  renderShippingMethod = () => {
    return (
      <div className="tac-email col-md-12">
        <div className={`col-md-12 heading `}>Shipment Setting</div>
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: " Shipment Method",
              type: "PICKLIST",
              value: this.state.settings.shipment_method_id,
              options: this.props.shipments,
              isRequired: true,
            }}
            width={12}
            multi={false}
            name="shipment_method_id"
            onChange={(e) => this.handleChange(e)}
            loading={this.props.isFetchingShipments}
            error={this.state.error.shipment_method_id}
            placeholder={`Select`}
          />
        </div>
      </div>
    );
  };

  renderRecievingStatusSetting = () => {
    return (
      <div className="tac-email col-md-12">
        <div className={`col-md-12 heading `}>Recieving Status</div>
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: "Recieving Status",
              type: "PICKLIST",
              value: this.state.settings.partial_receive_status,
              options: this.state.recieving_status_options,
              isRequired: true,
            }}
            width={12}
            multi={false}
            name="partial_receive_status"
            onChange={(e) => this.handleChange(e)}
            error={this.state.error.partial_receive_status}
            placeholder={`Select`}
          />
        </div>
      </div>
    );
  };

  renderPOStatus = () => {
    return (
      <div className="tac-email col-md-12">
        <div className={`col-md-12 heading `}>Purchase Order Status</div>
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: "Purchase Order Open Status",
              type: "PICKLIST",
              value: this.state.settings.purchase_order_open_statuses,
              options: this.state.poStatusList,
              isRequired: true,
            }}
            width={12}
            multi={true}
            name="purchase_order_open_statuses"
            onChange={(e) => this.handleChange(e)}
            error={this.state.error.purchase_order_open_statuses}
            placeholder={`Select`}
          />
        </div>
      </div>
    );
  };

  renderDefaultFields = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              label: "Email Notifications From Email",
              type: "TEXT",
              isRequired: true,
              value: this.state.settings.email_notifications_from_email,
            }}
            width={6}
            name="email_notifications_from_email"
            onChange={this.handleChange}
            error={this.state.error.email_notifications_from_email}
          />
        </div>
      </div>
    );
  };

  renderDefaultOpportunityFields = () => {
    const types = this.props.qTypeList
      ? this.props.qTypeList.map((t) => ({
          value: t.id,
          label: t.label,
        }))
      : [];

    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 col-lg-8 mapping-row">
          <Input
            field={{
              value: this.state.settings.opportunity_type_id,
              label: "Opportunity Type",
              type: "PICKLIST",
              isRequired: true,
              options: types,
            }}
            width={6}
            name={"opportunity_type_id"}
            placeholder="Select Status"
            onChange={(e) => this.handleChange(e)}
            className="select-type"
            error={this.state.error.opportunity_type_id}
          />

          <Input
            field={{
              value: this.state.settings.opportunity_status_id,
              label: "Status",
              type: "PICKLIST",
              isRequired: true,
              options: this.state.statusesList,
            }}
            width={6}
            name={"opportunity_status_id"}
            placeholder="Select Status"
            onChange={(e) => this.handleChange(e)}
            className="select-type"
            error={this.state.error.opportunity_status_id}
          />
          <Input
            field={{
              value: this.state.settings.opportunity_business_unit_id,
              label: "Select business",
              type: "PICKLIST",
              isRequired: true,
              options: this.state.businessList,
            }}
            width={6}
            name={"opportunity_business_unit_id"}
            placeholder="Select Business"
            onChange={(e) => this.handleChange(e)}
            className="select-type"
            error={this.state.error.opportunity_business_unit_id}
          />
          <Input
            field={{
              label: "Opportunity default Inside sales rep",
              type: "PICKLIST",
              isRequired: false,
              value: this.state.settings.opportunity_default_inside_sales_rep,
              options: this.getCustomerUserOptions(),
            }}
            width={6}
            name="opportunity_default_inside_sales_rep"
            onChange={this.handleChange}
            error={this.state.error.opportunity_default_inside_sales_rep}
          />
        </div>
      </div>
    );
  };

  saveMSAgentAssociation = () => {
    this.props
      .postMSAgentAssociation(
        this.state.ms_agent_association,
        this.state.ms_agent_association.provider > 0 ? "put" : "post"
      )
      .then((action) => {
        if (action.type === GET_MS_AGENT_ACC_SUCCESS) {
          this.props.addSuccessMessage("Association updated.");
          this.setState({ ms_agent_change: false });
        }
      });
  };

  renderMSAgent = () => {
    return (
      <div className="ms-agent-section col-md-12">
        <div className="col-xs-10 mapping-row">
          <Input
            field={{
              label: "Provider Users",
              type: "PICKLIST",
              value:
                this.state.ms_agent_association &&
                this.state.ms_agent_association.user,
              options:
                this.props.providerUsers &&
                this.props.providerUsers.length &&
                this.props.providerUsers
                  .filter((x) => Boolean(x.profile && x.profile.cco_id))
                  .map((user) => ({
                    value: user.id,
                    label: `${user.first_name} ${user.last_name} (${user.email}) (CCO ID: ${user.profile.cco_id})`,
                  })),
            }}
            width={12}
            multi={true}
            name="ms_agent_association"
            onChange={(e) => {
              this.setState((prevState) => ({
                ms_agent_change: true,
                ms_agent_association: {
                  ...prevState.ms_agent_association,
                  user: e.target.value,
                },
              }));
            }}
            error={this.state.error.ms_agent_association}
            placeholder={`Select`}
          />
        </div>
        {this.state.ms_agent_change && (
          <SquareButton
            content="Save Changes"
            bsStyle={"primary"}
            onClick={this.saveMSAgentAssociation}
            className="submit-ms-agent col-xs-2"
          />
        )}
      </div>
    );
  };

  smartNetSetting = () => {
    return (
      <div className="smartnet-setting">
        {this.props.isFetching ||
          (this.props.isFetchingType && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          ))}
        <div className="email-section">
          <Collapsible label={"SNT Email Settings"} isOpen={this.state.isOpen}>
            <ul className="namespace-list">
              {this.renderSmartNetEmailSection()}
            </ul>
          </Collapsible>
          <Collapsible label={"TAC Email Settings"} isOpen={this.state.isOpen}>
            <ul className="namespace-list">{this.renderTACEmailSection()}</ul>
          </Collapsible>
          <Collapsible label={"Shipment Setting"} isOpen={this.state.isOpen}>
            <ul className="namespace-list">{this.renderShippingMethod()}</ul>
          </Collapsible>
          <Collapsible
            label={"Recieving Status Setting"}
            isOpen={this.state.isOpen}
          >
            <ul className="namespace-list">
              {this.renderRecievingStatusSetting()}
            </ul>
          </Collapsible>
          <Collapsible
            label={"Purchase Order Status"}
            isOpen={this.state.isOpen}
          >
            <ul className="namespace-list">{this.renderPOStatus()}</ul>
          </Collapsible>
          <Collapsible
            label={"Ticket Closing Note Settings"}
            isOpen={this.state.isOpen}
          >
            <ul className="namespace-list">{this.renderTicketNoteSection()}</ul>
          </Collapsible>

          <Collapsible label={"Default fields"} isOpen={this.state.isOpen}>
            <ul className="namespace-list">{this.renderDefaultFields()}</ul>
          </Collapsible>

          <Collapsible
            label={"Default Opportunity fields"}
            isOpen={this.state.isOpen}
          >
            <ul className="namespace-list">
              {this.renderDefaultOpportunityFields()}
            </ul>
          </Collapsible>
        </div>
        {
          <div className="error-msg">
            {this.state.error &&
              Object.keys(this.state.error).map((e, idx) => {
                return (
                  <div className="message" key={idx}>
                    {this.state.error[e].errorMessage}
                  </div>
                );
              })}
          </div>
        }
        <div className="attachment-save-section">
          <SquareButton
            content="Save"
            bsStyle={"primary"}
            onClick={this.saveSmartNetSettings}
            className="submit-email-data"
            disabled={this.props.isFetching}
          />
        </div>
        <Collapsible
          label={"MS Agent Provider Association"}
          isOpen={this.state.isOpen}
        >
          <ul className="namespace-list">{this.renderMSAgent()}</ul>
        </Collapsible>
        {this.props.smartNetSetting && this.props.smartNetSetting.id && (
          <div className="attachment-section">
            <Collapsible label={"Attachments"} isOpen={true}>
              <ul className="namespace-list">
                <Attachments {...this.props} />
              </ul>
            </Collapsible>
          </div>
        )}
      </div>
    );
  };

  render() {
    return (
      <div className="operations-setting">
        <HorizontalTabSlider scrollOffset={400}>
          <Tab title={"Smartnet Settings"}>{this.smartNetSetting()}</Tab>
          <Tab title={"Email Template Settings"}>
            <OrderTrackingEmailSetting />
          </Tab>
          <Tab title={"Order Tracking Dashboard Settings"}>
            <OrderTrackingDashboardSetting />
          </Tab>
          <Tab title={"Collections"}>
            <Collections />
          </Tab>
          <Tab title={"Lifecycle Notifications"}>
            <SubscriptionNotice />
          </Tab>
          <Tab title={"SO-PO Data Sync"}>
            <SoPoSync />
          </Tab>
          <Tab title={"Weekly Tracking Reports"}>
            <WeeklyOrderTracking />
          </Tab>
        </HorizontalTabSlider>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  shipments: state.setting.shipments,
  isFetching: state.setting.isFetching,
  isFetchingType: state.setting.isFetchingType,
  isFetchingShipments: state.setting.isFetchingShipments,
  attachmentsList: state.setting.attachmentsList,
  smartNetSetting: state.setting.smartNetSetting,
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
  terretoryMembers: state.integration.terretoryMembers,
  qTypeList: state.sow.qTypeList,
  providerUsers: state.providerUser.providerUsers,
  user: state.profile.user,
  receivedStatusList: state.setting.receivedStatusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStatusesList: () => dispatch(getQuoteStatusesList()),
  getQuoteBusinessList: () => dispatch(getQuoteBusinessList()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  getSmartNetSettings: () => dispatch(getSmartNetSettings()),
  getShipmentMethods: () => dispatch(getShipmentMethods()),
  getPurchaseOrderStatusList: () => dispatch(getPurchaseOrderStatusList()),
  editSmartNetSettings: (data: any) => dispatch(editSmartNetSettings(data)),
  getSmartNetSettingsAttachments: () =>
    dispatch(getSmartNetSettingsAttachments()),
  fetchTerritoryMembers: () => dispatch(fetchTerritoryMembers()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  sendTestEmailSmartNet: (data: any) => dispatch(sendTestEmailSmartNet(data)),
  editPurchaseOrderSettings: (data: any) =>
    dispatch(editPurchaseOrderSettings(data)),
  getPurchaseOrderSettings: () => dispatch(getPurchaseOrderSettings()),
  getRecievingStatuses: () => dispatch(getRecievingStatuses()),
  getMSAgentAssociation: () => dispatch(getMSAgentAssociation()),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  postMSAgentAssociation: (data: any, method: string) =>
    dispatch(postMSAgentAssociation(data, method)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingSmartNet);
