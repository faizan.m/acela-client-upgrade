import ControlledEditor from "@monaco-editor/react";
import React from "react";
import Highlighter from "react-highlight-words";
import Spinner from "../../../components/Spinner";
import { fromISOStringToDateTimeString } from "../../../utils/CalendarUtil";
import _, { isEmpty } from "lodash";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import Table from "../../../components/Table/table";
import { RuleDetails } from "../RulesContainer/RuleDetails";
import Input from "../../../components/Input/input";
import DeleteButton from "../../../components/Button/deleteButton";
import EditRules from "../RulesContainer/EditRules";
import Checkbox from "../../../components/Checkbox/checkbox";
import SquareButton from "../../../components/Button/button";
import { connect } from "react-redux";
import {
  fetchDeviceConfigurationDetail,
  fetchCustomerDeviceConfigurationDetail,
} from "../../../actions/configuration";
import {
  RERUN_COMPLIANCE_SUCCESS,
  rerunConfigCompliance,
} from "../../../actions/collector";
import {
  TASK_STATUS_SUCCESS,
  TASK_STATUS_FAILURE,
  fetchTaskStatus,
} from "../../../actions/inventory";
import "../../../commonStyles/collapsable.scss";

enum PageType {
  Config,
  Violations,
  ViolationsNew,
  Bundles,
}
interface IViewMonitoringProps extends ICommonProps {
  show?: boolean;
  onClose?: (e: any) => void;
  manufacturers?: any[];
  configuration?: IConfiguration;
  isFetching: boolean;
  userType?: string;
  addSuccessMessage?: any;
  customerVariablesAll?: any;
  fetchDeviceConfigurationDetail: any;
  fetchCustomerDeviceConfigurationDetail: any;
  detailConfig: any;
  isFetchingConfigDetail: boolean;
  rerunConfigCompliance: any;
  fetchTaskStatus: any;
  isPostingBatch: any;
  addErrorMessage?: any;
}

interface IEditDeviceFormState {
  searchKey: string;
  activeIndex: number;
  lineNo: number;
  currentPage: {
    pageType: PageType;
  };
  openSectionInvalid: boolean;
  openSectionValid: boolean;
  isCollapsedValid: boolean[];
  isCollapsedInvalid: boolean[];
  configuration: IConfiguration;
  openViewModalRule: boolean;
  openEditModalRule: boolean;
  isCollapsed: boolean[];
  rule?: IRule;
  searchString: string;
  selectedRules: any[];
  showComplianceRerunBtn: boolean;
  isSuccess: boolean;
}

class ViewMonitoring extends React.Component<
  IViewMonitoringProps,
  IEditDeviceFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  props: any;
  static _editor;
  constructor(props: IViewMonitoringProps) {
    super(props);
    this.state = {
      searchKey: "",
      activeIndex: -1,
      lineNo: 0,
      currentPage: {
        pageType: PageType.Config,
      },
      openSectionInvalid: true,
      openSectionValid: false,
      isCollapsedValid: [],
      isCollapsedInvalid: [],
      configuration: null,
      openViewModalRule: false,
      openEditModalRule: false,
      isCollapsed: [],
      searchString: "",
      selectedRules: [],
      showComplianceRerunBtn: false,
      isSuccess: false,
    };
  }

  componentDidMount() {
    if (this.props.configuration) {
      this.props.fetchDeviceConfigurationDetail();
    }
  }

  componentDidUpdate(prevProps: IViewMonitoringProps) {
    if (
      this.props.configuration &&
      this.props.configuration !== prevProps.configuration
    ) {
      let configuration = this.props.configuration;
      configuration.compliance_statistics &&
        Array.isArray(configuration.compliance_statistics) &&
        configuration.compliance_statistics.map((s, index) => {
          s.isOpen = false;
          return s;
        });
      configuration.valid_compliance_statistics &&
        configuration.valid_compliance_statistics.map((s, index) => {
          s.id = index;
          return s;
        });
      configuration.invalid_compliance_statistics &&
        configuration.invalid_compliance_statistics.map((s, index) => {
          s.id = index;
          return s;
        });
      this.setState({
        configuration,
        isCollapsed: _.get(
          this.props,
          `configuration.compliance_statistics`,
          []
        ).map((key) => false),
        isCollapsedValid: _.get(
          this.props,
          `configuration.valid_compliance_statistics`,
          []
        ).map((key) => false),
        isCollapsedInvalid: _.get(
          this.props,
          `configuration.invalid_compliance_statistics`,
          []
        ).map((key) => false),
      });
    }
  }

  onClose = (event: any) => {
    this.setState({ searchKey: "" });
    this.props.onClose(event);
  };

  handleEditorDidMount = (_valueGetter, editor) => {
    ViewMonitoring._editor = editor;
    editor.focus();
    editor.getAction("actions.find").run();
  };

  scrolltoLine = (LineNumber) => {
    this.setState(
      {
        currentPage: {
          pageType: PageType.Config,
        },
      },
      () => {
        setTimeout(() => {
          do {
            ViewMonitoring._editor &&
              ViewMonitoring._editor.revealLinesInCenterIfOutsideViewport(
                LineNumber,
                LineNumber
              );
            ViewMonitoring._editor &&
              ViewMonitoring._editor.Selection(
                LineNumber,
                2,
                LineNumber + 20,
                3
              );
          } while (!ViewMonitoring._editor);
          // tslint:disable-next-line:align
        }, 500);
      }
    );
  };
  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="voilence__header">
        <div
          className={`voilence__header-link ${
            currentPage.pageType === PageType.Config
              ? "voilence__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Config)}
        >
          Configuration
        </div>
        <div
          className={`voilence__header-link ${
            currentPage.pageType === PageType.ViolationsNew
              ? "voilence__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.ViolationsNew)}
        >
          Compliance Result
        </div>
        <div
          className={`voilence__header-link ${
            currentPage.pageType === PageType.Bundles
              ? "voilence__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Bundles)}
        >
          Inventory
        </div>
      </div>
    );
  };

  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };
  showPartialCompliance = (valid_compliance: any) => {
    return (
      valid_compliance &&
      !isEmpty(valid_compliance) &&
      Object.keys(valid_compliance).map((missingSections, m) => (
        <div key={m + 22} className="d1">
          {missingSections && (
            <div style={{ color: "green" }}> {missingSections} </div>
          )}
          {valid_compliance[missingSections] &&
            valid_compliance[missingSections].map((k, id) => {
              return (
                <div
                  key={id}
                  style={{
                    paddingLeft: missingSections === "" ? 0 : "10px",
                    color: k.status === "valid" ? "green" : "red",
                  }}
                >
                  {k.value}
                </div>
              );
            })}
        </div>
      ))
    );
  };

  onClickReRunCompliance = (event: any, index?: any) => {
    const { selectedRules } = this.state;

    const data = {
      customer_id: this.props.customerId,
      rule_ids: selectedRules,
      devices_ids: [this.state.configuration.id],
    };

    this.props.rerunConfigCompliance(data).then((a) => {
      if (a.type === RERUN_COMPLIANCE_SUCCESS) {
        if (a.response && a.response.task_id) {
          this.fetchTaskStatus(a.response.task_id, "Re-run compliance ");
        }
      }
    });
  };

  fetchTaskStatus = (taskId: number, message: string) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then((a) => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === "SUCCESS"
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            // list();
            this.props.fetchDeviceConfigurationDetail();
            this.setState({ isSuccess: true });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "FAILURE"
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ isSuccess: true });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "PENDING"
          ) {
            setTimeout(() => {
              this.fetchTaskStatus(taskId, message);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.fetchDeviceConfigurationDetail();
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  renderDeviceDetails = () => {
    const options = {
      selectOnLineNumbers: false,
      readOnly: true,
    };
    const configuration = this.state.configuration;
    const currentPage = this.state.currentPage;
    const complianceStatistics = this.state.configuration.compliance_statistics;
    return (
      <div className="configuration-details-modal__body">
        <div className="configuration-details-modal__body-field">
          <label>Device Name</label>
          <label>
            {configuration.device_name ? configuration.device_name : "Unknown"}
          </label>
        </div>
        <div className="configuration-details-modal__body-field">
          <label>IP</label>
          <label>
            {configuration.host_name ? configuration.host_name : "-"}
          </label>
        </div>
        <div className="configuration-details-modal__body-field">
          <label>Model</label>
          <label>
            {configuration.model_number
              ? configuration.model_number
              : "Unknown"}
          </label>
        </div>
        <div className="configuration-details-modal__body-field">
          <label>Type</label>
          <label>
            {configuration.device_type ? configuration.device_type : "Unknown"}
          </label>
        </div>

        <div className="configuration-details-modal__body-field">
          <label>Location / Site</label>
          <label>{configuration.site ? configuration.site : "Unknown"}</label>
        </div>
        <div className="configuration-details-modal__body-field">
          <label>Config Last Updated</label>
          <label>
            {configuration.config_last_updated_on
              ? fromISOStringToDateTimeString(
                  configuration.config_last_updated_on
                )
              : "-"}
          </label>
        </div>
        <div className="voilence">
          {this.renderTopBar()}
          {currentPage.pageType === PageType.Config && (
            <div className="configuration-details-config-data col-md-12">
              {configuration.config && (
                <ControlledEditor
                  height="73vh"
                  width="1034px"
                  value={configuration.config}
                  onChange={(a) => null}
                  language="javascript"
                  options={options}
                  // editorDidMount={this.handleEditorDidMount}
                  theme="dark"
                />
              )}
            </div>
          )}
          {currentPage.pageType === PageType.ViolationsNew && (
            <div className="new-violation-ui">
              <div
                className="top-coln"
                style={{ display: "flex", justifyContent: "space-between" }}
              >
                {Array.isArray(complianceStatistics) &&
                  complianceStatistics.length > 2 && (
                    <Input
                      field={{
                        value: this.state.searchString,
                        label: "",
                        type: "SEARCH",
                      }}
                      width={5}
                      name="searchString"
                      onChange={(e) =>
                        this.setState({ searchString: e.target.value })
                      }
                      placeholder="Search by rule name & level"
                      className="search rules-search"
                    />
                  )}
                {this.state.showComplianceRerunBtn && (
                  <SquareButton
                    onClick={this.onClickReRunCompliance}
                    content={
                      <span>
                        <img alt="" src="/assets/icons/reload.png" />
                        Re-Run Compliance
                      </span>
                    }
                    disabled={this.props.isPostingBatch}
                    bsStyle={"primary"}
                    className={`rule-list-btn btn-img ${
                      this.props.isPostingBatch ? "rotate-img-running" : ""
                    }`}
                  />
                )}
              </div>
              {Array.isArray(complianceStatistics) &&
                complianceStatistics
                  .filter(
                    (x) =>
                      (x.name &&
                        x.name
                          .toLowerCase()
                          .includes(this.state.searchString.toLowerCase())) ||
                      x.level
                        .toLowerCase()
                        .includes(this.state.searchString.toLowerCase())
                  )
                  .sort(
                    (a, b) => Number(b.is_valid_rule) - Number(a.is_valid_rule)
                  )
                  .map((config, index) => {
                    return this.renderBoardContainer(config, index);
                  })}
            </div>
          )}
          {currentPage.pageType === PageType.Bundles && (
            <div className="configuration-details-config-data col-md-12">
              <div className="bundles">
                {configuration &&
                  this.getInventory(configuration.serial_numbers)}
                {}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };
  toggleCollapsedState = (e, boardIndex) => {
    e.stopPropagation();
    this.setState((prevState) => ({
      isCollapsed: [
        ...prevState.isCollapsed.slice(0, boardIndex),
        !prevState.isCollapsed[boardIndex],
        ...prevState.isCollapsed.slice(boardIndex + 1),
      ],
    }));
  };

  renderBoardContainer = (config: any, boardIndex: number) => {
    const isCollapsed = this.state.isCollapsed[boardIndex];

    return (
      <div
        key={boardIndex}
        className={`view-rule-details collapsable-section  ${
          isCollapsed ? "view-rule-details--collapsed" : "field--not-collapsed"
        }`}
      >
        <h2
          onClick={(e) => this.toggleCollapsedState(e, boardIndex)}
          className="view-rule-details-title collapsable-heading"
        >
          {this.getHeadingVoilations(config)}
          <div className="action-collapse">
            <span style={{ paddingLeft: "10px" }}>
              {!isCollapsed ? "+" : "-"}
            </span>
          </div>
        </h2>
        <div
          className={`view-rule-details-contents ${
            !isCollapsed ? "display-none" : "display-unset"
          }`}
        >
          <div className="rule-voilation">
            {config.function && (
              <div className="rule-name">
                <span className="bold-text">Function: </span>
                {config.function}
              </div>
            )}
            {config.multi_section_data && (
              <>
                <div className="heading-config">Rule Details</div>

                <div className="multi-line-diff-view">
                  <div className="diff-view">
                    {config.multi_section_data.map((e, k) =>
                      this.checkFirstCharWithVariable(
                        e,
                        k,
                        "Valid Command",
                        "Invalid Command",
                        config
                      )
                    )}
                  </div>
                </div>
              </>
            )}

            {config.valid_commands &&
              config.valid_commands.length > 0 &&
              config.valid_commands[0].length > 0 && (
                <div className="valid-commands">
                  <div className="heading-valid-config">VALID COMMANDS</div>
                  {config.valid_commands.map((c, i) => {
                    return c.map((d, j) => {
                      return (
                        <div
                          key={j}
                          title={`Line No. - ${d.line_number || "N.A."}`}
                          onClick={() => this.scrolltoLine(d.line_number)}
                          className="command"
                        >
                          {d.command ? d.command : d.detail}
                        </div>
                      );
                    });
                  })}
                </div>
              )}
            {config.invalid_commands &&
              config.invalid_commands[0] &&
              config.invalid_commands[0].length > 0 && (
                <div className="invalid-commands">
                  <div className="heading-invalid-config">INVALID COMMANDS</div>
                  <div className="command-single">
                    <div className="original-config bold-text">Rule Detail</div>
                    <div className="original-config bold-text">
                      Partial Match Found{" "}
                    </div>
                  </div>
                  {config.invalid_commands.map((c, i) => {
                    return c.map((d, j) => {
                      return (
                        <div key={Math.random()} className="command-single">
                          <div
                            className={`original-config ${
                              d.line_number ? "line-numer" : ""
                            }`}
                            title={`${
                              d.line_number
                                ? `line number - ${d.line_number}`
                                : ""
                            }`}
                            onClick={() =>
                              d.line_number && this.scrolltoLine(d.line_number)
                            }
                          >
                            {this.getVaribleDetails(config, d.command)}{" "}
                          </div>
                          <div className="partial_matching_commands">
                            {d.partial_matching_commands.length > 0 ? (
                              d.partial_matching_commands.map((e, k) => {
                                return (
                                  <div
                                    key={k}
                                    className="partial_matching_single"
                                    title={`Line No. - ${
                                      e.line_number || "N.A."
                                    }`}
                                  >
                                    {e.command_diff.map((f, k) =>
                                      this.checkFirstChar(
                                        f,
                                        k,
                                        "suggested value",
                                        "found value"
                                      )
                                    )}
                                    <br></br>
                                    <span className="partial_matching_percent">
                                      {e.match_percentage &&
                                        `Match Percent: ${(
                                          e.match_percentage * 100
                                        ).toFixed(2)}%`}
                                    </span>
                                  </div>
                                );
                              })
                            ) : (
                              <span className="no_partial_matching_percent">
                                No partial matches found
                              </span>
                            )}
                          </div>
                        </div>
                      );
                    });
                  })}
                </div>
              )}
          </div>
        </div>
      </div>
    );
  };
  getVaribleDetails = (config, rule: any) => {
    return (
      <Highlighter
        highlightClassName="YourHighlightClass"
        searchWords={this.getVariables(config.variable_values).map(
          (m) => m.key
        )}
        autoEscape={true}
        textToHighlight={rule}
        key={Math.random()}
        highlightTag={({ children, highlightIndex }) => (
          <strong
            key={Math.random()}
            title={`
        Name :  ${_.get(config, `variable_values[${children}].name`, "")},
code : ${_.get(config, `variable_values[${children}].variable_code`, "")}
Values : ${_.get(
              config,
              `variable_values[${children}].variable_values`,
              []
            ).join(", ")}
Option : ${_.get(config, `variable_values[${children}].option`)}
Type :  ${_.get(config, `variable_values[${children}].type`, "")},
Description : ${_.get(config, `variable_values[${children}].description`, "")}
        `}
            className="highlighted-text-2"
          >
            {children}
          </strong>
        )}
      />
    );
  };
  checkFirstChar = (
    charector: string,
    k: number,
    add: string,
    remove: string
  ) => {
    switch (charector.charAt(0)) {
      case "+":
        return null;
      case "-":
        return (
          <span key={k} className="add-red" title={remove}>
            {charector.slice(1)}
          </span>
        );
      default:
        return <span key={k}>{charector}</span>;
    }
  };

  checkFirstCharWithVariable = (
    charector: any,
    k: number,
    add: string,
    remove: string,
    config
  ) => {
    if (typeof charector === "string") {
      switch (charector.charAt(0)) {
        case "+":
          return (
            <span key={k} className="add-green" title={add}>
              {this.getVaribleDetails(config, charector.slice(1))}
            </span>
          );
        case "-":
          return (
            <span key={k} className="add-red" title={remove}>
              {this.getVaribleDetails(config, charector.slice(1))}
            </span>
          );
        default:
          return (
            <span key={k}>{this.getVaribleDetails(config, charector)}</span>
          );
      }
    } else {
      switch (charector.command.charAt(0)) {
        case "+":
          return (
            <span key={k} className="add-green" title={add}>
              {this.getVaribleDetails(config, charector.command.slice(1))}
            </span>
          );
        case "-":
          return (
            <span
              key={k}
              className={
                charector.type === "partial_match" ? "add-orange" : "add-red"
              }
              title={remove}
            >
              {this.getVaribleDetails(config, charector.command.slice(1))}
            </span>
          );
        default:
          return (
            <span key={k}>
              {this.getVaribleDetails(config, charector.command)}
            </span>
          );
      }
    }
  };

  toggleRulesSelection = (e, config) => {
    e.stopPropagation();

    const isChecked = e.target.checked;
    if (isChecked) {
      this.setState({
        selectedRules: [...this.state.selectedRules, config.id],
        showComplianceRerunBtn: true,
      });
    } else {
      this.setState({
        selectedRules: this.state.selectedRules.filter(
          (el) => el !== config.id
        ),
        showComplianceRerunBtn:
          this.state.selectedRules.filter((el) => el !== config.id).length > 0,
      });
    }
  };

  getHeadingVoilations = (config: any) => {
    return (
      <div className="voilation-rule-heading">
        <div style={{ display: "flex", alignItems: "center" }}>
          <Checkbox
            isChecked={this.state.selectedRules.indexOf(config.id) > -1}
            name="rerun-rule-checkbox"
            className="rerun-rule-checkbox-config-listing"
            onChange={(e) => this.toggleRulesSelection(e, config)}
          />
          <DeleteButton
            type="pdf_preview"
            title="View Rule Details"
            onClick={(e) => this.openRuleDetails(e, config)}
          />
          <div
            className="rule-name"
            title={`${config.name}`}
            onClick={() => {
              window.open(
                `/configuration/rules?is_global=${config.is_global}&id=${config.id}&name=${config.name}`,
                "_blank"
              );
            }}
          >
            <span
              className={`gray-heading ${
                config.is_valid_rule ? "valid-rule" : "invalid-rule"
              }`}
            >
              Rule :
            </span>
            {config.name}
          </div>
          <div className="right-pan" title={`Level : ${config.level}`}>
            <span className="gray-heading">( </span> {config.level}{" "}
            <span className="gray-heading"> )</span>
          </div>
        </div>
        <div>
          <div className="right-most-pan">
            <span>
              {config.updated_on
                ? `Last Updated - ${fromISOStringToDateTimeString(
                    config.updated_on
                  )}`
                : ""}
            </span>
          </div>
        </div>
      </div>
    );
  };

  openRuleDetails = (e, config) => {
    e.stopPropagation();
    this.setState({
      rule: config,
      openViewModalRule: !this.state.openViewModalRule,
    });
  };

  getInventory = (serial_numbers) => {
    const columnsPopUp: any = [
      {
        accessor: "serial_number",
        Header: "Serial Number",
      },
      {
        accessor: "device_type",
        Header: "Device Type",
      },
      {
        accessor: "model_number",
        Header: "Model Number",
      },
    ];
    const message = (
      <Table
        columns={columnsPopUp}
        customTopBar={null}
        rows={serial_numbers || []}
        className={``}
      />
    );

    return serial_numbers && serial_numbers.length > 0 && message ? (
      message
    ) : (
      <div className="no-data">No Inventory data</div>
    );
  };

  toggleViewModalRule = () => {
    this.setState({
      openViewModalRule: !this.state.openViewModalRule,
    });
  };

  toggleEditModalRule = () => {
    this.setState({
      openEditModalRule: !this.state.openEditModalRule,
    });
  };

  getVariables = (values) => {
    const variablesArray = [];
    if (values) {
      Object.keys(values).map((m) => {
        variablesArray.push({ key: m, value: values[m] });
      });
    }

    return variablesArray;
  };

  getCutomerVariablesValues = (children) => {
    const variablesObj = this.props.customerVariablesAll.filter(
      (v) => v.variable_code === children
    )[0];

    const value =
      variablesObj && !_.isEmpty(variablesObj.customer_variables)
        ? variablesObj.customer_variables.variable_values.join(",\n")
        : variablesObj && variablesObj.variable_values.join(",\n");

    return value ? value : "-";
  };

  getTitle = () => {
    return (
      <div className="configuration-details">
        Monitoring Details
        <div className={`status--${this.props.configuration.status}`}>
          {this.props.configuration.status === "Active"
            ? "In Service "
            : "Decommissioned"}
        </div>
      </div>
    );
  };

  getBody = () => {
    return (
      <div className="edit-configuration">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className={`${this.props.isFetching ? `loading` : ""}`}>
          {this.state.configuration && this.renderDeviceDetails()}

          <RuleDetails
            show={this.state.openViewModalRule}
            onClose={this.toggleViewModalRule}
            rule={this.state.rule}
          />
          {this.state.openEditModalRule && (
            <EditRules
              show={this.state.openEditModalRule}
              onClose={this.toggleEditModalRule}
              rule={this.state.rule}
            />
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    const config_last_synced_date = _.get(
      this.props.configuration,
      "config_last_synced_date"
    );
    return (
      <div
        className={`add-configuration__footer
        ${this.props.isFetching ? `loading` : ""}`}
      >
        <div className="updated-on">
          Last Automated Update :{" "}
          {config_last_synced_date
            ? fromISOStringToDateTimeString(config_last_synced_date)
            : "N.A."}
        </div>
      </div>
    );
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.props.configuration && this.getTitle()}
        bodyElement={this.props.configuration && this.getBody()}
        footerElement={this.props.configuration && this.getFooter()}
        className="add-configuration configuration-view"
      />
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  detailConfig: state.configuration.detailConfiguration,
  isFetchingConfigDetail: state.configuration.isFetchingConfigDetail,
  isPostingBatch: state.inventory.isPostingBatch,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchDeviceConfigurationDetail: (id: number, deviceId: number) =>
    dispatch(fetchDeviceConfigurationDetail(id, deviceId)),
  fetchCustomerDeviceConfigurationDetail: (deviceId: number) =>
    dispatch(fetchCustomerDeviceConfigurationDetail(deviceId)),
  rerunConfigCompliance: (data: any) => dispatch(rerunConfigCompliance(data)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ViewMonitoring);
