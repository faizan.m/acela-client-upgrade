import React from 'react';
import { connect } from 'react-redux';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import {
  fetchClassifications,
  fetchConfigurations,
  fetchConfigurationsCustomerUser,
  fetchRulesLevels,
  fetchCustomerVarsAll,
  fetchTaskStatusConfig,
  CONFIG_TASK_STATUS_SUCCESS,
  SYNC_CONFIG_SUCCESS,
  syncConfigCompliance,
  fetchConfigurationSearchResults,
  fetchCustomerConfigurationSearchResults,
  GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS,
  fetchDeviceConfigurationDetail,
  fetchCustomerDeviceConfigurationDetail,
  GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS,
  fetchRules,
  getCustomerRules,
  FETCH_RULES_SUCCESS,
  FETCH_CUS_RULES_SUCCESS
} from '../../../actions/configuration';
import {
  fetchContractStatuses,
  fetchDeviceCategories,
  fetchManufacturers,
  fetchProviderIntegrationsForFilter,
  fetchSites,
  fetchSitesCustomersUser,
  fetchStatuses,
  fetchTypes,
  setXlsxData,
  TASK_STATUS_SUCCESS,
  TASK_STATUS_FAILURE,
  fetchTaskStatus,
} from '../../../actions/inventory';

import { cloneDeep, debounce } from 'lodash';
import {
  FETCH_PROVIDER_CONFIG_STATUS_SUCCESS,
  fetchAllProviderIntegrations,
  fetchProviderConfigStatus,
} from '../../../actions/provider/integration';
import SquareButton from '../../../components/Button/button';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import Checkbox from '../../../components/Checkbox/checkbox';
import ModalBase from '../../../components/ModalBase/modalBase';
import Select from '../../../components/Input/Select/select';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';
import { allowPermission } from '../../../utils/permissions';
import FilterModal from './filterModal';
import ViewRules from './ruleView';
import './style.scss';
import ViewMonitoring from './view';
import { searchInFields } from '../../../utils/searchListUtils';
import _ from 'lodash';
import { REPORT_COMPLIANCE_SUCCESS, RERUN_COMPLIANCE_SUCCESS, rerunConfigCompliance, reportConfigCompliance } from '../../../actions/collector';
import '../../../commonStyles/filtersListing.scss';

interface IConfigurationProps extends ICommonProps {
  fetchConfiguration: TFetchDevices;
  fetchConfigurationCustomerUser: TFetchDevicesCustomerUser;
  fetchManufacturers: TFetchManufacturers;
  fetchSites: TFetchSites;
  fetchSitesCustomersUser: TFetchSitesCustomerUser;
  fetchTypes: TFetchTypes;
  configurations: IConfiguration[];
  manufacturers: any[];
  sites: any[];
  types: any[];
  customerId: number;
  configuration: IConfiguration;
  contractStatuses: any[];
  user: ISuperUser;
  isFetchingConfigList: boolean;
  isFetchingList: boolean;
  isDeviceFetching: boolean;
  addErrorMessage: TShowErrorMessage;
  fetchProviderConfigStatus: any;
  isFetchingSubscription: boolean;
  fetchDeviceCategories: any;
  deviceCategoryList: any;
  fetchProviderIntegrationsForFilter: any;
  providerIntegration: any;
  allProviderIntegrations: IProviderIntegration[];
  fetchAllProviderIntegrations: any;
  updateSitesCU: any;
  fetchContractStatuses: any;
  updateSitesPU: any;
  fetchProductMappings: any;
  productMappings: any;
  setXlsxData: any;
  fetchStatuses: any;
  statusList: any[];
  classifications: any;
  levels: any;
  fetchClassifications: any;
  fetchRulesLevels: any;
  addSuccessMessage: any;
  fetchCustomerVarsAll: any;
  fetchTaskStatus: any;
  rerunConfigCompliance: any;
  reportConfigCompliance: any;
  syncConfigCompliance: any;
  customerVariablesAll: any[];
  isPostingBatch: boolean;
  isReportDownloading: boolean;
  fetchTaskStatusConfig: any
  isTaskPolling: boolean;
  fetchConfigurationSearchResults: any;
  fetchCustomerConfigurationSearchResults: any;
  fetchDeviceConfigurationDetail: any;
  fetchCustomerDeviceConfigurationDetail: any;
  isFetchingConfigDetail: boolean;
  fetchRules: any;
  getCustomerRules: any;
}

interface IConfigurationState {
  rows: IConfiguration[];
  selectedRows: number[];
  errorList?: any;
  isViewDeviceModalOpen: boolean;
  isViewViolation: boolean;
  isFilterModalOpen: boolean;
  customerId?: number;
  configuration?: IConfiguration;
  searchString: string;
  showInactiveDevices: boolean;
  filters: IConfigurationFilters;
  siteLabelIds: { [id: number]: string };
  typesLabelIds: { [id: string]: string };
  statusLabelIds: { [id: number]: string };
  isDevicePosting: boolean;
  isPostingBulkUpdate: boolean;
  deviceCategories: any;
  isBulkUpdateModalOpen: boolean;
  clearAdd: boolean;
  fileReading: boolean;
  isSuccess: boolean;
  ComplianceRuntaskId: any;
  configSyncTaskId: any;
  level: string;
  searchConfig: boolean;
  isSearchingConfig: boolean;
  showComplianceRerunBtn: boolean;
  globalRulesRerun: any[];
  customerRulesRerun: any[];
  showReRunComplianceModal: boolean;
  fetchingGlobalRules: boolean;
  fetchingCustomerRules: boolean;
  globalRules: any[];
  customerRules: any[];
  isRerunForAllRules: boolean;
  rulesSelectSearchValue: string;
}

class MonitoringManagement extends React.Component<
  IConfigurationProps,
  IConfigurationState
  > {

  private debouncedFetchConfigSearch;

  constructor(props: IConfigurationProps) {
    super(props);

    this.state = {
      rows: [],
      selectedRows: [],
      isViewDeviceModalOpen: false,
      isViewViolation: false,
      isBulkUpdateModalOpen: false,
      isFilterModalOpen: false,
      searchString: '',
      showInactiveDevices: false,
      deviceCategories: null,
      filters: {
        site: [],
        status: [],
        type: [],
        level: [],
        classification: [],
      },
      typesLabelIds: {},
      statusLabelIds: {},
      siteLabelIds: {},
      isDevicePosting: false,
      isPostingBulkUpdate: false,
      clearAdd: false,
      fileReading: false,
      isSuccess: false,
      level: '',
      ComplianceRuntaskId: null,
      configSyncTaskId: null,
      searchConfig: false,
      isSearchingConfig: false,
      showComplianceRerunBtn: false,
      fetchingGlobalRules: false,
      fetchingCustomerRules: false,
      globalRules: [],
      customerRules: [],
      globalRulesRerun: [],
      customerRulesRerun: [],
      showReRunComplianceModal: false,
      isRerunForAllRules: true,
      rulesSelectSearchValue: ""
    };

    this.debouncedFetchConfigSearch = debounce(this.fetchConfigurationSearchResults, 750);
  }

  componentDidMount() {
    this.props.fetchProviderConfigStatus().then(action => {
      if (
        action.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS &&
        action.response &&
        action.response.crm_authentication_configured
      ) {
        this.props.fetchManufacturers();
        this.props.fetchDeviceCategories();
        this.props.fetchContractStatuses();
      }
    });
    this.props.fetchTypes();
    if (this.props.statusList.length === 0) {
      this.props.fetchStatuses();
    }
    this.fetchConfiguration();
    this.props.fetchCustomerVarsAll(this.props.customerId);

    if (_.get(this.props, 'user.type') === 'provider' && this.props.customerId) {
      this.props.fetchSites(this.props.customerId);
    }

    if (_.get(this.props, 'user.type') === 'customer') {
      this.props.fetchSitesCustomersUser();
    }

    if (!this.props.allProviderIntegrations) {
      this.props.fetchAllProviderIntegrations();
    }
    if (this.props.statusList) {
      this.setLabelsStatus(this.props);
    }
    if (this.props.sites) {
      this.setLabelsSites(this.props);
    }
    if (this.props.types) {
      this.setLabelsType(this.props);
    }
    if (this.props.levels.length === 0) {
      this.props.fetchRulesLevels();
    }
    if (this.props.classifications.length === 0) {
      this.props.fetchClassifications(this.props.user && this.props.user.type);
    }

    this.setState({ fetchingGlobalRules: false });
    this.setState({ fetchingCustomerRules: false });

    const params = {pagination: false};
    this.props.fetchRules(params).then(
      action => {
        if (action.type === FETCH_RULES_SUCCESS) {
          const globalRules = action.response.map(data => ({
            value: data.id,
            label: (
              <div data-name={data.name} style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <span>{data.name}</span>
                <img
                  src={`/assets/icons/global-green.svg`}
                  alt=""
                  height="20"
                  width="20"
                  title={`Global Rule`}
                  className="info-svg-images"
                />    
              </div>
            ),
          }))
          this.setState({ globalRules })
        }
        this.setState({ fetchingGlobalRules: false });
      }
    );

    this.props.getCustomerRules(this.props.customerId, params).then(
      action => {
        if (action.type === FETCH_CUS_RULES_SUCCESS) {
          const customerRules = action.response.map(data => ({
            value: data.id,
            label: (
              <div data-name={data.name} style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                <span>{data.name}</span>
                <img
                  src={`/assets/icons/global-grey.svg`}
                  alt=""
                  height="20"
                  width="20"
                  title={`Customer Rule`}
                  className="info-svg-images"
                />    
              </div>
            ),
          }))
          this.setState({ globalRules: [...this.state.globalRules, ...customerRules] })
        }
        this.setState({ fetchingCustomerRules: false });
      }
    );
  }

  componentDidUpdate(prevProps: IConfigurationProps) {
    const {
      configurations,
      customerId,
      configuration,
      statusList,
      sites,
      types,
      user,
      allProviderIntegrations,
    } = this.props;

    if (configurations !== prevProps.configurations) {
      const rows = this.getRows(this.props, "", this.state.filters);
      this.setState({
        rows,
      });
    }

    if (customerId && customerId !== prevProps.customerId) {
      this.props.fetchConfiguration(customerId, this.state.showInactiveDevices);
      this.props.fetchSites(customerId);
      this.setState({
        filters: {
          site: [],
          status: [],
          type: [],
          level: [],
          classification: [],
        },
      });
      this.props.fetchCustomerVarsAll(customerId);

      this.setState({ fetchingCustomerRules: false });
      this.props
        .getCustomerRules(customerId, { pagination: false })
        .then((action) => {
          if (action.type === FETCH_CUS_RULES_SUCCESS) {
            const customerRules = action.response.map((data) => ({
              value: data.id,
              label: `${data.name}`,
            }));
            this.setState({ customerRules });
          }
          this.setState({ fetchingCustomerRules: false });
        });
    }

    if (configuration !== prevProps.configuration) {
      this.setState({ configuration });
    }

    if (statusList !== prevProps.statusList) {
      this.setLabelsStatus(this.props);
    }

    if (sites !== prevProps.sites) {
      this.setLabelsSites(this.props);
    }

    if (types !== prevProps.types) {
      this.setLabelsType(this.props);
    }

    if (_.get(user, "type") === "customer" && user !== prevProps.user) {
      this.props.fetchConfigurationCustomerUser(this.state.showInactiveDevices);
      this.props.fetchSitesCustomersUser();
      this.props.fetchCustomerVarsAll(customerId);
    }

    if (
      allProviderIntegrations !== prevProps.allProviderIntegrations &&
      allProviderIntegrations
    ) {
      // This will be called on reload of page
      // and when the crm_authentication_configured
      // is true.
      // TODO: this is not dynamic
      const provider = allProviderIntegrations.filter(
        (int) => int.type === "CONNECTWISE"
      );

      if (provider.length > 0) {
        this.props.fetchProviderIntegrationsForFilter(provider[0].id);
      }
    }
  }

  setLabelsStatus = props => {
    const statusLabelIds = props.statusList.reduce(
      (statusIds, contractStatus) => {
        statusIds[contractStatus.id] = `Status: ${contractStatus.description}`;

        return statusIds;
      },
      {}
    ); // tslint:disable-line

    this.setState({
      statusLabelIds,
    });
  };

  setLabelsType = props => {
    if (props.types !== this.props.types) {
      const typesLabelIds = props.types.reduce((labelIds, type) => {
        labelIds[type] = type;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        typesLabelIds,
      });
    }
  };

  setLabelsSites = props => {
    if (props.sites !== this.props.sites) {
      const siteLabelIds = props.sites.reduce((labelIds, site) => {
        labelIds[site.site_id] = site.name;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        siteLabelIds,
      });
    }
  };

  getRows = (
    nextProps: IConfigurationProps,
    searchString?: string,
    filters?: IConfigurationFilters
  ) => {
    let configurations = nextProps.configurations;
    const search = searchString ? searchString : this.state.searchString;

    if (filters) {
      // Add filter for type once included.
      const filterSites = filters.site;
      const filterStatus = filters.status;
      const filterLevel = filters.level;
      const filterTypes =
        filters.type &&
        filters.type.map(item => {
          return item === 'N.A.' ? '' : item;
        });
      configurations =
        configurations &&
        configurations.filter(config => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
          const hasFilteredSite =
            filterSites.length > 0
              ? filterSites.includes(config.site_id)
              : true;
          const hasFilteredStatus =
            filterStatus.length > 0
              ? filterStatus.includes(config.status_id)
              : true;
          const hasFilteredType =
            filterTypes.length > 0
              ? filterTypes.includes(config.device_type)
              : true;

          const hasFilteredLevel =
            filterLevel.length > 0
              ? filterLevel.some(v =>
                config.violations_count_by_levels[v] > 0
              )
              : true;

          return (
            hasFilteredSite &&
            hasFilteredStatus &&
            hasFilteredType &&
            hasFilteredLevel
          );
        });
    }
    if (search && search.length > 0) {

      if (!this.state.searchConfig) {
        configurations =
          configurations &&
          configurations.filter(
            row =>
              searchInFields(row, search, ['name',
                'device_name',
                'name',
                'device_type',
                'status',
                'serial_number',
                'category_name',
                'manufacturer_name',
                'model_number',
                'service_contract_number',
                'monitoring_data.host_name',
              ])
          );
      } else {
        this.debouncedFetchConfigSearch(search);
      }
    }

    const rows: any =
      configurations &&
      configurations.map((config, index) => ({
        ...config,
        site_id: config.site_id,
        site: this.getSiteName(config.site_id),
        model: config.model_number,
        config: _.get(config, 'configuration_data.config_data.config', '-'),
        host_name: _.get(config, 'monitoring_data.host_name', '-'),
        stack_count: _.get(config, 'stack_info.count', '-'),
        serial_numbers: _.get(config, 'stack_info.bundled_devices_data', []),
        config_last_updated_on:
        _.get(config, 'config_last_updated_on', '-'),
        config_last_synced_date:
          _.get(config, 'config_last_synced_date', '-'),
        compliance_last_calculated: _.get(config, 'compliance_last_calculated'),
        index,
      }));

    return rows;
  };

  getSiteName = (id: number) => {
    const site =
      this.props.sites && this.props.sites.filter(s => s.site_id === id);

    return site && site[0] ? site[0].name : '';
  };
  onRowClick = rowInfo => {
    this.setState(() => ({
      isViewDeviceModalOpen: true,
      configuration: this.state.rows[rowInfo.index],
    }));

    this.fetchDeviceConfigDetail(rowInfo.original.id, rowInfo)
  };

  toggleViewDeviceModal = () => {
    this.setState(prevState => ({
      isViewDeviceModalOpen: !prevState.isViewDeviceModalOpen,
    }));
  };
  toggleViewViolation = () => {
    this.setState(prevState => ({
      isViewViolation: !prevState.isViewViolation,
    }));
  };

  toggleFilterModal = () => {
    this.setState(prevState => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onRowsToggle = selectedRows => {
    this.setState({
      selectedRows,
      showComplianceRerunBtn: selectedRows.length > 0
    });
  };

  onFiltersUpdate = (filters: IConfigurationFilters) => {
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState({
      rows,
      filters,
      isFilterModalOpen: false,
    });
  };

  handleChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };

  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters
    );

    this.setState({ rows });
  };

  levelList = () => {
    const levelList = this.props.levels
      ? this.props.levels.map(c => ({
        value: c,
        label: c,
        disabled: false,
      }))
      : [];

    return levelList;
  };

  siteList = () => {
    const sites = this.props.sites
      ? this.props.sites.map(c => ({
        value: c.site_id,
        label: c.name,
        disabled: false,
      }))
      : [];

    return sites;
  };

  typeList = () => {
    const types = this.props.types
      ? this.props.types.map(c => ({
        value: c,
        label: c ? c : 'N.A.',
        disabled: false,
      }))
      : [];

    return types;
  };

  classificationList = () => {
    const classification =
      this.props.classifications.length > 0
        ? this.props.classifications.map(c => ({
          value: c.id,
          label: c.name,
          disabled: false,
        }))
        : [];

    return classification;
  };
  statusList = () => {
    const statusList = this.props.statusList
      ? this.props.statusList.map(s => ({
        value: s.id,
        label: s.description,
      }))
      : [];

    return statusList;
  };
  // Render methods

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters = filters
      ? (filters.site && filters.site.length > 0) ||
      (filters.type && filters.type.length > 0) ||
      (filters.classification && filters.classification.length > 0) ||
      (filters.level && filters.level.length > 0) ||
      (filters.status && filters.status.length > 0)
      : false;

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.site.length > 0 && (
          <div className="section-show-filters">
            <label>Site: </label>
            <SelectInput
              name="site"
              value={filters.site}
              onChange={this.onFilterChange}
              options={this.siteList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.type.length > 0 && (
          <div className="section-show-filters">
            <label>Type: </label>
            <SelectInput
              name="type"
              value={filters.type}
              onChange={this.onFilterChange}
              options={this.typeList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.status.length > 0 && (
          <div className="section-show-filters">
            <label>Status: </label>
            <SelectInput
              name="status"
              value={filters.status}
              onChange={this.onFilterChange}
              options={this.statusList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.level.length > 0 && (
          <div className="section-show-filters">
            <label>Level: </label>
            <SelectInput
              name="level"
              value={filters.level}
              onChange={this.onFilterChange}
              options={this.levelList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.classification.length > 0 && (
          <div className="section-show-filters">
            <label>Classification: </label>
            <SelectInput
              name="classification"
              value={filters.classification}
              onChange={this.onFilterChange}
              options={this.classificationList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    const newState = cloneDeep(this.state);
    newState.filters[targetName] = targetValue;
    this.setState(newState);
    this.onFiltersUpdate(newState.filters);
  };

  onRerunRuleChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    if (targetName === "globalRules") {
      this.setState({
        globalRulesRerun: targetValue
      })
    }

    if (targetName === "customerRules") {
      this.setState({
        customerRulesRerun: targetValue
      })
    }
  };

  onSearchInputChange = e => {
    this.setState({ rulesSelectSearchValue: e});
  };

  getCount = (array: any, level) => {
    const statistics = array;

    const count =
    statistics &&  statistics.length > 0 && statistics.filter(stats => stats.level === level);

    return count ? count.length : 0;
  };

  toggleSearchQuery = (event: any) => {
    const targetValue = event.target.checked;
    this.setState({
      searchConfig: targetValue,
    }, () => {
      this.handleRows();
    });
  };

  renderTopBar = () => {
    return (
      <div
        className={
          this.props.customerId ||
            (this.props.user &&
              this.props.user.type &&
              this.props.user.type === 'customer')
            ? 'configuration-management__table-top '
            : 'configuration-management__table-top '
        }
      >
        <div className="configuration-management__table-actions header-panel">
          <div
            className={
              this.props.customerId ||
                (this.props.user &&
                  this.props.user.type &&
                  this.props.user.type === 'customer')
                ? 'configuration-management__table-header-search'
                : 'configuration-management__table-header-search disable-div'
            }
          >
            {allowPermission('search_device') && (
              <Input
                field={{
                  label: '',
                  type: "SEARCH",
                  value: this.state.searchString,
                  isRequired: false,
                }}
                width={12}
                placeholder="Search"
                name="searchString"
                onChange={this.handleChange}
                className="configuration-management__search search"
              />
            )}
            {/* <Checkbox
              isChecked={this.state.showInactiveDevices}
              name="option"
              onChange={e => this.onChecboxChanged(e)}
            >
              Show Canceled
            </Checkbox> */}

            {/* <div className="searchbox_container"> */}
              <Checkbox
                isChecked={this.state.searchConfig}
                name="configSearch"
                className="searchbox_container"
                onChange={e => this.toggleSearchQuery(e)}
              >
                Search in configuration
              </Checkbox>
            {/* </div> */}
              {/* <SquareButton
                onClick={}
                content={query.is_enabled ? 'Disable' : 'Enable'}
                bsStyle={query.is_enabled ? "default" : "info"}
                className="save-mapping-collector"
              /> */}
            {/* </div> */}
          </div>
          <div className="btn-group-left">
            <SquareButton
              onClick={this.fetchConfiguration}
              content={'Reload'}
              disabled={
                !this.props.configurations ||
                (this.props.configurations &&
                  this.props.configurations.length === 0)
              }
              bsStyle={"primary"}
              className={`btn-img ${this.props.isPostingBatch ? 'rotate-img-running' : ''}`}
            />
            {!this.state.showComplianceRerunBtn && (
              <SquareButton
                onClick={this.onClickReportDownload}
                content={
                  <span>
                    <img alt="" src="/assets/icons/pdf-file-format-symbol.svg" />
                    {` ${this.props.isReportDownloading ? 'Downloading' : 'Report'}`}
                  </span>
                }
                bsStyle={"primary"}
                className={`btn-img`}
              />
            )}

            {this.state.showComplianceRerunBtn && (
              <SquareButton
                // onClick={this.onClickReRunCompliance}
                onClick={() => this.setState({ showReRunComplianceModal: !this.state.showReRunComplianceModal})}
                content={
                  <span>
                    <img alt="" src="/assets/icons/reload.png" />
                      Re-Run Compliance
                  </span>
                }
                disabled={this.props.isPostingBatch}
                bsStyle={"primary"}
                className={`btn-img ${this.props.isPostingBatch ? 'rotate-img-running' : ''}`}
              />
            )}
            {!this.state.showComplianceRerunBtn && (
              <SquareButton
                onClick={this.onClickSyncConfig}
                content={
                  <span>
                    <img alt="" src="/assets/icons/reload.png" />
                    Sync Config
                  </span>
                }
                disabled={this.props.isTaskPolling}
                bsStyle={"primary"}
                className={`btn-img ${this.props.isTaskPolling ? 'rotate-img-running' : ''}`}
              />
            )}
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                !this.props.configurations ||
                (this.props.configurations &&
                  this.props.configurations.length === 0)
              }
              bsStyle={"primary"}
              className="btn-img"
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  getLevelIconsWithBadge = (violation_count, levels, status) => {
    return (
      <div
        className="image_with_badge_container"
        title={`HIGH: ${levels.HIGH}
MEDIUM: ${levels.MEDIUM}
LOW: ${levels.LOW}`
        }
      >
        <img
          className={`${
            violation_count > 0 ? `status-svg-images` : `status-svg-images grade-out`
            }`}
          src={`/assets/icons/${status}.svg`}
        />
        {violation_count > 0 && (
          <span className="badge badge-on-image">{violation_count}</span>
        )}
      </div>
    );
  };

  getIconsWithBadgeStack = (serial_numbers, count) => {

    return (
      <div className="image_with_badge_container" title={`Serial Numbers: ${serial_numbers && serial_numbers.length > 0 ? serial_numbers.map(s => { return s.serial_number }).join(', ') : 'N.A.'}`}>
        <img
          className={`${
            count > 0 ? `status-svg-images` : `status-svg-images grade-out`
            }`}
          src={`/assets/icons/stack.svg`}
        />
        {count > 0 ? (
          <span className="badge badge-on-image">{count}</span>
        ) : null}
      </div>
    );
  };
  fetchConfiguration = (showInactiveDevices: any = this.state.showInactiveDevices) => {
    if (_.get(this.props, 'user.type') === 'customer') {
      this.props.fetchConfigurationCustomerUser(showInactiveDevices);
    }
    if (_.get(this.props, 'user.type') === 'provider' && this.props.customerId) {
      this.props.fetchConfiguration(
        this.props.customerId,
        showInactiveDevices
      );
    }
  }

  fetchConfigurationSearchResults = (searchString: string) => {
    this.setState({isSearchingConfig: true});

    const deviceIds = this.state.rows.map(d => d.id);
    if (_.get(this.props, 'user.type') === 'customer') {
      this.props.fetchCustomerConfigurationSearchResults(searchString, deviceIds)
      .then((res) => {
        if (res.type === GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS) {
          this.setSearchResults(res.response);
        }
      });
    }
    if (_.get(this.props, 'user.type') === 'provider' && this.props.customerId) {
      this.props.fetchConfigurationSearchResults(
        this.props.customerId,
        searchString,
        deviceIds
      ).then((res) => {
        if (res.type === GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS) {
          this.setSearchResults(res.response);
        }
      });
    }
  }

  fetchDeviceConfigDetail = (deviceId: number, rowInfo: any) => {
    if (_.get(this.props, 'user.type') === 'customer') {
      this.props.fetchCustomerDeviceConfigurationDetail(deviceId)
      .then((res) => {
        if (res.type === GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS) {
          const configurationDetails =  {
            ...this.state.rows[rowInfo.index],
            ...res.response.config_data,
            ...res.response
          };

          this.setState(() => ({
            configuration: {
              ...this.state.rows[rowInfo.index],
              ...configurationDetails
            },
          }));
        }
      });
    }
    if (_.get(this.props, 'user.type') === 'provider' && this.props.customerId) {
      this.props.fetchDeviceConfigurationDetail(this.props.customerId, deviceId)
      .then((res) => {
        if (res.type === GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS) {
          const configurationDetails =  {
            ...this.state.rows[rowInfo.index],
            ...res.response.config_data,
            ...res.response
          };

          this.setState(() => ({
            configuration: {
              ...this.state.rows[rowInfo.index],
              ...configurationDetails
            },
          }));
        }
      });
    }
  }

  setSearchResults = (device_ids: any[]) => {
    let configurations = this.props.configurations;

    configurations =
      configurations &&
      configurations.filter(
        row => device_ids.indexOf(row.id) > -1
      )
    
    const rows: any =
      configurations &&
      configurations.map((config, index) => ({
        ...config,
        site_id: config.site_id,
        site: this.getSiteName(config.site_id),
        model: config.model_number,
        config: _.get(config, 'configuration_data.config_data.config', '-'),
        host_name: _.get(config, 'monitoring_data.host_name', '-'),
        stack_count: _.get(config, 'stack_info.count', '-'),
        serial_numbers: _.get(config, 'stack_info.bundled_devices_data', []),
        config_last_updated_on:
          _.get(config, 'config_last_updated_on', '-'),
        config_last_synced_date:
          _.get(config, 'config_last_synced_date', '-'),
        compliance_last_calculated: _.get(config, 'compliance_last_calculated'),
        index,
      }));
    
    this.setState({ rows, isSearchingConfig: false });
  }

  onClickReportDownload = () => {
    this.props
      .reportConfigCompliance(this.props.customerId)
      .then(a => {
        if (a.type === REPORT_COMPLIANCE_SUCCESS) {
          if (a.response && a.response.file_path) {
            const url = a.response.file_path;
            const link = document.createElement('a');
            link.href = url;
            link.target = '_blank';
            link.setAttribute('download', a.response.file_name);
            document.body.appendChild(link);
            link.click();
          } else {
            this.props.addErrorMessage('No data found')
          }

        }
      });
  };

  onClickReRunCompliance = (event: any, index?: any) => {
    const { globalRulesRerun, customerRulesRerun, selectedRows } = this.state;
    const rule_ids = [];

    globalRulesRerun.map((d) => rule_ids.push(d.id))
    customerRulesRerun.map((d) => rule_ids.push(d.id))

    const data = {
      customer_id: this.props.customerId,
      devices_ids: selectedRows,
      rule_ids: [ ...globalRulesRerun, ...customerRulesRerun ]
    };

    this.props
      .rerunConfigCompliance(data)
      .then(a => {
        if (a.type === RERUN_COMPLIANCE_SUCCESS) {
          if (a.response && a.response.task_id) {
            this.setState({
              ComplianceRuntaskId: a.response.task_id,
              selectedRows: [],
              globalRulesRerun: [],
              customerRulesRerun: [],
            });
            this.fetchTaskStatus(a.response.task_id, this.fetchConfiguration, 'Re-run compliance ', 'ComplianceRuntaskId');
          }
        }
      });

    this.setState(prevState => ({
      showReRunComplianceModal: false
    }));
  };

  onClickSyncConfig = (event: any, index?: any) => {
    this.props
      .syncConfigCompliance(this.props.customerId)
      .then(a => {
        if (a.type === SYNC_CONFIG_SUCCESS) {
          if (a.response && a.response.task_id) {
            this.setState({
              configSyncTaskId: a.response.task_id,
            });
            this.fetchTaskStatusSyncConfig(a.response.task_id, this.fetchConfiguration, 'Sync Config ', 'configSyncTaskId');
          }
        }
      });
  };

  fetchTaskStatusSyncConfig = (taskId, list, message: string, idInState) => {
    if (taskId) {
      this.props.fetchTaskStatusConfig(taskId).then(a => {
        if (a.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            list();
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatusSyncConfig(taskId, list, message, idInState);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.fetchConfiguration();
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  fetchTaskStatus = (taskId, list, message: string, idInState) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then(a => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'SUCCESS'
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            list();
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'FAILURE'
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ isSuccess: true, [idInState]: '' });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === 'PENDING'
          ) {
            setTimeout(() => {
              this.fetchTaskStatus(taskId, list, message, idInState);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.fetchConfiguration();
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.closeReRunCompliance}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onClickReRunCompliance}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  closeReRunCompliance = () => {
    this.setState(prevState => ({
      showReRunComplianceModal: !prevState.showReRunComplianceModal,
      isRerunForAllRules: true,
      globalRulesRerun: [],
      customerRulesRerun: []
    }))
  };

  toggleRulesRerun = (e) => {
    const targetValue = e.target.checked;
    this.setState({ isRerunForAllRules: targetValue });
  };

  getReRunComplianceBody = () => {
    const { globalRules, rulesSelectSearchValue } = this.state;
    let filteredGlobalRules = globalRules;

    if (rulesSelectSearchValue !== "") {
      filteredGlobalRules = filteredGlobalRules.filter((rule) =>
        rule.label.props['data-name'].toLowerCase().includes(rulesSelectSearchValue.toLocaleLowerCase())
      )
    }

    return (
      <div className="modal-body-container">
        <Checkbox
          isChecked={this.state.isRerunForAllRules}
          name="rerun-rule-checkbox"
          className="rerun-rule-checkbox"
          onChange={e => this.toggleRulesRerun(e)}
        >
          Re Run for all rules
        </Checkbox>
        <div className="filters-modal__body col-md-12">
          {' '}
          <div className="field-section col-md-6">
            <div className="field__label">
              <label className="field__label-label">Rules</label>
              <div className="field__input">
                <Select
                  name="globalRules"
                  value={this.state.globalRulesRerun}
                  onChange={this.onRerunRuleChange}
                  options={filteredGlobalRules}
                  multi={true}
                  placeholder="Select global rule."
                  disabled={this.state.isRerunForAllRules}
                  onInputChange={this.onSearchInputChange}
                  enableInternalFiltering={false}
                />
              </div>
            </div>
          </div>
        </div>
        <div className="info-bottom">
          <img
            style={{ marginRight: "6px" }}
            width="18px"
            height="18px"
            src={`/assets/icons/caution.svg`}
            className="status-svg-images"
            alt=""
            title={""}
          />
          By default compliance will rerun for all the rules. You can select rules by unchecking the above checkbox.
        </div>
      </div>
    );
  };

  render() {
    const columns: any = [
     {
        accessor: 'status',
        Header: 'Violations',
        sortable: false,
        width: 80,
        Cell: data => (
          <div className={`status-icon-rules`}>
            {
              data.original.violations_count > 0 &&
              this.getLevelIconsWithBadge(
                data.original.violations_count,
                data.original.violations_count_by_levels,
                'HIGH'
              )
            }
            {
              data.original.violations_count === 0 &&
              data.original.valid_count > 0 && (
                <img
                  src={`/assets/icons/checked-box.svg`}
                  alt=""
                  title={'Valid Compliance'}
                  className="status-svg-images"
                />
              )
            }
            {
              data.original.violations_count === 0 &&
              data.original.valid_count === 0 && (
                <img
                  src={`/assets/icons/question.svg`}
                  alt=""
                  title={'compliance not running'}
                  className="status-svg-images"
                />
              )
            }
          </div>
        ),
      },
      {
        accessor: 'status',
        Header: 'Inventory',
        sortable: false,
        width: 80,
        Cell: status => (
          <div className={`status-icon-rules`}>
            {
              status.original.serial_numbers && status.original.serial_numbers.length > 0 &&
              this.getIconsWithBadgeStack(
                status.original.serial_numbers,
                status.original.stack_count,
              )}

          </div>
        ),
      },
      {
        accessor: 'device_name',
        Header: 'Device Name',
        Cell: cell => <a> {`${cell.value ? cell.value : 'Unknown'}`}</a>,
      },
      {
        accessor: 'host_name',
        Header: 'IP',
        Cell: s => (
          <a>{s.original.host_name ? s.original.host_name : ''}</a>
        ),
      },
      {
        accessor: 'model_number',
        Header: 'Model',
        Cell: cell => <div> {`${cell.value ? cell.value : '-'}`}</div>,
      },
      {
        accessor: 'device_type',
        Header: 'Type',
        width: 100,
        Cell: cell => <div> {`${cell.value ? cell.value : '-'}`}</div>,
      },
      {
        accessor: 'config_last_updated_on',
        Header: 'Config date',
        width: 130,
        Cell: s => (
          <div>
            {' '}
            {s.original.config_last_updated_on
              ? fromISOStringToFormattedDate(s.original.config_last_updated_on)
              : ''}
          </div>
        ),
      },
      {
        accessor: 'compliance_last_calculated',
        Header: 'Last Calculated On',
        width: 150,
        Cell: s => (
          <div>
            {' '}
            {s.original.compliance_last_calculated
              ? fromISOStringToFormattedDate(s.original.compliance_last_calculated, 'MM/DD/YYYY h:mm A')
              : ''}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };

    return (
      <div className="configuration-management subscription-manage">
        <h3>Configuration Compliance</h3>
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingSubscription ||
              this.props.isFetchingList ||
              this.state.fileReading
            }
          />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows || []}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`device-listing__table ${
            this.props.isFetchingSubscription ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          loading={this.props.isFetchingConfigList || this.state.isSearchingConfig}
          pageSize={25}
        />
        <ViewMonitoring
          {...this.props}
          show={this.state.isViewDeviceModalOpen}
          onClose={this.toggleViewDeviceModal}
          manufacturers={this.props.manufacturers}
          configuration={this.state.configuration}
          isFetching={this.props.isDeviceFetching || this.props.isFetchingConfigDetail}
          userType={this.props.user && this.props.user.type}
          addSuccessMessage={this.props.addSuccessMessage}
          addErrorMessage={this.props.addErrorMessage}
          customerVariablesAll={this.props.customerVariablesAll}
        />
        <ViewRules
          {...this.props}
          show={this.state.isViewViolation}
          onClose={this.toggleViewViolation}
          configuration={this.state.configuration}
          isFetching={this.props.isDeviceFetching}
        />
        <FilterModal
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          sites={this.props.sites}
          types={this.props.types}
          statusList={this.props.statusList}
          prevFilters={this.state.filters}
          levels={this.props.levels}
          classifications={this.props.classifications}
        />
        <ModalBase
          show={this.state.showReRunComplianceModal}
          onClose={this.closeReRunCompliance}
          titleElement={'Select Rules'}
          bodyElement={this.getReRunComplianceBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  configurations: state.configuration.configurations,
  manufacturers: state.inventory.manufacturers,
  sites: state.inventory.sites,
  types: state.inventory.types,
  contractStatuses: state.inventory.contractStatuses,
  customerId: state.customer.customerId,
  configuration: state.configuration.configuration,
  user: state.profile.user,
  isFetchingConfigList: state.configuration.isFetchingConfigList,
  isFetchingList: state.configuration.isFetchingList,
  isFetchingSubscription: state.subscription.isFetchingSubscription,
  isDeviceFetching: state.configuration.isFetching,
  deviceCategoryList: state.inventory.deviceCategoryList,
  providerIntegration: state.inventory.providerIntegration,
  allProviderIntegrations: state.providerIntegration.allProviderIntegrations,
  productMappings: state.subscription.productMappings,
  statusList: state.inventory.statusList,
  classifications: state.configuration.classifications,
  levels: state.configuration.levels,
  customerVariablesAll: state.configuration.customerVariablesAll,
  isPostingBatch: state.inventory.isPostingBatch,
  isTaskPolling: state.configuration.isTaskPolling,
  isReportDownloading: state.collector.isReportDownloading,
  isFetchingConfigDetail: state.configuration.isFetchingConfigDetail
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchDeviceCategories: () => dispatch(fetchDeviceCategories()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchContractStatuses: () => dispatch(fetchContractStatuses()),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  fetchSitesCustomersUser: () => dispatch(fetchSitesCustomersUser()),
  fetchTypes: () => dispatch(fetchTypes()),
  fetchConfigurationCustomerUser: (show: boolean) =>
    dispatch(fetchConfigurationsCustomerUser(show)),
  fetchConfiguration: (id: number, show: boolean) =>
    dispatch(fetchConfigurations(id, show)),
  fetchConfigurationSearchResults: (id: number, searchString: string, deviceIds: number[]) =>
    dispatch(fetchConfigurationSearchResults(id, searchString, deviceIds)),
  fetchCustomerConfigurationSearchResults: (searchString: string, deviceIds: number[]) =>
    dispatch(fetchCustomerConfigurationSearchResults(searchString, deviceIds)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
  fetchProviderIntegrationsForFilter: (id: number) =>
    dispatch(fetchProviderIntegrationsForFilter(id)),
  fetchAllProviderIntegrations: () => dispatch(fetchAllProviderIntegrations()),
  setXlsxData: (data: any, file: any) => dispatch(setXlsxData(data, file)),
  fetchStatuses: () => dispatch(fetchStatuses()),
  fetchClassifications: () => dispatch(fetchClassifications()),
  fetchRulesLevels: () => dispatch(fetchRulesLevels()),
  rerunConfigCompliance: (data: any) => dispatch(rerunConfigCompliance(data)),
  reportConfigCompliance: (customerId: number) => dispatch(reportConfigCompliance(customerId)),
  syncConfigCompliance: (customerId: number) => dispatch(syncConfigCompliance(customerId)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  fetchTaskStatusConfig: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  fetchCustomerVarsAll: (custId: any, params?: IServerPaginationParams) =>
    dispatch(fetchCustomerVarsAll(custId, params)),
  fetchDeviceConfigurationDetail: (id: number, deviceId: number) =>
    dispatch(fetchDeviceConfigurationDetail(id, deviceId)),
  fetchCustomerDeviceConfigurationDetail: (deviceId: number) =>
    dispatch(fetchCustomerDeviceConfigurationDetail(deviceId)),
  fetchRules: (params?: ISPRulesFilterParams) => dispatch(fetchRules(params)),
  getCustomerRules: (id: any, params?: ISPRulesFilterParams) =>
    dispatch(getCustomerRules(id, params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MonitoringManagement);
