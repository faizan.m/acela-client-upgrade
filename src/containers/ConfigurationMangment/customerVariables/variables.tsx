import React from 'react';
import { connect } from 'react-redux';

import EditButton from '../../../components/Button/editButton';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';

import { cloneDeep, debounce } from 'lodash';
// import TagsInput from 'react-tagsinput'; (New Component)
import {
  checkDeleteCustomerVar,
  deleteCustomerVar,
  editCustomerVar,
  FETCH_C_VAR_SUCCESS,
  fetchCustomerVars,
  fetchSingleCustomerVar,
  fetchVariableTypes,
  saveCustomerVar,
} from '../../../actions/configuration';
import SquareButton from '../../../components/Button/button';
import DeleteButton from '../../../components/Button/deleteButton';
import ConfirmBox from '../../../components/ConfirmBox/ConfirmBox';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import AppValidators from '../../../utils/validator';
import VariableFilter from './filter';
import './style.scss';
import ViewVariable from '../VariablesContainer/viewVariable';
import _ from 'lodash';
import ViewVariableHistory from '../VariablesContainer/variableHistory';
import IconButton from '../../../components/Button/iconButton';
import ViewVariableCDHistory from '../VariablesContainer/variableCDHistory';
import { commonFunctions } from '../../../utils/commonFunctions';
import '../../../commonStyles/filtersListing.scss';

interface ICustomerVarProps {
  fetchCustomerVars: any;
  fetchVariableTypes: any;
  fetchSingleCustomerVar: any;
  variables: any;
  isFetching: any;
  variable: IVariable;
  types: any;
  isFetchingList: any;
  saveCustomerVar: any;
  editCustomerVar: any;
  customers: ICustomerShort[];
  deleteCustomerVar: any;
  isFetchingTypes: any;
  customerId: any;
  checkDeleteCustomerVar: any;
  user: any;
  customerProfile: Icustomer;
}

interface ICustomerVarState {
  variable: IVariable;
  open: boolean;
  error: {
    name: IFieldValidation;
    description: IFieldValidation;
    variable_code: IFieldValidation;
    type: IFieldValidation;
    option: IFieldValidation;
    variable_values: IFieldValidation;
  };
  isopenConfirm: boolean;
  variables: IVariable[];
  rows: IVariable[];
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  filters: IVariableFilter;
  id?: any;
  isFilterModalOpen: boolean;
  warningMessage: any;
  openViewModal: boolean;
  viewhistory: boolean;
  viewCDhistory: boolean;
}

class CustomerVarList extends React.Component<
  ICustomerVarProps,
  ICustomerVarState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  private debouncedFetch;

  constructor(props: ICustomerVarProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  getEmptyState = () => ({
    variables: [],
    rows: [],
    open: false,
    variable: {
      name: '',
      description: '',
      variable_code: '',
      type: '',
      option: '',
      variable_values: [],
      is_global: false,
    },
    error: {
      name: { ...CustomerVarList.emptyErrorState },
      description: { ...CustomerVarList.emptyErrorState },
      variable_code: { ...CustomerVarList.emptyErrorState },
      type: { ...CustomerVarList.emptyErrorState },
      option: { ...CustomerVarList.emptyErrorState },
      variable_values: { ...CustomerVarList.emptyErrorState },
    },
    isopenConfirm: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    reset: false,
    isFilterModalOpen: false,
    openViewModal: false,
    filters: {
      type: [],
      option: [],
      is_global: [],
    },
    warningMessage: null,
    viewhistory: false,
    viewCDhistory: false,
  });

  componentDidMount() {
    if (this.props.types.length === 0) {
      this.props.fetchVariableTypes();
    }
  }

  componentDidUpdate(prevProps: ICustomerVarProps) {
    if (this.props.variables && this.props.variables !== prevProps.variables) {
      this.setRows(this.props);
    }
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchCustomerVars(this.props.customerId);
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));
    if ( _.get(this.props, 'user.type') === 'provider' && this.props.customerId) {
      this.props.fetchCustomerVars(this.props.customerId, newParams);
    }
    if ( _.get(this.props, 'user.type') === 'customer') {
      this.props.fetchCustomerVars(null, newParams);
    }
  };
  onSearchListChange = (e?: any) => {
    const search = e ? e.target.value : this.state.pagination.params.search;


    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: ICustomerVarProps) => {
    const customersResponse = nextProps.variables;
    const variables: any[] = customersResponse.results;
    const rows: any[] =
      variables &&
      variables.map((variable, index) => ({
        name: variable.name,
        variable_code: variable.variable_code,
        type: variable.type,
        option:
          !_.isEmpty(variable.customer_variables)
            ? variable.customer_variables.option
            : variable.option,
        variable_values:
          !_.isEmpty(variable.customer_variables) ? variable.customer_variables.variable_values
            : variable.variable_values,
        is_edited: !_.isEmpty(variable.customer_variables),
        is_global: variable.is_global,
        description: variable.description,
        updated_on: variable.updated_on,
        created_on: variable.created_on,
        id: variable.id,
        index,
      }));

    this.setState(prevState => ({
      reset: false,
      rows,
      variables,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };
  onClickViewviewCDHistory = () => {
    this.setState({
      viewCDhistory: true,
    });
  }
  toggleviewCDHistoryPopup = (e, reverted) => {
    if (reverted) {
      this.fetchData(this.state.pagination.params)
    }
    this.setState({
      viewCDhistory: false,
    });
  };


  onEditRowClick = (variables: any, event: any) => {
    event.stopPropagation();
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.variable as IVariable) = variables;
    this.setState(newState);
  };

  onRowClick = rowInfo => {
    event.stopPropagation();
    // tslint:disable-next-line: max-line-length
    this.setState({ openViewModal: true, variable: rowInfo.original });
  };

  isAccessAvailable = () => {
    let isAccess: boolean = false;
    let writeAccess = _.get(this.props, 'customerProfile.config_compliance_service_write_allowed', false)
    let isProvider = _.get(this.props, 'user.type') === 'provider' ? true : false;

    isAccess = isProvider ? true : writeAccess;

    return isAccess;
  };
  renderTopBar = () => {
    return (
      <div className="service-catalog-listing__actions ">
        <div className="header-panel">
          <Input
            field={{
              value: this.state.pagination.params.search,
              label: '',
              type: "SEARCH",
            }}
            width={11}
            name="searchString"
            onChange={this.onSearchListChange}
            placeholder="Search"
            className="search"
          />
          <div className="actions">
            <SquareButton
              onClick={this.onClickViewviewCDHistory}
              content={
                <span>
                  <img alt="" src="/assets/icons/version.svg" />
                  History
                </span>
              }
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.variables && this.props.variables.length === 0
              }
              bsStyle={"primary"}
            />
            {
              this.isAccessAvailable() &&
              <SquareButton
                content={`+ Add Customer Variable`}
                bsStyle={"primary"}
                onClick={() => this.clearPopUp(true)}
                className="save"
                disabled={ _.get(this.props, 'user.type') === 'provider' && !this.props.customerId}
              />
            }
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };
  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      prevState => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.setState(prevState => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              is_global: prevState.filters.is_global.join(),
              option: prevState.filters.option.join(),
              type: prevState.filters.type.join(),
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };
  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: '',
    });
  };

  onClickViewHistory(id: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      id,
    });
  }

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      (filters.is_global && filters.is_global.length > 0) ||
      (filters.type && filters.type.length > 0) ||
      (filters.option && filters.option.length > 0);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.type.length > 0 && (
          <div className="section-show-filters">
            <label>Type: </label>
            <SelectInput
              name="type"
              value={filters.type}
              onChange={this.onFilterChange}
              options={this.typeList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.option.length > 0 && (
          <div className="section-show-filters">
            <label>Option : </label>
            <SelectInput
              name="option"
              value={filters.option}
              onChange={this.onFilterChange}
              options={[
                { value: 'OR', label: 'OR' },
                { value: 'AND', label: 'AND' },
              ]}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.is_global.length > 0 && (
          <div className="section-show-filters">
            <label>Is Global: </label>
            <SelectInput
              name="is_global"
              value={filters.is_global}
              onChange={this.onFilterChange}
              options={[
                { label: 'Global', value: 'true' },
                { label: 'Customer', value: 'false' },
              ]}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };
  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.props
      .checkDeleteCustomerVar(this.props.customerId, original.original.id)
      .then(a => {
        if (a.type === FETCH_C_VAR_SUCCESS) {
          const newState = cloneDeep(this.state);
          (newState.warningMessage as any) = this.getWarningMessage(
            a.response && a.response.associated_rules
          );
          (newState.isopenConfirm as boolean) = true;
          (newState.id as boolean) = original.original.id;
          this.setState(newState);
        }
      });
  }

  getWarningMessage = rules => {
    const message = (
      <div className="types-delete">
        {' '}
        {rules.length > 1 && (
          <p className="warning-message">
            {' '}
            {rules.length}
            {rules.length > 1 ? ' rules' : ' rule'} will be deleted associated
            with this variable.
          </p>
        )}
        <div className="body-section">
          <div className="device-details-circuit-info">
            {rules && rules.length > 0 && (
              <div className="document">
                <div className="tile-index">
                  <label> #</label>
                </div>
                <div className="tile">
                  <label> Name</label>
                </div>
                <div className="tile">
                  <label> Priority</label>
                </div>
              </div>
            )}
            {rules && rules.length > 0
              ? rules.map((key, fieldIndex) => {
                return (
                  <div className="document" key={fieldIndex}>
                    <div className="tile-index">
                      <label />
                      <label>{fieldIndex + 1}</label>
                    </div>
                    <div className="tile">
                      <label />
                      <label>{key.name ? key.name : 'N.A.'}</label>
                    </div>
                    <div className="tile">
                      <label />
                      <label>{key.level ? key.level : 'N.A.'}</label>
                    </div>
                  </div>
                );
              })
              : 'No rules are associated.'}
          </div>
        </div>
      </div>
    );

    return message;
  };

  onClickConfirm = () => {
    this.props
      .deleteCustomerVar(this.props.customerId, this.state.id)
      .then(action => {
        if (action.type === FETCH_C_VAR_SUCCESS) {
          this.debouncedFetch();
          this.toggleConfirmOpen();
        }
      });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: '',
    });
  };

  toggleFilterModal = () => {
    this.setState(prevState => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IVariableFilter) => {
    if (filters) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            is_global: filters.is_global.join(),
            option: filters.option.join(),
            type: filters.type.join(),
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal();
    }
  };

  handleChangeValue = e => {
    const newState = cloneDeep(this.state);
    (newState.variable.variable_values as any) = e;
    this.setState(newState);
  };
  handleChange = e => {
    const newState = cloneDeep(this.state);
    (newState.variable[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.variable as IVariable) = this.getEmptyState().variable;
    (newState.error as any) = this.getEmptyState().error;
    (newState.isopenConfirm as any) = false;
    (newState.warningMessage as any) = '';
    this.setState(newState);
  };

  validateForm() {
    const error: any = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.variable.name ||
      this.state.variable.name.trim().length === 0
    ) {
      error.name.errorState = "error";
      error.name.errorMessage = 'Name cannot be empty';
      isValid = false;
    }
    if (
      !this.state.variable.variable_values ||
      this.state.variable.variable_values.length === 0
    ) {
      error.variable_values.errorState = "error";
      error.variable_values.errorMessage = 'value cannot be empty';
      isValid = false;
    }
    if (
      !this.state.variable.description ||
      this.state.variable.description.trim().length === 0
    ) {
      error.description.errorState = "error";
      error.description.errorMessage = 'Description cannot be empty';
      isValid = false;
    }
    if (
      !this.state.variable.variable_code ||
      this.state.variable.variable_code.trim().length === 0
    ) {
      error.variable_code.errorState = "error";
      error.variable_code.errorMessage =
        'Variable code sould be in format:  @{{YOUR_CODE}}';
      isValid = false;
    }
    if (
      this.state.variable.variable_code &&
      !AppValidators.isValidRuleVarible(this.state.variable.variable_code)
    ) {
      error.variable_code.errorState = "error";
      error.variable_code.errorMessage =
        'Variable code sould be in format:  @{{YOUR_CODE}}';
      isValid = false;
    }

    if (!this.state.variable.type) {
      error.type.errorState = "error";
      error.type.errorMessage = 'Please select Type';
      isValid = false;
    }
    if (!this.state.variable.option) {
      error.option.errorState = "error";
      error.option.errorMessage = 'Please select Option';
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }
  onSaveClick = (
    variable: IVariable,
    reload: boolean = false,
    checkValid: boolean = false
  ) => {
    if (this.validateForm() || checkValid) {
      if (variable.id) {
        this.props
          .editCustomerVar(this.props.customerId, variable)
          .then(action => {
            if (action.type === 'FETCH_C_VAR_SUCCESS') {
              this.clearPopUp();
              if (reload) {
                this.debouncedFetch();
              }
            }
            if (action.type === 'FETCH_C_VAR_FAILURE') {
              this.setValidationErrors(action.errorList.data);
            }
          });
      } else {
        this.props
          .saveCustomerVar(this.props.customerId, variable)
          .then(action => {
            if (action.type === 'FETCH_C_VAR_SUCCESS') {
              this.clearPopUp();
              if (reload) {
                this.setState({ reset: true })
                this.debouncedFetch({ page: 1 });
              }
            }
            if (action.type === 'FETCH_C_VAR_FAILURE') {
              this.setValidationErrors(action.errorList.data);
            }
          });
      }
    }
  };

  setValidationErrors = errorList => {
    const newState: ICustomerVarState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };

  typeList = () => {
    const typeList = this.props.types
      ? this.props.types.map(c => ({
        value: c,
        label: c,
        disabled: false,
      }))
      : [];

    return typeList;
  };

  toggleViewModal = () => {
    this.setState({
      variable: this.getEmptyState().variable,
      openViewModal: !this.state.openViewModal,
    });
  };
  render() {
    const columns: ITableColumn[] = [
      {
        accessor: 'name',
        Header: 'Name',
        id: 'name',
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'is_global',
        Header: 'Variable Type',
        width: 95,
        sortable: false,
        id: 'is_global',
        Cell: cell => (
          <div className="icons-template-1">
            {cell.original.is_global && (
              <img
                src={`/assets/icons/global-green.svg`}
                alt=""
                title={`Global Variable`}
                className="info-svg-images"
              />
            )}
            {cell.original.is_global === false && (
              <img
                src={`/assets/icons/global-grey.svg`}
                alt=""
                title={`Customer Variable`}
                className="info-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'is_edited',
        Header: 'Edited',
        width: 60,
        sortable: false,
        id: 'is_edited',
        Cell: cell => (
          <div className="icons-template-1">
            {cell.original.is_edited && (
              <img
                src={`/assets/icons/active-right.svg`}
                alt=""
                title={`Edited`}
                className="info-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'variable_code',
        Header: 'Code',
        id: 'variable_code',
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'description',
        Header: 'Description',
        id: 'description',
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'type',
        Header: 'Type',
        id: 'type',
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'option',
        Header: 'Option',
        id: 'option',
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'id',
        Header: 'History',
        width: 55,
        sortable: false,
        Cell: cell => (
          <IconButton
            icon="version.svg"
            onClick={e => {
              this.onClickViewHistory(cell.original.id, e);
            }}
            title={'Show Variable History'}
          />
        ),
      },
      {
        accessor: 'id',
        Header: 'Actions',
        width: 140,
        show: this.isAccessAvailable(),
        sortable: false,
        Cell: cell => (
          <div>
            {<EditButton
              onClick={e => this.onEditRowClick(cell.original, e)}
            />}
            {cell.original.is_global === false && (
              <DeleteButton onClick={e => this.onDeleteRowClick(cell, e)} />
            )}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: 'id',
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };

    return (
      <div className="variable-listing-parent">
        <div className="loader">
          <Spinner
            show={
              this.props.isFetching ||
              this.props.isFetchingList ||
              this.props.isFetchingTypes
            }
          />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetching ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
          message={this.state.warningMessage}
        />
        <VariableFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          types={this.props.types}
          prevFilters={this.state.filters}
        />
        <ViewVariable
          show={this.state.openViewModal}
          onClose={this.toggleViewModal}
          variable={this.state.variable}
          isFetching={this.props.isFetching}
        />
        <ModalBase
          show={this.state.open}
          onClose={() => this.clearPopUp(!this.state.open)}
          titleElement={` ${
            this.state.variable.id ? 'Update' : 'Add'
            } Variable`}
          bodyElement={
            <div className="col-md-12 body">
              <Input
                field={{
                  label: 'Name',
                  type: "TEXT",
                  value: this.state.variable.name,
                  isRequired: true,
                }}
                width={12}
                name="name"
                onChange={e => this.handleChange(e)}
                error={this.state.error.name}
                placeholder={`Enter Name`}
                disabled={this.state.variable.is_global}
              />
              <Input
                field={{
                  label: 'Code',
                  type: "TEXT",
                  value: this.state.variable.variable_code,
                  isRequired: true,
                }}
                width={12}
                name="variable_code"
                onChange={e => this.handleChange(e)}
                error={this.state.error.variable_code}
                placeholder={`Enter Variable Code`}
                disabled={this.state.variable.id ? true : false}
              />
              <div
                className="email-tag  field-section
               field-section--error
               field-section--required
                 col-md-12 col-xs-12"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Value
                  </label>
                  <span className="field__label-required" />
                </div>
                <div>
                  {/* <TagsInput
                    value={
                      this.state.variable.variable_values
                        ? this.state.variable.variable_values
                        : []
                    }
                    onChange={e => this.handleChangeValue(e)}
                    inputProps={{
                      className: 'react-tagsinput-input',
                      placeholder: 'Enter Value',
                    }}
                    addOnBlur={true}
                  /> */}
                </div>
                {this.state.error.variable_values.errorMessage && (
                  <div className="field__error">
                    {this.state.error.variable_values.errorMessage}
                  </div>
                )}
              </div>
              <Input
                field={{
                  label: 'Option',
                  type: "PICKLIST",
                  value: this.state.variable.option,
                  options: ['AND', 'OR'],
                  isRequired: true,
                }}
                width={6}
                name="option"
                onChange={e => this.handleChange(e)}
                error={this.state.error.option}
                placeholder={`Select option`}
              />
              <Input
                field={{
                  label: 'Type',
                  type: "PICKLIST",
                  value: this.state.variable.type,
                  options: this.typeList(),
                  isRequired: true,
                }}
                width={6}
                name="type"
                onChange={e => this.handleChange(e)}
                error={this.state.error.type}
                placeholder={`Select type`}
                disabled={this.state.variable.is_global}
              />
              <Input
                field={{
                  label: 'Description',
                  type: "TEXTAREA",
                  value: this.state.variable.description,
                  isRequired: true,
                }}
                width={12}
                name="description"
                onChange={e => this.handleChange(e)}
                error={this.state.error.description}
                placeholder={`Enter Description`}
                disabled={this.state.variable.is_global}
              />
            </div>
          }
          footerElement={
            <div>
              {' '}
              <SquareButton
                content={`Cancel`}
                bsStyle={"default"}
                onClick={e => this.clearPopUp()}
                className="save"
              />
              <SquareButton
                content={`Save`}
                bsStyle={"primary"}
                onClick={e =>
                  this.onSaveClick(this.state.variable, true, false)
                }
                className="save"
              />
            </div>
          }
          className="add-edit-variable"
        />
        <ViewVariableHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
          variableType="customer"
        />
        {this.state.viewCDhistory &&
          <ViewVariableCDHistory
            show={this.state.viewCDhistory}
            onClose={this.toggleviewCDHistoryPopup}
          />
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  variables: state.configuration.customerVariables,
  isFetching: state.configuration.isFetching,
  isFetchingTypes: state.configuration.isFetchingTypes,
  variable: state.configuration.customerVariable,
  types: state.configuration.types,
  isFetchingList: state.configuration.isFetchingList,
  customers: state.customer.customersShort,
  user: state.profile.user,
  customerId: state.customer.customerId,
  customerProfile: state.customerUser.customerProfile
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchCustomerVars: (custId: any, params?: IServerPaginationParams) =>
    dispatch(fetchCustomerVars(custId, params)),
  fetchVariableTypes: () => dispatch(fetchVariableTypes()),
  fetchSingleCustomerVar: (custId: any, id: number) =>
    dispatch(fetchSingleCustomerVar(custId, id)),
  saveCustomerVar: (custId: any, variable: any) =>
    dispatch(saveCustomerVar(custId, variable)),
  editCustomerVar: (custId: any, variable: any) =>
    dispatch(editCustomerVar(custId, variable)),
  deleteCustomerVar: (custId: any, id: number) =>
    dispatch(deleteCustomerVar(custId, id)),
  checkDeleteCustomerVar: (custId: any, id: number) =>
    dispatch(checkDeleteCustomerVar(custId, id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerVarList);
