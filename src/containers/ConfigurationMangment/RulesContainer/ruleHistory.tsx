import React from "react";
import Spinner from "../../../components/Spinner";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { getRuleHistory } from "../../../actions/configuration";
// import ReactDiffViewer from "react-diff-viewer"; (New Component)
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
import { getFieldNames } from "../../../utils/fieldName";
import { isEqual } from "lodash";
import { isEmptyObj } from "../../../utils/CommonUtils";

interface IViewRuleHistoryProps extends ICommonProps {
  id: number;
  show: boolean;
  ruleHistory: any;
  isFetchingHistory: boolean;
  classifications: IDLabelObject[];
  onClose: (e: any) => void;
  getRuleHistory: (id: number) => Promise<any>;
}

interface IViewRuleHistoryState {
  open: boolean;
  ruleHistory: any;
  loadMore: boolean;
  isCollapsed: boolean[];
}

class ViewRuleHistory extends React.Component<
  IViewRuleHistoryProps,
  IViewRuleHistoryState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IViewRuleHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    ruleHistory: [],
    isCollapsed: [],
    loadMore: false,
  });

  componentDidUpdate(prevProps: IViewRuleHistoryProps) {
    if (this.props.id && this.props.id !== prevProps.id) {
      this.props.getRuleHistory(this.props.id);
    }
    if (
      this.props.ruleHistory &&
      this.props.ruleHistory !== prevProps.ruleHistory
    ) {
      this.setState({
        isCollapsed: Object.keys(
          this.props.ruleHistory.filter((h) => !isEmptyObj(h.diff))
        ).map((key) => false),
      });
    }
  }

  getTitle = () => {
    return <div className="title">Rule History</div>;
  };

  onClickLoadMore = () => {
    this.setState({ loadMore: !this.state.loadMore });
  };

  getVersion = (boardIndex: any, getNumber: boolean = false) => {
    const ruleHistory = this.props.ruleHistory.filter(
      (h) => !isEmptyObj(h.diff)
    );
    const version = ruleHistory && ruleHistory.length - boardIndex;

    return getNumber ? version : version.toFixed(2);
  };

  getValueByField = (value, field) => {
    let currentValue = value;

    if (typeof value === "object") {
      currentValue = null;
      currentValue = JSON.stringify(value, null, 4);
    }
    if (field.indexOf(`classification_id`) > -1) {
      const classification = this.props.classifications.filter(
        (v) => v.id === value
      );
      currentValue = classification[0].name;
    }
    if (field.indexOf("is_enabled") > -1) {
      currentValue = value === true ? "Enable" : "Disable";
    }

    if (field.indexOf("customers_not_assigned") > -1) {
      currentValue = value && value.length > 0 ? "Un-Assigned" : "Assigned";
    }

    if (field.indexOf("applies_to") > -1) {
      currentValue = value && value.length > 0 ? value.join(",\n") : "";
    }

    return currentValue;
  };

  renderHistoryContainer = (history: any, boardIndex: number) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];
    const toggleCollapsedState = () =>
      this.setState((prevState) => ({
        isCollapsed: [
          ...prevState.isCollapsed.slice(0, boardIndex),
          !prevState.isCollapsed[boardIndex],
          ...prevState.isCollapsed.slice(boardIndex + 1),
        ],
      }));
    return (
      <div
        key={boardIndex}
        className={`collapsable-section  ${
          isCollapsed
            ? "collapsable-section--collapsed"
            : "collapsable-section--not-collapsed"
        }`}
      >
        <div
          className="col-md-12 collapsable-heading"
          onClick={toggleCollapsedState}
        >
          <div className="left col-md-10">
            <div className="name">
              {" "}
              <span>User: </span>
              {history.revision.user}
            </div>
            <div className="version">
              {" "}
              <span>version: </span>
              {this.getVersion(boardIndex)}
            </div>
            <div className="date">
              {" "}
              <span>Updated On: </span>
              {utcToLocalInLongFormat(history.revision.date_created)}
            </div>
          </div>
        </div>
        <div className="collapsable-contents">
          {history.diff && history.diff.values_changed && (
            <>
              <div className="heading-version">
                <span>
                  {" "}
                  {` Version ${(
                    (this.getVersion(boardIndex, true) as any) - 1
                  ).toFixed(2)}`}{" "}
                </span>
                <span>{` Version ${this.getVersion(boardIndex)}`}</span>
              </div>
              {Object.keys(history.diff.values_changed).map((v, i) => {
                // const version = history.diff.values_changed[v];
                return (
                  <div key={i} className="diff-view-section">
                    <span className="heading">{getFieldNames(v)}</span>
                    {/* <ReactDiffViewer
                      oldValue={`${this.getValueByField(version.old_value, v)}`}
                      newValue={`${this.getValueByField(version.new_value, v)}`}
                      splitView={true}
                      disableWordDiff={version.diff ? false : true}
                    /> */}
                  </div>
                );
              })}
            </>
          )}
          {!history.diff ||
            (isEqual(history.diff, {}) && (
              <div className="no-data"> No diff data</div>
            ))}
        </div>
      </div>
    );
  };

  getBody = () => {
    const ruleHistory = this.props.ruleHistory.filter(
      (h) => !isEmptyObj(h.diff)
    );

    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingHistory} />
        </div>
        <div className="history-section heading  col-md-12">
          {ruleHistory.length === 0 && (
            <div className="col-md-12 no-data" style={{ textAlign: "center" }}>
              {" "}
              No History available.
            </div>
          )}
          {ruleHistory.length !== 0 &&
            !this.state.loadMore &&
            ruleHistory.slice(0, 5).map((history, i) => {
              return this.renderHistoryContainer(history, i);
            })}
          {ruleHistory.length !== 0 &&
            this.state.loadMore &&
            ruleHistory.map((history, i) => {
              return this.renderHistoryContainer(history, i);
            })}
          {ruleHistory.length > 5 && (
            <div
              className="col-md-12 show-more"
              onClick={(e) => this.onClickLoadMore()}
              style={{ textAlign: "center" }}
            >
              {" "}
              {!this.state.loadMore ? "load more..." : "show less"}
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`footer
        ${this.props.isFetchingHistory ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="version-history"
      />
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  ruleHistory: state.configuration.ruleHistory,
  isFetchingHistory: state.configuration.isFetchingHistory,
  classifications: state.configuration.classifications,
});

const mapDispatchToProps = (dispatch: any) => ({
  getRuleHistory: (id: number) => dispatch(getRuleHistory(id)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewRuleHistory)
);
