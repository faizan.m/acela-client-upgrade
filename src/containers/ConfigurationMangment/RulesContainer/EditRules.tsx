import _, { cloneDeep } from "lodash";
// import Select from "react-select"; (New Component)
import { connect } from "react-redux";
import { Mention, MentionsInput } from "react-mentions";
import React from "react";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import TooltipCustom from "../../../components/Tooltip/tooltip";
import Checkbox from "../../../components/Checkbox/checkbox";
import SquareButton from "../../../components/Button/button";

import {
  createClassifications,
  editRule,
  FETCH_COMP_CLASS_SUCCESS,
  fetchClassifications,
  fetchGlobalVariables,
  fetchRulesLevels,
  saveRule,
  fetchRuleFunctions,
  FETCH_RULE_FUNCTION_SUCCESS,
} from "../../../actions/configuration";
import {
  fetchTypes,
  fetchManufacturers,
  fetchDevices,
  FETCH_DEVICES_SUCCESS,
} from "../../../actions/inventory";

import "./editrule.style.scss";
import { commonFunctions } from "../../../utils/commonFunctions";

interface IRuleProps {
  rule: IRule;
  show?: boolean;
  devices: any[];
  types: string[];
  levels: string[];
  isFetching: boolean;
  globalVariables: IVariable[];
  manufacturers: IDLabelObject[];
  classifications: IDLabelObject[];
  onClose: () => void;
  fetchTypes: () => Promise<any>;
  fetchRulesLevels: () => Promise<any>;
  saveRule: (rule: any) => Promise<any>;
  editRule: (rule: any) => Promise<any>;
  fetchRuleFunctions: () => Promise<any>;
  fetchManufacturers: TFetchManufacturers;
  fetchDevices: (id: number) => Promise<any>;
  createClassifications: (text: string) => Promise<any>;
  fetchClassifications: (user_type?: string) => Promise<any>;
  fetchGlobalVariables: (params?: IServerPaginationParams) => Promise<any>;
}

interface IRuleState {
  rule: IRule;
  open: boolean;
  reset: boolean;
  functionList: IPickListOptions[];
  devicesList: IPickListOptions[];
  error: {
    detail: IFieldValidation;
    level: IFieldValidation;
    classification: IFieldValidation;
    customers_assigned: IFieldValidation;
    applies_to: IFieldValidation;
    applies_to_manufacturers: IFieldValidation;
    name: IFieldValidation;
    description: IFieldValidation;
    function: IFieldValidation;
    to_ignore_devices: IFieldValidation;
    apply_not: IFieldValidation;
  };
}

class EditRules extends React.Component<IRuleProps, IRuleState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IRuleProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  componentDidMount() {
    if (!this.props.manufacturers) {
      this.props.fetchManufacturers();
    }
    if (this.props.levels.length === 0) {
      this.props.fetchRulesLevels();
    }
    if (this.props.classifications.length === 0) {
      this.props.fetchClassifications(_.get(this.props, "user.type"));
    }
    if (this.props.types === null) {
      this.props.fetchTypes();
    }
    this.props.fetchGlobalVariables({ pagination: false });

    this.setState({
      rule: this.props.rule,
    });

    this.fetchRuleFunction();
    this.fetchDevicesData();
  }

  getEmptyState = () => ({
    open: false,
    reset: false,
    devicesList: [],
    functionList: [],
    rule: {
      detail: "",
      level: "",
      classification: 0,
      description: "",
      applies_to: [],
      applies_to_manufacturers: [],
      name: "",
      function: "",
      is_enabled: true,
      apply_not: false,
      to_ignore_devices: [],
    },
    error: {
      detail: { ...EditRules.emptyErrorState },
      level: { ...EditRules.emptyErrorState },
      classification: { ...EditRules.emptyErrorState },
      customers_assigned: { ...EditRules.emptyErrorState },
      applies_to: { ...EditRules.emptyErrorState },
      applies_to_manufacturers: { ...EditRules.emptyErrorState },
      name: { ...EditRules.emptyErrorState },
      description: { ...EditRules.emptyErrorState },
      function: { ...EditRules.emptyErrorState },
      to_ignore_devices: { ...EditRules.emptyErrorState },
      apply_not: { ...EditRules.emptyErrorState },
    },
  });

  fetchRuleFunction = () => {
    this.props.fetchRuleFunctions().then((action) => {
      if (action.type === FETCH_RULE_FUNCTION_SUCCESS && action.response) {
        const functionList = action.response.map((c) => ({
          value: c.key,
          label: c.key,
          disabled: false,
        }));
        this.setState({ functionList });
      }
    });
  };

  fetchDevicesData = () => {
    this.props.fetchDevices(10242).then((action) => {
      if (action.type === FETCH_DEVICES_SUCCESS && action.response) {
        const devicesList = action.response.map((c) => ({
          value: c.id,
          label: c.device_name,
          disabled: false,
        }));
        this.setState({ devicesList });
      }
    });
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.rule as IRule) = this.getEmptyState().rule;
    (newState.error as any) = this.getEmptyState().error;
    this.setState(newState);
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        [e.target.name]: e.target.value,
      },
    }));
  };

  levelList = () => {
    const levelList = this.props.levels
      ? this.props.levels.map((c) => ({
          value: c,
          label: c,
          disabled: false,
        }))
      : [];

    return levelList;
  };

  manufacturers = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((c) => ({
          value: c.label,
          label: c.label,
        }))
      : [];

    return manufacturers;
  };

  typeList = () => {
    const types = this.props.types
      ? this.props.types.filter((x) => x !== "")
      : [];
    const typess =
      types && types.length > 0
        ? types.map((c) => ({
            value: c,
            label: c,
            disabled: false,
          }))
        : [];

    return typess;
  };

  classificationList = () => {
    const classification =
      this.props.classifications.length > 0
        ? this.props.classifications.map((c) => ({
            value: c.id,
            label: c.name,
            disabled: false,
          }))
        : [];

    return classification;
  };

  handleChangeClassification = (e) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        classification: e.value,
      },
    }));
  };

  onNewOptionClick = (option) => {
    const { className, ...newOption } = option;
    const name = newOption.label;
    this.props.createClassifications(name).then((action) => {
      if (action.type === FETCH_COMP_CLASS_SUCCESS) {
        this.props.fetchClassifications(_.get(this.props, "user.type"));
      }
    });
  };

  globalVariableList = () => {
    const globalVariables =
      this.props.globalVariables && this.props.globalVariables.length > 0
        ? this.props.globalVariables.map((c) => ({
            id: c.id,
            display: c.variable_code,
          }))
        : [];

    return globalVariables;
  };

  rangeVariables = () => {
    const variablesList = [
      "#NUM_RANGE(1-10)",
      "#VAR_OPTION_OR(value1,value2,value3)",
      "#VAR_OPTION_AND(value1, value2,value3) ",
    ];
    const globalVariables = variablesList.map((c) => ({
      id: c,
      display: c,
    }));

    return globalVariables;
  };

  handleChangeType = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        is_enabled: JSON.parse(e.target.value),
      },
    }));
  };

  handleChangeDetails = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        detail: e.target.value,
      },
    }));
  };

  handleChangeCheckBox = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        [e.target.value]: e.target.checked,
      },
    }));
  };

  validateForm() {
    const newState: IRuleState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.rule.name || this.state.rule.name.trim().length === 0) {
      newState.error.name.errorState = "error";
      newState.error.name.errorMessage = "Name cannot be empty";
      isValid = false;
    }
    if (!this.state.rule.detail || this.state.rule.detail.trim().length === 0) {
      newState.error.detail.errorState = "error";
      newState.error.detail.errorMessage = "Text cannot be empty";
      isValid = false;
    }
    if (!this.state.rule.level) {
      newState.error.level.errorState = "error";
      newState.error.level.errorMessage = "Please select level";
      isValid = false;
    }
    if (!this.state.rule.classification) {
      newState.error.classification.errorState = "error";
      newState.error.classification.errorMessage =
        "Please select classification";
      isValid = false;
    }

    if (
      !this.state.rule.applies_to ||
      (this.state.rule.applies_to && this.state.rule.applies_to.length === 0)
    ) {
      newState.error.applies_to.errorState = "error";
      newState.error.applies_to.errorMessage = "Please select applies to";
      isValid = false;
    }
    if (
      !this.state.rule.applies_to_manufacturers ||
      (this.state.rule.applies_to_manufacturers &&
        this.state.rule.applies_to_manufacturers.length === 0)
    ) {
      newState.error.applies_to_manufacturers.errorState =
        "error";
      newState.error.applies_to_manufacturers.errorMessage =
        "Please select applies to manufacturers";
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  setValidationErrors = (errorList: object) => {
    const newState: IRuleState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  onSaveClick = (
    rule: IRule,
    reload: boolean = false,
    checkValid: boolean = false
  ) => {
    if (this.validateForm() || checkValid) {
      const regex = /#RULE#/gi;

      rule.detail = rule.detail.replace(regex, "");
      if (rule.id) {
        this.props.editRule(rule).then((action) => {
          if (action.type === "FETCH_RULE_SUCCESS") {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true });
            }
          }
          if (action.type === "FETCH_RULE_FAILURE") {
            this.setValidationErrors(action.errorList.data);
          }
        });
      }
    }
  };

  render() {
    return (
      <>
        <ModalBase
          show={this.props.show}
          onClose={() => this.props.onClose()}
          titleElement={` ${this.state.rule.id ? "Update" : "Add"} Rule`}
          bodyElement={
            <div className="col-md-12 body">
              <div className="loader">
                <Spinner show={this.props.isFetching} />
              </div>
              <Input
                field={{
                  label: "Name",
                  type: "TEXT",
                  value: this.state.rule.name,
                  isRequired: true,
                }}
                width={6}
                name="name"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.name}
                placeholder={`Enter Name`}
              />
              <Input
                field={{
                  label: "Level",
                  type: "PICKLIST",
                  value: this.state.rule.level,
                  options: this.levelList(),
                  isRequired: true,
                }}
                width={6}
                name="level"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.level}
                placeholder={`Select Level`}
              />
              <Input
                field={{
                  label: "Applies To Manufacturer",
                  type: "PICKLIST",
                  value: this.state.rule.applies_to_manufacturers,
                  options: this.manufacturers(),
                  isRequired: true,
                }}
                width={6}
                multi={true}
                name="applies_to_manufacturers"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.applies_to_manufacturers}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: "Applies To",
                  type: "PICKLIST",
                  value: this.state.rule.applies_to,
                  options: this.typeList(),
                  isRequired: true,
                }}
                width={6}
                multi={true}
                name="applies_to"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.applies_to}
                placeholder={`Select`}
              />
              <div
                className={`field-section
                 field-section--required   col-md-6 col-xs-6 ${
                   this.state.error.classification.errorMessage
                     ? `field-section--error`
                     : ""
                 } `}
              >
                <div className="field__label row">
                  <label className="field__label">Classification</label>
                  <span className="field__label-required" />
                </div>
                <div>
                  {" "}
                  {/* <Select.Creatable
                    name="classification"
                    options={this.classificationList()}
                    onChange={(e) => this.handleChangeClassification(e)}
                    value={this.state.rule.classification}
                    onNewOptionClick={this.onNewOptionClick}
                    placeholder="Select or Type to Create New"
                    loading={this.props.isFetching}
                    clearable={false}
                    promptTextCreator={(name) => `Create Type "${name}"`}
                  /> */}
                </div>
                {this.state.error.classification.errorMessage && (
                  <div className="field__error">
                    {this.state.error.classification.errorMessage}
                  </div>
                )}
              </div>
              <Input
                field={{
                  label: "Function ",
                  type: "PICKLIST",
                  value: this.state.rule.function,
                  options: this.state.functionList,
                  isRequired: false,
                }}
                width={6}
                multi={false}
                name="function"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.function}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: "To Ignore Devices",
                  type: "PICKLIST",
                  value: this.state.rule.to_ignore_devices,
                  options: this.state.devicesList,
                  isRequired: false,
                }}
                width={6}
                multi={true}
                name="to_ignore_devices"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.to_ignore_devices}
                placeholder={`Select`}
              />
              <div className="field-section field-section--required  col-md-12 col-xs-12">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Detail
                  </label>
                  <span className="field__label-required" />
                  <TooltipCustom />
                  <div className="not-checkbox">
                    <Checkbox
                      isChecked={this.state.rule.apply_not}
                      name="apply_not"
                      onChange={(e) => this.handleChangeCheckBox(e)}
                    >
                      Apply NOT
                    </Checkbox>
                  </div>
                </div>
                <div
                  className={`${
                    this.state.error.detail.errorMessage ? `error-input` : ""
                  }`}
                >
                  <MentionsInput
                    markup="[__display__]"
                    value={this.state.rule.detail}
                    onChange={this.handleChangeDetails}
                    className={"outer"}
                  >
                    <Mention
                      trigger="@"
                      data={this.globalVariableList()}
                      className={"inner-drop"}
                      markup="#RULE#__display__"
                    />
                    <Mention
                      trigger="#"
                      data={this.rangeVariables()}
                      className={"inner-drop"}
                      markup="#RULE#__display__"
                    />
                  </MentionsInput>
                </div>
                {this.state.error.detail.errorMessage && (
                  <div className="field__error">
                    {this.state.error.detail.errorMessage}
                  </div>
                )}
              </div>
              <Input
                field={{
                  label: "Description",
                  type: "TEXTAREA",
                  value: this.state.rule.description,
                  isRequired: false,
                }}
                width={12}
                name="description"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.description}
                placeholder={`Enter Description`}
              />
              {this.state.rule.id !== 0 && (
                <Input
                  field={{
                    label: "Status",
                    type: "RADIO",
                    value: this.state.rule.is_enabled,
                    isRequired: true,
                    options: [
                      { value: true, label: "Enabled" },
                      { value: false, label: "Disabled" },
                    ],
                  }}
                  width={6}
                  name="is_enabled"
                  onChange={(e) => this.handleChangeType(e)}
                  placeholder={""}
                />
              )}
            </div>
          }
          footerElement={
            <div>
              <SquareButton
                content={`Cancel`}
                bsStyle={"default"}
                onClick={(e) => this.props.onClose()}
                className="save"
              />
              <SquareButton
                content={`Save`}
                bsStyle={"primary"}
                onClick={(e) => this.onSaveClick(this.state.rule, true, false)}
                className="save"
              />
            </div>
          }
          className="add-edit-rule"
        />
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.configuration.isFetching,
  classifications: state.configuration.classifications,
  levels: state.configuration.levels,
  types: state.inventory.types,
  manufacturers: state.inventory.manufacturers,
  globalVariables: state.configuration.globalVariables,
  devices: state.inventory.deviceSuccess,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchClassifications: (user_type?: string) =>
    dispatch(fetchClassifications(user_type)),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchRulesLevels: () => dispatch(fetchRulesLevels()),
  saveRule: (rule: any) => dispatch(saveRule(rule)),
  editRule: (rule: any) => dispatch(editRule(rule)),
  createClassifications: (text: string) =>
    dispatch(createClassifications(text)),
  fetchTypes: () => dispatch(fetchTypes()),
  fetchGlobalVariables: (params?: IServerPaginationParams) =>
    dispatch(fetchGlobalVariables(params)),
  fetchRuleFunctions: () => dispatch(fetchRuleFunctions()),
  fetchDevices: (id: number) => dispatch(fetchDevices(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditRules);
