import _ from "lodash";
import React from "react";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import "./style.scss";

interface IRuleProps {
  rule: IRule;
  show: boolean;
  onClose: (e: any) => void;
  devices?: any;
}

interface IRuleState {}

export class RuleDetails extends React.Component<IRuleProps, IRuleState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IRuleProps) {
    super(props);
    this.state = {};
  }

  static getDerivedStateFromProps(nextProps: IRuleProps) {
    if (nextProps.rule) {
      return {
        rule: nextProps.rule,
      };
    }
    return null;
  }

  getDevicesName = (key: string) => {
    const { devices } = this.props;

    let deviceNames = "";
    const devicesIds = _.get(this.props.rule, key, []);
    devices &&
      devices.map((device) => {
        if (devicesIds.indexOf(device.value) > -1) {
          deviceNames = deviceNames.concat(`${device.label}, `);
        }
      });
    return deviceNames;
  };

  renderViewRule = () => {
    return (
      <div>
        <div className={`add-circuit__body`}>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Name
              </label>
            </div>
            <div>{this.props.rule.name || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Level
              </label>
            </div>
            <div>{this.props.rule.level || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Applies To Manufacturers
              </label>
            </div>
            <div>
              {_.get(this.props.rule, "applies_to_manufacturers", []).join(
                ", "
              ) || "N.A."}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Applies To
              </label>
            </div>
            <div>
              {_.get(this.props.rule, "applies_to", []).join(", ") || "N.A."}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Classification
              </label>
            </div>
            <div>{this.props.rule.classification_name || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Is Global
              </label>
            </div>
            <div>{(this.props.rule.is_global && "Yes") || "No"}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row ">
              <label className="field__label-label" title="">
                Detail
              </label>
            </div>
            <div className="preline-wrap">
              {this.props.rule.detail || "N.A."}
            </div>
          </div>
          {this.props.rule.function && this.props.rule.function !== "" && (
            <div className="field-section     col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Function
                </label>
              </div>
              <div>{this.props.rule.function}</div>
            </div>
          )}
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Description
              </label>
            </div>
            <div>{this.props.rule.description || "N.A."}</div>
          </div>
          {this.props.rule.apply_not && (
            <div className="field-section col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  NOT Applied
                </label>
              </div>
              <div>YES</div>
            </div>
          )}
          {this.props.rule.to_ignore_devices &&
            this.props.rule.to_ignore_devices.length > 0 && (
              <div className="field-section     col-md-6 col-xs-6">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Devices Ignored
                  </label>
                </div>
                <div>{this.getDevicesName("to_ignore_devices")}</div>
              </div>
            )}
          {this.props.rule.to_include_devices &&
            this.props.rule.to_include_devices.length > 0 && (
              <div className="field-section     col-md-6 col-xs-6">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Devices Included
                  </label>
                </div>
                <div>{this.getDevicesName("to_include_devices")}</div>
              </div>
            )}
          {this.props.rule.exceptions && this.props.rule.exceptions !== "" && (
            <div className="field-section     col-md-6 col-xs-6">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Exceptions
                </label>
              </div>
              <div>
                {typeof this.props.rule.exceptions === "string"
                  ? this.props.rule.exceptions
                      .split("\n")
                      .map((exception, id) => <div key={id}>{exception}</div>)
                  : this.props.rule.exceptions.map((exception, id) => (
                      <div key={id}>{exception}</div>
                    ))}
              </div>
            </div>
          )}
          {this.props.rule.applies_to_ios_version &&
            this.props.rule.applies_to_ios_version.length > 0 && (
              <div className="field-section     col-md-6 col-xs-6">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Applies to OS version
                  </label>
                </div>
                <div>
                  {typeof this.props.rule.applies_to_ios_version === "string"
                    ? this.props.rule.applies_to_ios_version
                        .split("\n")
                        .map((version, id) => <div key={id}>{version}</div>)
                    : this.props.rule.applies_to_ios_version.map(
                        (version, id) => <div key={id}>{version}</div>
                      )}
                </div>
              </div>
            )}
        </div>
      </div>
    );
  };

  onClose = (event: any) => {
    this.props.onClose(event);
  };

  render() {
    return (
      <div className="rules-listing-parent">
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={`View Rule Info`}
          bodyElement={this.props.show && this.renderViewRule()}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.onClose}
            />
          }
          className="view-contacts"
        />
      </div>
    );
  }
}
