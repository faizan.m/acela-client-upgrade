import React from "react";
import { connect } from "react-redux";

import EditButton from "../../../components/Button/editButton";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";

import { DebouncedFunc, cloneDeep, debounce } from "lodash";
// import TagsInput from "react-tagsinput"; (New Component)
import {
  checkDeleteGlobalVariable,
  createClassifications,
  deleteGlobalVariable,
  editGlobalVariable,
  FETCH_G_VAR_SUCCESS,
  fetchGlobalVariables,
  fetchSingleGlobalVariable,
  fetchVariableTypes,
  saveGlobalVariable,
} from "../../../actions/configuration";
import SquareButton from "../../../components/Button/button";
import DeleteButton from "../../../components/Button/deleteButton";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import AppValidators from "../../../utils/validator";
import VariableFilter from "./filter";
import "./style.scss";
import ViewVariable from "../VariablesContainer/viewVariable";
import ViewVariableHistory from "../VariablesContainer/variableHistory";
import ViewVariableCDHistory from "../VariablesContainer/variableCDHistory";
import IconButton from "../../../components/Button/iconButton";
import { commonFunctions } from "../../../utils/commonFunctions";
import "../../../commonStyles/filtersListing.scss";

interface IGlobalVariableProps {
  variables: any;
  types: string[];
  isFetching: boolean;
  variable: IVariable;
  isFetchingList: boolean;
  isFetchingTypes: boolean;
  fetchVariableTypes: () => Promise<any>;
  deleteGlobalVariable: (id: number) => Promise<any>;
  saveGlobalVariable: (variable: any) => Promise<any>;
  editGlobalVariable: (variable: any) => Promise<any>;
  createClassifications: (text: string) => Promise<any>;
  fetchSingleGlobalVariable: (id: number) => Promise<any>;
  checkDeleteGlobalVariable: (id: number) => Promise<any>;
  fetchGlobalVariables: (params?: IServerPaginationParams) => Promise<any>;
}

interface IGlobalVariableState {
  id?: number;
  open: boolean;
  reset: boolean;
  rows: IVariable[];
  variable: IVariable;
  viewhistory: boolean;
  isopenConfirm: boolean;
  variables: IVariable[];
  openViewModal: boolean;
  warningMessage: string;
  viewCDhistory: boolean;
  filters: IVariableFilter;
  isFilterModalOpen: boolean;
  error: {
    name: IFieldValidation;
    type: IFieldValidation;
    option: IFieldValidation;
    description: IFieldValidation;
    variable_code: IFieldValidation;
    variable_values: IFieldValidation;
  };
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
}

class GlobalVariableList extends React.Component<
  IGlobalVariableProps,
  IGlobalVariableState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  private debouncedFetch: DebouncedFunc<
    (params?: IServerPaginationParams) => void
  >;

  public columns: ITableColumn[] = [
    {
      accessor: "name",
      Header: "Name",
      id: "name",
      sortable: false,
      Cell: (c) => <div>{c.value}</div>,
    },
    {
      accessor: "variable_code",
      Header: "Code",
      id: "variable_code",
      sortable: false,
      Cell: (c) => <div>{c.value}</div>,
    },
    {
      accessor: "description",
      Header: "Description",
      id: "description",
      sortable: false,
      Cell: (c) => <div>{c.value}</div>,
    },
    {
      accessor: "type",
      Header: "Type",
      id: "type",
      sortable: false,
      Cell: (c) => <div>{c.value}</div>,
    },
    {
      accessor: "option",
      Header: "Option",
      id: "option",
      sortable: false,
      Cell: (c) => <div>{c.value}</div>,
    },
    {
      accessor: "id",
      Header: "History",
      width: 55,
      sortable: false,
      Cell: (cell) => (
        <IconButton
          icon="version.svg"
          onClick={(e) => {
            this.onClickViewHistory(cell.original.id, e);
          }}
          title={"Show Variable History"}
        />
      ),
    },
    {
      accessor: "id",
      Header: "Actions",
      width: 140,
      sortable: false,
      Cell: (cell) => (
        <div>
          <EditButton onClick={(e) => this.onEditRowClick(cell.original, e)} />
          <DeleteButton onClick={(e) => this.onDeleteRowClick(cell, e)} />
        </div>
      ),
    },
  ];

  constructor(props: IGlobalVariableProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  getEmptyState = () => ({
    rows: [],
    open: false,
    reset: false,
    variables: [],
    viewhistory: false,
    isopenConfirm: false,
    openViewModal: false,
    warningMessage: null,
    viewCDhistory: false,
    isFilterModalOpen: false,
    variable: {
      name: "",
      description: "",
      variable_code: "",
      type: "",
      option: "",
      variable_values: [],
    },
    error: {
      name: { ...GlobalVariableList.emptyErrorState },
      description: { ...GlobalVariableList.emptyErrorState },
      variable_code: { ...GlobalVariableList.emptyErrorState },
      type: { ...GlobalVariableList.emptyErrorState },
      option: { ...GlobalVariableList.emptyErrorState },
      variable_values: { ...GlobalVariableList.emptyErrorState },
    },
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    filters: {
      type: [],
      option: [],
    },
  });

  componentDidMount() {
    if (this.props.types.length === 0) {
      this.props.fetchVariableTypes();
    }
  }

  componentDidUpdate(prevProps: IGlobalVariableProps) {
    if (this.props.variables && this.props.variables !== prevProps.variables) {
      this.setRows(this.props);
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchGlobalVariables(newParams);
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: IGlobalVariableProps) => {
    const customersResponse = nextProps.variables;
    const variables: any[] = customersResponse.results;
    const rows: any[] =
      variables &&
      variables.map((variable, index) => ({
        name: variable.name,
        variable_code: variable.variable_code,
        variable_values: variable.variable_values,
        type: variable.type,
        option: variable.option,
        description: variable.description,
        updated_on: variable.updated_on,
        created_on: variable.created_on,
        id: variable.id,
        index,
      }));

    this.setState((prevState) => ({
      reset: false,
      rows,
      variables,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onEditRowClick = (variables: IVariable, event: any) => {
    event.stopPropagation();
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.variable as IVariable) = variables;
    this.setState(newState);
  };

  onRowClick = (rowInfo) => {
    event.stopPropagation();
    this.setState({ openViewModal: true, variable: rowInfo.original });
  };

  onClickViewviewCDHistory = () => {
    this.setState({
      viewCDhistory: true,
    });
  };

  toggleviewCDHistoryPopup = (e, reverted: boolean) => {
    if (reverted) {
      this.fetchData(this.state.pagination.params);
    }
    this.setState({
      viewCDhistory: false,
    });
  };

  renderTopBar = () => {
    return (
      <div className="service-catalog-listing__actions ">
        <div className="header-panel">
          <Input
            field={{
              value: this.state.pagination.params.search,
              label: "",
              type: "SEARCH",
            }}
            width={11}
            name="searchString"
            onChange={this.onSearchStringChange}
            placeholder="Search"
            className="search"
          />
          <div className="actions">
            <SquareButton
              onClick={this.onClickViewviewCDHistory}
              content={
                <span>
                  <img alt="" src="/assets/icons/version.svg" />
                  History
                </span>
              }
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.variables && this.props.variables.length === 0
              }
              bsStyle={"primary"}
            />
            <SquareButton
              content={`+ Add Global Variable`}
              bsStyle={"primary"}
              onClick={() => this.clearPopUp(true)}
              className="save"
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.setState((prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              option: prevState.filters.option.join(),
              type: prevState.filters.type.join(),
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      (filters.type && filters.type.length > 0) ||
      (filters.option && filters.option.length > 0);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.type.length > 0 && (
          <div className="section-show-filters">
            <label>Type: </label>
            <SelectInput
              name="type"
              value={filters.type}
              onChange={this.onFilterChange}
              options={this.typeList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.option.length > 0 && (
          <div className="section-show-filters">
            <label>Option : </label>
            <SelectInput
              name="option"
              value={filters.option}
              onChange={this.onFilterChange}
              options={[
                { value: "OR", label: "OR" },
                { value: "AND", label: "AND" },
              ]}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };

  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.props.checkDeleteGlobalVariable(original.original.id).then((a) => {
      if (a.type === FETCH_G_VAR_SUCCESS) {
        const newState = cloneDeep(this.state);
        (newState.warningMessage as any) = this.getWarningMessage(
          a.response && a.response.associated_rules
        );
        (newState.isopenConfirm as boolean) = true;
        (newState.id as number) = original.original.id;
        this.setState(newState);
      }
    });
  }

  getWarningMessage = (rules: IRule[]) => {
    const message = (
      <div className="types-delete">
        {rules.length > 1 && (
          <p className="warning-message">
            {" "}
            {rules.length}
            {rules.length > 1 ? " rules" : " rule"} will be deleted associated
            with this variable.
          </p>
        )}
        <div className="body-section">
          <div className="device-details-circuit-info">
            {rules && rules.length > 0 && (
              <div className="document">
                <div className="tile-index">
                  <label> #</label>
                </div>
                <div className="tile">
                  <label> Name</label>
                </div>
                <div className="tile">
                  <label> Priority</label>
                </div>
              </div>
            )}
            {rules && rules.length > 0
              ? rules.map((key, fieldIndex) => {
                  return (
                    <div className="document" key={fieldIndex}>
                      <div className="tile-index">
                        <label />
                        <label>{fieldIndex + 1}</label>
                      </div>
                      <div className="tile">
                        <label />
                        <label>{key.name ? key.name : "N.A."}</label>
                      </div>
                      <div className="tile">
                        <label />
                        <label>{key.level ? key.level : "N.A."}</label>
                      </div>
                    </div>
                  );
                })
              : "No rules are associated."}
          </div>
        </div>
      </div>
    );

    return message;
  };

  onClickConfirm = () => {
    this.props.deleteGlobalVariable(this.state.id).then((a) => {
      if (a.type === FETCH_G_VAR_SUCCESS) {
        this.debouncedFetch();
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: null,
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: null,
    });
  };

  onClickViewHistory(id: number, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      id,
    });
  }

  onFiltersUpdate = (filters: IVariableFilter) => {
    if (filters) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            option: filters.option.join(),
            type: filters.type.join(),
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal();
    }
  };

  handleChangeValue = (data: string[]) => {
    this.setState((prev) => ({
      variable: {
        ...prev.variable,
        variable_values: data,
      },
    }));
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      variable: {
        ...prev.variable,
        [e.target.name]: e.target.value,
      },
    }));
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.variable as IVariable) = this.getEmptyState().variable;
    (newState.error as any) = this.getEmptyState().error;
    (newState.isopenConfirm as any) = false;
    (newState.warningMessage as any) = "";
    this.setState(newState);
  };

  validateForm() {
    const error: any = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.variable.name ||
      this.state.variable.name.trim().length === 0
    ) {
      error.name.errorState = "error";
      error.name.errorMessage = "Name cannot be empty";
      isValid = false;
    }
    if (
      !this.state.variable.variable_values ||
      this.state.variable.variable_values.length === 0
    ) {
      error.variable_values.errorState = "error";
      error.variable_values.errorMessage = "value cannot be empty";
      isValid = false;
    }
    if (
      !this.state.variable.description ||
      this.state.variable.description.trim().length === 0
    ) {
      error.description.errorState = "error";
      error.description.errorMessage = "Description cannot be empty";
      isValid = false;
    }
    if (
      !this.state.variable.variable_code ||
      this.state.variable.variable_code.trim().length === 0
    ) {
      error.variable_code.errorState = "error";
      error.variable_code.errorMessage =
        "Variable code sould be in format:  @{{YOUR_CODE}}";
      isValid = false;
    }
    if (
      this.state.variable.variable_code &&
      !AppValidators.isValidRuleVarible(this.state.variable.variable_code)
    ) {
      error.variable_code.errorState = "error";
      error.variable_code.errorMessage =
        "Variable code should be in format:  @{{YOUR_CODE}}";
      isValid = false;
    }
    if (!this.state.variable.type) {
      error.type.errorState = "error";
      error.type.errorMessage = "Please select Type";
      isValid = false;
    }
    if (!this.state.variable.option) {
      error.option.errorState = "error";
      error.option.errorMessage = "Please select Option";
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }

  onSaveClick = (
    variable: IVariable,
    reload: boolean = false,
    checkValid: boolean = false
  ) => {
    if (this.validateForm() || checkValid) {
      if (variable.id) {
        this.props.editGlobalVariable(variable).then((action) => {
          if (action.type === "FETCH_G_VAR_SUCCESS") {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true });
              this.debouncedFetch({ page: 1 });
            }
          }
          if (action.type === "FETCH_G_VAR_FAILURE") {
            this.setValidationErrors(action.errorList.data);
          }
        });
      } else {
        this.props.saveGlobalVariable(variable).then((action) => {
          if (action.type === "FETCH_G_VAR_SUCCESS") {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true });
              this.debouncedFetch({ page: 1 });
            }
          }
          if (action.type === "FETCH_G_VAR_FAILURE") {
            this.setValidationErrors(action.errorList.data);
          }
        });
      }
    }
  };

  setValidationErrors = (errorList: object) => {
    const newState: IGlobalVariableState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  typeList = () => {
    const typeList = this.props.types
      ? this.props.types.map((c) => ({
          value: c,
          label: c,
          disabled: false,
        }))
      : [];

    return typeList;
  };

  toggleViewModal = () => {
    this.setState({
      variable: this.getEmptyState().variable,
      openViewModal: !this.state.openViewModal,
    });
  };

  render() {
    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };

    return (
      <div className="variable-listing-parent">
        <div className="loader">
          <Spinner
            show={
              this.props.isFetching ||
              this.props.isFetchingList ||
              this.props.isFetchingTypes
            }
          />
        </div>
        <Table
          columns={this.columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
          message={this.state.warningMessage}
        />
        <ViewVariable
          show={this.state.openViewModal}
          onClose={this.toggleViewModal}
          variable={this.state.variable}
          isFetching={this.props.isFetching}
        />
        <VariableFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          types={this.props.types}
          prevFilters={this.state.filters}
        />
        <ViewVariableHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
          variableType="global"
        />
        <ModalBase
          show={this.state.open}
          onClose={() => this.clearPopUp(!this.state.open)}
          titleElement={` ${
            this.state.variable.id ? "Update" : "Add"
          } Variable`}
          bodyElement={
            <div className="col-md-12 body">
              <Input
                field={{
                  label: "Name",
                  type: "TEXT",
                  value: this.state.variable.name,
                  isRequired: true,
                }}
                width={12}
                name="name"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.name}
                placeholder={`Enter Name`}
              />
              <Input
                field={{
                  label: "Code",
                  type: "TEXT",
                  value: this.state.variable.variable_code,
                  isRequired: true,
                }}
                width={12}
                disabled={this.state.variable.id ? true : false}
                name="variable_code"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.variable_code}
                placeholder={`Enter Variable Code`}
              />
              <div
                className="email-tag  field-section
                field-section--error field-section--required
                 col-md-12 col-xs-12"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Value
                  </label>
                  <span className="field__label-required" />
                </div>
                <div>
                  {/* <TagsInput
                    value={
                      this.state.variable.variable_values
                        ? this.state.variable.variable_values
                        : []
                    }
                    onChange={(e) => this.handleChangeValue(e)}
                    inputProps={{
                      className: "react-tagsinput-input",
                      placeholder: "Enter Value",
                    }}
                    addOnBlur={true}
                  /> */}
                </div>
                {this.state.error.variable_values.errorMessage && (
                  <div className="field__error">
                    {this.state.error.variable_values.errorMessage}
                  </div>
                )}
              </div>
              <Input
                field={{
                  label: "Option",
                  type: "PICKLIST",
                  value: this.state.variable.option,
                  options: ["AND", "OR"],
                  isRequired: true,
                }}
                width={6}
                name="option"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.option}
                placeholder={`Select option`}
              />
              <Input
                field={{
                  label: "Type",
                  type: "PICKLIST",
                  value: this.state.variable.type,
                  options: this.typeList(),
                  isRequired: true,
                }}
                width={6}
                name="type"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.type}
                placeholder={`Select type`}
              />
              <Input
                field={{
                  label: "Description",
                  type: "TEXTAREA",
                  value: this.state.variable.description,
                  isRequired: true,
                }}
                width={12}
                name="description"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.description}
                placeholder={`Enter Description`}
              />
            </div>
          }
          footerElement={
            <div>
              <SquareButton
                content={`Cancel`}
                bsStyle={"default"}
                onClick={(e) => this.clearPopUp()}
                className="save"
              />
              <SquareButton
                content={`Save`}
                bsStyle={"primary"}
                onClick={(e) =>
                  this.onSaveClick(this.state.variable, true, false)
                }
                className="save"
              />
            </div>
          }
          className="add-edit-variable"
        />
        {this.state.viewCDhistory && (
          <ViewVariableCDHistory
            show={this.state.viewCDhistory}
            onClose={this.toggleviewCDHistoryPopup}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  variables: state.configuration.globalVariables,
  isFetching: state.configuration.isFetching,
  isFetchingTypes: state.configuration.isFetchingTypes,
  variable: state.configuration.globalVariable,
  types: state.configuration.types,
  isFetchingList: state.configuration.isFetchingList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchGlobalVariables: (params?: IServerPaginationParams) =>
    dispatch(fetchGlobalVariables(params)),
  fetchVariableTypes: () => dispatch(fetchVariableTypes()),
  fetchSingleGlobalVariable: (id: number) =>
    dispatch(fetchSingleGlobalVariable(id)),
  saveGlobalVariable: (variable: any) => dispatch(saveGlobalVariable(variable)),
  editGlobalVariable: (variable: any) => dispatch(editGlobalVariable(variable)),
  createClassifications: (text: string) =>
    dispatch(createClassifications(text)),
  deleteGlobalVariable: (id: number) => dispatch(deleteGlobalVariable(id)),
  checkDeleteGlobalVariable: (id: number) =>
    dispatch(checkDeleteGlobalVariable(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(GlobalVariableList);
