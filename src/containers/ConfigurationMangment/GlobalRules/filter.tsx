import React from 'react';

import SquareButton from '../../../components/Button/button';
import Select from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import './style.scss';

interface IRulesFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: IRulesFilter) => void;
  prevFilters?: IRulesFilter;
  levels: string[];
  classifications: IDLabelObject[];
  types: string[];
}

interface IRulesFilterState {
  filters: IRulesFilter;
}

export default class RulesFilter extends React.Component<
  IRulesFilterProps,
  IRulesFilterState
> {
  constructor(props: IRulesFilterProps) {
    super(props);

    this.state = {
      filters: {
        level: [],
        classification: [],
        is_enabled: []
      },
    };
  }

  componentDidUpdate(prevProps: IRulesFilterProps) {
    if (this.props.show !== prevProps.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }
  
  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };

  getBody = () => {
    const levels = this.props.levels
      ? this.props.levels.map(user => ({
          value: user,
          label: user,
          disabled: false,
        }))
      : [];

    const classifications =
      this.props.classifications && this.props.classifications.length > 0
        ? this.props.classifications.map(user => ({
            value: user.id,
            label: user.name,
          }))
        : [];

    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Level</label>
            <div className="field__input">
              <Select
                name="level"
                value={filters.level}
                onChange={this.onFilterChange}
                options={levels}
                multi={true}
                placeholder="Select Levels"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Classifications</label>
            <div className="field__input">
              <Select
                name="classification"
                value={filters.classification}
                onChange={this.onFilterChange}
                options={classifications}
                multi={true}
                placeholder="Select Classifications"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Status</label>
            <div className="field__input">
              <Select
                name="is_enabled"
                value={filters.is_enabled}
                onChange={this.onFilterChange}
                options={[
                  { label: 'Enabled', value: 'true' },
                  { label: 'Disabled', value: 'false' },
                ]}
                multi={true}
                placeholder="Select"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
