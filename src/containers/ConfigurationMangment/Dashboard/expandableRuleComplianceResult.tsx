import React from "react";
import _ from "lodash";
import Highlighter from "react-highlight-words";

import "./configresult.style.scss";

interface IRuleProps {
  config: IRule;
}

interface IRuleState {
  rule: IRule;
}

class ExpandableRuleComplianceResult extends React.Component<
  IRuleProps,
  IRuleState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IRuleProps) {
    super(props);
  }

  getVariables = (values) => {
    const variablesArray = [];
    if (values) {
      Object.keys(values).map((m) => {
        variablesArray.push({ key: m, value: values[m] });
      });
    }

    return variablesArray;
  };

  getVaribleDetails = (config: IRule, rule: any) => {
    return (
      <Highlighter
        highlightClassName="YourHighlightClass"
        searchWords={this.getVariables(config.variable_values).map(
          (m) => m.key
        )}
        autoEscape={true}
        textToHighlight={rule}
        key={Math.random()}
        highlightTag={({ children, highlightIndex }) => (
          <strong
            key={Math.random()}
            title={`
              Name :  ${_.get(config, `variable_values[${children}].name`, "")},
              code : ${_.get(
                config,
                `variable_values[${children}].variable_code`,
                ""
              )}
              Values : ${_.get(
                config,
                `variable_values[${children}].variable_values`,
                []
              ).join(", ")}
              Option : ${_.get(config, `variable_values[${children}].option`)}
              Type :  ${_.get(config, `variable_values[${children}].type`, "")},
              Description : ${_.get(
                config,
                `variable_values[${children}].description`,
                ""
              )}
            `}
            className="highlighted-text-2"
          >
            {children}
          </strong>
        )}
      />
    );
  };

  checkFirstCharWithVariable = (
    charector: string | any,
    k: number,
    add: string,
    remove: string,
    config: IRule
  ) => {
    if (typeof charector === "string") {
      switch (charector.charAt(0)) {
        case "+":
          return (
            <span key={k} className="add-green" title={add}>
              {this.getVaribleDetails(config, charector.slice(1))}
            </span>
          );
        case "-":
          return (
            <span key={k} className="add-red" title={remove}>
              {this.getVaribleDetails(config, charector.slice(1))}
            </span>
          );
        default:
          return (
            <span key={k}>{this.getVaribleDetails(config, charector)}</span>
          );
      }
    } else {
      switch (charector.command.charAt(0)) {
        case "+":
          return (
            <span key={k} className="add-green" title={add}>
              {this.getVaribleDetails(config, charector.command.slice(1))}
            </span>
          );
        case "-":
          return (
            <span
              key={k}
              className={
                charector.type === "partial_match" ? "add-orange" : "add-red"
              }
              title={remove}
            >
              {this.getVaribleDetails(config, charector.command.slice(1))}
            </span>
          );
        default:
          return (
            <span key={k}>
              {this.getVaribleDetails(config, charector.command)}
            </span>
          );
      }
    }
  };

  checkFirstChar = (
    charector: string,
    k: number,
    add: string,
    remove: string
  ) => {
    switch (charector.charAt(0)) {
      case "+":
        return null;
      case "-":
        return (
          <span key={k} className="add-red" title={remove}>
            {charector.slice(1)}
          </span>
        );
      default:
        return <span key={k}>{charector}</span>;
    }
  };

  showOtherMatches = (command: string) => {};

  getConfigScore = () => {
    return (
      <div className="new-violation-ui">
        <div className="view-rule-details-contents">
          <div className="rule-voilation">
            {this.props.config && this.props.config.function && (
              <div className="rule-name">
                <span className="bold-text">Function: </span>
                {this.props.config.function}
              </div>
            )}
            {this.props.config && this.props.config.multi_section_data && (
              <>
                <div className="multi-line-diff-view">
                  <div className="diff-view">
                    {this.props.config.multi_section_data.map((e, k) =>
                      this.checkFirstCharWithVariable(
                        e,
                        k,
                        "Valid Command",
                        "Invalid Command",
                        this.props.config
                      )
                    )}
                  </div>
                </div>
              </>
            )}

            {this.props.config &&
              this.props.config.valid_commands &&
              this.props.config.valid_commands.length > 0 &&
              this.props.config.valid_commands[0].length > 0 && (
                <div className="valid-commands">
                  <div className="heading-valid-config">VALID COMMANDS</div>
                  {this.props.config.valid_commands.map((c, i) => {
                    return c.map((d, j) => {
                      return (
                        <div
                          key={j}
                          title={`Line No. - ${d.line_number || "N.A."}`}
                          className="command"
                        >
                          {d.command ? d.command : d.detail}
                        </div>
                      );
                    });
                  })}
                </div>
              )}
            {this.props.config &&
              this.props.config.invalid_commands &&
              this.props.config.invalid_commands[0] &&
              this.props.config.invalid_commands[0].length > 0 && (
                <div className="invalid-commands compliance_dashboard-expandable">
                  <div className="heading-invalid-config">INVALID COMMANDS</div>
                  <div className="command-single">
                    <div className="original-config bold-text">Rule Detail</div>
                    <div className="original-config bold-text">
                      Partial Match Found{" "}
                    </div>
                  </div>
                  {this.props.config.invalid_commands.map((c, i) => {
                    return c.map((d, j) => {
                      return (
                        <div key={Math.random()} className="command-single">
                          <div
                            className={`original-config ${
                              d.line_number ? "line-numer" : ""
                            }`}
                            title={`${
                              d.line_number
                                ? `line number - ${d.line_number}`
                                : ""
                            }`}
                          >
                            {d.partial_matching_commands.length > 0 && (
                              <div className="match_text">&nbsp;Best Match</div>
                            )}
                            {this.getVaribleDetails(
                              this.props.config,
                              d.command
                            )}{" "}
                            {d.partial_matching_commands.length > 0 && (
                              <div
                                className="match_text other"
                                onClick={() => this.showOtherMatches(d.command)}
                              >
                                &nbsp;Other Matches
                              </div>
                            )}
                          </div>
                          <div className="partial_matching_commands">
                            {d.partial_matching_commands.length > 0 ? (
                              <div
                                key={0}
                                className="partial_matching_single best_match"
                                title={`Line No. - ${d
                                  .partial_matching_commands[0].line_number ||
                                  "N.A."}`}
                              >
                                {d.partial_matching_commands[0].command_diff.map(
                                  (f, k) =>
                                    this.checkFirstChar(
                                      f,
                                      k,
                                      "suggested value",
                                      "found value"
                                    )
                                )}
                                <br></br>
                                <span className="partial_matching_percent">
                                  {d.partial_matching_commands[0]
                                    .match_percentage &&
                                    `Match Percent: ${(
                                      d.partial_matching_commands[0]
                                        .match_percentage * 100
                                    ).toFixed(2)}%`}
                                </span>
                              </div>
                            ) : (
                              <span className="no_partial_matching_percent">
                                No partial matches found
                              </span>
                            )}
                            {d.partial_matching_commands.length > 0 &&
                              d.partial_matching_commands.map((e, k) => {
                                return (
                                  <div
                                    key={k}
                                    className="partial_matching_single other_match"
                                    title={`Line No. - ${e.line_number ||
                                      "N.A."}`}
                                  >
                                    {e.command_diff.map((f, k) =>
                                      this.checkFirstChar(
                                        f,
                                        k,
                                        "suggested value",
                                        "found value"
                                      )
                                    )}
                                    <br></br>
                                    <span className="partial_matching_percent">
                                      {e.match_percentage &&
                                        `Match Percent: ${(
                                          e.match_percentage * 100
                                        ).toFixed(2)}%`}
                                    </span>
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      );
                    });
                  })}
                </div>
              )}
          </div>
        </div>
      </div>
    );
  };

  render() {
    return this.getConfigScore();
  }
}

export default ExpandableRuleComplianceResult;
