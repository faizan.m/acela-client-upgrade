import React from "react";
// import Select from "react-select"; (New Component)
import { connect } from "react-redux";
import { Mention, MentionsInput } from "react-mentions";
import { DebouncedFunc, cloneDeep, debounce, get } from "lodash";
import {
  editCustomerRule,
  fetchRulesLevels,
  getCustomerRules,
  saveCustomerRule,
  fetchCustomerVars,
  customerRuleAssign,
  deleteCustomerRule,
  fetchRuleFunctions,
  fetchClassifications,
  createClassifications,
  fetchSingleCustomerRule,
  FETCH_CUS_RULE_SUCCESS,
  FETCH_COMP_CLASS_SUCCESS,
  FETCH_RULE_FUNCTION_SUCCESS,
} from "../../../actions/configuration";
import {
  fetchTypes,
  fetchTaskStatus,
  fetchManufacturers,
  fetchComplianceDevices,
  fetchDevicesCustomerUser,
  TASK_STATUS_SUCCESS,
  TASK_STATUS_FAILURE,
  FETCH_DEVICES_SUCCESS,
} from "../../../actions/inventory";
import {
  rerunConfigCompliance,
  RERUN_COMPLIANCE_SUCCESS,
} from "../../../actions/collector";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import Input from "../../../components/Input/input";
import { RuleDetails } from "../RulesContainer/RuleDetails";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import EditButton from "../../../components/Button/editButton";
import IconButton from "../../../components/Button/iconButton";
import TooltipCustom from "../../../components/Tooltip/tooltip";
import ModalBase from "../../../components/ModalBase/modalBase";
import SelectInput from "../../../components/Input/Select/select";
import DeleteButton from "../../../components/Button/deleteButton";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import IOSVersionCustomTooltip from "../../../components/Tooltip/iosVersionTooltip";
import RuleExceptionCustomTooltip from "../../../components/Tooltip/ruleExceptionTooltip";
import ViewRuleCDHistory from "./../RulesContainer/rulesCDHistory";
import ViewRuleHistory from "../RulesContainer/ruleHistory";
import RulesFilter from "./filter";
import { searchInFields } from "../../../utils/searchListUtils";
import { commonFunctions } from "../../../utils/commonFunctions";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import "../../../commonStyles/filtersListing.scss";
import "./style.scss";

interface ICustomerRulesListingProps {
  rules: any;
  rule: IRule;
  editId?: number;
  types: string[];
  levels: string[];
  user: ISuperUser;
  customerId: number;
  searchName?: string;
  isFetching: boolean;
  isFetchingList: boolean;
  isPostingBatch: boolean;
  isFetchingDevice: boolean;
  customers: ICustomerShort[];
  customerVariables: IVariable[];
  customerProfile: Icustomer;
  manufacturers: IDLabelObject[];
  classifications: IDLabelObject[];
  fetchTypes: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  fetchRulesLevels: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  fetchRuleFunctions: () => Promise<any>;
  fetchManufacturers: TFetchManufacturers;
  fetchTaskStatus: (id: number) => Promise<any>;
  rerunConfigCompliance: (data: any) => Promise<any>;
  fetchComplianceDevices: (id: number) => Promise<any>;
  createClassifications: (text: string) => Promise<any>;
  saveRule: (custId: number, rule: IRule) => Promise<any>;
  editRule: (custId: number, rule: IRule) => Promise<any>;
  deleteRule: (custId: number, id: number) => Promise<any>;
  fetchDevicesCustomerUser: (show: boolean) => Promise<any>;
  fetchClassifications: (user_type?: string) => Promise<any>;
  fetchSingleRule: (custId: number, id: number) => Promise<any>;
  fetchRules: (id: number, params?: ISPRulesFilterParams) => Promise<any>;
  customerRuleAssign: (
    custId: number,
    id: number,
    status: string
  ) => Promise<any>;
  fetchCustomerVars: (
    custId: number,
    params?: IServerPaginationParams
  ) => Promise<any>;
}

interface ICustomerRulesListingState {
  rule: IRule;
  id?: number;
  open: boolean;
  rows: IRule[];
  rules: IRule[];
  reset: boolean;
  status: string;
  isSuccess: boolean;
  devicesShort: any[];
  viewhistory: boolean;
  searchDevice: string;
  filters: IRulesFilter;
  isopenConfirm: boolean;
  viewCDhistory: boolean;
  selectedRows: number[];
  showDeviceList: boolean;
  openViewModalRule: boolean;
  isFilterModalOpen: boolean;
  isLoadingDeviceList: boolean;
  devicesList: any[];
  showComplianceRerunBtn: boolean;
  functionList: IPickListOptions[];
  error: {
    detail: IFieldValidation;
    level: IFieldValidation;
    classification: IFieldValidation;
    customers_assigned: IFieldValidation;
    applies_to: IFieldValidation;
    applies_to_manufacturers: IFieldValidation;
    name: IFieldValidation;
    description: IFieldValidation;
    function: IFieldValidation;
    to_ignore_devices: IFieldValidation;
    to_include_devices: IFieldValidation;
    apply_not: IFieldValidation;
    exceptions: IFieldValidation;
    ios_version: IFieldValidation;
  };
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: ISPRulesFilterParams;
  };
}

class CustomerRulesList extends React.Component<
  ICustomerRulesListingProps,
  ICustomerRulesListingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  private debouncedFetch: DebouncedFunc<
    (params?: IServerPaginationParams) => void
  >;

  constructor(props: ICustomerRulesListingProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  getEmptyState = () => ({
    rows: [],
    rules: [],
    status: "",
    open: false,
    reset: false,
    devicesList: [],
    functionList: [],
    devicesShort: [],
    selectedRows: [],
    isSuccess: false,
    searchDevice: "",
    viewhistory: false,
    isopenConfirm: false,
    viewCDhistory: false,
    showDeviceList: false,
    openViewModalRule: false,
    isFilterModalOpen: false,
    isLoadingDeviceList: false,
    showComplianceRerunBtn: false,
    rule: {
      detail: "",
      level: "",
      classification: 0,
      description: "",
      applies_to: [],
      applies_to_manufacturers: [],
      name: "",
      function: "",
      is_enabled: true,
      apply_not: false,
      to_ignore_devices: [],
      to_include_devices: [],
      exceptions: "",
      ios_version: [],
    },
    error: {
      detail: { ...CustomerRulesList.emptyErrorState },
      level: { ...CustomerRulesList.emptyErrorState },
      classification: { ...CustomerRulesList.emptyErrorState },
      customers_assigned: { ...CustomerRulesList.emptyErrorState },
      applies_to: { ...CustomerRulesList.emptyErrorState },
      applies_to_manufacturers: { ...CustomerRulesList.emptyErrorState },
      name: { ...CustomerRulesList.emptyErrorState },
      description: { ...CustomerRulesList.emptyErrorState },
      function: { ...CustomerRulesList.emptyErrorState },
      to_ignore_devices: { ...CustomerRulesList.emptyErrorState },
      to_include_devices: { ...CustomerRulesList.emptyErrorState },
      apply_not: { ...CustomerRulesList.emptyErrorState },
      ios_version: { ...CustomerRulesList.emptyErrorState },
      exceptions: { ...CustomerRulesList.emptyErrorState },
    },
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    filters: {
      level: [],
      classification: [],
      status: [],
      is_global: [],
      is_enabled: [],
    },
  });

  isAccessAvailable = (props: ICustomerRulesListingProps) => {
    let isAccess: boolean = false;
    let writeAccess = get(
      props,
      "customerProfile.config_compliance_service_write_allowed",
      false
    );
    let isProvider = get(props, "user.type") === "provider" ? true : false;

    isAccess = isProvider ? true : writeAccess;

    return isAccess;
  };

  public columns: ITableColumn[] = [
    {
      accessor: "level",
      Header: "Level",
      id: "level_int",
      width: 70,
      sortable: true,
      Cell: (cell) => (
        <div className="icons-template">
          {cell.original.level && (
            <img
              src={`/assets/icons/l-${cell.original.level}.svg`}
              alt=""
              title={cell.original.level}
              className="status-svg-images"
            />
          )}
        </div>
      ),
    },
    {
      accessor: "is_global",
      Header: "Rule Type",
      width: 80,
      sortable: false,
      id: "is_global",
      Cell: (cell) => (
        <div className="icons-template-1">
          {cell.original.is_global && (
            <img
              src={`/assets/icons/global-green.svg`}
              alt=""
              title={`Global Rule`}
              className="info-svg-images"
            />
          )}
          {cell.original.is_global === false && (
            <img
              src={`/assets/icons/global-grey.svg`}
              alt=""
              title={`Customer Rule`}
              className="info-svg-images"
            />
          )}
        </div>
      ),
    },
    {
      accessor: "name",
      Header: "Name",
      id: "name",
      sortable: false,
      Cell: (c) => <div>{c.value}</div>,
    },
    {
      accessor: "applies_to",
      Header: "Applies To",
      sortable: false,
      id: "applies_to",
      Cell: (c) => <div>{c.value && c.value.join()}</div>,
    },
    {
      accessor: "detail",
      Header: "Detail",
      width: 80,
      sortable: false,
      id: "detail",
      Cell: (cell) => (
        <div className="icons-template-1">
          {cell.value && cell.value.length > 0 && (
            <img
              src={`/assets/new-icons/info.svg`}
              alt=""
              title={`${cell.value} `}
              className="info-svg-images"
            />
          )}
        </div>
      ),
    },
    {
      accessor: "description",
      Header: "Notes",
      width: 80,
      sortable: false,
      id: "description",
      Cell: (cell) => (
        <div className="icons-template-1">
          {cell.value && cell.value.length > 0 && (
            <img
              src={`/assets/icons/notepad.png`}
              alt=""
              title={`${cell.value} `}
              className="info-svg-images"
            />
          )}
        </div>
      ),
    },

    {
      accessor: "is_enabled",
      id: "is_enabled",
      sortable: false,
      Header: "Status",
      Cell: (cell) => (
        <div
          className={`status status--${
            cell.original.is_assigned && cell.original.is_enabled === true
              ? "Enabled"
              : "Disabled"
          }`}
          title={`Assigned-${cell.original.is_assigned} - Enable-${cell.original.is_enabled}`}
        >
          {cell.original.is_assigned && cell.original.is_enabled
            ? "Assigned"
            : "UnAssigned"}
        </div>
      ),
    },
    {
      accessor: "updated_by",
      Header: "Updated By",
      id: "updated_by",
      sortable: false,
      Cell: (cell) => (
        <div>{cell.original.updated_by && cell.original.updated_by.name}</div>
      ),
    },
    {
      accessor: "updated_on",
      Header: "Assign/Unassign",
      id: "updated_on",
      width: 140,
      sortable: true,
      Cell: (cell) => (
        <div>
          {cell.original.is_global === true &&
            this.isAccessAvailable(this.props) &&
            cell.original.is_enabled && (
              <SquareButton
                content={` ${
                  cell.original.is_assigned ? "Unassign" : "Assign"
                } `}
                bsStyle={"primary"}
                onClick={(e) => this.onAssigneRowClick(cell, e)}
                className="save"
              />
            )}
          {cell.original.is_global === false &&
            this.isAccessAvailable(this.props) && (
              <SquareButton
                content={` ${
                  cell.original.is_assigned ? "Unassign" : "Assign"
                } `}
                bsStyle={"primary"}
                onClick={(e) => this.onAssigneRowClick(cell, e)}
                className="save"
              />
            )}
          {cell.original.is_global === true &&
            this.isAccessAvailable(this.props) === false &&
            `${cell.original.is_assigned ? "Assigned" : "Unassigned"} `}
        </div>
      ),
    },
    {
      accessor: "id",
      Header: "History",
      width: 55,
      sortable: false,
      Cell: (cell) => (
        <IconButton
          icon="version.svg"
          onClick={(e) => {
            this.onClickViewHistory(cell.original.id, e);
          }}
          title={"Show Rule History"}
        />
      ),
    },
    {
      accessor: "id",
      Header: "Actions",
      width: 140,
      sortable: false,
      Cell: (cell) => (
        <div className="assigned-unassigned">
          <div>
            {this.isAccessAvailable(this.props) && (
              <EditButton
                onClick={(e) => this.onEditRowClick(cell.original, e)}
              />
            )}
            {this.isAccessAvailable(this.props) &&
              cell.original.is_global === false && (
                <>
                  <DeleteButton
                    type="cloned"
                    title="Clone Rule"
                    onClick={(e) => this.onCloneClick(cell.original, e)}
                  />
                  <DeleteButton
                    onClick={(e) => this.onDeleteRowClick(cell, e)}
                  />
                </>
              )}
          </div>
        </div>
      ),
    },
  ];

  toggleViewModalRule = () => {
    this.setState({
      rule: this.getEmptyState().rule,
      openViewModalRule: !this.state.openViewModalRule,
    });
  };

  componentDidMount() {
    if (this.props.searchName) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search: this.props.searchName,
          },
        },
      }));
    }
    if (this.props.editId) {
      this.props
        .fetchSingleRule(this.props.customerId, this.props.editId)
        .then((action) => {
          if (action.type === FETCH_CUS_RULE_SUCCESS) {
            const response: IRule = action.response;
            const rule = {
              ...response,
              exceptions: response.exceptions.join("\n"),
              applies_to_ios_version: (response.applies_to_ios_version as string[]).join(
                ", "
              ),
            };
            this.setState({
              open: true,
              rule: rule,
            });
          }
        });
    }
    if (!this.props.manufacturers) {
      this.props.fetchManufacturers();
    }
    if (this.props.levels.length === 0) {
      this.props.fetchRulesLevels();
    }
    if (this.props.classifications.length === 0) {
      this.props.fetchClassifications(get(this.props, "user.type"));
    }
    if (this.props.types === null) {
      this.props.fetchTypes();
    }
    this.props.fetchCustomerVars(this.props.customerId, {
      pagination: false,
    });
    this.fetchRuleFunction();
    this.fetchDevicesData();
  }

  componentDidUpdate(prevProps: ICustomerRulesListingProps) {
    if (this.props.rules && this.props.rules !== prevProps.rules) {
      this.setRows(this.props);
    }
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchRules(this.props.customerId);
      this.props.fetchCustomerVars(this.props.customerId, {
        pagination: false,
      });
    }
  }

  fetchDevicesData = () => {
    const {
      user,
      customerId,
      fetchDevicesCustomerUser,
      fetchComplianceDevices,
    } = this.props;

    if (customerId) {
      fetchComplianceDevices(customerId).then((action) => {
        if (action.type === FETCH_DEVICES_SUCCESS && action.response) {
          const devicesShort = action.response.map((c) => ({
            value: c.id,
            label: c.device_name,
            disabled: false,
          }));
          this.setState({ devicesShort, devicesList: action.response });
        }
      });
    } else {
      user &&
        fetchDevicesCustomerUser(false).then((action) => {
          if (action.type === FETCH_DEVICES_SUCCESS && action.response) {
            const devicesShort = action.response.map((c) => ({
              value: c.id,
              label: c.device_name,
              disabled: false,
            }));
            this.setState({ devicesShort, devicesList: action.response });
          }
        });
    }
  };

  fetchRuleFunction = () => {
    this.props.fetchRuleFunctions().then((action) => {
      if (action.type === FETCH_RULE_FUNCTION_SUCCESS && action.response) {
        const functionList = action.response.map((c) => ({
          value: c.key,
          label: c.key,
          disabled: false,
        }));
        this.setState({ functionList });
      }
    });
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));
    if (
      get(this.props, "user.type") === "provider" &&
      this.props.customerId
    ) {
      this.props.fetchRules(this.props.customerId, newParams);
    }
    if (get(this.props, "user.type") === "customer") {
      this.props.fetchRules(null, newParams);
    }
  };
  fetcRules = () => {
    const search = this.state.pagination.params.search;
    const page = 1;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
            page,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;
      newParams.page = 1;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({ page: 1 });
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: ICustomerRulesListingProps) => {
    const customersResponse = nextProps.rules;
    const rules: any[] = customersResponse.results;
    const rows: IRule[] = rules.map((rule, index) => ({
      classification: rule.classification,
      classification_name: rule.classification_name,
      customers_assigned: rule.customers_assigned,
      detail: rule.detail,
      level: rule.level,
      updated_on: rule.updated_on,
      updated_by: rule.updated_by,
      created_on: rule.created_on,
      name: rule.name,
      description: rule.description,
      applies_to: rule.applies_to,
      applies_to_manufacturers: rule.applies_to_manufacturers,
      is_assigned: rule.is_assigned,
      is_enabled: rule.is_enabled,
      is_global: rule.is_global,
      created_by: rule.created_by,
      id: rule.id,
      function: rule.function,
      to_ignore_devices: rule.to_ignore_devices,
      to_include_devices: rule.to_include_devices,
      apply_not: rule.apply_not,
      exceptions: rule.exceptions.join("\n"),
      applies_to_ios_version: rule.applies_to_ios_version.join(", "),
      index,
    }));

    this.setState((prevState) => ({
      reset: false,
      rows,
      rules,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: null,
    });
  };

  onClickViewHistory(id: number, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      id,
    });
  }

  onEditRowClick = (rule: IRule, event: any) => {
    event.stopPropagation();
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.rule as IRule) = rule;
    this.setState(newState);
  };

  onCloneClick = (rule: IRule, event: any) => {
    event.stopPropagation();
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    (newState.rule as IRule) = rule;
    (newState.rule.id as number) = 0;
    (newState.rule.name as string) = newState.rule.name + " (cloned)";
    this.setState(newState);
  };

  onRowClick = (rowInfo) => {
    // tslint:disable-next-line: max-line-lengthrules
    this.setState({
      openViewModalRule: true,
      rule: this.state.rows[rowInfo.original.index],
    });
  };

  onClickViewviewCDHistory = () => {
    this.setState({
      viewCDhistory: true,
    });
  };
  toggleviewCDHistoryPopup = (e, reverted) => {
    if (reverted) {
      this.fetchData(this.state.pagination.params);
    }
    this.setState({
      viewCDhistory: false,
    });
  };

  onClickReRunCompliance = () => {
    const { selectedRows } = this.state;

    const data = {
      customer_id: this.props.customerId,
      rule_ids: selectedRows,
    };

    this.props.rerunConfigCompliance(data).then((a) => {
      if (a.type === RERUN_COMPLIANCE_SUCCESS) {
        if (a.response && a.response.task_id) {
          this.fetchTaskStatus(a.response.task_id, "Re-run compliance ");
        }
      }
    });
  };

  fetchTaskStatus = (taskId: number, message: string) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then((a) => {
        if (a.type === TASK_STATUS_SUCCESS) {
          if (
            a.response &&
            a.response.status &&
            a.response.status === "SUCCESS"
          ) {
            this.props.addSuccessMessage(`${message} completed`);
            this.setState({ isSuccess: true });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "FAILURE"
          ) {
            this.props.addErrorMessage(`${message} failed`);
            this.setState({ isSuccess: true });
          }
          if (
            a.response &&
            a.response.status &&
            a.response.status === "PENDING"
          ) {
            setTimeout(() => {
              this.fetchTaskStatus(taskId, message);
              // tslint:disable-next-line:align
            }, 10000);
          }
        }
        if (a.type === TASK_STATUS_FAILURE) {
          this.props.addErrorMessage(a.errorList.data.import_file);
        }
      });
    }
  };

  renderTopBar = () => {
    return (
      <div className="service-catalog-listing__actions ">
        <div className="header-panel">
          <Input
            field={{
              value: this.state.pagination.params.search,
              label: "",
              type: "SEARCH",
            }}
            width={11}
            name="searchString"
            onChange={this.onSearchStringChange}
            placeholder="Search"
            className="search"
          />
          <div className="header-actions">
            {this.state.showComplianceRerunBtn && (
              <SquareButton
                onClick={this.onClickReRunCompliance}
                content={
                  <span>
                    <img alt="" src="/assets/icons/reload.png" />
                    &nbsp; Re-Run Compliance
                  </span>
                }
                bsStyle={"primary"}
                disabled={this.props.isPostingBatch}
                className={`btn-img ${
                  this.props.isPostingBatch ? "rotate-img-running" : ""
                }`}
              />
            )}
            <SquareButton
              onClick={this.onClickViewviewCDHistory}
              content={
                <span>
                  <img alt="" src="/assets/icons/version.svg" />
                  History
                </span>
              }
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={this.props.rules && this.props.rules.length === 0}
              bsStyle={"primary"}
            />
            {this.isAccessAvailable(this.props) && (
              <SquareButton
                content={`+ Add Rule`}
                bsStyle={"primary"}
                onClick={() => this.clearPopUp(true)}
                className="save"
                disabled={
                  get(this.props, "user.type") === "provider" &&
                  !this.props.customerId
                }
              />
            )}
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.setState((prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              is_global: prevState.filters.is_global.join(),
              level: prevState.filters.level.join(),
              is_enabled: prevState.filters.is_enabled.join(),
              classification: prevState.filters.classification.join(),
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      (filters.level && filters.level.length > 0) ||
      (filters.classification && filters.classification.length > 0) ||
      (filters.is_enabled && filters.is_enabled.length > 0) ||
      (filters.is_global && filters.is_global.length > 0);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.level.length > 0 && (
          <div className="section-show-filters">
            <label>Level: </label>
            <SelectInput
              name="level"
              value={filters.level}
              onChange={this.onFilterChange}
              options={this.levelList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.classification.length > 0 && (
          <div className="section-show-filters">
            <label>Classification: </label>
            <SelectInput
              name="classification"
              value={filters.classification}
              onChange={this.onFilterChange}
              options={this.classificationList()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.is_global.length > 0 && (
          <div className="section-show-filters">
            <label>Is Global: </label>
            <SelectInput
              name="is_global"
              value={filters.is_global}
              onChange={this.onFilterChange}
              options={[
                { label: "Global", value: "true" },
                { label: "Customer", value: "false" },
              ]}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.is_enabled.length > 0 && (
          <div className="section-show-filters">
            <label>Status: </label>
            <SelectInput
              name="is_enabled"
              value={filters.is_enabled}
              onChange={this.onFilterChange}
              options={[
                { label: "Enabled", value: "true" },
                { label: "Disabled", value: "false" },
              ]}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };

  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  rangeVariables = () => {
    const variablesList = [
      "#NUM_RANGE(1-10)",
      "#VAR_OPTION_OR(value1,value2,value3)",
      "#VAR_OPTION_AND(value1, value2,value3) ",
    ];
    const globalVariables = variablesList.map((c) => ({
      id: c,
      display: c,
    }));

    return globalVariables;
  };

  onClickConfirm = () => {
    this.props
      .deleteRule(this.props.customerId, this.state.id)
      .then((action) => {
        if (action.type === FETCH_CUS_RULE_SUCCESS) {
          this.toggleConfirmOpen();
          this.setState({
            reset: true,
            status: "",
          });
          this.debouncedFetch({ page: 1 });
        }
      });
  };

  onAssigneRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
      status: original.original.is_assigned ? "unassign" : "assign",
    });
  }

  onClickConfirmAssign = () => {
    this.props
      .customerRuleAssign(
        this.props.customerId,
        this.state.id,
        this.state.status
      )
      .then((action) => {
        if (action.type === FETCH_CUS_RULE_SUCCESS) {
          this.debouncedFetch();
          this.toggleConfirmOpen();
        }
      });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: null,
      status: "",
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IRulesFilter) => {
    if (filters) {
      this.setState(
        (prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              is_global: filters.is_global.join(),
              level: filters.level.join(),
              is_enabled: filters.is_enabled.join(),
              classification: filters.classification.join(),
            },
          },
          filters,
        }),
        () => {
          this.fetcRules();
        }
      );
      this.toggleFilterModal();
    }
  };

handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        [e.target.name]: e.target.value,
      },
    }));
  };

  handleChangeCheckBox = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        [e.target.name]: e.target.checked,
      },
    }));
  };

  handleChangeDetails = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        detail: e.target.value,
      },
    }));
  };

  handleChangeType = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prev) => ({
      rule: {
        ...prev.rule,
        is_enabled: JSON.parse(event.target.value),
      },
    }));
  };

  handleChangeClassification = (e) => {
    const newState = cloneDeep(this.state);
    (newState.rule.classification as any) = e.value;
    this.setState(newState);
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.rule as IRule) = this.getEmptyState().rule;
    (newState.error as any) = this.getEmptyState().error;
    (newState.isopenConfirm as any) = false;
    this.setState(newState);
  };

  manufacturers = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((c) => ({
          value: c.label,
          label: c.label,
        }))
      : [];

    return manufacturers;
  };

  validateForm() {
    const newState: ICustomerRulesListingState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.rule.name || this.state.rule.name.trim().length === 0) {
      newState.error.name.errorState = "error";
      newState.error.name.errorMessage = "Name cannot be empty";
      isValid = false;
    }
    if (!this.state.rule.detail || this.state.rule.detail.trim().length === 0) {
      newState.error.detail.errorState = "error";
      newState.error.detail.errorMessage = "Text cannot be empty";
      isValid = false;
    }
    if (!this.state.rule.level) {
      newState.error.level.errorState = "error";
      newState.error.level.errorMessage = "Please select level";
      isValid = false;
    }
    if (!this.state.rule.classification) {
      newState.error.classification.errorState = "error";
      newState.error.classification.errorMessage =
        "Please select classification";
      isValid = false;
    }

    if (
      !this.state.rule.applies_to ||
      (this.state.rule.applies_to && this.state.rule.applies_to.length === 0)
    ) {
      newState.error.applies_to.errorState = "error";
      newState.error.applies_to.errorMessage = "Please select applies to";
      isValid = false;
    }
    if (
      !this.state.rule.applies_to_manufacturers ||
      (this.state.rule.applies_to_manufacturers &&
        this.state.rule.applies_to_manufacturers.length === 0)
    ) {
      newState.error.applies_to_manufacturers.errorState =
        "error";
      newState.error.applies_to_manufacturers.errorMessage =
        "Please select applies to manufacturers";
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  onSaveClick = (
    rule: IRule,
    reload: boolean = false,
    checkValid: boolean = false
  ) => {
    if (this.validateForm() || checkValid) {
      const regex = /#RULE#/gi;
      rule.detail = rule.detail.replace(regex, "");
      rule.exceptions = rule.exceptions.split("\n");

      rule.applies_to_ios_version =
        rule.applies_to_ios_version === ""
          ? []
          : rule.applies_to_ios_version &&
            (rule.applies_to_ios_version as string).split(",");

      if (rule.id) {
        this.props.editRule(this.props.customerId, rule).then((action) => {
          if (action.type === "FETCH_CUS_RULE_SUCCESS") {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true });
              this.debouncedFetch({ page: 1 });
            }
          }
          if (action.type === "FETCH_CUS_RULE_FAILURE") {
            this.setValidationErrors(action.errorList.data);
          }
        });
      } else {
        this.props.saveRule(this.props.customerId, rule).then((action) => {
          if (action.type === "FETCH_CUS_RULE_SUCCESS") {
            this.clearPopUp();
            if (reload) {
              this.setState({ reset: true });
              this.debouncedFetch({ page: 1 });
            }
          }
          if (action.type === "FETCH_CUS_RULE_FAILURE") {
            this.setValidationErrors(action.errorList.data);
          }
        });
      }
    }
  };

  setValidationErrors = (errorList: object) => {
    const newState: ICustomerRulesListingState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  onNewOptionClick = (option) => {
    const { className, ...newOption } = option;
    const name = newOption.label;
    this.props.createClassifications(name).then((action) => {
      if (action.type === FETCH_COMP_CLASS_SUCCESS) {
        this.props.fetchClassifications(get(this.props, "user.type"));
      }
    });
  };

  levelList = () => {
    const levelList = this.props.levels
      ? this.props.levels.map((c) => ({
          value: c,
          label: c,
          disabled: false,
        }))
      : [];

    return levelList;
  };

  classificationList = () => {
    const classification =
      this.props.classifications.length > 0
        ? this.props.classifications.map((c) => ({
            value: c.id,
            label: c.name,
            disabled: false,
          }))
        : [];

    return classification;
  };

  typeList = () => {
    const types = this.props.types
      ? this.props.types.filter((x) => x !== "")
      : [];
    const typess =
      types && types.length > 0
        ? types.map((c) => ({
            value: c,
            label: c,
            disabled: false,
          }))
        : [];

    return typess;
  };

  variableList = () => {
    const globalVariables =
      this.props.customerVariables && this.props.customerVariables.length > 0
        ? this.props.customerVariables.map((c) => ({
            id: c.id,
            display: c.variable_code,
          }))
        : [];

    return globalVariables;
  };

  onRowsToggle = (selectedRows) => {
    this.setState({
      selectedRows,
      showComplianceRerunBtn: selectedRows.length > 0,
    });
  };

  toggleShowDeviceList = () => {
    this.setState({
      showDeviceList: !this.state.showDeviceList,
    });
  };

  render() {
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.debouncedFetch,
      reset: this.state.reset,
    };

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: "id",
      onRowsToggle: this.onRowsToggle,
    };

    const columns: any = [
      {
        accessor: "status",
        Header: "Device Status",
        sortable: false,
        width: 120,
        Cell: (status) => (
          <div className={`device-status-icons`}>
            {status.original.contract_status === "Active" && (
              <img
                src={`/assets/icons/verified.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.contract_status === "Expiring" && (
              <img
                src={`/assets/icons/caution.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.contract_status === "N.A." && (
              <img
                src={`/assets/icons/cancel.svg`}
                className="status-svg-images"
                alt=""
                title={"Not covered"}
              />
            )}
            {status.original.contract_status === "Expired" && (
              <img
                src={`/assets/icons/HIGH.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {((status.original.is_managed &&
              status.original.status !== "Active") ||
              (!status.original.is_managed &&
                status.original.status === "Active")) && (
              <img
                src={`/assets/icons/monitor-grey.png`}
                alt=""
                title={`${
                  status.value === "Active" ? "In Service " : "Decommissioned"
                } & ${
                  status.original.is_managed === true
                    ? "Monitoring"
                    : "Not Monitoring"
                }`}
              />
            )}
            {status.original.is_managed &&
              status.original.status === "Active" && (
                <img
                  src={`/assets/icons/monitor-green.png`}
                  alt=""
                  title={"In Service & Monitored"}
                />
              )}
            {!status.original.is_managed &&
              status.original.status !== "Active" && (
                <img
                  src={`/assets/icons/monitor-red.png`}
                  alt=""
                  title={"Decommissioned"}
                />
              )}
            {(status.original.migration_info === undefined ||
              status.original.migration_info === null) && (
              <img
                src={`/assets/icons/notification-gray.svg`}
                className="status-svg-images"
                alt=""
                title={`Replacement not available`}
              />
            )}
            {status.original.migration_info !== undefined &&
              status.original.migration_info !== null && (
                <img
                  src={`/assets/icons/notification.svg`}
                  className="status-svg-images"
                  alt=""
                  title={`Replacement available`}
                />
              )}
            {status.original.host_name &&
              status.original.host_name !== undefined &&
              status.original.host_name !== null && (
                <img
                  src={`/assets/icons/plug-green.svg`}
                  className="status-svg-images"
                  alt=""
                  title={`IP available`}
                />
              )}
            {(!status.original.host_name ||
              status.original.host_name === undefined ||
              status.original.host_name === null) && (
              <img
                src={`/assets/icons/plug.svg`}
                className="status-svg-images"
                alt=""
                title={`No IP available`}
              />
            )}
          </div>
        ),
      },
      {
        accessor: "device_name",
        Header: "Device Name",
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "serial_number",
        Header: "Serial",
        Cell: (s) => (
          <>
            <a
              title={`Contract End: ${
                s.original.expiration_date
                  ? fromISOStringToFormattedDate(s.original.expiration_date)
                  : " N.A."
              },
              End of Support: ${
                s.original.endOfSupport
                  ? fromISOStringToFormattedDate(s.original.endOfSupport)
                  : " N.A."
              },
              End of Life: ${
                s.original.EOL_date
                  ? fromISOStringToFormattedDate(s.original.EOL_date)
                  : " N.A."
              },
              Manufacturer: ${
                s.original.manufacturer_name
                  ? s.original.manufacturer_name
                  : " N.A."
              },
              ${s.original.host_name ? `IP : ${s.original.host_name}` : ""}`}
            >
              {s.original.serial_number ? s.original.serial_number : " N.A."}
            </a>
          </>
        ),
      },

      {
        accessor: "model_number",
        Header: "Model",
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "device_type",
        Header: "Type",
        width: 100,
        Cell: (cell) => <div> {`${cell.value ? cell.value : " N.A."}`}</div>,
      },
      {
        accessor: "site",
        Header: "Site",
        Cell: (cell) => (
          <div> {`${cell.value ? cell.value : "Not Selected"}`}</div>
        ),
      },
      {
        accessor: "service_contract_number",
        Header: "Contract",
        width: 100,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value
                ? cell.value
                : cell.original.expiration_date
                ? "Unknown"
                : "Not Covered"
            }`}
          </div>
        ),
      },
      {
        accessor: "expiration_date",
        Header: "End date",
        width: 100,
        Cell: (s) => (
          <div>
            {" "}
            {s.original.expiration_date
              ? fromISOStringToFormattedDate(s.original.expiration_date)
              : " N.A."}
          </div>
        ),
      },
    ];

    const devicesListCount = this.state.devicesList.filter((x) =>
      this.state.rule.applies_to.includes(x.device_type)
    ).length;

    return (
      <div className="rules-listing-parent">
        <div className="loader">
          <Spinner show={this.props.isFetching || this.props.isFetchingList} />
        </div>
        <Table
          columns={this.columns}
          rows={this.state.rows}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingList}
          rowSelection={rowSelectionProps}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={
            this.state.status ? this.onClickConfirmAssign : this.onClickConfirm
          }
          isLoading={this.props.isFetching}
          title={
            this.state.status
              ? ` Are you sure, want to ${this.state.status} the Rule for the customer ?`
              : ""
          }
        />
        <RulesFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          levels={this.props.levels}
          classifications={this.props.classifications}
          types={this.props.types}
          prevFilters={this.state.filters}
        />
        <RuleDetails
          show={this.state.openViewModalRule}
          onClose={this.toggleViewModalRule}
          rule={this.state.rule}
          devices={this.state.devicesShort}
        />
        <RightMenu
          show={this.state.open}
          onClose={() => this.clearPopUp(!this.state.open)}
          titleElement={
            <div className="right-popup-header">
              {" "}
              {this.state.rule.id ? "Update" : "Add"} Rule{" "}
            </div>
          }
          bodyElement={
            <div className="col-md-12 body rule-right-menu">
              <div className="loader">
                <Spinner show={this.props.isFetching} />
              </div>
              <Input
                field={{
                  label: "Name",
                  type: "TEXT",
                  value: this.state.rule.name,
                  isRequired: true,
                }}
                width={6}
                name="name"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.name}
                placeholder={`Enter Name`}
              />
              <Input
                field={{
                  label: "Level",
                  type: "PICKLIST",
                  value: this.state.rule.level,
                  options: this.levelList(),
                  isRequired: true,
                }}
                width={6}
                name="level"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.level}
                placeholder={`Select Level`}
              />

              <div
                className={`field-section
                 field-section--required   col-md-6 col-xs-6 ${
                   this.state.error.classification.errorMessage
                     ? `field-section--error`
                     : ""
                 } `}
              >
                <div className="field__label row">
                  <label className="field__label">Classification</label>
                  <span className="field__label-required" />
                </div>
                <div>
                  {" "}
                  {/* <Select.Creatable
                    name="classification"
                    options={this.classificationList()}
                    onChange={(e) => this.handleChangeClassification(e)}
                    value={this.state.rule.classification}
                    onNewOptionClick={this.onNewOptionClick}
                    loading={this.props.isFetching}
                    placeholder="Select or Type to Create New"
                    clearable={false}
                    promptTextCreator={(name) => `Create Type "${name}"`}
                  /> */}
                </div>
                {this.state.error.classification.errorMessage && (
                  <div className="field__error">
                    {this.state.error.classification.errorMessage}
                  </div>
                )}
              </div>
              <Input
                field={{
                  label: "Function ",
                  type: "PICKLIST",
                  value: this.state.rule.function,
                  options: this.state.functionList,
                  isRequired: false,
                }}
                width={6}
                multi={false}
                name="function"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.function}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: "To Ignore Devices",
                  type: "PICKLIST",
                  value: this.state.rule.to_ignore_devices,
                  options: this.state.devicesShort.filter(
                    (device) =>
                      !this.state.rule.to_include_devices.includes(device.value)
                  ),
                  isRequired: false,
                }}
                width={6}
                multi={true}
                name="to_ignore_devices"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.to_ignore_devices}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: "To Include Devices",
                  type: "PICKLIST",
                  value: this.state.rule.to_include_devices,
                  options: this.state.devicesShort.filter(
                    (device) =>
                      !this.state.rule.to_ignore_devices.includes(device.value)
                  ),
                  isRequired: false,
                }}
                width={6}
                multi={true}
                name="to_include_devices"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.to_include_devices}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: "Applies To Manufacturer",
                  type: "PICKLIST",
                  value: this.state.rule.applies_to_manufacturers,
                  options: this.manufacturers(),
                  isRequired: true,
                }}
                width={6}
                multi={true}
                name="applies_to_manufacturers"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.applies_to_manufacturers}
                placeholder={`Select`}
              />
              <Input
                field={{
                  label: "Applies To",
                  type: "PICKLIST",
                  value: this.state.rule.applies_to,
                  options: this.typeList(),
                  isRequired: true,
                }}
                width={6}
                multi={true}
                name="applies_to"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.applies_to}
                placeholder={`Select`}
              />
              <Input
                className="rule-iosversion1"
                field={{
                  label: "IOS version",
                  type: "TEXT",
                  value: this.state.rule.applies_to_ios_version,
                  isRequired: false,
                }}
                width={12}
                labelIcon="info"
                labelTitle={<IOSVersionCustomTooltip />}
                name="applies_to_ios_version"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.ios_version}
                placeholder={`Enter IOS versions`}
              />
              <SquareButton
                content={`View Device List ${
                  this.state.devicesList.length ? `(${devicesListCount})` : ""
                }`}
                bsStyle={"primary"}
                onClick={(e) => this.setState({ showDeviceList: true })}
                className="view-device-list"
              />
              <div className="field-section field-section--required  col-md-12 col-xs-12">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Detail
                  </label>
                  <span className="field__label-required" />
                  <TooltipCustom />
                  <div className="not-checkbox">
                    <Checkbox
                      isChecked={this.state.rule.apply_not}
                      name="apply_not"
                      onChange={(e) => this.handleChangeCheckBox(e)}
                    >
                      Fail if match found
                    </Checkbox>
                  </div>
                </div>
                <div
                  className={`${
                    this.state.error.detail.errorMessage ? `error-input` : ""
                  }`}
                >
                  <MentionsInput
                    markup="[__display__]"
                    value={this.state.rule.detail}
                    onChange={this.handleChangeDetails}
                    className={"outer"}
                  >
                    <Mention
                      trigger="@"
                      data={this.variableList()}
                      className={"inner-drop"}
                      markup="#RULE#__display__"
                    />
                    <Mention
                      trigger="#"
                      data={this.rangeVariables()}
                      className={"inner-drop"}
                      markup="#RULE#__display__"
                    />
                  </MentionsInput>
                </div>
                {this.state.error.detail.errorMessage && (
                  <div className="field__error">
                    {this.state.error.detail.errorMessage}
                  </div>
                )}
              </div>
              <Input
                className="rule-exceptions1"
                field={{
                  label: "Exceptions",
                  type: "TEXTAREA",
                  value: this.state.rule.exceptions,
                  isRequired: false,
                }}
                width={12}
                name="exceptions"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.exceptions}
                placeholder={`Enter Rule Exceptions`}
                labelIcon="info"
                labelTitle={<RuleExceptionCustomTooltip />}
              />
              <Input
                field={{
                  label: "Description",
                  type: "TEXTAREA",
                  value: this.state.rule.description,
                  isRequired: false,
                }}
                width={12}
                name="description"
                onChange={(e) => this.handleChange(e)}
                error={this.state.error.description}
                placeholder={`Enter Description`}
              />
            </div>
          }
          footerElement={
            <div className="right-menu-footer">
              {" "}
              <SquareButton
                content={`Save`}
                bsStyle={"primary"}
                onClick={(e) => this.onSaveClick(this.state.rule, true, false)}
                className="save"
              />
              <SquareButton
                content={`Cancel`}
                bsStyle={"default"}
                onClick={(e) => this.clearPopUp()}
                className="save"
              />
            </div>
          }
          className="add-edit-rule"
        />
        <ViewRuleHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
        />
        {this.state.viewCDhistory && (
          <ViewRuleCDHistory
            show={this.state.viewCDhistory}
            onClose={this.toggleviewCDHistoryPopup}
          />
        )}
        {this.props.customerId && this.state.showDeviceList && (
          <ModalBase
            show={this.state.showDeviceList}
            onClose={this.toggleShowDeviceList}
            titleElement={"Devices"}
            bodyElement={
              <>
                <Table
                  columns={columns}
                  rows={this.state.devicesList
                    .filter((x) =>
                      this.state.rule.applies_to.includes(x.device_type)
                    )
                    .filter((row) =>
                      searchInFields(row, this.state.searchDevice, [
                        "name",
                        "device_name",
                        "model_number",
                        "serial_number",
                        "classification_name",
                      ])
                    )}
                  rowSelection={{
                    showCheckbox: false,
                    selectIndex: "id",
                    onRowsToggle: () => null,
                  }}
                  customTopBar={
                    <Input
                      field={{
                        label: "",
                        type: "SEARCH",
                        value: this.state.searchDevice,
                        isRequired: false,
                      }}
                      width={4}
                      placeholder="Search"
                      name="searchString"
                      onChange={(e) => {
                        this.setState({ searchDevice: e.target.value });
                      }}
                      className="dashboard-order-tracking__search"
                    />
                  }
                  className={`device-listing__table ${
                    this.props.isFetchingDevice ? `loading` : ``
                  }`}
                  onRowClick={() => null}
                  loading={this.props.isFetchingDevice}
                />
              </>
            }
            footerElement={null}
            className={`device-list-rules`}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  types: state.inventory.types,
  rules: state.configuration.rules,
  levels: state.configuration.levels,
  customerId: state.customer.customerId,
  customers: state.customer.customersShort,
  isFetching: state.configuration.isFetching,
  rule: state.configuration.customerVariable,
  manufacturers: state.inventory.manufacturers,
  isPostingBatch: state.inventory.isPostingBatch,
  isFetchingList: state.configuration.isFetchingList,
  isFetchingDevice: state.inventory.isFetchingDevice,
  customerProfile: state.customerUser.customerProfile,
  classifications: state.configuration.classifications,
  customerVariables: state.configuration.customerVariables,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchRules: (id: number, params?: ISPRulesFilterParams) =>
    dispatch(getCustomerRules(id, params)),
  fetchClassifications: (user_type?: string) => dispatch(fetchClassifications()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchRulesLevels: () => dispatch(fetchRulesLevels()),
  fetchSingleRule: (custId: number, id: number) =>
    dispatch(fetchSingleCustomerRule(custId, id)),
  saveRule: (custId: number, rule: IRule) =>
    dispatch(saveCustomerRule(custId, rule)),
  editRule: (custId: number, rule: IRule) =>
    dispatch(editCustomerRule(custId, rule)),
  createClassifications: (text: string) =>
    dispatch(createClassifications(text)),
  deleteRule: (custId: number, id: number) =>
    dispatch(deleteCustomerRule(custId, id)),
  fetchTypes: () => dispatch(fetchTypes()),
  customerRuleAssign: (custId: number, id: number, status: string) =>
    dispatch(customerRuleAssign(custId, id, status)),
  fetchCustomerVars: (custId: number, params?: IServerPaginationParams) =>
    dispatch(fetchCustomerVars(custId, params)),
  fetchRuleFunctions: () => dispatch(fetchRuleFunctions()),
  fetchComplianceDevices: (id: number) => dispatch(fetchComplianceDevices(id)),
  fetchDevicesCustomerUser: (show: boolean) =>
    dispatch(fetchDevicesCustomerUser(show)),
  rerunConfigCompliance: (data: any) => dispatch(rerunConfigCompliance(data)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatus(id)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerRulesList);
