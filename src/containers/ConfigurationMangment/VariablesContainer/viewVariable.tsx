import React from 'react';
import Spinner from '../../../components/Spinner';
import SquareButton from '../../../components/Button/button';
import ModalBase from '../../../components/ModalBase/modalBase';

interface IViewVariableProps {
  show: boolean;
  onClose: (e: any) => void;
  variable: IVariable;
  isFetching: boolean;
}

interface IViewVariableState {
  variable: IVariable;
  open: boolean;
}

export default class ViewVariable extends React.Component<
  IViewVariableProps,
  IViewVariableState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IViewVariableProps) {
    super(props);
  }

  renderView = () => {
    return (
      <div
        className={`details-section`}
      >
        <div className="db">
          <label className="" title="">name</label>
          <label>{this.props.variable.name || 'N.A.'}</label>
        </div>
        <div className="db">
          <label className="" title="">code</label>
          <label>{this.props.variable.variable_code || 'N.A.'}</label>
        </div>
        <div className="db">
          <label className="" title="">values</label>
          <label>{this.props.variable.variable_values && this.props.variable.variable_values.join(', ') || 'N.A.'}</label>
        </div>
        <div className="db">
          <label className="" title="">option</label>
          <label>{this.props.variable.option || 'N.A.'}</label>
        </div>
        <div className="db">
          <label className="" title="">type</label>
          <label>{this.props.variable.type || 'N.A.'}</label>
        </div>
        <div className="db">
          <label className="" title="">description</label>
          <label>{this.props.variable.description || 'N.A.'}</label>
        </div>
      </div>
    );
  };
  getTitle = () => {
    return (
      <div className="variable-details">
        Variable Details
      </div>
    );
  };

  getBody = () => {
    return (
      <div className="variable">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className={`${this.props.isFetching ? `loading` : ''}`}>
          {this.props.variable && this.renderView()}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`variable__footer
        ${this.props.isFetching ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.props.variable && this.getTitle()}
        bodyElement={this.props.variable && this.getBody()}
        footerElement={this.props.variable && this.getFooter()}
        className="view-variable"
      />
    );
  }
}
