import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import _, { cloneDeep } from "lodash";
import "./style.scss";
import Table from "../../../components/Table/table";
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
import DeleteButton from "../../../components/Button/deleteButton";
import Spinner from "../../../components/Spinner";
import IconButton from "../../../components/Button/iconButton";
import {
  FETCH_RULE_SUCCESS,
  getCreatedVariableCDHistory,
  getDeletedVariableCDHistory,
  revertDeletedVariable,
  getCustomerCreatedVariableCDHistory,
  getCustomerDeletedVariableCDHistory,
  revertCustomerDeletedVariable,
} from "../../../actions/configuration";
import ViewVariable from "./viewVariable";

interface IViewVariableCDHistoryProps extends ICommonProps {
  show: boolean;
  isFetching: boolean;
  onClose: (e: any, reverted?: boolean) => void;
  createdHistory: any;
  isFetchingCreated: boolean;
  deletedHistory: any;
  isFetchingDeleted: boolean;
  revertDeletedVariable: (id: number) => Promise<any>;
  getCreatedVariableHistory: (params?: IServerPaginationParams) => Promise<any>;
  getDeletedVariableHistory: (params?: IServerPaginationParams) => Promise<any>;
  revertCustomerDeletedVariable: (id: number, custId: number) => Promise<any>;
  getCustomerCreatedVariableHistory: (
    id: number,
    params?: IServerPaginationParams
  ) => Promise<any>;
  getCustomerDeletedVarHistory: (
    id: number,
    params?: IServerPaginationParams
  ) => Promise<any>;
}

interface IViewVariableCDHistoryState {
  open: boolean;
  reverted: boolean;
  variable: IVariable;
  openPreview: boolean;
  variableDetails: any;
  createdHistory: any[];
  deletedHistory: any[];
  viewVariable: boolean;
  createHistoryRows: any[];
  deleteHistoryRows: any[];
  isCollapsedCreate: boolean;
  isCollapsedDelete: boolean;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  paginationDelete: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
}

class ViewVariableCDHistory extends React.Component<
  IViewVariableCDHistoryProps,
  IViewVariableCDHistoryState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IViewVariableCDHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    variable: null,
    reverted: false,
    createdHistory: [],
    deletedHistory: [],
    openPreview: false,
    viewVariable: false,
    createHistoryRows: [],
    deleteHistoryRows: [],
    variableDetails: null,
    isCollapsedDelete: true,
    isCollapsedCreate: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    paginationDelete: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
  });

  componentDidUpdate(prevProps: IViewVariableCDHistoryProps) {
    if (
      this.props.createdHistory &&
      this.props.createdHistory !== prevProps.createdHistory
    ) {
      this.setRows(this.props);
    }
    if (
      this.props.deletedHistory &&
      this.props.deletedHistory !== prevProps.deletedHistory
    ) {
      this.setRowsDelete(this.props);
    }
  }

  setRows = (nextProps: IViewVariableCDHistoryProps) => {
    const createdHistory = nextProps.createdHistory;
    const history: any[] = createdHistory.results;
    const createHistoryRows: any[] =
      history &&
      history.map((row, index) => ({
        id: row.id,
        name: `${_.get(row, "snapshot_data.name")}`,
        date_created: `${_.get(row, "revision.date_created")}`,
        index,
        snapshot_data: _.get(row, "snapshot_data"),
        is_global: _.get(row, "snapshot_data.is_global"),
        user: `${_.get(row, "revision.user", "-")}`,
      }));

    this.setState((prevState) => ({
      createHistoryRows,
      createdHistory,
      pagination: {
        ...prevState.pagination,
        totalRows: createdHistory.count,
        currentPage: createdHistory.links.page_number - 1,
        totalPages: Math.ceil(
          createdHistory.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  setRowsDelete = (nextProps: IViewVariableCDHistoryProps) => {
    const deletedHistory = nextProps.deletedHistory;
    const history: any[] = deletedHistory.results;
    const deleteHistoryRows: any[] =
      history &&
      history.map((row, index) => ({
        id: row.id,
        name: `${_.get(row, "snapshot_data.name")}`,
        date_created: `${_.get(row, "revision.date_created")}`,
        user: `${_.get(row, "revision.user", "-")}`,
        index,
        is_global: _.get(row, "snapshot_data.is_global"),
        snapshot_data: _.get(row, "snapshot_data"),
      }));

    this.setState((prevState) => ({
      deleteHistoryRows,
      deletedHistory,
      pagination: {
        ...prevState.pagination,
        totalRows: deletedHistory.count,
        currentPage: deletedHistory.links.page_number - 1,
        totalPages: Math.ceil(
          deletedHistory.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  getTitle = () => {
    return (
      <div className="text-center">Created & Deleted Variable History</div>
    );
  };

  getVersion = (obj: any) => {
    const version = `${obj.major_version}.${obj.minor_version}`;
    return version ? version : "N.A.";
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getCreatedVariableHistory(newParams);
  };
  
  // Server side searching, sorting, ordering
  fetchDataDelete = (params: IServerPaginationParams) => {
    const prevParams = this.state.paginationDelete.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      paginationDelete: {
        ...prevState.paginationDelete,
        params: newParams,
      },
    }));

    this.props.getDeletedVariableHistory(newParams);
  };

  toggleviewVariablePopup = () => {
    this.setState({
      viewVariable: false,
    });
  };

  toggleCollapsedState = (data: any) => {
    const newState = cloneDeep(this.state);
    (newState[data] as any) = !this.state[data];
    this.setState(newState);
  };

  renderHistoryContainer = () => {
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };
    const manualPropsDelete = {
      manual: true,
      pages: this.state.paginationDelete.totalPages,
      onFetchData: this.fetchDataDelete,
    };
    const columns: ITableColumn[] = [
      {
        accessor: "name",
        Header: "Name",
        sortable: false,
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: "is_global",
        Header: "Is Global",
        sortable: false,
        width: 80,
        Cell: (cell) => (
          <div className="icons-template-1">
            {cell.original.is_global && (
              <img
                src={`/assets/icons/global-green.svg`}
                alt=""
                title={`Global Variable`}
                className="info-svg-images"
              />
            )}
            {cell.original.is_global === false && (
              <img
                src={`/assets/icons/global-grey.svg`}
                alt=""
                title={`Customer Variable`}
                className="info-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: "date_created",
        Header: "Created On",
        sortable: false,
        Cell: (name) => (
          <div className="pl-15">{utcToLocalInLongFormat(name.value)}</div>
        ),
      },
      {
        accessor: "status",
        Header: "View",
        sortable: false,
        Cell: (data) => (
          <DeleteButton
            type="pdf_preview"
            title="View"
            onClick={(e) => this.previewDoc(data.original.snapshot_data)}
          />
        ),
      },
    ];
    const columnsDeleted: ITableColumn[] = [
      {
        accessor: "name",
        Header: "Name",
        sortable: false,
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: "is_global",
        Header: "Is Global",
        sortable: false,
        width: 80,
        Cell: (cell) => (
          <div className="icons-template-1">
            {cell.original.is_global && (
              <img
                src={`/assets/icons/global-green.svg`}
                alt=""
                title={`Global Variable`}
                className="info-svg-images"
              />
            )}
            {cell.original.is_global === false && (
              <img
                src={`/assets/icons/global-grey.svg`}
                alt=""
                title={`Customer Variable`}
                className="info-svg-images"
              />
            )}
          </div>
        ),
      },
      {
        accessor: "date_created",
        Header: "Deleted On",
        sortable: false,
        Cell: (name) => (
          <div className="pl-15">{utcToLocalInLongFormat(name.value)}</div>
        ),
      },
      {
        accessor: "status",
        Header: "Action",
        sortable: false,
        Cell: (data) => (
          <>
            <DeleteButton
              type="pdf_preview"
              title="View"
              onClick={(e) => this.previewDoc(data.original.snapshot_data)}
            />
            <IconButton
              icon="revert.svg"
              title="Revert Variable"
              onClick={(e) => this.checkoutVariable(data.original, e)}
            />
          </>
        ),
      },
    ];
    return (
      <>
        <div
          key={0}
          className={`collapsable-section  ${
            this.state.isCollapsedCreate
              ? "collapsable-section--collapsed"
              : "collapsable-section--not-collapsed"
          }`}
        >
          <div
            className="col-md-12 collapsable-heading"
            onClick={(e) => this.toggleCollapsedState("isCollapsedCreate")}
          >
            <div className="left col-md-9">Created Variable's</div>
          </div>
          <div className="collapsable-contents">
            <Table
              manualProps={manualProps}
              columns={columns}
              rows={this.state.createHistoryRows}
              className={`provider-users-listing__table`}
              loading={this.props.isFetchingCreated}
            />
          </div>
        </div>
        <div
          key={1}
          className={`collapsable-section  ${
            this.state.isCollapsedDelete
              ? "collapsable-section--collapsed"
              : "collapsable-section--not-collapsed"
          }`}
        >
          <div
            className="col-md-12 collapsable-heading"
            onClick={(e) => this.toggleCollapsedState("isCollapsedDelete")}
          >
            <div className="left col-md-9">Deleted Variable's</div>
          </div>
          <div className="collapsable-contents">
            <Table
              manualProps={manualPropsDelete}
              columns={columnsDeleted}
              rows={this.state.deleteHistoryRows}
              className={`provider-users-listing__table`}
              loading={this.props.isFetchingDeleted || this.props.isFetching}
            />
          </div>
        </div>
      </>
    );
  };

  getBody = () => {
    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingCreated} />
        </div>
        <div className="history-section heading  col-md-12">
          {this.renderHistoryContainer()}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return <div className="footer"></div>;
  };

  previewDoc = (obj: any) => {
    const variable = obj;
    this.setState({
      viewVariable: true,
      variableDetails: variable,
    });
  };
  checkoutVariable = (variable: any, e) => {
    this.setState({
      openPreview: !this.state.openPreview,
      variable,
    });
    this.props.revertDeletedVariable(variable.id).then((action) => {
      if (action.type === FETCH_RULE_SUCCESS) {
        this.setState({ reverted: true });
        this.fetchDataDelete(this.state.pagination.params);
      }
    });
  };

  render() {
    return (
      <>
        {this.props.show && (
          <RightMenu
            show={this.props.show}
            onClose={(e) => this.props.onClose(e, this.state.reverted)}
            titleElement={this.getTitle()}
            bodyElement={this.getBody()}
            footerElement={this.getFooter()}
            className="version-history"
          />
        )}
        {this.state.viewVariable && (
          <ViewVariable
            show={this.state.viewVariable}
            onClose={this.toggleviewVariablePopup}
            variable={this.state.variableDetails}
            isFetching={false}
          />
        )}
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  createdHistory: state.configuration.createdHistory,
  isFetching: state.configuration.isFetching,
  isFetchingCreated: state.configuration.isFetchingCreated,
  deletedHistory: state.configuration.deletedHistory,
  isFetchingDeleted: state.configuration.isFetchingDeleted,
});

const mapDispatchToProps = (dispatch: any) => ({
  getCreatedVariableHistory: (params?: IServerPaginationParams) =>
    dispatch(getCreatedVariableCDHistory(params)),
  getDeletedVariableHistory: (params?: IServerPaginationParams) =>
    dispatch(getDeletedVariableCDHistory(params)),
  revertDeletedVariable: (id: number) => dispatch(revertDeletedVariable(id)),
  getCustomerCreatedVariableHistory: (
    id: number,
    params?: IServerPaginationParams
  ) => dispatch(getCustomerCreatedVariableCDHistory(id, params)),
  getCustomerDeletedVarHistory: (
    id: number,
    params?: IServerPaginationParams
  ) => dispatch(getCustomerDeletedVariableCDHistory(id, params)),
  revertCustomerDeletedVariable: (id: number, custId: number) =>
    dispatch(revertCustomerDeletedVariable(id, custId)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewVariableCDHistory)
);
