import React from "react";
import { isEqual } from "lodash";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
// import ReactDiffViewer from "react-diff-viewer"; (New Component)
import {
  getVariableHistory,
  getCustomerVariableHistory,
} from "../../../actions/configuration";
import Spinner from "../../../components/Spinner";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";

import { isEmptyObj } from "../../../utils/CommonUtils";
import { getFieldNames } from "../../../utils/fieldName";
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
interface IViewVariableHistoryProps extends ICommonProps {
  id: number;
  show: boolean;
  customerId: number;
  variableHistory: any;
  variableType: string;
  isFetchingHistory: boolean;
  onClose: (e: any) => void;
  getVariableHistory: (id: number) => Promise<any>
  getCustomerVariableHistory: (custId: number, id: number) => Promise<any>;
}

interface IViewVariableHistoryState {
  open: boolean;
  loadMore: boolean;
  variableHistory: any;
  isCollapsed: boolean[];
}

class ViewVariableHistory extends React.Component<
  IViewVariableHistoryProps,
  IViewVariableHistoryState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IViewVariableHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    variableHistory: [],
    isCollapsed: [],
    loadMore: false,
  });

  componentDidUpdate(prevProps: IViewVariableHistoryProps) {
    if (this.props.id && this.props.id !== prevProps.id) {
      if (this.props.variableType === "customer") {
        this.props.getCustomerVariableHistory(
          this.props.customerId,
          this.props.id
        );
      }
      if (this.props.variableType === "global") {
        this.props.getVariableHistory(this.props.id);
      }
    }
    if (
      this.props.variableHistory &&
      this.props.variableHistory !== prevProps.variableHistory
    ) {
      this.setState({
        isCollapsed: Object.keys(
          this.props.variableHistory.filter((h) => !isEmptyObj(h.diff))
        ).map((key) => false),
      });
    }
  }

  getTitle = () => {
    return <div className="title">Variable History</div>;
  };

  getVersion = (boardIndex: any, getNumber: boolean = false) => {
    const variableHistory = this.props.variableHistory.filter(
      (h) => !isEmptyObj(h.diff)
    );

    const version = variableHistory && variableHistory.length - boardIndex;

    return getNumber ? version : version.toFixed(2);
  };

  onClickLoadMore = () => {
    this.setState({ loadMore: !this.state.loadMore });
  };

  getValueByField = (value, field) => {
    let currentValue = value;

    if (typeof value === "object") {
      currentValue = null;
      currentValue = JSON.stringify(value, null, 4);
    }

    if (field.indexOf("variable_values") > -1) {
      currentValue = value && value.length > 0 ? value.join(",\n") : "";
    }

    return currentValue;
  };

  renderHistoryContainer = (history: any, boardIndex: number) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];
    const toggleCollapsedState = () =>
      this.setState((prevState) => ({
        isCollapsed: [
          ...prevState.isCollapsed.slice(0, boardIndex),
          !prevState.isCollapsed[boardIndex],
          ...prevState.isCollapsed.slice(boardIndex + 1),
        ],
      }));
    return (
      <div
        key={boardIndex}
        className={`collapsable-section  ${
          isCollapsed
            ? "collapsable-section--collapsed"
            : "collapsable-section--not-collapsed"
        }`}
      >
        <div
          className="col-md-12 collapsable-heading"
          onClick={toggleCollapsedState}
        >
          <div className="left col-md-10">
            <div className="name">
              {" "}
              <span>User: </span>
              {history.revision.user}
            </div>
            <div className="version">
              {" "}
              <span>version: </span>
              {this.getVersion(boardIndex)}
            </div>
            <div className="date">
              {" "}
              <span>Updated On: </span>
              {utcToLocalInLongFormat(history.revision.date_created)}
            </div>
          </div>
        </div>
        <div className="collapsable-contents">
          {history.diff && history.diff.values_changed && (
            <>
              <div className="heading-version">
                <span>
                  {" "}
                  {` Version ${(
                    (this.getVersion(boardIndex, true) as any) - 1
                  ).toFixed(2)}`}{" "}
                </span>
                <span>{` Version ${this.getVersion(boardIndex)}`}</span>
              </div>
              {Object.keys(history.diff.values_changed).map((v, i) => {
                // const version = history.diff.values_changed[v];
                return (
                  <div key={i} className="diff-view-section">
                    <span className="heading">{getFieldNames(v)}</span>
                    {/* <ReactDiffViewer
                      oldValue={`${this.getValueByField(version.old_value, v)}`}
                      newValue={`${this.getValueByField(version.new_value, v)}`}
                      splitView={true}
                      disableWordDiff={version.diff ? false : true}
                    /> */}
                  </div>
                );
              })}
            </>
          )}
          {history.diff && history.diff.iterable_item_added && (
            <div className={"col-md-6 row"}>
              <div className="heading-version">
                <span> {`Added`} </span>
              </div>

              {history.diff &&
                history.diff.iterable_item_added &&
                Object.keys(history.diff.iterable_item_added).map((v, i) => {
                  const version = history.diff.iterable_item_added[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span> {version}
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
          {history.diff && history.diff.iterable_item_removed && (
            <div className={"col-md-6 row removed-section"}>
              <div className="heading-version">
                <span> {`Removed`} </span>
              </div>
              {history.diff &&
                history.diff.iterable_item_removed &&
                Object.keys(history.diff.iterable_item_removed).map((v, i) => {
                  const version = history.diff.iterable_item_removed[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span> {version}
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
          {!history.diff ||
            (isEqual(history.diff, {}) && (
              <div className="no-data"> No diff data</div>
            ))}
        </div>
      </div>
    );
  };

  getBody = () => {
    const variableHistory = this.props.variableHistory.filter(
      (h) => !isEmptyObj(h.diff)
    );

    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingHistory} />
        </div>
        <div className="history-section heading  col-md-12">
          {variableHistory.length === 0 && (
            <div className="col-md-12 no-data" style={{ textAlign: "center" }}>
              {" "}
              No History available.
            </div>
          )}
          {variableHistory.length !== 0 &&
            !this.state.loadMore &&
            variableHistory.slice(0, 5).map((history, i) => {
              return this.renderHistoryContainer(history, i);
            })}
          {variableHistory.length !== 0 &&
            this.state.loadMore &&
            variableHistory.map((history, i) => {
              return this.renderHistoryContainer(history, i);
            })}
          {variableHistory.length > 5 && (
            <div
              className="col-md-12 show-more"
              onClick={(e) => this.onClickLoadMore()}
              style={{ textAlign: "center" }}
            >
              {" "}
              {!this.state.loadMore ? "load more..." : "show less"}
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`footer
        ${this.props.isFetchingHistory ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="version-history"
      />
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  variableHistory: state.configuration.variableHistory,
  isFetchingHistory: state.configuration.isFetchingHistory,
  customerId: state.customer.customerId,
});

const mapDispatchToProps = (dispatch: any) => ({
  getVariableHistory: (id: number) => dispatch(getVariableHistory(id)),
  getCustomerVariableHistory: (custId: number, id: number) =>
    dispatch(getCustomerVariableHistory(custId, id)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewVariableHistory)
);
