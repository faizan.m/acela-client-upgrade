import React from "react";
import Spinner from "../../components/Spinner";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";

interface IPurchaseOrderProps {
  loadingOrderDetails: boolean;
  order: IPurchaseHistory;
  show: boolean;
  onClose: () => void;
}

interface IPurchaseOrderState {}

export default class PurchaseOrderDetail extends React.Component<
  IPurchaseOrderProps,
  IPurchaseOrderState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IPurchaseOrderProps) {
    super(props);
  }

  componentDidUpdate(prevProps: IPurchaseOrderProps) {
    if (this.props.order && this.props.order !== prevProps.order) {
      this.setState({ order: this.props.order });
    }
  }

  renderOrderDetail = () => {
    return (
      <div>
        <div className="loader">
          <Spinner show={this.props.loadingOrderDetails} />
        </div>
        <div className={`add-circuit__body`}>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Product ID
              </label>
            </div>
            <div>{this.props.order.product.identifier || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Description
              </label>
            </div>
            <div>{this.props.order.description || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Customer PO
              </label>
            </div>
            <div>{this.props.order.customer_po_number || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Purchase Quantity
              </label>
            </div>
            <div>
              <div>{this.props.order.quantity || "N.A."}</div>
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Received Quantity
              </label>
            </div>
            <div>{this.props.order.receivedQuantity || "N.A."}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Received Status
              </label>
            </div>
            <div>{this.props.order.received || "NA"}</div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Date Received
              </label>
            </div>
            <div>
              {fromISOStringToFormattedDate(
                this.props.order.dateReceived,
                "MMM DD, YYYY"
              ) || "NA"}
            </div>
          </div>
          <div className="field-section col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Shipment Method
              </label>
            </div>
            <div>
              {(this.props.order.shipmentMethod &&
                this.props.order.shipmentMethod.name) ||
                "N.A."}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Ship Date
              </label>
            </div>
            <div>
              {fromISOStringToFormattedDate(
                this.props.order.shipDate,
                "MMM DD, YYYY"
              ) || "NA"}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Serial Numbers
              </label>
            </div>
            <div style={{ overflowWrap: "anywhere" }}>
              {this.props.order.serialNumbers || "N.A."}
            </div>
          </div>
          <div className="field-section     col-md-6 col-xs-6">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Tracking Numbers
              </label>
            </div>
            <div style={{ overflowWrap: "anywhere" }}>
              {this.props.order.trackingNumber || "N.A."}
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const poNumber =
      (this.props.order && this.props.order.purchaseOrderNumber) || "";

    return (
      <div className="rules-listing-parent">
        <ModalBase
          show={this.props.show}
          onClose={this.props.onClose}
          titleElement={`Purchase Order Details - #${poNumber}`}
          bodyElement={this.props.show && this.renderOrderDetail()}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.props.onClose}
            />
          }
          className="view-contacts"
        />
      </div>
    );
  }
}
