import React, { Component } from "react";
import moment from "moment";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import {
  getAllLineItemsOperations,
  getSinglePurchaseOrder,
  GET_SINGLE_PO_SUCCESS,
  LINE_ITEMS_SUCCESS,
} from "../../actions/inventory";
import { getSingleQuote } from "../../actions/sow";
import { getDataCommon, GET_DATA_SUCCESS } from "../../actions/orderTracking";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import Checkbox from "../../components/Checkbox/checkbox";
import { commonFunctions } from "../../utils/commonFunctions";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import "./style.scss";

interface IOrderTrackingState {
  loading: boolean;
  purchaseOrder: any;
  inputValue: string;
  showSearch: boolean;
  ticket_last_note: string;
  loadingLineItems: boolean;
  showReceivedOnly: boolean;
  toExcludeServices: boolean;
  lineItems: CustomLineItem[];
}

interface IOrderTrackingProps {
  quote: IQuote;
  order: IPurchaseHistory;
  isFetchingShipments: boolean;
  shipments: IPickListOptions[];
  purchaseOrderSetting: IPurchaseOrderSetting;
  getDataCommon: (url: string) => Promise<any>;
  getSingleQuote: (id: number) => Promise<any>;
  getSinglePurchaseOrder: (id: number) => Promise<any>;
  getLineItemsOperations: (purchase_order_ids: number[]) => Promise<any>;
}

class OrderTracking extends Component<
  IOrderTrackingProps,
  IOrderTrackingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IOrderTrackingProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    loading: false,
    ticket_last_note: "-",
    purchaseOrder: {},
    lineItems: [],
    loadingLineItems: false,
    showReceivedOnly: false,
    toExcludeServices: false,
    inputValue: "",
    showSearch: false,
    categoryIds: [],
  });

  componentDidMount() {
    this.setState({ loading: true });
    this.props
      .getSinglePurchaseOrder(this.props.order.purchaseOrderId)
      .then((action) => {
        if (action.type === GET_SINGLE_PO_SUCCESS) {
          this.setState(
            {
              purchaseOrder: action.response,
            },
            () => {
              this.getLineItemsOperations(this.props.order.purchaseOrderId);
              this.getLastNote(this.props.order.ticket_crm_id);
            }
          );
        }
      })
      .finally(() => this.setState({ loading: false }));
    this.props.getSingleQuote(this.props.order.opportunity_crm_id);
  }

  handleChangeCheckbox = (e: React.ChangeEvent<HTMLInputElement>) => {
    const stateKey = e.target.name as keyof IOrderTrackingState;
    const updatedState: Partial<IOrderTrackingState> = {
      [stateKey]: e.target.checked,
    };
    this.setState(
      updatedState as Pick<IOrderTrackingState, keyof IOrderTrackingState>
    );
  };

  renderLabelWithDetails = (width: number, label: string, detail: string) => {
    return (
      <div className={`field-section col-xs-${width}`}>
        <div className="field__label row">
          <div className="field__label-label">{label}</div>
        </div>
        <div className="detail">{detail}</div>
      </div>
    );
  };

  renderHeader = () => {
    return (
      <div className="po-ot-header">
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            6,
            "Opportunity",
            this.props.order.opportunity_name
          )}
          {this.renderLabelWithDetails(
            6,
            "Won Date",
            this.props.quote && this.props.quote.closed_date
              ? moment(this.props.quote.closed_date).format("MM/DD/YYYY")
              : "-"
          )}
        </div>
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            6,
            "Purchase Order",
            this.props.order.purchaseOrderNumber +
              (this.state.purchaseOrder.customerCompany &&
              this.state.purchaseOrder.customerCompany.name
                ? ` (${this.state.purchaseOrder.customerCompany.name})`
                : "")
          )}
          {this.renderLabelWithDetails(
            3,
            "Customer PO",
            this.props.order.customer_po_number
              ? this.props.order.customer_po_number
              : "-"
          )}
          {this.renderLabelWithDetails(
            3,
            "Order Status",
            this.state.purchaseOrder
              ? this.state.purchaseOrder.closedFlag
                ? "Closed"
                : "Open"
              : "N/A"
          )}
        </div>
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            6,
            "Order Ticket",
            this.props.order.ticket_crm_id +
              "-" +
              this.props.order.ticket_summary
          )}
          {this.renderLabelWithDetails(
            3,
            "LookingPoint PO",
            this.props.order.purchaseOrderNumber
          )}
        </div>
        <div className="po-ot-header-row col-md-12">
          {this.renderLabelWithDetails(
            12,
            "Last Note",
            this.state.ticket_last_note
          )}
        </div>
      </div>
    );
  };

  getLastNote = (ticket_crm_id: number) => {
    this.props
      .getDataCommon(`providers/purchase-order-tickets/${ticket_crm_id}/notes`)
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          this.setState({ ticket_last_note: action.response.text });
        }
      });
  };

  getLineItemsOperations = (value: number) => {
    this.setState({ loadingLineItems: true });
    this.props.getLineItemsOperations([value]).then((action) => {
      if (action.type === LINE_ITEMS_SUCCESS) {
        const lineItems: ILineItem[] = action.response;
        const newLineItems: CustomLineItem[] = lineItems.map((item) => {
          const isOpenPO: boolean = !this.state.purchaseOrder.closedFlag;
          return {
            ...item,
            serial_numbers: isOpenPO
              ? commonFunctions.getArrFromString(item.serialNumbers)
              : item.received_serial_numbers,
            tracking_number: isOpenPO
              ? commonFunctions.getArrFromString(item.trackingNumber)
              : item.received_tracking_numbers,
            shipping_method_id:
              item.shipmentMethod && item.shipmentMethod.id
                ? item.shipmentMethod.id
                : null,
            ship_date: item.shipDate ? item.shipDate : "",
            estimated_delivery_date: item.expectedShipDate
              ? item.expectedShipDate
              : "",
          };
        });

        this.setState({ lineItems: newLineItems, loadingLineItems: false });
      }
      this.setState({ loadingLineItems: false });
    });
  };

  sortBy = (itemA: CustomLineItem, itemB: CustomLineItem) => {
    let valueA = itemA.shipped_quantity;
    let valueB = itemB.shipped_quantity;
    let a = valueA || 0;
    let b = valueB || 0;
    let r = b - a;
    if (r === 0) {
      // Next line makes the magic :)
      r =
        typeof itemA.id !== "undefined" && typeof itemB.id !== "undefined"
          ? itemA.id - itemB.id
          : 0;
    }
    return r;
  };

  renderPurchaseOrders = () => {
    const {
      lineItems,
      toExcludeServices,
      inputValue,
      showReceivedOnly,
    } = this.state;
    let filteredLineItems = lineItems;

    if (toExcludeServices) {
      filteredLineItems = filteredLineItems.filter(
        (item) => !item.product.identifier.startsWith("CON")
      );
    }

    if (inputValue !== "") {
      filteredLineItems = filteredLineItems.filter(
        (item) =>
          item.product.identifier
            .toLowerCase()
            .includes(inputValue.toLowerCase()) ||
          item.description.toLowerCase().includes(inputValue.toLowerCase())
      );
    }

    if (showReceivedOnly) {
      filteredLineItems = filteredLineItems.filter(
        (a) => a.partially_received_quantity > 0
      );
    }

    const isOpenPO: boolean = !this.state.purchaseOrder.closedFlag;

    filteredLineItems = filteredLineItems.sort((a, b) => this.sortBy(a, b));

    return filteredLineItems.length > 0 ? (
      filteredLineItems.map((data) => (
        <div
          key={data.id}
          className={`product-details-container ${
            data.selected ? "selected" : ""
          }`}
        >
          <div className="product-detail top-secton">
            <div className="product-id">
              <div className="detail-title" title={String(data.id)}>
                Product ID
              </div>
              <div className="product-data">{data.product.identifier}</div>
            </div>
            <div className="product-desc">
              <div className="detail-title">Description</div>
              <div className="product-data po-description">
                {data.description}
              </div>
            </div>
            <div className="product-qty-section ph-product-qty-section">
              <div>
                <div className="detail-title">
                  {data.partially_received_quantity > 0
                    ? "Remaining"
                    : "Purchased Qty"}
                </div>
                <div className="quantity-pending product-data purchased-qty">
                  {Math.round(
                    data.total_quantity - data.partially_received_quantity
                  )}
                </div>
              </div>
              <div>
                <div className="detail-title">Shipped Qty</div>
                <div className="quantity-p quantity-received product-data shipped-qty">
                  {data.partially_received_quantity
                    ? data.partially_received_quantity
                    : "-"}
                </div>
              </div>
            </div>
          </div>
          <div className="product-detail middle-section">
            <div className="tag-input">
              <div className="detail-title">Tracking Number</div>
              {/* <TagsInput
                value={data.tracking_number}
                onChange={() => null}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Tracking Numbers",
                }}
                disabled={true}
              /> */}
            </div>
            <div className="tag-input">
              <div className="detail-title">Serial Number</div>
              {/* <TagsInput
                value={data.serial_numbers}
                onChange={() => null}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Serial Numbers",
                }}
                disabled={true}
              /> */}
            </div>
            <div className="mapped-carrier-container">
              <div className="detail-title">Carrier</div>
              <Input
                field={{
                  label: "",
                  type: "TEXT",
                  isRequired: false,
                  value: this.props.shipments.find(
                    (el) => el.value === data.shipping_method_id
                  )
                    ? this.props.shipments.find(
                        (el) => el.value === data.shipping_method_id
                      ).label
                    : null,
                }}
                disabled={true}
                width={12}
                name="shipping_method_id"
                onChange={null}
                loading={this.props.isFetchingShipments}
                className="product-data"
              />
            </div>
            <div>
              <div className="detail-title">Ship Date</div>
              <Input
                field={{
                  label: "",
                  type: "TEXT",
                  isRequired: false,
                  value:
                    fromISOStringToFormattedDate(
                      data.ship_date,
                      "MM-DD-YYYY"
                    ) || "-",
                }}
                disabled={true}
                width={12}
                name="ship_date"
                onChange={null}
                className="product-data"
              />
            </div>
            {isOpenPO && (
              <div>
                <div className="detail-title">Estimated Ship Date</div>
                <Input
                  field={{
                    label: "",
                    type: "TEXT",
                    isRequired: false,
                    value:
                      fromISOStringToFormattedDate(
                        data.estimated_delivery_date,
                        "MM-DD-YYYY"
                      ) || "-",
                  }}
                  disabled={true}
                  width={12}
                  name="internalNotes"
                  onChange={null}
                  className="product-data"
                />
              </div>
            )}
          </div>
          <div className="product-detail bottom-section"></div>
        </div>
      ))
    ) : (
      <div className="no-data" style={{ margin: "0 10px 30px" }}>
        No data found
      </div>
    );
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ inputValue: e.target.value });
  };

  render() {
    return (
      <div className="po-order-tracking">
        <div className="loader">
          <Spinner show={this.state.loading || this.state.loadingLineItems} />
        </div>
        {this.renderHeader()}
        <div className="product-data-section">
          {this.state.lineItems.length > 0 ? (
            <>
              <div className="select-all-rows">
                <div className="section-title">PRODUCT DETAILS</div>
                <div className="checkbox-section">
                  <Checkbox
                    isChecked={this.state.toExcludeServices}
                    name="toExcludeServices"
                    onChange={this.handleChangeCheckbox}
                  >
                    Exclude Services
                  </Checkbox>
                  <Checkbox
                    isChecked={this.state.showReceivedOnly}
                    name="showReceivedOnly"
                    onChange={this.handleChangeCheckbox}
                  >
                    Show Received Only
                  </Checkbox>
                </div>
                <Input
                  field={{
                    value: this.state.inputValue,
                    label: "",
                    type: "SEARCH",
                  }}
                  width={10}
                  name="searchString"
                  onChange={this.onSearchStringChange}
                  onBlur={() => {
                    if (this.state.inputValue.trim() === "") {
                      this.setState({ showSearch: false });
                    }
                  }}
                  placeholder="Search"
                  className="search-order-tracking search-order-tracking-outside"
                />
              </div>
              {this.renderPurchaseOrders()}
            </>
          ) : (
            !(this.state.loading || this.state.loadingLineItems) && (
              <div className="no-data">No data available</div>
            )
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  quote: state.sow.quote,
  shipments: state.setting.shipments,
  isFetchingShipments: state.setting.isFetchingShipments,
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getLineItemsOperations: (purchase_order_ids: number[]) =>
    dispatch(getAllLineItemsOperations(purchase_order_ids)),
  getSingleQuote: (id: number) => dispatch(getSingleQuote(id)),
  getSinglePurchaseOrder: (id: number) => dispatch(getSinglePurchaseOrder(id)),
  getDataCommon: (url: string) => dispatch(getDataCommon(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTracking);
