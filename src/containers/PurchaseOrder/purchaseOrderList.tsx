import moment from "moment";
import React from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import {
  getPurchaseHistories,
  getPurchaseOrderDetail,
  exportPurchaseOrderData,
  PURCHASE_HISTORY_SUCCESS,
} from "../../actions/sales";
import { getShipmentMethods } from "../../actions/setting";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Checkbox from "../../components/Checkbox/checkbox";
import { getPurchaseOrderSettings } from "../../actions/setting";
import { LAST_VISITED_CLIENT_360_TAB } from "../../actions/customer";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import PurchaseOrderDetail from "./purchaseOrderDetail";
// import DateRangePicker from "react-daterange-picker"; (New Component)
import { searchInFields } from "../../utils/searchListUtils";
import ModalBase from "../../components/ModalBase/modalBase";
import OrderTracking from "./orderTracking";
import "../../commonStyles/orderTracking.scss";
import store from "../../store";
import { getUserProfile } from "../../utils/AuthUtil";
import "./style.scss";

interface PurchaseHistoryProps extends ICommonProps {
  customerId: number;
  customerProfile: Icustomer;
  getShipmentMethods: () => Promise<any>;
  getPurchaseOrderSettings: () => Promise<any>;
  exportPurchaseOrderData: (customerId: number) => Promise<any>;
  getPurchaseHistories: (customerId: number) => Promise<any>;
  getPurchaseOrderDetail: (
    orderId: number,
    lineItemId: number,
    customer_po_number: number
  ) => Promise<any>;
}

interface PurchaseHistoryState {
  orders: IPurchaseHistory[];
  inputValue: string;
  noData: boolean;
  groupBy: string;
  showSearch: boolean;
  showGroupBy: boolean;
  loading: boolean;
  orderBy: string;
  showOrderDetail: boolean;
  selectedOrder: IPurchaseHistory;
  fetchingOrderDetails: boolean;
  searchString: string;
  showRecievedOnly: boolean;
  fetchingExport: boolean;
  filteredOrders: IPurchaseHistory[];
  showDateRangePicker: boolean;
  isDateValueSelected: boolean;
  isDatePickerSelected: boolean;
  showPurchaseOrderTracking: boolean;
  filterDateValue: any;
  filterDateFrom: string;
  filterDateTo: string;
}

class PurchaseOrderList extends React.Component<
  PurchaseHistoryProps,
  PurchaseHistoryState
> {
  constructor(props: PurchaseHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    orders: [],
    inputValue: "",
    noData: false,
    showSearch: false,
    showGroupBy: false,
    loading: false,
    groupBy: "",
    orderBy: "",
    showOrderDetail: false,
    selectedOrder: undefined,
    fetchingOrderDetails: false,
    searchString: "",
    showRecievedOnly: false,
    fetchingExport: false,
    filteredOrders: [],
    showDateRangePicker: false,
    isDatePickerSelected: false,
    isDateValueSelected: false,
    showPurchaseOrderTracking: false,
    filterDateValue: undefined,
    filterDateFrom: "",
    filterDateTo: "",
  });

  componentDidMount() {
    if (this.props.location.pathname === "/client-360") {
      store.dispatch({
        type: LAST_VISITED_CLIENT_360_TAB,
        response: "Purchase History",
      });
    }

    const { getShipmentMethods, getPurchaseOrderSettings } = this.props;
    getShipmentMethods();
    getPurchaseOrderSettings();
    let customerID: number = this.getCustomerId();
    this.fetchPurchaseHistories(customerID);
  }

  componentDidUpdate(prevProps: PurchaseHistoryProps) {
    if (
      (this.props.customerId &&
        this.props.customerId !== prevProps.customerId) ||
      (this.props.customerProfile &&
        this.props.customerProfile !== prevProps.customerProfile)
    ) {
      let customerID: number = this.getCustomerId();
      this.fetchPurchaseHistories(customerID);
    }
  }

  getCustomerId = () =>
    getUserProfile() &&
    getUserProfile().scopes &&
    getUserProfile().scopes.type === "CUSTOMER" &&
    this.props.customerProfile &&
    this.props.customerProfile.purchase_order_history_visibility_allowed
      ? this.props.customerProfile.id
      : this.props.customerId;

  fetchPurchaseHistories = (customerID: number) => {
    if (customerID) {
      this.setState({ loading: true });
      this.props.getPurchaseHistories(customerID).then((action) => {
        if (action.type === PURCHASE_HISTORY_SUCCESS) {
          this.setState({
            orders: this.getFormattedData(action.response),
            filteredOrders: this.getFormattedData(action.response),
            loading: false,
          });
        }
      });
    }
  };

  getFormattedData = (list: IPurchaseHistory[]): IPurchaseHistory[] => {
    return list.map((x) => {
      x.product_id = x.product.identifier;
      return x;
    });
  };

  toggleShowPurchaseOrder = (order?: IPurchaseHistory) => {
    this.setState((prevState) => ({
      selectedOrder: order,
      showPurchaseOrderTracking: !prevState.showPurchaseOrderTracking,
    }));
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ inputValue: e.target.value });
  };

  fetchByOrder = (field: string) => {
    let orderBy = "";
    switch (this.state.orderBy) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }

    this.setState(
      {
        orderBy,
      },
      () => this.sortData()
    );
  };

  getOrderClass = (field: string) => {
    let currentClassName = "";

    if (this.state.orderBy === `-${field}`) currentClassName = "desc-order";
    if (this.state.orderBy === field) currentClassName = "asc-order";

    return currentClassName;
  };

  onEditClick = (order) => {
    this.setState({
      showOrderDetail: true,
      selectedOrder: order,
      fetchingOrderDetails: true,
    });

    this.props
      .getPurchaseOrderDetail(
        order.purchaseOrderId,
        order.id,
        order.purchaseOrderNumber
      )
      .then((action) => {
        if (action.type === PURCHASE_HISTORY_SUCCESS) {
          const data = action.response;
          this.setState({
            selectedOrder: { ...this.state.selectedOrder, ...data },
            fetchingOrderDetails: false,
          });
        }
      });
  };

  handleSearchChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.filterOrders();
    });
  };

  onChecboxChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    const showRecievedOnly = event.target.checked;
    this.setState(
      {
        showRecievedOnly,
      },
      () => {
        this.filterOrders();
      }
    );
  };

  sortData = () => {
    const { orderBy } = this.state;
    let filteredOrders = cloneDeep(this.state.filteredOrders);

    if (orderBy === "-quantity") {
      filteredOrders = filteredOrders.sort((a, b) => a.quantity - b.quantity);
    } else if (orderBy === "quantity") {
      filteredOrders = filteredOrders.sort((a, b) => b.quantity - a.quantity);
    } else {
      if (orderBy.charAt(0) !== "-") {
        filteredOrders = filteredOrders.sort((a, b) => {
          let s1: string = a[orderBy] ? a[orderBy] : "ZZZ"; // For pushing the empty entries in bottom
          let s2: string = b[orderBy] ? b[orderBy] : "ZZZ";
          return s1.localeCompare(s2);
        });
      }

      if (orderBy.charAt(0) === "-") {
        const orderByfield = orderBy.substring(1);

        filteredOrders = filteredOrders.sort((a, b) => {
          let s1: string = a[orderByfield] ? a[orderByfield] : "";
          let s2: string = b[orderByfield] ? b[orderByfield] : "";
          return s2.localeCompare(s1);
        });
      }
    }
    this.setState({ filteredOrders });
  };

  onClickExport = () => {
    let customerID = this.getCustomerId();
    if (customerID) {
      this.setState({ fetchingExport: true });
      this.props.exportPurchaseOrderData(customerID).then((action) => {
        if (action.type === PURCHASE_HISTORY_SUCCESS) {
          const url = window.URL.createObjectURL(new Blob([action.response]));
          const link = document.createElement("a");
          link.href = url;
          link.setAttribute("download", "purchase-order-history.csv"); //or any other extension
          document.body.appendChild(link);
          link.click();
        }
        this.setState({ fetchingExport: false });
      });
    }
  };

  onSelect = (value, states) => {
    this.setState(
      {
        filterDateFrom: fromISOStringToFormattedDate(
          value.start._d,
          "YYYY-MM-DD"
        ),
        filterDateTo: fromISOStringToFormattedDate(value.end._d, "YYYY-MM-DD"),
        filterDateValue: value,
        isDatePickerSelected: false,
        isDateValueSelected: true,
      },
      () => this.filterOrders()
    );
  };

  resetDate = () => {
    this.setState(
      {
        filterDateFrom: "",
        filterDateTo: "",
        filterDateValue: undefined,
        isDatePickerSelected: false,
        isDateValueSelected: false,
      },
      () => this.filterOrders()
    );
  };

  filterOrders = () => {
    const {
      searchString,
      showRecievedOnly,
      orders,
      filterDateValue,
      filterDateFrom,
      filterDateTo,
      orderBy,
    } = this.state;

    let filteredOrders = orders;

    if (showRecievedOnly) {
      filteredOrders = filteredOrders.filter((order) => {
        return order.received === "Yes";
      });
    } else {
      filteredOrders = filteredOrders.filter((order) => {
        return ["Yes", "No"].indexOf(order.received) > -1;
      });
    }

    if (searchString.length > 0) {
      filteredOrders = filteredOrders.filter((row) =>
        searchInFields(row, searchString, [
          "description",
          "purchaseOrderNumber",
          "product_id",
          "customer_po_number",
          "opportunity_name",
        ])
      );
    }

    if (filterDateValue) {
      const startDate = moment(filterDateFrom, "YYYY/MM/DD");
      const endDate = moment(filterDateTo, "YYYY/MM/DD");

      filteredOrders = filteredOrders.filter((order) => {
        return moment(order.purchaseOrderDate, "YYYY/MM/DD").isBetween(
          startDate,
          endDate,
          null,
          "[]"
        );
      });
    }

    this.setState({ filteredOrders }, () => {
      if (orderBy) {
        this.sortData();
      }
    });
  };

  orderListingRows = (order: IPurchaseHistory, index: number) => {
    return (
      <div
        className={`row-panel column-8 ${index % 2 ? "even" : "odd"}`}
        key={`${order.purchaseOrderNumber}-${index}`}
      >
        <div
          onClick={() => this.onEditClick(order)}
          className="hide-overflow-width"
        >
          <div className="type ellipsis-text" title={order.purchaseOrderNumber}>
            {order.purchaseOrderNumber}
          </div>
        </div>
        <div
          onClick={() => this.onEditClick(order)}
          className="hide-overflow-width"
        >
          <div
            className="type ellipsis-no-width"
            title={order.customer_po_number}
          >
            {order.customer_po_number || "-"}
          </div>
        </div>
        <div
          onClick={() => this.onEditClick(order)}
          className="hide-overflow-width"
        >
          <div
            className="type ellipsis-no-width"
            title={
              order.vendorInvoiceNumber
                ? order.vendorInvoiceNumber.join(", ")
                : "-"
            }
          >
            {order.vendorInvoiceNumber
              ? order.vendorInvoiceNumber.join(", ")
              : "-"}
          </div>
        </div>
        <div className="hide-overflow-width ellipsis-no-width">
          <div
            className="type ellipsis-no-width"
            title={order.opportunity_name ? order.opportunity_name : "-"}
            onClick={() => {
              if (order.opportunity_crm_id) this.toggleShowPurchaseOrder(order);
            }}
          >
            {order.opportunity_name ? order.opportunity_name : "-"}
          </div>
        </div>
        <div
          className="hide-overflow-width"
          onClick={() => this.onEditClick(order)}
        >
          {fromISOStringToFormattedDate(
            order.purchaseOrderDate,
            "MMM DD, YYYY"
          )}
        </div>
        <div
          onClick={() => this.onEditClick(order)}
          className="hide-overflow-width"
        >
          <div className="type ellipsis-no-width" title={order.product_id}>
            {order.product_id}
          </div>
        </div>
        <div
          onClick={() => this.onEditClick(order)}
          className="hide-overflow-width"
        >
          <div className="type ellipsis-no-width" title={order.description}>
            {order.description}
          </div>
        </div>
        <div
          onClick={() => this.onEditClick(order)}
          className="order-fixed-width"
          title={String(order.quantity)}
        >
          {order.quantity}
        </div>
        <div
          onClick={() => this.onEditClick(order)}
          className="order-fixed-width"
          title={order.received}
        >
          {order.received}
        </div>
      </div>
    );
  };

  renderTopBar = () => {
    return (
      <>
        <div className="left-section">
          <Input
            field={{
              label: "",
              type: "SEARCH",
              value: this.state.searchString,
              isRequired: false,
            }}
            width={3}
            placeholder="Search"
            name="searchString"
            onChange={this.handleSearchChange}
            className="purchase-order__search"
          />
          <Checkbox
            isChecked={this.state.showRecievedOnly}
            name="showRecievedOnly"
            onChange={(e) => this.onChecboxChanged(e)}
            className="recieved_checkbox"
          >
            Show only received orders
          </Checkbox>
          <div className="po-date-range-filter">
            <div className="field-section col-md-6">
              <div className="date-range-box">
                <SquareButton
                  onClick={() =>
                    this.setState({
                      isDatePickerSelected: !this.state.isDatePickerSelected,
                      isDateValueSelected: false,
                    })
                  }
                  content=""
                  bsStyle={"default"}
                  className="date-button"
                />
                {/* {this.state.isDatePickerSelected &&
                  !this.state.isDateValueSelected && (
                    <DateRangePicker
                      value={this.state.filterDateValue}
                      onSelect={this.onSelect}
                      singleDateRange={true}
                      minimumDate={new Date("1990/01/01")}
                      maximumDate={new Date()}
                      numberOfCalendars={1}
                      showLegend={true}
                      clearAriaLabel={"reset"}
                      ClearIcon={"reset"}
                    />
                  )} */}
                {!this.state.isDatePickerSelected &&
                  this.state.isDateValueSelected && (
                    <div className="date-value" title={"MM-DD-YYYY"}>
                      {fromISOStringToFormattedDate(
                        this.state.filterDateFrom,
                        "MM-DD-YYYY"
                      )}
                      {" - "}
                      {fromISOStringToFormattedDate(
                        this.state.filterDateTo,
                        "MM-DD-YYYY"
                      )}
                    </div>
                  )}
                {!this.state.isDatePickerSelected &&
                  this.state.isDateValueSelected && (
                    <div
                      onClick={(e) => this.resetDate()}
                      className="reset-button"
                    >
                      x
                    </div>
                  )}
              </div>
            </div>
          </div>
          {!this.state.isDatePickerSelected && !this.state.isDateValueSelected && (
            <div className="field__label-label" style={{ marginLeft: "-15px" }}>
              Filter on purchase date
            </div>
          )}
        </div>
        <div className="right-section">
          <SquareButton
            onClick={this.onClickExport}
            content={
              <span>
                <img
                  className={this.state.fetchingExport ? "icon__loading" : ""}
                  style={{
                    margin: "0 5px 2px 0",
                    width: this.state.fetchingExport ? "22px" : "inherit",
                  }}
                  src={
                    this.state.fetchingExport
                      ? "/assets/icons/loading.gif"
                      : "/assets/icons/export.png"
                  }
                  alt="Downloading File"
                />
                Export
              </span>
            }
            bsStyle={"primary"}
            className="export-btn"
            disabled={
              this.state.orders && this.state.orders.length > 0 ? false : true
            }
          />
        </div>
      </>
    );
  };

  renderPurchaseOrderTracking = () => {
    return (
      <ModalBase
        show={true}
        onClose={this.toggleShowPurchaseOrder}
        titleElement={"Order Tracking Info"}
        bodyElement={<OrderTracking order={this.state.selectedOrder} />}
        footerElement={
          <SquareButton
            onClick={this.toggleShowPurchaseOrder}
            content={"Close"}
            bsStyle={"default"}
          />
        }
        className={`po-tracking-modal`}
      />
    );
  };

  render() {
    if (!this.getCustomerId()) return null;

    return (
      <>
        <div className="purchase-history purchase-list-panel">
          <div className="action-top-bar">{this.renderTopBar()}</div>
          <div className="purchase-list">
            <div className={`header column-8`}>
              <div
                className={`purchase-history-title ${this.getOrderClass(
                  "purchaseOrderNumber"
                )}`}
                onClick={() => this.fetchByOrder("purchaseOrderNumber")}
              >
                Purchase Order
              </div>
              <div
                className={`purchase-history-title  ${this.getOrderClass(
                  "customer_po_number"
                )}`}
                onClick={() => this.fetchByOrder("customer_po_number")}
              >
                Customer PO
              </div>
              <div className={`purchase-history-title no-ordering`}>
                Invoice Number
              </div>
              <div
                className={`purchase-history-title ${this.getOrderClass(
                  "opportunity_name"
                )}`}
                onClick={() => this.fetchByOrder("opportunity_name")}
              >
                Opportunity
              </div>
              <div
                className={`purchase-history-title  ${this.getOrderClass(
                  "purchaseOrderDate"
                )}`}
                onClick={() => this.fetchByOrder("purchaseOrderDate")}
              >
                Purchase Date
              </div>
              <div
                className={`purchase-history-title ${this.getOrderClass(
                  "product_id"
                )}`}
                onClick={() => this.fetchByOrder("product_id")}
              >
                Product ID
              </div>
              <div
                className={`purchase-history-title ${this.getOrderClass(
                  "description"
                )}`}
                onClick={() => this.fetchByOrder("description")}
              >
                Description
              </div>
              <div
                className={`purchase-history-title ${this.getOrderClass(
                  "quantity"
                )}`}
                onClick={() => this.fetchByOrder("quantity")}
              >
                Quantity
              </div>
              <div
                className={`purchase-history-title ${this.getOrderClass(
                  "received"
                )}`}
                onClick={() => this.fetchByOrder("received")}
              >
                Received Status
              </div>
            </div>
            <div
              id="purchaseHistoryList"
              style={{ position: "relative", height: "72vh", overflow: "auto" }}
            >
              {this.state.orders.length > 0 &&
                this.state.filteredOrders.map((data, index) =>
                  this.orderListingRows(data, index)
                )}
              {this.state.filteredOrders.length === 0 && (
                <div className="no-data">
                  {this.state.loading
                    ? "Loading..."
                    : "No Purchase History Available"}
                </div>
              )}
            </div>
          </div>
        </div>
        <PurchaseOrderDetail
          order={this.state.selectedOrder}
          show={this.state.showOrderDetail}
          loadingOrderDetails={this.state.fetchingOrderDetails}
          onClose={() => {
            this.setState({
              showOrderDetail: false,
              selectedOrder: undefined,
              fetchingOrderDetails: false,
            });
          }}
        />
        {this.state.showPurchaseOrderTracking &&
          this.renderPurchaseOrderTracking()}
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customerId: state.customer.customerId,
  customerProfile: state.customerUser.customerProfile,
});

const mapDispatchToProps = (dispatch: any) => ({
  getPurchaseHistories: (customerId: number) =>
    dispatch(getPurchaseHistories(customerId)),
  getPurchaseOrderDetail: (
    orderId: number,
    lineItemId: number,
    customer_po_number: number
  ) =>
    dispatch(getPurchaseOrderDetail(orderId, lineItemId, customer_po_number)),
  exportPurchaseOrderData: (customer_id: number) =>
    dispatch(exportPurchaseOrderData(customer_id)),
  getShipmentMethods: () => dispatch(getShipmentMethods()),
  getPurchaseOrderSettings: () => dispatch(getPurchaseOrderSettings()),
});

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseOrderList);
