import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import { pick, cloneDeep, debounce, DebouncedFunc, has, round } from "lodash";
import {
  createQuote,
  getSingleQuote,
  updateQuoteStage,
  getQuoteTypeList,
  GET_QUOTES_SUCCESS,
  CREATE_QUOTES_SUCCESS,
} from "../../../../actions/sow";
import {
  agreementCRUD,
  getAgreementPreview,
  getAgreementTemplate,
  getAgreementTemplateList,
  CREATE_AGREEMENT_FAILURE,
  CREATE_AGREEMENT_SUCCESS,
  DOWNLOAD_AGREEMENT_SUCCESS,
  DOWNLOAD_AGREEMENT_FAILURE,
} from "../../../../actions/agreement";
import {
  productCatalogCRUD,
  PRODUCT_CATALOG_SUCCESS,
} from "../../../../actions/sales";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../../../actions/documentation";
import {
  addErrorMessage,
  addInfoMessage,
  addSuccessMessage,
  addWarningMessage,
} from "../../../../actions/appState";
import {
  fetchQuoteDashboardListingPU,
  FETCH_QUOTE_LIST_SUCCESS,
} from "../../../../actions/dashboard";
import {
  getQuoteAllStages,
  GET_QUOTE_STAGES_SUCCESS,
} from "../../../../actions/setting";
import Products from "./products";
import CustomerUserNew from "./../../Sow/addUser";
import Spinner from "../../../../components/Spinner";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import Checkbox from "../../../../components/Checkbox/checkbox";
import Slider from "../../../../components/RangeSlider/maxSlider";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import SelectInput from "../../../../components/Input/Select/select";
import ConfirmBox from "../../../../components/ConfirmBox/ConfirmBox";
import Sections from "./../../AgreementTemplate/Create/agreementSections";
import PromptUnsaved from "../../../../components/UnsavedWarning/PromptUnsaved";
import AddQuote from "../../Sow/addQuote";
import PAX8Products from "./pax8Products";
import CatalogProducts from "./catalogProducts";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { calculateCosts } from "../../../../utils/agreementCalculations";
import "../../../../commonStyles/serviceCostCalculations.scss";
import "../../../../commonStyles/pax8_products.scss";
import "../../../../commonStyles/collapsable.scss";
import "./style.scss";

interface IAgreementCreateProps extends ICommonProps {
  loggenInUser: ISuperUser;
  quote: any;
  isFetchingTemplates: boolean;
  isFetching: boolean;
  quoteList: any;
  quoteFetching: boolean;
  customers: ICustomerShort[];
  isFetchingUsers: boolean;
  templates: IAgreementTemplate[];
  isFetchingAgreements: boolean;
  qTypeList: any;
  isFetchingQStageList: boolean;
  isFetchingSingleQuote: boolean;
  agreementCRUD: (
    method: HTTPMethods,
    agt: IAgreement,
    params?: any
  ) => Promise<any>;
  getAgreementTemplate: (id: number) => Promise<any>;
  getQuoteTypeList: any;
  getTemplateList: (
    showDisable: boolean,
    params: IServerPaginationParams
  ) => Promise<any>;
  createQuote: any;
  fetchCatalogProductDetails: (id: number) => Promise<any>;
  fetchAllCustomerUsers: (id: number) => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addWarningMessage: TShowWarningMessage;
  getSingleQuote: (id: number) => Promise<any>;
  updateQuoteStage: (id: number, stageId: number) => Promise<any>;
  fetchQuoteDashboardListing: (id: number, openOnly?: boolean) => Promise<any>;
  getAgreementPreview: (payload: IAgreement, id: number) => Promise<any>;
  getQuoteStages: () => Promise<any>;
}

interface BillingGroup {
  [billingTerm: string]: {
    [vendorName: string]: {
      cost: number;
      revenue: number;
      margin: number;
      margin_percent: number;
    };
  };
}
interface IAgreementCreateState {
  open: boolean;
  loading: boolean;
  agt: IAgreement;
  openPreview: boolean;
  loadingPreview: boolean;
  loadingQuotes: boolean;
  sendEmail: boolean;
  previewHTML: IFileResponse;
  groupByBilling: BillingGroup;
  error: {
    name: IFieldValidation;
    quote_id: IFieldValidation;
    stage: IFieldValidation;
    user: IFieldValidation;
    customer: IFieldValidation;
    template: IFieldValidation;
    products: IFieldValidation;
    pax8Products: IFieldValidation;
    catalogProducts: IFieldValidation;
  };
  agtError: boolean;
  users: any[];
  isCreateUserModal: boolean;
  isCreateOpportunityModal: boolean;
  errorList?: any;
  isPosting: boolean;
  isopenConfirm: boolean;
  quote: any;
  quoteList: IQuote[];
  stages: any[];
  stageLoading: boolean;
  unsaved: boolean;
}

const generateTableGroups = (billingGroup: BillingGroup): BillingGroup => {
  /*Merges the Consumption Billing Group into Monthly Group for LookingPoint vendor*/
  const newGroup = cloneDeep(billingGroup);
  let cost1 = 0,
    revenue1 = 0,
    margin1 = 0,
    cost2 = 0,
    revenue2 = 0,
    margin2 = 0,
    notFound = 0;
  if (newGroup["Monthly"] && newGroup["Monthly"]["LookingPoint"]) {
    cost1 = newGroup["Monthly"]["LookingPoint"].cost;
    revenue1 = newGroup["Monthly"]["LookingPoint"].revenue;
    margin1 = newGroup["Monthly"]["LookingPoint"].margin;
  } else notFound++;
  if (
    newGroup["Monthly Consumption"] &&
    newGroup["Monthly Consumption"]["LookingPoint"]
  ) {
    cost2 = newGroup["Monthly Consumption"]["LookingPoint"].cost;
    revenue2 = newGroup["Monthly Consumption"]["LookingPoint"].revenue;
    margin2 = newGroup["Monthly Consumption"]["LookingPoint"].margin;
    delete newGroup["Monthly Consumption"];
  } else {
    notFound++;
  }
  if (notFound === 2) return billingGroup;
  const lpMonthlyCost = cost1 + cost2;
  const lpMonthlyRevenue = revenue1 + revenue2;
  const lpMonthlyMargin = margin1 + margin2;
  const lpMarginPercent =
    lpMonthlyRevenue !== 0
      ? round((lpMonthlyMargin / lpMonthlyRevenue) * 100, 1)
      : 0;
  if (!newGroup["Monthly"]) newGroup["Monthly"] = {};
  newGroup["Monthly"]["LookingPoint"] = {
    cost: lpMonthlyCost,
    revenue: lpMonthlyRevenue,
    margin: lpMonthlyMargin,
    margin_percent: lpMarginPercent,
  };
  return newGroup;
};

const getBillingMultiplier = (
  billingTerm: PAX8BillingTerm | CatalogBillingTerm,
  isConsumption: boolean = false
): number => {
  return !isConsumption &&
    (billingTerm === "Monthly" ||
      billingTerm === "Monthly")
    ? 12
    : 1;
};

class CreateAgreement extends React.Component<
  IAgreementCreateProps,
  IAgreementCreateState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  private debouncedUpdate: DebouncedFunc<(discount: number) => void>;

  constructor(props: IAgreementCreateProps) {
    super(props);

    this.state = this.getEmptyState();
    this.debouncedUpdate = debounce(this.updateDiscount, 500);
  }

  getEmptyState = () => ({
    open: true,
    loading: false,
    loadingQuotes: false,
    agt: {
      id: 0,
      name: "",
      author_name: null,
      min_discount: 0,
      max_discount: 0,
      discount_percentage: 0,
      products: [],
      pax8_products: { name: null, products: null },
      sections: [],
      catalog_products: [],
    },
    openPreview: false,
    loadingPreview: false,
    sendEmail: false,
    previewHTML: null,
    error: {
      name: { ...CreateAgreement.emptyErrorState },
      quote_id: { ...CreateAgreement.emptyErrorState },
      stage: { ...CreateAgreement.emptyErrorState },
      user: { ...CreateAgreement.emptyErrorState },
      customer: { ...CreateAgreement.emptyErrorState },
      template: { ...CreateAgreement.emptyErrorState },
      products: { ...CreateAgreement.emptyErrorState },
      pax8Products: { ...CreateAgreement.emptyErrorState },
      catalogProducts: { ...CreateAgreement.emptyErrorState },
    },
    agtError: false,
    users: [],
    isCreateUserModal: false,
    isCreateOpportunityModal: false,
    isPosting: false,
    isopenConfirm: false,
    quote: {},
    quoteList: [],
    stageLoading: false,
    stages: [],
    groupByBilling: {},
    unsaved: false,
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    const query = new URLSearchParams(this.props.location.search);

    this.props.getTemplateList(false, { pagination: false });
    this.props.getQuoteTypeList();
    this.props
      .getQuoteStages()
      .then((action) => {
        if (action.type === GET_QUOTE_STAGES_SUCCESS) {
          this.setState({
            stages: action.response.map((stage) => ({
              value: stage.id,
              label: stage.label,
            })),
          });
        }
      })
      .finally(() => this.setState({ stageLoading: false }));

    if (id !== "0") {
      this.setState({ loading: true, stageLoading: true });
      this.props
        .agreementCRUD("get", { id } as IAgreement)
        .then((action) => {
          if (action.type === CREATE_AGREEMENT_SUCCESS) {
            const cloned = query.get("cloned");
            let agt: IAgreement = action.response;
            agt.products.forEach((productBundle) => {
              calculateCosts(productBundle, agt.discount_percentage || 0);
            });
            let groupByBilling: BillingGroup = {};
            let pax8Products =
              agt.pax8_products && agt.pax8_products.products
                ? agt.pax8_products.products
                : [];
            let catalogProducts = agt.catalog_products
              ? agt.catalog_products
              : [];
            groupByBilling = this.groupProductsByBillingTerm(
              pax8Products,
              catalogProducts
            );
            let unsaved = false;
            if (cloned === "true") {
              agt.id = 0;
              agt.name = agt.name + " (cloned)";
              agt.quote_id = null;
              agt.customer = null;
              agt.major_version = 1;
              agt.minor_version = 0;
              delete agt.is_current_version;
              delete agt.parent_agreement;
              delete agt.root_agreement;
              unsaved = true;
            } else {
              this.props.getSingleQuote(agt.quote_id);
              this.props.fetchAllCustomerUsers(agt.customer).then((a) => {
                if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
                  this.setState({
                    users: a.response,
                  });
                }
              });
            }
            this.setState({ agt, groupByBilling, unsaved });
          }
          this.setState({ loading: false });
        });
    }
    if (id === "0") {
      const template = query.get("template");
      const customer = Number(query.get("customer"));
      const rollback = query.get("rollback");
      if (template) {
        this.setValuesFromTemplate(Number(template));
      }
      if (rollback === "true") {
        const agreement: IAgreement = this.props.location.state;
        agreement.products.forEach((productBundle) =>
          calculateCosts(productBundle, agreement.discount_percentage || 0)
        );
        let groupByBilling: BillingGroup = {};
        let pax8Products =
          agreement.pax8_products && agreement.pax8_products.products
            ? agreement.pax8_products.products
            : [];
        let catalogProducts = agreement.catalog_products
          ? agreement.catalog_products
          : [];
        groupByBilling = this.groupProductsByBillingTerm(
          pax8Products,
          catalogProducts
        );
        // calculate costs for pax8 products (not sure right now)
        this.setState({ agt: agreement, groupByBilling });
        this.props.getSingleQuote(agreement.quote_id);
        this.props.fetchAllCustomerUsers(agreement.customer).then((a) => {
          if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
            this.setState({
              users: a.response,
            });
          }
        });
      }
      if (customer) {
        this.getQuotes(customer);
        this.props.fetchAllCustomerUsers(customer).then((a) => {
          if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
            this.setState({
              users: a.response,
            });
          }
        });
        this.setState({
          agt: {
            ...this.state.agt,
            customer: customer,
          },
        });
      }
    }
  }

  groupProductsByBillingTerm = (
    pax8Products: IPAX8Product[],
    catalogProducts: IQuotedCatalogProduct[]
  ): BillingGroup => {
    let groupByBilling: BillingGroup = {};

    const addToBillingTerm = (
      product: IPAX8Product | IQuotedCatalogProduct,
      billingTerm: string
    ) => {
      if (billingTerm in groupByBilling) {
        if (product.vendor_name in groupByBilling[billingTerm]) {
          let newCost =
            groupByBilling[billingTerm][product.vendor_name].cost +
            product.quantity * product.internal_cost;
          let newRevenue =
            groupByBilling[billingTerm][product.vendor_name].revenue +
            product.revenue;
          let newMargin =
            groupByBilling[billingTerm][product.vendor_name].margin +
            product.total_margin;
          let newMarginPercent =
            newRevenue !== 0 ? round((newMargin / newRevenue) * 100, 1) : 0;

          groupByBilling[billingTerm][product.vendor_name] = {
            cost: newCost,
            revenue: newRevenue,
            margin: newMargin,
            margin_percent: newMarginPercent,
          };
        } else {
          groupByBilling[billingTerm][product.vendor_name] = {
            cost: product.quantity * product.internal_cost,
            revenue: product.revenue,
            margin: product.total_margin,
            margin_percent: product.margin,
          };
        }
      } else
        groupByBilling[billingTerm] = {
          [product.vendor_name]: {
            cost: product.quantity * product.internal_cost,
            revenue: product.revenue,
            margin: product.total_margin,
            margin_percent: product.margin,
          },
        };
    };

    if (pax8Products && pax8Products.length) {
      pax8Products.forEach((product) => {
        if (
          !product.is_comment &&
          product.billing_term !== "Not Set" &&
          product.quantity
        ) {
          addToBillingTerm(
            product,
            product.billing_term === "One-Time"
              ? "One Time" // Because PAX8 One Time is spelled as One-Time
              : product.billing_term
          );
        }
      });
    }
    if (catalogProducts && catalogProducts.length) {
      catalogProducts.forEach((product) => {
        if (
          !product.is_comment &&
          product.billing_term !== "Not Set" &&
          product.quantity
        ) {
          addToBillingTerm(
            product,
            product.product_type === "Consumption" &&
              product.billing_term === "Monthly"
              ? "Monthly Consumption" // This will enable us to exclude this from Annual Projected calculations
              : product.billing_term
          );
        }
      });
    }
    return groupByBilling;
  };

  handleChangeName = (e) => {
    const newState = cloneDeep(this.state);
    newState.agt[e.target.name] = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  previewDoc = (event: any) => {
    if (this.checkStateValidation()) {
      this.setState({
        agtError: false,
        loadingPreview: true,
      });
      const agt = this.generatePayload(true);

      const data = pick(agt, [
        "name",
        "version_description",
        "sections",
        "products",
        "customer_cost",
        "customer",
        "major_version",
        "minor_version",
        "internal_cost",
        "discount_percentage",
        "margin",
        "margin_percentage",
        "quote_id",
        "author_name",
        "pax8_products",
        "user",
        "catalog_products",
        "catalog_products_name",
      ]);
      this.props.getAgreementPreview(data, 0).then((a) => {
        if (a.type === DOWNLOAD_AGREEMENT_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            loadingPreview: false,
          });
        }
        if (
          a.type === DOWNLOAD_AGREEMENT_FAILURE &&
          a.errorList &&
          a.errorList.data
        ) {
          this.setValidationErrors(a.errorList.data);
          this.setState({
            openPreview: false,
            loadingPreview: false,
          });
        }
      });
    } else {
      this.setState({ agtError: true });
    }
  };

  generatePayload = (forPreview: boolean = false) => {
    const sections = this.state.agt.sections;
    const products = this.state.agt.products;
    const pax_products =
      this.state.agt.pax8_products && this.state.agt.pax8_products.name
        ? this.state.agt.pax8_products.products
        : null;
    const agt = cloneDeep(this.state.agt);

    if (this.state.agt && sections) {
      {
        Object.keys(sections).map((k, i) => {
          sections[k].error = undefined;
          sections[k].content = commonFunctions.convertToMarkdown(
            sections[k].value_html
          );
        });
      }
      agt.sections = sections;
    }
    if (pax_products) {
      agt.pax8_products.products = pax_products.map((product) => {
        delete product.error;
        return product;
      });
    }
    let customer_cost_total: number = commonFunctions.arrSum(
      products.map((x) => x.customer_cost_total || 0)
    );
    let internal_cost_total: number = commonFunctions.arrSum(
      products.map((x) => x.internal_cost_total || 0)
    );
    let margin_total: number = customer_cost_total - internal_cost_total;
    let discount_total: number = commonFunctions.arrSum(
      products.map((x) => x.discount || 0)
    );
    if (discount_total) {
      customer_cost_total -= discount_total;
      margin_total -= discount_total;
    }

    agt.customer_cost = Math.round(customer_cost_total * 100) / 100;
    agt.internal_cost = Math.round(internal_cost_total * 100) / 100;
    agt.margin = Math.round(margin_total * 100) / 100;
    agt.total_discount = Math.round(discount_total * 100) / 100;
    agt.margin_percentage =
      Math.round((margin_total / customer_cost_total) * 10000) / 100 || 0;

    if (!forPreview) {
      if (this.state.agt && products) {
        products.forEach((bundle: IProductBundle) => {
          bundle.hasError = undefined;
          bundle.customer_cost_total = undefined;
          bundle.internal_cost_total = undefined;
          bundle.margin_total = undefined;
          bundle.discount = undefined;
          bundle.products.forEach((product) => {
            product.error = undefined;
          });
        });
        agt.products = products;
      }
      delete agt.minor_version;
      delete agt.major_version;
    }
    delete agt.forecast_data;
    return agt;
  };

  callSave = (closeDocument: boolean) => {
    if (this.checkStateValidation()) {
      const agt: IAgreement = this.generatePayload();
      this.setState({ loading: true, agtError: false });
      if (this.state.agt.id && this.state.agt.id !== 0) {
        const query = new URLSearchParams(this.props.location.search);
        const rollback = query.get("rollback") === "true" ? true : undefined;
        this.props
          .agreementCRUD("put", agt, {
            set_user_as_author: agt.set_user_as_author,
            update_major_version: agt.update_major_version,
            rollback: rollback,
            "email-account-manager": this.state.sendEmail,
          })
          .then((a) => {
            if (a.type === CREATE_AGREEMENT_SUCCESS) {
              if (this.state.sendEmail)
                this.props.addInfoMessage(
                  "Sending email to Account Manager..."
                );
              let agt_response: IAgreement = a.response;
              if (
                has(
                  agt_response,
                  "forecast_data.forecast_error_details.error_in_forecast_creation"
                ) &&
                agt_response.forecast_data.forecast_error_details
                  .error_in_forecast_creation
              ) {
                this.props.addWarningMessage(
                  agt_response.forecast_data.forecast_error_details
                    .error_message
                );
              }
              if (closeDocument) {
                this.setState(
                  {
                    unsaved: false,
                  },
                  () => {
                    this.props.history.goBack();
                  }
                );
              } else {
                agt_response.products.forEach((productBundle) => {
                  calculateCosts(
                    productBundle,
                    agt_response.discount_percentage || 0
                  );
                });
                this.setState({
                  agt: agt_response,
                  sendEmail: false,
                  unsaved: false,
                });
                this.props.history.replace(`/sow/agreement/${agt_response.id}`);
              }
            }

            if (
              a.type === CREATE_AGREEMENT_FAILURE &&
              a.errorList &&
              a.errorList.data
            ) {
              this.setValidationErrors(a.errorList.data);
            }
            this.setState({ loading: false });
          });
      } else {
        this.props
          .agreementCRUD("post", agt, {
            "email-account-manager": this.state.sendEmail,
          })
          .then((a) => {
            if (a.type === CREATE_AGREEMENT_SUCCESS) {
              if (this.state.sendEmail)
                this.props.addInfoMessage(
                  "Sending email to Account Manager..."
                );

              let agt_response: IAgreement = a.response;
              if (
                has(
                  agt_response,
                  "forecast_data.forecast_error_details.error_in_forecast_creation"
                ) &&
                agt_response.forecast_data.forecast_error_details
                  .error_in_forecast_creation
              ) {
                this.props.addWarningMessage(
                  agt_response.forecast_data.forecast_error_details
                    .error_message
                );
              }
              this.setState(
                {
                  unsaved: false,
                },
                () => {
                  this.props.history.goBack();
                }
              );
            }
            if (
              a.type === CREATE_AGREEMENT_FAILURE &&
              a.errorList &&
              a.errorList.data
            ) {
              this.setValidationErrors(a.errorList.data);
            }
            this.setState({ loading: false });
          });
      }
    } else {
      this.setState({ agtError: true });
    }
  };

  checkStateValidation = (): boolean => {
    let noError = true;
    const newState = cloneDeep(this.state);
    const errorObj = {
      errorState: "error",
      errorMessage: "This field may not be blank",
    };
    const validationFields = [
      "name",
      "template",
      "customer",
      "user",
      "quote_id",
    ];
    validationFields.forEach((field) => {
      if (
        !this.state.agt[field] ||
        (typeof this.state.agt[field] === "string" &&
          !this.state.agt[field].trim())
      ) {
        noError = false;
        newState.error[field] = { ...errorObj };
      }
    });
    // Boolean value to make sure all the required products are selected
    let requiredProductsChecked: boolean = true;
    newState.agt.products = this.state.agt.products.map((productBundle) => {
      let productError = false;
      requiredProductsChecked =
        requiredProductsChecked &&
        (productBundle.product_type === "Comment" ||
          !productBundle.is_required ||
          (productBundle.is_required &&
            productBundle.selected_product !== null));

      if (
        productBundle.product_type === "Comment" &&
        !productBundle.comment.trim()
      ) {
        noError = false;
        productError = true;
      } else {
        productBundle.products.forEach((product, idx) => {
          if (
            productBundle.is_required &&
            productBundle.is_checked &&
            productBundle.selected_product === idx &&
            !product.quantity
          ) {
            noError = false;
            productError = true;
            product.error = {
              errorState: "error",
              errorMessage: "Required",
            };
          } else {
            product.error = { ...CreateAgreement.emptyErrorState };
          }
        });
      }
      productBundle.hasError = productError;
      return productBundle;
    });

    if (!(this.state.agt.products.length === 0) && !requiredProductsChecked) {
      noError = false;
      newState.error.products = {
        ...errorObj,
        errorMessage: "All required products should be selected!",
      };
    } else {
      newState.error.products = { ...CreateAgreement.emptyErrorState };
    }

    if (this.state.agt.pax8_products && this.state.agt.pax8_products.name) {
      newState.agt.pax8_products.products = this.state.agt.pax8_products.products.map(
        (pax_product: IPAX8Product) => {
          if (
            !pax_product.is_comment &&
            !(
              pax_product.margin < 100 &&
              pax_product.margin >= 0 &&
              pax_product.billing_term !== "Not Set" &&
              pax_product.quantity >= pax_product.min_quantity &&
              pax_product.quantity <= pax_product.max_quantity
            )
          ) {
            noError = false;
            newState.error.pax8Products = {
              errorState: "error",
              errorMessage:
                "Please fill all the required fields properly for each quoted product.",
            };
            return { ...pax_product, error: true };
          } else if (pax_product.is_comment && !pax_product.comment.trim()) {
            noError = false;
            newState.error.pax8Products = {
              errorState: "error",
              errorMessage:
                "Please fill all the required fields properly for each quoted product.",
            };
            return { ...pax_product, error: true };
          } else {
            return { ...pax_product, error: false };
          }
        }
      );
    }
    newState.agt.catalog_products = this.state.agt.catalog_products.map(
      (product) => {
        if (
          !product.is_comment &&
          !(
            product.margin < 100 &&
            product.margin >= 0 &&
            product.billing_term !== "Not Set"
          )
        ) {
          noError = false;
          newState.error.catalogProducts = commonFunctions.getErrorState(
            "Please fill all the required fields properly for each quoted product."
          );
          return { ...product, error: true };
        } else if (product.is_comment && !product.comment.trim()) {
          noError = false;
          newState.error.catalogProducts = commonFunctions.getErrorState(
            "Please fill all the required fields properly for each quoted product."
          );
          return { ...product, error: true };
        } else {
          return { ...product, error: false };
        }
      }
    );
    newState.agt.sections = this.state.agt.sections.map((section) => {
      if (
        !section.is_locked &&
        (!section.name.trim() ||
          commonFunctions.isEditorEmpty(section.value_html))
      ) {
        noError = false;
        section.error = {
          errorState: "error",
          errorMessage: "This field is required",
        };
      } else {
        section.error = { ...CreateAgreement.emptyErrorState };
      }
      return section;
    });
    this.setState(newState);
    return noError;
  };

  setValidationErrors = (errorList: object) => {
    const error = this.getEmptyState().error;
    const agt = cloneDeep(this.state.agt);

    Object.keys(errorList).map((key) => {
      if (key && error[key]) {
        error[key].errorState = "error";
        error[key].errorMessage = errorList[key];
      }
    });

    const sections = agt.sections;

    if (sections) {
      Object.keys(sections).map((key) => {
        const boardContainerData = sections[key];
        {
          Object.keys(boardContainerData).map((k, i) => {
            if (k !== "section_label" && k !== "ordering") {
              if (
                boardContainerData[k] &&
                boardContainerData[k].type === "MARKDOWN"
              ) {
                sections[key][k].value = sections[key][k].value_html
                  ? sections[key][k].value_html
                  : sections[key][k].value;

                sections[key][k].markdownKey = Math.random();
              }
            }
          });
        }
      });
    }
    agt.sections = sections;
    this.setState({ agt, error, agtError: true });
  };

  callbackfnSections = (sections: ISection[]) => {
    const newState = cloneDeep(this.state);
    newState.agt.sections = sections;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  callbackfnProduct = (products: IProductBundle[]) => {
    const newState = cloneDeep(this.state);
    newState.agt.products = products;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addToQuotedProducts = (
    product?: IPAX8ProductDetail,
    comment: boolean = false
  ) => {
    let newProduct: IPAX8Product;
    if (comment)
      newProduct = {
        id: Math.round(Math.random() * 100000000),
        product_crm_id: null,
        sku: null,
        product_name: null,
        short_description: null,
        vendor_name: null,
        billing_term: null,
        commitment_term: null,
        commitment_term_in_months: null,
        min_quantity: null,
        max_quantity: null,
        pricing_type: null,
        unit_of_measurement: null,
        partner_buy_rate: null,
        suggested_retail_price: null,
        start_quantity_range: null,
        end_quantity_range: null,
        charge_type: null,
        internal_cost: null,
        customer_cost: null,
        margin: null,
        revenue: null,
        total_margin: null,
        quantity: null,
        is_comment: true,
        comment: "",
      };
    else
      newProduct = {
        id: Math.round(Math.random() * 100000000),
        product_crm_id: product.product_crm_id,
        sku: product.sku,
        product_name: product.product_name,
        short_description: product.short_description,
        vendor_name: product.vendor_name,
        billing_term: "Not Set",
        commitment_term: null,
        commitment_term_in_months: null,
        pricing_type: null,
        unit_of_measurement: null,
        partner_buy_rate: 0,
        suggested_retail_price: 0,
        start_quantity_range: 0,
        end_quantity_range: 0,
        charge_type: null,
        internal_cost: 0,
        customer_cost: 0,
        margin: 0,
        revenue: 0,
        total_margin: 0,
        quantity: 0,
        is_comment: false,
        comment: null,
      };

    this.handleChangePAX8Products([
      ...this.state.agt.pax8_products.products,
      newProduct,
    ]);
  };

  handleChangePAX8Products = (products: IPAX8Product[]) => {
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        pax8_products: { ...prevState.agt.pax8_products, products },
      },
      groupByBilling: this.groupProductsByBillingTerm(
        products,
        this.state.agt.catalog_products
      ),
      unsaved: true,
    }));
  };

  addToQuotedCatalogProducts = (
    product?: IQuotedCatalogProduct,
    comment: boolean = false
  ) => {
    if (comment) {
      const newComment: IQuotedCatalogProduct = {
        id: Math.round(Math.random() * 100000000),
        product_id: null,
        name: null,
        description: null,
        revenue_rounding: null,
        measurement_unit: null,
        no_of_units: null,
        product_type: null,
        pricing_data: null,
        billing_term: null,
        contract_term: null,
        internal_cost: null,
        customer_cost: null,
        margin: null,
        revenue: null,
        total_margin: null,
        quantity: null,
        is_required: null,
        vendor_name: null,
        is_comment: true,
        comment: "",
      };
      this.handleChangeCatalogProducts([
        ...this.state.agt.catalog_products,
        newComment,
      ]);
    } else
      this.props.fetchCatalogProductDetails(product.id).then((action) => {
        if (action.type === PRODUCT_CATALOG_SUCCESS) {
          const productInfo: IProductCatalogItem = action.response;
          const quotedProduct: IQuotedCatalogProduct = {
            ...productInfo,
            billing_term: "Not Set",
            contract_term: null,
            internal_cost: 0,
            customer_cost: 0,
            margin: 0,
            revenue: 0,
            total_margin: 0,
            quantity: 0,
            is_comment: false,
            vendor_name: "LookingPoint",
            is_required: false,
            comment: null,
          };
          this.handleChangeCatalogProducts([
            ...this.state.agt.catalog_products,
            quotedProduct,
          ]);
        }
      });
  };

  handleChangeCatalogProducts = (products: IQuotedCatalogProduct[]) => {
    const pax8Products = this.state.agt.pax8_products.products
      ? this.state.agt.pax8_products.products
      : [];
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        catalog_products: products,
      },
      groupByBilling: this.groupProductsByBillingTerm(pax8Products, products),
      unsaved: true,
    }));
  };

  handleChangeCatalogProductsName = (name: string) => {
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        catalog_products_name: name,
      },
      unsaved: true,
    }));
  };

  onChecboxChangeAgreement = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.agt[event.target.name] = event.target.checked;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  setValuesFromTemplate = (templateId: number) => {
    this.setState({ loading: true });
    this.props
      .getAgreementTemplate(templateId)
      .then((action) => {
        if (action.type === "CREATE_TEMPLATE_SUCCESS") {
          const templateResponse: IAgreementTemplate = action.response;
          templateResponse.products.forEach((productBundle) => {
            calculateCosts(productBundle, 0);
          });
          let groupByBilling: BillingGroup = {};
          let pax8Products =
            templateResponse.pax8_products &&
            templateResponse.pax8_products.products
              ? templateResponse.pax8_products.products
              : [];
          let catalogProducts = templateResponse.catalog_products
            ? templateResponse.catalog_products
            : [];
          groupByBilling = this.groupProductsByBillingTerm(
            pax8Products,
            catalogProducts
          );
          this.setState({
            agt: {
              ...this.state.agt,
              template: templateId,
              discount_percentage: 0,
              min_discount: templateResponse.min_discount,
              max_discount: templateResponse.max_discount,
              sections: templateResponse.sections,
              products: templateResponse.products,
              pax8_products: templateResponse.pax8_products,
              catalog_products: catalogProducts,
              catalog_products_name: templateResponse.catalog_products_name,
            },
            unsaved: true,
            groupByBilling,
          });
        }
      })
      .finally(() => this.setState({ loading: false }));
  };

  updateDiscount = (discount: number) => {
    const newState = cloneDeep(this.state);
    newState.agt.products.forEach((product: IProductBundle) =>
      calculateCosts(product, discount)
    );
    // add discount support for pax8 Products
    this.setState(newState);
  };

  handleChangeCustomer = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.agt[e.target.name] = e.target.value;
    newState.agt.user = null;
    newState.agt.quote_id = null;
    this.setState(newState);
    (newState.unsaved as boolean) = true;
    this.getQuotes(e.target.value);
    this.props.fetchAllCustomerUsers(Number(e.target.value)).then((a) => {
      if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const ns = cloneDeep(this.state);
        (ns.users as any) = a.response;
        this.setState(ns);
      }
    });
  };

  getQuotes = (id) => {
    this.setState({ loadingQuotes: true });
    this.props.fetchQuoteDashboardListing(id, true).then((a) => {
      if (a.type === FETCH_QUOTE_LIST_SUCCESS) {
        const ns = cloneDeep(this.state);
        (ns.quoteList as any) = a.response;
        (ns.loadingQuotes as boolean) = false;
        this.setState(ns);
      }
    });
  };

  getTemplateOptions = () => {
    const templates =
      this.props.templates && Array.isArray(this.props.templates)
        ? this.props.templates.map((t) => ({
            value: t.id,
            label: t.name,
            disabled: false,
          }))
        : [];

    return templates;
  };

  getCustomerOptions = () => {
    if (this.props.customers && this.props.customers.length > 0) {
      return this.props.customers.map((role) => ({
        value: role.id,
        label: role.name,
        disabled: false,
      }));
    } else {
      return [];
    }
  };

  getCustomerUserOptions = () => {
    const users = this.state.users
      ? this.state.users.map((t) => ({
          value: t.id,
          label: `${t.first_name} ${t.last_name}`,
          disabled: false,
        }))
      : [];

    return users;
  };

  getOpportunityOptions = () => {
    const quoteList = this.state.quoteList
      ? this.state.quoteList.map((t) => ({
          value: t.id,
          label: `${t.name} (${t.stage_name})`,
          disabled: false,
        }))
      : [];

    return quoteList;
  };

  createQuote = (data: any) => {
    this.setState({ isPosting: true });
    const customerId = this.state.agt.customer;
    if (customerId) {
      (data.customer_id as any) = customerId;
      (data.user_id as any) = this.state.agt.user;
      this.props
        .createQuote(customerId, data)
        .then((action) => {
          if (action.type === CREATE_QUOTES_SUCCESS) {
            this.setState({
              isCreateOpportunityModal: false,
            });
            this.getQuotes(customerId);
          } else {
            this.setState({
              isCreateOpportunityModal: true,
              errorList: action.errorList.data,
              quote: data,
            });
          }
          this.setState({ isPosting: false });
        })
        .catch(() => {
          this.setState({ isPosting: false });
        });
    }
  };

  handleChangeQuotes = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.value !== "0") {
      const newState = cloneDeep(this.state);
      newState.agt[e.target.name] = e.target.value;
      this.setState(newState);
    }
  };

  handleChangeQuote = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.toggleConfirmOpen(e.target.value);
  };

  toggleConfirmOpen = (e) => {
    this.setState((prevState) => ({
      isopenConfirm: !prevState.isopenConfirm,
      agt: {
        ...prevState.agt,
        stage_id: e,
      },
      unsaved: true,
    }));
  };

  onClickConfirm = () => {
    this.props
      .updateQuoteStage(this.props.quote.id, this.state.agt.stage_id)
      .then((action) => {
        if (action.type === GET_QUOTES_SUCCESS) {
          const newState: IAgreementCreateState = cloneDeep(this.state);
          newState.error.stage.errorState = "success";
          newState.error.stage.errorMessage = "";
          (newState.isopenConfirm as boolean) = false;
          this.setState(newState);
        } else {
          const newState: IAgreementCreateState = cloneDeep(this.state);
          newState.error.stage.errorState = "error";
          newState.error.stage.errorMessage =
            (action.errorList &&
              action.errorList.data &&
              action.errorList.data.detail) ||
            "Something went wrong";
          (newState.isopenConfirm as boolean) = false;
          (newState.agt.id as any) = 0;
          this.setState(newState);
        }
      });
  };
  toggleCreateUserModal = () => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
  };

  toggleCreateOpportunityModal = () => {
    this.setState((prevState) => ({
      isCreateOpportunityModal: !prevState.isCreateOpportunityModal,
    }));
  };

  handleChangeUser = (e) => {
    if (e.target.value === 0) {
      this.toggleCreateUserModal();
    } else {
      const newState = cloneDeep(this.state);
      newState.agt[e.target.name] = e.target.value;
      (newState.unsaved as boolean) = true;
      this.setState(newState);
    }
  };

  closeUserModal = () => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
    this.props.fetchAllCustomerUsers(this.state.agt.customer).then((a) => {
      if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const ns = cloneDeep(this.state);
        (ns.users as any) = a.response;
        this.setState(ns);
      }
    });
  };

  renderProductServiceCost = () => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: 2,
    });

    const pax8Products = Boolean(
      this.state.agt.pax8_products && this.state.agt.pax8_products.name
    )
      ? this.state.agt.pax8_products.products
      : [];
    const products = this.state.agt.catalog_products;
    const billingGroup = this.state.groupByBilling;

    const annTotalRevenue: number = commonFunctions.arrSum(
      [...pax8Products, ...products]
        .filter((el) => !el.is_comment)
        .map(
          (el) =>
            getBillingMultiplier(
              el.billing_term,
              (el as IQuotedCatalogProduct).product_type ===
                "Consumption"
            ) * el.revenue
        )
    );

    const annTotalCost: number = commonFunctions.arrSum(
      [...pax8Products, ...products]
        .filter((el) => !el.is_comment)
        .map(
          (el) =>
            getBillingMultiplier(
              el.billing_term,
              (el as IQuotedCatalogProduct).product_type ===
                "Consumption"
            ) *
            el.internal_cost *
            el.quantity
        )
    );

    const annTotalMargin: number = commonFunctions.arrSum(
      [...pax8Products, ...products]
        .filter((el) => !el.is_comment)
        .map(
          (el) =>
            getBillingMultiplier(
              el.billing_term,
              (el as IQuotedCatalogProduct).product_type ===
                "Consumption"
            ) * el.total_margin
        )
    );

    const annTotalMarginPercent: number =
      annTotalRevenue !== 0
        ? round((annTotalMargin / annTotalRevenue) * 100, 1)
        : 0;

    const tableBilling = generateTableGroups(billingGroup);

    return (
      <div className="calculations pax8-calculations">
        <div className="sub-heading col-md-12">
          <div className="text">SERVICE COST</div>
        </div>
        {Object.keys(tableBilling)
          .sort((a, b) => b.localeCompare(a))
          .map((billingTerm: PAX8BillingTerm, externalIdx: number) => {
            const billingTerms: string[] = Object.keys(
              tableBilling[billingTerm]
            );
            const totalRevenue: number = commonFunctions.arrSum(
              billingTerms.map(
                (vendor) => tableBilling[billingTerm][vendor].revenue
              )
            );
            const totalCost: number = commonFunctions.arrSum(
              billingTerms.map(
                (vendor) => tableBilling[billingTerm][vendor].cost
              )
            );
            const totalMargin: number = commonFunctions.arrSum(
              billingTerms.map(
                (vendor) => tableBilling[billingTerm][vendor].margin
              )
            );
            const totalMarginPercent: number =
              totalRevenue !== 0
                ? round((totalMargin / totalRevenue) * 100, 1)
                : 0;

            return (
              <div className="calculations-table" key={externalIdx}>
                <h4>{`${billingTerm} Billed Details`}</h4>
                <div className="calculations-table-header">
                  <div className="calculations-header-title">Vendor</div>
                  <div className="calculations-header-title">{`${billingTerm} Revenue`}</div>
                  <div className="calculations-header-title">{`${billingTerm} Cost`}</div>
                  <div className="calculations-header-title">Margin $</div>
                  <div className="calculations-header-title">Margin %</div>
                </div>
                <div className="calculations-table-body">
                  {billingTerms
                    .sort((a, b) => b.localeCompare(a))
                    .map((vendorName: string, internalIdx) => (
                      <div className="calculations-table-row" key={internalIdx}>
                        <div className="calculations-table-col">
                          {vendorName}
                        </div>
                        <div className="calculations-table-col">
                          {formatter.format(
                            tableBilling[billingTerm][vendorName].revenue
                          )}
                        </div>
                        <div className="calculations-table-col">
                          {formatter.format(
                            tableBilling[billingTerm][vendorName].cost
                          )}
                        </div>
                        <div className="calculations-table-col">
                          {formatter.format(
                            tableBilling[billingTerm][vendorName].margin
                          )}
                        </div>
                        <div className="calculations-table-col">
                          {tableBilling[billingTerm][vendorName]
                            .margin_percent + "%"}
                        </div>
                      </div>
                    ))}
                  <div className="calculations-table-row">
                    <div className="calculations-table-col">Totals:</div>
                    <div className="calculations-table-col">
                      {formatter.format(totalRevenue)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(totalCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(totalMargin)}
                    </div>
                    <div className="calculations-table-col">
                      {totalMarginPercent + "%"}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}

        <div className="calculations-table">
          <h4>{`Annual Projected Subscription Value`}</h4>
          <div className="calculations-table-header">
            <div className="calculations-header-title">Vendor</div>
            <div className="calculations-header-title">Annual Revenue</div>
            <div className="calculations-header-title">Cost</div>
            <div className="calculations-header-title">Margin $</div>
            <div className="calculations-header-title">Margin %</div>
          </div>
          <div className="calculations-table-body">
            {Object.keys(billingGroup)
              .sort((a, b) => b.localeCompare(a))
              .map((billingTerm) => {
                return Object.keys(billingGroup[billingTerm]).map(
                  (vendorName, idx) => (
                    <div className="calculations-table-row" key={idx}>
                      <div className="calculations-table-col">
                        {billingTerm + " " + vendorName}
                      </div>
                      <div className="calculations-table-col">
                        {formatter.format(
                          getBillingMultiplier(
                            billingTerm as PAX8BillingTerm | CatalogBillingTerm,
                            billingTerm === "Monthly Consumption"
                          ) * billingGroup[billingTerm][vendorName].revenue
                        )}
                      </div>
                      <div className="calculations-table-col">
                        {formatter.format(
                          getBillingMultiplier(
                            billingTerm as PAX8BillingTerm | CatalogBillingTerm,
                            billingTerm === "Monthly Consumption"
                          ) * billingGroup[billingTerm][vendorName].cost
                        )}
                      </div>
                      <div className="calculations-table-col">
                        {formatter.format(
                          getBillingMultiplier(
                            billingTerm as PAX8BillingTerm | CatalogBillingTerm,
                            billingTerm === "Monthly Consumption"
                          ) * billingGroup[billingTerm][vendorName].margin
                        )}
                      </div>
                      <div className="calculations-table-col">
                        {billingGroup[billingTerm][vendorName].margin_percent +
                          "%"}
                      </div>
                    </div>
                  )
                );
              })}
            <div className="calculations-table-row">
              <div className="calculations-table-col"></div>
              <div className="calculations-table-col"></div>
              <div className="calculations-table-col"></div>
              <div className="calculations-table-col"></div>
              <div className="calculations-table-col"></div>
            </div>
            <div className="calculations-table-row">
              <div className="calculations-table-col">Annualized Totals:</div>
              <div className="calculations-table-col">
                {formatter.format(annTotalRevenue)}
              </div>
              <div className="calculations-table-col">
                {formatter.format(annTotalCost)}
              </div>
              <div className="calculations-table-col">
                {formatter.format(annTotalMargin)}
              </div>
              <div className="calculations-table-col">
                {annTotalMarginPercent + "%"}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    const query = new URLSearchParams(this.props.location.search);
    const cloned = query.get("cloned");

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: 0,
    });

    let customer_cost_total: number = commonFunctions.arrSum(
      this.state.agt.products.map((x) => x.customer_cost_total || 0)
    );
    let internal_cost_total: number = commonFunctions.arrSum(
      this.state.agt.products.map((x) => x.internal_cost_total || 0)
    );
    let margin_total: number = commonFunctions.arrSum(
      this.state.agt.products.map((x) => x.margin_total || 0)
    );
    let discount_total: number = commonFunctions.arrSum(
      this.state.agt.products.map((x) => x.discount || 0)
    );

    // let pax_customer_cost_total: number, pax_margin_total: number;
    // if (this.state.agt.pax8_products && this.state.agt.pax8_products.products) {
    //   pax_customer_cost_total = commonFunctions.arrSum(
    //     this.state.agt.pax8_products.products.map((x) => x.revenue)
    //   );
    //   pax_margin_total = commonFunctions.arrSum(
    //     this.state.agt.pax8_products.products.map((x) => x.total_margin)
    //   );
    // }
    return (
      <div className="agr-container agreement-template-create">
        <div className="add-agreement col-md-10">
          <div className="agr-add-edit-header">
            <h3>
              {this.state.agt.id && this.state.agt.id !== 0
                ? "Edit"
                : this.props.isFetchingAgreements
                ? ""
                : "Add"}{" "}
              Agreement
            </h3>
          </div>
          <div className="loader">
            <Spinner
              show={
                this.state.loading ||
                this.props.isFetching ||
                this.state.loadingPreview
              }
            />
          </div>
          <div className="basic-field">
            <Input
              field={{
                value: this.state.agt.name,
                label: "Name",
                type: "TEXT",
                isRequired: true,
              }}
              width={6}
              name={"name"}
              placeholder="Enter Agreement name"
              onChange={(e) => this.handleChangeName(e)}
              className="select-type"
              error={this.state.error.name}
            />

            {this.state.agt.id === 0 && cloned !== "true" ? (
              <div
                className="select-type select-agr
          field-section col-lg-6 col-md-6"
              >
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Select Template
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.error.template.errorMessage &&
                    !this.state.agt.template
                      ? `error-input`
                      : ""
                  }`}
                >
                  <SelectInput
                    name="template"
                    value={this.state.agt.template}
                    onChange={(e) =>
                      this.setValuesFromTemplate(Number(e.target.value))
                    }
                    options={this.getTemplateOptions()}
                    multi={false}
                    searchable={true}
                    placeholder="Select Template"
                    loading={this.props.isFetchingTemplates}
                    disabled={
                      this.state.agt.id && this.state.agt.id !== 0
                        ? true
                        : false
                    }
                  />
                </div>
                {this.state.error.template.errorMessage &&
                  !this.state.agt.template && (
                    <div className="select-agr-error">
                      {this.state.error.template.errorMessage}
                    </div>
                  )}
              </div>
            ) : (
              <div
                className="select-type select-sow
                opportunity quote-name-doc-section
              field-section col-lg-6 col-md-6"
              >
                <div className="field__label row ">
                  <label className="field__label-label" title="">
                    <span className="template-label">Template</span>
                    <img
                      id="redirect-template"
                      alt="Redirect to Template"
                      src={"/assets/icons/redirect.png"}
                      onClick={() => {
                        window.open(
                          `/setting/agreement-template/${this.state.agt.template}?disabled=true`,
                          "_blank"
                        );
                      }}
                    />
                  </label>
                </div>
                <div className="quote-name-doc">
                  {this.state.agt.template_name}
                </div>
              </div>
            )}
            <div
              className="select-type select-agr
          field-section col-lg-6 col-md-6"
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Select Customer
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.customer.errorMessage &&
                  !this.state.agt.customer
                    ? `error-input`
                    : ""
                }`}
              >
                <SelectInput
                  name="customer"
                  value={this.state.agt.customer}
                  onChange={(e) => this.handleChangeCustomer(e)}
                  options={this.getCustomerOptions()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Customer"
                  disabled={
                    this.state.agt.id && this.state.agt.id !== 0 ? true : false
                  }
                />
              </div>
              <div className="select-agr-error">
                {this.state.error.customer.errorMessage &&
                !this.state.agt.customer
                  ? this.state.error.customer.errorMessage
                  : ""}
              </div>
            </div>
            <div
              className="select-type select-agr opportunity add-new-option-section
          field-section col-lg-6 col-md-6"
            >
              <div className="add-new-option-box">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Select User
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.error.user.errorMessage && !this.state.agt.user
                      ? `error-input`
                      : ""
                  }`}
                >
                  <SelectInput
                    name="user"
                    value={this.state.agt.user}
                    onChange={(e) => this.handleChangeUser(e)}
                    options={this.getCustomerUserOptions()}
                    multi={false}
                    searchable={true}
                    placeholder="Select User"
                    disabled={!this.state.agt.customer}
                    loading={this.props.isFetchingUsers}
                  />
                </div>
                {this.state.error.user.errorMessage && !this.state.agt.user && (
                  <div className="select-agr-error">
                    {this.state.error.user.errorMessage}
                  </div>
                )}
              </div>
              <SquareButton
                content="+"
                onClick={(e) => this.toggleCreateUserModal()}
                className="add-new-option-agr"
                bsStyle={"primary"}
                title="Add New User"
                disabled={!this.state.agt.customer}
              />
            </div>
            {this.state.agt.id !== 0 && this.props.quote && (
              <div
                className="select-type select-agr
              opportunity quote-name-doc-section
            field-section col-lg-6 col-md-6"
              >
                <div className="field__label row ">
                  <label className="field__label-label" title="">
                    Opportunity
                  </label>
                </div>
                <div className="quote-name-doc">
                  {this.props.quote.name} {`(${this.props.quote.stage_name})`}
                </div>
                {this.state.error.quote_id.errorMessage && (
                  <div className="agr-validations-error-quote">
                    {this.state.error.quote_id.errorMessage}
                  </div>
                )}
              </div>
            )}
            {this.state.agt.id !== 0 && this.props.quote && (
              <Input
                field={{
                  value: this.props.quote.stage_id,
                  label: "Stage",
                  type: "PICKLIST",
                  isRequired: true,
                  options: this.state.stages,
                }}
                width={6}
                name={"stage_id"}
                placeholder="Select Stage"
                onChange={(e) => this.handleChangeQuote(e)}
                className="select-type"
                loading={this.props.quoteFetching || this.state.stageLoading}
                error={this.state.error.stage}
              />
            )}
            {(this.state.agt.id === 0 ||
              (this.state.agt.id !== 0 &&
                this.props.quote === null &&
                this.props.isFetchingSingleQuote === false)) && (
              <div
                className="select-type select-agr
              opportunity add-new-option-section
            field-section col-lg-6 col-md-6"
              >
                <div className="add-new-option-box">
                  <div className="field__label row ">
                    <label className="field__label-label" title="">
                      Select Opportunity
                    </label>
                    <span className="field__label-required" />
                  </div>
                  <div
                    className={`${
                      this.state.error.quote_id.errorMessage &&
                      !this.state.agt.quote_id
                        ? `error-input`
                        : ""
                    }`}
                  >
                    <SelectInput
                      name="quote_id"
                      value={this.state.agt.quote_id}
                      onChange={(e) => this.handleChangeQuotes(e)}
                      options={this.getOpportunityOptions()}
                      multi={false}
                      searchable={true}
                      placeholder="Select Opportunity"
                      loading={this.state.loadingQuotes}
                      disabled={
                        !this.state.agt.customer || !this.state.agt.user
                      }
                    />
                  </div>
                  {this.state.error.quote_id.errorMessage &&
                    !this.state.agt.quote_id && (
                      <div className="select-agr-error">
                        {this.state.error.quote_id.errorMessage}
                      </div>
                    )}
                </div>
                <SquareButton
                  content="+"
                  onClick={(e) => this.toggleCreateOpportunityModal()}
                  className="add-new-option-agr"
                  bsStyle={"primary"}
                  title="Add New Opportunity"
                  disabled={!this.state.agt.customer || !this.state.agt.user}
                />
              </div>
            )}

            {!this.state.loading && Boolean(this.state.agt.sections.length) && (
              <Sections
                sections={this.state.agt.sections}
                setSections={this.callbackfnSections}
                isAgreement={true}
              />
            )}
            {Boolean(
              this.state.agt.pax8_products && this.state.agt.pax8_products.name
            ) && (
              <PAX8Products
                pax8Name={this.state.agt.pax8_products.name}
                products={this.state.agt.pax8_products.products}
                productsError={this.state.error.pax8Products}
                handleChangeProducts={this.handleChangePAX8Products}
                addToQuotedProducts={this.addToQuotedProducts}
              />
            )}
            <CatalogProducts
              catalogName={this.state.agt.catalog_products_name}
              products={this.state.agt.catalog_products}
              productsError={this.state.error.catalogProducts}
              handleChangeProducts={this.handleChangeCatalogProducts}
              addToQuotedProducts={this.addToQuotedCatalogProducts}
            />
            {!this.state.loading && this.state.agt.products.length !== 0 && (
              <Products
                discount={this.state.agt.discount_percentage}
                products={this.state.agt.products}
                setProductsFn={this.callbackfnProduct}
                productError={this.state.error.products}
              />
            )}

            {this.state.agt.products.length !== 0 && (
              <div className="service-cost-agreement-temp col-md-12">
                <div className="section-heading">{"SERVICE COST"}</div>

                <div
                  id="agr-service-cost-content"
                  className="sub-heading col-md-12"
                >
                  <div className="field-section col-xs-4">
                    <div className="field__label row">
                      <label
                        className="field__label-label"
                        title="Customer Cost(In $)"
                      >
                        Customer Cost(In $)
                      </label>
                    </div>
                    <div className="agr-service-cost-value">
                      {discount_total ? (
                        <>
                          <span className="agr-product-prev-cost">
                            {formatter.format(customer_cost_total)}
                          </span>
                          <span className="agr-product-new-cost">
                            {formatter.format(
                              customer_cost_total - discount_total
                            )}
                          </span>
                        </>
                      ) : (
                        formatter.format(customer_cost_total)
                      )}
                    </div>
                  </div>

                  <div className="field-section col-xs-4">
                    <div className="field__label row">
                      <label
                        className="field__label-label"
                        title="Margin(In $)"
                      >
                        Margin(In $)
                      </label>
                    </div>
                    <div className="agr-service-cost-value">
                      {discount_total ? (
                        <>
                          <span className="agr-product-prev-cost">
                            {formatter.format(margin_total)}
                          </span>
                          <span className="agr-product-new-cost">
                            {formatter.format(margin_total - discount_total)}
                          </span>
                        </>
                      ) : (
                        formatter.format(margin_total)
                      )}
                    </div>
                  </div>
                  <div className="field-section col-xs-4">
                    <div className="field__label row">
                      <label className="field__label-label" title="Margin %">
                        Margin %
                      </label>
                    </div>
                    <div className="agr-service-cost-value">
                      {!customer_cost_total ? (
                        "0.00"
                      ) : discount_total ? (
                        <>
                          <span className="agr-product-prev-cost">
                            {(
                              (margin_total / customer_cost_total) *
                              100
                            ).toFixed(2)}
                          </span>
                          <span className="agr-product-new-cost">
                            {(
                              ((margin_total - discount_total) /
                                (customer_cost_total - discount_total)) *
                              100
                            ).toFixed(2)}
                          </span>
                        </>
                      ) : (
                        ((margin_total / customer_cost_total) * 100).toFixed(2)
                      )}
                    </div>
                  </div>
                  <div className="field-section col-xs-4">
                    <div className="field__label row">
                      <label
                        className="field__label-label"
                        title="Internal Cost(In $)"
                      >
                        Internal Cost(In $)
                      </label>
                    </div>
                    <div className="agr-service-cost-value">
                      {formatter.format(internal_cost_total)}
                    </div>
                  </div>
                  {Boolean(discount_total) && (
                    <div className="field-section col-xs-4">
                      <div className="field__label row">
                        <label
                          className="field__label-label"
                          title="Discount (In $)"
                        >
                          Discount(In $)
                        </label>
                      </div>
                      <div className="agr-service-cost-value">
                        {formatter.format(discount_total)}
                      </div>
                    </div>
                  )}
                  <div className="range-box col-xs-12">
                    <div className="range-box-label">Discount Control</div>
                    <div className="slider">
                      <Slider
                        min={0}
                        max={this.state.agt.max_discount}
                        maxVal={this.state.agt.discount_percentage}
                        thumbsize={10}
                        onChange={(e) => {
                          this.setState({
                            agt: {
                              ...this.state.agt,
                              discount_percentage: e.maxVal,
                            },
                            unsaved: true,
                          });
                          this.debouncedUpdate(e.maxVal);
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            )}

            {this.renderProductServiceCost()}

            <Input
              field={{
                value: this.state.agt.version_description,
                label: "Version Description",
                type: "TEXTAREA",
                isRequired: false,
              }}
              width={8}
              name={"version_description"}
              placeholder=" "
              onChange={(e) => this.handleChangeName(e)}
              className="version-description"
            />
            <AddQuote
              show={this.state.isCreateOpportunityModal}
              onClose={this.toggleCreateOpportunityModal}
              onSubmit={this.createQuote}
              types={this.props.qTypeList}
              stages={this.state.stages}
              isLoading={this.state.isPosting}
              errorList={this.state.errorList}
              quote={this.state.quote}
            />
            {this.state.isCreateUserModal && (
              <CustomerUserNew
                isVisible={this.state.isCreateUserModal}
                close={this.closeUserModal}
                customerId={this.state.agt.customer}
              />
            )}
            <PDFViewer
              show={this.state.openPreview}
              onClose={this.toggleOpenPreview}
              titleElement={`View Agreement Preview`}
              previewHTML={this.state.previewHTML}
              footerElement={
                <SquareButton
                  content="Close"
                  bsStyle={"default"}
                  onClick={this.toggleOpenPreview}
                />
              }
              className=""
            />
            <ConfirmBox
              show={this.state.isopenConfirm}
              onClose={this.toggleConfirmOpen}
              onSubmit={this.onClickConfirm}
              isLoading={
                this.props.isFetching || this.props.isFetchingSingleQuote
              }
              title={"Are you sure, want to change Stage?"}
            />
          </div>
        </div>

        <div className="col-md-2 footer">
          <SquareButton
            content="Preview"
            onClick={(e) => this.previewDoc(e)}
            className="preview-agr-doc"
            bsStyle={"primary"}
          />
          {this.state.agt.id !== 0 && (
            <div className="doc-details col-md-12">
              <div>
                {" "}
                Created by <span>{this.state.agt.author_name}</span>.
              </div>
              <div>
                {" "}
                Last updated by <span>
                  {this.state.agt.updated_by_name}
                </span> on{" "}
                <span>
                  {moment
                    .utc(this.state.agt.updated_on)
                    .local()
                    .format("MM/DD/YYYY hh:mm A")}
                </span>
              </div>
              {this.state.agt.id && (
                <div>
                  {" "}
                  Version -{" "}
                  <span>
                    {this.state.agt.major_version || "1"}.
                    {this.state.agt.minor_version}
                  </span>
                </div>
              )}
            </div>
          )}
          <div className="agreement-checkboxes">
            <Checkbox
              isChecked={this.state.sendEmail}
              name="option"
              onChange={(e) => this.setState({ sendEmail: e.target.checked })}
              className="show-hidden-section-checkbox"
            >
              Send to Account Manager
            </Checkbox>
            <Checkbox
              isChecked={this.state.agt.update_major_version}
              name="update_major_version"
              onChange={(e) => this.onChecboxChangeAgreement(e)}
              className="show-hidden-section-checkbox"
            >
              Create major version
            </Checkbox>
            <Checkbox
              isChecked={this.state.agt.set_user_as_author}
              name="set_user_as_author"
              onChange={(e) => this.onChecboxChangeAgreement(e)}
              className="show-hidden-section-checkbox"
            >
              Make me agreement author
            </Checkbox>
          </div>

          <SquareButton
            content="Close"
            onClick={() => this.props.history.goBack()}
            className="save-mapping"
            bsStyle={"default"}
          />

          {Boolean(this.state.agt.id && this.state.agt.id !== 0) && (
            <SquareButton
              content={
                <span>
                  {this.state.loading && (
                    <img src={"/assets/icons/loading.gif"} alt="Saving" />
                  )}
                  {"Update Agreement"}
                </span>
              }
              onClick={() => this.callSave(false)}
              className="save-mapping"
              bsStyle={"primary"}
              title={"Update Agreement"}
            />
          )}

          <SquareButton
            content={
              <span>
                {this.state.loading && (
                  <img src={"/assets/icons/loading.gif"} alt="Saving" />
                )}
                {this.state.agt.id && this.state.agt.id !== 0
                  ? "Update & Close"
                  : "Save Agreement"}
              </span>
            }
            onClick={() => this.callSave(true)}
            className="save-mapping"
            bsStyle={"primary"}
            title={`${
              this.state.agt.id && this.state.agt.id !== 0 ? "Update" : "Save"
            } Agreement`}
          />
          {this.state.agtError && (
            <div id="agreement-save-error">
              Please fill all the required fields properly
            </div>
          )}
        </div>
        <PromptUnsaved
          when={this.state.unsaved}
          navigate={(path) => this.props.history.push(path)}
          shouldBlockNavigation={(location) => {
            if (this.state.unsaved) {
              return true;
            }
            return false;
          }}
          onSaveClick={(e) => this.callSave(true)}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  loggenInUser: state.profile.user,
  quote: state.sow.quote,
  isFetchingTemplates: state.agreement.isFetchingTemplates,
  isFetching: state.agreement.isFetching,
  quoteList: state.dashboard.quoteList,
  quoteFetching: state.dashboard.quoteFetching,
  customers: state.customer.customersShort,
  isFetchingUsers: state.documentation.isFetching,
  templates: state.agreement.templates,
  isFetchingAgreements: state.agreement.isFetchingAgreements,
  qTypeList: state.sow.qTypeList,
  isFetchingSingleQuote: state.sow.isFetchingSingleQuote,
});

const mapDispatchToProps = (dispatch: any) => ({
  agreementCRUD: (method: HTTPMethods, agt: IAgreement, params?: any) =>
    dispatch(agreementCRUD(method, agt, params)),
  getAgreementTemplate: (id: number) => dispatch(getAgreementTemplate(id)),
  getQuoteStages: () => dispatch(getQuoteAllStages()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  getTemplateList: (showDisable: boolean, params: IServerPaginationParams) =>
    dispatch(getAgreementTemplateList(showDisable, params)),
  createQuote: (id: number, q: any) => dispatch(createQuote(id, q)),
  fetchAllCustomerUsers: (id: number) => dispatch(fetchAllCustomerUsers(id)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addInfoMessage: (message: string) => dispatch(addInfoMessage(message)),
  addWarningMessage: (message: string) => dispatch(addWarningMessage(message)),
  getSingleQuote: (id: number) => dispatch(getSingleQuote(id)),
  updateQuoteStage: (id: number, stageId: number) =>
    dispatch(updateQuoteStage(id, stageId)),
  fetchQuoteDashboardListing: (id: number, openOnly?: boolean) =>
    dispatch(fetchQuoteDashboardListingPU(id, openOnly)),
  getAgreementPreview: (payload: IAgreement, id: number) =>
    dispatch(getAgreementPreview(payload, id)),
  fetchCatalogProductDetails: (id: number) =>
    dispatch(
      productCatalogCRUD("get", undefined, {
        id,
      } as IProductCatalogItem)
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateAgreement);
