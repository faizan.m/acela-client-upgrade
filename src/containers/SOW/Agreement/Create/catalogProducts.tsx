import React, { ReactNode, useCallback, useEffect, useState } from "react";
import { connect } from "react-redux";
import { cloneDeep, round } from "lodash";
import { CopyToClipboard } from "react-copy-to-clipboard";
import SquareButton from "../../../../components/Button/button";
import EditButton from "../../../../components/Button/editButton";
import IconButton from "../../../../components/Button/iconButton";
import InfiniteList from "../../../../components/InfiniteList/infiniteList";
import Input from "../../../../components/Input/input";
import ModalBase from "../../../../components/ModalBase/modalBase";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../../components/Spinner";
import Checkbox from "../../../../components/Checkbox/checkbox";
import { DraggableArea } from "react-draggable-tags";
import {
  PRODUCT_CATALOG_FAILURE,
  PRODUCT_CATALOG_SUCCESS,
  productCatalogCRUD,
} from "../../../../actions/sales";
import {
  addInfoMessage,
  addWarningMessage,
} from "../../../../actions/appState";
import { getContractLabel } from "../../../ProductCatalog/CreateProduct";

interface ICatalogProductProps {
  catalogName: string;
  products: IQuotedCatalogProduct[];
  productsError: IFieldValidation;
  addInfoMessage: TShowInfoMessage;
  addWarningMessage: TShowWarningMessage;
  fetchProductDetails: (id: number) => Promise<any>;
  handleChangeProducts: (products: IQuotedCatalogProduct[]) => void;
  addToQuotedProducts: (
    product?: IProductCatalogItem,
    comment?: boolean
  ) => void;
}

const CatalogProducts: React.FC<ICatalogProductProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [editMode, setEditMode] = useState<boolean>(false);
  const [isOrdering, setIsOrdering] = useState<boolean>(false);
  const [showCatalog, setShowCatalog] = useState<boolean>(false);
  const [editProduct, setEditProduct] = useState<IQuotedCatalogProduct>();
  const [editProductIdx, setEditProductIdx] = useState<number>(-1);
  const [curPricingOption, setCurPricingOption] = useState<string>("");

  useEffect(() => {
    if (
      props.productsError &&
      props.productsError.errorState === "error"
    )
      setIsOrdering(false);
  }, [props.productsError]);

  const formatCurrency = (
    value: number,
    noDecimal: boolean = false
  ): string => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: noDecimal ? 0 : 2,
    });
    return formatter.format(value);
  };

  const closeModal = () => {
    setEditMode(false);
    setEditProductIdx(-1);
    setCurPricingOption("");
    setEditProduct(undefined);
  };

  const closeCatalogModal = () => {
    setShowCatalog(false);
  };

  const roundData = (val: number) => {
    return round(val, editProduct.revenue_rounding ? 0 : 2);
  };

  const handleChangeMargin = (event: React.ChangeEvent<HTMLInputElement>) => {
    const margin: number =
      event.target.value === ""
        ? 0
        : Number(Number(event.target.value).toFixed(2));
    let customer_cost: number =
      margin >= 0 && margin < 100
        ? roundData(editProduct.internal_cost / (1 - margin / 100))
        : 0;
    let revenue: number = roundData(editProduct.quantity * customer_cost);
    let total_margin: number = roundData(
      editProduct.quantity * (customer_cost - editProduct.internal_cost)
    );
    setEditProduct((prevState) => ({
      ...prevState,
      margin,
      customer_cost,
      revenue,
      total_margin,
    }));
  };

  const handleChangeQuantity = (event: React.ChangeEvent<HTMLInputElement>) => {
    const quantity: number =
      event.target.value === "" ? 0 : parseInt(event.target.value);

    let revenue: number = roundData(quantity * editProduct.customer_cost);
    let total_margin: number = roundData(
      quantity * (editProduct.customer_cost - editProduct.internal_cost)
    );

    setEditProduct((prevState) => ({
      ...prevState,
      quantity,
      revenue,
      total_margin,
    }));
  };

  const removeProduct = (idx: number) => {
    props.handleChangeProducts(
      props.products.filter((el, index) => idx !== index)
    );
  };

  const validateProduct = (): boolean => {
    return Boolean(
      curPricingOption &&
        editProduct.margin < 100 &&
        editProduct.margin >= 0 &&
        editProduct.billing_term !== "Not Set"
    );
  };

  const saveProduct = () => {
    const products = props.products;
    props.handleChangeProducts([
      ...products.slice(0, editProductIdx),
      editProduct,
      ...products.slice(editProductIdx + 1),
    ]);
    closeModal();
  };

  const onRefreshProductData = () => {
    const productId: number = editProduct.id;
    if (!productId) return;
    setLoading(true);
    props
      .fetchProductDetails(productId)
      .then((action) => {
        if (action.type === PRODUCT_CATALOG_SUCCESS) {
          const productInfo: IProductCatalogItem = action.response;
          const updatedProduct: IQuotedCatalogProduct = {
            ...editProduct,
            ...productInfo,
            billing_term: "Not Set",
            contract_term: null,
            internal_cost: 0,
            customer_cost: 0,
            margin: 0,
            total_margin: 0,
            quantity: 0,
          };
          setEditProduct(updatedProduct);
          setCurPricingOption("");
          props.addInfoMessage("Refreshed with latest changes!");
        } else if (action.type === PRODUCT_CATALOG_FAILURE) {
          props.addWarningMessage(
            "No product found in the catalog, no changes were made to current product!"
          );
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChangeComment = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newProducts = cloneDeep(props.products);
    newProducts[index].comment = e.target.value;
    props.handleChangeProducts(newProducts);
  };

  const handleDraggableChange = (products: IQuotedCatalogProduct[]) => {
    props.handleChangeProducts(products);
  };

  const handleChangeOrderingCheckbox = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    setIsOrdering(e.target.checked);
  };

  const onClickEdit = (product: IQuotedCatalogProduct, idx: number) => {
    setEditProductIdx(idx);
    setEditProduct(product);
    if (product.billing_term && product.contract_term)
      setCurPricingOption(`${product.billing_term}_${product.contract_term}`);
    setEditMode(true);
  };

  const onPricingChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    const optionValue = e.target.value;
    const [billingTerm, contractTerm] = e.target.value.split("_");
    const productPricing: IProductPricing = cloneDeep(
      editProduct.pricing_data[billingTerm][contractTerm]
    );
    const multiplier = billingTerm === "Annual" ? 12 : 1;
    const margin = productPricing.margin_percent;
    const customer_cost = multiplier * productPricing.customer_cost;
    const internal_cost = multiplier * productPricing.internal_cost;
    const quantity = editProduct.quantity ? editProduct.quantity : 1;
    let revenue: number = roundData(quantity * customer_cost);
    let total_margin: number = roundData(
      quantity * (customer_cost - internal_cost)
    );
    setCurPricingOption(optionValue);
    setEditProduct((prevState) => ({
      ...prevState,
      revenue,
      margin,
      quantity,
      total_margin,
      customer_cost,
      internal_cost,
      billing_term: billingTerm as CatalogBillingTerm,
      contract_term: contractTerm as CatalogContractTerm,
    }));
  };

  const renderCatalogProduct = ({
    product,
    idx,
  }: {
    product: IQuotedCatalogProduct;
    idx: number;
  }) => {
    return (
      <div
        className={
          "row-panel" +
          `${product.error ? " pax-error-border" : ""}` +
          (isOrdering ? " ordering-mode" : "")
        }
        key={idx}
      >
        {product.is_comment ? (
          <div className="pax8-comment">
            <div className="pax8-comment-actions">
              <div className="pax8-icon-placeholder" />
              <SmallConfirmationBox
                onClickOk={() => removeProduct(idx)}
                text={"Comment"}
                showButton={true}
                title="Delete Comment"
              />
              <CopyToClipboard text={product.comment}>
                <IconButton
                  icon="copy.svg"
                  onClick={() => null}
                  newIcon={true}
                  title="Copy Comment"
                  className="product-copy"
                />
              </CopyToClipboard>
            </div>
            <Input
              field={{
                label: "",
                type: "TEXT",
                value: product.comment,
              }}
              width={12}
              name="comment"
              onChange={(e) => handleChangeComment(e, idx)}
              disabled={isOrdering}
              error={
                product.error && {
                  errorState: "error",
                  errorMessage: "Required",
                }
              }
              className={"pax8-comment-input"}
            />
          </div>
        ) : (
          <>
            <div className="cell width-10 pax8-btns">
              <EditButton
                title="Edit Product"
                onClick={() => onClickEdit(product, idx)}
              />
              {product.is_required &&
              props.products.filter(
                (el) => el.product_id === product.product_id
              ).length === 1 ? (
                <IconButton
                  icon="success.svg"
                  onClick={() => null}
                  newIcon={true}
                  title="Required Product"
                  className="required-icon"
                />
              ) : (
                <SmallConfirmationBox
                  className="remove"
                  showButton={true}
                  onClickOk={() => removeProduct(idx)}
                  text={"Product"}
                  title="Remove Product"
                />
              )}
              <CopyToClipboard
                text={product.product_id + "\n" + product.name}
                onCopy={() => {
                  props.addInfoMessage("Text copied!");
                }}
              >
                <IconButton
                  icon="copy.svg"
                  onClick={() => null}
                  newIcon={true}
                  title="Copy line item"
                  className="product-copy"
                />
              </CopyToClipboard>
            </div>
            <div className="cell pax8-desc-sku-col text-ellipsis">
              <span className="desc-line text-ellipsis" title={product.name}>
                {product.name}
              </span>
              <span className="sku-line" title={product.product_id}>
                {product.product_id}
              </span>
            </div>
            <div
              className="cell pax8-quantity-col align-center"
              title={String(product.quantity)}
            >
              {product.quantity}
            </div>
            <div
              className="cell width-10 align-center"
              title={formatCurrency(product.customer_cost)}
            >
              {formatCurrency(product.customer_cost)}
            </div>
            <div
              className="cell width-10 align-center"
              title={String(product.margin)}
            >
              {product.margin + "%"}
            </div>
            <div
              className="cell contract-col align-center"
              title={product.contract_term}
            >
              {getContractLabel(product.contract_term)}
            </div>
            <div
              className="cell width-10 align-center"
              title={product.billing_term}
            >
              {product.billing_term}
            </div>
          </>
        )}
      </div>
    );
  };

  const renderCatalogProductContainer = () => {
    return (
      <section className="pax8-products-container">
        <Spinner show={loading} className="pax8-agr-spinner" />
        <div className="pax8-quoted-products-section">
          <h3>
            <span className="heading-text">{props.catalogName}</span>
            <SquareButton
              onClick={() => setShowCatalog(true)}
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Add Product</span>
                </>
              }
              bsStyle={"link"}
              className={"pax8-add-product add-btn"}
            />
            <SquareButton
              onClick={() => props.addToQuotedProducts(undefined, true)}
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Comment</span>
                </>
              }
              bsStyle={"link"}
              className={"add-btn pax8-add-product"}
            />
          </h3>
          <Checkbox
            isChecked={isOrdering}
            name="orderingProducts"
            onChange={(e) => handleChangeOrderingCheckbox(e)}
            className="order-checkbox"
          >
            Reorder Products
          </Checkbox>
          <div className="quoted-products-list infinite-list-component">
            <div className="header">
              <div className="cell width-10" />
              <div className="cell align-center">Product</div>
              <div className="cell pax8-quantity-col align-center">
                Quantity
              </div>
              <div className="cell width-10 align-center">
                Customer Cost Per.
              </div>
              <div className="cell width-10 align-center">Margin %</div>
              <div className="cell contract-col align-center">
                Contract Length
              </div>
              <div className="cell width-10 align-center">Billing Option</div>
            </div>
            <div className="quoted-products-rows">
              {isOrdering ? (
                <DraggableArea
                  isList
                  tags={props.products}
                  render={({ tag, index }) =>
                    renderCatalogProduct({ product: tag, idx: index })
                  }
                  onChange={(tags) => handleDraggableChange(tags)}
                />
              ) : (
                props.products.map((product, idx) =>
                  renderCatalogProduct({ product, idx })
                )
              )}
            </div>
          </div>
          {props.products.length === 0 && (
            <div className="no-data">No Product Quoted!</div>
          )}
          {props.productsError.errorState === "error" && (
            <div id="pax8-products-error">
              {props.productsError.errorMessage}
            </div>
          )}
        </div>
      </section>
    );
  };

  const renderCatalog = () => {
    return (
      <ModalBase
        show={showCatalog}
        onClose={closeCatalogModal}
        titleElement={"Products Catalog"}
        bodyElement={
          <div className="pax8-catalog-section">
            <InfiniteList
              showSearch={true}
              showIncludeExclude={false}
              id="pax8-products-catalog"
              className="pax8-infinite-list"
              height="250px"
              url={`providers/sales/products`}
              columns={
                [
                  {
                    name: "",
                    className: "width-10 align-center",
                    Cell: (product: IProductCatalogItem) => (
                      <IconButton
                        onClick={() => {
                          props.addToQuotedProducts(product);
                          props.addInfoMessage(
                            `Quoted Product ${product.product_id}!`
                          );
                        }}
                        icon="plus.svg"
                        title="Add to Quoted Products"
                        className="add-to-quoted-list"
                        newIcon={true}
                      />
                    ),
                  },
                  {
                    id: "product_id",
                    ordering: "product_id",
                    name: "Product ID",
                    className: "pax8-sku-col",
                  },
                  {
                    id: "name",
                    ordering: "name",
                    name: "Name",
                    className: "pax8-name-col",
                  },
                  {
                    id: "description",
                    ordering: "description",
                    name: "Description",
                    className: "pax8-desc-col text-ellipsis",
                  },
                  {
                    id: "product_type",
                    ordering: "product_type",
                    name: "Product Type",
                    className: "pax8-vendor-col",
                  },
                ] as IColumnInfinite<IProductCatalogItem>[]
              }
            />
          </div>
        }
        footerElement={null}
        className={`pax8-product-edit-modal`}
      />
    );
  };

  const renderLabelWithDetails = useCallback(
    (width: number, label: string, detail: ReactNode) => {
      return (
        <div className={`field-section col-xs-${width}`}>
          <div className="field__label row">
            <div className="field__label-label">{label}</div>
          </div>
          <div className="detail">{detail}</div>
        </div>
      );
    },
    []
  );

  const renderEditProductModal = () => {
    return (
      <ModalBase
        show={editMode}
        onClose={closeModal}
        titleElement={"Configure " + editProduct.product_id}
        bodyElement={
          <div className="pax8-product-edit-container product-catalog-edit-container">
            <div className="product-config">
              <SquareButton
                onClick={onRefreshProductData}
                content={
                  <img
                    alt="refresh-products"
                    src={"/assets/icons/refresh.svg"}
                  />
                }
                className="refresh-product-btn"
                bsStyle={"primary"}
                title="Refresh latest data from Product Catalog"
              />
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(6, "Name", editProduct.name)}
              {renderLabelWithDetails(
                6,
                "Product Type",
                editProduct.product_type
              )}
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(
                12,
                "Description",
                editProduct.description
              )}
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(
                4,
                "Unit of Measure",
                editProduct.measurement_unit
              )}
              {renderLabelWithDetails(
                4,
                "Units Included",
                editProduct.no_of_units
              )}
              {renderLabelWithDetails(
                4,
                "Revenue Rounding",
                editProduct.revenue_rounding ? "Yes" : "No"
              )}
            </div>
            <div className="pax8-product-pricing">
              <h4>Pricing Information</h4>
              <div className="pax8-pricing-table catalog-agr-pricing">
                <div className="catalog-pricing-header">
                  <div>Contract Term</div>
                  <div>Internal Cost</div>
                  <div>Margin %</div>
                  <div>Margin $</div>
                  <div>Customer Cost</div>
                </div>
                {[
                  "Monthly",
                  "Annual",
                  "One Time",
                ]
                  .filter((el) =>
                    Object.keys(editProduct.pricing_data).includes(el)
                  )
                  .map((billingTerm: CatalogBillingTerm) => {
                    const multiplier =
                      billingTerm === "Annual" ? 12 : 1;
                    return (
                      <>
                        <h4>Billing Term: {billingTerm}</h4>
                        {[
                          Monthly,
                          "12 Months",
                          "24 Months",
                          "36 Months",
                          "One Time",
                        ]
                          .filter((el) =>
                            Object.keys(
                              editProduct.pricing_data[billingTerm]
                            ).includes(el)
                          )
                          .map(
                            (
                              contractTerm: CatalogContractTerm,
                              idx: number
                            ) => {
                              const radioOption = `${billingTerm}_${contractTerm}`;
                              const productOption =
                                editProduct.pricing_data[billingTerm][
                                  contractTerm
                                ];

                              return (
                                <div
                                  className="pax8-pricing-row catalog-pricing-row"
                                  key={idx}
                                >
                                  <div className="radio-btn-group">
                                    <div className="radio-btn">
                                      <input
                                        type="radio"
                                        name={`catalog_pricing`}
                                        onChange={onPricingChanged}
                                        value={radioOption}
                                        checked={
                                          curPricingOption === radioOption
                                        }
                                      />
                                    </div>
                                    <div className="contract-text">
                                      {getContractLabel(contractTerm)}
                                    </div>
                                  </div>
                                  <div>
                                    {formatCurrency(
                                      multiplier * productOption.internal_cost
                                    )}
                                  </div>
                                  <div>
                                    {productOption.margin_percent + "%"}
                                  </div>
                                  <div>
                                    {formatCurrency(
                                      multiplier * productOption.margin
                                    )}
                                  </div>
                                  <div>
                                    {formatCurrency(
                                      multiplier * productOption.customer_cost
                                    )}
                                  </div>
                                </div>
                              );
                            }
                          )}
                      </>
                    );
                  })}
              </div>
            </div>
            {curPricingOption && (
              <>
                <div className="pax8-modal-row">
                  {renderLabelWithDetails(
                    4,
                    "Billing Term",
                    editProduct.billing_term !== "Not Set"
                      ? editProduct.billing_term
                      : "N/A"
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Contract Term",
                    getContractLabel(editProduct.contract_term)
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Internal Cost",
                    formatCurrency(editProduct.internal_cost)
                  )}
                </div>
                <div className="pax8-modal-row" style={{ marginBottom: "0" }}>
                  <Input
                    field={{
                      label: "Quantity",
                      type: "NUMBER",
                      value: `${editProduct.quantity}`,
                      isRequired: true,
                    }}
                    width={4}
                    name="quantity"
                    minimumValue={"0"}
                    onlyInteger={true}
                    placeholder={"Enter Quantity"}
                    className="pax8-product-quantity"
                    onChange={handleChangeQuantity}
                  />
                  <Input
                    field={{
                      label: "Margin %",
                      type: "NUMBER",
                      value: `${editProduct.margin}`,
                      isRequired: true,
                    }}
                    width={4}
                    name="margin"
                    placeholder={"Enter Margin (in %)"}
                    className="pax8-product-margin"
                    onChange={handleChangeMargin}
                    minimumValue={"0"}
                    maximumValue={"100"}
                    error={
                      editProduct.margin < 0 || editProduct.margin >= 100
                        ? {
                            errorState: "error",
                            errorMessage: "Margin should be between 0 to 100",
                          }
                        : undefined
                    }
                  />
                  {renderLabelWithDetails(
                    4,
                    "Margin per",
                    formatCurrency(
                      editProduct.margin === 0
                        ? 0
                        : editProduct.customer_cost - editProduct.internal_cost
                    )
                  )}
                </div>
                <div className="pax8-modal-row">
                  {renderLabelWithDetails(
                    4,
                    "Customer Price",
                    formatCurrency(editProduct.customer_cost)
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Total Revenue",
                    formatCurrency(editProduct.revenue)
                  )}
                  {renderLabelWithDetails(
                    4,
                    "Total Margin",
                    formatCurrency(editProduct.total_margin)
                  )}
                </div>
              </>
            )}
          </div>
        }
        footerElement={
          <>
            <SquareButton
              content={"Save Product"}
              bsStyle={"primary"}
              onClick={saveProduct}
              disabled={!validateProduct()}
            />
            <SquareButton
              content={"Cancel"}
              bsStyle={"default"}
              onClick={closeModal}
            />
          </>
        }
        className={`pax8-product-edit-modal`}
      />
    );
  };

  return (
    <>
      {renderCatalogProductContainer()}
      {editMode && renderEditProductModal()}
      {showCatalog && renderCatalog()}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addInfoMessage: (msg: string) => dispatch(addInfoMessage(msg)),
  addWarningMessage: (msg: string) => dispatch(addWarningMessage(msg)),
  fetchProductDetails: (id: number) =>
    dispatch(
      productCatalogCRUD("get", undefined, {
        id,
      } as IProductCatalogItem)
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(CatalogProducts);
