import React from 'react';
import Spinner from '../../../components/Spinner';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import RightMenu from '../../../components/RighMenuBase/rightMenuBase';
import { previewSOW, CREATE_SOW_SUCCESS, getCreatedSOWHistory, getDeletedSOWHistory } from '../../../actions/sow';
import SquareButton from '../../../components/Button/button';
import _, { cloneDeep } from 'lodash';
import './style.scss';
import PDFViewer from '../../../components/PDFViewer/PDFViewer';
import Table from '../../../components/Table/table';
import { utcToLocalInLongFormat } from '../../../utils/CalendarUtil';

interface IViewSOWCDHistoryProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  sowCreateHistory: any;
  isFetchingCreateHistory: boolean;
  sowDeleteHistory: any;
  getCreatedSOWHistory: any;
  getDeletedSOWHistory: any
  previewSOW: any;
  isFetchingSow: boolean;
}

interface IViewSOWCDHistoryState {
  open: boolean;
  sowCreateHistory: any[];
  sowDeleteHistory: any[];
  isCollapsedCreate: boolean;
  isCollapsedDelete: boolean;
  sow: any;
  openPreview: boolean;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  paginationDelete: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  createHistoryRows: any[];
  deleteHistoryRows: any[];
}

class ViewSOWCDHistory extends React.Component<
  IViewSOWCDHistoryProps,
  IViewSOWCDHistoryState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IViewSOWCDHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    sowCreateHistory: [],
    sowDeleteHistory: [],
    isCollapsedCreate: false,
    isCollapsedDelete: true,
    openPreview: false,
    sow: null,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    paginationDelete: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    id: '',
    createHistoryRows: [],
    deleteHistoryRows: [],
  })

  componentDidUpdate(prevProps: IViewSOWCDHistoryProps) {
    if (
      this.props.sowCreateHistory &&
      prevProps.sowCreateHistory !== this.props.sowCreateHistory
    ) {
      this.setRows(this.props);
    }
    if (
      this.props.sowDeleteHistory &&
      prevProps.sowDeleteHistory !== this.props.sowDeleteHistory
    ) {
      this.setRows(this.props);
    }
  }

  setRows = (nextProps: IViewSOWCDHistoryProps) => {
    const sowCreateHistory = nextProps.sowCreateHistory;
    const history: any[] = sowCreateHistory.results;
    const createHistoryRows: any[] = history && history.map(
      (row, index) => ({
        id: row.id,
        name: `${_.get(row, 'snapshot_data.name')}`,
        date_created: `${_.get(row, 'revision.date_created')}`,
        index,
        snapshot_data: _.get(row, 'snapshot_data'),
      })
    );

    this.setState(prevState => ({
      createHistoryRows,
      sowCreateHistory,
      pagination: {
        ...prevState.pagination,
        totalRows: sowCreateHistory.count,
        currentPage: sowCreateHistory.links.page_number - 1,
        totalPages: Math.ceil(
          sowCreateHistory.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  getTitle = () => {
    return (
      <div className="text-center">
        SOW Created History
      </div>
    );
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getCreatedSOWHistory(newParams);
  };
  // Server side searching, sorting, ordering
  fetchDataDelete = (params: IServerPaginationParams) => {
    const prevParams = this.state.paginationDelete.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      paginationDelete: {
        ...prevState.paginationDelete,
        params: newParams,
      },
    }));

    this.props.getDeletedSOWHistory(newParams);
  };

  toggleCollapsedState = (data: any) => {
    const newState = cloneDeep(this.state);
    (newState[data] as any) = !this.state[data];
    this.setState(newState);
  }


  renderHistoryContainer = () => {

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };
    const columns: ITableColumn[] = [
      {
        accessor: 'name',
        Header: 'Name',
        sortable: false,
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: 'date_created', Header: 'Created On',
        sortable: false,
        Cell: name => <div className="pl-15">{utcToLocalInLongFormat(name.value)}</div>,
      },
      {
        accessor: 'status',
        Header: 'Preview Doc',
        sortable: false,
        Cell: data => (
          <SquareButton
            onClick={e => this.previewDoc(data.original.snapshot_data)}
            content="Preview"
            title="View this SOW version "
            bsStyle={"primary"}
            className={'history-buttons'}
          />),
      },

    ];
    return (
      <div
        key={0}
        className={`collapsable-section  ${
          this.state.isCollapsedCreate ? 'collapsable-section--collapsed' : 'collapsable-section--not-collapsed'
          }`}
      >
        <div className="col-md-12 collapsable-heading" onClick={e => this.toggleCollapsedState('isCollapsedCreate')}>
          <div className="left col-md-9">Created SOW's</div>
        </div>
        <div className="collapsable-contents">
          <Table
            manualProps={manualProps}
            columns={columns}
            rows={this.state.createHistoryRows}
            className={`provider-users-listing__table ${
              this.props.isFetchingCreateHistory ? `loading` : ``
              }`}
            loading={this.props.isFetchingCreateHistory}
          />
        </div>
      </div>
    );
  };


  getBody = () => {

    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingCreateHistory || this.props.isFetchingSow} />
        </div>
        <div className="history-section heading  col-md-12">
          {this.renderHistoryContainer()}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return <div className={`footer`}></div>;
  };


  previewDoc = (obj: any) => {
    const sow = obj;
    sow.major_version= 1;
    sow.minor_version= 0;
    delete sow.author_name;
    delete sow.updated_by_name;
    delete sow.update_version;
    delete sow.user_name;
    this.props.previewSOW(sow).then(a => {
      if (a.type === CREATE_SOW_SUCCESS) {
        this.setState({ openPreview: true, sow: a.response });
      }
    });
  };
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      sow: null,
    });
  };
  render() {
    return (
      <>
        <RightMenu
          show={this.props.show}
          onClose={this.props.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="version-history"
        />
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.sow}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </>
    );
  }
}


const mapStateToProps = (state: IReduxStore) => ({
  sowCreateHistory: state.sow.sowCreateHistory,
  isFetchingCreateHistory: state.sow.isFetchingCreateHistory,
  sowDeleteHistory: state.sow.sowDeleteHistory,
  isFetchingSow: state.sow.isFetchingSow
});

const mapDispatchToProps = (dispatch: any) => ({
  getCreatedSOWHistory: (params?: IServerPaginationParams) => dispatch(getCreatedSOWHistory(params)),
  getDeletedSOWHistory: (params?: IServerPaginationParams) => dispatch(getDeletedSOWHistory(params)),
  previewSOW: (sow: any) => dispatch(previewSOW(sow)),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ViewSOWCDHistory)
);
