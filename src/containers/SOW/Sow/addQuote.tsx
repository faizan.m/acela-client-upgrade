import { cloneDeep } from "lodash";
import React from "react";
import SquareButton from "../../../components/Button/button";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import { commonFunctions } from "../../../utils/commonFunctions";

interface IAddQuoteProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newDevice: any) => void;
  stages: any[];
  types: any[];
  isLoading: boolean;
  errorList: any;
  quote: IQuote;
}

interface IAddQuoteState {
  quote: {
    name: string;
    stage_id: number;
    type_id: number;
  };
  error: {
    stage_id: IFieldValidation;
    type_id: IFieldValidation;
    name: IFieldValidation;
    customer_id: IFieldValidation;
  };
}

export default class AddQuote extends React.Component<
  IAddQuoteProps,
  IAddQuoteState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IAddQuoteProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    quote: {
      stage_id: null,
      type_id: null,
      name: null,
    },
    error: {
      stage_id: { ...AddQuote.emptyErrorState },
      type_id: { ...AddQuote.emptyErrorState },
      name: { ...AddQuote.emptyErrorState },
      customer_id: { ...AddQuote.emptyErrorState },
    },
  });

  // Check if getDerivedStateFromProps can be used (in future)
  componentDidUpdate(prevProps: IAddQuoteProps) {
    if (this.props.errorList !== prevProps.errorList) {
      this.setValidationErrors(this.props.errorList);
    }
    if (this.props.quote && this.props.quote !== prevProps.quote) {
      this.setState({ quote: this.props.quote });
    }
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prevState) => ({
      quote: {
        ...prevState.quote,
        [event.target.name]: event.target.value,
      },
    }));
  };

  onClose = (e) => {
    this.props.onClose(e);
    const newState = this.getEmptyState();
    this.setState(newState);
  };

  setValidationErrors = (errorList) => {
    const newState: IAddQuoteState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.quote.name || this.state.quote.name.trim().length === 0) {
      error.name.errorState = "error";
      error.name.errorMessage = "Enter a valid opportunity name";

      isValid = false;
    } else if (this.state.quote.name.length > 300) {
      error.name.errorState = "error";
      error.name.errorMessage =
        "Opportunity name should be less than 300 chars";

      isValid = false;
    }

    if (this.state.quote && !this.state.quote.stage_id) {
      error.stage_id.errorState = "error";
      error.stage_id.errorMessage = "Please select stage";

      isValid = false;
    }

    if (this.state.quote && !this.state.quote.type_id) {
      error.type_id.errorState = "error";
      error.type_id.errorMessage = "Please select type";

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = (e) => {
    if (this.isValid()) {
      this.props.onSubmit(this.state.quote);
      const newState = this.getEmptyState();
      this.setState(newState);
    }
  };

  getTitle = () => {
    return "Add New Opportunity";
  };

  getBody = () => {
    const types = this.props.types
      ? this.props.types.map((site) => ({
          value: site.id,
          label: site.label,
        }))
      : [];

    return (
      <div className="add-quote">
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`add-quote__body ${this.props.isLoading ? `loading` : ""}`}
        >
          <Input
            field={{
              label: "Name",
              type: "TEXT",
              value: this.state.quote.name,
              isRequired: true,
            }}
            width={12}
            placeholder="Enter name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />

          <div className="field-section  col-md-12 col-xs-12">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.type_id.errorMessage ? `error-input` : ""
              }`}
            >
              <SelectInput
                name="type_id"
                value={this.state.quote.type_id}
                onChange={this.handleChange}
                options={types}
                searchable={true}
                placeholder="Select type"
                clearable={false}
              />
            </div>
            <div className="field-error">
              {this.state.error.type_id.errorMessage
                ? this.state.error.type_id.errorMessage
                : ""}
            </div>
          </div>
          <div className="field-section  col-md-12 col-xs-12">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Stage
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.stage_id.errorMessage ? `error-input` : ""
              }`}
            >
              <SelectInput
                name="stage_id"
                value={this.state.quote.stage_id}
                onChange={this.handleChange}
                options={this.props.stages}
                searchable={true}
                placeholder="Select stage"
                clearable={false}
              />
            </div>
            <div className="field-error">
              {this.state.error.stage_id.errorMessage
                ? this.state.error.stage_id.errorMessage
                : ""}
            </div>
          </div>

          {this.state.error.customer_id.errorMessage && (
            <div className="field-error">
              {this.state.error.customer_id.errorMessage}
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-quote__footer
      ${this.props.isLoading ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Add"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-quote"
      />
    );
  }
}
