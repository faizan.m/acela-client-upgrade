import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import {
  map,
  pick,
  cloneDeep,
  debounce,
  DebouncedFunc,
  isNil,
  round,
  random,
  isEqual,
} from "lodash";
import AppValidators from "../../../utils/validator";
import { DraggableArea } from "react-draggable-tags";
import { fetchQuoteDashboardListingPU } from "../../../actions/dashboard";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../../actions/documentation";
import {
  fetchSOWDOCSetting,
  getQuoteAllStages,
  GET_QUOTE_STAGES_SUCCESS,
} from "../../../actions/setting";
import {
  getSOW,
  saveSOW,
  updateSOW,
  previewSOW,
  createQuote,
  getTemplate,
  getSingleQuote,
  getCategoryList,
  getTemplateList,
  getDocumentType,
  getQuoteTypeList,
  updateQuoteStage,
  EDIT_SOW_FAILURE,
  EDIT_SOW_SUCCESS,
  GET_QUOTES_SUCCESS,
  CREATE_SOW_FAILURE,
  CREATE_SOW_SUCCESS,
  CREATE_QUOTES_SUCCESS,
  FETCH_TEMPLATE_SUCCESS,
  getVendorsList,
  getVendorMappingList,
  previewServiceDetail,
  PREVIEW_SERVICE_DETAIL_SUCCESS,
} from "../../../actions/sow";
import {
  customerLogoCRUD,
  CUSTOMER_LOGO_SUCCESS,
} from "../../../actions/customer";
import {
  addWarningMessage,
  addErrorMessage,
  addInfoMessage,
} from "../../../actions/appState";
import {
  getCalculatedHourlyResource,
  getCustomerCost,
  getRecommendedHours,
  getSowCalculationFields,
  roudingOffNext,
} from "../../../utils/sowCalculations";

import { commonFunctions } from "../../../utils/commonFunctions";
import AddQuote from "./addQuote";
import CustomerUserNew from "./addUser";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import Spinner from "../../../components/Spinner";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import PromptUnsaved from "../../../components/UnsavedWarning/PromptUnsaved";
import { QuillEditorAcela } from "../../../components/QuillEditor/QuillEditor";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import EditButton from "../../../components/Button/editButton";
import VendorMappingModal from "./sowVendorMapping";
import "../../../commonStyles/serviceCostCalculations.scss";
import AddVendor from "./addVendor";
import IconButton from "../../../components/Button/iconButton";
import LogoUpload from "../../Customer/LogoUpload";
import "./style.scss";

interface IAddSowProps extends ICommonProps {
  sow: ISoW;
  quote: IQuote;
  templates: any[];
  quoteList: IQuote[];
  user: ISuperUser;
  documentTypes: string[];
  qTypeList: IDLabelObject[];
  docSetting: IDOCSetting;
  customers: ICustomerShort[];
  categoryList: ICategoryList[];
  vendorOptions: IPickListOptions[];
  vendorMapping: IVendorAliasMapping[];
  isFetching: boolean;
  quoteFetching: boolean;
  isFetchingSow: boolean;
  isFetchingUsers: boolean;
  isFetchingVendors: boolean;
  isFetchingCategory: boolean;
  isFetchingTemplate: boolean;
  isFetchingQTypeList: boolean;
  isFetchingQStageList: boolean;
  isFetchingSingleQuote: boolean;
  getDocumentType: () => Promise<any>;
  getTemplateList: () => Promise<any>;
  getCategoryList: () => Promise<any>;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addWarningMessage: TShowWarningMessage;
  checkCustomerLogo: (customerId: number) => Promise<any>;
  fetchQuoteDashboardListing: (id: number, openOnly?: boolean) => Promise<any>;
  getSOW: (id: number) => Promise<any>;
  saveSOW: (
    sow: ISoW,
    sendEmail: boolean,
    sendEmailViaGraph: boolean
  ) => Promise<any>;
  updateSOW: (
    sow: ISoW,
    sendEmail: boolean,
    sendEmailViaGraph: boolean
  ) => Promise<any>;
  fetchAllCustomerUsers: (id: number) => Promise<any>;
  getQuoteTypeList: () => Promise<any>;
  createQuote: (id: number, quote: IQuote) => Promise<any>;
  fetchSOWDOCSetting: () => Promise<any>;
  previewSOW: (sow: ISoW) => Promise<any>;
  getSingleQuote: (id: number) => Promise<any>;
  getQuoteStages: () => Promise<any>;
  getVendorsList: () => Promise<any>;
  getVendorMapping: () => Promise<any>;
  getTemplate: (id: number) => Promise<any>;
  updateQuoteStage: (id: number, stageId: number) => Promise<any>;
  previewServiceDetail: (payload: ISoWServiceDetailPayload) => Promise<any>;
}

interface OverrideInfo {
  name: string;
  checked: boolean;
  idx: number;
}

interface IAddSowState {
  sow: ISoW;
  id: number;
  orderingPhase: boolean;
  showLogoModal: boolean;
  customerHasLogo: boolean;
  orderingMilestones: boolean;
  orderingHourlyResources: boolean;
  // resourceOrderingOptions: IPickListOptions[];
  users: ICustomerUser[];
  quote: IQuote;
  templates: any[];
  isValid: boolean;
  showError: boolean;
  isPosting: boolean;
  errorList?: object;
  showHidden: boolean;
  isCollapsed: { [fieldName: string]: boolean };
  isopenConfirm: boolean;
  validationError: string;
  isAddSowPosting: boolean;
  showVendorModal: boolean;
  showOverrideConfirmation: boolean;
  ehtOverrideInfo: OverrideInfo;
  errorsLabel: {
    [fieldName: string]: IFieldValidation;
  };
  errorsValue: {
    [fieldName: string]: IFieldValidation;
  };
  isCreateUserModal: boolean;
  isCreateOpportunityModal: boolean;
  error: {
    name: IFieldValidation;
    template: IFieldValidation;
    doc_type: IFieldValidation;
    user: IFieldValidation;
    customer: IFieldValidation;
    quote_id: IFieldValidation;
    category: IFieldValidation;
    json_config: IFieldValidation;
    engineering_hourly_rate: IFieldValidation;
    after_hours_rate: IFieldValidation;
    integration_technician_hourly_rate: IFieldValidation;
    project_management_hourly_rate: IFieldValidation;
    stage: IFieldValidation;
    default_ps_risk: IFieldValidation;
    milestone_percent: IFieldValidation;
  };
  loading: boolean;
  saving: boolean;
  sendEmail: boolean;
  sendEmailViaGraph: boolean;
  openPreview: boolean;
  openServicePreview: boolean;
  previewHTML: IFileResponse;
  numPages: number;
  pageNumber: number;
  versionObj: boolean;
  total_margin: number;
  sowCalculations: ISoWCalculationFields;
  stages: IPickListOptions[];
  phaseResources: IPickListOptions[];
  stageLoading: boolean;
  phasesGroupedByResources: object;
  vendorDescriptionMapping: Map<number, IVendorAliasMapping>;
  showVendorMappingModal: boolean;
  currentVendorMapping: IVendorAliasMapping;
  unsaved: boolean;
  riskVarOverride: boolean;
  riskVarRemove: boolean;
}

const getResourceIdentifier = (resource: IHourlyResource): string =>
  resource.resource_description
    ? resource.resource_description
    : resource.resource_name;

const OverrideMap = {
  project_management_hours_override: {
    hoursVal: "project_management_hours",
    resourceVal: "Project Management",
  },
  engineering_hours_override: {
    hoursVal: "engineering_hours",
    resourceVal: "Engineer",
  },
  after_hours_override: {
    hoursVal: "after_hours",
    resourceVal: "After Hours Engineer",
  },
  integration_technician_hours_override: {
    hoursVal: "integration_technician_hours",
    resourceVal: "Integration Technician",
  },
};

class AddSow extends React.Component<IAddSowProps, IAddSowState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  static EmptyHourlyResource: IHourlyResource = {
    hours: 0,
    override: false,
    is_hidden: false,
    hourly_rate: 0,
    hourly_cost: 0,
    resource_id: null,
    resource_name: "",
    internal_cost: 0,
    customer_cost: 0,
    margin: 0,
    margin_percentage: 0,
    resource_description: null,
  };
  static EmptyContractor: IContractor = {
    name: "",
    partner_cost: 0,
    customer_cost: 0,
    margin_percentage: 0,
    type: "Service",
    vendor_id: null,
    description: "",
  };
  private debouncedCalculations: DebouncedFunc<() => void>;
  private debouncedTermUpdate: DebouncedFunc<(jsonConfig: IJSONConfig) => void>;

  constructor(props: IAddSowProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedCalculations = debounce(this.doSowCalculations, 1000);
    this.debouncedTermUpdate = debounce((jsonConfig: IJSONConfig) => {
      const newJsonConfig = this.assignGeneratedTerms(jsonConfig);
      this.setState((prev) => ({
        sow: { ...prev.sow, json_config: newJsonConfig },
      }));
    }, 1000);
  }
  getEmptyState = () => ({
    sow: {
      json_config: null,
      name: "",
      doc_type: "" as SowDocType,
      customer: null,
      quote_id: null,
      template: null,
      user: null,
      user_name: null,
      category_name: null,
      id: 0,
      category: null,
      update_version: false,
      update_author: false,
      major_version: 0,
      minor_version: 0,
      include_cover_page: true,
      include_customer_logo: false,
      pm_type_t_and_m: false,
    },
    orderingPhase: false,
    orderingMilestones: false,
    showOverrideConfirmation: false,
    ehtOverrideInfo: {
      name: "",
      checked: false,
      idx: null,
    },
    riskVarRemove: false,
    orderingHourlyResources: false,
    riskVarOverride: false,
    showHidden: false,
    showVendorModal: false,
    quote: null,
    isAddSowPosting: false,
    isopenConfirm: false,
    phaseResources: [
      { value: "Engineer", label: <div>Engineer</div>, data: -1 }, // Data value is added to indentify a resource
      {
        value: "After Hours Engineer",
        label: <div>After Hours Engineer</div>,
        data: -2,
      },
      {
        value: "Project Management",
        label: <div>Project Management</div>,
        data: -3,
      },
      {
        value: "Integration Technician",
        label: <div>Integration Technician</div>,
        data: -4,
      },
    ],
    vendorDescriptionMapping: new Map<number, IVendorAliasMapping>(),
    showVendorMappingModal: false,
    currentVendorMapping: {
      vendor_name: "",
      vendor_crm_id: null,
      resource_type: null,
      resource_description: "",
    },
    saving: false,
    id: null,
    isCollapsed: {},
    errorsLabel: {},
    errorsValue: {},
    isValid: false,
    showError: false,
    validationError: "Please enter all required fields.",
    isPosting: false,
    templates: [],
    users: [],
    isCreateUserModal: false,
    isCreateOpportunityModal: false,
    phasesGroupedByResources: {},
    unsaved: false,
    error: {
      name: commonFunctions.getErrorState(),
      template: commonFunctions.getErrorState(),
      doc_type: commonFunctions.getErrorState(),
      user: commonFunctions.getErrorState(),
      customer: commonFunctions.getErrorState(),
      quote_id: commonFunctions.getErrorState(),
      category: commonFunctions.getErrorState(),
      json_config: commonFunctions.getErrorState(),
      engineering_hourly_rate: commonFunctions.getErrorState(),
      after_hours_rate: commonFunctions.getErrorState(),
      integration_technician_hourly_rate: commonFunctions.getErrorState(),
      project_management_hourly_rate: commonFunctions.getErrorState(),
      stage: commonFunctions.getErrorState(),
      default_ps_risk: commonFunctions.getErrorState(),
      milestone_percent: commonFunctions.getErrorState(),
    },
    loading: false,
    sendEmail: false,
    showLogoModal: false,
    customerHasLogo: false,
    sendEmailViaGraph: false,
    openPreview: false,
    openServicePreview: false,
    previewHTML: null,
    numPages: 0,
    pageNumber: 1,
    versionObj: false,
    total_margin: null,
    stageLoading: false,
    stages: [],
    sowCalculations: {
      engineeringHoursInternalCost: 0,
      engineeringHoursCustomerCost: 0,
      engineeringHoursMargin: 0,
      engineeringHoursMarginPercent: 0,
      afterHoursInternalCost: 0,
      afterHoursCustomerCost: 0,
      afterHoursMargin: 0,
      afterHoursMarginPercent: 0,
      integrationTechnicianInternalCost: 0,
      integrationTechnicianCustomerCost: 0,
      integrationTechnicianMargin: 0,
      integrationTechnicianMarginPercent: 0,
      pmHoursInternalCost: 0,
      pmHoursCustomerCost: 0,
      pmHoursMargin: 0,
      pmHoursMarginPercent: 0,
      totalCustomerCost: 0,
      totalInternalCost: 0,
      totalMargin: 0,
      totalMarginPercent: 0,
      hourlyLaborCustomerCost: 0,
      hourlyLaborInternalCost: 0,
      hourlyLaborMargin: 0,
      hourlyLaborMarginPercent: 0,
      riskBudgetCustomerCost: 0,
      riskBudgetInternalCost: 0,
      riskBudgetMargin: 0,
      riskBudgetMarginPercent: 0,
      contractorCustomerCost: 0,
      contractorInternalCost: 0,
      contractorMargin: 0,
      contractorMarginPercent: 0,
      proSerCustomerCost: 0,
      proSerInternalCost: 0,
      proSerMargin: 0,
      proSerMarginPercent: 0,
      travelCustomerCost: 0,
      travelInternalCost: 0,
      travelMargin: 0,
      travelMarginPercent: 0,
    },
  });

  componentDidMount() {
    this.setState({ stageLoading: true });
    this.props
      .getQuoteStages()
      .then((action) => {
        if (action.type === GET_QUOTE_STAGES_SUCCESS) {
          this.setState({
            stages: action.response.map((stage: IDLabelObject) => ({
              value: stage.id,
              label: stage.label,
            })),
          });
        }
      })
      .finally(() => this.setState({ stageLoading: false }));

    this.props.getVendorsList();
    this.props.getQuoteTypeList();
    this.props.getDocumentType();
    this.props.getCategoryList();
    this.props.getTemplateList();
    this.props.fetchSOWDOCSetting();
    this.props.getVendorMapping();
    const id: string = this.props.match.params.id;
    const query = new URLSearchParams(this.props.location.search);
    const customerId: number = Number(query.get("customer"));
    const versionObj = query.get("versionobj");

    if (versionObj) {
      const obj: string = sessionStorage.getItem("versionobj") as any;
      let sowObj: ISoW = JSON.parse(obj);
      const json_config = sowObj.json_config;
      this.setPhaseIds(json_config);
      this.setHourlyResourceIds(json_config);
      this.setVendorDescription(json_config);
      const riskVarOverride: boolean = this.props.docSetting
        ? this.props.docSetting.default_ps_risk !==
          json_config.service_cost.default_ps_risk
        : false;
      const riskVarRemove =
        json_config.service_cost.default_ps_risk === 0 &&
        this.props.sow.doc_type === "T & M";
      json_config.service_cost.default_ps_risk *= 100;
      const sow = {
        json_config,
        name: sowObj.name,
        customer: sowObj.customer_id,
        doc_type: sowObj.doc_type,
        change_request: sowObj.change_request
          ? sowObj.change_request[0]
          : undefined,
        quote_id: sowObj.quote_id,
        user: sowObj.user,
        id: sowObj.id,
        category: sowObj.category,
        author: sowObj.author,
        author_name: sowObj.author_name ? sowObj.author_name : "N.A.",
        updated_on: sowObj.updated_on ? sowObj.updated_on : "N.A.",
        updated_by_name: sowObj.updated_by_name,
        update_version: false,
        update_author: false,
        major_version: sowObj.major_version,
        minor_version: sowObj.minor_version,
        include_cover_page: sowObj.include_cover_page,
        include_customer_logo: this.props.sow.include_customer_logo,
        pm_type_t_and_m: Boolean(sowObj.pm_type_t_and_m),
      };
      this.setState(
        {
          sow,
          riskVarRemove,
          riskVarOverride,
          phaseResources: this.getPhaseResources(json_config.service_cost),
          isCollapsed: Object.keys(json_config).reduce(
            (prev, cur) => ({ ...prev, [cur]: false }),
            {}
          ),
        },
        () => {
          this.doSowCalculations();
          this.setPhasesGroupedByResources();
        }
      );
      if (sow.id !== 0 && sow.quote_id) {
        this.props.getSingleQuote(sow.quote_id);
      }
      this.props.fetchQuoteDashboardListing(sowObj.customer_id, true);
      this.props.fetchAllCustomerUsers(sowObj.customer_id).then((a) => {
        if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
          this.setState({ users: a.response });
        }
      });
      this.setState({
        versionObj: true,
      });
    } else if (id !== "0") {
      this.props.getSOW(Number(id));
    } else if (id === "0" && customerId) {
      this.setState((prevState) => ({
        sow: {
          ...prevState.sow,
          user: null,
          quote_id: null,
          customer: Number(customerId),
        },
      }));
      this.props.fetchQuoteDashboardListing(Number(customerId), true);
      this.props.fetchAllCustomerUsers(customerId).then((a) => {
        if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
          this.setState({ users: a.response });
        }
      });
    }
  }

  static getDerivedStateFromProps(
    nextProps: IAddSowProps,
    prevState: IAddSowState
  ) {
    if (nextProps.quote && !isEqual(nextProps.quote, prevState.quote)) {
      return { quote: nextProps.quote };
    }
    return null;
  }

  componentDidUpdate(prevProps: IAddSowProps) {
    if (this.props.sow && this.props.sow !== prevProps.sow) {
      let unsaved = false;
      const json_config = this.props.sow.json_config;
      if (
        this.props.sow.doc_type === "Fixed Fee" &&
        (!json_config.service_cost.milestones ||
          json_config.service_cost.milestones.length === 0)
      ) {
        json_config.service_cost.milestones = [];
        this.props.addWarningMessage(
          "Milestones are not configured for this SoW!"
        );
      }
      this.setPhaseIds(json_config);
      this.setHourlyResourceIds(json_config);
      this.setVendorDescription(json_config);
      const riskVarOverride: boolean = this.props.docSetting
        ? this.props.docSetting.default_ps_risk !==
          json_config.service_cost.default_ps_risk
        : false;
      const riskVarRemove =
        json_config.service_cost.default_ps_risk === 0 &&
        this.props.sow.doc_type === "T & M";
      json_config.service_cost.default_ps_risk *= 100;
      const sow = {
        json_config,
        name: this.props.sow.name,
        customer: this.props.sow.customer.id,
        doc_type: this.props.sow.doc_type,
        change_request: this.props.sow.change_request
          ? this.props.sow.change_request[0]
          : undefined,
        quote_id: this.props.sow.quote_id,
        user: this.props.sow.user,
        id: this.props.sow.id,
        category: this.props.sow.category,
        author: this.props.sow.author,
        author_name: this.props.sow.author_name
          ? this.props.sow.author_name
          : "N.A.",
        updated_on: this.props.sow.updated_on
          ? this.props.sow.updated_on
          : "N.A.",
        updated_by_name: this.props.sow.updated_by_name,
        update_version: false,
        update_author: false,
        major_version: this.props.sow.major_version,
        minor_version: this.props.sow.minor_version,
        include_cover_page: this.props.sow.include_cover_page,
        include_customer_logo: this.props.sow.include_customer_logo,
        pm_type_t_and_m: Boolean(this.props.sow.pm_type_t_and_m),
      };

      const query = new URLSearchParams(this.props.location.search);
      const cloned = query.get("cloned");
      if (cloned === "true") {
        sow.id = 0;
        sow.name = this.props.sow.name + " (cloned)";
        sow.quote_id = null;
        sow.major_version = 1;
        sow.minor_version = 0;
        unsaved = true;
      }

      this.setState(
        {
          sow,
          riskVarRemove,
          riskVarOverride,
          phaseResources: this.getPhaseResources(json_config.service_cost),
          isCollapsed: Object.keys(this.props.sow.json_config).reduce(
            (prev, cur) => ({ ...prev, [cur]: false }),
            {}
          ),
          unsaved,
        },
        () => {
          this.doSowCalculations();
          this.setPhasesGroupedByResources();
        }
      );
      if (sow.id !== 0 && sow.quote_id) {
        this.props.getSingleQuote(sow.quote_id);
      }
      this.checkForCustomerLogo(this.props.sow.customer.id);
      this.props.fetchQuoteDashboardListing(this.props.sow.customer.id, true);
      this.props.fetchAllCustomerUsers(this.props.sow.customer.id).then((a) => {
        if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
          this.setState({ users: a.response });
        }
      });
    }
    if (this.props.templates && this.props.templates !== prevProps.templates) {
      this.setState({ templates: this.props.templates });
      this.setValuesFromTemplate();
    }
    if (
      this.props.vendorMapping &&
      this.props.vendorMapping !== prevProps.vendorMapping
    ) {
      const vendorMapping = new Map<number, IVendorAliasMapping>();
      this.props.vendorMapping.forEach((el) => {
        vendorMapping.set(el.vendor_crm_id, el);
      });
      this.setState({ vendorDescriptionMapping: vendorMapping }, () => {
        if (this.state.sow.json_config) {
          const sow = cloneDeep(this.state.sow);
          this.setVendorDescription(sow.json_config);
          this.setState({ sow });
        }
      });
    }
  }

  setValuesFromTemplate = () => {
    const query = new URLSearchParams(this.props.location.search);
    const templateId = parseInt(query.get("template"), 10);
    if (templateId && this.props.templates && this.props.templates.length > 0) {
      this.props.getTemplate(templateId).then((action) => {
        if (action.type === FETCH_TEMPLATE_SUCCESS) {
          const template: ISoWTemplate = action.response;
          const riskVarOverride: boolean = this.props.docSetting
            ? this.props.docSetting.default_ps_risk !==
              template.json_config.service_cost.default_ps_risk
            : false;
          const riskVarRemove =
            template.json_config.service_cost.default_ps_risk === 0;
          template.json_config.service_cost.default_ps_risk *= 100;
          const templates = this.props.templates.filter(
            (temp) => template.category === temp.category
          );
          this.setState(
            {
              templates,
              riskVarRemove,
              riskVarOverride,
              isCollapsed: Object.keys(template.json_config).reduce(
                (prev, cur) => ({ ...prev, [cur]: false }),
                {}
              ),
              sow: {
                ...this.state.sow,
                category: template.category,
                template: templateId,
                json_config: template.json_config,
              },
              unsaved: true,
            },
            () => this.doSowCalculations()
          );
        } else {
          this.props.addErrorMessage("Error fetching template!");
        }
      });
    }
  };

  setValidationErrors = (errorList: object) => {
    const error = this.getEmptyState().error;

    Object.keys(errorList).map((key) => {
      if (key && error[key]) {
        error[key].errorState = "error";
        error[key].errorMessage = errorList[key];
      }
    });
    const isCollapsed = Object.keys(this.state.sow.json_config).reduce(
      (prev, cur) => ({ ...prev, [cur]: false }),
      {}
    );

    this.setState({ isCollapsed, error });
  };

  onClickConfirm = () => {
    this.props
      .updateQuoteStage(this.props.quote.id, Number(this.state.id))
      .then((action) => {
        if (action.type === GET_QUOTES_SUCCESS) {
          const newState: IAddSowState = cloneDeep(this.state);
          newState.error.stage.errorState = "success";
          newState.error.stage.errorMessage = "";
          (newState.isopenConfirm as boolean) = false;
          (newState.id as number) = 0;
          this.setState(newState);
        } else {
          const newState: IAddSowState = cloneDeep(this.state);
          newState.error.stage.errorState = "error";
          newState.error.stage.errorMessage =
            (action.errorList &&
              action.errorList.data &&
              action.errorList.data.detail) ||
            "Something went wrong";
          (newState.isopenConfirm as boolean) = false;
          (newState.id as number) = 0;
          this.setState(newState);
        }
      });
  };

  doSowCalculations = () => {
    if (this.state.sow.json_config && this.props.docSetting) {
      const sowCalculations = getSowCalculationFields(
        this.state.sow.json_config.service_cost,
        this.props.docSetting
      );
      const roundedValue = round(
        Number(roudingOffNext(sowCalculations.totalCustomerCost, 100)) -
          sowCalculations.totalCustomerCost
      );
      const sow = cloneDeep(this.state.sow);
      sow.json_config.service_cost.total_revenue_rounding_offset =
        this.state.sow.doc_type === "Fixed Fee" ? roundedValue : 0;
      this.setState({
        sow,
        sowCalculations,
      });
    }
  };

  toggleConfirmOpen = (id: number) => {
    this.setState({
      id,
      isopenConfirm: !this.state.isopenConfirm,
    });
  };

  toggleLogoModal = (save?: boolean) => {
    this.setState((prev) => ({ showLogoModal: !prev.showLogoModal }));
    if (save === true) this.checkForCustomerLogo(this.state.sow.customer);
  };

  checkForCustomerLogo = (customerId: number) => {
    this.props.checkCustomerLogo(customerId).then((action) => {
      if (action.type === CUSTOMER_LOGO_SUCCESS) {
        const customerHasLogo = Boolean(action.response.file_path);
        this.setState((prev) => ({
          customerHasLogo,
          sow: {
            ...prev.sow,
            include_customer_logo: !customerHasLogo
              ? false
              : prev.sow.include_customer_logo,
          },
        }));
      }
    });
  };

  getDocumentTypesOptions = () => {
    const documentTypes = this.props.documentTypes
      ? this.props.documentTypes.map((t) => ({
          value: t,
          label: t,
          disabled:
            t === "Change Request" &&
            this.state.sow.doc_type !== "Change Request",
        }))
      : [];

    return documentTypes;
  };

  getTemplateOptions = () => {
    const templates = this.state.templates
      ? this.state.templates.map((t) => ({
          value: t.id,
          label: t.name,
          disabled: false,
        }))
      : [];

    return templates;
  };

  getCategoryOptions = () => {
    const categories = this.props.categoryList
      ? this.props.categoryList.map((cat) => ({
          value: cat.id,
          label: cat.name,
          disabled:
            cat.name === "Change Request" &&
            this.state.sow.doc_type !== "Change Request",
        }))
      : [];

    return categories;
  };

  getCustomerOptions = () => {
    if (this.props.customers && this.props.customers.length > 0) {
      return this.props.customers.map((role) => ({
        value: role.id,
        label: role.name,
        disabled: false,
      }));
    } else {
      return [];
    }
  };

  getCustomerUserOptions = () => {
    const users = this.state.users
      ? this.state.users.map((t) => ({
          value: t.id,
          label: `${t.first_name} ${t.last_name}`,
          disabled: false,
        }))
      : [];

    return users;
  };

  getOpportunityOptions = () => {
    const quoteList = this.props.quoteList
      ? this.props.quoteList.map((t) => ({
          value: t.id,
          label: `${t.name} (${t.stage_name})`,
          disabled: false,
        }))
      : [];

    return quoteList;
  };

  setPhaseIds = (jsonConfig: IJSONConfig) => {
    if (
      jsonConfig.service_cost.engineering_hours_breakup &&
      jsonConfig.service_cost.engineering_hours_breakup.phases
    ) {
      jsonConfig.service_cost.engineering_hours_breakup.phases.forEach(
        (phase: IPhase) => {
          phase.id = random(1, 100000);
        }
      );
    }
  };

  setHourlyResourceIds = (jsonConfig: IJSONConfig) => {
    if (jsonConfig.service_cost.hourly_resources) {
      jsonConfig.service_cost.hourly_resources.forEach((resource) => {
        resource.id = random(1, 100000);
      });
    }
  };

  setVendorDescription = (jsonConfig: IJSONConfig) => {
    /* Sets the description for those resources which does not have a set resource description*/

    if (!jsonConfig) return;

    if (jsonConfig.service_cost.hourly_resources) {
      jsonConfig.service_cost.hourly_resources.forEach((resource) => {
        if (
          this.state.vendorDescriptionMapping.has(resource.resource_id) &&
          !resource.resource_description
        )
          resource.resource_description = this.state.vendorDescriptionMapping.get(
            resource.resource_id
          ).resource_description;
        else
          resource.resource_description = resource.resource_description
            ? resource.resource_description
            : "";
      });
    }

    if (jsonConfig.service_cost.contractors) {
      jsonConfig.service_cost.contractors.forEach((contractor) => {
        if (
          this.state.vendorDescriptionMapping.has(contractor.vendor_id) &&
          !contractor.description
        ) {
          contractor.description = this.state.vendorDescriptionMapping.get(
            contractor.vendor_id
          ).resource_description;
        }
      });
    }
  };

  getPhaseResources = (serviceCost: IServiceCost): IPickListOptions[] => {
    const getTitle = (hide: boolean, override: boolean) => {
      if (hide)
        return "This resource is marked as hidden in ESTIMATED HOURS TOTAL Section, uncheck the Hide option to select this resource";
      if (override)
        return "This resource is marked as overridden in ESTIMATED HOURS TOTAL Section, uncheck the Override option to select this resource";
      return undefined;
    };

    let vendorResources: IPickListOptions[] = [];
    const uniqueDesc: Set<string> = new Set(); // For avoiding repetitive hourly resources
    if (serviceCost.hourly_resources) {
      serviceCost.hourly_resources.forEach((el) => {
        const resourceIdentifier = getResourceIdentifier(el);

        if (!uniqueDesc.has(resourceIdentifier))
          vendorResources.push({
            value: resourceIdentifier, // this should be ideally unique
            label: (
              <div title={getTitle(el.is_hidden, el.override)}>
                {resourceIdentifier}
              </div>
            ),
            data: el.resource_id, // Storing ID of the main resource
            disabled: el.is_hidden || el.override,
          });
        uniqueDesc.add(resourceIdentifier);
      });
    }
    return [
      {
        value: "Engineer",
        label: (
          <div
            title={getTitle(
              serviceCost.hide_engineering_hours,
              serviceCost.engineering_hours_override
            )}
          >
            Engineer
          </div>
        ),
        data: -1,
        disabled:
          serviceCost.hide_engineering_hours ||
          serviceCost.engineering_hours_override,
      },
      {
        value: "After Hours Engineer",
        label: (
          <div
            title={getTitle(
              serviceCost.hide_after_hours,
              serviceCost.after_hours_override
            )}
          >
            After Hours Engineer
          </div>
        ),
        data: -2,
        disabled:
          serviceCost.hide_after_hours || serviceCost.after_hours_override,
      },
      {
        value: "Project Management",
        label: (
          <div
            title={getTitle(
              serviceCost.hide_project_management_hours,
              serviceCost.project_management_hours_override
            )}
          >
            Project Management
          </div>
        ),
        data: -3,
        disabled:
          serviceCost.hide_project_management_hours ||
          serviceCost.project_management_hours_override,
      },
      {
        value: "Integration Technician",
        label: (
          <div
            title={getTitle(
              serviceCost.hide_integration_technician_hours,
              serviceCost.integration_technician_hours_override
            )}
          >
            Integration Technician
          </div>
        ),
        data: -4,
        disabled:
          serviceCost.hide_integration_technician_hours ||
          serviceCost.integration_technician_hours_override,
      },
      ...vendorResources,
    ];
  };

  calculateTotalHours = (serviceCost: IServiceCost): number => {
    let total_hours: number =
      serviceCost.after_hours +
      serviceCost.engineering_hours +
      serviceCost.project_management_hours +
      serviceCost.integration_technician_hours;
    if (serviceCost.hourly_resources)
      serviceCost.hourly_resources.forEach((el) => {
        total_hours += el.hours ? el.hours : 0;
      });
    return total_hours;
  };

  handleChangeCategory = (e) => {
    const newState = cloneDeep(this.state);
    newState.sow[e.target.name] = e.target.value;
    newState.sow.json_config = null;
    newState.sow.template = null;
    const templates = this.props.templates.filter(
      (id) => e.target.value === id.category
    );
    (newState.templates as any) = templates;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeType = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    const docType = e.target.value as SowDocType;
    // If we change type to Fixed Fee, unhide all the resources
    if (docType === "Fixed Fee") {
      if (
        this.state.sow.json_config &&
        this.state.sow.json_config.service_cost
      ) {
        newState.sow.json_config.service_cost.hourly_resources = this.state.sow
          .json_config.service_cost.hourly_resources
          ? this.state.sow.json_config.service_cost.hourly_resources.map(
              (el) => ({
                ...el,
                is_hidden: false,
              })
            )
          : [];
        newState.sow.json_config.service_cost.hide_after_hours = false;
        newState.sow.json_config.service_cost.hide_engineering_hours = false;
        newState.sow.json_config.service_cost.hide_project_management_hours = false;
        newState.sow.json_config.service_cost.hide_integration_technician_hours = false;
        (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
          newState.sow.json_config.service_cost
        );
      }
      if (newState.riskVarRemove) {
        (newState.sow.json_config.service_cost.default_ps_risk as number) = 0;
        (newState.riskVarOverride as boolean) = true;
        (newState.riskVarRemove as boolean) = false;
      }
    }
    if (
      this.state.sow.json_config &&
      this.state.sow.json_config.project_summary
    ) {
      newState.sow.json_config.project_summary[
        "project_summary"
      ].label =
        docType === "Fixed Fee"
          ? "Project Outcome"
          : "Project Summary";
    }
    (newState.showHidden as boolean) = false;
    newState.sow.pm_type_t_and_m = false;
    newState.sow.doc_type = docType;
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doSowCalculations);
  };

  handleChangeTemplate = (e: React.ChangeEvent<HTMLInputElement>) => {
    let templateId = Number(e.target.value);
    this.props.getTemplate(templateId).then((action) => {
      if (action.type === FETCH_TEMPLATE_SUCCESS) {
        const template = action.response;
        const riskVarOverride: boolean = this.props.docSetting
          ? this.props.docSetting.default_ps_risk !==
            template.json_config.service_cost.default_ps_risk
          : false;
        template.json_config.service_cost.default_ps_risk *= 100;
        this.setState(
          (prevState) => ({
            riskVarOverride,
            isCollapsed: Object.keys(template.json_config).reduce(
              (prev, cur) => ({ ...prev, [cur]: false }),
              {}
            ),
            sow: {
              ...prevState.sow,
              template: templateId,
              category: template.category,
              json_config: template.json_config,
            },
            unsaved: true,
          }),
          this.doSowCalculations
        );
      } else {
        this.props.addErrorMessage("Error fetching template!");
      }
    });
  };

  handleChangeCustomer = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prevState) => ({
      sow: {
        ...prevState.sow,
        user: null,
        quote_id: null,
        [e.target.name]: e.target.value,
      },
      unsaved: true,
    }));
    this.checkForCustomerLogo(Number(e.target.value));
    this.props.fetchQuoteDashboardListing(Number(e.target.value), true);
    this.props.fetchAllCustomerUsers(Number(e.target.value)).then((a) => {
      if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const ns = cloneDeep(this.state);
        (ns.users as any) = a.response;
        this.setState(ns);
      }
    });
  };

  handleChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: string,
    field: string,
    type: string
  ) => {
    const newState = cloneDeep(this.state);
    (newState.sow.json_config[key][field][type] as string) = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeCheckBox = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: string,
    field: string,
    type: string,
    optionIndex: number
  ) => {
    const newState = cloneDeep(this.state);
    (newState.sow.json_config[key][field][type][optionIndex]
      .active as boolean) = e.target.checked;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeSubSection = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx: number,
    field: string,
    type: string
  ) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.implementation_logistics.sections[idx][field][
      type
    ] = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeSimpleFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState((prevState) => ({
      sow: { ...prevState.sow, [e.target.name]: e.target.value },
      unsaved: true,
    }));
  };

  handleChangeQuote = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.toggleConfirmOpen(Number(e.target.value));
  };

  handleChangeMarkdown = (
    html: string,
    key: string,
    field: string,
    type: string,
    source: string
  ) => {
    const newState = cloneDeep(this.state);
    (newState.sow.json_config[key][field][type] as string) = html;
    (newState.unsaved as boolean) = source === "api" ? false : true;
    this.setState(newState);
  };

  handleClickResetMarkdown = (key: string, field: string, type: string) => {
    const newState = cloneDeep(this.state);
    if (key === "terms_fixed_fee" && field === "generated_terms") {
      newState.sow.json_config = this.assignGeneratedTerms(
        newState.sow.json_config
      );
    } else {
      const settingKey =
        key === "terms_t_and_m" && this.state.sow.pm_type_t_and_m
          ? "pm_type_t_and_m_terms"
          : key;
      (newState.sow.json_config[key][field][type] as string) = this.props
        .docSetting[settingKey]
        ? this.props.docSetting[settingKey]
        : "";
    }
    this.setState(newState);
  };

  handlePhaseReorder = (phases: IPhase[]) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.engineering_hours_breakup.phases = phases;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleMilestoneReorder = (milestones: IMilestone[]) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.milestones = milestones;
    newState.sow.json_config = this.assignGeneratedTerms(
      newState.sow.json_config
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleHourlyResourceReorder = (hourlyResources: IHourlyResource[]) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.hourly_resources = hourlyResources;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  // render methods
  renderTemplateByJson = (rawJson: IJSONConfig) => {
    let count = 0;
    const json: IJSONConfig = Object.assign({}, rawJson);
    delete json.service_cost;

    return (
      <div className="template-fields col-md-12 row">
        {json &&
          Object.keys(json).length > 0 &&
          Object.keys(json)
            .filter(
              (field) =>
                // For showing only the required data related to doc_typ
                json[field].visible_in &&
                json[field].visible_in.includes(this.state.sow.doc_type) &&
                // Should this be hidden in SoW Document
                // (currently for Implementation Logistics)
                !json[field].hide_in_sow &&
                // For showing the hidden sections
                (this.state.showHidden === true
                  ? true
                  : !json[field].default_hidden)
            )
            .sort((a, b) => json[a].ordering - json[b].ordering)
            .map((key, fieldIndex) => {
              const isCollapsed = !this.state.isCollapsed[key];
              const toggleCollapsedState = () =>
                this.setState((prevState) => ({
                  isCollapsed: { ...prevState.isCollapsed, [key]: isCollapsed },
                }));
              const sectionLabel =
                key === "terms_t_and_m" && this.state.sow.pm_type_t_and_m
                  ? "Terms T & M (Fee based Project Management) "
                  : `${json[key].section_label} `;

              return (
                <div
                  className={`field ${isCollapsed ? "field--collapsed" : ""}`}
                  key={fieldIndex}
                >
                  <div
                    className="section-heading"
                    onClick={toggleCollapsedState}
                  >
                    {sectionLabel}
                    <div className="action-collapse">
                      {isCollapsed ? "+" : "-"}
                    </div>
                  </div>
                  <div className="body-section">
                    {key &&
                      Object.keys(json[key])
                        .sort(
                          (a, b) =>
                            ((json[key][a] && json[key][a].ordering) || 0) -
                            ((json[key][b] && json[key][b].ordering) || 0)
                        )
                        .map((field, i) => {
                          if (
                            field === "visible_in" ||
                            field === "section_label" ||
                            field === "ordering" ||
                            field === "default_hidden" ||
                            field === "hide_in_sow"
                          ) {
                            return false;
                          }
                          count++;

                          return key === "implementation_logistics" &&
                            json.implementation_logistics.sections &&
                            json.implementation_logistics.sections.length >
                              0 ? (
                            this.renderImplementationLogisticsSections(
                              json.implementation_logistics.sections
                            )
                          ) : (
                            <div
                              className={`${
                                json[key][field].type === "MARKDOWN"
                                  ? "single-field col-lg-12 col-md-12 col-xs-12"
                                  : "single-field col-lg-12 col-md-12 col-xs-12"
                              }`}
                              key={i}
                            >
                              {json[key][field].type === "MARKDOWN" && (
                                <div
                                  className={`${
                                    this.state.errorsValue &&
                                    this.state.errorsValue[count] &&
                                    this.state.errorsValue[count].errorMessage
                                      ? "markdown markdown-error"
                                      : "markdown"
                                  }`}
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  {json[key][field].reset && (
                                    <label
                                      className="img-button-reset"
                                      onClick={() =>
                                        this.handleClickResetMarkdown(
                                          key,
                                          field,
                                          "value"
                                        )
                                      }
                                    >
                                      <img
                                        className="icon-reset"
                                        src="/assets/icons/reset.svg"
                                        title="Reset to default from setting"
                                      />
                                    </label>
                                  )}
                                  <QuillEditorAcela
                                    onChange={(e, source) =>
                                      this.handleChangeMarkdown(
                                        e,
                                        key,
                                        field,
                                        "value",
                                        source
                                      )
                                    }
                                    label={`${
                                      json[key][field].is_label_editable ===
                                      "true"
                                        ? "Value"
                                        : json[key][field].label
                                    }`}
                                    value={json[key][field].value}
                                    scrollingContainer=".add-sow"
                                    wrapperClass={"sow-config-quill"}
                                    isRequired={
                                      String(json[key][field].is_required) ===
                                      "true"
                                    }
                                    error={
                                      this.state.errorsValue &&
                                      this.state.errorsValue[count] &&
                                      this.state.errorsValue[count].errorMessage
                                        ? {
                                            errorState: "error",
                                            errorMessage: this.state
                                              .errorsValue[count].errorMessage,
                                          }
                                        : undefined
                                    }
                                  />
                                </div>
                              )}
                              {json[key][field].type === "TEXTBOX" && (
                                <div className="input-label-box">
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  <Input
                                    field={{
                                      value: json[key][field].value,
                                      label: `${
                                        json[key][field].is_label_editable ===
                                        "true"
                                          ? "Value"
                                          : json[key][field].label
                                      }`,
                                      type: "TEXT",
                                      isRequired:
                                        json[key][field].is_required &&
                                        JSON.parse(
                                          json[key][field].is_required
                                        ),
                                    }}
                                    width={8}
                                    name={field}
                                    onChange={(e) =>
                                      this.handleChange(e, key, field, "value")
                                    }
                                    error={this.state.errorsValue[count]}
                                  />
                                </div>
                              )}
                              {json[key][field].type === "RADIOBOX" && (
                                <div
                                  key={field}
                                  className="options input-label-box"
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}

                                  <div className="options-content">
                                    <Input
                                      field={{
                                        value: json[key][field].value,
                                        label:
                                          json[key][field].is_label_editable ===
                                          "false"
                                            ? json[key][field].label
                                            : "",
                                        type: "RADIO",
                                        isRequired: false,
                                        options: json[key][field].options.map(
                                          (role: string) => ({
                                            value: role,
                                            label: role,
                                          })
                                        ),
                                      }}
                                      width={8}
                                      name={field + count}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "value"
                                        )
                                      }
                                      error={this.state.errorsValue[count]}
                                    />
                                  </div>
                                </div>
                              )}
                              {json[key][field].type === "CHECKBOX" && (
                                <div
                                  key={field}
                                  className="options input-label-box"
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  <div className="options-content checkbox-option">
                                    {json[key][field].options &&
                                      json[key][field].options.map(
                                        (option, optionIndex) => {
                                          return (
                                            <Checkbox
                                              isChecked={option.active}
                                              key={optionIndex}
                                              name="loa"
                                              onChange={(e) =>
                                                this.handleChangeCheckBox(
                                                  e,
                                                  key,
                                                  field,
                                                  "options",
                                                  optionIndex
                                                )
                                              }
                                            >
                                              {option.label}
                                            </Checkbox>
                                          );
                                        }
                                      )}
                                  </div>
                                  {this.state.errorsValue &&
                                    this.state.errorsValue[count] &&
                                    this.state.errorsValue[count]
                                      .errorMessage && (
                                      <div className="select-markdown-error">
                                        {
                                          this.state.errorsValue[count]
                                            .errorMessage
                                        }
                                      </div>
                                    )}
                                </div>
                              )}
                            </div>
                          );
                        })}
                  </div>
                </div>
              );
            })}
        {this.state.error.json_config.errorMessage && (
          <div className="board-error">
            {this.state.error.json_config.errorMessage}
          </div>
        )}
      </div>
    );
  };

  getColWidth = (type: string) => {
    let width = "col-md-6";
    switch (type) {
      case "MARKDOWN":
        width = "col-md-6";
        break;
      case "TEXTBOX":
        width = "col-md-5";
        break;
      case "TEXTAREA":
        width = "col-md-5";
        break;
      case "RADIOBOX":
        width = "col-md-2";
        break;

      default:
        break;
    }

    return width;
  };

  renderImplementationLogisticsSections = (
    sections: {
      column1?: IJSONConfigSectionInfo;
      column2?: IJSONConfigSectionInfo;
      column3?: IJSONConfigSectionInfo;
      column4?: IJSONConfigSectionInfo;
      delivery_model: IJSONConfigSectionInfo;
      detail: IJSONConfigSectionInfo;
    }[]
  ) => {
    let count = 0;
    return (
      <div className="sub-section-fields col-md-12" key={1000}>
        {sections.map((section, fieldIndex) => {
          return (
            <div className={`sub-section col-md-12`} key={fieldIndex}>
              {Object.keys(section)
                .sort(
                  (a, b) =>
                    (section[a].ordering || 0) - (section[b].ordering || 0)
                )
                .map((field, i) => {
                  count++;

                  return (
                    <div
                      className={`${this.getColWidth(section[field].type)}`}
                      key={i}
                    >
                      {section[field].type === "TEXTBOX" && (
                        <div className="input-label-box">
                          <Input
                            field={{
                              value: section[field].value,
                              label: `${
                                section[field].is_label_editable === "true"
                                  ? "Value"
                                  : section[field].label
                              }`,
                              type: "TEXT",
                              isRequired:
                                section[field].is_required &&
                                JSON.parse(section[field].is_required),
                            }}
                            width={12}
                            name={field}
                            onChange={(e) =>
                              this.handleChangeSubSection(
                                e,
                                fieldIndex,
                                field,
                                "value"
                              )
                            }
                            error={section[field].error && section[field].error}
                          />
                        </div>
                      )}
                      {section[field].type === "TEXTAREA" && (
                        <div className="input-label-box">
                          <Input
                            field={{
                              value: section[field].value,
                              label: `${
                                section[field].is_label_editable === "true"
                                  ? "Value"
                                  : section[field].label
                              }`,
                              type: "TEXTAREA",
                              isRequired:
                                section[field].is_required &&
                                JSON.parse(section[field].is_required),
                            }}
                            width={12}
                            name={field}
                            onChange={(e) =>
                              this.handleChangeSubSection(
                                e,
                                fieldIndex,
                                field,
                                "value"
                              )
                            }
                            error={section[field].error && section[field].error}
                          />
                        </div>
                      )}
                      {section[field].type === "RADIOBOX" && (
                        <div key={field} className="options input-label-box">
                          <div className="options-content">
                            <Input
                              field={{
                                value: section[field].value,
                                label:
                                  section[field].is_label_editable === "false"
                                    ? section[field].label
                                    : "",
                                type: "RADIO",
                                isRequired: false,
                                options: section[field].options.map(
                                  (role: string) => ({
                                    value: role,
                                    label: role,
                                  })
                                ),
                              }}
                              width={12}
                              name={field + count}
                              onChange={(e) =>
                                this.handleChangeSubSection(
                                  e,
                                  fieldIndex,
                                  field,
                                  "value"
                                )
                              }
                              error={
                                section[field].error && section[field].error
                              }
                            />
                          </div>
                        </div>
                      )}
                    </div>
                  );
                })}
            </div>
          );
        })}
        {this.state.error.json_config.errorMessage && (
          <div className="board-error">
            {this.state.error.json_config.errorMessage}
          </div>
        )}
      </div>
    );
  };

  renderPhase = ({ tag, index }: { tag: IPhase; index: number }) => {
    const currentPhaseResource = this.state.phaseResources.find(
      // Resource Name will be ideally unique, it can be resource_description, resource_name or any of the defined Resource i.e Engineer, etc.
      (el) => el.value == tag.resource_name
    );
    let isDisabled: boolean = currentPhaseResource
      ? Boolean(currentPhaseResource.disabled)
      : false;
    const disabledText: string = isDisabled
      ? "The Resource for this Phase is marked as hidden in ESTIMATED HOURS TOTAL section, to modify this Phase uncheck the Hide option"
      : undefined;
    isDisabled = isDisabled || this.state.orderingPhase;

    return (
      <div
        className={
          "col-md-12 mapping-row" +
          (this.state.orderingPhase ? " ordering-phase" : "")
        }
        key={index}
      >
        <Input
          field={{
            label: "",
            type: "TEXT",
            value: tag.name,
            isRequired: false,
          }}
          width={4}
          multi={true}
          disabled={tag.default || isDisabled}
          name="name"
          title={disabledText}
          onChange={(e) => this.handleChangeStaticCalculationsPhases(e, index)}
          placeholder={`Enter Name`}
          className="phase-name"
          error={tag.errorName}
        />
        <div className="override-img col-md-1">
          <img
            className="daily-check-passed"
            alt=""
            src={`/assets/icons/${tag.hours === 0 ? "delete" : "check"}.png`}
            title=""
          />
        </div>
        <Input
          field={{
            label: "",
            type: "NUMBER",
            value: tag.hours,
            isRequired: false,
          }}
          width={2}
          name="hours"
          minimumValue={"0"}
          disabled={isDisabled}
          title={disabledText}
          onChange={(e) => this.handleChangeStaticCalculationsPhases(e, index)}
          placeholder={`Hours`}
        />
        <Input
          field={{
            label: "",
            type: "PICKLIST",
            options: this.state.phaseResources,
            value: tag.resource_name,
            isRequired: false,
          }}
          width={4}
          multi={false}
          name="resource"
          onChange={(e) => this.handleChangeStaticCalculationsPhases(e, index)}
          placeholder={`Select Resource`}
          loading={this.props.isFetchingVendors}
          disabled={isDisabled}
          title={disabledText}
          error={tag.errorResource}
        />
        {!isDisabled && (
          <SmallConfirmationBox
            showButton={true}
            onClickOk={() => this.deleteCalculationsPhases(index)}
            className="col-md-1"
            text="phase"
          />
        )}
      </div>
    );
  };

  renderMilestone = ({ tag, index }: { tag: IMilestone; index: number }) => {
    let isDisabled = this.state.orderingMilestones;

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    const customerCost = formatter.format(
      (tag.milestone_percent / 100) *
        this.state.sowCalculations.totalCustomerCost
    );
    const grossProfit = formatter.format(
      (tag.milestone_percent / 100) * this.state.sowCalculations.totalMargin
    );

    return (
      <div
        className={
          "col-md-12 milestone-row" +
          (this.state.orderingMilestones ? " ordering-milestone" : "")
        }
        key={tag.id}
      >
        <Input
          field={{
            label: "Milestone Name",
            type: "TEXT",
            value: tag.name,
            isRequired: false,
          }}
          width={12}
          multi={true}
          disabled={isDisabled}
          name="name"
          onChange={(e) => this.handleChangeMilestone(e, index)}
          placeholder={`Enter Name`}
          className="phase-name"
          error={
            tag.errorName && tag.errorName.errorState === "error"
              ? tag.errorName
              : {
                  errorState: "warning",
                  errorMessage: tag.terms
                    ? `${tag.milestone_percent}% ${tag.terms}`
                    : "",
                }
          }
        />
        <Input
          field={{
            label: "Milestone %",
            type: "NUMBER",
            value: tag.milestone_percent,
            isRequired: false,
          }}
          width={12}
          name="milestone_percent"
          minimumValue="0"
          maximumValue="100"
          disabled={isDisabled}
          onChange={(e) => this.handleChangeMilestone(e, index)}
          placeholder={`Enter %`}
          error={tag.errorMargin}
        />
        <Input
          field={{
            label: "Customer Cost(in $)",
            type: "TEXT",
            value: customerCost,
            isRequired: false,
          }}
          width={12}
          name="customerCost"
          onChange={(e) => null}
          placeholder={`0`}
          disabled={true}
        />
        <Input
          field={{
            label: "GP $",
            type: "TEXT",
            value: grossProfit,
            isRequired: false,
          }}
          width={12}
          name="grossProfit"
          onChange={(e) => null}
          placeholder={`0`}
          disabled={true}
        />
        <Input
          field={{
            label: "Terms and Condition",
            type: "TEXT",
            value: tag.terms,
            isRequired: false,
          }}
          width={12}
          name="terms"
          onChange={(e) => this.handleChangeMilestone(e, index)}
          placeholder={`Enter Terms`}
          disabled={isDisabled}
          error={tag.errorTerms}
        />
        {!isDisabled && (
          <SmallConfirmationBox
            showButton={true}
            onClickOk={() => this.deleteMilestone(index)}
            className="delete-milestone"
            text="milestone"
          />
        )}
      </div>
    );
  };

  renderHourlyResource = ({
    tag,
    index,
  }: {
    tag: IHourlyResource;
    index: number;
  }) => {
    const isTMDoc: boolean = this.state.sow.doc_type === "T & M";

    return (
      <div
        className={
          "col-md-12 mapping-row" +
          (this.state.orderingHourlyResources ? " ordering-phase" : "")
        }
        key={index}
      >
        {!this.state.orderingHourlyResources && (
          <EditButton
            title="Edit Resource Description"
            onClick={() =>
              this.handleClickEditDescription(tag, index, "Hourly Resource")
            }
          />
        )}
        <Input
          field={{
            label: "",
            type: "PICKLIST",
            options: this.props.vendorOptions,
            value: tag.resource_id,
            isRequired: false,
          }}
          width={3}
          multi={false}
          name="resource_id"
          onChange={(e) => this.handleChangeStatic(e, index)}
          disabled={this.state.orderingHourlyResources}
          placeholder={`Select Resource`}
          loading={this.props.isFetchingVendors}
          error={
            tag.errorName && tag.errorName.errorState === "error"
              ? tag.errorName
              : {
                  errorState: "warning",
                  errorMessage: tag.resource_description
                    ? tag.resource_description
                    : "",
                }
          }
        />
        <Input
          field={{
            value: tag.hours,
            label: "",
            type: "NUMBER",
            isRequired: false,
          }}
          width={2}
          name={"hours"}
          minimumValue={"0"}
          placeholder="Enter Hours"
          onChange={(e) => this.handleChangeStatic(e, index)}
          className="phase-name"
          title={
            tag.is_hidden
              ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
              : undefined
          }
          disabled={
            tag.is_hidden || !tag.override || this.state.orderingHourlyResources
          }
        />

        <div
          className={`col-md-1 override-column ${
            isTMDoc ? " hide-column" : ""
          }`}
        >
          <Checkbox
            isChecked={tag.override}
            name="override"
            onChange={(e) => this.showOverrideConfirmationPopup(e, index)}
            title={
              tag.is_hidden
                ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                : undefined
            }
            disabled={tag.is_hidden || this.state.orderingHourlyResources}
          />
        </div>
        {isTMDoc && (
          <div className={`col-md-1 override-column hide-column`}>
            <Checkbox
              isChecked={tag.is_hidden}
              name="is_hidden"
              disabled={tag.hours !== 0 || this.state.orderingHourlyResources}
              title={
                tag.hours !== 0
                  ? "Hide option is enabled when Total Hours has 0 value"
                  : "If this option is checked, the resource will be hidden in SoW"
              }
              onChange={(e) => this.handleChangeStaticHide(e, index)}
            />
          </div>
        )}
        <Input
          field={{
            value: tag.hourly_rate,
            label: "",
            type: "NUMBER",
            isRequired: false,
          }}
          width={2}
          name={"hourly_rate"}
          minimumValue={"0"}
          placeholder="Enter Rate"
          onChange={(e) => this.handleChangeStatic(e, index)}
          error={tag.errorRate}
          disabled={this.state.orderingHourlyResources}
        />
        <Input
          field={{
            value: tag.hourly_cost,
            label: "",
            type: "NUMBER",
            isRequired: false,
          }}
          width={2}
          name={"hourly_cost"}
          minimumValue={"0"}
          placeholder="Enter Rate"
          disabled={this.state.orderingHourlyResources}
          onChange={(e) => this.handleChangeStatic(e, index)}
          error={tag.errorCost}
        />
        <div className="col-md-1 mapping-row-text">
          {tag.margin_percentage + "%"}
        </div>
        {!this.state.orderingHourlyResources && (
          <SmallConfirmationBox
            text="Hourly Resource"
            showButton={true}
            onClickOk={() => this.removeHourlyResource(index)}
            className={isTMDoc ? "remove-vendor-tm" : "remove-vendor"}
          />
        )}
      </div>
    );
  };

  renderMilestonesCollapsable = () => {
    const serCostObj: IServiceCost = this.state.sow.json_config.service_cost;
    const isCollapsed = !Boolean(this.state.isCollapsed.milestones);
    const toggleCollapsedState = () =>
      this.setState((prevState) => ({
        isCollapsed: { ...prevState.isCollapsed, milestones: isCollapsed },
      }));

    return (
      <div className="milestone-section template-fields col-md-12 row">
        <div className={`field ${isCollapsed ? "field--collapsed" : ""}`}>
          <div className="section-heading" onClick={toggleCollapsedState}>
            MILESTONES
            <div className="action-collapse">{isCollapsed ? "+" : "-"}</div>
          </div>
          <div className="body-section">
            <Checkbox
              isChecked={this.state.orderingMilestones}
              name="orderingMilestones"
              onChange={this.handleChangeOrderingCheckbox}
              className="order-checkbox"
            >
              Reorder Milestones
            </Checkbox>
            {this.state.error.milestone_percent &&
              this.state.error.milestone_percent.errorState ===
                "error" && (
                <div className="section-error">
                  {this.state.error.milestone_percent.errorMessage}
                </div>
              )}
            {serCostObj.milestones && (
              <>
                {this.state.orderingMilestones ? (
                  <DraggableArea
                    isList
                    tags={serCostObj.milestones}
                    render={this.renderMilestone}
                    onChange={(tags) => this.handleMilestoneReorder(tags)}
                  />
                ) : (
                  serCostObj.milestones.map((milestone, index) =>
                    this.renderMilestone({ tag: milestone, index })
                  )
                )}
              </>
            )}
            {!this.state.orderingMilestones && (
              <div className="add-new-row" onClick={this.addNewMilestone}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Milestone</span>
              </div>
            )}
            {this.getMilestonePreview(serCostObj.milestones)}
          </div>
        </div>
      </div>
    );
  };

  renderStaticFields = () => {
    const serCostObj: IServiceCost = this.state.sow.json_config.service_cost;
    if (!serCostObj) {
      return null;
    }

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    const serviceCost: ISoWCalculationFields = this.state.sowCalculations;
    const recommendedHours: number = this.getProjectManagementRecommendedHours();
    const isTMDoc: boolean = this.state.sow.doc_type === "T & M";
    let totalCustomerCost = serviceCost.totalCustomerCost;
    let totalMargin = serviceCost.totalMargin;
    let totalMarginPercent = serviceCost.totalMarginPercent;
    if (!isTMDoc) {
      totalCustomerCost = round(
        totalCustomerCost + serCostObj.total_revenue_rounding_offset
      );
      totalMargin = round(totalCustomerCost - serviceCost.totalInternalCost);
      totalMarginPercent =
        totalCustomerCost !== 0
          ? round((totalMargin / totalCustomerCost) * 100, 1)
          : 0;
    }

    return (
      <div className="template-fields col-md-12 row">
        <div className="section-heading">
          SERVICE COST
          <div className="action-collapse">{""}</div>
        </div>
        <div className="service-cost-body">
          {serCostObj.engineering_hours_breakup && (
            <>
              <div className="project-scope-detail">
                <div className="sub-heading col-md-12">
                  <div className="text">PROJECT SCOPE DETAIL</div>
                </div>
                <div className="project-scope-content col-md-12">
                  <div className="total-nos">
                    <div className="project-scope-row">
                      <Input
                        field={{
                          value:
                            serCostObj.engineering_hours_breakup.total_sites,
                          label: (
                            <div className="override">
                              Total no. of sites
                              <span className="hours-override-required ">
                                *
                              </span>
                            </div>
                          ),
                          type: "NUMBER",
                          isRequired: false,
                        }}
                        width={4}
                        name={"total_sites"}
                        minimumValue={"0"}
                        placeholder="Enter Rate"
                        onChange={(e) => this.handleChangeStaticCalculations(e)}
                        className="select-type"
                        disabled={
                          serCostObj.engineering_hours_breakup
                            .total_sites_override
                        }
                      />
                      <Checkbox
                        isChecked={
                          serCostObj.engineering_hours_breakup
                            .total_sites_override
                        }
                        name="total_sites_override"
                        onChange={(e) =>
                          this.handleChangeSiteCutOversOverride(e)
                        }
                      >
                        Remove from SOW
                      </Checkbox>
                    </div>
                    <div className="project-scope-row">
                      <Input
                        field={{
                          value:
                            serCostObj.engineering_hours_breakup.total_cutovers,
                          label: (
                            <div className="override">
                              Total no. of cutovers
                              <span className="hours-override-required ">
                                *
                              </span>
                            </div>
                          ),
                          type: "NUMBER",
                          isRequired: false,
                        }}
                        width={4}
                        name={"total_cutovers"}
                        minimumValue={"0"}
                        placeholder="Enter Rate"
                        onChange={(e) => this.handleChangeStaticCalculations(e)}
                        className="select-type"
                        disabled={
                          serCostObj.engineering_hours_breakup
                            .total_cutovers_override
                        }
                      />
                      <Checkbox
                        isChecked={
                          serCostObj.engineering_hours_breakup
                            .total_cutovers_override
                        }
                        name="total_cutovers_override"
                        onChange={(e) =>
                          this.handleChangeSiteCutOversOverride(e)
                        }
                      >
                        Remove from SOW
                      </Checkbox>
                    </div>
                  </div>
                  <div className="risk-control-section">
                    <div className="risk-variable-row">
                      <Input
                        field={{
                          value: serCostObj.default_ps_risk,
                          label: "Risk Variable (in %)",
                          type: "NUMBER",
                          isRequired: false,
                        }}
                        width={5}
                        labelIcon="info"
                        labelTitle={
                          "(i) - The risk variable controls the amount of risk added to the project. This variable controls the amount of risk budget added to the project. Based on the project variables add or reduce risk."
                        }
                        name={"default_ps_risk"}
                        className="risk-variable-input"
                        placeholder="Enter Risk Variable"
                        onChange={this.handleChangeRiskVariable}
                        disabled={
                          !this.state.riskVarOverride ||
                          this.state.riskVarRemove
                        }
                        error={this.state.error.default_ps_risk}
                      />
                      <Checkbox
                        isChecked={this.state.riskVarOverride}
                        name="risk-variable-override"
                        onChange={this.handleChangeRiskVarOverride}
                        className="risk-variable-override"
                        disabled={this.state.riskVarRemove}
                      >
                        Override
                      </Checkbox>
                      {isTMDoc && (
                        <Checkbox
                          isChecked={this.state.riskVarRemove}
                          name="riskVarRemove"
                          onChange={this.handleChangeRemoveRisk}
                        >
                          Remove Risk Budget
                        </Checkbox>
                      )}
                    </div>
                    {!isTMDoc && (
                      <Input
                        field={{
                          value: serCostObj.hide_travel_expenses
                            ? "Hide"
                            : String(Boolean(serCostObj.bill_travel_expenses)),
                          label: "Bill Travel expenses based on actuals?",
                          type: "RADIO",
                          isRequired: false,
                          options: [
                            { value: "true", label: "Yes" },
                            {
                              value: "false",
                              label: "No, Travel costs have been accounted for",
                            },
                            {
                              value: "Hide",
                              label: "Travels are not expected",
                            },
                          ],
                        }}
                        width={12}
                        name="bill_travel_expenses"
                        onChange={this.handleChangeBillExpenses}
                        className="bill-expenses-radio"
                      />
                    )}
                  </div>
                </div>
              </div>
              <div className="staging-section">
                <label htmlFor="Staging at LP">Staging at LookingPoint?</label>
                <Checkbox
                  isChecked={serCostObj.staging}
                  name="staging"
                  onChange={this.handleChangeStaging}
                  className="staging-checkbox"
                />
              </div>
              <div className="phases">
                <div className="sub-heading col-md-12">
                  <div className="text">
                    <span>PHASES</span>
                  </div>
                  <Checkbox
                    isChecked={this.state.orderingPhase}
                    name="orderingPhase"
                    onChange={this.handleChangeOrderingCheckbox}
                    className="order-checkbox"
                  >
                    Reorder Phases
                  </Checkbox>
                </div>
                <div className="phase-header">
                  <div className="col-md-4">Phase</div>
                  <div className="col-md-1 override-image" />
                  <div className="col-md-2">Hours</div>
                  <div className="col-md-3">Resource</div>
                </div>
                {serCostObj.engineering_hours_breakup.phases && (
                  <>
                    <div className="col-md-12 mapping-row default-mapping-row">
                      <div className="col-md-4 mapping-row-text">
                        Project Management
                        <span className="info-recomended">
                          {`Recommended Hours: ${recommendedHours}`}
                        </span>
                      </div>
                      <div className="col-md-1 override-image" />
                      <div className="col-md-2 mapping-row-text">
                        {Math.round(recommendedHours)}
                      </div>
                      <div className="col-md-3 mapping-row-text">
                        Project Management
                      </div>
                    </div>
                    {this.state.orderingPhase ? (
                      <DraggableArea
                        isList
                        tags={serCostObj.engineering_hours_breakup.phases}
                        render={this.renderPhase}
                        onChange={(tags) => this.handlePhaseReorder(tags)}
                      />
                    ) : (
                      serCostObj.engineering_hours_breakup.phases.map(
                        (phase, index) =>
                          this.renderPhase({ tag: phase, index })
                      )
                    )}
                  </>
                )}
                <div
                  className="add-new-row"
                  onClick={this.addNewCalculationsPhases}
                >
                  <span className="add-new-row-plus">+</span>
                  <span className="add-new-row-text">Add Phase</span>
                </div>
              </div>
            </>
          )}
          <div className="estimated-hours-rates-container">
            <div className="sub-heading col-md-12">
              <div className="text">
                <span>ESTIMATED HOURS TOTAL</span>
                <div className="eht-actions-right">
                  <img
                    className="icon-reset-static"
                    src="/assets/icons/reset.svg"
                    title="Reset to default from setting"
                    onClick={this.setDefaultStatic}
                  />
                  <SquareButton
                    content={
                      <>
                        <span className="add-plus">+</span>
                        <span className="add-text">Create Vendor</span>
                      </>
                    }
                    className="add-vendor add-btn"
                    bsStyle={"link"}
                    onClick={() => this.toggleVendorModal(true)}
                    title={"Create new vendor"}
                  />
                </div>
              </div>
              <Checkbox
                isChecked={this.state.orderingHourlyResources}
                name="orderingHourlyResources"
                onChange={this.handleChangeOrderingCheckbox}
                className="order-checkbox"
              >
                Reorder Hourly Resources
              </Checkbox>
            </div>
            <div className="estimated-hours-rates">
              <div className="estimated-hours-header">
                <div className="col-md-3">Resource</div>
                <div className="col-md-2">Total Hours</div>
                <div
                  className={`col-md-1 ${
                    isTMDoc ? "hide-header" : "override-header"
                  }`}
                >
                  Override
                </div>
                {isTMDoc && <div className="hide-header">Hide</div>}
                <div className="col-md-2">Hourly Rate (in $)</div>
                <div className="col-md-2">Hourly Cost (in $)</div>
                <div className="col-md-1">GP%</div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>Engineer</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.engineering_hours_description
                      ? this.props.docSetting.engineering_hours_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: serCostObj.engineering_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  placeholder="Enter Hours"
                  name={"engineering_hours"}
                  className="phase-name"
                  onChange={(e) => this.handleChangeStatic(e)}
                  title={
                    serCostObj.hide_engineering_hours
                      ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={
                    serCostObj.hide_engineering_hours ||
                    !serCostObj.engineering_hours_override
                  }
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={serCostObj.engineering_hours_override}
                    name="engineering_hours_override"
                    onChange={(e) => this.showOverrideConfirmationPopup(e)}
                    title={
                      serCostObj.hide_engineering_hours
                        ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                        : undefined
                    }
                    disabled={serCostObj.hide_engineering_hours}
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_engineering_hours}
                      name="hide_engineering_hours"
                      disabled={serCostObj.engineering_hours !== 0}
                      title={
                        serCostObj.engineering_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in SoW"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.engineering_hourly_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  name={"engineering_hourly_rate"}
                  placeholder="Enter Rate"
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.engineering_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.engineeringHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.engineeringHoursMarginPercent)
                    ? serviceCost.engineeringHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>After Hours Engineer</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.after_hours_description
                      ? this.props.docSetting.after_hours_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: serCostObj.after_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"after_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  title={
                    serCostObj.hide_after_hours
                      ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={
                    serCostObj.hide_after_hours ||
                    !serCostObj.after_hours_override
                  }
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={serCostObj.after_hours_override}
                    name="after_hours_override"
                    onChange={(e) => this.showOverrideConfirmationPopup(e)}
                    title={
                      serCostObj.hide_after_hours
                        ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                        : undefined
                    }
                    disabled={serCostObj.hide_after_hours}
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_after_hours}
                      name="hide_after_hours"
                      disabled={serCostObj.after_hours !== 0}
                      title={
                        serCostObj.after_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in SoW"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.after_hours_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"after_hours_rate"}
                  placeholder="Enter Rate"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.after_hours_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.afterHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.afterHoursMarginPercent)
                    ? serviceCost.afterHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>Integration Technician</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.integration_technician_description
                      ? this.props.docSetting.integration_technician_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: serCostObj.integration_technician_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"integration_technician_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  title={
                    serCostObj.hide_integration_technician_hours
                      ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={
                    serCostObj.hide_integration_technician_hours ||
                    !serCostObj.integration_technician_hours_override
                  }
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={serCostObj.integration_technician_hours_override}
                    name="integration_technician_hours_override"
                    onChange={(e) => this.showOverrideConfirmationPopup(e)}
                    title={
                      serCostObj.hide_integration_technician_hours
                        ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                        : undefined
                    }
                    disabled={serCostObj.hide_integration_technician_hours}
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_integration_technician_hours}
                      name="hide_integration_technician_hours"
                      disabled={serCostObj.integration_technician_hours !== 0}
                      title={
                        serCostObj.integration_technician_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in SoW"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.integration_technician_hourly_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"integration_technician_hourly_rate"}
                  placeholder="Enter Rate"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.integration_technician_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(
                    serviceCost.integrationTechnicianCustomerCost
                  )}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.integrationTechnicianMarginPercent)
                    ? serviceCost.integrationTechnicianMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text eht-static-text">
                  <div>Project Management</div>
                  <div className="static-text-desc">
                    {this.props.docSetting &&
                    this.props.docSetting.project_management_hours_description
                      ? this.props.docSetting
                          .project_management_hours_description
                      : ""}
                  </div>
                </div>
                <Input
                  field={{
                    value: serCostObj.project_management_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"project_management_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  title={
                    serCostObj.hide_project_management_hours
                      ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={
                    serCostObj.hide_project_management_hours ||
                    !serCostObj.project_management_hours_override
                  }
                />

                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={serCostObj.project_management_hours_override}
                    name="project_management_hours_override"
                    onChange={(e) => this.showOverrideConfirmationPopup(e)}
                    title={
                      serCostObj.hide_project_management_hours
                        ? "Resource is marked to hide in the SoW, to modify Hours uncheck the Hide option"
                        : undefined
                    }
                    disabled={serCostObj.hide_project_management_hours}
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_project_management_hours}
                      name="hide_project_management_hours"
                      disabled={serCostObj.project_management_hours !== 0}
                      title={
                        serCostObj.project_management_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in SoW"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.project_management_hourly_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  name={"project_management_hourly_rate"}
                  placeholder="Enter Rate"
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.project_management_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.pmHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.pmHoursMarginPercent)
                    ? serviceCost.pmHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              {serCostObj.hourly_resources &&
                (this.state.orderingHourlyResources ? (
                  <DraggableArea
                    isList
                    tags={serCostObj.hourly_resources}
                    render={this.renderHourlyResource}
                    onChange={(tags) => this.handleHourlyResourceReorder(tags)}
                  />
                ) : (
                  serCostObj.hourly_resources.map(
                    (hourlyResource: IHourlyResource, index) =>
                      this.renderHourlyResource({ tag: hourlyResource, index })
                  )
                ))}

              <div className="add-new-row" onClick={this.addNewHourlyResource}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Hourly Resource</span>
              </div>
            </div>
          </div>
          {serCostObj && serCostObj.contractors && (
            <div className="contractors">
              <div className="sub-heading col-md-12">
                <div className="text">
                  {isTMDoc
                    ? "FEE BASED SERVICE OR PRODUCT"
                    : "FEE BASED CONTRACTORS"}
                </div>
              </div>
              {serCostObj.contractors.map((contractor, index) => {
                return (
                  <div className="contractor-row col-md-12" key={index}>
                    <EditButton
                      title="Edit Contractor Description"
                      onClick={() =>
                        this.handleClickEditDescription(
                          contractor,
                          index,
                          "Contractor"
                        )
                      }
                    />
                    <Input
                      field={{
                        value: contractor.vendor_id,
                        label: "Contractor Name",
                        type: "PICKLIST",
                        options: this.props.vendorOptions,
                        isRequired: false,
                      }}
                      width={3}
                      name={"vendor_id"}
                      placeholder="Select Contractor"
                      loading={this.props.isFetchingVendors}
                      onChange={(e) => this.handleChangeContractors(e, index)}
                      className="select-type"
                      error={
                        contractor.errorName &&
                        contractor.errorName.errorState ===
                          "error"
                          ? contractor.errorName
                          : {
                              errorState: "warning",
                              errorMessage: contractor.description
                                ? contractor.description
                                : "",
                            }
                      }
                    />
                    <Input
                      field={{
                        value: contractor.partner_cost,
                        label: "Partner Cost(In $)",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={2}
                      name={"partner_cost"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      onChange={(e) => this.handleChangeContractors(e, index)}
                      className="select-type"
                      error={contractor.errorRate}
                    />
                    <Input
                      field={{
                        value: contractor.margin_percentage,
                        label: "GP %",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={1}
                      minimumValue={"0"}
                      maximumValue={"100"}
                      name={"margin_percentage"}
                      placeholder="Enter %"
                      onChange={(e) => this.handleChangeContractors(e, index)}
                      className="select-type"
                    />
                    <Input
                      field={{
                        label: "Customer Cost(In $)",
                        value: formatter.format(
                          Number(getCustomerCost(contractor))
                        ),
                        type: "TEXT",
                        isRequired: false,
                      }}
                      width={2}
                      name={"margin_percentage"}
                      placeholder=" "
                      disabled={true}
                      className="disabled-calculations"
                      onChange={(e) => null}
                    />
                    <Input
                      field={{
                        label: "GP $",
                        value: formatter.format(
                          Number(getCustomerCost(contractor)) -
                            contractor.partner_cost
                        ),
                        type: "TEXT",
                        isRequired: false,
                      }}
                      width={2}
                      name="gross_profit"
                      disabled={true}
                      className="disabled-calculations"
                      onChange={(e) => null}
                    />
                    <Checkbox
                      isChecked={contractor.type === "Product"}
                      name="type"
                      onChange={(e) =>
                        this.handleChangeContractorsProduct(e, index)
                      }
                      className="product-type"
                    >
                      Is Product ?
                    </Checkbox>
                    {serCostObj.contractors.length > 1 && (
                      <SmallConfirmationBox
                        text="contractor"
                        onClickOk={() => this.removeContractor(index)}
                        showButton={true}
                      />
                    )}
                  </div>
                );
              })}
              <div className="add-new-row" onClick={this.addNewContractor}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Contractor</span>
              </div>
            </div>
          )}
          {serCostObj && serCostObj.travels && (
            <div className="travelscontractors">
              <div className="sub-heading col-md-12">
                <div className="text">
                  {isTMDoc ? "TRAVEL ESTIMATE" : "TRAVEL"}
                </div>
                {!isTMDoc && (
                  <Checkbox
                    isChecked={serCostObj.preview_travel_costs}
                    name="preview_travel_costs"
                    onChange={this.handleChangePreviewTravels}
                    className="order-checkbox"
                  >
                    Show Travel Cost
                  </Checkbox>
                )}
              </div>
              {serCostObj.travels.map((travel, index) => {
                return (
                  <div className="contractor-row col-md-12" key={index}>
                    <Input
                      field={{
                        value: travel.description,
                        label: "Description",
                        type: "TEXT",
                        isRequired: false,
                      }}
                      width={4}
                      name={"description"}
                      placeholder="Enter description"
                      onChange={(e) => this.handleChangeTravels(e, index)}
                      error={travel.errorDescription}
                    />
                    <Input
                      field={{
                        value: travel.cost,
                        label: "Cost (in $)",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={3}
                      name={"cost"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      onChange={(e) => this.handleChangeTravels(e, index)}
                      error={travel.errorCost}
                    />
                    {serCostObj.travels.length > 1 && (
                      <SmallConfirmationBox
                        text="travel"
                        showButton={true}
                        onClickOk={() => this.removeTravel(index)}
                      />
                    )}
                  </div>
                );
              })}
              <div className="add-new-row" onClick={this.addNewTravel}>
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">Add Travel Estimate</span>
              </div>
            </div>
          )}

          <div className="calculations">
            <div className="sub-heading col-md-12">
              <div className="text">
                <span>
                  SERVICE COST ({this.state.sow.doc_type.toUpperCase()})
                </span>
              </div>
            </div>
            <div className="calculations-table risk-hour-table">
              <div className="calculations-table-header">
                <div className="calculations-header-title">Resource</div>
                <div className="calculations-header-title">Hours</div>
                <div className="calculations-header-title">Risk Hours</div>
                <div className="calculations-header-title">Total Hours</div>
                <div className="calculations-header-title">Customer Rate</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Engineer</div>
                  <div className="calculations-table-col">
                    {serCostObj.engineering_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.engRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.engineering_hours + serviceCost.engRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serCostObj.engineering_hourly_rate)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    After Hours Engineer
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.after_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.ahRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.after_hours + serviceCost.ahRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serCostObj.after_hours_rate)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Integration Technician
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.integration_technician_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.itRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.integration_technician_hours +
                      serviceCost.itRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serCostObj.integration_technician_hourly_rate
                    )}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Project Management
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.project_management_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.pmRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.project_management_hours +
                      serviceCost.pmRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serCostObj.project_management_hourly_rate
                    )}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Hourly Contract Labor
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborTotalHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborTotalHours +
                      serviceCost.hourlyLaborRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborTotalRate)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Subtotal</div>
                  <div className="calculations-table-col">
                    {serviceCost.totalHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.totalRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.totalHours + serviceCost.totalRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serCostObj.engineering_hourly_rate +
                        serCostObj.after_hours_rate +
                        serCostObj.integration_technician_hourly_rate +
                        serCostObj.project_management_hourly_rate +
                        serviceCost.hourlyLaborTotalRate
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="calculations-table">
              <div className="calculations-table-header">
                <div className="calculations-header-title">Description</div>
                <div className="calculations-header-title">Revenue</div>
                <div className="calculations-header-title">Internal Cost</div>
                <div className="calculations-header-title">GP $</div>
                <div className="calculations-header-title">GP %</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Engineering Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.engineeringHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Engineering After Hours Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.afterHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Integration Technician Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serviceCost.integrationTechnicianCustomerCost
                    )}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serviceCost.integrationTechnicianInternalCost
                    )}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.integrationTechnicianMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.integrationTechnicianMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Project Management Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.pmHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Hourly Contract Labor
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborMarginPercent + "%"}
                  </div>
                </div>
              </div>
            </div>
            <div className="calculations-table">
              <h4>Project Summary</h4>
              <div className="calculations-table-header">
                <div className="calculations-header-title">Description</div>
                <div className="calculations-header-title">Revenue</div>
                <div className="calculations-header-title">Internal Cost</div>
                <div className="calculations-header-title">GP $</div>
                <div className="calculations-header-title">GP %</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    {isTMDoc
                      ? "Estimated Professional Services"
                      : "Professional Services"}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.proSerMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.proSerMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Risk Budget</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.riskBudgetCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.riskBudgetInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.riskBudgetMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.riskBudgetMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    {isTMDoc ? "Fee Based Service or Product" : "Contractors"}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.contractorCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.contractorInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.contractorMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.contractorMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    {isTMDoc ? "Estimated Travel Budget" : "Travel Budget"}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.travelCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.travelInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.travelMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.travelMarginPercent + "%"}
                  </div>
                </div>
                {!isTMDoc && (
                  <div className="calculations-table-row">
                    <div className="calculations-table-col">Rounding</div>
                    <div className="calculations-table-col">
                      {formatter.format(
                        serCostObj.total_revenue_rounding_offset
                      )}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(0)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(
                        serCostObj.total_revenue_rounding_offset
                      )}
                    </div>
                    <div className="calculations-table-col">100%</div>
                  </div>
                )}
              </div>
            </div>
            <div className="calculations-table project-total-section">
              <h4>Project Total</h4>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Internal Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalInternalCost)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Total Margin</div>
                  <div className="calculations-table-col">
                    {formatter.format(totalMargin)}
                    <span className="total-margin-percent">
                      @ {totalMarginPercent}% GP
                    </span>
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Customer Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(totalCustomerCost)}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="notes col-md-12">
            <div className="sub-heading-notes col-md-12">
              <div className="text">NOTES</div>
            </div>
            <div className="col-md-12">
              <QuillEditorAcela
                scrollingContainer=".add-sow"
                onChange={(e) => this.handleChangeNoteMarkDown(e)}
                value={serCostObj.notes}
                wrapperClass={`${"ql-sow-notes"}`}
              />
            </div>
          </div>
          {this.state.sow.id !== 0 && (
            <div className="version-section col-md-10">
              <Input
                field={{
                  value: this.state.sow.version_description,
                  label: "Version Description",
                  type: "TEXTAREA",
                  isRequired: false,
                }}
                width={12}
                name={"version_description"}
                placeholder=" "
                onChange={(e) => this.handleChangeSimpleFields(e)}
                className="Enter version description"
              />
            </div>
          )}
        </div>
      </div>
    );
  };

  getProjectManagementRecommendedHours = (): number => {
    let hours = 0;
    if (
      this.state.sow.json_config.service_cost &&
      this.state.sow.json_config.service_cost.engineering_hours_breakup &&
      this.state.sow.json_config.service_cost.engineering_hours_breakup.phases
    ) {
      this.state.sow.json_config.service_cost.engineering_hours_breakup.phases.forEach(
        (el) => {
          if (el.resource_name !== "Project Management") {
            hours += el.hours;
          }
        }
      );
    }
    return parseFloat(getRecommendedHours(hours));
  };

  onChecboxChangeUpdateCheckbox = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    this.setState((prevState) => ({
      sow: { ...prevState.sow, [event.target.name]: event.target.checked },
      unsaved: true,
    }));
  };

  handleChangeOrderingCheckbox = (e: React.ChangeEvent<HTMLInputElement>) => {
    const stateKey = e.target.name as keyof IAddSowState;
    const updatedState: Partial<IAddSowState> = {
      [stateKey]: e.target.checked,
      unsaved: true,
    };
    this.setState(updatedState as Pick<IAddSowState, keyof IAddSowState>);
  };

  handleChangePreviewTravels = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    sow.json_config.service_cost.preview_travel_costs = e.target.checked;
    this.setState({ sow });
  };

  handleClickEditDescription = (
    resource: IHourlyResource | IContractor,
    index: number,
    type: "Hourly Resource" | "Contractor"
  ) => {
    let mapping: IVendorAliasMapping;
    if (type === "Hourly Resource")
      mapping = {
        index,
        resource_type: type,
        vendor_crm_id: (resource as IHourlyResource).resource_id,
        vendor_name: (resource as IHourlyResource).resource_name,
        resource_description: (resource as IHourlyResource)
          .resource_description,
      };
    else
      mapping = {
        index,
        resource_type: type,
        vendor_crm_id: (resource as IContractor).vendor_id,
        vendor_name: (resource as IContractor).name,
        resource_description: (resource as IContractor).description,
      };

    this.setState({
      currentVendorMapping: mapping,
      showVendorMappingModal: true,
    });
  };

  handleChangeStatic = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    // Hours should be integer
    const value = this.getValues(
      e,
      [
        "hours",
        "after_hours",
        "integration_technician_hours",
        "engineering_hours",
        "project_management_hours",
      ].includes(e.target.name)
    );

    // For hourly resource, index will be passed
    if (!isNil(idx)) {
      let updatedResource: IHourlyResource;
      if (e.target.name === "resource_id") {
        const resource_id = value as number;
        const resource_name = this.props.vendorOptions.find(
          (el) => el.value === value
        ).label as string;
        let resource_description: string = null;
        // For newly set resource, set description if any is configured in Vendor Description mapping settings
        if (this.state.vendorDescriptionMapping.has(resource_id))
          resource_description = this.state.vendorDescriptionMapping.get(
            resource_id
          ).resource_description;

        const resourceIdentifier = resource_description
          ? resource_description
          : resource_name;
        // If the hourly resource is not overridden, then set the calculated value
        // of resource, else just update the hourly resource name
        if (
          !newState.sow.json_config.service_cost.hourly_resources[idx].override
        ) {
          const hours = this.state.phasesGroupedByResources[resourceIdentifier]
            ? this.state.phasesGroupedByResources[resourceIdentifier]
            : 0;
          updatedResource = getCalculatedHourlyResource({
            ...newState.sow.json_config.service_cost.hourly_resources[idx],
            hours,
            resource_id,
            resource_name,
            resource_description,
          });
        } else {
          updatedResource = {
            ...newState.sow.json_config.service_cost.hourly_resources[idx],
            resource_id,
            resource_name,
            resource_description,
          };
        }
      } else {
        updatedResource = getCalculatedHourlyResource({
          ...newState.sow.json_config.service_cost.hourly_resources[idx],
          [e.target.name]: value,
        });
      }
      newState.sow.json_config.service_cost.hourly_resources[
        idx
      ] = updatedResource;
    } else newState.sow.json_config.service_cost[e.target.name] = value;

    (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
      newState.sow.json_config.service_cost
    );
    newState.sow.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.sow.json_config.service_cost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    this.debouncedCalculations();
  };

  showOverrideConfirmationPopup = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const isOverrided = e.target.checked;
    this.setState(
      {
        showOverrideConfirmation: isOverrided,
        ehtOverrideInfo: {
          name: e.target.name,
          checked: e.target.checked,
          idx,
        },
      },
      () => {
        if (!isOverrided) this.handleChangeStaticOverride();
      }
    );
  };

  closeOverrideConfirmationPopup = () => {
    this.setState({
      showOverrideConfirmation: false,
      ehtOverrideInfo: {
        name: "",
        checked: false,
        idx: null,
      },
    });
  };

  handleChangeStaticOverride = () => {
    const newState = cloneDeep(this.state);
    const info = this.state.ehtOverrideInfo;
    const serviceCost = newState.sow.json_config.service_cost;
    const groupedPhases = newState.phasesGroupedByResources;
    if (!isNil(info.idx)) {
      serviceCost.hourly_resources[info.idx].override = info.checked;
      let currentResource = serviceCost.hourly_resources[info.idx];
      const resourceIdentifier = getResourceIdentifier(currentResource);
      if (!info.checked) {
        let calculatedHours = groupedPhases[resourceIdentifier]
          ? groupedPhases[resourceIdentifier]
          : 0;
        serviceCost.hourly_resources[info.idx] = getCalculatedHourlyResource({
          ...currentResource,
          hours: calculatedHours,
          override: info.checked,
        });
      } else {
        groupedPhases[resourceIdentifier] = 0;
        serviceCost.engineering_hours_breakup.phases = serviceCost.engineering_hours_breakup.phases.filter(
          (phase) => phase.resource_name !== resourceIdentifier
        );
      }
    } else {
      serviceCost[info.name] = info.checked;

      const currentMap = OverrideMap[info.name];
      if (!info.checked) {
        if (info.name === "project_management_hours_override")
          serviceCost.project_management_hours = Math.round(
            (groupedPhases["Project Management"]
              ? groupedPhases["Project Management"]
              : 0) + this.getProjectManagementRecommendedHours()
          );
        else
          serviceCost[currentMap.hoursVal] = groupedPhases[
            currentMap.resourceVal
          ]
            ? groupedPhases[currentMap.resourceVal]
            : 0;
      } else {
        groupedPhases[currentMap.resourceVal] = 0;
        serviceCost.engineering_hours_breakup.phases = serviceCost.engineering_hours_breakup.phases.filter(
          (phase) => phase.resource_name !== currentMap.resourceVal
        );
      }
    }
    serviceCost.total_hours = this.calculateTotalHours(serviceCost);
    (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
      serviceCost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState, () => {
      this.debouncedCalculations();
      this.closeOverrideConfirmationPopup();
    });
  };

  handleChangeStaticHide = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    if (!isNil(idx)) {
      newState.sow.json_config.service_cost.hourly_resources[idx].is_hidden =
        e.target.checked;
    } else {
      newState.sow.json_config.service_cost[e.target.name] = e.target.checked;
    }
    (newState.unsaved as boolean) = true;
    (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
      newState.sow.json_config.service_cost
    );
    this.setState(newState);
  };

  handleChangeStaging = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    sow.json_config.service_cost.staging = e.target.checked;
    this.setState({
      sow,
      unsaved: true,
    });
  };

  assignGeneratedTerms = (jsonConfig: IJSONConfig): IJSONConfig => {
    let terms = "";
    if (
      jsonConfig.service_cost.hide_travel_expenses &&
      this.props.docSetting.hidden_bill_travel_text
    )
      terms = this.props.docSetting.hidden_bill_travel_text;
    else if (
      jsonConfig.service_cost.bill_travel_expenses &&
      this.props.docSetting.include_bill_travel_text
    )
      terms = this.props.docSetting.include_bill_travel_text;
    else if (
      !jsonConfig.service_cost.bill_travel_expenses &&
      this.props.docSetting.exclude_bill_travel_text
    )
      terms = this.props.docSetting.exclude_bill_travel_text;
    jsonConfig.terms_fixed_fee[
      "generated_terms"
    ].value = terms;
    return jsonConfig;
  };

  getMilestonePreview = (milestones: IMilestone[]): JSX.Element => {
    let milestoneItems = "";
    milestones
      .filter(
        (el) => !isNil(el.milestone_percent) && el.terms && el.terms.trim()
      )
      .forEach((milestone) => {
        milestoneItems += `<li class=ql-indent-1">${milestone.milestone_percent}% ${milestone.terms}</li>`;
      });
    return milestoneItems ? (
      <div className="milestone-preview-section">
        <h4>Milestone Preview</h4>
        <div className="milestone-preview">
          <div>Milestones</div>
          <ul
            dangerouslySetInnerHTML={{
              __html: milestoneItems,
            }}
          />
        </div>
      </div>
    ) : null;
  };

  handleChangeBillExpenses = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    if (e.target.value === "Hide") {
      sow.json_config.service_cost.hide_travel_expenses = true;
    } else {
      sow.json_config.service_cost.bill_travel_expenses =
        e.target.value === "true" ? true : false;
      sow.json_config.service_cost.hide_travel_expenses = false;
    }
    // Reset the generated terms text
    sow.json_config = this.assignGeneratedTerms(sow.json_config);
    this.setState({
      sow,
      unsaved: true,
    });
  };

  handleChangeSiteCutOversOverride = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.engineering_hours_breakup[
      e.target.name
    ] = e.target.checked;
    if (e.target.name === "total_sites_override" && e.target.checked) {
      newState.sow.json_config.service_cost.engineering_hours_breakup.total_sites = 1;
    }
    if (e.target.name === "total_cutovers_override" && e.target.checked) {
      newState.sow.json_config.service_cost.engineering_hours_breakup.total_cutovers = 1;
    }

    (newState.unsaved as boolean) = true;
    this.setState(newState);
    this.debouncedCalculations();
  };

  handleChangeStaticCalculations = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.engineering_hours_breakup[
      e.target.name
    ] = this.getValues(e);
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    if (e.target.type === "number") this.debouncedCalculations();
  };

  setPhasesGroupedByResources = () => {
    if (
      this.state.sow.json_config.service_cost.engineering_hours_breakup &&
      this.state.sow.json_config.service_cost.engineering_hours_breakup.phases
    ) {
      const phasesGroupedByResources: object = this.state.sow.json_config.service_cost.engineering_hours_breakup.phases.reduce(
        (sumObj, el) => {
          let resource_name = el.resource_name;
          if (resource_name in sumObj) {
            sumObj[resource_name] += el.hours;
          } else sumObj[resource_name] = el.hours;
          return sumObj;
        },
        {}
      );
      this.setState({ phasesGroupedByResources });
    }
  };

  changeEstimatedNonOverrideHours = debounce(() => {
    // Group resources from phases section and then calculate total hours for each resource
    // Associate the calculated hours to each resource in estimated hours total (if override is not enabled)
    // this is to be done
    const newState = cloneDeep(this.state);
    const phasesGroupedByResources: object = newState.sow.json_config.service_cost.engineering_hours_breakup.phases.reduce(
      (sumObj, el) => {
        let resource_name = el.resource_name;
        if (resource_name in sumObj) {
          sumObj[resource_name] += el.hours;
        } else sumObj[resource_name] = el.hours;
        return sumObj;
      },
      {}
    );
    (newState.phasesGroupedByResources as object) = phasesGroupedByResources;
    let totalEngineeringHours = phasesGroupedByResources["Engineer"]
      ? phasesGroupedByResources["Engineer"]
      : 0;
    let totalEngineeringAfterHours = phasesGroupedByResources[
      "After Hours Engineer"
    ]
      ? phasesGroupedByResources["After Hours Engineer"]
      : 0;
    let totalIntegrationTechnicianHours = phasesGroupedByResources[
      "Integration Technician"
    ]
      ? phasesGroupedByResources["Integration Technician"]
      : 0;
    let totalProjectManagementHours = Math.round(
      (phasesGroupedByResources["Project Management"]
        ? phasesGroupedByResources["Project Management"]
        : 0) + this.getProjectManagementRecommendedHours()
    );

    if (!newState.sow.json_config.service_cost.engineering_hours_override)
      newState.sow.json_config.service_cost.engineering_hours = totalEngineeringHours;
    if (!newState.sow.json_config.service_cost.after_hours_override)
      newState.sow.json_config.service_cost.after_hours = totalEngineeringAfterHours;
    if (
      !newState.sow.json_config.service_cost
        .integration_technician_hours_override
    )
      newState.sow.json_config.service_cost.integration_technician_hours = totalIntegrationTechnicianHours;
    if (
      !newState.sow.json_config.service_cost.project_management_hours_override
    )
      newState.sow.json_config.service_cost.project_management_hours = totalProjectManagementHours;
    if (newState.sow.json_config.service_cost.hourly_resources)
      newState.sow.json_config.service_cost.hourly_resources.forEach((el) => {
        const resourceIdentifier = getResourceIdentifier(el);
        let phaseHours = phasesGroupedByResources[resourceIdentifier]
          ? phasesGroupedByResources[resourceIdentifier]
          : 0;
        if (!el.override) {
          el.hours = phaseHours;
          const newPhase = getCalculatedHourlyResource(el);
          el.customer_cost = newPhase.customer_cost;
          el.internal_cost = newPhase.internal_cost;
          el.margin = newPhase.margin;
          el.margin_percentage = newPhase.margin_percentage;
        }
      });
    newState.sow.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.sow.json_config.service_cost
    );

    this.setState(newState, () => this.doSowCalculations());
  }, 500);

  handleChangeStaticCalculationsPhases = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    const currentPhase =
      newState.sow.json_config.service_cost.engineering_hours_breakup.phases[
        index
      ];
    if (e.target.name === "resource") {
      // In case if we select a resource, set the resource to resource id (vendor_crm_id or [-1, -4])
      currentPhase.resource = this.state.phaseResources.find(
        (el) => el.value === e.target.value
      ).data;
      // phase.resource_name will contain the local(SoW specific)/global(Vendor Desc Mapping) for Hourly Resources
      // or Engineer, After Hours Engineer, etc.
      currentPhase.resource_name = e.target.value;
    } else currentPhase[e.target.name] = this.getValues(e, true);
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.changeEstimatedNonOverrideHours);
  };

  addNewCalculationsPhases = () => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.engineering_hours_breakup &&
      newState.sow.json_config.service_cost.engineering_hours_breakup.phases.push(
        {
          id: random(1, 100000),
          name: "",
          hours: 0,
          override: false,
          after_hours: false,
          resource_name: "",
          resource: null,
        }
      );
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  deleteCalculationsPhases = (index: number) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.engineering_hours_breakup.phases.splice(
      index,
      1
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.changeEstimatedNonOverrideHours);
  };

  handleChangeMilestone = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const key = e.target.name;
    const newState = cloneDeep(this.state);
    const currentMilestone =
      newState.sow.json_config.service_cost.milestones[index];
    currentMilestone[key] = this.getValues(e, true);
    if (key === "milestone_percent" || key === "terms")
      this.debouncedTermUpdate(newState.sow.json_config);

    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addNewMilestone = () => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.milestones.push({
      id: random(1, 100000),
      name: "",
      terms: "",
      milestone_percent: 0,
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  deleteMilestone = (index: number) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.milestones.splice(index, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.changeEstimatedNonOverrideHours);
  };

  handleChangeNoteMarkDown = (html: string) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.notes = html;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  setDefaultStatic = () => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.engineering_hourly_rate = this.props.docSetting.engineering_hourly_rate;
    newState.sow.json_config.service_cost.after_hours_rate = this.props.docSetting.after_hours_rate;
    newState.sow.json_config.service_cost.integration_technician_hourly_rate = this.props.docSetting.integration_technician_hourly_rate;
    newState.sow.json_config.service_cost.project_management_hourly_rate = this.props.docSetting.project_management_hourly_rate;
    newState.sow.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.sow.json_config.service_cost
    );
    this.setState(newState, () => this.doSowCalculations());
  };

  getValues = (
    e: React.ChangeEvent<HTMLInputElement>,
    integer: boolean = false
  ) => {
    const value =
      e.target.type === "number"
        ? round(Number(e.target.value), integer ? 0 : 2)
        : e.target.value;

    return value;
  };

  handleChangeContractors = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    (newState.unsaved as boolean) = true;
    const currentContractor =
      newState.sow.json_config.service_cost.contractors[index];

    currentContractor[e.target.name] = this.getValues(e);

    if (e.target.name === "vendor_id") {
      const vendorId = Number(e.target.value);
      if (vendorId) {
        currentContractor.name = this.props.vendorOptions.find(
          (el) => el.value == vendorId
        ).label as string;
        currentContractor.description = this.state.vendorDescriptionMapping.has(
          vendorId
        )
          ? this.state.vendorDescriptionMapping.get(vendorId)
              .resource_description
          : "";
      } else {
        currentContractor.name = "";
        currentContractor.description = "";
      }
    }
    if (e.target.type === "number")
      currentContractor.customer_cost = getCustomerCost(currentContractor);

    this.setState(newState, () => {
      if (e.target.type === "number") this.debouncedCalculations();
    });
  };

  handleChangeContractorsProduct = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.contractors[index].type = e.target
      .checked
      ? "Product"
      : "Service";
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addNewContractor = () => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.contractors.push({
      ...AddSow.EmptyContractor,
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeContractor = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.contractors.splice(idx, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doSowCalculations);
  };

  addNewTravel = () => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.travels.push({
      cost: 0,
      description: "",
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeTravel = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.travels.splice(idx, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doSowCalculations);
  };

  addNewHourlyResource = () => {
    const newState = cloneDeep(this.state);
    if (!newState.sow.json_config.service_cost.hourly_resources)
      newState.sow.json_config.service_cost.hourly_resources = [];
    newState.sow.json_config.service_cost.hourly_resources.push({
      ...AddSow.EmptyHourlyResource,
      id: random(1, 100000),
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeHourlyResource = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.hourly_resources.splice(idx, 1);
    newState.sow.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.sow.json_config.service_cost
    );
    (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
      newState.sow.json_config.service_cost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doSowCalculations);
  };

  handleChangeTravels = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.service_cost.travels[index][
      e.target.name
    ] = this.getValues(e);
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    if (e.target.type === "number") this.debouncedCalculations();
  };

  handleChangeRiskVariable = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newRiskValue: number = Number(e.target.value);
    const sow = cloneDeep(this.state.sow);
    const error = cloneDeep(this.state.error);
    sow.json_config.service_cost.default_ps_risk = newRiskValue;
    error.default_ps_risk =
      newRiskValue < this.props.docSetting.risk_low_watermark * 100
        ? {
            errorState: "error",
            errorMessage: `Risk variable should not go below ${this.props
              .docSetting.risk_low_watermark * 100}% (Risk Low Watermark)`,
          }
        : commonFunctions.getErrorState();
    this.setState({ sow, error }, this.doSowCalculations);
  };

  handleChangeRemoveRisk = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    sow.json_config.service_cost.default_ps_risk = !e.target.checked
      ? this.props.docSetting.default_ps_risk * 100
      : 0;
    this.setState(
      {
        sow,
        riskVarOverride: false,
        riskVarRemove: e.target.checked,
      },
      this.doSowCalculations
    );
  };

  handleChangeRiskVarOverride = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    if (!e.target.checked) {
      sow.json_config.service_cost.default_ps_risk =
        this.props.docSetting.default_ps_risk * 100;
    }
    this.setState(
      {
        sow,
        riskVarOverride: e.target.checked,
      },
      this.doSowCalculations
    );
  };

  checkValidaBoards = () => {
    const error = this.getEmptyState().error;

    const sow = this.state.sow.json_config;
    let isValid = true;
    const isCollapsed = sow
      ? Object.keys(sow).reduce((prev, cur) => ({ ...prev, [cur]: false }), {})
      : {};
    const errorsLabel = {};
    const errorsValue = {};

    const query = new URLSearchParams(this.props.location.search);
    const cloned = query.get("cloned");
    if (!this.state.sow.name || this.state.sow.name.trim().length === 0) {
      error.name = commonFunctions.getErrorState("Enter a valid sow name");
      isValid = false;
    } else if (this.state.sow.name.length > 300) {
      error.name = commonFunctions.getErrorState(
        "name should be less than 300 chars."
      );
      isValid = false;
    }

    if (cloned !== "true") {
      if (this.state.sow.id === 0 && !this.state.sow.template) {
        error.template = commonFunctions.getErrorState(
          "Please select template"
        );
        isValid = false;
      }
      if (!this.state.sow.doc_type) {
        error.doc_type = commonFunctions.getErrorState("Please select type");
        isValid = false;
      }
      if (!this.state.sow.category) {
        error.category = commonFunctions.getErrorState(
          "Please select category"
        );
        isValid = false;
      }
    }

    if (!this.state.sow.user) {
      error.user = commonFunctions.getErrorState("Please select user");
      isValid = false;
    }
    if (!this.state.sow.customer) {
      error.customer = commonFunctions.getErrorState("Please select customer");
      isValid = false;
    }
    if (!this.state.sow.doc_type) {
      error.doc_type = commonFunctions.getErrorState("Please select type ");
      isValid = false;
    }

    if (!this.state.sow.quote_id) {
      error.quote_id = commonFunctions.getErrorState(
        "Please select opportunity"
      );
      isValid = false;
    }
    if (sow) {
      sow.service_cost.milestones.forEach((milestone) => {
        milestone.errorName = commonFunctions.getErrorState();
        milestone.errorMargin = commonFunctions.getErrorState();
        milestone.errorTerms = commonFunctions.getErrorState();
      });

      sow.service_cost.milestones.forEach((milestone) => {
        if (!milestone.name || !milestone.name.trim()) {
          isCollapsed["milestones"] = true;
          milestone.errorName = commonFunctions.getErrorState(
            "Please enter a name"
          );
          isValid = false;
        }
        if (!milestone.milestone_percent) {
          isCollapsed["milestones"] = true;
          milestone.errorMargin = commonFunctions.getErrorState(
            "+ve No. Required"
          );
          isValid = false;
        }
        if (milestone.milestone_percent > 100) {
          isCollapsed["milestones"] = true;
          milestone.errorMargin = commonFunctions.getErrorState(
            "Between 0-100"
          );
          isValid = false;
        }
        if (!milestone.terms || !milestone.terms.trim()) {
          isCollapsed["milestones"] = true;
          milestone.errorTerms = commonFunctions.getErrorState(
            "Please enter terms"
          );
          isValid = false;
        }
      });

      let milestonePercentTotal = commonFunctions.arrSum(
        sow.service_cost.milestones.map((el) => el.milestone_percent)
      );
      if (
        this.state.sow.doc_type === "Fixed Fee" &&
        milestonePercentTotal !== 100
      ) {
        isCollapsed["milestones"] = true;
        error.milestone_percent = commonFunctions.getErrorState(
          "Milestone % total should be equal to 100%"
        );
        isValid = false;
      }

      sow.service_cost.contractors.forEach((contractor) => {
        contractor.errorName = commonFunctions.getErrorState();
        contractor.errorRate = commonFunctions.getErrorState();
      });

      sow.service_cost.contractors.forEach((contractor) => {
        if (
          sow.service_cost.contractors.length > 1 ||
          contractor.vendor_id ||
          contractor.partner_cost > 0
        ) {
          if (!contractor.vendor_id) {
            contractor.errorName = commonFunctions.getErrorState(
              "Please select a contractor"
            );
            isValid = false;
          }
          if (contractor.partner_cost === 0) {
            contractor.errorRate = commonFunctions.getErrorState(
              "Positive No. Required"
            );
            isValid = false;
          }
        }
      });

      sow.service_cost.travels &&
        sow.service_cost.travels.map((travel) => {
          travel.errorDescription = commonFunctions.getErrorState();
          travel.errorCost = commonFunctions.getErrorState();
        });

      sow.service_cost.travels &&
        sow.service_cost.travels.map((travel) => {
          if (
            sow.service_cost.travels.length > 1 ||
            travel.description.trim() ||
            travel.cost > 0
          ) {
            if (travel.description.trim() === "") {
              travel.errorDescription = commonFunctions.getErrorState(
                "Please enter description"
              );
              isValid = false;
            }
            if (travel.cost === 0) {
              travel.errorCost = commonFunctions.getErrorState(
                "Please enter cost"
              );
              isValid = false;
            }
          }
        });

      if (
        sow.service_cost.engineering_hours_breakup &&
        sow.service_cost.engineering_hours_breakup.phases
      ) {
        sow.service_cost.engineering_hours_breakup.phases.forEach(
          (resource) => {
            resource.errorName = commonFunctions.getErrorState();
            resource.errorResource = commonFunctions.getErrorState();
          }
        );

        sow.service_cost.engineering_hours_breakup.phases.forEach(
          (resource) => {
            if (!resource.name.trim()) {
              resource.errorName = commonFunctions.getErrorState(
                "Please enter phase name"
              );
              isValid = false;
            }
            if (!resource.resource_name) {
              resource.errorResource = commonFunctions.getErrorState(
                "Please select a resource"
              );
              isValid = false;
            }
          }
        );
      }

      if (sow.service_cost.hourly_resources) {
        sow.service_cost.hourly_resources.forEach((resource) => {
          resource.errorName = commonFunctions.getErrorState();
          resource.errorRate = commonFunctions.getErrorState();
          resource.errorCost = commonFunctions.getErrorState();
        });

        let uniqueHourlyResources: Set<string> = new Set();
        sow.service_cost.hourly_resources.forEach((resource) => {
          const resourceIdentifier = getResourceIdentifier(resource);
          if (!resource.resource_id) {
            resource.errorName = commonFunctions.getErrorState(
              "Please select resource"
            );
            isValid = false;
          }
          if (uniqueHourlyResources.has(resourceIdentifier)) {
            resource.errorName = commonFunctions.getErrorState(
              "Duplicate resource"
            );
            isValid = false;
          }
          if (resource.hourly_rate === 0) {
            resource.errorRate = commonFunctions.getErrorState("Required");
            isValid = false;
          }
          if (resource.hourly_cost === 0) {
            resource.errorCost = commonFunctions.getErrorState("Required");
            isValid = false;
          }
          if (resourceIdentifier) uniqueHourlyResources.add(resourceIdentifier);
        });
      }

      if (
        !this.state.riskVarRemove &&
        sow.service_cost.default_ps_risk <
          this.props.docSetting.risk_low_watermark * 100
      ) {
        error.default_ps_risk = commonFunctions.getErrorState(
          `Risk variable should not go below ${this.props.docSetting
            .risk_low_watermark * 100}% (Risk Low Watermark)`
        );
        isValid = false;
      }

      let count = 1;

      Object.keys(sow)
        .sort((a, b) => sow[a].ordering - sow[b].ordering)
        .filter(
          (field) =>
            !sow[field].hide_in_sow &&
            sow[field].visible_in &&
            sow[field].visible_in.includes(this.state.sow.doc_type)
        )
        .map((key, index) => {
          const boardContainerData = sow[key];
          {
            Object.keys(boardContainerData)
              .sort(
                (a, b) =>
                  ((sow[key][a] && sow[key][a].ordering) || 0) -
                  ((sow[key][b] && sow[key][b].ordering) || 0)
              )
              .map((k) => {
                if (
                  k !== "section_label" &&
                  k !== "ordering" &&
                  k !== "visible_in" &&
                  k !== "default_hidden" &&
                  k !== "hide_in_sow"
                ) {
                  const hasOptions =
                    typeof boardContainerData[k] === "object" &&
                    Object.keys(boardContainerData[k]).findIndex(
                      (v) => v === "options"
                    ) !== -1;
                  if (
                    boardContainerData[k].type !== "TEXTBOX" &&
                    boardContainerData[k].type !== "MARKDOWN"
                  ) {
                    if (hasOptions) {
                      if (boardContainerData[k].is_required) {
                        if (sow[key] && sow[key][k]) {
                          if (
                            !sow[key][k].options.reduce(
                              (prev: boolean, cur) => prev || cur.active,
                              false
                            )
                          ) {
                            isValid = false;
                            isCollapsed[key] = true;
                            errorsValue[count] = {
                              errorState: "error",
                              errorMessage: `Required`,
                            };
                          }
                        } else {
                          isValid = false;
                        }
                      }
                    } else {
                      Object.keys(boardContainerData[k]).map((l) => {
                        if (boardContainerData[k][l].is_required) {
                          if (sow[key] && sow[key][k] && sow[key][k][l]) {
                            if (sow[key][k][l].length === 0) {
                              isValid = false;
                            }
                          } else {
                            isValid = false;
                          }
                        }
                      });
                    }
                  } else if (boardContainerData[k].type === "MARKDOWN") {
                    if (boardContainerData[k].is_required) {
                      if (sow[key] && sow[key][k]) {
                        if (commonFunctions.isEditorEmpty(sow[key][k].value)) {
                          isValid = false;
                          isCollapsed[key] = true;
                          errorsValue[count] = {
                            errorState: "error",
                            errorMessage: `Required`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  } else {
                    if (boardContainerData[k].is_required) {
                      if (sow[key] && sow[key][k]) {
                        if (
                          !sow[key][k].value ||
                          sow[key][k].value.length === 0
                        ) {
                          isValid = false;
                          isCollapsed[key] = true;
                          errorsValue[count] = {
                            errorState: "error",
                            errorMessage: `Required`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  }
                  if (boardContainerData[k]) {
                    if (sow[key] && sow[key][k]) {
                      if (sow[key][k].label === "") {
                        isValid = false;
                        isCollapsed[key] = true;
                        errorsLabel[count] = {
                          errorState: "error",
                          errorMessage: `label is required`,
                        };
                      }
                    } else {
                      isValid = false;
                    }
                  }

                  if (boardContainerData[k].input_type === "FLOAT") {
                    if (boardContainerData[k].is_required) {
                      if (sow[key] && sow[key][k]) {
                        if (
                          sow[key][k].value.length > 0 &&
                          !AppValidators.isValidPositiveFloat(sow[key][k].value)
                        ) {
                          isValid = false;
                          isCollapsed[key] = true;
                          errorsValue[count] = {
                            errorState: "error",
                            errorMessage: `Please enter numeric values only`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  }
                  if (
                    boardContainerData.sections &&
                    boardContainerData.sections.length > 0
                  ) {
                    boardContainerData.sections.map((keyS, indexS) => {
                      Object.keys(keyS).map((secKey, x) => {
                        if (keyS[secKey].is_required) {
                          delete sow[key].sections[indexS][secKey].error;
                        }
                      });
                    });
                  }
                  if (
                    boardContainerData.sections &&
                    boardContainerData.sections.length > 0
                  ) {
                    boardContainerData.sections.map((keyS, indexS) => {
                      Object.keys(keyS).map((secKey, x) => {
                        if (keyS[secKey].is_required) {
                          if (keyS[secKey].value === "") {
                            isValid = false;
                            isCollapsed[key] = true;
                            (sow[key].sections[indexS][secKey]
                              .error as IFieldValidation) = {
                              errorState: "error",
                              errorMessage: `Required`,
                            };
                          }
                        }
                      });
                    });
                  }
                  count++;
                }
              });
          }
        });
    }

    this.setState((prevState) => ({
      error,
      errorsLabel,
      errorsValue,
      isValid,
      isCollapsed,
      showError: !isValid,
      sow: {
        ...prevState.sow,
        json_config: sow,
      },
    }));

    return isValid;
  };

  checkValidationForPreview = () => {
    const error = this.getEmptyState().error;

    const sow = this.state.sow.json_config;
    let isValid = true;
    const isCollapsed = sow
      ? Object.keys(sow).reduce((prev, cur) => ({ ...prev, [cur]: false }), {})
      : {};
    const errorsLabel = {};
    const errorsValue = {};

    const query = new URLSearchParams(this.props.location.search);
    const cloned = query.get("cloned");
    if (!this.state.sow.name || this.state.sow.name.trim().length === 0) {
      error.name = commonFunctions.getErrorState("Enter a valid sow name");

      isValid = false;
    } else if (this.state.sow.name.length > 300) {
      error.name = commonFunctions.getErrorState(
        "name should be less than 300 chars."
      );
      isValid = false;
    }

    if (cloned !== "true") {
      if (this.state.sow.id === 0 && !this.state.sow.template) {
        error.template = commonFunctions.getErrorState(
          "Please select template"
        );
        isValid = false;
      }
      if (!this.state.sow.doc_type) {
        error.doc_type = commonFunctions.getErrorState("Please select type");
        isValid = false;
      }
      if (!this.state.sow.category) {
        error.category = commonFunctions.getErrorState(
          "Please select category"
        );
        isValid = false;
      }
    }
    if (!this.state.sow.user) {
      error.user = commonFunctions.getErrorState("Please select user");
      isValid = false;
    }
    if (!this.state.sow.customer) {
      error.customer = commonFunctions.getErrorState("Please select customer");
      isValid = false;
    }
    if (!this.state.sow.doc_type) {
      error.doc_type = commonFunctions.getErrorState("Please select type ");
      isValid = false;
    }

    if (!this.state.sow.quote_id) {
      error.quote_id = commonFunctions.getErrorState(
        "Please select opportunity"
      );
      isValid = false;
    }

    this.setState((prevState) => ({
      error,
      errorsLabel,
      errorsValue,
      isValid,
      isCollapsed,
      showError: !isValid,
    }));

    return isValid;
  };

  generatePayloadSOW = () => {
    const json_config = this.state.sow.json_config;
    const sow = cloneDeep(this.state.sow);
    if (this.state.sow && json_config) {
      Object.keys(json_config).map((key) => {
        const boardContainerData = json_config[key];
        {
          Object.keys(boardContainerData).map((k, i) => {
            if (k !== "section_label" && k !== "ordering") {
              if (
                boardContainerData[k] &&
                boardContainerData[k].type === "MARKDOWN"
              ) {
                // convert HTML to markdown for payload
                json_config[key][
                  k
                ].value_markdown = commonFunctions.convertToMarkdown(
                  json_config[key][k].value
                );
              }
            }
          });
        }
      });
    }
    if (json_config.service_cost) {
      const serviceCost: ISoWCalculationFields = getSowCalculationFields(
        this.state.sow.json_config.service_cost,
        this.props.docSetting
      );
      sow.json_config.service_cost.customer_cost_fixed_fee =
        serviceCost.totalCustomerCost;
      sow.json_config.service_cost.internal_cost_fixed_fee =
        serviceCost.totalInternalCost;
      sow.json_config.service_cost.customer_cost_t_and_m_fee =
        serviceCost.totalCustomerCost;
      sow.json_config.service_cost.internal_cost_t_and_m_fee =
        serviceCost.totalInternalCost;
      sow.json_config.service_cost.contractors = map(
        json_config.service_cost.contractors,
        (object) => {
          return pick(object, [
            "customer_cost",
            "margin_percentage",
            "name",
            "partner_cost",
            "type",
            "vendor_id",
            "description",
          ]);
        }
      );
      sow.json_config.service_cost.travels = map(
        json_config.service_cost.travels,
        (object) => {
          return pick(object, ["cost", "description"]);
        }
      );
      sow.json_config.service_cost.hourly_resources = map(
        json_config.service_cost.hourly_resources,
        (object) => {
          return pick(object, [
            "hours",
            "override",
            "hourly_cost",
            "hourly_rate",
            "resource_id",
            "resource_name",
            "internal_cost",
            "customer_cost",
            "margin",
            "margin_percentage",
            "resource_description",
            "is_hidden",
          ]);
        }
      );
      sow.json_config.service_cost.milestones = map(
        sow.json_config.service_cost.milestones,
        (object) => {
          return pick(object, ["id", "name", "terms", "milestone_percent"]);
        }
      );
      if (
        sow.json_config.service_cost.engineering_hours_breakup &&
        sow.json_config.service_cost.engineering_hours_breakup.phases
      )
        json_config.service_cost.engineering_hours_breakup.phases = json_config.service_cost.engineering_hours_breakup.phases.map(
          (el) => {
            delete el.id;
            delete el.errorName;
            delete el.errorResource;
            return el;
          }
        );
      sow.json_config.service_cost.default_ps_risk =
        json_config.service_cost.default_ps_risk / 100;
    }
    sow.json_config.service_cost.notesMD = commonFunctions.convertToMarkdown(
      sow.json_config.service_cost.notes
    );
    return sow;
  };

  callSaveSOW = (closeDocument: boolean) => {
    if (this.checkValidaBoards()) {
      const sow = this.generatePayloadSOW();
      this.setState({ saving: true });
      if (this.state.sow.id && this.state.sow.id !== 0) {
        this.props
          .updateSOW(sow, this.state.sendEmail, this.state.sendEmailViaGraph)
          .then((a) => {
            if (a.type === EDIT_SOW_SUCCESS) {
              if (this.state.sendEmail || this.state.sendEmailViaGraph)
                this.props.addInfoMessage(
                  "Sending email to Account Manager..."
                );
              if (
                a.response.json_config.forecast_exception &&
                a.response.json_config.forecast_exception
                  .exception_in_forecast_creation
              ) {
                this.props.addWarningMessage("Forecast creation failed!");
              }
              if (closeDocument) {
                this.setState(
                  {
                    unsaved: false,
                  },
                  () => {
                    this.props.history.goBack();
                  }
                );
              } else {
                const json_config = cloneDeep(this.state.sow.json_config);
                this.setPhaseIds(json_config);
                this.setHourlyResourceIds(json_config);
                this.setState({
                  sow: {
                    ...this.state.sow,
                    json_config,
                    update_author: false,
                    update_version: false,
                    author_name: a.response.author_name,
                    updated_on: a.response.updated_on,
                    updated_by_name: a.response.updated_by_name,
                    major_version: a.response.major_version,
                    minor_version: a.response.minor_version,
                  },
                  sendEmail: false,
                  sendEmailViaGraph: false,
                  unsaved: false,
                });
              }
            }
            if (
              a.type === EDIT_SOW_FAILURE &&
              a.errorList &&
              a.errorList.data
            ) {
              this.setValidationErrors(a.errorList.data);
            }
          })
          .finally(() => this.setState({ saving: false }));
      } else {
        this.props
          .saveSOW(sow, this.state.sendEmail, this.state.sendEmailViaGraph)
          .then((a) => {
            if (a.type === CREATE_SOW_SUCCESS) {
              if (this.state.sendEmail || this.state.sendEmailViaGraph)
                this.props.addInfoMessage(
                  "Sending email to Account Manager..."
                );

              if (
                a.response.json_config.forecast_exception &&
                a.response.json_config.forecast_exception
                  .exception_in_forecast_creation
              ) {
                this.props.addWarningMessage("Forecast creation failed!");
              }
              this.setState(
                {
                  unsaved: false,
                },
                () => {
                  this.props.history.goBack();
                }
              );
            }
            if (
              a.type === CREATE_SOW_FAILURE &&
              a.errorList &&
              a.errorList.data
            ) {
              this.setValidationErrors(a.errorList.data);
            }
          })
          .finally(() => this.setState({ saving: false }));
      }
    } else if (
      this.state.orderingPhase ||
      this.state.orderingHourlyResources ||
      this.state.orderingMilestones
    ) {
      this.setState({
        orderingPhase: false,
        orderingHourlyResources: false,
        orderingMilestones: false,
      });
    }
  };

  toggleVendorModal = (show: boolean) => {
    this.setState({
      showVendorModal: show,
    });
  };

  onSubmitVendorModal = () => {
    this.props.getVendorsList();
    this.toggleVendorModal(false);
  };

  toggleCreateUserModal = () => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
  };

  closeUserModal = () => {
    this.setState((prevState) => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
    this.props.fetchAllCustomerUsers(this.state.sow.customer).then((a) => {
      if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        this.setState({ users: a.response });
      }
    });
  };

  closeVendorMappingModal = (
    save: boolean,
    vendorMapping?: IVendorAliasMapping
  ) => {
    const newState = cloneDeep(this.state);
    if (save) {
      if (vendorMapping.resource_type === "Hourly Resource") {
        // Update the resource description for hourly resource
        const prevResource = this.state.sow.json_config.service_cost
          .hourly_resources[vendorMapping.index];
        newState.sow.json_config.service_cost.hourly_resources[
          vendorMapping.index
        ] = {
          ...AddSow.EmptyHourlyResource,
          resource_id: prevResource.resource_id,
          resource_name: prevResource.resource_name,
          resource_description: vendorMapping.resource_description,
        };
        // Update phase resources listing with new resource alias/friendly name
        (newState.phaseResources as IPickListOptions[]) = this.getPhaseResources(
          newState.sow.json_config.service_cost
        );
      } else {
        // Update description for contractor
        newState.sow.json_config.service_cost.contractors[
          vendorMapping.index
        ].description = vendorMapping.resource_description;
      }
      (newState.unsaved as boolean) = true;
    }
    (newState.showVendorMappingModal as boolean) = false;
    (newState.currentVendorMapping as IVendorAliasMapping) = {
      vendor_name: "",
      resource_type: null,
      vendor_crm_id: null,
      resource_description: "",
    };

    this.setState(newState, this.doSowCalculations);
  };

  toggleCreateOpportunityModal = () => {
    this.setState((prevState) => ({
      isCreateOpportunityModal: !prevState.isCreateOpportunityModal,
    }));
  };

  createQuote = (data: any) => {
    this.setState({ isPosting: true });
    const customerId = this.state.sow.customer;
    if (customerId) {
      (data.customer_id as number) = customerId;
      (data.user_id as any) = this.state.sow.user;
      this.props
        .createQuote(customerId, data)
        .then((action) => {
          if (action.type === CREATE_QUOTES_SUCCESS) {
            this.setState({
              isCreateOpportunityModal: false,
            });

            this.props.fetchQuoteDashboardListing(customerId, true);
          } else {
            this.setState({
              isCreateOpportunityModal: true,
              errorList: action.errorList.data,
              quote: data,
            });
          }
          this.setState({ isPosting: false });
        })
        .catch(() => {
          this.setState({ isPosting: false });
        });
    }
  };

  onToggleSendEmail = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ sendEmail: event.target.checked, unsaved: true });
  };

  onToggleSendEmailViaGraph = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ sendEmailViaGraph: event.target.checked, unsaved: true });
  };

  previewDoc = () => {
    if (this.checkValidationForPreview()) {
      // tslint:disable-next-line: variable-name
      const json_config = cloneDeep(this.state.sow.json_config);
      const sow = cloneDeep(this.state.sow);
      const serviceCost: ISoWCalculationFields = getSowCalculationFields(
        this.state.sow.json_config.service_cost,
        this.props.docSetting
      );
      if (sow && json_config && json_config.service_cost) {
        json_config.service_cost.customer_cost_fixed_fee =
          serviceCost.totalCustomerCost;
        json_config.service_cost.internal_cost_fixed_fee =
          serviceCost.totalInternalCost;
        json_config.service_cost.customer_cost_t_and_m_fee =
          serviceCost.totalCustomerCost;
        json_config.service_cost.internal_cost_t_and_m_fee =
          serviceCost.totalInternalCost;
      }
      json_config.service_cost.customer_cost = serviceCost.totalCustomerCost;
      json_config.service_cost.internal_cost = serviceCost.totalInternalCost;
      json_config.service_cost.default_ps_risk /= 100;
      sow.json_config = json_config;
      delete sow.template;
      delete sow.update_version;
      delete sow.update_author;
      if (sow.change_request) {
        sow.project = sow.change_request.project;
        sow.change_request_type = sow.change_request.change_request_type;
        sow.requested_by = sow.change_request.requested_by;
        sow.change_number = sow.change_request.change_number;
        delete sow.change_request;
      }
      this.props.previewSOW(sow).then((a) => {
        if (a.type === CREATE_SOW_SUCCESS) {
          this.setState({ openPreview: true, previewHTML: a.response });
        }
        if (a.type === CREATE_SOW_FAILURE) {
          this.setValidationErrors(a.errorList.data);
        }
      });
    }
  };

  previewServiceDetail = () => {
    if (this.checkValidationForPreview()) {
      this.setState({ loading: true });
      // tslint:disable-next-line: variable-name
      const json_config = cloneDeep(this.state.sow.json_config);
      const serviceCost: ISoWCalculationFields = getSowCalculationFields(
        this.state.sow.json_config.service_cost,
        this.props.docSetting
      );
      if (json_config && json_config.service_cost) {
        json_config.service_cost.customer_cost_fixed_fee =
          serviceCost.totalCustomerCost;
        json_config.service_cost.internal_cost_fixed_fee =
          serviceCost.totalInternalCost;
        json_config.service_cost.customer_cost_t_and_m_fee =
          serviceCost.totalCustomerCost;
        json_config.service_cost.internal_cost_t_and_m_fee =
          serviceCost.totalInternalCost;
      }
      json_config.service_cost.customer_cost = serviceCost.totalCustomerCost;
      json_config.service_cost.internal_cost = serviceCost.totalInternalCost;
      json_config.service_cost.default_ps_risk /= 100;
      const payload: ISoWServiceDetailPayload = {
        json_config,
        name: this.state.sow.name,
        user: this.state.sow.user,
        customer: this.state.sow.customer,
        quote_id: this.state.sow.quote_id,
        doc_type: this.state.sow.doc_type,
        author: this.state.sow.author
          ? this.state.sow.author
          : this.props.user.id,
        updated_by: this.props.user.id,
        pm_type_t_and_m: this.state.sow.pm_type_t_and_m,
      };
      this.props
        .previewServiceDetail(payload)
        .then((a) => {
          if (a.type === PREVIEW_SERVICE_DETAIL_SUCCESS) {
            this.setState({
              openServicePreview: true,
              previewHTML: a.response,
            });
          }
          if (a.type === CREATE_SOW_FAILURE) {
            this.setValidationErrors(a.errorList.data);
          }
        })
        .finally(() => this.setState({ loading: false }));
    }
  };

  closeDocumentPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  closeServiceDetailPreview = () => {
    this.setState({
      openServicePreview: !this.state.openServicePreview,
      previewHTML: null,
    });
  };

  onChecboxChangedHidden = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      showHidden: event.target.checked,
    });
  };

  onHideImplementationLogistics = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.sow.json_config.implementation_logistics.hide_in_sow =
      e.target.checked;
    this.setState(newState);
  };

  onCoverPageChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    const option = e.target.name;
    sow[option] = e.target.checked;
    this.setState({ sow });
  };

  onFeeBasedTMChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const sow = cloneDeep(this.state.sow);
    const isFeeBasedTM = e.target.checked;
    sow.pm_type_t_and_m = isFeeBasedTM;
    sow.json_config.terms_t_and_m[
      "terms"
    ].value = isFeeBasedTM
      ? this.props.docSetting.pm_type_t_and_m_terms
      : this.props.docSetting.terms_t_and_m;
    this.setState({ sow });
  };

  render() {
    const query = new URLSearchParams(this.props.location.search);
    const cloned = query.get("cloned");
    const isOutlookEmail: boolean = Boolean(
      this.props.user && this.props.user.profile.outlook_email
    );

    const categoryObj = this.getCategoryOptions().find(
      (x) => x.label === "Change Request"
    );
    let categoryChangeRequest = 0;
    if (categoryObj) {
      categoryChangeRequest = categoryObj.value;
    }
    return (
      <div className="sow-container ">
        <div className="add-sow col-md-10">
          <div className="sow-add-edit-header">
            <h3>
              {this.state.sow.id && this.state.sow.id !== 0
                ? "Edit"
                : this.props.isFetchingSow
                ? ""
                : "Add"}{" "}
              Document
            </h3>
          </div>
          <div className="loader">
            <Spinner
              show={
                this.state.loading ||
                this.props.isFetching ||
                this.props.isFetchingSow ||
                this.props.isFetchingTemplate ||
                this.props.isFetchingCategory
              }
            />
          </div>
          <div className="basic-field">
            <Input
              field={{
                value: this.state.sow.name,
                label: "Name",
                type: "TEXT",
                isRequired: true,
              }}
              width={6}
              name={"name"}
              placeholder="Enter Document name"
              onChange={(e) => this.handleChangeSimpleFields(e)}
              className="select-type"
              error={this.state.error.name}
            />

            <div className="select-type select-sow field-section col-lg-6 col-md-6">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Select Type
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.doc_type.errorMessage ? `error-input` : ""
                }`}
              >
                <SelectInput
                  name="doc_type"
                  value={this.state.sow.doc_type}
                  onChange={(e) => this.handleChangeType(e)}
                  options={this.getDocumentTypesOptions()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Type"
                  disabled={this.state.sow.category === categoryChangeRequest}
                />
              </div>
              {this.state.error.doc_type.errorMessage && (
                <div className="select-sow-error">
                  {this.state.error.doc_type.errorMessage}
                </div>
              )}
            </div>
            <div className="select-type select-sow field-section col-lg-6 col-md-6">
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Select Category
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.category.errorMessage ? `error-input` : ""
                }`}
              >
                <SelectInput
                  name="category"
                  value={this.state.sow.category}
                  onChange={(e) => this.handleChangeCategory(e)}
                  options={this.getCategoryOptions()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Category"
                  disabled={
                    (this.state.sow.id && this.state.sow.id !== 0) ||
                    cloned === "true"
                      ? true
                      : false
                  }
                />
              </div>
              <div className="select-sow-error">
                {this.state.error.category.errorMessage}
              </div>
            </div>
            {this.state.sow.id === 0 && cloned !== "true" && (
              <div className="select-type select-sow field-section col-lg-6 col-md-6">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Select Template
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.error.template.errorMessage ? `error-input` : ""
                  }`}
                >
                  <SelectInput
                    name="template"
                    value={this.state.sow.template}
                    onChange={(e) => this.handleChangeTemplate(e)}
                    options={this.getTemplateOptions()}
                    multi={false}
                    searchable={true}
                    placeholder="Select Template"
                    disabled={
                      (this.state.sow.id && this.state.sow.id !== 0) ||
                      !this.state.sow.category
                        ? true
                        : false
                    }
                  />
                </div>
                <div className="select-sow-error">
                  {this.state.error.template.errorMessage}
                </div>
              </div>
            )}
            <div
              className="select-type select-sow
          field-section col-lg-6 col-md-6"
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Select Customer
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.error.customer.errorMessage ? `error-input` : ""
                }`}
              >
                <SelectInput
                  name="customer"
                  value={this.state.sow.customer}
                  onChange={(e) => this.handleChangeCustomer(e)}
                  options={this.getCustomerOptions()}
                  multi={false}
                  searchable={true}
                  placeholder="Select Customer"
                  disabled={
                    this.state.sow.id && this.state.sow.id !== 0 ? true : false
                  }
                />
              </div>
              <div className="select-sow-error">
                {this.state.error.customer.errorMessage}
              </div>
            </div>
            <div
              className="select-type select-sow opportunity add-new-option-section
          field-section col-lg-6 col-md-6"
            >
              <div className="add-new-option-box">
                <div className="field__label row">
                  <label className="field__label-label" title="">
                    Select User
                  </label>
                  <span className="field__label-required" />
                </div>
                <div
                  className={`${
                    this.state.error.user.errorMessage ? `error-input` : ""
                  }`}
                >
                  <SelectInput
                    name="user"
                    value={this.state.sow.user}
                    onChange={(e) => this.handleChangeSimpleFields(e)}
                    options={this.getCustomerUserOptions()}
                    multi={false}
                    searchable={true}
                    placeholder="Select User"
                    disabled={!this.state.sow.customer}
                    loading={this.props.isFetchingUsers}
                  />
                </div>
                {this.state.error.user.errorMessage && (
                  <div className="select-sow-error">
                    {this.state.error.user.errorMessage}
                  </div>
                )}
              </div>
              <SquareButton
                content="+"
                onClick={() => this.toggleCreateUserModal()}
                className="add-new-option-sow"
                bsStyle={"primary"}
                title="Add New User"
                disabled={!this.state.sow.customer}
              />
            </div>
            {this.state.sow.id !== 0 && this.props.quote && (
              <div
                className="select-type select-sow
              opportunity quote-name-doc-section
            field-section col-lg-6 col-md-6"
              >
                <div className="field__label row ">
                  <label className="field__label-label" title="">
                    Opportunity
                  </label>
                </div>
                <div className="quote-name-doc">
                  {this.props.quote.name} {`(${this.props.quote.stage_name})`}
                </div>
                {this.state.error.quote_id.errorMessage && (
                  <div className="sow-validations-error-quote">
                    {this.state.error.quote_id.errorMessage}
                  </div>
                )}
              </div>
            )}
            {this.state.sow.id !== 0 && this.state.quote && (
              <Input
                field={{
                  value: this.state.quote.stage_id,
                  label: "Stage",
                  type: "PICKLIST",
                  isRequired: true,
                  options: this.state.stages,
                }}
                width={6}
                name={"stage_id"}
                placeholder="Select Stage"
                onChange={(e) => this.handleChangeQuote(e)}
                className="select-type"
                loading={this.props.quoteFetching || this.state.stageLoading}
                error={this.state.error.stage}
              />
            )}
            {(this.state.sow.id === 0 ||
              (this.state.sow.id !== 0 &&
                this.props.quote === null &&
                this.props.isFetchingSingleQuote === false)) && (
              <div
                className="select-type select-sow
              opportunity add-new-option-section
            field-section col-lg-6 col-md-6"
              >
                <div className="add-new-option-box">
                  <div className="field__label row ">
                    <label className="field__label-label" title="">
                      Select Opportunity
                    </label>
                    <span className="field__label-required" />
                  </div>
                  <div
                    className={`${
                      this.state.error.quote_id.errorMessage
                        ? `error-input`
                        : ""
                    }`}
                  >
                    <SelectInput
                      name="quote_id"
                      value={this.state.sow.quote_id}
                      onChange={(e) => this.handleChangeSimpleFields(e)}
                      options={this.getOpportunityOptions()}
                      multi={false}
                      searchable={true}
                      placeholder="Select Opportunity"
                      disabled={
                        !this.state.sow.customer || !this.state.sow.user
                      }
                    />
                  </div>
                  {this.state.error.quote_id.errorMessage && (
                    <div className="select-sow-error">
                      {this.state.error.quote_id.errorMessage}
                    </div>
                  )}
                </div>
                <SquareButton
                  content="+"
                  onClick={(e) => this.toggleCreateOpportunityModal()}
                  className="add-new-option-sow"
                  bsStyle={"primary"}
                  title="Add New Opportunity"
                  disabled={!this.state.sow.customer || !this.state.sow.user}
                />
              </div>
            )}
            {this.state.sow.json_config && (
              <div className="select-type select-sow field-section col-lg-6 col-md-6">
                <div className="field__label row">
                  <label
                    className="field__label-label"
                    title="Document Details"
                  >
                    Document Details
                  </label>
                </div>
                <div className="sow-checkbox-inputs">
                  <Checkbox
                    isChecked={this.state.showHidden}
                    name="option"
                    onChange={(e) => this.onChecboxChangedHidden(e)}
                    className="sow-top-section-checkbox"
                  >
                    Show hidden sections
                  </Checkbox>
                  <Checkbox
                    isChecked={Boolean(
                      this.state.sow.json_config.implementation_logistics &&
                        this.state.sow.json_config.implementation_logistics
                          .hide_in_sow
                    )}
                    name="option"
                    onChange={(e) => this.onHideImplementationLogistics(e)}
                    className="sow-top-section-checkbox"
                  >
                    Hide Implementation Logistics
                  </Checkbox>
                  <Checkbox
                    isChecked={this.state.sow.include_cover_page}
                    name="include_cover_page"
                    onChange={this.onCoverPageChange}
                    className="sow-top-section-checkbox"
                  >
                    Add Cover Page
                  </Checkbox>
                  {this.state.sow.doc_type === "T & M" && (
                    <Checkbox
                      isChecked={this.state.sow.pm_type_t_and_m}
                      name="pm_type_t_and_m"
                      onChange={this.onFeeBasedTMChange}
                      className="sow-top-section-checkbox"
                    >
                      Fee Based Project Management
                    </Checkbox>
                  )}
                  <Checkbox
                    isChecked={this.state.sow.include_customer_logo}
                    name="include_customer_logo"
                    onChange={this.onCoverPageChange}
                    className="sow-top-section-checkbox"
                    disabled={!this.state.customerHasLogo}
                  >
                    Include Logo
                  </Checkbox>
                  <IconButton
                    newIcon={true}
                    icon={
                      this.state.customerHasLogo
                        ? "edit_pencil.svg"
                        : "plus.svg"
                    }
                    className="add-logo-btn"
                    onClick={this.toggleLogoModal}
                    title={`${
                      this.state.customerHasLogo ? "Edit" : "Add"
                    } Customer Logo`}
                  />
                </div>
              </div>
            )}
          </div>

          {this.state.sow.json_config &&
            this.renderTemplateByJson(this.state.sow.json_config)}
          {this.state.sow.json_config &&
            this.state.sow.doc_type === "Fixed Fee" &&
            this.renderMilestonesCollapsable()}
          {this.state.sow.json_config &&
            this.props.docSetting &&
            this.renderStaticFields()}
          {this.state.showError && (
            <div className="board-error">{this.state.validationError}</div>
          )}
          <AddVendor
            show={this.state.showVendorModal}
            onClose={() => this.toggleVendorModal(false)}
            onSubmit={() => this.onSubmitVendorModal()}
          />
          <AddQuote
            show={this.state.isCreateOpportunityModal}
            onClose={this.toggleCreateOpportunityModal}
            onSubmit={this.createQuote}
            types={this.props.qTypeList}
            stages={this.state.stages}
            isLoading={this.state.isPosting}
            errorList={this.state.errorList}
            quote={this.state.quote}
          />
          {this.state.isCreateUserModal && (
            <CustomerUserNew
              isVisible={this.state.isCreateUserModal}
              close={this.closeUserModal}
              customerId={this.state.sow.customer}
            />
          )}
          <VendorMappingModal
            show={this.state.showVendorMappingModal}
            closeModal={this.closeVendorMappingModal}
            mapping={this.state.currentVendorMapping}
          />
          <ConfirmBox
            show={this.state.isopenConfirm}
            onClose={this.toggleConfirmOpen}
            onSubmit={this.onClickConfirm}
            isLoading={
              this.props.isFetching || this.props.isFetchingSingleQuote
            }
            title={"Are you sure, want to change Stage?"}
          />
          <PDFViewer
            show={this.state.openPreview}
            onClose={this.closeDocumentPreview}
            titleElement={`View SOW Preview`}
            previewHTML={this.state.previewHTML}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={"default"}
                onClick={this.closeDocumentPreview}
              />
            }
            className=""
          />
          {this.state.showLogoModal && (
            <LogoUpload
              show={true}
              onClose={this.toggleLogoModal}
              customerId={this.state.sow.customer}
            />
          )}
          <PDFViewer
            show={this.state.openServicePreview}
            onClose={this.closeServiceDetailPreview}
            titleElement={`SoW Service Detail Preview`}
            previewHTML={this.state.previewHTML}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={"default"}
                onClick={this.closeServiceDetailPreview}
              />
            }
            className=""
          />
        </div>
        <div className="col-md-2 footer">
          <SquareButton
            content="Preview"
            onClick={this.previewDoc}
            className="preview-sow-doc"
            bsStyle={"primary"}
          />
          {this.state.sow.json_config && (
            <SquareButton
              content="Service Detail"
              onClick={this.previewServiceDetail}
              className="preview-service-cost"
              bsStyle={"primary"}
              title="Preview the Service Detail PDF for SoW"
            />
          )}
          {this.state.sow.id !== 0 && (
            <div className="doc-details col-md-12">
              <div>
                {" "}
                Created by <span>{this.state.sow.author_name}</span>.
              </div>
              <div>
                {" "}
                Last updated by <span>
                  {this.state.sow.updated_by_name}
                </span> on{" "}
                <span>
                  {moment
                    .utc(this.state.sow.updated_on)
                    .local()
                    .format("MM/DD/YYYY hh:mm A")}
                </span>
              </div>
              {Boolean(this.state.sow.id) && (
                <div>
                  {" "}
                  Version -{" "}
                  <span>
                    {this.state.sow.major_version || "1"}.
                    {this.state.sow.minor_version}
                  </span>
                </div>
              )}
            </div>
          )}
          <div className="sow-checkboxes">
            <Checkbox
              isChecked={this.state.sendEmail}
              name="option"
              onChange={(e) => this.onToggleSendEmail(e)}
              className="send-acc-mngr"
            >
              Send to Account Manager
            </Checkbox>
            <Checkbox
              isChecked={this.state.sendEmailViaGraph}
              name="option"
              onChange={(e) => this.onToggleSendEmailViaGraph(e)}
              className="send-acc-mngr"
              title={
                isOutlookEmail
                  ? "Send the email to Account Manager via Microsoft Graph API"
                  : "Please set the Outlook Email"
              }
              disabled={!isOutlookEmail}
            >
              Send to AM Via Graph API
            </Checkbox>
            <Checkbox
              isChecked={this.state.sow.update_version}
              name="update_version"
              onChange={(e) => this.onChecboxChangeUpdateCheckbox(e)}
              className="crt-mjr-vrsn"
            >
              Create major version
            </Checkbox>
            <Checkbox
              isChecked={this.state.sow.update_author}
              name="update_author"
              onChange={(e) => this.onChecboxChangeUpdateCheckbox(e)}
              className="make-doc-owner"
            >
              Make me document author
            </Checkbox>
          </div>
          <SquareButton
            content="Close"
            onClick={() => this.props.history.goBack()}
            className="save-mapping"
            bsStyle={"default"}
          />

          {Boolean(this.state.sow.id && this.state.sow.id !== 0) && (
            <SquareButton
              content={
                <span>
                  {this.state.saving && (
                    <img
                      src={"/assets/icons/loading.gif"}
                      alt="Downloading File"
                    />
                  )}
                  Update Document
                </span>
              }
              onClick={() => this.callSaveSOW(false)}
              className="save-mapping"
              bsStyle={"primary"}
              disabled={
                (this.props.docSetting &&
                  this.props.docSetting.engineering_hourly_cost === null) ||
                this.state.saving
              }
              title={`${
                this.props.docSetting &&
                this.props.docSetting.engineering_hourly_cost === null
                  ? "Complete SOW setting to enable Update & Close."
                  : " "
              }`}
            />
          )}
          <SquareButton
            content={
              <span>
                {this.state.saving && (
                  <img
                    src={"/assets/icons/loading.gif"}
                    alt="Downloading File"
                  />
                )}
                {this.state.sow.id && this.state.sow.id !== 0
                  ? "Update & Close"
                  : "Save Document"}
              </span>
            }
            onClick={() => this.callSaveSOW(true)}
            className="save-mapping"
            bsStyle={"primary"}
            disabled={
              (this.props.docSetting &&
                this.props.docSetting.engineering_hourly_cost === null) ||
              this.state.saving
            }
            title={`${
              this.props.docSetting &&
              this.props.docSetting.engineering_hourly_cost === null
                ? "Complete SOW setting to enable Save."
                : " "
            }`}
          />
        </div>
        <ConfirmBox
          isLoading={false}
          okText="Override"
          show={this.state.showOverrideConfirmation}
          onClose={this.closeOverrideConfirmationPopup}
          onSubmit={this.handleChangeStaticOverride}
          title={
            "Are you sure about overriding this resource, doing so will delete the Phase(s) mapped to this resource?"
          }
        />
        <PromptUnsaved
          when={this.state.unsaved}
          navigate={(path) => this.props.history.push(path)}
          shouldBlockNavigation={(location) => {
            if (this.state.unsaved) {
              return true;
            }
            return false;
          }}
          onSaveClick={(e) => this.callSaveSOW(true)}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  vendorMapping: state.sow.vendorMapping,
  quote: state.sow.quote,
  sow: state.sow.sow,
  documentTypes: state.sow.documentTypes,
  vendorOptions: state.sow.vendorOptions,
  isFetchingVendors: state.sow.isFetchingVendors,
  categoryList: state.sow.categoryList,
  isFetchingTemplate: state.sow.isFetchingCategory,
  isFetchingCategory: state.sow.isFetchingTemplate,
  isFetching: state.sow.isFetching,
  quoteList: state.dashboard.quoteList,
  quoteFetching: state.dashboard.quoteFetching,
  customers: state.customer.customersShort,
  isFetchingUsers: state.documentation.isFetching,
  templates: state.sow.templates,
  isFetchingSow: state.sow.isFetchingSow,
  qTypeList: state.sow.qTypeList,
  isFetchingQStageList: state.sow.isFetchingQStageList,
  isFetchingQTypeList: state.sow.isFetchingQTypeList,
  isFetchingSingleQuote: state.sow.isFetchingSingleQuote,
  docSetting: state.setting.docSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getSOW: (id: number) => dispatch(getSOW(id)),
  getVendorsList: () => dispatch(getVendorsList()),
  getDocumentType: () => dispatch(getDocumentType()),
  getCategoryList: () => dispatch(getCategoryList()),
  getTemplateList: () => dispatch(getTemplateList()),
  getQuoteStages: () => dispatch(getQuoteAllStages()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  getVendorMapping: () => dispatch(getVendorMappingList()),
  getTemplate: (id: number) => dispatch(getTemplate(id)),
  checkCustomerLogo: (customerId: number) =>
    dispatch(customerLogoCRUD(customerId, "get")),
  previewServiceDetail: (payload: ISoWServiceDetailPayload) =>
    dispatch(previewServiceDetail(payload)),
  createQuote: (id: number, quote: IQuote) => dispatch(createQuote(id, quote)),
  saveSOW: (sow: ISoW, sendEmail: boolean, sendEmailViaGraph: boolean) =>
    dispatch(saveSOW(sow, sendEmail, sendEmailViaGraph)),
  previewSOW: (sow: ISoW) => dispatch(previewSOW(sow)),
  updateSOW: (sow: ISoW, sendEmail: boolean, sendEmailViaGraph: boolean) =>
    dispatch(updateSOW(sow, sendEmail, sendEmailViaGraph)),
  fetchQuoteDashboardListing: (id: number, openOnly?: boolean) =>
    dispatch(fetchQuoteDashboardListingPU(id, openOnly)),
  fetchAllCustomerUsers: (id: number) => dispatch(fetchAllCustomerUsers(id)),
  getSingleQuote: (id: number) => dispatch(getSingleQuote(id)),
  fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
  updateQuoteStage: (id: number, stageId: number) =>
    dispatch(updateQuoteStage(id, stageId)),
  addInfoMessage: (msg: string) => dispatch(addInfoMessage(msg)),
  addWarningMessage: (msg: string) => dispatch(addWarningMessage(msg)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AddSow);
