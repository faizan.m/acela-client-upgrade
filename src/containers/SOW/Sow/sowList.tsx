import React from "react";
import moment from "moment";
import { saveAs } from "file-saver";
import { connect } from "react-redux";
// import htmlDocx from "html-docx-js/dist/html-docx"; (New Component)
import { addInfoMessage } from "../../../actions/appState";
import { fetchAllProviderUsers } from "../../../actions/provider/user";
import {
  deleteSOW,
  getSOWList,
  downloadSOWDocumnet,
  sendEmailToAccountManager,
  EDIT_SOW_SUCCESS,
  DOWNLOAD_SOW_SUCCESS,
  EDIT_TEMPLATE_SUCCESS,
  sendEmailToAccountManagerViaGraph,
} from "../../../actions/sow";
import { LAST_VISITED_CLIENT_360_TAB } from "../../../actions/customer";
import Input from "../../../components/Input/input";
import Table from "../../../components/Table/table";
import Select from "../../../components/Input/Select/select";
import Checkbox from "../../../components/Checkbox/checkbox";
import SquareButton from "../../../components/Button/button";
import IconButton from "../../../components/Button/iconButton";
import RenameBox from "../../../components/RenameBox/RenameBox";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../components/Button/deleteButton";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import ViewSOWHistory from "./sowHistory";
import DocumentFilter from "./filterDocument";
import ViewSOWCDHistory from "./sowCDHistory";
import {
  fromISOStringToFormattedDate,
  utcToLocalInLongFormat,
} from "../../../utils/CalendarUtil";
import { searchInFields } from "../../../utils/searchListUtils";
import "../../../commonStyles/filtersListing.scss";
import store from "../../../store";
import "./style.scss";

interface ISOWListProps extends ICommonProps {
  customerId?: number;
  SOWList: ISOWShort[];
  isFetchingSow: boolean;
  isFetchingSowList: boolean;
  providerUsers: ISuperUser[];
  customers: ICustomerShort[];
  addInfoMessage: TShowInfoMessage;
  deleteSOW: (id: number) => Promise<any>;
  fetchAllProviderUsers: () => Promise<any>;
  sendEmailToAccountManager: (id: number) => Promise<any>;
  sendEmailToAccountManagerViaGraph: (id: number) => Promise<any>;
  getSOWList: (
    show: boolean,
    closed: boolean,
    params?: IServerPaginationParams
  ) => Promise<any>;
  downloadSOWDocumnet: (
    id: number,
    type: string,
    name?: string
  ) => Promise<any>;
}

interface ISOWListtate {
  id: number;
  document: any;
  closed: boolean;
  docType: string;
  previewHTML: IFileResponse;
  documentName: string;
  openPreview: boolean;
  rows: ISowTableRow[];
  searchString: string;
  viewhistory: boolean;
  isopenRename: boolean;
  isSOWPosting: boolean;
  showOlderSOW: boolean;
  emailSending: number[];
  emailSendingForGraph: number[];
  isopenConfirm: boolean;
  viewCDhistory: boolean;
  previewPDFIds: number[];
  downloadingIds: number[];
  filters: IDocumentFilters;
  isFilterModalOpen: boolean;
  downloadingPDFIds: number[];
  currentSOWChangeRequest: ICRShort;
  authorLabelIds: { [id: number]: string };
  customerLabelIds: { [id: number]: string };
  showRowActions: boolean;
  rowEditId: any;
  message: string;
  messageId: any;
}

interface ISowTableRow {
  id: number;
  name: string;
  quote: IQuote;
  index: number;
  version: string;
  type: SowDocType;
  updated_on: string;
  created_on: string;
  author_name: string;
  customer_name: string;
  category_name: string;
  updated_by_name: string;
  change_request: ICRShort;
  forecast_exception: boolean;
}

class SOWListing extends React.Component<ISOWListProps, ISOWListtate> {
  constructor(props: ISOWListProps) {
    super(props);

    this.state = {
      rows: [],
      isSOWPosting: false,
      isopenConfirm: false,
      isopenRename: false,
      id: null,
      downloadingIds: [],
      downloadingPDFIds: [],
      previewPDFIds: [],
      emailSending: [],
      emailSendingForGraph: [],
      searchString: "",
      isFilterModalOpen: false,
      filters: {
        customer: [],
        author: [],
        is_quote_exist: [],
      },
      customerLabelIds: {},
      authorLabelIds: {},
      showOlderSOW: false,
      closed: false,
      documentName: "",
      document: null,
      docType: "",
      openPreview: false,
      previewHTML: null,
      viewhistory: false,
      viewCDhistory: false,
      currentSOWChangeRequest: undefined,
      showRowActions: false,
      rowEditId: undefined,
      message: "",
      messageId: undefined,
    };
  }

  componentDidMount() {
    if (this.props.location.pathname === "/client-360") {
      store.dispatch({
        type: LAST_VISITED_CLIENT_360_TAB,
        response: "Documents",
      });
    }
    if (this.props.customerId) {
      this.setState((prevState) => ({
        filters: {
          ...prevState.filters,
          customer: [this.props.customerId],
        },
      }));
    }
    this.props.fetchAllProviderUsers();
    this.props.getSOWList(this.state.showOlderSOW, this.state.closed);
    if (this.props.customers && this.props.customers.length > 0) {
      const customerLabelIds = this.props.customers.reduce((labelIds, s) => {
        labelIds[s.id] = s.name;

        return labelIds;
      }, {}); // tslint:disable-line

      this.setState({
        customerLabelIds,
      });
    }
  }

  componentDidUpdate(prevProps: ISOWListProps) {
    if (
      this.props.customerId &&
      prevProps.customerId !== this.props.customerId
    ) {
      this.setState(
        (prevState) => ({
          filters: {
            ...prevState.filters,
            customer: [this.props.customerId],
          },
        }),
        () => this.onFiltersUpdate(this.state.filters)
      );
    }
    if (this.props.SOWList !== prevProps.SOWList) {
      const rows = this.getRows(this.props, "", this.state.filters);
      this.setState({
        rows,
      });
    }
    if (this.props.customers && this.props.customers !== prevProps.customers) {
      const customerLabelIds = this.props.customers.reduce((labelIds, s) => {
        labelIds[s.id] = s.name;
        return labelIds;
      }, {});

      this.setState({
        customerLabelIds,
      });
    }
    if (this.props.providerUsers !== prevProps.providerUsers) {
      const authorLabelIds = this.props.providerUsers.reduce((labelIds, s) => {
        labelIds[s.id] = `${s.first_name} ${s.last_name}`;
        return labelIds;
      }, {});

      this.setState({
        authorLabelIds,
      });
    }
  }

  getRows = (
    nextProps: ISOWListProps,
    searchString?: string,
    filters?: IDocumentFilters,
    showOlderSOW?: boolean
  ): ISowTableRow[] => {
    let SOWList = nextProps.SOWList;
    const search = searchString ? searchString : this.state.searchString;

    if (filters) {
      // Add filter for type once included.
      const filterAuthor = filters.author;
      const filterCustomer = filters.customer;
      const filterQuote = filters.is_quote_exist;

      SOWList = SOWList.filter((device) => {
        const hasFilteredrAuthor =
          filterAuthor.length > 0 ? filterAuthor.includes(device.author) : true;
        const hasFilteredCustomer =
          filterCustomer.length > 0
            ? filterCustomer.includes(device.customer_id)
            : true;
        const hasFilterQuote =
          filterQuote.length > 0
            ? filterQuote.includes(device.quote_exist)
            : true;

        return hasFilteredrAuthor && hasFilteredCustomer && hasFilterQuote;
      });
    }
    if (search && search.length > 0) {
      SOWList = SOWList.filter((row) =>
        searchInFields(row, search, [
          "name",
          "doc_type",
          "customer_name",
          "updated_by_name",
          "category_name",
        ])
      );
    }

    const rows: ISowTableRow[] =
      SOWList &&
      SOWList.map((sow, index) => ({
        name: sow.name ? sow.name : "N.A.",
        type: sow.doc_type,
        change_request: sow.change_request ? sow.change_request[0] : undefined,
        customer_name: sow.customer_name ? sow.customer_name : "N.A.",
        author_name: sow.author_name ? sow.author_name : "N.A.",
        updated_on: sow.updated_on ? sow.updated_on : "N.A.",
        category_name: sow.category_name ? sow.category_name : "N.A.",
        updated_by_name: sow.updated_by_name ? sow.updated_by_name : "N.A.",
        created_on: sow.created_on ? sow.created_on : "",
        forecast_exception: sow.forecast_exception
          ? Boolean(sow.forecast_exception.exception_in_forecast_creation)
          : false,
        quote: sow.quote,
        version: sow.version,
        id: sow.id,
        index,
      }));

    return rows;
  };

  onChecboxChanged = (event: React.ChangeEvent<HTMLInputElement>) => {
    const showOlderSOW = event.target.checked;
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters,
      showOlderSOW
    );

    this.setState({
      showOlderSOW,
      rows,
    });
    this.props.getSOWList(showOlderSOW, this.state.closed);
  };

  onChecboxChangedClose = (event: React.ChangeEvent<HTMLInputElement>) => {
    const closed = event.target.checked;
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters,
      closed
    );

    this.setState({
      closed,
      rows,
    });
    this.props.getSOWList(this.state.showOlderSOW, closed);
  };

  onEditRowClick = (e, row: ISowTableRow) => {
    if (e) {
      e.stopPropagation();
    }
    if (row.type !== "Change Request")
      this.props.history.push(`/sow/sow/${row.id}`);
    else
      this.props.history.push(
        `/Projects/${row.change_request.project}/change-requests/${row.change_request.id}`
      );
  };

  toggleviewCDHistoryPopup = () => {
    this.setState({
      viewCDhistory: false,
    });
  };

  onClickViewviewCDHistory = () => {
    this.setState({
      viewCDhistory: true,
    });
  };

  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: null,
    });
  };

  onClickViewHistory(id: number, change_request: ICRShort, e) {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      currentSOWChangeRequest: change_request,
      id,
    });
  }

  getSOWFromVersions = () => {
    this.setState({
      viewhistory: false,
      id: null,
    });
  };

  renderTopBar = () => {
    return (
      <div className="SOW-users-listing__actions">
        <div className="row-action">
          <div className="left">
            <Input
              field={{
                value: this.state.searchString,
                label: "",
                type: "SEARCH",
              }}
              width={9}
              name="searchString"
              onChange={this.handleChange}
              placeholder="Search Documents"
              className="search"
            />
            <div className="checkbox-header">
              <Checkbox
                isChecked={this.state.showOlderSOW}
                name="option"
                onChange={(e) => this.onChecboxChanged(e)}
                className="show-older"
              >
                Show older(60 days)
              </Checkbox>
              <Checkbox
                isChecked={this.state.closed}
                name="option"
                onChange={(e) => this.onChecboxChangedClose(e)}
                className="show-older"
              >
                Show Closed/Missing Opps.
              </Checkbox>
            </div>
          </div>
          <div className="field-section actions-right">
            {this.props.customerId && (
              <SquareButton
                content={
                  <>
                    <span className="add-plus">+</span>
                    <span className="add-text">Add Document</span>
                  </>
                }
                onClick={this.addSOWClick}
                className="add-btn"
                bsStyle={"link"}
              />
            )}
            <SquareButton
              onClick={this.onClickViewviewCDHistory}
              content={
                <span>
                  <img alt="" src="/assets/icons/version.svg" />
                  History
                </span>
              }
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={this.props.SOWList && this.props.SOWList.length === 0}
              bsStyle={"primary"}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };

  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters
    );

    this.setState({ rows });
  };

  onClickConfirm = () => {
    this.props.deleteSOW(this.state.id).then((action) => {
      if (action.type === EDIT_TEMPLATE_SUCCESS) {
        this.props.getSOWList(this.state.showOlderSOW, this.state.closed);
        this.toggleConfirmOpen();
      }
    });
  };

  addSOWClick = () => {
    this.props.history.push(
      "/sow/sow/0" +
        (this.props.customerId ? `?customer=${this.props.customerId}` : "")
    );
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: null,
    });
  };

  toggleOpenRename = () => {
    this.setState({
      isopenRename: !this.state.isopenRename,
    });
  };

  onPDFClick(original: ISowTableRow, e) {
    if (e) {
      e.stopPropagation();
    }
    let updatedDate: string = moment(original.updated_on).format("YYYY-MM-DD");
    let name = `${original.customer_name} - ${
      original.name
    } - Sow v${original.version.replace(".", "-")} - ${updatedDate}.pdf`;
    this.setState({
      isopenRename: !this.state.isopenRename,
      documentName: name,
      id: original.id,
      docType: "pdf",
    });
  }

  downloadPDFReport = (name: string, docId: number) => {
    this.setState({
      downloadingPDFIds: [...this.state.downloadingPDFIds, docId],
    });
    this.props.downloadSOWDocumnet(docId, "pdf", name).then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          isopenRename: false,
          id: null,
          docType: "",
          downloadingPDFIds: this.state.downloadingPDFIds.filter(
            (id) => docId !== id
          ),
        });
        const url = action.response.file_path;
        const link = document.createElement("a");
        link.href = url;
        link.target = "_blank";
        link.setAttribute("download", action.response.file_name);
        document.body.appendChild(link);
        link.click();
      }
    });
  };

  getStream = (doc: any) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, doc.id],
    });
    this.props.downloadSOWDocumnet(doc.id, "doc").then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        // const sourceHTML = action.response;
        // const converted = htmlDocx.asBlob(sourceHTML, {
        //   orientation: "portrait",
        //   margins: {
        //     top: 300,
        //     right: 1300,
        //     left: 1300,
        //     header: 400,
        //     footer: 200,
        //     bottom: 0,
        //   },
        // });
        // let updatedDate: string = moment(doc.updated_on).format("YYYY-MM-DD");
        // let name = `${doc.customer_name} - ${
        //   doc.name
        // } - Sow v${doc.version.replace(".", "-")} - ${updatedDate}`;
        // this.setState({
        //   documentName: name,
        //   document: converted,
        //   isopenRename: true,
        //   docType: "doc",
        // });
        // this.setState({
        //   downloadingIds: this.state.downloadingIds.filter(
        //     (id) => doc.id !== id
        //   ),
        // });
      }
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IDocumentFilters) => {
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState({
      rows,
      filters,
      isFilterModalOpen: false,
    });
  };

  OnRenameAndDownload = (name: string) => {
    if (this.state.docType === "pdf") {
      this.downloadPDFReport(name, this.state.id);
      this.setState({ docType: "" });
    } else {
      saveAs(this.state.document, `${name}.docx`);
      this.setState({ docType: "" });
    }
  };

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.handleRows();
      }
    );
  };

  onClickEllipsis = (cell) => {
    let showPopUp = false;
    let editedRow = cell.value;

    if (this.state.rowEditId === cell.value) editedRow = undefined;
    showPopUp = false;

    if (this.state.rowEditId !== cell.value) editedRow = cell.value;
    showPopUp = true;

    this.setState({
      showRowActions: showPopUp,
      rowEditId: editedRow,
    });
  };
  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      (!this.props.customerId && filters.customer.length > 0) ||
      filters.author.length > 0 ||
      filters.is_quote_exist.length > 0
        ? true
        : false;
    const customers = this.props.customers
      ? this.props.customers.map((user) => ({
          value: user.id,
          label: user.name,
        }))
      : [];
    const providerUsers = this.props.providerUsers
      ? this.props.providerUsers.map((author, index) => ({
          value: author.id,
          label: `${author.first_name} ${author.last_name}`,
        }))
      : [];

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {!this.props.customerId && filters.customer.length > 0 && (
          <div className="section-show-filters">
            <label>Customer: </label>
            <Select
              name="customer"
              value={filters.customer}
              onChange={this.onFilterChange}
              options={customers}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.author.length > 0 && (
          <div className="section-show-filters">
            <label>Author: </label>
            <Select
              name="author"
              value={filters.author}
              onChange={this.onFilterChange}
              options={providerUsers}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.is_quote_exist.length > 0 && (
          <div className="section-show-filters">
            <label>Opportunity : </label>
            <Select
              name="is_quote_exist"
              value={filters.is_quote_exist}
              onChange={this.onFilterChange}
              options={[
                { value: true, label: "Available" },
                { value: false, label: "Not Available" },
              ]}
              multi={true}
              placeholder=""
            />
          </div>
        )}
      </div>
    ) : null;
  };

  onCloneClick = (id: number, e) => {
    if (e) {
      e.stopPropagation();
    }
    this.props.history.push(`/sow/sow/${id}?cloned=true`);
  };

  onPDFPreviewClick(original: ISowTableRow, e) {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, original.id],
    });
    this.props.downloadSOWDocumnet(original.id, "pdf").then((a) => {
      if (a.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          id: null,
          docType: "",
          previewPDFIds: this.state.previewPDFIds.filter(
            (id) => original.id !== id
          ),
        });
      }
    });
  }

  onEmailSending(id: number, e) {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      emailSending: [...this.state.emailSending, id],
    });
    this.props.sendEmailToAccountManager(id).then((action) => {
      if (action.type === EDIT_SOW_SUCCESS) {
        this.setState({
          emailSending: this.state.emailSending.filter((i) => i !== id),
        });
        this.props.addInfoMessage("Sending email to Account Manager...");
      }
    });
  }

  onEmailSendingViaGraph(id: number, e) {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      emailSendingForGraph: [...this.state.emailSendingForGraph, id],
    });
    this.props.sendEmailToAccountManagerViaGraph(id).then((action) => {
      if (action.type === EDIT_SOW_SUCCESS) {
        this.setState({
          emailSendingForGraph: this.state.emailSendingForGraph.filter(
            (i) => i !== id
          ),
        });
        this.props.addInfoMessage("Sending email to Account Manager...");
      }
    });
  }

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  render() {
    let columns: ITableColumn[] = [
      {
        accessor: "type",
        Header: " Type, Category & Quote",
        id: "type",
        width: 140,
        sortable: false,
        Cell: (cell) => (
          <div className="icons-template">
            {cell.original.forecast_exception && (
              <img
                className="forecast-warning"
                title="Forecast creation failed"
                alt="forecast-warning"
                src="/assets/icons/warning.png"
              />
            )}
            {cell.original.type === "Fixed Fee" && (
              <img
                className="left"
                title="Fixed Fee"
                alt=""
                src="/assets/icons/coin.svg"
              />
            )}{" "}
            {cell.original.type === "T & M" && (
              <img
                className="left"
                title={"T & M"}
                alt=""
                src="/assets/icons/timer.svg"
              />
            )}
            {cell.original.type === "Change Request" && (
              <img
                className="left"
                title={
                  cell.original.change_request
                    ? cell.original.change_request.change_request_type
                    : "Change Request"
                }
                alt=""
                src={`/assets/icons/${
                  cell.original.change_request
                    ? cell.original.change_request.change_request_type ===
                      "T & M"
                      ? "timer.svg"
                      : "coin.svg"
                    : "writing.svg"
                }`}
              />
            )}
            {cell.original.category_name === "Change Request" && (
              <img
                className="change-request"
                title=" Change Request"
                alt=""
                src="/assets/icons/writing.svg"
              />
            )}
            {cell.original.category_name === "Cloud" && (
              <img
                title="Cloud"
                alt="Cloud"
                src="/assets/new-icons/cloud.svg"
                className="cloud-svg"
              />
            )}
            {cell.original.category_name === "Collaboration" && (
              <img
                title="Collaboration"
                alt=""
                src="/assets/icons/collabHomeHover.webp"
              />
            )}
            {cell.original.category_name === "Networking" && (
              <img
                title="Networking"
                alt=""
                src="/assets/icons/NetworkingHomeHover.webp"
              />
            )}
            {cell.original.category_name === "Data Center" && (
              <img
                title="Data Center"
                alt=""
                src="/assets/icons/lifecycle.webp"
              />
            )}
            {cell.original.category_name === "Security" && (
              <img
                title="Security"
                alt=""
                src="/assets/icons/SecurityHomeHover.webp"
              />
            )}
            {cell.original.quote &&
              cell.original.quote.status_name &&
              cell.original.quote.status_name === "Open" && (
                <img
                  title={cell.original.quote.status_name}
                  className="quote"
                  alt=""
                  src="/assets/icons/open.svg"
                />
              )}
            {cell.original.quote &&
              cell.original.quote.status_name &&
              (cell.original.quote.status_name === "Won" ||
                cell.original.quote.status_name ===
                  "Won - Errors corrected ") && (
                <img
                  title={cell.original.quote.status_name}
                  className="quote"
                  alt=""
                  src="/assets/icons/won.svg"
                />
              )}
            {cell.original.quote &&
              cell.original.quote.status_name &&
              cell.original.quote.status_name === "Lost" && (
                <img
                  title={cell.original.quote.status_name}
                  className="quote"
                  alt=""
                  src="/assets/icons/HIGH.svg"
                />
              )}
            {cell.original.quote &&
              cell.original.quote.status_name &&
              cell.original.quote.status_name === "No decision" && (
                <img
                  title={cell.original.quote.status_name}
                  className="quote"
                  alt=""
                  src="/assets/icons/closed.svg"
                />
              )}
            {cell.original.quote &&
              cell.original.quote.status_name &&
              (cell.original.quote.status_name === "Rejected - See Notes Tab" ||
                cell.original.quote.status_name === "Rejected") && (
                <img
                  title={cell.original.quote.status_name}
                  className="quote"
                  alt=""
                  src="/assets/icons/rejected.svg"
                />
              )}
          </div>
        ),
      },
      {
        accessor: "customer_name",
        Header: "Customer",
        id: "customer_name",
        sortable: false,
        Cell: (c) => <div>{c.value}</div>,
      },
      // {
      //   accessor: "name",
      //   Header: "Name",
      //   sortable: true,
      //   id: "name",
      //   Cell: (name) => <div className="pl-15">{name.value}</div>,
      // },
      {
        accessor: "Opportunity",
        Header: "Opportunity",
        id: "quote",
        sortable: false,
        Cell: (cell) => (
          <div
            className="quote-name-listing"
            title={cell.original.quote && cell.original.quote.name}
          >
            {cell.original.quote && cell.original.quote.name}
          </div>
        ),
      },
      {
        accessor: "created_on",
        Header: "Created On",
        id: "created_on",
        sortable: true,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "updated_by_name",
        Header: "Updated by",
        id: "updated_by_name",
        Cell: (c) => <div>{c.value}</div>,
        sortable: false,
      },
      {
        accessor: "version",
        Header: "Version",
        width: 50,
        id: "version",
        Cell: (c) => <div>{c.value}</div>,
        sortable: false,
      },
      {
        accessor: "updated_on",
        Header: "Updated On",
        id: "updated_on",
        sortable: true,
        Cell: (cell) => (
          <div title={utcToLocalInLongFormat(cell.value)}>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      // {
      //   accessor: "id",
      //   Header: "Versions",
      //   width: 65,
      //   sortable: false,
      //   Cell: (cell) => (
      //     <IconButton
      //       icon="version.svg"
      //       onClick={(e) => {
      //         this.onClickViewHistory(
      //           cell.original.id,
      //           cell.original.change_request
      //         );
      //       }}
      //       className="version-btn"
      //       title={"Show SOW versions"}
      //     />
      //   ),
      // },
      {
        accessor: "id",
        Header: "Actions",
        width: 100,
        sortable: false,
        Cell: (cell) => (
          <div className="icons-template">
            <IconButton
              icon="edit.png"
              onClick={(e) => this.onEditRowClick(e, cell.original)}
              className="version-btn"
              title={"Edit"}
            />
            {cell.original.quote !== null && getPDFPrevieMarkUp(cell)}
            {cell.original.quote === null && (
              <img
                title={"Please add Opportunity for this SOW"}
                className="quote"
                alt=""
                src="/assets/icons/warning.png"
              />
            )}
            <div
              className="ellipse-actions"
              onClick={() => this.onClickEllipsis(cell)}
            >
              {this.state.messageId !== cell.value &&
                Array(3)
                  .fill(0)
                  .map((_, i) => (
                    <div
                      className="ellipse-item"
                      style={{
                        backgroundColor:
                          this.state.showRowActions &&
                          this.state.rowEditId === cell.value &&
                          "#41a2dd",
                      }}
                      key={i}
                    />
                  ))}
              {this.state.message !== "" &&
                this.state.messageId === cell.value && (
                  <div className="action-status-message-sales">
                    <div className="message-text">{this.state.message}</div>
                  </div>
                )}
              {this.state.showRowActions &&
                this.state.rowEditId === cell.value && (
                  <div
                    className="rowellipse-actions"
                    onClick={() => this.onClickEllipsis(cell)}
                  >
                    <img
                      src={`/assets/icons/cancel.svg`}
                      alt=""
                      className="ellipse-actions-close"
                      title={`Close`}
                    />
                    {cell.original.type !== "Change Request" && (
                      <div
                        className="option-dropdown"
                        onClick={(e) => this.onCloneClick(cell.value, e)}
                      >
                        {optionChildElements(
                          cell,
                          [],
                          "copy-document.svg",
                          "Clone document",
                          "Clone"
                        )}
                      </div>
                    )}
                    <div
                      className="option-dropdown"
                      onClick={(e) => {
                        this.state.emailSending.includes(cell.value)
                          ? null
                          : this.onEmailSending(cell.original.id, e);
                      }}
                    >
                      {optionChildElements(
                        cell,
                        this.state.emailSending,
                        "mail.svg",
                        "Send email to Account Manager",
                        "Send Email"
                      )}
                    </div>
                    <div
                      className="option-dropdown"
                      onClick={(e) => {
                        this.state.emailSendingForGraph.includes(cell.value)
                          ? null
                          : this.onEmailSendingViaGraph(cell.original.id, e);
                      }}
                    >
                      {optionChildElements(
                        cell,
                        this.state.emailSendingForGraph,
                        "mail.svg",
                        "Send email to Account Manager via Microsoft Graph API",
                        "Send Email Via Graph (Beta)"
                      )}
                    </div>
                    {cell.original.type !== "Change Request" && (
                      <div
                        className="option-dropdown"
                        onClick={(e) => {
                          this.onClickViewHistory(
                            cell.original.id,
                            cell.original.change_request,
                            e
                          );
                        }}
                      >
                        {optionChildElements(
                          cell,
                          [],
                          "version.svg",
                          "Show SOW versions",
                          "Versions"
                        )}
                      </div>
                    )}
                    {cell.original.quote !== null && (
                      <div
                        className="option-dropdown"
                        onClick={(e) => {
                          this.state.downloadingPDFIds.includes(cell.value)
                            ? null
                            : this.onPDFClick(cell.original, e);
                        }}
                      >
                        {optionChildElements(
                          cell,
                          this.state.downloadingPDFIds,
                          "pdf-file-format-symbol.svg",
                          "PDF Download",
                          "PDF Download"
                        )}
                      </div>
                    )}
                    {cell.original.type !== "Change Request" && (
                      <div
                        className="option-dropdown"
                        onClick={(e) => {
                          this.state.downloadingIds.includes(cell.value)
                            ? null
                            : this.getStream(cell.original);
                        }}
                      >
                        {optionChildElements(
                          cell,
                          this.state.downloadingIds,
                          "docx-file-format-symbol.svg",
                          "DOCX Download",
                          "DOCX Download"
                        )}
                      </div>
                    )}

                    {/* {cell.original.type !== "Change Request" &&
                      cell.original.quote !== null &&
                      getDOCXDownloadMarkUp(cell)} */}
                  </div>
                )}
            </div>
          </div>
        ),
      },
    ];

    const optionChildElements = (cell, loadingState, icon, title, label) => {
      return (
        <>
          <IconButton
            icon={loadingState.includes(cell.value) ? "loading.gif" : icon}
            onClick={(e) => null}
            className="version-btn"
            title={title}
          />
          <div className="label">{label}</div>
        </>
      );
    };
    const getPDFPrevieMarkUp = (cell) => {
      if (this.state.previewPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_preview"
            title="Preview"
            onClick={(e) => this.onPDFPreviewClick(cell.original, e)}
          />
        );
      }
    };

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: null,
    };

    // Remove customer column, for a single customer
    if (this.props.customerId) columns.splice(1, 1);

    return (
      <div className="sow-listing">
        {!this.props.customerId && (
          <div className="header">
            <h3>SOW Documents</h3>
            <SquareButton
              content="Add Document"
              onClick={this.addSOWClick}
              className="save-mapping"
              bsStyle={"primary"}
            />
          </div>
        )}
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetchingSowList ? `loading` : ``
          }`}
          loading={this.props.isFetchingSowList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetchingSow}
        />
        <RenameBox
          show={this.state.isopenRename}
          onClose={this.toggleOpenRename}
          onSubmit={this.OnRenameAndDownload}
          name={this.state.documentName}
          isLoading={this.props.isFetchingSow}
        />
        <DocumentFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          customers={this.props.customers}
          providerUsers={this.props.providerUsers}
          prevFilters={this.state.filters}
          singleCustomer={Boolean(this.props.customerId)}
        />
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
        <ViewSOWHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
          getSow={this.getSOWFromVersions}
          changeRequest={this.state.currentSOWChangeRequest}
        />
        {this.state.viewCDhistory && (
          <ViewSOWCDHistory
            show={this.state.viewCDhistory}
            onClose={this.toggleviewCDHistoryPopup}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  SOWList: state.sow.sowList,
  isFetchingSow: state.sow.isFetchingSow,
  customers: state.customer.customersShort,
  isFetchingSowList: state.sow.isFetchingSowList,
  providerUsers: state.providerUser.providerUsersAll,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchAllProviderUsers: () => dispatch(fetchAllProviderUsers()),
  getSOWList: (
    show: boolean,
    closed: boolean,
    params?: IServerPaginationParams
  ) => dispatch(getSOWList(show, closed, params)),
  deleteSOW: (id: number) => dispatch(deleteSOW(id)),
  sendEmailToAccountManager: (id: number) =>
    dispatch(sendEmailToAccountManager(id)),
  sendEmailToAccountManagerViaGraph: (id: number) =>
    dispatch(sendEmailToAccountManagerViaGraph(id)),
  downloadSOWDocumnet: (id: number, type: string, name?: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
  addInfoMessage: (message: string) => dispatch(addInfoMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SOWListing);
