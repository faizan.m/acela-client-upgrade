import React from "react";
import Spinner from "../../../components/Spinner";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import {
  previewSOW,
  CREATE_SOW_SUCCESS,
  getSOWHistory,
} from "../../../actions/sow";
import SquareButton from "../../../components/Button/button";
import _, { isEqual } from "lodash";
import "./style.scss";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
// import ReactDiffViewer from "react-diff-viewer"; (New Component)
import { getFieldNames } from "../../../utils/fieldName";
import "../../../commonStyles/versionHistory.scss";
import InfiniteHistory from "../../../components/InfiniteList/InfiniteHistory";

interface IViewSOWHistoryProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  id: any;
  sowHistory: any;
  isFetchingHistory: any;
  getSOWHistory: any;
  previewSOW: any;
  getSow: any;
  isFetchingSow: boolean;
  changeRequest?: any;
}

interface IViewSOWHistoryState {
  open: boolean;
  sowHistory: any;
  isCollapsed: boolean[];
  loadMore: boolean;
  sow: any;
  openPreview: boolean;
}

class ViewSOWHistory extends React.Component<
  IViewSOWHistoryProps,
  IViewSOWHistoryState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IViewSOWHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    sowHistory: [],
    isCollapsed: [],
    loadMore: false,
    openPreview: false,
    sow: null,
  });

  getTitle = () => {
    return (
      <div className="text-center">
        New Versions :{" "}
        {_.get(this.props.sowHistory, "[0]snapshot_data.name", "-")}
      </div>
    );
  };

  onClickLoadMore = () => {
    this.setState({ loadMore: !this.state.loadMore });
  };

  getVersion = (revision: any) => {
    const version =
      typeof revision.meta_data === "object"
        ? revision.meta_data.version
        : "N.A.";

    return version ? version : "N.A.";
  };

  checker = (value) => {
    var prohibited = [
      `root['json_config']['service_cost']['notes']`,
      "user_id",
      `root['json_config']['project_details']['out_of_scope']['value']`,
      `root['json_config']['project_details']['project_assumptions']['value']`,
      `root['json_config']['project_details']['task_deliverables']['value']`,
      `root['json_config']['project_management_t_and_m']['project_management']['value']`,
      `root['json_config']['project_summary']['project_summary']['value']`,
      `root['json_config']['terms_t_and_m']['terms']['value']`,
      `root['json_config']['alternates']['alternates']['value']`,
      `root['json_config']['change_description']['change_description']['value']`,
      `root['json_config']['change_impact']['change_impact']['value']`,
      `root['json_config']['reason']['reason']['value']`,
      `root['json_config']['risk']['risk']['value']`,
      `root['json_config']['technical_changes']['technical_changes']['value']`,
      `markdownKey`,
      `id`,
    ];

    for (var i = 0; i < prohibited.length; i++) {
      if (value.indexOf(prohibited[i]) > -1) {
        return false;
      }
    }
    return true;
  };

  getValueByField = (value, field) => {
    let currentValue = value;

    if (typeof value === "object") {
      currentValue = null;
      currentValue = JSON.stringify(value, null, 4);
    }

    if (typeof value === "object" && field.indexOf("travels") > -1) {
      currentValue = "";
      currentValue = `cost : ${value.cost || 0} 
description : ${value.description || "N.A."}  `;
    }
    if (typeof value === "object" && field.indexOf("contractors") > -1) {
      currentValue = "";
      currentValue = `Name : ${value.name || "N.A."}, 
Customer Cost : ${value.customer_cost || 0}, 
Margin Percentage : ${value.margin_percentage || "0"}% 
Partner cost : ${value.partner_cost}  `;
    }

    if (typeof value === "object" && field.indexOf("implementation") > -1) {
      currentValue = "";

      Object.keys(value).map((v) => {
        currentValue = currentValue + `${value[v].label} :${value[v].value}, `;
      });
    }

    return currentValue;
  };

  renderHistoryContainer = (history: any, boardIndex: number) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];
    const toggleCollapsedState = () =>
      this.setState((prevState) => ({
        isCollapsed: [
          ...prevState.isCollapsed.slice(0, boardIndex),
          !prevState.isCollapsed[boardIndex],
          ...prevState.isCollapsed.slice(boardIndex + 1),
        ],
      }));
    return (
      <div
        key={boardIndex}
        className={`collapsable-section  ${
          isCollapsed
            ? "collapsable-section--collapsed"
            : "collapsable-section--not-collapsed"
        }`}
      >
        <div
          className="col-md-12 collapsable-heading"
          onClick={toggleCollapsedState}
        >
          <div className="left col-md-9">
            <div className="name">
              {" "}
              <span>User: </span>
              {history.revision.user}
            </div>
            <div className="version">
              {" "}
              <span>version: </span>
              {this.getVersion(history.revision)}
            </div>
            <div className="date">
              {" "}
              <span>Updated On: </span>
              {utcToLocalInLongFormat(history.revision.date_created)}
            </div>
          </div>

          <div className="right col-md-3">
            <SquareButton
              onClick={(e) => this.previewDoc(history.snapshot_data)}
              content="Preview"
              title="View this SOW version "
              bsStyle={"primary"}
              className={"history-buttons"}
            />
            <SquareButton
              onClick={(e) => this.checkoutSOW(history.snapshot_data)}
              content="Edit"
              title="Edit this SOW version "
              bsStyle={"default"}
              className={"history-buttons"}
            />
          </div>
        </div>
        <div className="collapsable-contents">
          {history.revision &&
            _.get(history, "revision.meta_data.description") && (
              <div className="no-data">
                {" "}
                <span>Version Description : </span>
                {_.get(history, "revision.meta_data.description")}
              </div>
            )}
          {history.diff === null ||
            !history.diff ||
            (isEqual(history.diff, {}) && (
              <div className="no-data"> No diff data</div>
            ))}
          {history.diff && history.diff.values_changed && (
            <>
              <div className="heading-version">
                <span> {` Previous Version `} </span>
                <span>{` Version ${this.getVersion(history.revision)}`}</span>
              </div>
              {Object.keys(history.diff.values_changed)
                .sort((a, b) => (a !== b ? (a < b ? -1 : 1) : 0))
                .map((v, i) => {
                  // const version = history.diff.values_changed[v];

                  if (!this.checker(v)) {
                    return "";
                  }
                  return (
                    <div key={i} className="diff-view-section">
                      <span className="heading">{getFieldNames(v)}</span>
                      {/* <ReactDiffViewer
                        oldValue={`${this.getValueByField(
                          version.old_value,
                          v
                        )}`}
                        newValue={`${this.getValueByField(
                          version.new_value,
                          v
                        )}`}
                        splitView={true}
                        disableWordDiff={version.diff ? false : true}
                      /> */}
                    </div>
                  );
                })}
            </>
          )}
          {history.diff && history.diff.iterable_item_added && (
            <div className={"col-md-6 row"}>
              <div className="heading-version">
                <span> {`Added`} </span>
              </div>

              {history.diff &&
                history.diff.iterable_item_added &&
                Object.keys(history.diff.iterable_item_added).map((v, i) => {
                  const version = history.diff.iterable_item_added[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span>{" "}
                        {this.getValueByField(version, v)}
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
          {history.diff && history.diff.iterable_item_removed && (
            <div className={"col-md-6 row removed-section"}>
              <div className="heading-version">
                <span> {`Removed`} </span>
              </div>
              {history.diff &&
                history.diff.iterable_item_removed &&
                Object.keys(history.diff.iterable_item_removed).map((v, i) => {
                  const version = history.diff.iterable_item_removed[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span>{" "}
                        {typeof version === "object"
                          ? JSON.stringify(version)
                          : version}
                      </div>
                    </div>
                  );
                })}
            </div>
          )}
        </div>
      </div>
    );
  };

  getBody = () => {
    var prohibited = [
      `root['json_config']['service_cost']['notes']`,
      "user_id",
      `root['json_config']['project_details']['out_of_scope']['value']`,
      `root['json_config']['project_details']['project_assumptions']['value']`,
      `root['json_config']['project_details']['task_deliverables']['value']`,
      `root['json_config']['project_management_t_and_m']['project_management']['value']`,
      `root['json_config']['project_summary']['project_summary']['value']`,
      `root['json_config']['terms_t_and_m']['terms']['value']`,
      `root['json_config']['alternates']['alternates']['value']`,
      `root['json_config']['change_description']['change_description']['value']`,
      `root['json_config']['change_impact']['change_impact']['value']`,
      `root['json_config']['reason']['reason']['value']`,
      `root['json_config']['risk']['risk']['value']`,
      `root['json_config']['technical_changes']['technical_changes']['value']`,
      `markdownKey`,
    ];
    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner
            show={this.props.isFetchingHistory || this.props.isFetchingSow}
          />
        </div>
        {this.props.id && (
          <InfiniteHistory
            url={`providers/document/${this.props.id}/change-history`}
            key={"s"}
            prohibited={prohibited}
            id="OpenOrders"
            height={"calc(100% - 23px)"}
            className=""
            previewDoc={(data) => this.previewDoc(data)}
            checkoutSOW={(data) => this.checkoutSOW(data)}
          />
        )}
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`footer
        ${this.props.isFetchingHistory ? `loading` : ""}`}
      ></div>
    );
  };

  previewDoc = (obj: any) => {
    const sow = obj;
    delete sow.author_name;
    delete sow.updated_by_name;
    delete sow.update_version;
    delete sow.user_name;
    if (this.props.changeRequest) {
      sow.project = this.props.changeRequest.project;
      sow.change_request_type = this.props.changeRequest.change_request_type;
      sow.requested_by = this.props.changeRequest.requested_by;
      sow.change_number = this.props.changeRequest.change_number;
    }
    
    this.props.previewSOW(sow).then((a) => {
      if (a.type === CREATE_SOW_SUCCESS) {
        this.setState({ openPreview: true, sow: a.response });
      }
    });
  };
  checkoutSOW = (sow: any) => {
    this.setState({
      openPreview: !this.state.openPreview,
      sow,
    });
    sessionStorage.setItem("versionobj", JSON.stringify(sow));
    this.props.history.push(`/sow/sow/${sow.id}?versionobj=true`);
  };
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      sow: null,
    });
  };
  render() {
    return (
      <>
        <RightMenu
          show={this.props.show}
          onClose={this.props.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="version-history"
        />
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.sow}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  sowHistory: state.sow.sowHistory,
  isFetchingHistory: state.sow.isFetchingHistory,
  isFetchingSow: state.sow.isFetchingSow,
});

const mapDispatchToProps = (dispatch: any) => ({
  getSOWHistory: (id: any) => dispatch(getSOWHistory(id)),
  previewSOW: (sow: any) => dispatch(previewSOW(sow)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewSOWHistory)
);
