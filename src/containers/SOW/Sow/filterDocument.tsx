import React from "react";

import SquareButton from "../../../components/Button/button";
import Select from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import "../../../commonStyles/filterModal.scss";
import "./style.scss";

interface IDocumentFilterFormProps {
  show: boolean;
  onClose: () => void;
  onSubmit: (filters: IDocumentFilters) => void;
  prevFilters?: IDocumentFilters;
  customers?: any[];
  providerUsers: any[];
  singleCustomer: boolean;
}

interface IDocumentFilterFormState {
  filters: IDocumentFilters;
}

export default class DocumentFilter extends React.Component<
  IDocumentFilterFormProps,
  IDocumentFilterFormState
> {
  constructor(props: IDocumentFilterFormProps) {
    super(props);

    this.state = {
      filters: {
        customer: [],
        author: [],
        is_quote_exist: [],
      },
    };
  }

  componentDidUpdate(prevProps: IDocumentFilterFormProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }

  onClose = () => {
    this.props.onClose();
  };

  onSubmit = () => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  getBody = () => {
    const customers = this.props.customers
      ? this.props.customers.map((user) => ({
          value: user.id,
          label: user.name,
        }))
      : [];
    const providerUsers = this.props.providerUsers
      ? this.props.providerUsers.map((author, index) => ({
          value: author.id,
          label: `${author.first_name} ${author.last_name}`,
        }))
      : [];
    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Customer</label>
            <div className="field__input">
              <Select
                name="customer"
                value={filters.customer}
                onChange={this.onFilterChange}
                options={customers}
                multi={true}
                placeholder="Select Customer"
                disabled={this.props.singleCustomer}
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Author</label>
            <div className="field__input">
              <Select
                name="author"
                value={filters.author}
                onChange={this.onFilterChange}
                options={providerUsers}
                multi={true}
                placeholder="Select Author"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Opportunity</label>
            <div className="field__input">
              <Select
                name="is_quote_exist"
                value={filters.is_quote_exist}
                onChange={this.onFilterChange}
                options={[
                  { value: true, label: "Available" },
                  { value: false, label: "Not Available" },
                ]}
                multi={true}
                placeholder="Select Opportunity"
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
