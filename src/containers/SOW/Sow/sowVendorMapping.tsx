import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import "./style.scss";

interface VendorMappingModalProps {
  show: boolean;
  mapping: IVendorAliasMapping;
  closeModal: (save: boolean, mapping?: IVendorAliasMapping) => void;
}

const SowVendorMapping: React.FC<VendorMappingModalProps> = (props) => {
  const [currentMapping, setCurrentMapping] = useState<IVendorAliasMapping>({
    vendor_name: "",
    vendor_crm_id: null,
    resource_description: "",
  });
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    setCurrentMapping(props.mapping);
  }, [props.mapping]);

  const handleChangeDescription = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentMapping((prevState) => ({
      ...prevState,
      resource_description: e.target.value,
    }));
  };

  const isValid = () => {
    let valid = true;
    if (
      [
        "Engineer",
        "After Hours Engineer",
        "Project Management",
        "Integration Technician",
      ].includes(currentMapping.resource_description)
    ) {
      valid = false;
      setError(true);
    }
    return valid;
  };

  const onSave = () => {
    if (isValid()) {
      props.closeModal(true, currentMapping);
      setError(false);
    }
  };

  return (
    <ModalBase
      show={props.show}
      onClose={() => props.closeModal(false)}
      titleElement={`${
        currentMapping.id ? "Update" : "Create"
      } Vendor Description Mapping`}
      bodyElement={
        <div className="vendor-alias-edit-container">
          <Input
            field={{
              label: "Vendor Name",
              type: "TEXT",
              value: currentMapping.vendor_name,
              isRequired: true,
            }}
            className="vendor-name"
            width={6}
            multi={false}
            name="vendor_name"
            onChange={() => null}
            placeholder={`Select Vendor Name`}
            disabled={true}
          />
          <Input
            field={{
              label: "Description",
              type: "TEXT",
              value: currentMapping.resource_description,
              isRequired: true,
            }}
            className="resource-description"
            width={6}
            name="resource_description"
            onChange={handleChangeDescription}
            placeholder={`Enter Vendor Default Description`}
            error={
              error
                ? {
                    errorState: "error",
                    errorMessage:
                      "Vendor Description cannot be from the reserved keywords",
                  }
                : undefined
            }
          />
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={"default"}
            onClick={() => props.closeModal(false)}
          />
          <SquareButton
            content={"Save"}
            bsStyle={"primary"}
            onClick={onSave}
            disabled={
              !currentMapping.vendor_crm_id ||
              !currentMapping.resource_description ||
              !currentMapping.resource_description.trim()
            }
          />
        </div>
      }
      className={"vendor-mapping-modal"}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SowVendorMapping);
