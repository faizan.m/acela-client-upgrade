import React from 'react';

import SquareButton from '../../../components/Button/button';
import Select from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import "../../../commonStyles/filterModal.scss";
import './style.scss';

interface IServiceCatalogFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: IServiceCatalogFilters) => void;
  prevFilters?: IServiceCatalogFilters;
  service_type: any;
  service_category: any;
  linked_service_catalog: IserviceCatalogShort[];
}

interface IServiceCatalogFilterState {
  filters: IServiceCatalogFilters;
}

export default class ServiceCatalogFilter extends React.Component<
  IServiceCatalogFilterProps,
  IServiceCatalogFilterState
> {
  constructor(props: IServiceCatalogFilterProps) {
    super(props);

    this.state = {
      filters: {
        service_type: [],
        service_category: [],
        service_technology_types: [],
        linked_service_catalog: [],
      },
    };
  }

  componentDidUpdate(prevProps: IServiceCatalogFilterProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }

  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    })
    );
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };

  getServiceCatalogsOptions = () => {
    const serviceCatalogShortList = this.props.linked_service_catalog
      ? this.props.linked_service_catalog.map(s => ({
          value: s.id,
          label: s.service_name,
          disabled: false,
        }))
      : [];

    return serviceCatalogShortList;
  };
  getBody = () => {
    const serviceTypes = this.props.service_type
      ? this.props.service_type.map(user => ({
          value: user.id,
          label: user.name,
        }))
      : [];

    const serviceCategories = this.props.service_category
      ? this.props.service_category.map(user => ({
          value: user.id,
          label: user.name,
        }))
      : [];

    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Category</label>
            <div className="field__input">
              <Select
                name="service_type"
                value={filters.service_type}
                onChange={this.onFilterChange}
                options={serviceTypes}
                multi={true}
                placeholder="Select Category"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Service Category</label>
            <div className="field__input">
              <Select
                name="service_category"
                value={filters.service_category}
                onChange={this.onFilterChange}
                options={serviceCategories}
                multi={true}
                placeholder="Select Service Category"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Service Catalog</label>
            <div className="field__input">
              <Select
                name="service_technology_types"
                value={filters.service_technology_types}
                onChange={this.onFilterChange}
                options={this.getServiceCatalogsOptions()}
                multi={true}
                placeholder="Select Service Catalog"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Catalog Availability</label>
            <div className="field__input">
              <Select
                name="linked_service_catalog"
                value={filters.linked_service_catalog}
                onChange={this.onFilterChange}
                options={[
                  { value: 'Available', label: 'Available' },
                  { value: 'NotAvailable', label: 'Not Available' },
                ]}
                multi={true}
                placeholder="Select "
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
