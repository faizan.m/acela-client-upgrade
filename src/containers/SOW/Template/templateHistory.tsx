import React from 'react';
import Spinner from '../../../components/Spinner';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { utcToLocalInLongFormat } from '../../../utils/CalendarUtil';
import RightMenu from '../../../components/RighMenuBase/rightMenuBase';
import { getTemplateHistory, getCategoryList } from '../../../actions/sow';
import SquareButton from '../../../components/Button/button';
import { isEqual } from 'lodash';
import PDFViewer from '../../../components/PDFViewer/PDFViewer';
// import ReactDiffViewer from 'react-diff-viewer'; (New Component)
import { getFieldNames } from '../../../utils/fieldName';
import _ from 'lodash';
import "../../../commonStyles/versionHistory.scss";
import './style.scss';

interface IViewSOWHistoryProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  id: any;
  sowHistory: any;
  isFetchingHistory: any;
  getSOWHistory: any;
  getSow: any;
  isFetchingSow: boolean;
  serviceCatalogCategories: any;
  categoryList: any;
  getCategoryList: any;
}

interface IViewSOWHistoryState {
  open: boolean;
  sowHistory: any;
  isCollapsed: boolean[];
  loadMore: boolean;
  sow: any;
  openPreview: boolean;
}

class ViewSOWHistory extends React.Component<
  IViewSOWHistoryProps,
  IViewSOWHistoryState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IViewSOWHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    sowHistory: [],
    isCollapsed: [],
    loadMore: false,
    openPreview: false,
    sow: null,
  })


  componentDidMount() {
    if(!this.props.categoryList){
      this.props.getCategoryList();
    }
  }

  componentDidUpdate(prevProps: IViewSOWHistoryProps) {
    if (this.props.id && prevProps.id !== this.props.id) {
      this.props.getSOWHistory(this.props.id);
    }
    if (
      this.props.sowHistory &&
      prevProps.sowHistory !== this.props.sowHistory
    ) {
      this.setState({
        isCollapsed: Object.keys(this.props.sowHistory).map((key) => false),
      });
    }
  }

  getTitle = () => {
    return (
      <div className="text-center">
        Template Versions
      </div>
    );
  };

  onClickLoadMore = () => {
    this.setState({ loadMore: !this.state.loadMore })
  };


  getVersion = (revision: any) => {
    const version = typeof (revision.meta_data) === 'object' ? revision.meta_data.version : "N.A.";

    return version ? version : 'N.A.';
  };


  checker = (value) => {
    var prohibited = [
      `root['json_config']['service_cost']['notes']`,
      'user_id',
      `root['json_config']['project_details']['out_of_scope']['value']`,
      `root['json_config']['project_details']['project_assumptions']['value']`,
      `root['json_config']['project_details']['task_deliverables']['value']`,
      `root['json_config']['project_management_t_and_m']['project_management']['value']`,
      `root['json_config']['project_summary']['project_summary']['value']`,
      `root['json_config']['terms_t_and_m']['terms']['value']`,
      `root['is_disabled']`
    ];

    for (var i = 0; i < prohibited.length; i++) {
      if (value.indexOf(prohibited[i]) > -1) {
        return false;
      }
    }
    return true;
  }

  getValueByField = (value, field) => {
    let currentValue = value;


    if (typeof (value) === 'object') {
      currentValue = null;
      currentValue = JSON.stringify(value, null, 4)
    }

    if (typeof (value) === 'object' && field.indexOf('travels') > -1) {
      currentValue = '';
      currentValue = `cost : ${value.cost || 0} 
description : ${value.description || 'N.A.'}  `
    }
    if (typeof (value) === 'object' && field.indexOf('contractors') > -1) {
      currentValue = '';
      currentValue =
        `Name : ${value.name || 'N.A.'}, 
Customer Cost : ${value.customer_cost || 0}, 
Margin Percentage : ${value.margin_percentage || '0'}% 
Partner cost : ${value.partner_cost}  `
    }

    if (typeof (value) === 'object' && field.indexOf('implementation') > -1) {
      currentValue = ' ';

      Object.keys(value).map(v => {
        currentValue = currentValue + `${value[v].label} :${value[v].value},\n `;
      });
    }

    if (field === `root['service_catalog_category_id']`) {
      const classification = this.props.serviceCatalogCategories &&
        this.props.serviceCatalogCategories.filter(v => v.id === value);
      currentValue = classification && classification[0].name || '';
    }

    if (field === `root['category_id']`) {
      const classification = this.props.categoryList &&
        this.props.categoryList.filter(v => v.id === value);
      currentValue = classification && classification[0].name || '';
    }


    return currentValue;
  }


  renderHistoryContainer = (history: any, boardIndex: number) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];
    const toggleCollapsedState = () =>
      this.setState(prevState => ({
        isCollapsed: [
          ...prevState.isCollapsed.slice(0, boardIndex),
          !prevState.isCollapsed[boardIndex],
          ...prevState.isCollapsed.slice(boardIndex + 1),
        ],
      }));
    return (
      <div
        key={boardIndex}
        className={`collapsable-section  ${
          isCollapsed ? 'collapsable-section--collapsed' : 'collapsable-section--not-collapsed'
          }`}
      >
        <div className="col-md-12 collapsable-heading" onClick={toggleCollapsedState}>
          <div className="left col-md-9">
            <div className="name"> <span>User: </span>{history.revision.user}</div>
            <div className="version"> <span>version: </span>{this.getVersion(history.revision)}</div>
            <div className="date"> <span>Updated On: </span>{utcToLocalInLongFormat(history.revision.date_created)}</div>
          </div>

          <div className="right col-md-3">
            <SquareButton
              onClick={e => this.checkoutSOW(history.snapshot_data)}
              content="Edit"
              title="Edit this Template version "
              bsStyle={"default"}
              className={'history-buttons'}
            />
          </div>
        </div>
        <div className="collapsable-contents">
          {
            history.revision && _.get(history, 'revision.meta_data.description') && (
              <div className="no-data"> <span>Version Description  : </span>{_.get(history, 'revision.meta_data.description')}</div>
            )
          }
          {
            history.diff &&
            history.diff.values_changed &&
            <>
              <div className="heading-version">
                <span> {` Previous Version `} </span>
                <span>{` Version ${this.getVersion(history.revision)}`}</span>
              </div>
              {
                Object.keys(history.diff.values_changed)
                  .sort((a, b) => a !== b ? a < b ? -1 : 1 : 0)
                  .map((v, i) => {
                    // const version = history.diff.values_changed[v];

                    if (!this.checker(v)) {
                      return '';
                    }
                    return (
                      <div key={i} className="diff-view-section">
                        <span className="heading">{getFieldNames(v)}</span>
                        {/* <ReactDiffViewer
                          oldValue={`${this.getValueByField(version.old_value, v)}`}
                          newValue={`${this.getValueByField(version.new_value, v)}`}
                          splitView={true}
                          disableWordDiff={version.diff ? false : true}
                        /> */}
                      </div>
                    )
                  })
              }
            </>
          }
          {
            history.diff &&
            history.diff.iterable_item_added &&
            <div className={'col-md-6 row'}>

              <div className="heading-version">
                <span> {`Added`} </span>
              </div>

              {
                history.diff &&
                history.diff.iterable_item_added &&
                Object.keys(history.diff.iterable_item_added).map((v, i) => {
                  const version = history.diff.iterable_item_added[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span> {this.getValueByField(version, v)}
                      </div>
                    </div>
                  )
                })
              }
            </div>
          }
          {
            history.diff &&
            history.diff.iterable_item_removed &&
            <div className={'col-md-6 row removed-section'}>
              <div className="heading-version">
                <span> {`Removed`} </span>
              </div>
              {
                history.diff &&
                history.diff.iterable_item_removed &&
                Object.keys(history.diff.iterable_item_removed).map((v, i) => {
                  const version = history.diff.iterable_item_removed[v];
                  return (
                    <div key={i} className="iterable-view-section">
                      <div className="row-item">
                        <span>{getFieldNames(v)} : </span> {version}
                      </div>
                    </div>
                  )
                })
              }
            </div>
          }
          {
            !history.diff || isEqual(history.diff, {}) && (
              <div className="no-data"> No  diff data</div>
            )
          }
        </div>
      </div>
    );
  };


  getBody = () => {
    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingHistory || this.props.isFetchingSow} />
        </div>
        <div className="history-section heading  col-md-12">
          {
            this.props.sowHistory.length === 0 && (
              <div className="col-md-12 no-data" style={{ textAlign: "center" }}> No History available.</div>
            )
          }
          {
            this.props.sowHistory.length !== 0 &&
            !this.state.loadMore && this.props.sowHistory.slice(0, 5).map((history, i) => {
              return this.renderHistoryContainer(history, i)
            })
          }
          {
            this.props.sowHistory.length !== 0 &&
            this.state.loadMore && this.props.sowHistory.map((history, i) => {
              return this.renderHistoryContainer(history, i)
            })
          }
          {
            this.props.sowHistory.length > 5 && (
              <div className="col-md-12 show-more" onClick={e => this.onClickLoadMore()} style={{ textAlign: "center" }}> {!this.state.loadMore ? 'load more...' : 'show less'}</div>
            )
          }
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`footer
        ${this.props.isFetchingHistory ? `loading` : ''}`}
      ></div>
    );
  };

  checkoutSOW = (sow: any) => {
    this.setState({
      openPreview: !this.state.openPreview,
      sow,
    });
    sessionStorage.setItem('versionobj', JSON.stringify(sow));
    this.props.history.push(`/sow/template/${sow.id}?versionobj=true`);
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      sow: null,
    });
  };

  render() {
    return (
      <>
        <RightMenu
          show={this.props.show}
          onClose={this.props.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="version-history"
        />
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.sow}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </>
    );
  }
}


const mapStateToProps = (state: IReduxStore) => ({
  sowHistory: state.sow.sowHistory,
  isFetchingHistory: state.sow.isFetchingHistory,
  isFetchingSow: state.sow.isFetchingSow,
  serviceCatalogCategories: state.sow.serviceCatalogCategories,
  categoryList: state.sow.categoryList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getSOWHistory: (id: any) => dispatch(getTemplateHistory(id)),
  getCategoryList: () => dispatch(getCategoryList()),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ViewSOWHistory)
);
