import React from 'react';
import Spinner from '../../../components/Spinner';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import RightMenu from '../../../components/RighMenuBase/rightMenuBase';
import { previewSOW, getCreatedTemplateCDHistory, getDeletedTemplateCDHistory, revertDeletedTemplate } from '../../../actions/sow';
import _, { cloneDeep } from 'lodash';
import Table from '../../../components/Table/table';
import { utcToLocalInLongFormat } from '../../../utils/CalendarUtil';
import ViewTemplate from './templateDetails';
import DeleteButton from '../../../components/Button/deleteButton';
import IconButton from '../../../components/Button/iconButton';
import "../../../commonStyles/versionHistory.scss";
import './style.scss';

interface IViewTemplateCDHistoryProps extends ICommonProps {
  show: boolean;
  onClose: (e: any, reverted?: boolean) => void;
  sowCreateHistory: any;
  isFetchingCreateHistory: boolean;
  sowDeleteHistory: any;
  isFetchingDeleteHistory: boolean;
  getCreatedSOWHistory: any;
  getDeletedSOWHistory: any
  previewSOW: any;
  isFetchingSow: boolean;
  revertDeletedTemplate: any;
}

interface IViewTemplateCDHistoryState {
  open: boolean;
  sowCreateHistory: any[];
  sowDeleteHistory: any[];
  isCollapsedCreate: boolean;
  isCollapsedDelete: boolean;
  sow: any;
  openPreview: boolean;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  paginationDelete: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  createHistoryRows: any[];
  deleteHistoryRows: any[];
  viewTemplate: boolean;
  templateDetails: any;
  reverted: boolean;
}

class ViewTemplateCDHistory extends React.Component<
  IViewTemplateCDHistoryProps,
  IViewTemplateCDHistoryState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IViewTemplateCDHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    sowCreateHistory: [],
    sowDeleteHistory: [],
    isCollapsedCreate: false,
    isCollapsedDelete: true,
    openPreview: false,
    sow: null,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    paginationDelete: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    id: '',
    createHistoryRows: [],
    deleteHistoryRows: [],
    viewTemplate: false,
    templateDetails: null,
    reverted: false,
  })

  componentDidUpdate(prevProps: IViewTemplateCDHistoryProps) {
    if (
      this.props.sowCreateHistory &&
      prevProps.sowCreateHistory !== this.props.sowCreateHistory
    ) {
      this.setRows(this.props);
    }
    if (
      this.props.sowDeleteHistory &&
      prevProps.sowDeleteHistory !== this.props.sowDeleteHistory
    ) {
      this.setRowsDelete(this.props);
    }
  }

  setRows = (nextProps: IViewTemplateCDHistoryProps) => {
    const sowCreateHistory = nextProps.sowCreateHistory;
    const history: any[] = sowCreateHistory.results;
    const createHistoryRows: any[] = history && history.map(
      (row, index) => ({
        id: row.id,
        name: `${_.get(row, 'snapshot_data.name')}`,
        date_created: _.get(row, 'revision.date_created'),
        index,
        snapshot_data: _.get(row, 'snapshot_data'),
        user: `${_.get(row, 'revision.user', '-')}`,
      })
    );

    this.setState(prevState => ({
      createHistoryRows,
      sowCreateHistory,
      pagination: {
        ...prevState.pagination,
        totalRows: sowCreateHistory.count,
        currentPage: sowCreateHistory.links.page_number - 1,
        totalPages: Math.ceil(
          sowCreateHistory.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  setRowsDelete = (nextProps: IViewTemplateCDHistoryProps) => {
    const sowDeleteHistory = nextProps.sowDeleteHistory;
    const history: any[] = sowDeleteHistory.results;
    const deleteHistoryRows: any[] = history && history.map(
      (row, index) => ({
        id: row.id,
        name: `${_.get(row, 'snapshot_data.name')}`,
        date_created: _.get(row, 'revision.date_created'),
        user: `${_.get(row, 'revision.user', '-')}`,
        index,
        snapshot_data: _.get(row, 'snapshot_data'),
      })
    );

    this.setState(prevState => ({
      deleteHistoryRows,
      sowDeleteHistory,
      pagination: {
        ...prevState.pagination,
        totalRows: sowDeleteHistory.count,
        currentPage: sowDeleteHistory.links.page_number - 1,
        totalPages: Math.ceil(
          sowDeleteHistory.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  getTitle = () => {
    return (
      <div className="text-center">
        Created & Deleted Template History
      </div>
    );
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getCreatedSOWHistory(newParams);
  };
  // Server side searching, sorting, ordering
  fetchDataDelete = (params: IServerPaginationParams) => {
    const prevParams = this.state.paginationDelete.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      paginationDelete: {
        ...prevState.paginationDelete,
        params: newParams,
      },
    }));

    this.props.getDeletedSOWHistory(newParams);
  };

  toggleViewTemplatePopup = () => {
    this.setState({
      viewTemplate: !this.state.viewTemplate,
    });
  };
  checker = (value) => {
    var prohibited = ['notesMD', 'value_markdown', 'user_id'];

    for (var i = 0; i < prohibited.length; i++) {
      if (value.indexOf(prohibited[i]) > -1) {
        return false;
      }
    }
    return true;
  }

  getValueByField = (value, field) => {
    let currentValue = value;


    if (typeof (value) === 'object') {
      currentValue = null;
      currentValue = JSON.stringify(value, null, 4)
    }

    if (typeof (value) === 'object' && field.indexOf('implementation') > -1) {
      currentValue = [];

      Object.keys(value).map(v => {
        currentValue.push(_.pick(value[v], ['label', 'value']));
      })

      currentValue = JSON.stringify(currentValue, null, 4).split(',').join("\r\n")
    }


    return currentValue;
  }
  toggleCollapsedState = (data: any) => {
    const newState = cloneDeep(this.state);
    (newState[data] as any) = !this.state[data];
    this.setState(newState);
  }


  renderHistoryContainer = () => {

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };
    const manualPropsDelete = {
      manual: true,
      pages: this.state.paginationDelete.totalPages,
      onFetchData: this.fetchDataDelete,
    };
    const columns: ITableColumn[] = [
      {
        accessor: 'name',
        Header: 'Name',
        sortable: false,
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: 'date_created', Header: 'Created On',
        sortable: false,
        Cell: name => <div className="pl-15">{utcToLocalInLongFormat(name.value)}</div>,
      },
      {
        accessor: 'status',
        Header: 'View',
        sortable: false,
        Cell: data => (
          <DeleteButton
            type="pdf_preview"
            title="View"
            onClick={e => this.previewDoc(data.original.snapshot_data)}
          />),
      },

    ];
    const columnsDeleted: ITableColumn[] = [
      {
        accessor: 'name',
        Header: 'Name',
        sortable: false,
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: 'date_created', Header: 'Deleted On',
        sortable: false,
        Cell: name => <div className="pl-15">{utcToLocalInLongFormat(name.value)}</div>,
      },
      {
        accessor: 'status',
        Header: 'Action',
        sortable: false,
        Cell: data => (
          <>
            <IconButton
              icon="revert.svg"
              title="Revert Template"
              onClick={e => this.revertTemplate(data.original, e)}
            />
            <DeleteButton
              type="pdf_preview"
              title="View"
              onClick={e => this.previewDoc(data.original.snapshot_data)}
            />

          </>
        ),
      },

    ];
    return (
      <>
        <div
          key={0}
          className={`collapsable-section  ${
            this.state.isCollapsedCreate ? 'collapsable-section--collapsed' : 'collapsable-section--not-collapsed'
            }`}
        >
          <div className="col-md-12 collapsable-heading" onClick={e => this.toggleCollapsedState('isCollapsedCreate')}>
            <div className="left col-md-9">Created Template's</div>
          </div>
          <div className="collapsable-contents">
            <Table
              manualProps={manualProps}
              columns={columns}
              rows={this.state.createHistoryRows}
              className={`provider-users-listing__table ${
                this.props.isFetchingCreateHistory ? `loading` : ``
                }`}
              loading={this.props.isFetchingCreateHistory}
            />
          </div>
        </div>
        <div
          key={1}
          className={`collapsable-section  ${
            this.state.isCollapsedDelete ? 'collapsable-section--collapsed' : 'collapsable-section--not-collapsed'
            }`}
        >
          <div className="col-md-12 collapsable-heading" onClick={e => this.toggleCollapsedState('isCollapsedDelete')}>
            <div className="left col-md-9">Deleted Template's</div>
          </div>
          <div className="collapsable-contents">
            <Table
              manualProps={manualPropsDelete}
              columns={columnsDeleted}
              rows={this.state.deleteHistoryRows}
              className={`provider-users-listing__table ${
                this.props.isFetchingDeleteHistory ? `loading` : ``
                }`}
              loading={this.props.isFetchingDeleteHistory}
            />
          </div>
        </div>
      </>
    );
  };


  getBody = () => {

    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetchingCreateHistory || this.props.isFetchingSow} />
        </div>
        <div className="history-section heading  col-md-12">
          {this.renderHistoryContainer()}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return <div className={`footer`}></div>;
  };

  revertTemplate = (sow: any, e) => {
    this.setState({
      openPreview: !this.state.openPreview,
      sow,
    });
    this.props.revertDeletedTemplate(sow.id).then(action => {
      if (action.type === 'FETCH_TEMPLATE_SUCCESS') {
        this.setState({ reverted: true });
        this.fetchDataDelete(this.state.pagination.params)
      }
    });
  };

  previewDoc = (obj: any) => {
    const sow = obj;
    this.setState({
      viewTemplate: true,
      templateDetails: sow
    });
  };

  toggleOpenPreview = () => {
    this.setState({
      viewTemplate: false,
      templateDetails: null,
    });
  };
  render() {
    return (
      <>
        {
          this.props.show &&
          <RightMenu
            show={this.props.show}
            onClose={e => this.props.onClose(e, this.state.reverted)}
            titleElement={this.getTitle()}
            bodyElement={this.getBody()}
            footerElement={this.getFooter()}
            className="version-history"
          />
        }

        {this.state.viewTemplate &&
          <ViewTemplate
            show={this.state.viewTemplate}
            templateDetails={this.state.templateDetails}
            {...this.props}
            onClose={this.toggleOpenPreview}
          />
        }
      </>
    );
  }
}


const mapStateToProps = (state: IReduxStore) => ({
  sowCreateHistory: state.sow.sowCreateHistory,
  isFetchingCreateHistory: state.sow.isFetchingCreateHistory,
  sowDeleteHistory: state.sow.sowDeleteHistory,
  isFetchingDeleteHistory: state.sow.isFetchingDeleteHistory,
  isFetchingSow: state.sow.isFetchingSow
});

const mapDispatchToProps = (dispatch: any) => ({
  getCreatedSOWHistory: (params?: IServerPaginationParams) => dispatch(getCreatedTemplateCDHistory(params)),
  getDeletedSOWHistory: (params?: IServerPaginationParams) => dispatch(getDeletedTemplateCDHistory(params)),
  previewSOW: (sow: any) => dispatch(previewSOW(sow)),
  revertDeletedTemplate: (id: number) => dispatch(revertDeletedTemplate(id))
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(ViewTemplateCDHistory)
);
