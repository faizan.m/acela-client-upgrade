import React from "react";
import { connect } from "react-redux";
import { debounce } from "lodash";

import EditButton from "../../../components/Button/editButton";
import Select from "../../../components/Input/Select/select";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import { fetchServiceTypeSTT } from "../../../actions/setting";
import {
  deleteserviceCatalog,
  EDIT_SCL_SUCCESS,
  fetchServiceCategoriesFull,
  getserviceCatalogList,
} from "../../../actions/sow";
import SquareButton from "../../../components/Button/button";
import DeleteButton from "../../../components/Button/deleteButton";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import ServiceCatalogFilter from "../ServiceCatalog/filterServiceCatalog";
import AddServiceCatalog from "./addServiceCatalog";
import ServiceCatalogDetails from "./details";
import "./style.scss";

interface IserviceCatalogListProps extends ICommonProps {
  serviceCatalogList: any;
  isFetchingSCList: boolean;
  isFetchingSC: boolean;
  isFetching: boolean;
  isFetchingType: boolean;
  getserviceCatalogList: any;
  deleteserviceCatalog: any;
  fetchServiceCatalog: any;
  fetchServiceCatalogCategories: any;
  serviceCatalogCategories: any;
  serviceType: any;
  fetchServiceCategoriesFull: any;
  fetchServiceTypeSTT: any;
}

interface IserviceCatalogListtate {
  errorList?: any;
  isserviceCatalogPosting: boolean;
  isopenConfirm: boolean;
  id: string;
  rows: any;
  serviceCatalogList: any;
  downloadingIds: number[];
  searchString: string;
  filters: IServiceCatalogFilters;
  isFilterModalOpen: boolean;
  customerLabelIds: { [id: number]: string };
  authorLabelIds: { [id: number]: string };
  showOlderserviceCatalog: boolean;
  isAddServiceCatalogModalOpen: boolean;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  isDetailVisible: boolean;
  detailRow: any;
  reset: boolean;
}

class ServiceCatalogListing extends React.Component<
  IserviceCatalogListProps,
  IserviceCatalogListtate
> {
  private debouncedFetch;

  constructor(props: IserviceCatalogListProps) {
    super(props);

    this.state = {
      rows: [],
      serviceCatalogList: [],
      isserviceCatalogPosting: false,
      isopenConfirm: false,
      id: "",
      downloadingIds: [],
      searchString: "",
      isFilterModalOpen: false,
      filters: {
        service_type: [],
        service_category: [],
        service_technology_types: [],
        is_template_present: "",
      },
      customerLabelIds: {},
      authorLabelIds: {},
      showOlderserviceCatalog: false,
      isAddServiceCatalogModalOpen: false,
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      isDetailVisible: false,
      detailRow: null,
      reset: false,
    };
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  componentDidMount() {
    this.props.fetchServiceCategoriesFull();
    this.props.fetchServiceTypeSTT();
  }

  componentDidUpdate(prevProps: IserviceCatalogListProps) {
    if (
      this.props.serviceCatalogList &&
      prevProps.serviceCatalogList !== this.props.serviceCatalogList
    ) {
      this.setRows(this.props);
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getserviceCatalogList(newParams);
  };
  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: IserviceCatalogListProps) => {
    const customersResponse = nextProps.serviceCatalogList;
    const serviceCatalogList: any[] = customersResponse.results;
    const rows: any[] = serviceCatalogList.map((catalog, index) => ({
      service_name: catalog.service_name ? catalog.service_name : "N.A.",
      service_type: catalog.service_type.name,
      service_type_id: catalog.service_type.id,
      service_category: catalog.service_category.name,
      service_category_id: catalog.service_category.id,
      service_technology_types: catalog.service_technology_types,
      linked_template: catalog.linked_template,
      linked_template_name: catalog.linked_template_name,
      updated_on: catalog.updated_on ? catalog.updated_on : "N.A.",
      created_on: catalog.created_on ? catalog.created_on : "N.A.",
      notes: catalog.notes,
      id: catalog.id,
      index,
    }));

    this.setState((prevState) => ({
      reset: false,
      rows,
      serviceCatalogList,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onEditRowClick = (id: any, event: any) => {
    event.stopPropagation();
    this.setState({ id, isAddServiceCatalogModalOpen: true });
  };

  onRowClick = (rowInfo) => {
    event.stopPropagation();
    this.setState({ isDetailVisible: true, detailRow: rowInfo.original.index });
  };

  getserviceCatalogDetails = () => {
    const catalog = this.props.serviceCatalogList.results[this.state.detailRow];
    const types = this.getAllTechnoType(true);
    const techNoTypes = [];
    catalog.service_technology_types.map((t) =>
      techNoTypes.push(types && types.filter((ty) => t === ty.value))
    );

    const typeNames = [];
    techNoTypes.map((techType, i) =>
      typeNames.push(techType && techType[0] && techType[0].label)
    );
    catalog.service_technology_types_names = typeNames;

    return catalog as IserviceCatalog;
  };

  renderTopBar = () => {
    return (
      <div className="service-catalog-listing__actions">
        <div className="row-action header-panel">
          <div className="left">
            <Input
              field={{
                value: this.state.pagination.params.search,
                label: "",
                type: "SEARCH",
              }}
              width={11}
              name="searchString"
              onChange={this.onSearchStringChange}
              placeholder="Search"
              className="search"
            />
          </div>
          <div className="field-section actions-right">
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.serviceCatalogList &&
                this.props.serviceCatalogList.length === 0
              }
              bsStyle={"primary"}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };
  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          [targetName]: targetValue,
        },
      }),
      () => {
        this.setState((prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              service_type: prevState.filters.service_type.join(),
              service_category: prevState.filters.service_category.join(),
              // tslint:disable-next-line: max-line-length
              service_technology_types: prevState.filters.service_technology_types.join(),
              // tslint:disable-next-line: max-line-length
              is_template_present: this.getIsTemplate(
                prevState.filters.is_template_present
              ),
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  getAllTechnoType = (short: boolean = false) => {
    const types1 = [];

    if (this.props.serviceType) {
      this.props.serviceType.map(
        (cat) =>
          cat.technology_types &&
          cat.technology_types.map((t) =>
            types1.push({
              value: t.id,
              label: short ? `${t.name}` : `${cat.name}: ${t.name}`,
              disabled: false,
            })
          )
      );
    }

    return types1;
  };
  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      filters.service_type.length > 0 ||
      filters.service_category.length > 0 ||
      filters.service_technology_types.length > 0 ||
      filters.is_template_present.length > 0
        ? true
        : false;
    const serviceTypes = this.props.serviceType
      ? this.props.serviceType.map((user) => ({
          value: user.id,
          label: user.name,
        }))
      : [];

    const serviceCategories = this.props.serviceCatalogCategories
      ? this.props.serviceCatalogCategories.map((user) => ({
          value: user.id,
          label: user.name,
        }))
      : [];

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.service_type.length > 0 && (
          <div className="section-show-filters">
            <label>Service Type: </label>
            <Select
              name="service_type"
              value={filters.service_type}
              onChange={this.onFilterChange}
              options={serviceTypes}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.service_category.length > 0 && (
          <div className="section-show-filters">
            <label>Service Category: </label>
            <Select
              name="service_category"
              value={filters.service_category}
              onChange={this.onFilterChange}
              options={serviceCategories}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.service_technology_types.length > 0 && (
          // tslint:disable-next-line: max-line-length
          <div className="section-show-filters service_technology_types_filter-1">
            <label>Service Technology Types: </label>
            <Select
              name="service_technology_types"
              value={filters.service_technology_types}
              onChange={this.onFilterChange}
              options={this.getAllTechnoType()}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {filters.is_template_present.length > 0 && (
          <div className="section-show-filters">
            <label>Template: </label>
            <Select
              name="is_template_present"
              value={filters.is_template_present}
              onChange={this.onFilterChange}
              options={[
                { value: "Available", label: "Available" },
                { value: "NotAvailable", label: "Not Available" },
              ]}
              multi={true}
              placeholder="Select"
            />
          </div>
        )}
      </div>
    ) : null;
  };
  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteserviceCatalog(this.state.id).then((action) => {
      if (action.type === EDIT_SCL_SUCCESS) {
        this.setState({
          reset: true,
        });
        this.debouncedFetch({
          page: 1,
        });
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IServiceCatalogFilters) => {
    if (filters) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            service_type: filters.service_type.join(),
            service_category: filters.service_category.join(),
            service_technology_types: filters.service_technology_types.join(),
            // tslint:disable-next-line: max-line-length
            is_template_present: this.getIsTemplate(
              filters.is_template_present
            ),
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal();
    }
  };

  getIsTemplate = (isTemplate) => {
    let isTemp = "";
    if (isTemplate.includes("Available")) {
      isTemp = "2";
    }
    if (isTemplate.includes("NotAvailable")) {
      isTemp = "3";
    }
    if (isTemplate.length === 2) {
      isTemp = "";
    }

    return isTemp;
  };

  toggleAddServiceCatalogModal = () => {
    this.setState((prevState) => ({
      isAddServiceCatalogModalOpen: !prevState.isAddServiceCatalogModalOpen,
      id: "",
    }));
  };

  toggleSubmitServiceCatalogModal = () => {
    this.setState((prevState) => ({
      isAddServiceCatalogModalOpen: false,
      id: "",
      reset: true,
    }));
    this.debouncedFetch({
      page: 1,
    });
  };

  closeDetailModal = () => {
    this.setState({ isDetailVisible: false, detailRow: null });
  };

  onCreateSOWClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/sow/0?template=${id}`);
  };

  render() {
    const columns: ITableColumn[] = [
      {
        accessor: "type",
        Header: "Template & Type",
        id: "type",
        width: 120,
        sortable: false,
        Cell: (cell) => (
          <div className="icons-template">
            {cell.original.linked_template && (
              <img
                src={`/assets/icons/verified.svg`}
                className="left"
                alt=""
                title={"Available"}
              />
            )}
            {!cell.original.linked_template && (
              <img
                src={`/assets/icons/cancel.svg`}
                className="left"
                alt=""
                title={"Not Available"}
              />
            )}
            {cell.original.service_type === "Collaboration" && (
              <img
                title="Collaboration"
                alt=""
                src="/assets/icons/collabHomeHover.webp"
                className="left"
              />
            )}
            {cell.original.service_type === "Networking" && (
              <img
                title="Networking"
                alt=""
                src="/assets/icons/NetworkingHomeHover.webp"
                className="left"
              />
            )}
            {cell.original.service_type === "Data Center" && (
              <img
                title="Data Center"
                alt=""
                src="/assets/icons/lifecycle.webp"
                className="left"
              />
            )}
            {cell.original.service_type === "Security" && (
              <img
                title="Security"
                alt=""
                src="/assets/icons/SecurityHomeHover.webp"
                className="left"
              />
            )}
          </div>
        ),
      },
      {
        accessor: "service_name",
        Header: "Service Name",
        id: "service_name",
        sortable: true,
        Cell: (c) => <div>{c.value}</div>,
      },
      {
        accessor: "service_category",
        Header: "Service Category",
        sortable: true,
        id: "service_category__name",
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: "created_on",
        Header: "Created On",
        id: "created_on",
        width: 140,
        sortable: true,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "updated_on",
        Header: "Updated On",
        id: "updated_on",
        width: 140,
        sortable: true,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        width: 140,
        sortable: false,
        Cell: (cell) => (
          <div>
            <EditButton onClick={(e) => this.onEditRowClick(cell.value, e)} />
            <DeleteButton onClick={(e) => this.onDeleteRowClick(cell, e)} />
            {cell.original.linked_template && (
              <DeleteButton
                type="create"
                title="Create SOW"
                onClick={(e) =>
                  this.onCreateSOWClick(cell.original.linked_template, e)
                }
              />
            )}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };

    return (
      <div className="service-catalog-listing">
        <div className="header">
          <h3>Service Catalog</h3>
          <SquareButton
            content="Add Service"
            onClick={this.toggleAddServiceCatalogModal}
            className="save-mapping"
            bsStyle={"primary"}
          />
        </div>
        <div className="loader">
          <Spinner
            show={this.props.isFetchingSCList || this.props.isFetchingSC}
          />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetchingSCList ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingSCList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
        />
        <ServiceCatalogFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          service_type={this.props.serviceType}
          service_category={this.props.serviceCatalogCategories}
          prevFilters={this.state.filters}
        />
        {this.state.isAddServiceCatalogModalOpen && (
          <AddServiceCatalog
            show={this.state.isAddServiceCatalogModalOpen}
            onClose={this.toggleAddServiceCatalogModal}
            onSubmitClose={this.toggleSubmitServiceCatalogModal}
            id={this.state.id}
            {...this.props}
          />
        )}
        {this.state.isDetailVisible && (
          <ServiceCatalogDetails
            isVisible={this.state.isDetailVisible}
            close={this.closeDetailModal}
            serviceCatalog={this.getserviceCatalogDetails()}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  serviceCatalogList: state.sow.serviceCatalogList,
  isFetchingSCList: state.sow.isFetchingSCList,
  isFetchingSC: state.sow.isFetchingSC,
  serviceType: state.setting.serviceType,
  serviceCatalogCategories: state.sow.serviceCatalogCategories,
});

const mapDispatchToProps = (dispatch: any) => ({
  getserviceCatalogList: (params?: IServerPaginationParams) =>
    dispatch(getserviceCatalogList(params)),
  deleteserviceCatalog: (id: any) => dispatch(deleteserviceCatalog(id)),
  fetchServiceCategoriesFull: () => dispatch(fetchServiceCategoriesFull()),
  fetchServiceTypeSTT: () => dispatch(fetchServiceTypeSTT()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceCatalogListing);
