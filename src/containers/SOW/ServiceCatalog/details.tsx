import React, { Component } from 'react';
import SquareButton from '../../../components/Button/button';
import ModalBase from '../../../components/ModalBase/modalBase';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';
import './style.scss';
interface IserviceCatalogProps {
  close: (e: any) => void;
  serviceCatalog: IserviceCatalog;
  isVisible: boolean;
}

export default class ServiceCatalogDetails extends Component<
  IserviceCatalogProps
> {
  constructor(props: IserviceCatalogProps) {
    super(props);
  }

  renderBody = () => {
    const {
      service_type,
      service_category,
      service_name,
      service_technology_types,
      created_on,
      updated_on,
      linked_template_name,
      service_technology_types_names,
      notes,
    } = this.props.serviceCatalog;

    return (
      <div className="detail__body">
        <div className="body">
          <div className="field">
            <p className="heading">NAME</p>
            <p>{`${service_name}`}</p>
          </div>{' '}
          <div className="field">
            <p className="heading">service type</p>
            <p>{`${service_type.name}`}</p>
          </div>
          <div className="field">
            <p className="heading">service category</p>
            <p>{service_category ? `${service_category.name}` : `N/A`}</p>
          </div>
          <div className="field">
            <p className="heading">service technology types</p>
            <p>
              {service_technology_types && service_technology_types.length > 0
                ? `${service_technology_types_names.join(', ')}`
                : `N/A`}
            </p>
          </div>
          <div className="field">
            <p className="heading">created on</p>
            <p>{created_on && fromISOStringToFormattedDate(created_on)}</p>
          </div>
          <div className="field">
            <p className="heading">updated on</p>
            <p>{updated_on && fromISOStringToFormattedDate(updated_on)}</p>
          </div>
          <div className="field">
            <p className="heading">linked template</p>
            <p>{linked_template_name ? `${linked_template_name}` : `N/A`}</p>
          </div>
          <div className="field">
            <p className="heading">Note</p>
            <p>{notes ? `${notes}` : `N/A`}</p>
          </div>
        </div>
        <div />
      </div>
    );
  };

  renderFooter = () => {
    return (
      <SquareButton
        content="Close"
        bsStyle={"primary"}
        onClick={this.props.close}
      />
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Service Catalog Details"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="service-catalog-detail-container"
      />
    );
  }
}
