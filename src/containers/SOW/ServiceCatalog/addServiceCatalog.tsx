import _cloneDeep from 'lodash/cloneDeep';
import React from 'react';
import { connect } from 'react-redux';

// tslint:disable-next-line:no-implicit-dependencies
import { cloneDeep } from 'lodash';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import { fetchServiceTypeSTT } from '../../../actions/setting';
import {
  CREATE_SCL_FAILURE,
  CREATE_SCL_SUCCESS,
  createServiceCatalog,
  EDIT_SCL_FAILURE,
  EDIT_SCL_SUCCESS,
  fetchServiceCategoriesFull,
  getserviceCatalog,
  getTemplateList,
  updateServiceCatalog,
} from '../../../actions/sow';
import SquareButton from '../../../components/Button/button';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import Spinner from '../../../components/Spinner';
import 'easymde/dist/easymde.min.css';
import './style.scss';

interface IAddServiceCatalogProps extends ICommonProps {
  fetchServiceCategoriesFull: any;
  fetchServiceTypeSTT: any;
  getserviceCatalog: any;
  createServiceCatalog: any;
  updateServiceCatalog: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  isFetching: boolean;
  isFetchingType: boolean;
  isFetchingSC: boolean;
  serviceCatalog: IserviceCatalog;
  serviceCatalogCategories: any;
  serviceType: any;
  show: boolean;
  onClose: (e: any) => void;
  onSubmitClose: (e: any) => void;
  id: any;
  getTemplateList: any;
  templates: any[];
  isFetchingTemplates: boolean;
}

interface IAddServiceCatalogState {
  errorList?: any;
  serviceCatalog: IserviceCatalog;
  error: {
    service_type: IFieldValidation;
    service_category: IFieldValidation;
    service_name: IFieldValidation;
    service_technology_types: IFieldValidation;
    linked_template: IFieldValidation;
  };
  id?: string;
  technologyTypes: any[];
  templates: any[];
  loading: boolean;
}

class AddServiceCatalog extends React.Component<
  IAddServiceCatalogProps,
  IAddServiceCatalogState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IAddServiceCatalogProps) {
    super(props);
    this.state = this.getEmptyState();
  }
  getEmptyState = () => ({
    serviceCatalog: {
      service_type: '',
      service_category: '',
      service_name: '',
      service_technology_types: [],
      is_template_present: false,
      is_disabled: false,
      linked_template: '',
      notes: '',
    },
    error: {
      service_type: { ...AddServiceCatalog.emptyErrorState },
      service_category: { ...AddServiceCatalog.emptyErrorState },
      service_name: { ...AddServiceCatalog.emptyErrorState },
      service_technology_types: { ...AddServiceCatalog.emptyErrorState },
      linked_template: { ...AddServiceCatalog.emptyErrorState },
    },
    loading: false,
    technologyTypes: [],
    templates: [],
  });

  componentDidMount() {
    this.props.fetchServiceCategoriesFull();
    this.props.fetchServiceTypeSTT();
    if (this.props.id) {
      this.props.getserviceCatalog(this.props.id);
    }
    this.props.getTemplateList();
  }

  componentDidUpdate(prevProps: IAddServiceCatalogProps) {
    if (
      this.props.serviceCatalog &&
      this.props.serviceCatalog !== prevProps.serviceCatalog
    ) {
      const sc = this.getEmptyState().serviceCatalog as any;
      sc.service_category = this.props.serviceCatalog.service_category.id;
      sc.service_type = this.props.serviceCatalog.service_type.id;
      sc.service_name = this.props.serviceCatalog.service_name;
      sc.linked_template = this.props.serviceCatalog.linked_template;
      sc.id = this.props.serviceCatalog.id;
      sc.notes = this.props.serviceCatalog.notes;
      sc.service_technology_types = this.props.serviceCatalog.service_technology_types;
      let types = [];
      if (
        this.props.serviceCatalog.service_type.id !== "" &&
        this.props.serviceType
      ) {
        const types1 = this.props.serviceType.filter(
          (type) => this.props.serviceCatalog.service_type.id === type.id
        );

        types =
          types1 &&
          types1[0].technology_types.map((t) => ({
            value: t.id,
            label: t.name,
            disabled: t.is_disabled,
          }));
      }
      this.setState({
        serviceCatalog: sc,
        technologyTypes: types,
      });
    }
    if (
      this.props.serviceType &&
      this.props.serviceType !== prevProps.serviceType &&
      prevProps.id &&
      prevProps.serviceCatalog
    ) {
      let types = [];
      if (
        prevProps.serviceCatalog.service_type.id !== "" &&
        this.props.serviceType
      ) {
        const types1 = this.props.serviceType.filter(
          (type) => prevProps.serviceCatalog.service_type.id === type.id
        );

        types =
          types1 &&
          types1[0].technology_types.map((t) => ({
            value: t.id,
            label: t.name,
            disabled: t.is_disabled,
          }));
      }
      this.setState({
        technologyTypes: types,
      });
    }
  }

  setValidationErrors = errorList => {
    const error = this.getEmptyState().error;
    const serviceCatalog = cloneDeep(this.state.serviceCatalog);

    try {
      Object.keys(errorList).map(key => {
        error[key].errorState = "error";
        error[key].errorMessage = errorList[key];
      });
      this.setState({ serviceCatalog, error });
    } catch (e) {
      console.info(e);
    }
  };

  getCategoryList = () => {
    const serviceCatalogCategories = this.props.serviceCatalogCategories
      ? this.props.serviceCatalogCategories.map(t => ({
          value: t.id,
          label: t.name,
          disabled: t.is_disabled,
        }))
      : [];

    return serviceCatalogCategories;
  };

  handleChange = e => {
    const newState = cloneDeep(this.state);
    newState.serviceCatalog[e.target.name] = e.target.value;
    this.setState(newState);
  };

  handleChangeServiceType = e => {
    const newState = cloneDeep(this.state);
    newState.serviceCatalog.service_type = e.target.value;
    let types = [];
    if (e.target.value !== '' && this.props.serviceType) {
      const types1 = this.props.serviceType.filter(
        type => e.target.value === type.id
      );

      types =
        types1 &&
        types1[0].technology_types.map(t => ({
          value: t.id,
          label: t.name,
          disabled: t.is_disabled,
        }));
    }
    (newState.technologyTypes as any) = types;
    newState.serviceCatalog.linked_template = '';
    newState.serviceCatalog.service_technology_types = [];
    this.setState(newState);
  };

  handleChangeServiceTechTypes = e => {
    const newState = cloneDeep(this.state);
    newState.serviceCatalog.service_technology_types = e.target.value;
    this.setState(newState);
  };

  handleChangeCategory = e => {
    const newState = cloneDeep(this.state);
    newState.serviceCatalog[e.target.name] = e.target.value;
    newState.serviceCatalog.linked_template = null;
    const templates = this.props.templates.filter(
      id => e.target.value === id.category
    );
    (newState.templates as any) = templates;
    newState.serviceCatalog.linked_template = '';
    this.setState(newState);
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Save"
          bsStyle={"primary"}
        />
      </div>
    );
  };
  onClose = e => {
    this.props.onClose(e);
  };

  onSubmitClose = e => {
    this.props.onSubmitClose(e);
  };
  onSubmit = e => {
    if (this.isValid()) {
      this.setState({loading: true});
      const serviceCatalog = _cloneDeep(this.state).serviceCatalog;
      if (
        !serviceCatalog.linked_template ||
        serviceCatalog.linked_template === 'null'
      ) {
        serviceCatalog.linked_template = null;
      }
      if (this.props.id) {
        this.props
          .updateServiceCatalog(this.props.id, serviceCatalog)
          .then(action => {
            if (action.type === EDIT_SCL_SUCCESS) {
              this.onSubmitClose(e);
            }
            if (action.type === EDIT_SCL_FAILURE) {
              this.setValidationErrors(action.errorList.data);
            }
            this.setState({loading: false});
          });
      } else {
        this.props.createServiceCatalog(serviceCatalog).then(action => {
          if (action.type === CREATE_SCL_SUCCESS) {
            this.onSubmitClose(e);
          }
          if (action.type === CREATE_SCL_FAILURE) {
            this.setValidationErrors(action.errorList.data);
          }
          this.setState({loading: false});
        });
      }
    }
  };
  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.serviceCatalog.service_name ||
      this.state.serviceCatalog.service_name.trim().length === 0
    ) {
      error.service_name.errorState = "error";
      error.service_name.errorMessage = 'Enter a valid  name';

      isValid = false;
    } else if (this.state.serviceCatalog.service_name.length > 300) {
      error.service_name.errorState = "error";
      error.service_name.errorMessage = 'Name should be less than 300 chars.';

      isValid = false;
    }

    if (!this.state.serviceCatalog.service_category) {
      error.service_category.errorState = "error";
      error.service_category.errorMessage = 'Please select category';

      isValid = false;
    }

    if (!this.state.serviceCatalog.service_type) {
      error.service_type.errorState = "error";
      error.service_type.errorMessage = 'Please select service types';

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };
  getTemplateOptions = () => {
    const templateAll = this.props.templates;
    let templates = [];
    if (templateAll && this.state.serviceCatalog.service_type) {
      templates = templateAll.filter(
        id => this.state.serviceCatalog.service_type === id.category
      );
    }
    if (
      templateAll &&
      this.state.serviceCatalog.service_type &&
      this.state.serviceCatalog.service_category
    ) {
      templates = templates.filter(
        id =>
          this.state.serviceCatalog.service_category ===
          id.service_catalog_category
      );
    }
    const templatesList = templates
      ? templates.map(t => ({
          value: t.id,
          label: t.name,
          disabled: false,
        }))
      : [];
    templatesList.push({ label: 'None', value: 'null', disabled: false });

    return templatesList;
  };
  getBody = () => {
    const serviceType = this.props.serviceType
      ? this.props.serviceType.map(t => ({
          value: t.id,
          label: t.name,
          disabled: false,
        }))
      : [];

    return (
      <div className="add-serviceCatalog">
        <div className="loader">
          <Spinner show={this.props.isFetching || this.state.loading} />
        </div>
        <div className="basic-field">
          <Input
            field={{
              value: this.state.serviceCatalog.service_name,
              label: 'Service Name',
              type: "TEXT",
              isRequired: true,
            }}
            width={6}
            name={'service_name'}
            placeholder="Enter Service Name"
            onChange={e => this.handleChange(e)}
            className="select-type"
            error={this.state.error.service_name}
          />
          <div
            className="select-type select-serviceCatalog
                field-section  field-section--required  col-lg-6 col-md-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Service Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.service_type.errorMessage ? `error-input` : ''
              }`}
            >
              <SelectInput
                name="service_type"
                value={this.state.serviceCatalog.service_type}
                onChange={e => this.handleChangeServiceType(e)}
                options={serviceType}
                multi={false}
                searchable={true}
                placeholder="Select Service Type"
              />
            </div>
            {this.state.error.service_type.errorMessage && (
              <div className="select-serviceCatalog-error field__error">
                {this.state.error.service_type.errorMessage}
              </div>
            )}
          </div>
          <div
            className="select-type multiple-choice
                field-section field-section--required  col-lg-6 col-md-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Service Technology Types
              </label>
            </div>
            <div
              className={`${
                this.state.error.service_technology_types.errorMessage
                  ? `error-input`
                  : ''
              }`}
            >
              <SelectInput
                name="service_technology_types"
                value={this.state.serviceCatalog.service_technology_types}
                onChange={e => this.handleChangeServiceTechTypes(e)}
                options={this.state.technologyTypes}
                multi={true}
                searchable={true}
                placeholder="Select Service Technology Types"
              />
            </div>
            {this.state.error.service_technology_types.errorMessage && (
              <div className="select-serviceCatalog-error field__error">
                {this.state.error.service_technology_types.errorMessage}
              </div>
            )}
          </div>
          <div
            className="select-type select-serviceCatalog
                field-section  field-section--required  col-lg-6 col-md-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Service Category
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.service_category.errorMessage
                  ? `error-input`
                  : ''
              }`}
            >
              <SelectInput
                name="service_category"
                value={this.state.serviceCatalog.service_category}
                onChange={e => this.handleChangeCategory(e)}
                options={this.getCategoryList()}
                multi={false}
                searchable={true}
                placeholder="Select Service Category"
              />
            </div>
            {this.state.error.service_category.errorMessage && (
              <div className="select-serviceCatalog-error field__error">
                {this.state.error.service_category.errorMessage}
              </div>
            )}
          </div>
          <div
            className="select-type select-sow
          field-section col-lg-6 col-md-6"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Link Template
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.linked_template.errorMessage
                  ? `error-input`
                  : ''
              }`}
            >
              <SelectInput
                name="linked_template"
                value={
                  this.state.serviceCatalog.linked_template
                    ? this.state.serviceCatalog.linked_template
                    : 'null'
                }
                onChange={e => this.handleChange(e)}
                options={this.getTemplateOptions()}
                multi={false}
                searchable={true}
                placeholder="Select Template"
                disabled={
                  !this.state.serviceCatalog.service_category ||
                  !this.state.serviceCatalog.service_type
                    ? true
                    : false
                }
              />
            </div>
            {this.state.error.linked_template.errorMessage && (
              <div className="select-sow-error field__error">
                {this.state.error.linked_template.errorMessage}
              </div>
            )}
          </div>
          <Input
            field={{
              value: this.state.serviceCatalog.notes,
              label: 'Note',
              type: "TEXTAREA",
              isRequired: false,
            }}
            width={6}
            name={'notes'}
            placeholder="Enter Note"
            onChange={e => this.handleChange(e)}
            className="select-type"
          />
        </div>
      </div>
    );
  };
  getTitle = () => {
    return this.state.serviceCatalog.id && this.state.serviceCatalog.id !== 0
      ? 'Edit Service'
      : 'Add Service';
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.setting.isFetching,
  isFetchingType: state.setting.isFetchingType,
  isFetchingSC: state.sow.isFetchingSC,
  serviceCatalog: state.sow.serviceCatalog,
  serviceType: state.setting.serviceType,
  serviceCatalogCategories: state.sow.serviceCatalogCategories,
  templates: state.sow.templates,
  isFetchingTemplates: state.sow.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchServiceCategoriesFull: () => dispatch(fetchServiceCategoriesFull()),
  fetchServiceTypeSTT: () => dispatch(fetchServiceTypeSTT()),
  getserviceCatalog: (id: any) => dispatch(getserviceCatalog(id)),
  createServiceCatalog: (serviceCatalog: any) =>
    dispatch(createServiceCatalog(serviceCatalog)),
  updateServiceCatalog: (id: any, serviceCatalog: any) =>
    dispatch(updateServiceCatalog(id, serviceCatalog)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getTemplateList: () => dispatch(getTemplateList()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddServiceCatalog);
