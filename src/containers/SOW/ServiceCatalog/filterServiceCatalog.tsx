import React from 'react';

import SquareButton from '../../../components/Button/button';
import Select from '../../../components/Input/Select/select';
import ModalBase from '../../../components/ModalBase/modalBase';
import '../../../commonStyles/filterModal.scss';
import './style.scss';

interface IServiceCatalogFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: IServiceCatalogFilters) => void;
  prevFilters?: IServiceCatalogFilters;
  service_type: any;
  service_category: any;
}

interface IServiceCatalogFilterState {
  filters: IServiceCatalogFilters;
}

export default class ServiceCatalogFilter extends React.Component<
  IServiceCatalogFilterProps,
  IServiceCatalogFilterState
> {
  constructor(props: IServiceCatalogFilterProps) {
    super(props);

    this.state = {
      filters: {
        service_type: [],
        service_category: [],
        service_technology_types: [],
        is_template_present: '',
      },
    };
  }

  componentDidUpdate(prevProps: IServiceCatalogFilterProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }

  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };

  getAllTechnoType = () => {
    const types1 = [];

    if (this.props.service_type) {
      this.props.service_type.map(
        cat =>
          cat.technology_types &&
          cat.technology_types.map(t =>
            types1.push({
              value: t.id,
              label: `${cat.name}: ${t.name}`,
              disabled: false,
            })
          )
      );
    }

    return types1;
  };
  getBody = () => {
    const serviceTypes = this.props.service_type
      ? this.props.service_type.map(user => ({
          value: user.id,
          label: user.name,
          disabled: user.is_disabled,
        }))
      : [];

    const serviceCategories = this.props.service_category
      ? this.props.service_category.map(user => ({
          value: user.id,
          label: user.name,
        }))
      : [];

    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Service Types</label>
            <div className="field__input">
              <Select
                name="service_type"
                value={filters.service_type}
                onChange={this.onFilterChange}
                options={serviceTypes}
                multi={true}
                placeholder="Select Service Types"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Service Category</label>
            <div className="field__input">
              <Select
                name="service_category"
                value={filters.service_category}
                onChange={this.onFilterChange}
                options={serviceCategories}
                multi={true}
                placeholder="Select Category"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">
              Service Technology Types
            </label>
            <div className="field__input">
              <Select
                name="service_technology_types"
                value={filters.service_technology_types}
                onChange={this.onFilterChange}
                options={this.getAllTechnoType()}
                multi={true}
                placeholder="Select Service Technology Types"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Template Availability</label>
            <div className="field__input">
              <Select
                name="is_template_present"
                value={filters.is_template_present}
                onChange={this.onFilterChange}
                options={[
                  { value: 'Available', label: 'Available' },
                  { value: 'NotAvailable', label: 'Not Available' },
                ]}
                multi={true}
                placeholder="Select "
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
