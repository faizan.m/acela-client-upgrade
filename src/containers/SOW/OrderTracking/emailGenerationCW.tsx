import React, { Component } from "react";
import { isEmpty } from "lodash";
import { connect } from "react-redux";
import { addErrorMessage } from "../../../actions/appState";
import {
  getOperationsCustomerQuotes,
  getEmailTemplateUsingOpportunity,
  getServiceTicketsList,
  GET_CUSTOMER_QUOTES_SUCCESS,
  GET_EMAIL_TEMPLATE_OPPORTUNITY_FAILURE,
  GET_EMAIL_TEMPLATE_OPPORTUNITY_SUCCESS,
} from "../../../actions/orderTracking";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import Input from "../../../components/Input/input";
import SoPoLinkingModal from "./soPoLinkingModal";
import Spinner from "../../../components/Spinner";
import { downloadfile } from "../../../utils/download";
import "./style.scss";
import TicketOppLinkingModal from "./ticketOppLinkingModal";

interface IEmailGenerationCWState {
  serviceTicket: IServiceTicket;
  quote: number;
  loadingQuotes: boolean;
  customerQuotes: IPickListOptions[];
  tablesCSVs: any[];
  loading: boolean;
  htmlContextData: any;
  showShippedQty: boolean;
  isContentCopied: boolean;
  isCopyStateActive: boolean;
  showSOPOModal: boolean;
  showOppSOModal: boolean;
  soPoMapping?: ISOPOMapping;
}

interface IEmailGenerationCWProps extends ICommonProps {
  customers: ICustomerShort[];
  isFetching: boolean;
  isTicketsFetching: boolean;
  serviceTickets: IServiceTicket[];
  getServiceTicketsList: () => Promise<any>;
  getCustomerQuotes: (customerId: number) => Promise<any>;
  getEmailTemplateUsingOpportunity: (opportunityId: number) => Promise<any>;
  addErrorMessage: TShowErrorMessage;
}

class EmailGenerationCW extends Component<
  IEmailGenerationCWProps,
  IEmailGenerationCWState
> {
  constructor(props: IEmailGenerationCWProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    serviceTicket: null,
    quote: null,
    customerQuotes: [],
    loading: false,
    tablesCSVs: null,
    htmlContextData: {},
    showShippedQty: false,
    isContentCopied: false,
    isCopyStateActive: false,
    loadingQuotes: false,
    showSOPOModal: false,
    showOppSOModal: false,
  });

  componentDidMount() {
    this.props.getServiceTicketsList();
  }

  getServiceTickets = () => {
    if (this.props.serviceTickets && this.props.serviceTickets.length > 0) {
      return this.props.serviceTickets.map((serviceTicket) => ({
        value: serviceTicket.id,
        label: serviceTicket.summary,
        disabled: false,
      }));
    } else {
      return [];
    }
  };

  handleChangeServiceTicket = (value: number): void => {
    const serviceTicket: IServiceTicket = this.props.serviceTickets.find(
      (el) => el.id === value
    );

    const previousServiceTicket = this.state.serviceTicket;
    if (
      !previousServiceTicket ||
      previousServiceTicket.company_id !== serviceTicket.company_id
    ) {
      this.setState({ customerQuotes: null, loadingQuotes: true }, () => {
        this.props
          .getCustomerQuotes(serviceTicket.company_id)
          .then((action) => {
            if (action.type === GET_CUSTOMER_QUOTES_SUCCESS) {
              let res = action.response ? action.response : [];
              this.setState({
                customerQuotes: res.map((t) => ({
                  value: t.id,
                  label: `${t.name} (${t.stage_name})`,
                  disabled: false,
                })),
              });
            }
          })
          .finally(() => this.setState({ loadingQuotes: false }));
      });
    }

    this.setState({ quote: null, serviceTicket: serviceTicket });
    if (serviceTicket.opportunity_id)
      this.getEmailTemplate(serviceTicket.opportunity_id);
  };

  getEmailTemplate = (quote: number): void => {
    this.setState(
      {
        quote: quote,
        loading: true,
        tablesCSVs: null,
        htmlContextData: {},
      },
      () =>
        this.props
          .getEmailTemplateUsingOpportunity(quote)
          .then((action) => {
            if (action.type === GET_EMAIL_TEMPLATE_OPPORTUNITY_SUCCESS) {
              this.setState({
                htmlContextData: action.response.products_data,
                tablesCSVs: action.response.table_csvs,
              });
            } else if (action.type === GET_EMAIL_TEMPLATE_OPPORTUNITY_FAILURE) {
              let errorData = action.errorList.data;
              if (errorData.sales_order_crm_id) {
                this.setState({
                  showSOPOModal: true,
                  soPoMapping: errorData,
                });
              } else if (
                errorData.detail &&
                errorData.detail ===
                  "Sales order not found for the opportunity!"
              ) {
                if (
                  this.state.serviceTicket &&
                  this.state.serviceTicket.opportunity_id &&
                  this.state.serviceTicket.opportunity_id ===
                    errorData.opportunity_crm_id
                ) {
                  this.setState({ showOppSOModal: true });
                } else {
                  this.props.addErrorMessage(
                    "No Sales Order found for the selected Opportunity!"
                  );
                }
              } else {
                this.props.addErrorMessage("An unknown error occurred!");
              }
            } else {
              this.props.addErrorMessage("Server error!");
            }
          })
          .finally(() => this.setState({ loading: false }))
    );
  };

  closeSOPOModal = (fetch: boolean = false) => {
    this.setState({ showSOPOModal: false, soPoMapping: undefined });
    if (fetch) this.getEmailTemplate(this.state.quote);
  };

  closeOppSOModal = (fetch: boolean = false) => {
    this.setState({ showOppSOModal: false });
    if (fetch) this.getEmailTemplate(this.state.serviceTicket.opportunity_id);
  };

  copyContentToClipboard = (containerid = "email-generation-content") => {
    this.setState({ isCopyStateActive: true });

    setTimeout(() => {
      if (window.getSelection) {
        let selection = window.getSelection();
        var range = document.createRange();
        range.selectNode(document.getElementById(containerid));
        selection.removeAllRanges();
        selection.addRange(range);
        document.execCommand("copy");

        this.setState({
          isContentCopied: true,
        });

        setTimeout(
          () =>
            this.setState({
              isContentCopied: false,
              isCopyStateActive: false,
            }),
          2000
        );
      }
    }, 750);
  };

  renderOrderInfo = (hardware_products, licences) => {
    return (
      <div id="hardware-content" className="hardware-products-info">
        <table id="hardware-table">
          <tbody>
            <tr>
              <td>
                <p
                  style={{
                    textAlign: "center",
                  }}
                >
                  <strong>Part Number</strong>
                </p>
              </td>
              <td>
                <p>
                  <strong>Qty.</strong>
                </p>
              </td>
              {this.state.showShippedQty && (
                <td>
                  <p>
                    <strong>Shipped Qty.</strong>
                  </p>
                </td>
              )}
              <td>
                <p>
                  <strong>Tracking Number</strong>
                </p>
              </td>
              <td>
                <p>
                  <strong>Carrier</strong>
                </p>
              </td>
              <td>
                <p>
                  <strong>Status</strong>
                </p>
              </td>
            </tr>
            {hardware_products.map((product, idx) => (
              <tr key={idx}>
                <td>
                  <p>{product.part_number}</p>
                </td>
                {product.shipped_quantity < product.purchased_quantity ? (
                  <>
                    <td style={{ background: "#ff000033" }}>
                      <p>{product.purchased_quantity}</p>
                    </td>
                    {this.state.showShippedQty && (
                      <td style={{ background: "#ff000033" }}>
                        <p>{product.shipped_quantity}</p>
                      </td>
                    )}
                  </>
                ) : (
                  <>
                    <td>
                      <p>{product.purchased_quantity}</p>
                    </td>
                    {this.state.showShippedQty && (
                      <td>
                        <p>{product.shipped_quantity}</p>
                      </td>
                    )}
                  </>
                )}
                <td>
                  {product.tracking_numbers ? (
                    product.tracking_numbers
                      .split(", ")
                      .map((tracking_number: string, idx: number) => (
                        <p
                          key={idx}
                          dangerouslySetInnerHTML={{ __html: tracking_number }}
                        />
                      ))
                  ) : (
                    <p>N.A.</p>
                  )}
                </td>
                <td>
                  <p>{product.carriers}</p>
                </td>
                <td>
                  <p>{product.ship_date}</p>
                </td>
              </tr>
            ))}
            {licences.length > 0 &&
              licences.map((product, idx) => (
                <tr key={idx}>
                  <td>
                    <p>{product.part_number}</p>
                  </td>
                  {product.shipped_quantity < product.purchased_quantity ? (
                    <>
                      <td style={{ background: "#ff000033" }}>
                        <p>{product.purchased_quantity}</p>
                      </td>
                      {this.state.showShippedQty && (
                        <td style={{ background: "#ff000033" }}>
                          <p>{product.shipped_quantity}</p>
                        </td>
                      )}
                    </>
                  ) : (
                    <>
                      <td>
                        <p>{product.purchased_quantity}</p>
                      </td>
                      {this.state.showShippedQty && (
                        <td>
                          <p>{product.shipped_quantity}</p>
                        </td>
                      )}
                    </>
                  )}
                  <td>
                    {product.tracking_numbers ? (
                      product.tracking_numbers
                        .split(", ")
                        .map((tracking_number: string, idx: number) => (
                          <p
                            key={idx}
                            dangerouslySetInnerHTML={{
                              __html: tracking_number,
                            }}
                          />
                        ))
                    ) : (
                      <p />
                    )}
                  </td>
                  <td>
                    <p>{product.carriers}</p>
                  </td>
                  <td>
                    <p>{product.ship_date}</p>
                  </td>
                  <td>
                    <p>{product.status}</p>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <br></br>
        {hardware_products.length > 0 && !this.state.isCopyStateActive && (
          <div id="hardware-action-buttons" className="action-row-table">
            {this.state.tablesCSVs && this.state.tablesCSVs.length >= 1 && (
              <SquareButton
                onClick={(e) => downloadfile(this.state.tablesCSVs[0])}
                content={`Download Table`}
                bsStyle={"primary"}
                title={`Download File`}
              />
            )}
            {!this.state.isCopyStateActive && (
              <Checkbox
                isChecked={this.state.showShippedQty}
                name="showShippedQty"
                className="show-shipped-checkbox"
                onChange={(e) =>
                  this.setState({ showShippedQty: e.target.checked })
                }
              >
                <div className="bulk-detail-title">Show Shipped Qty column</div>
              </Checkbox>
            )}
          </div>
        )}
      </div>
    );
  };

  renderContracts = (contracts) => {
    return (
      <div id="contract-content" className="contract-content-info">
        <p style={{ fontStyle: "italic" }}>
          Please note: Shipping dates are estimated by manufacturer and subject
          to change.
        </p>
        <table>
          <tbody>
            <tr>
              <td>
                <p>
                  <strong>Part Number</strong>
                </p>
              </td>

              <td>
                <p>
                  <strong>Status </strong>
                </p>
              </td>
            </tr>
            {contracts &&
              contracts.map((contract, idx) => (
                <tr key={idx}>
                  <td>
                    <p>{contract.part_number}</p>
                  </td>

                  <td>
                    <p>{contract.status}</p>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
        <br></br>
      </div>
    );
  };

  renderEmailGenerationContent = () => {
    const {
      cc_contacts,
      hardware_products,
      licences,
      name,
      to_contacts_emails,
      shipping_details,
      customer_po,
      contracts,
      subject_opportunity_name,
    } = this.state.htmlContextData;
    const isData =
      !isEmpty(this.state.htmlContextData) &&
      (hardware_products.length > 0 || contracts.length > 0);
    return (
      <div className="html-section col-md-12">
        {isData && (
          <SquareButton
            onClick={(e) =>
              this.copyContentToClipboard("email-generation-content")
            }
            content={`${
              this.state.isContentCopied ? "Copied!" : "Copy to clipboard"
            }`}
            bsStyle={"primary"}
            className="copy-to-clipboard"
            title={`Copy to clipboard`}
          />
        )}
        {isData ? (
          <div id="email-generation-content" className={`html-data`}>
            <div id="introduction-content">
              <p>{`Hello ${name}`},</p>
              <div>
                Thank you for your recent product purchase. Please see the
                shipping and tracking information below:
              </div>
              <br />
              <p>
                <b>
                  <u style={{ color: "#3399ff" }}>
                    <span style={{ fontSize: "14.0pt", color: "#3399ff" }}>
                      ORDER INFORMATION
                    </span>
                  </u>
                </b>
              </p>
            </div>
            <div className="shipping_details">
              <p>
                <u>Shipping Destination</u>:<br></br>
              </p>
              <p>{shipping_details.addressline1}</p>
              <p>{shipping_details.addressline2}</p>
              <p>{shipping_details.addressline3}</p>
              <p>
                {shipping_details.city}, {shipping_details.state}{" "}
                {shipping_details.postalcode}
              </p>
            </div>
            <p className="customer-po">
              <span>Customer PO:</span> {customer_po}
            </p>
            {(hardware_products.length > 0 || licences.length > 0) &&
              this.renderOrderInfo(hardware_products, licences)}
            {contracts.length > 0 && this.renderContracts(contracts)}
            {!this.state.isCopyStateActive &&
              this.state.tablesCSVs &&
              this.state.tablesCSVs.length >= 2 && (
                <SquareButton
                  onClick={(e) => downloadfile(this.state.tablesCSVs[1])}
                  content={`Download Table`}
                  bsStyle={"primary"}
                  className="download-button"
                  title={`Download File`}
                />
              )}
            <div
              id="to-content"
              style={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "centerwrap",
              }}
            >
              <span style={{ display: "inline-block", marginTop: "4px" }}>
                TO: &nbsp;
              </span>
              {to_contacts_emails &&
                to_contacts_emails.map((email, idx) => (
                  <div
                    key={idx}
                    style={{
                      padding: "3px",
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    <div style={{ marginLeft: "5px", color: "#000000" }}>
                      {email}
                    </div>
                  </div>
                ))}
            </div>
            <div
              id="cc-content"
              style={{
                display: "flex",
                flexWrap: "wrap",
                alignItems: "centerwrap",
              }}
            >
              <span style={{ display: "inline-block", marginTop: "4px" }}>
                CC: &nbsp;
              </span>
              {cc_contacts &&
                cc_contacts.map((contact, idx) => (
                  <div
                    key={idx}
                    style={{
                      padding: "3px",
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    <div style={{ marginLeft: "5px", color: "#000000" }}>
                      {contact.email}
                    </div>
                  </div>
                ))}
            </div>
            <div className="email-generator-subject">
              <span>Subject:</span> {subject_opportunity_name}
            </div>
          </div>
        ) : (
          <div className="no-data">
            {this.state.loading ? (
              "Loading..."
            ) : !this.state.serviceTicket ? (
              <p>
                <img
                  src={`/assets/icons/l-HIGH.svg`}
                  alt=""
                  className="status-images"
                  title={``}
                />
                {"Please select " +
                  (!this.state.serviceTicket
                    ? "a service ticket"
                    : "an opportunity")}
              </p>
            ) : (
              "No data"
            )}
          </div>
        )}
      </div>
    );
  };

  renderEmailGeneration = () => {
    return (
      <div className="email-generation-body col-md-12">
        <Input
          field={{
            label: "Service Ticket",
            type: "PICKLIST",
            value: this.state.serviceTicket && this.state.serviceTicket.id,
            isRequired: true,
            options: this.getServiceTickets(),
          }}
          loading={this.props.isTicketsFetching}
          width={6}
          placeholder="Select Service Ticket"
          name="serviceTicket"
          className="serviceTicket-select"
          onChange={(e) => this.handleChangeServiceTicket(e.target.value)}
          multi={false}
        />
        {this.state.serviceTicket && (
          <Input
            field={{
              label: "Opportunity",
              type: "PICKLIST",
              value: this.state.quote,
              isRequired: true,
              options: this.state.customerQuotes,
            }}
            loading={this.state.loadingQuotes}
            width={6}
            placeholder="Select Opportunity"
            name="quote"
            className="quote-select"
            onChange={(e) => this.getEmailTemplate(e.target.value)}
            multi={false}
            disabled={true}
          />
        )}
        {this.renderEmailGenerationContent()}
      </div>
    );
  };

  render() {
    return (
      <>
        {this.state.showSOPOModal && (
          <SoPoLinkingModal
            showModal={this.state.showSOPOModal}
            closeModal={(fetch) => this.closeSOPOModal(fetch)}
            soPoMapping={this.state.soPoMapping}
          />
        )}
        {this.state.showOppSOModal && (
          <TicketOppLinkingModal
            serviceTicket={this.state.serviceTicket}
            closeModal={(fetch: boolean) => this.closeOppSOModal(fetch)}
          />
        )}
        <div className="cr-container report">
          <div className="loader">
            <Spinner show={this.state.loading} />
          </div>
          {this.renderEmailGeneration()}
        </div>
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  serviceTickets: state.orderTracking.serviceTickets,
  isTicketsFetching: state.orderTracking.isTicketsFetching,
  customers: state.customer.customersShort,
  isFetching: state.orderTracking.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  getServiceTicketsList: () => dispatch(getServiceTicketsList()),
  getEmailTemplateUsingOpportunity: (opportunityId: number) =>
    dispatch(getEmailTemplateUsingOpportunity(opportunityId)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getCustomerQuotes: (customerId: number) =>
    dispatch(getOperationsCustomerQuotes(customerId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailGenerationCW);
