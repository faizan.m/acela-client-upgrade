import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  getSalesOrdersListing,
  getUnmappedServiceTickets,
  linkServiceTicketOpportunity,
  LINK_TICKET_OPP_SUCCESS,
} from "../../../actions/orderTracking";
import { addSuccessMessage } from "../../../actions/appState";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import "./style.scss";

interface ITicketOppModalProps {
  unlinkedSOs: IPickListOptions[];
  isFetchingSOs: boolean;
  closeModal: (refresh?: boolean) => void;
  salesOrderCrmId?: number;
  serviceTicket?: IServiceTicket;
  isTicketsFetching: boolean;
  serviceTickets: IServiceTicket[];
  fetchUnlinkedSOs: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  getUnmappedServiceTickets: () => Promise<any>;
  linkTicketOpportunity: (data: ISaveOppSOMapping) => Promise<any>;
}

interface ISaveOppSOMapping {
  ticket_crm_id: number;
  summary: string;
  service_board_crm_id: number;
  status_crm_id: number;
  status_name: string;
  company_crm_id: number;
  company_name: string;
  opportunity_crm_id: number;
  opportunity_name: string;
  connectwise_last_data_update: string;
  owner_crm_id: number;
}

const TicketOpportunityMappingModal: React.FC<ITicketOppModalProps> = (
  props
) => {
  const [posting, setPosting] = useState<boolean>(false);
  const [salesOrder, setSalesOrder] = useState<number>(null);
  const [currServiceTicket, setCurrServiceTicket] = useState<IServiceTicket>();
  const ticketOptions: IPickListOptions[] = useMemo(
    () =>
      props.serviceTickets
        .filter((el) => el.opportunity_id)
        .map((el: IServiceTicket) => ({
          value: el.id,
          label: el.summary,
          data: el,
        })),
    [props.serviceTickets]
  );

  useEffect(() => {
    if (props.serviceTicket) setCurrServiceTicket(props.serviceTicket);
    else props.getUnmappedServiceTickets();
  }, [props.serviceTicket]);

  useEffect(() => {
    if (props.salesOrderCrmId) setSalesOrder(props.salesOrderCrmId);
    else props.fetchUnlinkedSOs();
  }, [props.salesOrderCrmId]);

  const handleChangeServiceTicket = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const selectedTicket: IServiceTicket = ticketOptions.find(
      (el) => el.value === e.target.value
    ).data;
    setCurrServiceTicket(selectedTicket);
  };

  const handleChangeSalesOrder = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSalesOrder(Number(e.target.value));
  };

  const handleSubmit = () => {
    setPosting(true);
    const data = {
      ticket_crm_id: currServiceTicket.id,
      summary: currServiceTicket.summary,
      service_board_crm_id: currServiceTicket.board_id,
      status_crm_id: currServiceTicket.status_id,
      status_name: currServiceTicket.status,
      company_crm_id: currServiceTicket.company_id,
      company_name: currServiceTicket.company_name,
      opportunity_crm_id: currServiceTicket.opportunity_id,
      opportunity_name: currServiceTicket.opportunity_name,
      sales_order_crm_id: salesOrder,
      owner_crm_id: currServiceTicket.owner_id,
      connectwise_last_data_update: currServiceTicket.last_updated_on,
    };
    props
      .linkTicketOpportunity(data)
      .then((action) => {
        if (action.type === LINK_TICKET_OPP_SUCCESS) {
          props.addSuccessMessage(
            `${currServiceTicket.summary} linked to Sales Order #: ${salesOrder} successfully!`
          );
          setCurrServiceTicket(undefined);
          props.closeModal(true);
        }
      })
      .finally(() => setPosting(false));
  };

  return (
    <ModalBase
      show={true}
      onClose={() => props.closeModal(false)}
      hideCloseButton={posting}
      titleElement={`Link Opportunity to Sales Order ${
        props.salesOrderCrmId ? "#: " + props.salesOrderCrmId : ""
      }`}
      bodyElement={
        <div className="link-so-po-container">
          <div className="loader">
            <Spinner show={posting} />
          </div>
          <Input
            field={{
              label: "Service Ticket",
              type: "PICKLIST",
              value: currServiceTicket ? currServiceTicket.id : null,
              options: Boolean(props.serviceTicket)
                ? [
                    {
                      value: props.serviceTicket.id,
                      label: props.serviceTicket.summary,
                    },
                  ]
                : ticketOptions,
              isRequired: true,
            }}
            className="link-so-po-input"
            width={6}
            multi={false}
            name="service-ticket"
            onChange={handleChangeServiceTicket}
            placeholder={`Select Service Ticket`}
            loading={props.isTicketsFetching}
            disabled={Boolean(props.serviceTicket)}
          />
          {!props.salesOrderCrmId && (
            <Input
              field={{
                label: "Sales Order",
                type: "PICKLIST",
                value: salesOrder,
                options: props.unlinkedSOs,
                isRequired: true,
              }}
              className="unlinked-so-input"
              width={6}
              multi={false}
              name="sales-order"
              onChange={handleChangeSalesOrder}
              placeholder={`Select Sales Order`}
              loading={props.isFetchingSOs}
            />
          )}
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={"default"}
            onClick={() => props.closeModal(false)}
            className="save-unlink-so-po"
            disabled={posting}
          />
          <SquareButton
            content={`Save`}
            bsStyle={"primary"}
            onClick={handleSubmit}
            className="save-unlink-so-po"
            disabled={posting || !currServiceTicket || !salesOrder}
          />
        </div>
      }
      className={"link-so-po-modal"}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  unlinkedSOs: state.orderTracking.salesOrders,
  serviceTickets: state.orderTracking.unmappedServiceTickets,
  isFetchingSOs: state.orderTracking.isFetchingSalesOrders,
  isTicketsFetching: state.orderTracking.isTicketsFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchUnlinkedSOs: () => dispatch(getSalesOrdersListing()),
  getUnmappedServiceTickets: () => dispatch(getUnmappedServiceTickets()),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  linkTicketOpportunity: (data: ISaveOppSOMapping) =>
    dispatch(linkServiceTicketOpportunity(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TicketOpportunityMappingModal);
