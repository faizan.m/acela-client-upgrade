import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';


import { FETCH_POTICKETS_SUCCESS,  getLineItemsIngramData, getLineItemsOperations, getListPOTickts, getListPurchaseOrdersAllStatus, INGRAM_DATA_SUCCESS, LINE_ITEMS_SUCCESS, postLineItemsData, POST_LINE_ITEMS_FAILURE, POST_LINE_ITEMS_SUCCESS } from '../../../actions/inventory';
import Input from '../../../components/Input/input';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import Table from '../../../components/Table/table';
import Checkbox from '../../../components/Checkbox/checkbox';
import SquareButton from '../../../components/Button/button';
// import TagsInput from 'react-tagsinput'; (New Component)
import Spinner from '../../../components/Spinner';
import { getPurchaseOrderSettings, getShipmentMethods } from '../../../actions/setting';
enum PageType {
  Email,
  Receiving,
}
interface IReceivingState {
  currentPage: {
    pageType: PageType;
  };
  // purchaseOrders: any[];
  loadingPurchaseOrders: boolean;
  ticket_id: any;
  ticket_note: any;
  customer_id: any;
  purchase_order_id: any;
  loading: boolean;
  poTickets: any;
  poTicketsRaw: any;
  lineItems: any[];
  loadingLineItems: boolean;
  ingramData: any[];
  loadingIngramData: boolean;
  selectedRows: any[];
  loadingTickts: boolean;
  errors: any;
}

interface IReceivingProps extends ICommonProps {
  ticket_id: any;
  customer_id: any;
  shipments: IPickListOptions[];
  isFetchingShipments: boolean;
  purchase_order_id: any;
  isFetching: boolean;
  purchaseOrdersAll: any[];
  addSuccessMessage: any;
  addErrorMessage: any;
  customers: any;
}

class OrderTrackingReceiving extends Component<any, IReceivingState> {
  constructor(props: IReceivingProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Email,
    },
    // purchaseOrders: [],
    loadingPurchaseOrders: false,
    loading: false,
    ticket_id: 0,
    ticket_note: '',
    customer_id: null,
    purchase_order_id: null,
    poTicketsRaw: [],
    poTickets: [],
    lineItems: [],
    loadingLineItems: false,
    ingramData: [],
    selectedRows: [],
    loadingIngramData: false,
    loadingTickts: false,
    errors: null,
  });

  componentDidMount() {
    this.props.getShipmentMethods();
    this.props.getPurchaseOrderSettings();
  }

  changePage = (pageType: PageType) => {
    const newstate = this.getEmptyState();
    newstate.currentPage.pageType = pageType;
    this.setState(newstate);
  };
  handleChange = (e: any) => {
    const newstate = this.state;
    newstate[e.target.name] = e.target.value;
    this.setState(newstate);
    if (e.target.name === 'purchase_order_id') {
      this.getLineItemsOperations(e.target.value);
      const POID = e.target.value;
      const CRMID = this.props.purchaseOrdersAll.find(x => x.id === POID).customerCompany.id;
      const acelaID = this.props.customers.find(x => x.crm_id === CRMID).id;
      this.getListPOTickts(acelaID);
      this.setState({ lineItems: [], ticket_id: 0 });
    }
  };
  getCustomerOptions = () => {
    if (this.props.customers && this.props.customers.length > 0) {
      return this.props.customers.map(role => ({
        value: role.id,
        label: role.name,
      }));
    } else {
      return [];
    }
  };

  renderHeader = () => {
    return (
      <div className="email-generation-body col-md-12">
        <Input
          field={{
            label: 'Purchase Order',
            type: "PICKLIST",
            value: this.state.purchase_order_id,
            isRequired: true,
            options: this.props.purchaseOrdersAll
            .map(d => { return { label: `${d.poNumber} (${d.customerCompany  && d.customerCompany.name || ''})`, value: d.id } })
          }}
          width={4}
          loading={this.props.isFetching}
          placeholder="Select Purchase Order"
          name="purchase_order_id"
          className="purchaseOrder-select"
          onChange={e => this.handleChange(e)}
          multi={false}
        />
        <Input
          field={{
            label: 'Ticket',
            type: "PICKLIST",
            value: this.state.ticket_id,
            isRequired: false,
            options: this.state.poTickets,
          }}
          disabled={!this.state.purchase_order_id}
          loading={this.state.loadingTickts}
          width={4}
          placeholder="Select Ticket"
          name="ticket_id"
          className="purchaseOrder-select"
          onChange={e => this.handleChange(e)}
          multi={false}
        />
      </div>
    );
  };


  getListPOTickts = (customerId) => {
    this.setState({ loadingTickts: true })
    this.props.getListPOTickts(customerId).then(action => {
      if (action.type === FETCH_POTICKETS_SUCCESS) {
        const poTicketsRaw = action.response;
        const poTickets = poTicketsRaw
          ? poTicketsRaw.map(c => ({
            value: c.id,
            label: ` ${c.id}-${c.summary}`,
          }))
          : [];

      const poNumber = this.props.purchaseOrdersAll.find(x => x.id === this.state.purchase_order_id).poNumber;
      const ticket =  poTickets.find(x => x.label.includes(poNumber));
      const ticket_id =  ticket && ticket.value;
      this.setState({ poTickets, poTicketsRaw, loadingTickts: false, ticket_id })
      }
    });
  }

  getLineItemsOperations = (value) => {
    this.setState({ loadingLineItems: true })
    this.props.getLineItemsOperations([value]).then(action => {
      if (action.type === LINE_ITEMS_SUCCESS) {
        const lineItems = action.response;
        this.setState({ lineItems, loadingLineItems: false }, () => {
          if (this.state.lineItems.length) {
            this.getLineItemsIngramData();
          }
        })
      }
      this.setState({ loadingLineItems: false })
    });
  }

  getLineItemsIngramData = () => {
    this.setState({ loadingIngramData: true })
    this.props.getLineItemsIngramData(
      [this.props.purchaseOrdersAll.find(x => x.id === this.state.purchase_order_id).poNumber],
      this.state.lineItems.map(item => item.product.identifier,
        [this.state.purchase_order_id]
      )).then(action => {
        if (action.type === INGRAM_DATA_SUCCESS) {
          const ingramData = action.response;
          const newstate = this.state;
          (newstate.loadingIngramData as boolean) = false;
          (newstate.ingramData as any) = ingramData;
          this.state.lineItems.map((item, index) => {
            if (item.product.identifier && ingramData[item.product.identifier]) {
              newstate.lineItems[index].shipping_method_id = this.props.purchaseOrderSetting
                && this.props.purchaseOrderSetting.extra_config
                && this.props.purchaseOrderSetting.extra_config.carrier_tracking_url
                && this.props.purchaseOrderSetting.extra_config.carrier_tracking_url[ingramData[item.product.identifier].carrier]
                && this.props.purchaseOrderSetting.extra_config.carrier_tracking_url[ingramData[item.product.identifier].carrier].connectwise_shipper;
              newstate.lineItems[index].serial_numbers = ingramData[item.product.identifier].serial_numbers || [];
              newstate.lineItems[index].ship_date = ingramData[item.product.identifier].ship_date;
              newstate.lineItems[index].tracking_number = ingramData[item.product.identifier].tracking_number;
              newstate.lineItems[index].shipped_quantity = ingramData[item.product.identifier].shipped_quantity;
              newstate.lineItems[index].carrier = ingramData[item.product.identifier].carrier;
            }
          })
          this.setState(newstate);
        }

        this.setState({ loadingIngramData: false })
      });
  }
  addOneHourToISO = date =>{
    let d = new Date(date);
    return new Date(d.setHours(d.getHours() + 8)).toISOString();
   }
  postLineItemsData = () => {
    this.setState({errors : null, loading: true })
    const line_items = [];
    this.state.lineItems
      .filter(x => x.selected)
      .map((item) => {
        line_items.push({
          line_item_id: item.id,
          tracking_number: item.tracking_number || '',
          ship_date: item.ship_date ? this.addOneHourToISO(item.ship_date) : '',
          serial_numbers: item.serial_numbers,
          receive_all: item.receive_all,
          note: item.internalNotes ,
          shipping_method_id: item.shipping_method_id,
        })
      });
    this.props.postLineItemsData(
      123,
      this.state.purchase_order_id,
      this.state.ticket_id,
      this.state.ticket_note,
      line_items,
    ).then(action => {
      if (action.type === POST_LINE_ITEMS_SUCCESS) {
        this.props.addSuccessMessage('Updated');
        this.getLineItemsOperations(this.state.purchase_order_id);
        this.setState({  loading: false })
      }
      if (action.type === POST_LINE_ITEMS_FAILURE) {
        this.props.addErrorMessage('Failed');
        this.setState({ errors: action.errorList.data, loading: false })
      }
    });
  }


  onRowsToggle = selectedRows => {
    const newState = this.state;

    this.state.lineItems.map((row, i) => {
      newState.lineItems[i].selected = false;
    });

    selectedRows.map((id, index) => {
      this.state.lineItems.map((row, i) => {
        if (row.id === id) {
          newState.lineItems[i].selected = true;
        }
      })
    });



    this.setState({
      lineItems: newState.lineItems,
    }, () => {
      this.setState({
        selectedRows,
      });
    });
  };

  handleChangeTable = (event: any, data?: any) => {
    const lineItems = this.state.lineItems;
    lineItems[data.index][event.target.name] = event.target.value;
    this.setState({
      lineItems,
    });
  };

  handleChangeTableSN = (event: any, data?: any) => {
    const lineItems = this.state.lineItems;
    lineItems[data.index].serial_numbers = event;
    this.setState({
      lineItems,
    });
  };

  handleChangeTableCheckBox = (event: any, data?: any) => {
    const lineItems = this.state.lineItems;
    lineItems[data.index][event.target.name] = event.target.checked;
    this.setState({
      lineItems,
    });
  };
  getPurchaseOrder = () => {

    const rowSelectionProps = {
      showCheckbox: true,
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };

    const columns: any = [
      {
        accessor: 'product',
        Header: <div className="connectwise-header">Product ID</div>,
        sortable: false,
        Cell: cell => (
          <div className={`${cell.original.unitCost > 0? 'unit-cost' : ''}`}
           title={`Unit Cost : ${cell.original.unitCost}`}> {
            cell.original.product.identifier
          }
          </div>
        ),
      },
      {
        accessor: 'product',
        Header: <div className="connectwise-header">Description</div>,
        sortable: false,
        Cell: cell => (
          <> {
            cell.original.description
          }
          </>
        ),
      },
      {
        accessor: 'product',
        Header: <div className="connectwise-header">Purchased Qty</div>,
        sortable: false,
        width: 80,
        Cell: cell => (
               <div className={`${ cell.original.quantity > (cell.original.shipped_quantity || 0) ? 'quantity-pending' : 'quantity-p'}`}
              > {
                cell.original.quantity
              }
              </div>
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header">Shipped Qty</div>,
        sortable: false,
        width: 90,
        Cell: cell => (
               <div className={`${ cell.original.quantity > cell.original.shipped_quantity ? 'quantity-pending' : 'quantity-p'}`}
               > {
                cell.original.shipped_quantity || '-'
              }
              </div>
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header"> Tracking Number</div>,
        sortable: false,
        Cell: cell => (
          <Input
            field={{
              label: '',
              type: "TEXT",
              isRequired: false,
              value: cell.original.tracking_number,
            }}
            disabled={false}
            width={12}
            name="tracking_number"
            onChange={e => this.handleChangeTable(e, cell)}
          />
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header">Carrier</div>,
        sortable: false,
        // width: 150,
        Cell: cell => (
          <> {
            cell.original.carrier
          }
          </>
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header"> Mapped Carrier</div>,
        sortable: false,
        // width: 150,
        Cell: cell => (
          <Input
            field={{
              label: '',
              type: "PICKLIST",
              isRequired: false,
              value: cell.original.shipping_method_id,
              options: this.props.shipments,
            }}
            disabled={false}
            width={12}
            name="shipping_method_id"
            loading={this.props.isFetchingShipments}
            onChange={e => this.handleChangeTable(e, cell)}
          />
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header">Ship Date</div>,
        sortable: false,
        width: 150,
        Cell: cell => (
          <Input
            field={{
              label: '',
              type: "DATE",
              isRequired: false,
              value: cell.original.ship_date,
            }}
            disabled={false}
            showTime={false}
            width={12}
            name="ship_date"
            onChange={e => this.handleChangeTable(e, cell)}
          />
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header"> Serial Numbers</div>,
        sortable: false,
        Cell: cell => (
          // <TagsInput
          //   value={cell.original.serial_numbers || []}
          //   onChange={e => this.handleChangeTableSN(e, cell)}
          //   inputProps={{
          //     className: 'react-tagsinput-input',
          //     placeholder: 'Enter Serial Numbers',
          //   }}
          //   addOnBlur={true}
          // />
          null
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header">Receive All</div>,
        sortable: false,
        width: 70,
        Cell: cell => (
          <Checkbox
            isChecked={cell.original.receive_all}
            name="receive_all"
            onChange={e => this.handleChangeTableCheckBox(e, cell)}
          >
          </Checkbox>
        ),
      },
      {
        accessor: 'product',
        Header: <div className="ingram-header">Notes</div>,
        sortable: false,
        Cell: cell => (
          <Input
            field={{
              label: '',
              type: "TEXTAREA",
              isRequired: false,
              value: cell.original.internalNotes ,
            }}
            disabled={false}
            width={12}
            name="internalNotes"
            onChange={e => this.handleChangeTable(e, cell)}
          />
        ),
      },
    ];
    return (
      <div className="field-mapping po-mapping">
        <div className="line-items-header">
          <div className="connecwise connectwise-header">Connectwise Data</div>
          <div className="updates ingram-header"> Updates </div>
        </div>
        <div>
          <Table
            columns={columns}
            rows={this.state.lineItems}
            rowSelection={rowSelectionProps}
            className={`order-tracking-line-items`}
            loading={this.state.loadingLineItems || this.state.loadingIngramData}
            pageSize={10}
          />
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="cr-container report">
        <div className="loader">
          <Spinner
            show={
              this.state.loading
            }
          />
        </div>
        {this.renderHeader()}
        {this.getPurchaseOrder()}
        <div className="footer-line-items">
          {
            this.state.ticket_id !== 0 &&
            <Input
              field={{
                label: 'Ticket Notes',
                type: "TEXTAREA",
                value: this.state.ticket_note,
                isRequired: false,
              }}
              width={12}
              name="description"
              onChange={e => this.setState({ ticket_note: e.target.value })}
              placeholder={`Enter Description`}
            />
          }
          {this.state.errors &&
            <div className="error" >{JSON.stringify(this.state.errors, null, 2)}</div>}
          {
            this.state.lineItems.filter(item => item.selected).length !== 0 &&
            <SquareButton
              content={`Update Selected`}
              bsStyle={"primary"}
              onClick={e => this.postLineItemsData()}
              className="save"
            />
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  shipments: state.setting.shipments,
  isFetching: state.inventory.isPostingBatch,
  customers: state.customer.customersShort,
  isFetchingShipments: state.setting.isFetchingShipments,
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getListPurchaseOrdersAllStatus: (showAll: boolean) => dispatch(getListPurchaseOrdersAllStatus(showAll)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getListPOTickts: (customerId: number) => dispatch(getListPOTickts(customerId)),
  getLineItemsOperations: (purchase_order_ids: number[]) => dispatch(getLineItemsOperations(purchase_order_ids)),
  postLineItemsData: (cust_ids, ids, ti, tn, li) => dispatch(postLineItemsData(cust_ids, ids, ti, tn, li)),
  getLineItemsIngramData:
    (purchase_orders: string[], line_item_identifiers: string[], purchase_order_id: any) =>
      dispatch(getLineItemsIngramData(purchase_orders, line_item_identifiers, purchase_order_id)),
  getShipmentMethods: () => dispatch(getShipmentMethods()),
  getPurchaseOrderSettings: () => dispatch(getPurchaseOrderSettings()),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderTrackingReceiving);
