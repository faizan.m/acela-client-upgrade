import React, { useMemo, useState } from "react";
import { connect } from "react-redux";
import { random } from "lodash";
import {
  postIgnoredSOs,
  POST_IGNORE_SO_FAILURE,
  POST_IGNORE_SO_SUCCESS,
} from "../../../../actions/orderTracking";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Checkbox from "../../../../components/Checkbox/checkbox";
import SquareButton from "../../../../components/Button/button";
import ConfirmBox from "../../../../components/ConfirmBox/ConfirmBox";
import InfiniteListing from "../../../../components/InfiniteList/infiniteList";
import TicketOpportunityMappingModal from "../ticketOppLinkingModal";
import SoPoLinkingModal from "../soPoLinkingModal";

interface MappingInfoProps {
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  postIgnoredSOs: (tickets: number[]) => Promise<any>;
}

const checkBoxfilters = [
  {
    key: "unmapped_service_tickets",
    name: "SO not linked to Opportunity",
    value: false,
  },
  {
    key: "unmapped_purchase_orders",
    name: "SO not linked to POs",
    value: true,
  },
  {
    key: "mapped_tickets_sos_pos",
    name: "Fully Mapped Data",
    value: false,
  },
];

const MappingInfo: React.FC<MappingInfoProps> = (props) => {
  // State for SO-PO Mapping
  const [soPoMapping, setSoPoMapping] = useState<ISOPOMapping>();
  const [showSOPOLinkingModal, setShowSOPOLinkModal] = useState<boolean>(false);
  // State for Opportunity-SO Mapping
  const [currMappingInfo, setCurrMappingInfo] = useState<IMappingInfo>();
  const [showOppLinkingModal, setShowOppLinkModal] = useState<boolean>(false);

  // State for ignoring Sales Order
  const [posting, setPosting] = useState<boolean>(false);
  const [soToIgnore, setIgnoreSO] = useState<Set<number>>(new Set());
  const [showConfirmModal, setShowConfirmModal] = useState<boolean>(false);

  const [updateKey, setUpdateKey] = useState<number>(random(1, 1000));
  const columns: IColumnInfinite<IMappingInfo>[] = useMemo(
    () => [
      {
        name: "",
        className: "ignore-so",
        Cell: (mapping) => {
          return (
            <Checkbox
              isChecked={soToIgnore.has(mapping.sales_order_crm_id)}
              name="so-ignore"
              onChange={(e) =>
                handleCheckboxChange(e, mapping.sales_order_crm_id)
              }
            />
          );
        },
      },
      {
        name: "Ticket Summary",
        id: "ticket_summary",
        Cell: (c) => (
          <div title={c.ticket_summary ? c.ticket_summary : "-"}>
            {c.ticket_summary ? c.ticket_summary : "-"}
          </div>
        ),
      },
      {
        name: "Opportunity",
        id: "opportunity_name",
        className: "mapping-info-link-col",
        Cell: (c) => (
          <>
            <div title={c.opportunity_name ? c.opportunity_name : "-"}>
              {c.opportunity_name ? c.opportunity_name : "-"}
            </div>
            {!c.ticket_summary &&
              !c.opportunity_crm_id &&
              c.sales_order_crm_id && (
                <img
                  src="/assets/new-icons/link.svg"
                  className="ticket-opp-link-svg"
                  alt="Link Opp to SO"
                  title="Link Opportunity to Sales Order"
                  onClick={() => handleClickOppSOLink(c)}
                />
              )}
          </>
        ),
      },
      {
        name: "Sales Order #",
        id: "sales_order_crm_id",
        className: "width-10 mapping-info-link-col",
        ordering: "crm_id",
        Cell: (c) => (
          <>
            <div
              title={c.sales_order_crm_id ? String(c.sales_order_crm_id) : "-"}
            >
              {c.sales_order_crm_id ? c.sales_order_crm_id : "-"}
            </div>
            {c.opportunity_crm_id && c.sales_order_crm_id && (
              <img
                src="/assets/new-icons/link.svg"
                className="ticket-opp-link-svg"
                alt="Link SO to PO"
                title="Link Sales Order to Purchase Order(s)"
                onClick={() => handleClickSOPOLink(c)}
              />
            )}
          </>
        ),
      },
      {
        name: "Purchase Order #",
        id: "po_number",
        className: "width-15",
        ordering: "linked_pos__po_number__po_number",
        Cell: (c) => (
          <div title={c.po_number ? c.po_number : "-"}>
            {c.po_number ? c.po_number : "-"}
          </div>
        ),
      },
      {
        name: "Reason",
        id: "po_custom_field_present",
        Cell: (c) => {
          const reason: string =
            c.po_custom_field_present &&
            c.po_custom_field_present.po_custom_field_present &&
            c.po_custom_field_present.po_custom_field_present.reason
              ? c.po_custom_field_present.po_custom_field_present.reason
              : "-";
          return <div title={reason}>{reason}</div>;
        },
      },
    ],
    [soToIgnore]
  );

  const handleClickOppSOLink = (data: IMappingInfo) => {
    setCurrMappingInfo(data);
    setShowOppLinkModal(true);
  };

  const toggleConfirmModal = () => {
    setShowConfirmModal((prev) => !prev);
  };

  const handleCheckboxChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    id: number
  ) => {
    const newTicketsToIgnore = new Set(soToIgnore);
    if (e.target.checked) newTicketsToIgnore.add(id);
    else newTicketsToIgnore.delete(id);
    setIgnoreSO(newTicketsToIgnore);
  };

  const onClickIgnore = () => {
    setPosting(true);
    props
      .postIgnoredSOs(Array.from(soToIgnore))
      .then((action) => {
        if (action.type === POST_IGNORE_SO_SUCCESS) {
          setUpdateKey(random(1000));
          props.addSuccessMessage(
            `Sales Orders: ${Array.from(soToIgnore).join(
              ", "
            )} ignored successfully!`
          );
          setIgnoreSO(new Set());
        } else if (action.type === POST_IGNORE_SO_FAILURE) {
          props.addErrorMessage("Error ignoring the selected sales orders!");
        }
      })
      .finally(() => {
        toggleConfirmModal();
        setPosting(false);
      });
  };

  const handleClickSOPOLink = (data: IMappingInfo) => {
    const soPoMapping: ISOPOMapping = {
      opportunity_crm_id: data.opportunity_crm_id,
      opportunity_name: data.opportunity_name,
      sales_order_crm_id: data.sales_order_crm_id,
      purchase_order_crm_ids: data.purchase_order_crm_ids,
      purchase_order_numbers: data.so_linked_pos,
    };
    setSoPoMapping(soPoMapping);
    setShowSOPOLinkModal(true);
  };

  const closeOppLinkingModal = (refresh: boolean = false) => {
    setShowOppLinkModal(false);
    setCurrMappingInfo(undefined);
    if (refresh) setUpdateKey(random(1, 1000));
  };

  const closeSOPOLinkingModal = (refresh: boolean = false) => {
    setShowSOPOLinkModal(false);
    setSoPoMapping(undefined);
    if (refresh) setUpdateKey(random(1, 1000));
  };

  return (
    <>
      {showOppLinkingModal && (
        <TicketOpportunityMappingModal
          closeModal={(refresh: boolean) => closeOppLinkingModal(refresh)}
          salesOrderCrmId={currMappingInfo.sales_order_crm_id}
        />
      )}
      {showSOPOLinkingModal && (
        <SoPoLinkingModal
          showModal={showSOPOLinkingModal}
          closeModal={(refresh: boolean) => closeSOPOLinkingModal(refresh)}
          soPoMapping={soPoMapping}
        />
      )}
      <ConfirmBox
        show={showConfirmModal}
        onClose={toggleConfirmModal}
        onSubmit={onClickIgnore}
        isLoading={posting}
        title={`Are you sure about ignoring the Sales Orders: ${Array.from(
          soToIgnore
        ).join(", ")}?`}
      />
      <div className="mapping-info-container">
        {soToIgnore.size > 0 && (
          <SquareButton
            onClick={toggleConfirmModal}
            content={"Ignore Selected SOs"}
            bsStyle={"primary"}
            className={"ignore-so-btn"}
          />
        )}
        <InfiniteListing
          id="MappingInfo"
          url={"providers/sales-order/opp-ticket-so-po-link"}
          checkBoxfilters={checkBoxfilters}
          refreshKey={updateKey}
          columns={columns}
          className="mapping-info-listing"
          showSearch={true}
          height={"70vh"}
        />
      </div>
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  postIgnoredSOs: (ids: number[]) => dispatch(postIgnoredSOs(ids)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MappingInfo);
