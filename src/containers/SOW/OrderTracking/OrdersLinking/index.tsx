import React from "react";
import HorizontalTabSlider, {
  Tab,
} from "../../../../components/HorizontalTabSlider/HorizontalTabSlider";
import UnlinkedPOs from "./unlinkedPOs";
import MappingInfo from "./mappingInfo";
import FailedTickets from "./failedTickets";
import IgnoredSOs from "./ignoredSOs";
import "./style.scss";

const OrdersMapping: React.FC = () => {
  return (
    <div className="orders-linking-container">
      <HorizontalTabSlider>
        <Tab title={"Mapping Info"}>
          <MappingInfo />
        </Tab>
        <Tab title={"Failed Tickets"}>
          <FailedTickets />
        </Tab>
        <Tab title={"Unlinked Purchase Orders"}>
          <UnlinkedPOs />
        </Tab>
        <Tab title={"Ignored Sales Orders"}>
          <IgnoredSOs />
        </Tab>
      </HorizontalTabSlider>
    </div>
  );
};

export default OrdersMapping;
