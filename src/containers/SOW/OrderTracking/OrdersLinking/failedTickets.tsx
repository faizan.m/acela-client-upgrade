import React, { useMemo, useState } from "react";
import { connect } from "react-redux";
import { random } from "lodash";
import {
  postIgnoredTickets,
  POST_IGNORE_TICKETS_FAILURE,
  POST_IGNORE_TICKETS_SUCCESS,
} from "../../../../actions/orderTracking";
import { addErrorMessage } from "../../../../actions/appState";
import Checkbox from "../../../../components/Checkbox/checkbox";
import SquareButton from "../../../../components/Button/button";
import InfiniteListing from "../../../../components/InfiniteList/infiniteList";
import ConfirmBox from "../../../../components/ConfirmBox/ConfirmBox";

interface FailedTicketProps {
  addErrorMessage: TShowErrorMessage;
  postIgnoredTickets: (tickets: number[]) => Promise<any>;
}

interface IFailedTicket {
  id: number;
  is_ignored: boolean;
  summary: string;
  ticket_crm_id: number;
}

const FailedTickets: React.FC<FailedTicketProps> = (props) => {
  const [saving, setSaving] = useState<boolean>(false);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [reloadKey, setReloadKey] = useState<number>(random(10000));
  const [ticketsToIgnore, setIgnoreTickets] = useState<Set<number>>(new Set());
  const columns: IColumnInfinite<IFailedTicket>[] = useMemo(
    () => [
      {
        name: "",
        className: "so-po-ticket-ignore",
        Cell: (ticket) => {
          return (
            <Checkbox
              isChecked={ticketsToIgnore.has(ticket.ticket_crm_id)}
              name="ticket-ignore"
              onChange={(e) => handleCheckboxChange(e, ticket.ticket_crm_id)}
            />
          );
        },
      },
      {
        name: "Ticket #",
        ordering: "ticket_crm_id",
        className: "width-10",
        id: "ticket_crm_id",
      },
      {
        name: "Summary",
        ordering: "summary",
        className: "",
        id: "summary",
        Cell: (status) => (
          <div className={`status-icon-rules`}>{status.summary}</div>
        ),
      },
    ],
    [ticketsToIgnore]
  );

  const handleCheckboxChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    id: number
  ) => {
    const newTicketsToIgnore = new Set(ticketsToIgnore);
    if (e.target.checked) newTicketsToIgnore.add(id);
    else newTicketsToIgnore.delete(id);
    setIgnoreTickets(newTicketsToIgnore);
  };

  const toggleModal = () => {
    setShowModal((prev) => !prev);
  };

  const onClickConfirm = () => {
    setSaving(true);
    props
      .postIgnoredTickets(Array.from(ticketsToIgnore))
      .then((action) => {
        if (action.type === POST_IGNORE_TICKETS_SUCCESS) {
          setReloadKey(random(1000));
          setIgnoreTickets(new Set());
        } else if (action.type === POST_IGNORE_TICKETS_FAILURE) {
          props.addErrorMessage("Error ignoring the tickets!");
        }
      })
      .finally(() => {
        toggleModal();
        setSaving(false);
      });
  };

  return (
    <div className="failed-tickets-container">
      {ticketsToIgnore.size > 0 && (
        <SquareButton
          onClick={toggleModal}
          content={"Ignore Selected Tickets"}
          bsStyle={"primary"}
          className={"ignore-failed-tickets-btn"}
        />
      )}
      <ConfirmBox
        show={showModal}
        onClose={toggleModal}
        onSubmit={onClickConfirm}
        isLoading={saving}
        title={
          "Are you sure about ignoring the selected tickets, they cannot be retrieved later?"
        }
      />
      <InfiniteListing
        id="FailedTickets"
        url={"providers/order-tracking/dashboard/failed-service-tickets"}
        key={reloadKey}
        columns={columns}
        className="failed-service-tickets"
        showSearch={true}
        height={"70vh"}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  postIgnoredTickets: (tickets: number[]) =>
    dispatch(postIgnoredTickets(tickets)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FailedTickets);
