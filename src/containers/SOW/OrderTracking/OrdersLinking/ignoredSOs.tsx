import React, { useMemo, useState } from "react";
import { connect } from "react-redux";
import { random } from "lodash";
import {
  postUnignoredSOs,
  POST_IGNORE_SO_FAILURE,
  POST_IGNORE_SO_SUCCESS,
} from "../../../../actions/orderTracking";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Checkbox from "../../../../components/Checkbox/checkbox";
import SquareButton from "../../../../components/Button/button";
import ConfirmBox from "../../../../components/ConfirmBox/ConfirmBox";
import InfiniteListing from "../../../../components/InfiniteList/infiniteList";

interface IgnoredSOsProps {
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  postUnignoredSOs: (tickets: number[]) => Promise<any>;
}

const IgnoredSOs: React.FC<IgnoredSOsProps> = (props) => {
  const [posting, setPosting] = useState<boolean>(false);
  const [soToUnignore, setUnignoredSO] = useState<Set<number>>(new Set());
  const [showConfirmModal, setShowConfirmModal] = useState<boolean>(false);
  const [updateKey, setUpdateKey] = useState<number>(random(1, 1000));

  const columns: IColumnInfinite<IMappingInfo>[] = useMemo(
    () => [
      {
        name: "",
        className: "ignore-so",
        Cell: (mapping) => {
          return (
            <Checkbox
              isChecked={soToUnignore.has(mapping.sales_order_crm_id)}
              name="so-ignore"
              onChange={(e) =>
                handleCheckboxChange(e, mapping.sales_order_crm_id)
              }
            />
          );
        },
      },
      {
        name: "Ticket Summary",
        id: "ticket_summary",
        Cell: (c) => (
          <div title={c.ticket_summary ? c.ticket_summary : "-"}>
            {c.ticket_summary ? c.ticket_summary : "-"}
          </div>
        ),
      },
      {
        name: "Opportunity",
        id: "opportunity_name",
        className: "mapping-info-link-col",
        Cell: (c) => (
          <div title={c.opportunity_name ? c.opportunity_name : "-"}>
            {c.opportunity_name ? c.opportunity_name : "-"}
          </div>
        ),
      },
      {
        name: "Sales Order #",
        id: "sales_order_crm_id",
        className: "width-10 mapping-info-link-col",
        ordering: "crm_id",
        Cell: (c) => (
          <div
            title={c.sales_order_crm_id ? String(c.sales_order_crm_id) : "-"}
          >
            {c.sales_order_crm_id ? c.sales_order_crm_id : "-"}
          </div>
        ),
      },
      {
        name: "Purchase Order #",
        id: "po_number",
        className: "width-15",
        ordering: "linked_pos__po_number__po_number",
        Cell: (c) => (
          <div title={c.po_number ? c.po_number : "-"}>
            {c.po_number ? c.po_number : "-"}
          </div>
        ),
      },
      {
        name: "Reason",
        id: "po_custom_field_present",
        Cell: (c) => {
          const reason: string =
            c.po_custom_field_present &&
            c.po_custom_field_present.po_custom_field_present &&
            c.po_custom_field_present.po_custom_field_present.reason
              ? c.po_custom_field_present.po_custom_field_present.reason
              : "-";
          return <div title={reason}>{reason}</div>;
        },
      },
    ],
    [soToUnignore]
  );

  const toggleConfirmModal = () => {
    setShowConfirmModal((prev) => !prev);
  };

  const handleCheckboxChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    id: number
  ) => {
    const newTicketsToIgnore = new Set(soToUnignore);
    if (e.target.checked) newTicketsToIgnore.add(id);
    else newTicketsToIgnore.delete(id);
    setUnignoredSO(newTicketsToIgnore);
  };

  const onClickIgnore = () => {
    setPosting(true);
    props
      .postUnignoredSOs(Array.from(soToUnignore))
      .then((action) => {
        if (action.type === POST_IGNORE_SO_SUCCESS) {
          setUpdateKey(random(1000));
          props.addSuccessMessage(
            `Sales Orders: ${Array.from(soToUnignore).join(
              ", "
            )} unignored successfully!`
          );
          setUnignoredSO(new Set());
        } else if (action.type === POST_IGNORE_SO_FAILURE) {
          props.addErrorMessage("Error unignoring the selected sales orders!");
        }
      })
      .finally(() => {
        toggleConfirmModal();
        setPosting(false);
      });
  };

  return (
    <>
      <ConfirmBox
        show={showConfirmModal}
        onClose={toggleConfirmModal}
        onSubmit={onClickIgnore}
        isLoading={posting}
        title={`Are you sure about reverting the Sales Orders: ${Array.from(
          soToUnignore
        ).join(", ")} to Linking Info?`}
      />
      <div className="mapping-info-container">
        {soToUnignore.size > 0 && (
          <SquareButton
            onClick={toggleConfirmModal}
            content={"Unignore Selected SOs"}
            bsStyle={"primary"}
            className={"unignore-so-btn"}
          />
        )}
        <InfiniteListing
          id="IgnoredSOs"
          url={"providers/sales-order/list-ignored-sos"}
          refreshKey={updateKey}
          columns={columns}
          className="mapping-info-listing"
          showSearch={true}
          height={"70vh"}
        />
      </div>
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  postUnignoredSOs: (ids: number[]) => dispatch(postUnignoredSOs(ids)),
});

export default connect(mapStateToProps, mapDispatchToProps)(IgnoredSOs);
