import React from "react";
import InfiniteListing from "../../../../components/InfiniteList/infiniteList";

const UnlinkedPOs: React.FC = () => (
  <InfiniteListing
    id="UnlinkedPOs"
    url={"providers/order-tracking/dashboard/unlinked-purchase-orders"}
    columns={[
      {
        name: "Purchase Order #",
        ordering: "po_number",
        className: "width-15",
        id: "po_number",
      },
      {
        name: "Customer",
        className: "width-20",
        id: "customer_name",
        ordering: "customer_name",
      },
      {
        name: "Ticket Summary",
        id: "service_ticket_summary",
        ordering: "service_ticket_summary",
      },
      {
        name: "Reason",
        className: "failed-ticket-reason",
        Cell: (ticket) =>
          ticket.custom_field_present && ticket.custom_field_present.reason
            ? ticket.custom_field_present.reason
            : "",
      },
    ]}
    className="unlinked-pos"
    showSearch={true}
    height={"70vh"}
  />
);

export default UnlinkedPOs;
