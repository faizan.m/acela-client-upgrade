import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  getUnlinkedPurchaseOrdersListing,
  getSalesOrdersListing,
  getLinkedPurchaseOrders,
  postSOPOLinking,
  GET_SO_PO_LINKING_SUCCESS,
  GET_SO_PO_LINKING_FAILURE,
} from "../../../actions/orderTracking";
import SquareButton from "../../../components/Button/button";
import Input from "../../../components/Input/input";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import "./style.scss";

interface ISOPOLinkingModalProps {
  showModal: boolean;
  soPoMapping: ISOPOMapping;
  isFetchingLinkedPOs: boolean;
  isFetchingSalesOrders: boolean;
  isFetchingPurchaseOrders: boolean;
  unlinkedPOs: IPickListOptions[];
  salesOrderOptions: IPickListOptions[];
  existingPurchaseOrders: IPickListOptions[];
  closeModal: (refresh?: boolean) => void;
  getUnlinkedPOs: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getSalesOrdersListing: () => Promise<any>;
  getLinkedPOs: (sales_order_crm_id: number) => Promise<any>;
  postSOPOLinking: (data: {
    sales_order_crm_id: number;
    opportunity_crm_id: number;
    purchase_order_crm_ids: number[];
  }) => Promise<any>;
}

const SOPOLinkingModal: React.FC<ISOPOLinkingModalProps> = (props) => {
  const [salesOrder, setSalesOrder] = useState<number>(0);
  const [purchaseOrders, setPurchaseOrders] = useState<number[]>([]);
  const [isPostingSOPO, setIsPostingSOPO] = useState<boolean>(false);
  const [saveModalClick, setSaveModalClick] = useState<boolean>(false);
  const purchaseOrderOptions: IPickListOptions[] = useMemo(
    () => [...props.existingPurchaseOrders, ...props.unlinkedPOs],
    [props.existingPurchaseOrders, props.unlinkedPOs]
  );

  useEffect(() => {
    props.getUnlinkedPOs();
    if (!props.soPoMapping.sales_order_crm_id) props.getSalesOrdersListing();
    else {
      setSalesOrder(props.soPoMapping.sales_order_crm_id);
      props.getLinkedPOs(props.soPoMapping.sales_order_crm_id);
    }
  }, [props.soPoMapping]);

  useEffect(() => {
    setPurchaseOrders(props.existingPurchaseOrders.map((el) => el.value));
  }, [props.existingPurchaseOrders]);

  const handleChangeSO = (so: number) => {
    setSalesOrder(so);
  };

  const handleChangePO = (POs: number[]) => {
    setPurchaseOrders(POs);
  };

  const saveSOPOMapping = () => {
    setIsPostingSOPO(true);
    setSaveModalClick(true);
    // If existing purchase orders are there then, it is allowed to remove all
    // purchase orders and hence, no validation is required for that
    if (
      !salesOrder ||
      (props.existingPurchaseOrders.length === 0 && purchaseOrders.length === 0)
    ) {
      setIsPostingSOPO(false);
      return;
    }
    props
      .postSOPOLinking({
        sales_order_crm_id: salesOrder,
        opportunity_crm_id: props.soPoMapping.opportunity_crm_id,
        purchase_order_crm_ids: purchaseOrders,
      })
      .then((action) => {
        if (action.type === GET_SO_PO_LINKING_SUCCESS) {
          props.closeModal(true);
          props.addSuccessMessage("All changes applied successfully!");
        } else if (action.type === GET_SO_PO_LINKING_FAILURE) {
          props.closeModal(false);
          let linked_pos_error: string = action.errorList.data.po_linking_result.linking_errors
            .map((el: { po_number: string }) => el.po_number)
            .join(", ");
          let unlinked_pos_error: string = action.errorList.data.po_link_removal_result.purchase_order_link_remove_errors
            .map((el: { po_number: string }) => el.po_number)
            .join(", ");
          if (linked_pos_error) {
            props.addErrorMessage(
              "Unable to link purchase orders: " + linked_pos_error
            );
          }
          if (unlinked_pos_error) {
            props.addErrorMessage(
              "Unable to remove link for purchase orders: " + unlinked_pos_error
            );
          }
        } else {
          props.addErrorMessage("Server error!");
        }
      });
  };

  return (
    <ModalBase
      show={props.showModal}
      onClose={() => props.closeModal(false)}
      hideCloseButton={isPostingSOPO}
      titleElement={
        props.soPoMapping.sales_order_crm_id
          ? `Link Purchase Order(s) with Sales Order #: ${props.soPoMapping.sales_order_crm_id}`
          : "Map Sales Order with Purchase Order(s)"
      }
      bodyElement={
        <div className="link-so-po-container">
          <div className="loader">
            <Spinner show={isPostingSOPO} />
          </div>
          {props.soPoMapping.detail && (
            <p id="so-po-link-note">{props.soPoMapping.detail}</p>
          )}
          {!props.soPoMapping.sales_order_crm_id && (
            // This case is not yet handled and currently, sales_order_crm_id
            // is assumed to be there each time this modal is mounted
            <Input
              field={{
                label: "Sales Order",
                type: "PICKLIST",
                value: salesOrder,
                options: props.salesOrderOptions,
                isRequired: true,
              }}
              className="link-so-po-input"
              width={6}
              multi={false}
              name="salesOrder"
              onChange={(e) => handleChangeSO(e.target.value)}
              placeholder={`Select Sales Order`}
              loading={props.isFetchingSalesOrders}
              error={
                Boolean(saveModalClick && !salesOrder) && {
                  errorState: "error",
                  errorMessage: "Please select a sales order",
                }
              }
            />
          )}
          <Input
            field={{
              label: "Purchase Orders",
              type: "PICKLIST",
              value: purchaseOrders,
              options: purchaseOrderOptions,
              isRequired: true,
            }}
            className="link-so-po-input"
            width={8}
            multi={true}
            name="purchaseOrders"
            onChange={(e) => handleChangePO(e.target.value)}
            placeholder={`Select Purchase Orders`}
            loading={
              props.isFetchingPurchaseOrders || props.isFetchingLinkedPOs
            }
            error={
              Boolean(
                saveModalClick &&
                  props.existingPurchaseOrders.length === 0 &&
                  purchaseOrders.length === 0
              ) && {
                errorState: "error",
                errorMessage: "Please select at least one purchase order",
              }
            }
          />
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={"default"}
            onClick={() => props.closeModal(false)}
            className="save-unlink-so-po"
            disabled={isPostingSOPO}
          />
          <SquareButton
            content={`Save`}
            bsStyle={"primary"}
            onClick={saveSOPOMapping}
            className="save-unlink-so-po"
            disabled={
              isPostingSOPO ||
              (props.existingPurchaseOrders.length === 0 &&
                purchaseOrders.length === 0)
            }
          />
        </div>
      }
      className={"link-so-po-modal"}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  existingPurchaseOrders: state.orderTracking.linkedPOs,
  salesOrderOptions: state.orderTracking.salesOrders,
  unlinkedPOs: state.orderTracking.purchaseOrders,
  isFetchingSalesOrders: state.orderTracking.isFetchingSalesOrders,
  isFetchingPurchaseOrders: state.orderTracking.isFetchingPurchaseOrders,
  isFetchingLinkedPOs: state.orderTracking.isFetchingLinkedPOs,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  postSOPOLinking: (data: {
    sales_order_crm_id: number;
    opportunity_crm_id: number;
    purchase_order_crm_ids: number[];
  }) => dispatch(postSOPOLinking(data)),
  getSalesOrdersListing: () => dispatch(getSalesOrdersListing()),
  getUnlinkedPOs: () => dispatch(getUnlinkedPurchaseOrdersListing()),
  getLinkedPOs: (sales_order_crm_id: number) =>
    dispatch(getLinkedPurchaseOrders(sales_order_crm_id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SOPOLinkingModal);
