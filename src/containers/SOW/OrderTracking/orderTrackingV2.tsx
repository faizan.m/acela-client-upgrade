import React, { Component } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
// import TagsInput from "react-tagsinput"; (New Component)
import {
  getListPOTickts,
  postLineItemsData,
  fetchDeviceCategories,
  getLineItemsIngramData,
  getLineItemsOperations,
  getListPurchaseOrdersAll,
  getListPurchaseOrdersAllStatus,
  LINE_ITEMS_SUCCESS,
  INGRAM_DATA_SUCCESS,
  FETCH_POTICKETS_SUCCESS,
  POST_LINE_ITEMS_FAILURE,
  POST_LINE_ITEMS_SUCCESS,
} from "../../../actions/inventory";
import {
  getShipmentMethods,
  getPurchaseOrderSettings,
} from "../../../actions/setting";
import {
  fetchTerritoryMembers,
  fetchAllProviderIntegrations,
  FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS,
} from "../../../actions/provider/integration";
import {
  getDataCommon,
  GET_DATA_SUCCESS,
} from "../../../actions/orderTracking";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../../actions/documentation";
import { getQuoteStageList, getQuoteTypeList } from "../../../actions/sow";
import { fetchTypes, fetchManufacturers } from "../../../actions/inventory";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import Panel from "../../../components/Panel/panel";
import Checkbox from "../../../components/Checkbox/checkbox";
import SquareButton from "../../../components/Button/button";
import TooltipCustom from "../../../components/Tooltip/tooltip";
// import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import { commonFunctions } from "../../../utils/commonFunctions";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import "../../../commonStyles/orderTracking.scss";
import "./style.scss";

interface IReceivingState {
  bulkEdit: any;
  errors: object;
  loading: boolean;
  lineItems: CustomLineItem[];
  opportunity: any;
  ticket_id: number;
  configuration: any;
  customerId: number;
  inputValue: string;
  ticket_note: string;
  customer_id: number;
  showSearch: boolean;
  loadingNote: boolean;
  showBulkEdit: boolean;
  categoryIds: number[];
  selectedRows: number[];
  loadingTickts: boolean;
  showConfigAdd: boolean;
  selectAllRows: boolean;
  // isopenConfirm: boolean;
  shouldFetchIngram: boolean;
  purchase_order_id: number;
  loadingLineItems: boolean;
  ticketNoteUpated: boolean;
  showReceivedOnly: boolean;
  loadingIngramData: boolean;
  toExcludeServices: boolean;
  poTickets: IPickListOptions[];
  dataIndex: number | undefined;
  showCreateOpportunity: boolean;
  fetchingCustomersUsers: boolean;
  customerUsers: IPickListOptions[];
  ticket_last_note: {
    text: string;
    last_updated_on?: string;
  };
  error: {
    category_id: IFieldValidation;
    manufacturer_id: IFieldValidation;
    service_contract_number: IFieldValidation;
    notes: IFieldValidation;
    expiration_date: IFieldValidation;
    opportunity_name: IFieldValidation;
    opportunity_type: IFieldValidation;
    opportunity_stage: IFieldValidation;
    inside_rep: IFieldValidation;
    user_id: IFieldValidation;
    installation_date: IFieldValidation;
  };
}

interface IReceivingProps extends ICommonProps {
  types: string[];
  customerId: number;
  isFetching: boolean;
  terretoryMembers: any[];
  purchaseOrdersAll: any[];
  qTypeList: IDLabelObject[];
  isFetchingMembers: boolean;
  qStageList: IDLabelObject[];
  customers: ICustomerShort[];
  isFetchingShipments: boolean;
  shipments: IPickListOptions[];
  manufacturers: IDLabelObject[];
  isFetchingDeviceCategories: boolean;
  deviceCategoryList: IDeviceCategory[];
  purchaseOrderSetting: IPurchaseOrderSetting;
  fetchTypes: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  getQuoteTypeList: () => Promise<any>;
  getQuoteStageList: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  fetchManufacturers: () => Promise<any>;
  getShipmentMethods: () => Promise<any>;
  fetchDeviceCategories: () => Promise<any>;
  fetchTerritoryMembers: () => Promise<any>;
  getDataCommon: (url: string) => Promise<any>;
  getPurchaseOrderSettings: () => Promise<any>;
  getListPurchaseOrdersAll: () => Promise<any>;
  fetchAllProviderIntegrations: () => Promise<any>;
  getListPOTickts: (customerId: number) => Promise<any>;
  getListPurchaseOrdersAllStatus: (showAll: boolean) => Promise<any>;
  getLineItemsOperations: (purchase_order_ids: number[]) => Promise<any>;
  fetchAllCustomerUsers: (customerId: number, params: any) => Promise<any>;
  getLineItemsIngramData: (
    purchase_orders: string[],
    line_item_identifiers: string[]
  ) => Promise<any>;
  postLineItemsData: (
    customer_id: number,
    ids: number,
    ti: number,
    tn: string,
    li: any
  ) => Promise<any>;
}

class OrderTrackingReceiving extends Component<
  IReceivingProps,
  IReceivingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IReceivingProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    ticket_id: 0,
    errors: null,
    bulkEdit: {},
    poTickets: [],
    lineItems: [],
    loading: false,
    inputValue: "",
    ticket_note: "",
    opportunity: {},
    categoryIds: [],
    selectedRows: [],
    customer_id: null,
    customerUsers: [],
    configuration: {},
    showSearch: false,
    loadingNote: false,
    showBulkEdit: false,
    // isopenConfirm: false,
    loadingTickts: false,
    showConfigAdd: false,
    dataIndex: undefined,
    selectAllRows: false,
    customerId: undefined,
    ticket_last_note: null,
    purchase_order_id: null,
    loadingLineItems: false,
    ticketNoteUpated: false,
    showReceivedOnly: false,
    loadingIngramData: false,
    shouldFetchIngram: false,
    toExcludeServices: false,
    showCreateOpportunity: false,
    fetchingCustomersUsers: false,
    error: {
      category_id: { ...OrderTrackingReceiving.emptyErrorState },
      manufacturer_id: { ...OrderTrackingReceiving.emptyErrorState },
      service_contract_number: { ...OrderTrackingReceiving.emptyErrorState },
      notes: { ...OrderTrackingReceiving.emptyErrorState },
      expiration_date: { ...OrderTrackingReceiving.emptyErrorState },
      opportunity_name: { ...OrderTrackingReceiving.emptyErrorState },
      opportunity_type: { ...OrderTrackingReceiving.emptyErrorState },
      opportunity_stage: { ...OrderTrackingReceiving.emptyErrorState },
      inside_rep: { ...OrderTrackingReceiving.emptyErrorState },
      user_id: { ...OrderTrackingReceiving.emptyErrorState },
      installation_date: { ...OrderTrackingReceiving.emptyErrorState },
    },
  });

  componentDidMount() {
    this.props.fetchTypes();
    this.props.fetchManufacturers();
    this.props.getQuoteStageList();
    this.props.getQuoteTypeList();
    this.props.getShipmentMethods();
    this.props.getListPurchaseOrdersAll().then(() => {
      const queryParams = new URLSearchParams(this.props.location.search);
      const purchaseOrderId = parseInt(queryParams.get("po-number"));
      const companyCRMId = queryParams.get("company");
      if (purchaseOrderId && companyCRMId) {
        this.setState(
          {
            purchase_order_id: purchaseOrderId,
          },
          () => {
            this.getPOData(purchaseOrderId, companyCRMId);
            // this.getLastNote(purchaseOrderId)
          }
        );
      }
    });
    this.props.getPurchaseOrderSettings();
    this.props.fetchDeviceCategories();
    this.props.fetchTerritoryMembers();
    this.props.fetchAllProviderIntegrations().then((action) => {
      if (action.type === FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS) {
        const categoryIds = action.response.find(
          (int) => int.type === "CONNECTWISE"
        ).other_config;
        this.setState({
          categoryIds:
            categoryIds &&
            categoryIds.empty_serial_mapping &&
            categoryIds.empty_serial_mapping.device_category_ids,
        });
      }
    });
  }

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newstate = this.state;
    newstate[e.target.name] = e.target.value;
    (newstate.ticket_note as string) = "";
    if (e.target.name === "ticket_id") {
      if (newstate.ticket_last_note) {
        (newstate.ticket_last_note.text as string) = "";
        (newstate.loadingNote as boolean) = true;
      }
      this.getLastNote(Number(e.target.value));
    }
    this.setState(newstate);
    if (e.target.name === "purchase_order_id") {
      this.getPOData(Number(e.target.value));
    }
  };

  getPOData = (purchase_order_id: number, companyCRMId?: string) => {
    this.getLineItemsOperations(purchase_order_id);
    const POID = purchase_order_id;
    let CRMID = companyCRMId;
    if (!companyCRMId) {
      CRMID = this.props.purchaseOrdersAll.find((x) => x.id === POID)
        .customerCompany.id;
    }
    const acelaID = this.props.customers.find(
      (x) => x.crm_id === parseInt(CRMID)
    ).id;
    this.getListPOTickts(acelaID);
    this.getAllCustomerUsers(acelaID);
    this.setState({
      customerId: acelaID,
      lineItems: [],
      ticket_id: 0,
      ticket_note: "",
    });
  };

  getCustomerOptions = () => {
    if (this.props.customers && this.props.customers.length > 0) {
      return this.props.customers.map((role) => ({
        value: role.id,
        label: role.name,
      }));
    } else {
      return [];
    }
  };

  handleExcludeServicesChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      toExcludeServices: e.target.checked,
    });
  };

  handleShouldFetchIngram = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      {
        shouldFetchIngram: e.target.checked,
      },
      () => {
        if (this.state.lineItems.length && e.target.checked) {
          this.getLineItemsIngramData();
        }
      }
    );
  };

  renderHeader = () => {
    return (
      <div className="ot-v2-header col-md-12">
        <Input
          field={{
            label: "Purchase Order",
            type: "PICKLIST",
            value: this.state.purchase_order_id,
            isRequired: true,
            options:
              this.props.purchaseOrdersAll &&
              this.props.purchaseOrdersAll.map((d) => {
                return {
                  label: `${d.poNumber} (${(d.customerCompany &&
                    d.customerCompany.name) ||
                    ""})`,
                  value: d.id,
                };
              }),
          }}
          width={6}
          loading={this.props.isFetching}
          placeholder="Select Purchase Order"
          name="purchase_order_id"
          className="purchaseOrder-select"
          onChange={this.handleChange}
          multi={false}
        />
        <Input
          field={{
            label: "Ticket",
            type: "PICKLIST",
            value: this.state.ticket_id,
            isRequired: false,
            options: this.state.poTickets,
          }}
          disabled={!this.state.purchase_order_id}
          loading={this.state.loadingTickts}
          width={6}
          placeholder="Select Ticket"
          name="ticket_id"
          className="purchaseOrder-select"
          onChange={(e) => this.handleChange(e)}
          multi={false}
        />
      </div>
    );
  };

  getAllCustomerUsers = (customerID: number) => {
    this.setState({ fetchingCustomersUsers: true });
    this.props
      .fetchAllCustomerUsers(customerID, { is_active: true })
      .then((action) => {
        if (action.type === FETCH_ALL_CUST_USERS_SUCCESS) {
          const activeCustomerUsers = [];
          action.response.map((data: ISuperUser) => {
            if (data.is_active) {
              activeCustomerUsers.push({
                value: data.id,
                label: `${data.first_name} ${data.last_name}`,
              });
            }
          });
          this.setState({ customerUsers: activeCustomerUsers });
        }
        this.setState({ fetchingCustomersUsers: false });
      });
  };

  getListPOTickts = (customerId: number) => {
    this.setState({ loadingTickts: true });
    this.props.getListPOTickts(customerId).then((action) => {
      if (action.type === FETCH_POTICKETS_SUCCESS) {
        const poTicketsRaw: IServiceTicket[] = action.response;
        const poTickets = poTicketsRaw
          ? poTicketsRaw.map((c) => ({
              value: c.id,
              label: ` ${c.id}-${c.summary}`,
            }))
          : [];

        const poNumberData = this.props.purchaseOrdersAll.find(
          (x) => x.id === this.state.purchase_order_id
        );
        const poNumber = poNumberData && poNumberData.poNumber;
        const ticket = poTickets.find((x) => x.label.includes(poNumber));
        const ticket_id = ticket && ticket.value;
        this.setState(
          {
            loadingNote: true,
            poTickets,
            ticket_last_note: {
              text: "",
            },
            loadingTickts: false,
            ticket_id,
          },
          () => {
            ticket_id && this.getLastNote(ticket_id);
          }
        );
      }
    });
  };

  getLastNote = (purchase_order_id: number) => {
    this.props
      .getDataCommon(
        `providers/purchase-order-tickets/${purchase_order_id}/notes`
      )
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          const ticket_last_note = action.response;
          this.setState({ ticket_last_note, loadingNote: false });
        }
        this.setState({ loadingNote: false });
      });
  };

  // toggleConfirmOpen = () => {
  //   this.setState({
  //     isopenConfirm: !this.state.isopenConfirm,
  //   });
  // };

  // handleSubmit = () => {
  //   const poNumberData = this.props.purchaseOrdersAll.find(
  //     (x) => x.id === this.state.purchase_order_id
  //   );
  //   const poNumber = poNumberData && poNumberData.poNumber;
  //   this.props.history.push(
  //     `/operations/email-generation?po-number=${poNumber}`
  //   );
  //   this.setState({
  //     isopenConfirm: !this.state.isopenConfirm,
  //   });
  // };

  getLineItemsOperations = (value: number) => {
    this.setState({ loadingLineItems: true });
    this.props.getLineItemsOperations([value]).then((action) => {
      if (action.type === LINE_ITEMS_SUCCESS) {
        const lineItems: ILineItem[] = action.response;

        const newLineItems: CustomLineItem[] = lineItems.map((item) => {
          return {
            ...item,
            serial_numbers: commonFunctions.getArrFromString(
              item.serialNumbers
            ),
            tracking_number: commonFunctions.getArrFromString(
              item.trackingNumber
            ),
            shipping_method_id:
              item.shipmentMethod && item.shipmentMethod.id
                ? item.shipmentMethod.id
                : null,
            ship_date: item.shipDate ? item.shipDate : "",
            estimated_delivery_date: item.expectedShipDate
              ? item.expectedShipDate
              : "",
          };
        });

        this.setState(
          { lineItems: newLineItems, loadingLineItems: false },
          () => {
            if (this.state.lineItems.length && this.state.shouldFetchIngram) {
              this.getLineItemsIngramData();
            }
          }
        );
      }
      this.setState({ loadingLineItems: false });
    });
  };

  getLineItemsIngramData = () => {
    this.setState({ loadingIngramData: true });
    this.props
      .getLineItemsIngramData(
        [
          this.props.purchaseOrdersAll.find(
            (x) => x.id === this.state.purchase_order_id
          ).poNumber,
        ],
        this.state.lineItems.map((item) => item.product.identifier, [
          this.state.purchase_order_id,
        ])
      )
      .then((action) => {
        if (action.type === INGRAM_DATA_SUCCESS) {
          const ingramData: IIngramData = action.response;
          const newstate = this.state;
          this.state.lineItems.map((item, index) => {
            if (
              item.product.identifier &&
              ingramData[item.product.identifier]
            ) {
              newstate.lineItems[index].shipping_method_id =
                this.props.purchaseOrderSetting &&
                this.props.purchaseOrderSetting.extra_config &&
                this.props.purchaseOrderSetting.extra_config
                  .carrier_tracking_url &&
                this.props.purchaseOrderSetting.extra_config
                  .carrier_tracking_url[
                  ingramData[item.product.identifier].carrier
                ] &&
                this.props.purchaseOrderSetting.extra_config
                  .carrier_tracking_url[
                  ingramData[item.product.identifier].carrier
                ].connectwise_shipper;
              newstate.lineItems[index].serial_numbers =
                ingramData[item.product.identifier].serial_numbers || [];
              newstate.lineItems[index].ship_date =
                ingramData[item.product.identifier].ship_date;
              newstate.lineItems[index].estimated_delivery_date =
                ingramData[item.product.identifier].estimated_delivery_date;
              newstate.lineItems[index].tracking_number =
                ingramData[item.product.identifier].tracking_number || [];
              newstate.lineItems[index].shipped_quantity =
                ingramData[item.product.identifier].shipped_quantity;
              newstate.lineItems[index].carrier =
                ingramData[item.product.identifier].carrier;
              newstate.lineItems[index].isIngram = true;
            }
          });
          (newstate.loadingIngramData as boolean) = false;
          this.setState(newstate);
        }

        this.setState({ loadingIngramData: false });
      });
  };

  addOneHourToISO = (date: string) => {
    let d = new Date(date);
    return new Date(d.setHours(d.getHours() + 8)).toISOString();
  };

  postLineItemsData = () => {
    this.setState({ errors: null, loading: true });
    const lineItemsState = cloneDeep(this.state.lineItems);
    const line_items = [];
    lineItemsState
      .filter((x) => x.selected)
      .forEach((item) => {
        const data = {
          line_item_id: item.id,
          tracking_number: item.tracking_number.join(",") || "",
          ship_date: item.ship_date ? this.addOneHourToISO(item.ship_date) : "",
          serial_numbers: item.serial_numbers || [],
          // receive_all: item.receive_all,
          note: item.internalNotes,
          shipping_method_id: item.shipping_method_id,
          received_qty: Math.round(item.received_qty) || 0,
          received_status: item.received_status || "",
        };

        if (item.configuration && Object.keys(item.configuration).length > 0) {
          item.configuration.installation_date = item.configuration.installation_date
            .toISOString(true)
            .replace(/\+.*/, "Z");

          item.configuration.expiration_date = item.configuration.expiration_date
            .toISOString(true)
            .replace(/\+.*/, "Z");

          if (
            item.configuration &&
            Object.keys(item.configuration).length > 0
          ) {
            data["configuration"] = item.configuration;
            data["configuration"]["product_id"] = item.product.identifier;
          }

          if (item.opportunity && Object.keys(item.opportunity).length > 0) {
            data["opportunity"] = item.opportunity;
          }
        }

        line_items.push(data);
      });
    this.setState({ lineItems: lineItemsState });
    this.props
      .postLineItemsData(
        this.state.customerId,
        this.state.purchase_order_id,
        this.state.ticket_id,
        this.state.ticket_note,
        line_items
      )
      .then((action) => {
        if (action.type === POST_LINE_ITEMS_SUCCESS) {
          this.props.addSuccessMessage(
            `Product${line_items.length > 1 ? "s" : ""} updated successfully!`
          );
          this.getLineItemsOperations(this.state.purchase_order_id);
          this.setState({
            selectAllRows: false,
            loading: false,
          });
        }
        if (action.type === POST_LINE_ITEMS_FAILURE) {
          let errorMsg = "";
          const errorData = action.errorList.data;

          if (Array.isArray(errorData)) {
            errorData.map((err) => {
              Object.keys(err).map((key) => {
                errorMsg = errorMsg + err[key];
              });
            });
          } else if (typeof errorData === "object") {
            Object.keys(errorData).forEach((key, idx, arr) => {
              errorMsg = errorMsg + errorData[key];
              if (idx !== arr.length - 1) errorMsg += "\n";
            });
          }

          this.props.addErrorMessage(errorMsg);
          this.setState({
            selectAllRows: false,
            errors: action.errorList.data,
            loading: false,
          });
        }
      });
  };

  onRowsToggle = (selectedRows: number[]) => {
    const newState = this.state;

    this.state.lineItems.map((row, i) => {
      newState.lineItems[i].selected = false;
    });

    selectedRows.map((id, index) => {
      this.state.lineItems.map((row, i) => {
        if (row.id === id) {
          newState.lineItems[i].selected = true;
        }
      });
    });

    this.setState(
      {
        lineItems: newState.lineItems,
      },
      () => {
        this.setState({
          selectedRows,
        });
      }
    );
  };

  onRowsCheck = (e: React.ChangeEvent<HTMLInputElement>, data) => {
    const newState = this.state;
    const isChecked = e.target.checked;

    this.state.lineItems.map((row, i) => {
      if (data.id === row.id) newState.lineItems[i].selected = isChecked;
    });

    let selectionCount = 0;
    newState.lineItems.map((item) => {
      if (item.selected) selectionCount++;
    });

    this.setState({
      lineItems: newState.lineItems,
      selectAllRows: selectionCount === newState.lineItems.length,
    });
  };

  handleChangeTable = (
    event: React.ChangeEvent<HTMLInputElement>,
    index?: number
  ) => {
    const lineItems = cloneDeep(this.state.lineItems);
    lineItems[index][event.target.name] = event.target.value;
    this.setState({
      lineItems,
    });
  };

  handleChangeTableSN = (event: string[], index?: number) => {
    const lineItems = cloneDeep(this.state.lineItems);
    lineItems[index].serial_numbers = event;
    this.setState({
      lineItems,
    });
  };

  handleChangeTableTrackingNo = (event: string[], index?: number) => {
    const lineItems = cloneDeep(this.state.lineItems);
    lineItems.forEach((x) => (x.error = ""));
    lineItems[index].tracking_number = [];
    event.map((e, i) => {
      if (e && e.length < 51) {
        lineItems[index].tracking_number[i] = e;
      } else {
        lineItems[index].error = "maximum 50 characters are allowed.";
      }
    });
    this.setState({
      lineItems,
    });
  };

  handleBulkEditTrackingNumberChange = (tracking_nos: string[]) => {
    const bulkEdit = { ...this.state.bulkEdit };
    bulkEdit.trackingNumber = tracking_nos.join(",");
    this.setState({
      bulkEdit,
    });
  };

  handleChangeTableCheckBox = (
    event: React.ChangeEvent<HTMLInputElement>,
    data?: any
  ) => {
    const lineItems = cloneDeep(this.state.lineItems);
    lineItems[data.index][event.target.name] = event.target.checked;
    this.setState({
      lineItems,
    });
  };

  changeRecievedOty = (option: string, data: any, index: number) => {
    const lineItems = this.state.lineItems;

    lineItems[index].recievedQtyOption = option;

    if (option === "all") {
      lineItems[index].received_qty = data.quantity;
      lineItems[index].received_status = "FullyReceived";
    }
    if (option === "none") lineItems[index].received_qty = 0;

    this.setState({
      lineItems,
    });
  };

  changeCustomRecievedOty = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const lineItems = this.state.lineItems;
    if (e.target.value !== "") {
      lineItems[index].received_qty = parseInt(e.target.value);
      lineItems[index].received_status = "PartiallyReceiveCloneRest";
      this.setState({
        lineItems,
      });
    }
  };

  setValidationErrors = (errorList: object) => {
    const newState = cloneDeep(this.state);

    Object.keys(errorList).map((key) => {
      newState.error[key].errorState = "error";
      newState.error[key].errorMessage = errorList[key];
    });
    this.setState(newState);
  };

  applyBulkEdit = () => {
    // edit selected orders.
    if (Object.keys(this.state.bulkEdit).length === 0) return;

    const newState = this.state;

    this.state.lineItems.map((row, i) => {
      if (row.selected) {
        if (this.state.bulkEdit.trackingNumber) {
          let newTrackingNumbers =
            (this.state.bulkEdit.trackingNumber !== "" &&
              this.state.bulkEdit.trackingNumber.split(",")) ||
            [];

          newState.lineItems[i].tracking_number = [
            ...newState.lineItems[i].tracking_number,
            ...newTrackingNumbers,
          ];
        }

        if (this.state.bulkEdit.shipDate) {
          newState.lineItems[i].ship_date = this.state.bulkEdit.shipDate;
        }

        if (this.state.bulkEdit.shipping_method_id) {
          newState.lineItems[
            i
          ].shipping_method_id = this.state.bulkEdit.shipping_method_id;
        }

        if (
          this.state.bulkEdit.internalNotes &&
          this.state.bulkEdit.internalNotes !== ""
        ) {
          newState.lineItems[
            i
          ].internalNotes = this.state.bulkEdit.internalNotes;
        }

        if (this.state.bulkEdit.isRecieveAllSelected) {
          newState.lineItems[i].received_qty = newState.lineItems[i].quantity;
          newState.lineItems[i].recievedQtyOption = "all";
        }
      }
    });

    this.setState({
      lineItems: newState.lineItems,
      bulkEdit: {},
      showBulkEdit: false,
    });
  };

  setSelectAllRows = (event: React.ChangeEvent<HTMLInputElement>) => {
    const lineItems = cloneDeep(this.state.lineItems);
    const checkValue = event.target.checked;

    this.state.lineItems.map((row, i) => {
      lineItems[i].selected = checkValue;
    });

    this.setState({
      lineItems,
      selectAllRows: checkValue,
    });
  };

  unCheckConfig = (dataIndex: number) => {
    const newState = this.state;

    newState.lineItems[dataIndex].configuration = {};
    newState.lineItems[dataIndex].opportunity = {};
    newState.lineItems[dataIndex].configAdded = false;

    this.setState({
      lineItems: newState.lineItems,
    });
  };

  sortBy = (itemA: CustomLineItem, itemB: CustomLineItem) => {
    let valueA = itemA.shipped_quantity;
    let valueB = itemB.shipped_quantity;

    let a = valueA || 0;
    let b = valueB || 0;
    let r = b - a;
    if (r === 0) {
      // Next line makes the magic :)
      r =
        typeof itemA.id !== "undefined" && typeof itemB.id !== "undefined"
          ? itemA.id - itemB.id
          : 0;
    }
    return r;
  };

  renderPurchaseOrders = () => {
    const {
      lineItems,
      toExcludeServices,
      inputValue,
      showReceivedOnly,
    } = this.state;
    let filteredLineItems = lineItems;

    if (toExcludeServices) {
      filteredLineItems = filteredLineItems.filter(
        (item) => !item.product.identifier.startsWith("CON")
      );
    }

    if (inputValue !== "") {
      filteredLineItems = filteredLineItems.filter(
        (item) =>
          item.product.identifier
            .toLowerCase()
            .includes(inputValue.toLowerCase()) ||
          item.description.toLowerCase().includes(inputValue.toLowerCase())
      );
    }

    const IngramTooltip = (
      <img
        src="/assets/new-icons/i.png"
        alt="Ingram icon"
        title="Ingram Data"
        className="ingram-tooltip"
      />
    );

    if (showReceivedOnly) {
      filteredLineItems = filteredLineItems.filter(
        (a) => a.partially_received_quantity > 0
      );
    }
    filteredLineItems = filteredLineItems.sort((a, b) => this.sortBy(a, b));

    return filteredLineItems.length ? (
      filteredLineItems.map((data, index) => (
        <div
          key={data.id}
          className={`product-details-container ${
            data.selected ? "selected" : ""
          } ${data.isIngram ? "shipped-quantity" : ""}`}
        >
          <div className="product-detail top-secton">
            <div className="product-id">
              <div className="detail-title" title={String(data.id)}>
                Product ID
              </div>
              <Checkbox
                isChecked={data.selected}
                name="receive_all"
                onChange={(e) => this.onRowsCheck(e, data)}
              >
                <div className="product-data">{data.product.identifier}</div>
              </Checkbox>
            </div>
            <div className="product-desc">
              <div className="detail-title">Description</div>
              <div className="product-data po-description">
                {data.description}
              </div>
            </div>
            <div className="product-qty-section">
              <div>
                <div className="detail-title" style={{ textAlign: "center" }}>
                  {data.partially_received_quantity > 0
                    ? "Remaining"
                    : "Purchased Qty"}
                </div>
                <div className="quantity-pending product-data purchased-qty">
                  {data.quantity}
                </div>
              </div>
              <div>
                <div className="detail-title" style={{ textAlign: "center" }}>
                  Shipped Qty {data.isIngram && IngramTooltip}
                </div>
                <div className="quantity-shipped product-data shipped-qty">
                  {data.shipped_quantity ? data.shipped_quantity : "-"}
                </div>
              </div>
              <div>
                <div className="detail-title" style={{ textAlign: "center" }}>
                  Received Qty
                </div>
                <div className="quantity-received product-data shipped-qty">
                  {data.partially_received_quantity
                    ? data.partially_received_quantity
                    : "-"}
                </div>
              </div>
              <div className="recieved-qty-actions">
                <div className="detail-title">Receiving</div>
                <div className="product-data qty-action-tabs">
                  <div
                    className={`or-action-item ${["none", undefined].includes(
                      data.recievedQtyOption
                    ) && "active"}`}
                    onClick={() => this.changeRecievedOty("none", data, index)}
                  >
                    None
                  </div>
                  <div
                    className={`or-action-item ${data.recievedQtyOption ===
                      "all" && "active"}`}
                    onClick={() => this.changeRecievedOty("all", data, index)}
                  >
                    All
                  </div>
                  {data.recievedQtyOption !== "custom" ? (
                    <div
                      className={`or-action-item ${data.recievedQtyOption ===
                        "custom" && "active"}`}
                      onClick={() =>
                        this.changeRecievedOty("custom", data, index)
                      }
                    >
                      Custom
                    </div>
                  ) : (
                    <Input
                      field={{
                        label: "",
                        type: "NUMBER",
                        isRequired: false,
                        value: data.received_qty || 0,
                      }}
                      disabled={false}
                      width={12}
                      name="received_qty"
                      onChange={(e) => this.changeCustomRecievedOty(e, index)}
                      className="custom-recieved-qty"
                    />
                  )}
                </div>
              </div>
            </div>
            <div className="add-config-or">
              {(!data.configuration ||
                Object.keys(data.configuration).length === 0) &&
              (!data.opportunity ||
                Object.keys(data.opportunity).length === 0) ? (
                <div
                  className="config"
                  onClick={() =>
                    this.setState({ showConfigAdd: true, dataIndex: index })
                  }
                >
                  Add Configuration
                </div>
              ) : (
                <>
                  <Checkbox
                    isChecked={data.configAdded}
                    name="configuration_added"
                    onChange={() => this.unCheckConfig(index)}
                  />
                  <div
                    title=""
                    className="detail-title config"
                    onClick={() =>
                      this.setState({
                        showConfigAdd: true,
                        dataIndex: index,
                        configuration: { ...data.configuration },
                        opportunity: { ...data.opportunity },
                      })
                    }
                  >
                    Configuration
                  </div>
                  <TooltipCustom className="config-tooltip-wrapper">
                    <div className="custom-config-tooltip">
                      <div className="title">Configuration added</div>
                      <div>
                        If you do not wish to send this configuration to
                        connectwise then uncheck the checkbox
                      </div>
                    </div>
                  </TooltipCustom>
                </>
              )}
            </div>
          </div>
          <div className="product-detail middle-section">
            <div className="tag-input">
              <div className="detail-title">
                Tracking Number {data.isIngram && IngramTooltip}
              </div>
              {/* <TagsInput
                value={data.tracking_number}
                onChange={(e) => this.handleChangeTableTrackingNo(e, index)}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Tracking Numbers",
                }}
                addOnBlur={true}
              /> */}
              {
                <div className="error-tag-input">
                  {this.state.lineItems[index].error}
                </div>
              }
            </div>
            <div className="tag-input">
              <div className="detail-title">
                Serial Number {data.isIngram && IngramTooltip}
              </div>
              {/* <TagsInput
                value={data.serial_numbers}
                onChange={(e) => this.handleChangeTableSN(e, index)}
                inputProps={{
                  className: "react-tagsinput-input",
                  placeholder: "Enter Serial Numbers",
                }}
                addOnBlur={true}
              /> */}
            </div>
            <div className="mapped-carrier-container">
              <div className="detail-title">
                Carrier {data.isIngram && IngramTooltip}
              </div>
              <Input
                field={{
                  label: "",
                  type: "PICKLIST",
                  isRequired: false,
                  value: data.shipping_method_id,
                  options: this.props.shipments,
                }}
                disabled={false}
                width={12}
                name="shipping_method_id"
                loading={this.props.isFetchingShipments}
                onChange={(e) => this.handleChangeTable(e, index)}
                className="product-data"
              />
            </div>
            <div>
              <div className="detail-title">
                Ship Date {data.isIngram && IngramTooltip}
              </div>
              <Input
                field={{
                  label: "",
                  type: "DATE",
                  isRequired: false,
                  value: data.ship_date,
                }}
                disabled={false}
                showTime={false}
                width={12}
                name="ship_date"
                onChange={(e) => this.handleChangeTable(e, index)}
                className="product-data"
              />
            </div>
            <div>
              <div className="detail-title">
                Estimated Ship Date {data.isIngram && IngramTooltip}
              </div>
              <Input
                field={{
                  label: "",
                  type: "TEXT",
                  isRequired: false,
                  value:
                    fromISOStringToFormattedDate(
                      data.estimated_delivery_date,
                      "MM-DD-YYYY"
                    ) || "-",
                }}
                disabled={true}
                width={12}
                name="internalNotes"
                onChange={(e) => {}}
                className="product-data"
              />
            </div>
          </div>
        </div>
      ))
    ) : (
      <div className="no-data po-no-data">No Data Found</div>
    );
  };

  renderBulkEditPanel = () => {
    return (
      <div className="bulk-parent">
        <div className="bulk-edit-container">
          <div className="title">BULK EDITT</div>
          <div className="info-top">
            Enter values in the fields that need to be updated for the selected
            product entries
          </div>
          <div className="tag-input">
            <div className="detail-title">Tracking Number</div>
            {/* <TagsInput
              value={
                this.state.bulkEdit.trackingNumber &&
                this.state.bulkEdit.trackingNumber !== ""
                  ? this.state.bulkEdit.trackingNumber.split(",")
                  : []
              }
              onChange={(e) => this.handleBulkEditTrackingNumberChange(e)}
              inputProps={{
                className: "react-tagsinput-input",
                placeholder: "Enter Tracking Numbers",
              }}
              addOnBlur={true}
            /> */}
          </div>
          <div className="bulk-ship-date">
            <div className="bulk-detail-title">Ship Date</div>
            <Input
              field={{
                label: "",
                type: "DATE",
                isRequired: false,
                value: this.state.bulkEdit.shipDate,
              }}
              disabled={false}
              showTime={false}
              width={12}
              name="shipDate"
              onChange={(e) => this.handleBulkEditChange(e)}
              className="bulk-product-data"
            />
          </div>
          <div className="mapped-carrier-container">
            <div className="bulk-detail-title">Carrier</div>
            <Input
              field={{
                label: "",
                type: "PICKLIST",
                isRequired: false,
                value: this.state.bulkEdit.shipping_method_id,
                options: this.props.shipments,
              }}
              disabled={false}
              width={12}
              name="shipping_method_id"
              onChange={(e) => this.handleBulkEditChange(e)}
              className="bulk-product-data"
            />
          </div>
          <Checkbox
            isChecked={this.state.bulkEdit.isRecieveAllSelected}
            name="isRecieveAllSelected"
            className="bulk-recieve-all-checkbox"
            onChange={(e) => {
              const bulkEdit = { ...this.state.bulkEdit };
              bulkEdit["isRecieveAllSelected"] = e.target.checked;
              this.setState({
                bulkEdit,
              });
            }}
          >
            <div className="bulk-detail-title">Recieved all</div>
          </Checkbox>
          <div className="bulk-internal-notes">
            <div className="bulk-detail-title">Notes</div>
            <Input
              field={{
                label: "",
                type: "TEXTAREA",
                isRequired: false,
                value: this.state.bulkEdit.internalNotes,
              }}
              disabled={false}
              width={12}
              name="internalNotes"
              onChange={(e) => this.handleBulkEditChange(e)}
              className="product-data"
            />
          </div>
          <div className="info-bottom">
            <img
              style={{ marginRight: "6px" }}
              width="18px"
              height="18px"
              src={`/assets/icons/caution.svg`}
              className="status-svg-images"
              alt=""
              title={""}
            />
            Clicking on apply will update these values to the table but the
            updates will not be sent to connectwise
          </div>
        </div>
        <div className="bulk-actions-footer">
          <SquareButton
            content={`CANCEL`}
            bsStyle={"primary"}
            onClick={(e) =>
              this.setState({ bulkEdit: {}, showBulkEdit: false })
            }
            className="bulk-cancel-or"
          />
          <SquareButton
            content={`APPLY`}
            bsStyle={"primary"}
            onClick={(e) => this.applyBulkEdit()}
            className="bulk-apply-or"
          />
        </div>
      </div>
    );
  };

  typeList = (serial_numbers: boolean) => {
    const types = this.props.deviceCategoryList
      ? this.props.deviceCategoryList.filter((x) => x.category_name !== "")
      : [];
    const deviceCategoryList =
      types && types.length > 0
        ? types.map((c) => ({
            value: c.category_id,
            label: c.category_name,
            disabled: serial_numbers
              ? false
              : !this.state.categoryIds.includes(c.category_id),
          }))
        : [];

    return deviceCategoryList;
  };

  salesRepOptions = () => {
    let memberOptions = this.props.terretoryMembers
      ? this.props.terretoryMembers.filter((x) => x !== "")
      : [];

    memberOptions =
      memberOptions && memberOptions.length > 0
        ? memberOptions.map((c) => ({
            value: c.id,
            label: `${c.first_name} ${c.last_name}`,
            disabled: false,
          }))
        : [];

    return memberOptions;
  };

  handleConfigChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: string
  ) => {
    const newState = cloneDeep(this.state);
    const configuration = { ...this.state[key] };
    configuration[e.target.name] = e.target.value;
    newState.error[e.target.name].errorState = "success";
    newState.error[e.target.name].errorMessage = "";

    if (key === "configuration") {
      (newState as any).configuration = configuration;
    } else {
      (newState as any).opportunity = configuration;
    }

    this.setState({
      ...newState,
    });
  };

  handleBulkEditChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const bulkEdit = { ...this.state.bulkEdit };
    bulkEdit[e.target.name] = e.target.value;
    this.setState({
      bulkEdit,
    });
  };

  validateForm() {
    const newState = cloneDeep(this.state);
    const lineItem = this.state.lineItems[this.state.dataIndex];
    const lineItemConfig = this.state.configuration;
    const lineItemOpportunity = this.state.opportunity;

    let isValid = true;

    if (lineItem.serial_numbers.length > 0) {
      if (!lineItemConfig.category_id) {
        newState.error.category_id.errorState = "error";
        newState.error.category_id.errorMessage =
          "Please select a product type";
        isValid = false;
      }

      if (!lineItemConfig.manufacturer_id) {
        newState.error.manufacturer_id.errorState = "error";
        newState.error.manufacturer_id.errorMessage =
          "Please select a manufacturer";
        isValid = false;
      }

      if (!lineItemConfig.expiration_date) {
        newState.error.expiration_date.errorState = "error";
        newState.error.expiration_date.errorMessage =
          "Please select an expiration date";
        isValid = false;
      }
    }

    if (Object.keys(lineItemOpportunity).length > 0) {
      if (
        !lineItemOpportunity.opportunity_name ||
        lineItemOpportunity.opportunity_name.trim().length === 0
      ) {
        newState.error.opportunity_name.errorState = "error";
        newState.error.opportunity_name.errorMessage =
          "Please enter an opportunity name";
        isValid = false;
      }

      if (!lineItemOpportunity.opportunity_type) {
        newState.error.opportunity_type.errorState = "error";
        newState.error.opportunity_type.errorMessage =
          "Please select an opportunity type";
        isValid = false;
      }

      if (!lineItemOpportunity.opportunity_stage) {
        newState.error.opportunity_stage.errorState = "error";
        newState.error.opportunity_stage.errorMessage =
          "Please select an opportunity stage";
        isValid = false;
      }

      if (!lineItemOpportunity.inside_rep) {
        newState.error.inside_rep.errorState = "error";
        newState.error.inside_rep.errorMessage = "Please select an inside rep";
        isValid = false;
      }

      if (!lineItemOpportunity.user_id) {
        newState.error.user_id.errorState = "error";
        let errorMsg = "Please select an contact";
        if (this.state.customerUsers.length === 0) {
          errorMsg =
            "Please set atleast one active user to select contacts here.";
        }
        newState.error.user_id.errorMessage = errorMsg;
        isValid = false;
      }
    }

    this.setState(newState);

    return isValid;
  }

  createOrderConfig = (dataIndex: number) => {
    // handle create config

    if (!this.validateForm()) return;

    const lineItems = this.state.lineItems;
    lineItems[dataIndex].configuration = this.state.configuration;
    lineItems[dataIndex].opportunity = this.state.opportunity;
    lineItems[dataIndex].configAdded = true;

    this.setState({
      lineItems,
      showCreateOpportunity: false,
      opportunity: {},
      configuration: {},
      showConfigAdd: false,
    });
  };

  manufacturers = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((c) => ({
          value: c.id,
          label: c.label,
        }))
      : [];

    return manufacturers;
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
  };

  renderAddConfigPanel = () => {
    const data = this.state.lineItems[this.state.dataIndex];

    const stages = this.props.qStageList
      ? this.props.qStageList.map((manufacturer) => ({
          value: manufacturer.id,
          label: manufacturer.label,
        }))
      : [];

    const types = this.props.qTypeList
      ? this.props.qTypeList.map((site) => ({
          value: site.id,
          label: site.label,
        }))
      : [];

    return (
      <div className="config-parent">
        <div className="config-create-container">
          <div className="title">CREATE CONFIGURATION</div>
          <div className="product-id">
            <div className="detail-title">Product ID</div>
            <div className="product-data">{data.product.identifier}</div>
          </div>
          <div className="serial-number-container">
            <div className="detail-title">Serial Numbers</div>
            <div className="serial-numbers-list">
              {data.serial_numbers.length > 0
                ? data.serial_numbers.map((val) => (
                    <div className="serial-number-item">{val}</div>
                  ))
                : "-"}
            </div>
          </div>
          <div className="config-section">
            {
              <>
                <Input
                  field={{
                    label: "Configuration type",
                    type: "PICKLIST",
                    isRequired: true,
                    value:
                      this.state.configuration &&
                      this.state.configuration.category_id,
                    options: this.typeList(data.serial_numbers.length > 0),
                  }}
                  loading={this.props.isFetchingDeviceCategories}
                  disabled={false}
                  width={12}
                  name="category_id"
                  onChange={(e) => this.handleConfigChange(e, "configuration")}
                  className="configuration-type"
                  error={this.state.error.category_id}
                />
                <Input
                  field={{
                    label: "Select Manufacturer",
                    type: "PICKLIST",
                    isRequired: true,
                    value:
                      this.state.configuration &&
                      this.state.configuration.manufacturer_id,
                    options: this.manufacturers(),
                  }}
                  disabled={false}
                  width={12}
                  name="manufacturer_id"
                  onChange={(e) => this.handleConfigChange(e, "configuration")}
                  className="manufacturer"
                  error={this.state.error.manufacturer_id}
                />
                <Input
                  field={{
                    label: "Contract Number",
                    type: "TEXT",
                    isRequired: false,
                    value:
                      this.state.configuration &&
                      this.state.configuration.service_contract_number,
                    options: this.props.shipments,
                  }}
                  disabled={false}
                  width={12}
                  name="service_contract_number"
                  onChange={(e) => this.handleConfigChange(e, "configuration")}
                  className="contract-number"
                  error={this.state.error.service_contract_number}
                />
                <Input
                  field={{
                    label: "Start Date",
                    type: "DATE",
                    isRequired: true,
                    value:
                      this.state.configuration &&
                      this.state.configuration.installation_date,
                  }}
                  disabled={false}
                  showTime={false}
                  width={12}
                  name="installation_date"
                  onChange={(e) => this.handleConfigChange(e, "configuration")}
                  className="start-date"
                  error={this.state.error.installation_date}
                />
                <Input
                  field={{
                    label: "Expiration Date",
                    type: "DATE",
                    isRequired: true,
                    value:
                      this.state.configuration &&
                      this.state.configuration.expiration_date,
                  }}
                  disabled={false}
                  showTime={false}
                  width={12}
                  name="expiration_date"
                  onChange={(e) => this.handleConfigChange(e, "configuration")}
                  className="expiration-date"
                  error={this.state.error.expiration_date}
                />
              </>
            }
          </div>
          <div className="opportunity-details">
            <div className="title-op">Opportunity Details</div>
            {!this.state.showCreateOpportunity &&
            Object.keys(this.state.opportunity).length === 0 ? (
              <div
                onClick={() => this.setState({ showCreateOpportunity: true })}
                className="create-opp-action"
              >
                Create Opportunity
              </div>
            ) : (
              <>
                <Input
                  field={{
                    label: "Opportunity Name",
                    type: "TEXT",
                    isRequired: true,
                    value:
                      this.state.opportunity &&
                      this.state.opportunity.opportunity_name,
                  }}
                  disabled={false}
                  width={12}
                  name="opportunity_name"
                  onChange={(e) => this.handleConfigChange(e, "opportunity")}
                  className="opportunity-name"
                  error={this.state.error.opportunity_name}
                />
                <Input
                  field={{
                    label: "Opportunity type",
                    type: "PICKLIST",
                    isRequired: true,
                    value:
                      this.state.opportunity &&
                      this.state.opportunity.opportunity_type,
                    options: types,
                  }}
                  disabled={false}
                  width={12}
                  name="opportunity_type"
                  onChange={(e) => this.handleConfigChange(e, "opportunity")}
                  className="opportunity-type"
                  error={this.state.error.opportunity_type}
                />
                <Input
                  field={{
                    label: "Opportunity Stage",
                    type: "PICKLIST",
                    isRequired: true,
                    value:
                      this.state.opportunity &&
                      this.state.opportunity.opportunity_stage,
                    options: stages,
                  }}
                  disabled={false}
                  width={12}
                  name="opportunity_stage"
                  onChange={(e) => this.handleConfigChange(e, "opportunity")}
                  className="opportunity-stage"
                  error={this.state.error.opportunity_stage}
                />
                <Input
                  field={{
                    label: "Customer Contact",
                    type: "PICKLIST",
                    isRequired: false,
                    value:
                      this.state.opportunity && this.state.opportunity.user_id,
                    options: this.state.customerUsers,
                  }}
                  disabled={false}
                  width={12}
                  name="user_id"
                  onChange={(e) => this.handleConfigChange(e, "opportunity")}
                  className="inside-rep"
                  error={this.state.error.user_id}
                />
                <Input
                  field={{
                    label: "Inside Rep (Secondary Sales rep)",
                    type: "PICKLIST",
                    isRequired: true,
                    value:
                      this.state.opportunity &&
                      this.state.opportunity.inside_rep,
                    options: this.salesRepOptions(),
                  }}
                  loading={this.props.isFetchingMembers}
                  disabled={false}
                  width={12}
                  name="inside_rep"
                  onChange={(e) => this.handleConfigChange(e, "opportunity")}
                  className="inside-rep"
                  error={this.state.error.inside_rep}
                />
              </>
            )}
          </div>
        </div>
        <div className="bulk-actions-footer">
          <SquareButton
            content={`CANCEL`}
            bsStyle={"primary"}
            onClick={(e) =>
              this.setState({
                showCreateOpportunity: false,
                opportunity: {},
                configuration: {},
                showConfigAdd: false,
                error: this.getEmptyState().error,
              })
            }
            className="bulk-cancel-or"
          />
          <SquareButton
            content={`APPLY`}
            bsStyle={"primary"}
            onClick={(e) => this.createOrderConfig(this.state.dataIndex)}
            className="bulk-apply-or"
          />
        </div>
      </div>
    );
  };

  render() {
    let selectionCount = 0;
    this.state.lineItems.map((item) => {
      if (item.selected) selectionCount++;
    });

    return (
      <div className="order-tracking-v2-main cr-container">
        <div className="loader">
          <Spinner show={this.state.loading} />
        </div>
        {this.state.showBulkEdit && <Panel>{this.renderBulkEditPanel()}</Panel>}
        {this.state.showConfigAdd && (
          <Panel>{this.renderAddConfigPanel()}</Panel>
        )}
        {this.renderHeader()}
        {this.state.ticket_id !== 0 && (
          <div className="notes">
            <Input
              field={{
                label: "Ticketing Notes",
                type: "TEXTAREA",
                value: this.state.ticket_note,
                isRequired: false,
              }}
              width={6}
              name="description"
              onChange={(e) =>
                this.setState({
                  ticket_note: e.target.value,
                  ticketNoteUpated: true,
                })
              }
              placeholder={`Enter notes`}
              className="or-ticket-notes"
            />
            {this.state.ticket_id && (
              <>
                <Input
                  field={{
                    label: "Last note",
                    type: "TEXTAREA",
                    value:
                      this.state.ticket_last_note &&
                      this.state.ticket_last_note.text,
                    isRequired: false,
                  }}
                  width={6}
                  name="description"
                  onChange={(e) =>
                    this.setState({
                      ticket_note: e.target.value,
                      ticketNoteUpated: true,
                    })
                  }
                  placeholder={`Last note not found`}
                  className="or-ticket-notes"
                  disabled={true}
                  title={
                    this.state.ticket_last_note &&
                    `Last Updated on :${fromISOStringToFormattedDate(
                      this.state.ticket_last_note.last_updated_on
                    )}`
                  }
                />
                <div className="loader">
                  <Spinner
                    show={
                      this.state.loadingNote || this.state.loadingIngramData
                    }
                  />
                </div>
              </>
            )}
          </div>
        )}
        {(this.state.ticketNoteUpated ||
          this.state.lineItems.filter((item) => item.selected).length !==
            0) && (
          <div className="update-actions-btn">
            {this.state.lineItems.length > 0 && (
              <SquareButton
                content={`BULK EDIT`}
                bsStyle={"primary"}
                onClick={(e) => this.setState({ showBulkEdit: true })}
                className="bulk-or"
              />
            )}
            <SquareButton
              content={`SEND UPDATE`}
              bsStyle={"primary"}
              onClick={(e) => this.postLineItemsData()}
              className="update-or"
            />
          </div>
        )}
        {/* <ConfirmBox
          show={Boolean(
            this.state.isopenConfirm && this.state.purchase_order_id
          )}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.handleSubmit}
          isLoading={false}
          title="Do you want to send email?"
          okText="Yes"
          cancelText="No"
        /> */}
        <div className="product-data-section">
          {this.state.lineItems.length > 0 && (
            <>
              <div className="select-all-rows">
                <div className="section-title">PRODUCT DETAILS</div>
                <div className="checkbox-section">
                  <Checkbox
                    isChecked={this.state.selectAllRows}
                    name="selectAllRows"
                    onChange={this.setSelectAllRows}
                  >
                    Select All Rows
                  </Checkbox>
                  <Checkbox
                    isChecked={this.state.toExcludeServices}
                    name="toExcludeServices"
                    onChange={this.handleExcludeServicesChange}
                    className=""
                  >
                    Exclude Services
                  </Checkbox>
                  <Checkbox
                    isChecked={this.state.showReceivedOnly}
                    name="showReceivedOnly"
                    onChange={this.handleExcludeServicesChange}
                    className=""
                  >
                    Show Received Only
                  </Checkbox>
                  <Checkbox
                    isChecked={this.state.shouldFetchIngram}
                    name="shouldFetchIngram"
                    onChange={this.handleShouldFetchIngram}
                    className=""
                  >
                    Fetch Ingram Data
                  </Checkbox>
                </div>
                <Input
                  field={{
                    value: this.state.inputValue,
                    label: "",
                    type: "SEARCH",
                  }}
                  width={10}
                  name="searchString"
                  onChange={this.onSearchStringChange}
                  onBlur={() => {
                    if (this.state.inputValue.trim() === "") {
                      this.setState({ showSearch: false });
                    }
                  }}
                  placeholder="Search"
                  className="search-order-tracking search-order-tracking-outside"
                />
                {selectionCount > 0 && (
                  <div className="detail-title selection-count">{`${selectionCount} Selected`}</div>
                )}
                {this.state.shouldFetchIngram && (
                  <div className="legents-box">
                    <div className="legent-data ">
                      <div className="square-border border-yello" />
                      Data from Connectwise & Ingram
                    </div>
                    <div className="legent-data ">
                      <div className="square-border border-white" />
                      Data from Connectwise only
                    </div>
                  </div>
                )}
              </div>
              {this.renderPurchaseOrders()}
            </>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  types: state.inventory.types,
  qTypeList: state.sow.qTypeList,
  qStageList: state.sow.qStageList,
  shipments: state.setting.shipments,
  customerId: state.customer.customerId,
  customers: state.customer.customersShort,
  isFetching: state.inventory.isPostingBatch,
  manufacturers: state.inventory.manufacturers,
  purchaseOrdersAll: state.inventory.purchaseOrdersAll,
  terretoryMembers: state.integration.terretoryMembers,
  isFetchingShipments: state.setting.isFetchingShipments,
  deviceCategoryList: state.inventory.deviceCategoryList,
  isFetchingDeviceCategories: state.inventory.isFetching,
  isFetchingMembers: state.integration.isFetchingMembers,
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTypes: () => dispatch(fetchTypes()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  getQuoteStageList: () => dispatch(getQuoteStageList()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  getShipmentMethods: () => dispatch(getShipmentMethods()),
  getDataCommon: (url: string) => dispatch(getDataCommon(url)),
  fetchDeviceCategories: () => dispatch(fetchDeviceCategories()),
  fetchTerritoryMembers: () => dispatch(fetchTerritoryMembers()),
  getPurchaseOrderSettings: () => dispatch(getPurchaseOrderSettings()),
  getListPurchaseOrdersAll: () => dispatch(getListPurchaseOrdersAll()),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchAllProviderIntegrations: () => dispatch(fetchAllProviderIntegrations()),
  getListPOTickts: (customerId: number) =>
    dispatch(getListPOTickts(customerId)),
  getListPurchaseOrdersAllStatus: (showAll: boolean) =>
    dispatch(getListPurchaseOrdersAllStatus(showAll)),
  getLineItemsOperations: (purchase_order_ids: number[]) =>
    dispatch(getLineItemsOperations(purchase_order_ids)),
  fetchAllCustomerUsers: (customerId: number, params: any) =>
    dispatch(fetchAllCustomerUsers(customerId, params)),
  postLineItemsData: (
    customer_id: number,
    ids: number,
    ti: number,
    tn: string,
    li: any
  ) => dispatch(postLineItemsData(customer_id, ids, ti, tn, li)),
  getLineItemsIngramData: (
    purchase_orders: string[],
    line_item_identifiers: string[]
  ) => dispatch(getLineItemsIngramData(purchase_orders, line_item_identifiers)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OrderTrackingReceiving);
