import React from "react";
import { Line } from "react-chartjs-2";
import { connect } from "react-redux";
import { Chart, Legend, Tooltip, Title, LineController } from "chart.js";
import {
  exportRawData,
  getRawData,
  getServiceActivity,
  getServiceHistoryExpected,
  getServiceHostoryWon,
} from "../../../actions/sowDashboard";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import "./style.scss";

// TODO: RPU - Backend response has to be upgraded for service-history-created-won and service-history-expected-vs-won APIs
Chart.register(Legend, Title, Tooltip, LineController);

interface IDashboardProps extends ICommonProps {
  rawData: any;
  sowCreated: any;
  expectedWon: any;
  isFetchingActivity: boolean;
  isFetchingExpected: boolean;
  isFetchingSRawData: boolean;
  isFetchingSOWCreated: boolean;
  service_activity: ISOWDashboardField[];
  getRawData: () => Promise<any>;
  exportRawData: () => Promise<any>;
  getServiceActivity: () => Promise<any>;
  getServiceHostoryWon: () => Promise<any>;
  getServiceHistoryExpected: () => Promise<any>;
}

interface IDashboardState {
  documents: any[];
  previewHTML: IFileResponse;
  showDetails: boolean;
  openPreview: boolean;
  downloadingPDFIds: number[];
  service_activity: ISOWDashboardField[];
}

class SOWDashBoard extends React.Component<IDashboardProps, IDashboardState> {
  constructor(props: IDashboardProps) {
    super(props);
    this.state = {
      service_activity: [],
      showDetails: false,
      documents: [],
      downloadingPDFIds: [],
      openPreview: false,
      previewHTML: null,
    };
  }

  componentDidMount() {
    if (this.props.service_activity) {
      this.setState({
        service_activity: this.props.service_activity,
      });
    } else {
      this.props.getServiceActivity();
    }
    this.props.getServiceHostoryWon();
    this.props.getServiceHistoryExpected();
    this.props.getRawData();
  }

  componentDidUpdate(prevProps: IDashboardProps) {
    if (
      this.props.service_activity &&
      prevProps.service_activity !== this.props.service_activity
    ) {
      this.setState({
        service_activity: this.props.service_activity,
      });
    }
  }

  getBody = (docs) => {
    const docsAsso =
      docs &&
      docs.map((item) => {
        return `${item.circuit_id}`;
      });
    const columnsPopUp: any = [
      {
        accessor: "name",
        Header: "Name",
      },
      {
        accessor: "customer",
        Header: "Customer",
      },
      {
        accessor: "quote",
        Header: "Quote",
      },
      {
        accessor: "author_name",
        Header: "Author",
      },
      {
        accessor: "hours",
        Header: "Hours",
        Cell: (c) => <div>{c.value ? c.value.toFixed(2) : "-"}</div>,
      },
      {
        accessor: "created_on",
        Header: "Created On",
        id: "created_on",
        width: 110,
        sortable: true,
        Cell: (cell) => (
          <div>
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
    ];
    const message = (
      <div style={{ width: "100%" }}>
        <Table
          columns={columnsPopUp}
          customTopBar={null}
          rows={docs || []}
          loading={this.props.isFetchingSRawData}
        />
      </div>
    );

    return docsAsso && docsAsso.length > 0 && message ? message : "";
  };

  toggleShowDetails = () => {
    this.setState({
      showDetails: false,
      documents: [],
    });
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  openShowDetails = (activity) => {
    if (activity.documents && activity.documents.length > 0) {
      this.setState({
        showDetails: true,
        documents: activity.documents,
      });
    }
  };

  render() {
    const options2 = {
      maintainAspectRatio: false,
      responsive: true,
      plugins: {
        title: {
          display: true,
          text: `Expected vs Won Hours`,
          font: {
            size: 20,
            weight: "bold" as "bold",
          },
        },
        legend: {
          display: true,
          labels: {
            font: { weight: "bold" as "bold" },
          },
          position: "bottom" as "bottom",
        },
      },
    };
    const options = {
      plugins: {
        legend: {
          display: true,
          labels: {
            font: { weight: "bold" as "bold" },
          },
          position: "bottom" as "bottom",
        },
        title: {
          display: true,
          text: `SoW's Created / Won`,
          font: {
            size: 20,
            weight: "bold" as "bold",
          },
        },
      },
      maintainAspectRatio: false,
      responsive: true,
    };

    const columns: any = [
      {
        accessor: "date",
        Header: "Date",
      },
      {
        accessor: "sow_created",
        Header: "SoW’s Created",
      },
      {
        accessor: "sow_won",
        Header: "SoW’s Won",
      },
      {
        accessor: "expected_hours",
        Header: "Expected Hours",
      },
      {
        accessor: "won_hours",
        Header: "Won Hours",
      },
    ];

    return (
      <div className="sow-dashboard col-md-12 row ">
        <div className="header">
          <h3>Service Analytics</h3>
        </div>
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingActivity ||
              this.props.isFetchingSOWCreated ||
              this.props.isFetchingExpected
            }
          />
        </div>
        {this.state.service_activity &&
          Object.keys(this.state.service_activity).map((section, index) => (
            <div key={index} className="sow-dashboard-row col-md-12 ">
              <div className="heading"> {section} </div>
              <div className="body col-md-12 ">
                {this.state.service_activity &&
                  this.state.service_activity[section].map((activity, i) => (
                    <div key={i} className="block col-md-3 col-xs-6">
                      <div className="block-label">{activity.label}</div>
                      <div
                        className={` ${
                          activity.documents && activity.documents.length > 0
                            ? "pop-up-available"
                            : ""
                        } box`}
                        style={{
                          backgroundColor: activity.bg_color,
                          color: activity.text_color,
                          borderLeft: `5px solid ${activity.text_color}`,
                        }}
                        title={activity.hover_text}
                        onClick={(e) => this.openShowDetails(activity)}
                      >
                        {activity.value}
                        {activity.unit}
                        {activity.documents &&
                          activity.documents.length > 0 && (
                            <img
                              className="view-details-icon"
                              src={`/assets/new-icons/info.svg`}
                              title={"view details"}
                            />
                          )}
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          ))}
        <div className="sow-dashboard-row  col-md-12 ">
          <div className="heading">Service History</div>
          <div className="body graph-box col-md-6">
            {this.props.sowCreated && (
              <Line data={this.props.sowCreated} options={options} />
            )}
          </div>
          <div className="body graph-box col-md-6">
            {this.props.expectedWon && (
              <Line data={this.props.expectedWon} options={options2} />
            )}
          </div>
        </div>
        <div className="sow-dashboard-row  col-md-12 ">
          <div className="heading raw-data-heading">
            <span>Raw Data</span>{" "}
            <SquareButton
              content="Export"
              bsStyle={"primary"}
              onClick={(e) => this.props.exportRawData()}
            />
          </div>
          <div className={true ? "loader" : ""}>
            <Spinner show={this.props.isFetchingSRawData} />
          </div>
          <div className="body col-md-12">
            <Table
              columns={columns}
              customTopBar={null}
              rows={this.props.rawData || []}
              className={`${this.props.isFetchingSRawData ? "loading" : ""}`}
            />
          </div>
          <ModalBase
            show={this.state.showDetails}
            onClose={this.toggleShowDetails}
            titleElement={"Documents"}
            bodyElement={this.getBody(this.state.documents)}
            footerElement={
              <div className={`add-configuration__footer`}>
                <SquareButton
                  onClick={this.toggleShowDetails}
                  content="Close"
                  bsStyle={"primary"}
                />
              </div>
            }
            className="add-configuration configuration-view"
          />
        </div>
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetchingActivity: state.sowDashboard.isFetchingActivity,
  isFetchingSOWCreated: state.sowDashboard.isFetchingSOWCreated,
  isFetchingExpected: state.sowDashboard.isFetchingExpected,
  isFetchingSRawData: state.sowDashboard.isFetchingSRawData,
  service_activity: state.sowDashboard.service_activity,
  rawData: state.sowDashboard.rawData,
  sowCreated: state.sowDashboard.sowCreated,
  expectedWon: state.sowDashboard.expectedWon,
});

const mapDispatchToProps = (dispatch: any) => ({
  getServiceActivity: () => dispatch(getServiceActivity()),
  getServiceHistoryExpected: () => dispatch(getServiceHistoryExpected()),
  getServiceHostoryWon: () => dispatch(getServiceHostoryWon()),
  getRawData: () => dispatch(getRawData()),
  exportRawData: () => dispatch(exportRawData()),
});

export default connect(mapStateToProps, mapDispatchToProps)(SOWDashBoard);
