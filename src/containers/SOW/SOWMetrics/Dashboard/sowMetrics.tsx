import React from "react";
import moment from "moment";
import * as XLSX from "xlsx";
import saveAs from "file-saver";
import { connect } from "react-redux";
// import htmlDocx from "html-docx-js/dist/html-docx"; (New Component)
import { DebouncedFunc, cloneDeep, debounce } from "lodash";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  getSOWAuthors,
  getMarginData,
  downloadSOWDocumnet,
  getSOWByTechnologies,
  getTabledataSOWMetrics,
  getFullSOWMetricsTable,
  sendEmailToAccountManager,
  getSOWDocumentsLastUpdated,
  EDIT_SOW_SUCCESS,
  DOWNLOAD_SOW_SUCCESS,
  GET_FULL_TABLE_SUCCESS,
  FETCH_SOW_MARGIN_SUCCESS,
  FETCH_SOW_BY_TECH_SUCCESS,
  FETCH_SOW_AUTHORS_SUCCESS,
  FETCH_SOW_DOC_LAST_UPDATED_SUCCESS,
} from "../../../../actions/sow";
import Input from "../../../../components/Input/input";
import Table from "../../../../components/Table/table";
import SquareButton from "../../../../components/Button/button";
import EditButton from "../../../../components/Button/editButton";
import IconButton from "../../../../components/Button/iconButton";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import RenameBox from "../../../../components/RenameBox/RenameBox";
import DeleteButton from "../../../../components/Button/deleteButton";
import { rawDoughnutChart } from "./DoughnutChart";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import "../../../../commonStyles/doughnut_chart.scss";
import "../../../../commonStyles/filtersListing.scss";
import Filters from "./filters";
import "./style.scss";

interface IDashboardProps extends ICommonProps {
  isFetching: boolean;
  SOWMetricsTable: any;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  sendEmailToAccountManager: (id: number) => Promise<any>;
  getSOWAuthors: (params: ISoWMetricsFilterParams) => Promise<any>;
  getMarginData: (params: ISoWMetricsFilterParams) => Promise<any>;
  getSOWByTechnologies: (params: ISoWMetricsFilterParams) => Promise<any>;
  getSOWDocumentsLastUpdated: (params: ISoWMetricsFilterParams) => Promise<any>;
  getTableData: (
    params?: IServerPaginationParams & ISoWMetricsFilterParams
  ) => Promise<any>;
  getFullTableData: (
    params?: IServerPaginationParams & ISoWMetricsFilterParams
  ) => Promise<any>;
  downloadSOWDocumnet: (
    id: number,
    type: string,
    name?: string
  ) => Promise<any>;
}

interface IDashboardState {
  id: number;
  document: any;
  reset: boolean;
  closed: boolean;
  docType: string;
  loading: boolean;
  previewHTML: IFileResponse;
  rows: ISowTableRow[];
  searchString: string;
  openPreview: boolean;
  documentName: string;
  showOlderSOW: boolean;
  isopenRename: boolean;
  showMarginOpen: string;
  emailSending: number[];
  previewPDFIds: number[];
  downloadingIds: number[];
  loadingExcelData: boolean;
  loadingSOWByTech: boolean;
  showFiltersModal: boolean;
  loadingMarginData: boolean;
  loadingSOWAuthors: boolean;
  downloadingPDFIds: number[];
  tableFilters: ISowTableFilters;
  loadingSOWDocLastUpdated: boolean;
  sowByTech: IDoughnutChartObject[];
  sowAuthors: IDoughnutChartObject[];
  marginData: IDoughnutChartObject[];
  sowDocumentsLastUpdated: IDoughnutChartObject[];
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams & ISoWMetricsFilterParams;
  };
}

interface ISowTableFilters {
  "doc-status": string;
  created_after: string;
  created_before: string;
  updated_after: string;
  updated_before: string;
  created_status: string;
  updated_status: string;
}

class SOWMetrics extends React.Component<IDashboardProps, IDashboardState> {
  private debouncedFetch: DebouncedFunc<
    (params: IServerPaginationParams) => void
  >;

  constructor(props: IDashboardProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  priorityColorMap = [
    "#4ba2c1",
    "#e55b7a",
    "#fac64d",
    "#5b9950",
    "#ba89f2",
    "#cdd5e1",
  ];

  estShipppedDatePriorityColorMap = [
    "#5b9950",
    "#fac64d",
    "#4ba2c1",
    "#e55b7a",
    "#ba89f2",
    "#cdd5e1",
  ];

  getEmptyState = () => ({
    id: null,
    rows: [],
    docType: "",
    reset: false,
    closed: false,
    sowAuthors: [],
    marginData: [],
    loading: false,
    document: null,
    searchString: "",
    emailSending: [],
    documentName: "",
    previewPDFIds: [],
    previewHTML: null,
    downloadingIds: [],
    openPreview: false,
    showOlderSOW: false,
    isopenRename: false,
    downloadingPDFIds: [],
    showMarginOpen: "Open",
    loadingExcelData: false,
    showFiltersModal: false,
    loadingSOWByTech: false,
    loadingMarginData: false,
    loadingSOWAuthors: false,
    sowDocumentsLastUpdated: [],
    loadingSOWDocLastUpdated: false,
    sowByTech: [{ label: "", value: 0, filter: false }],
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: { page_size: 25 },
    },
    tableFilters: {
      "doc-status": "",
      created_after: "",
      created_before: "",
      updated_after: "",
      updated_before: "",
      created_status: "",
      updated_status: "",
    },
  });

  componentDidMount() {
    this.loadGraphsData();
  }

  loadGraphsData = () => {
    this.getSOWDocumentsLastUpdated();
    this.getSOWAuthors();
    this.getSOWByTechnologies();
    this.getMarginGraphData();
  };

  componentDidUpdate(prevProps: IDashboardProps) {
    if (
      this.props.SOWMetricsTable &&
      prevProps.SOWMetricsTable !== this.props.SOWMetricsTable
    ) {
      this.setRows(this.props);
    }
  }

  getSOWDocumentsLastUpdated = () => {
    this.setState({ loadingSOWDocLastUpdated: true });
    const params = this.filterForAll();
    this.props.getSOWDocumentsLastUpdated(params).then((action) => {
      if (action.type === FETCH_SOW_DOC_LAST_UPDATED_SUCCESS) {
        const sowDocumentsLastUpdated = action.response.map((data) => ({
          value: data.count,
          label: `${data.days_range}`,
          filter: this.getFilterValueByName(
            this.state.sowDocumentsLastUpdated,
            data.days_range
          ),
        }));
        this.setState({ sowDocumentsLastUpdated });
      }
      this.setState({ loadingSOWDocLastUpdated: false });
    });
  };

  getSOWAuthors = () => {
    this.setState({ loadingSOWAuthors: true });
    const params = this.filterForAll();
    this.props.getSOWAuthors(params).then((action) => {
      if (action.type === FETCH_SOW_AUTHORS_SUCCESS) {
        const sowAuthors = action.response.map((data) => ({
          value: data.count,
          label: `${data.author__name}`,
          filter: this.getFilterValueByName(
            this.state.sowAuthors,
            data.author__name
          ),
          id: data.author_id,
        }));
        this.setState({ sowAuthors });
      }
      this.setState({ loadingSOWAuthors: false });
    });
  };

  getSOWByTechnologies = () => {
    const params = this.filterForAll();
    this.setState({ loadingSOWByTech: true });
    this.props.getSOWByTechnologies(params).then((action) => {
      if (action.type === FETCH_SOW_BY_TECH_SUCCESS) {
        const sowByTech = action.response.map((data) => ({
          value: data.margin,
          count: data.count,
          label: data.category,
          filter: this.getFilterValueByName(
            this.state.sowByTech,
            data.category
          ),
        }));
        this.setState({ sowByTech });
      }
      this.setState({ loadingSOWByTech: false });
    });
  };

  getMarginGraphData = () => {
    const params = this.filterForAll();
    this.setState({ loadingMarginData: true });
    this.props.getMarginData(params).then((action) => {
      if (action.type === FETCH_SOW_MARGIN_SUCCESS) {
        const res = action.response;
        const marginData = [];
        Object.keys(res).map((name) => {
          const t = res[name];
          Object.keys(t).map((y) => {
            if (this.state.showMarginOpen === name) {
              marginData.push({
                label: y,
                value: t[y],
                filter: this.getFilterValueByName(this.state.marginData, y),
                type: name,
              });
            }
          });
        });
        this.setState({ marginData });
      }
      this.setState({ loadingMarginData: false });
    });
  };

  getFilterValueByName = (list: IDoughnutChartObject[], value: string) => {
    const filterValue = list.find((x) => x.label === value);
    return filterValue ? filterValue.filter : false;
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.

    let filterParams = this.createFilterParams();
    const list = this.state.sowByTech.filter((x) => x.filter);
    const sowByTech = list && list[0] && { technology_type: list[0].label };

    const list1 = this.state.sowAuthors.filter((x) => x.filter);
    const sowAuthors = list1 && list1[0] && { author_id: list1[0].id };

    let newParams = {
      ...prevParams,
      ...params,
      ...filterParams,
      ...sowByTech,
      ...sowAuthors,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getTableData({
      ...newParams,
      ...this.createTableFilterParams(),
    });
  };

  filterForAll = (): ISoWMetricsFilterParams => {
    let filterParams = this.createFilterParams();
    const list = this.state.sowByTech.filter((x) => x.filter);
    const sowByTech = list && list[0] && { technology_type: list[0].label };
    const list1 = this.state.sowAuthors.filter((x) => x.filter);
    const sowAuthors = list1 && list1[0] && { author_id: list1[0].id };

    return {
      ...filterParams,
      ...sowByTech,
      ...sowAuthors,
    };
  };

  getFilterParams = (list: IDoughnutChartObject[]): ISoWMetricsFilterParams => {
    const data = list.filter((x) => x.filter);
    const derivedData = data[0] && this.calculateDate(data[0].label);
    return derivedData;
  };

  createFilterParams = (): ISoWMetricsFilterParams => {
    const sowDocumentsLastUpdated = this.getFilterParams(
      this.state.sowDocumentsLastUpdated
    );
    const marginData = this.getFilterParams(this.state.marginData);
    const docStatus = {};
    if (marginData) {
      marginData["doc-status"] = this.state.showMarginOpen;
    } else {
      docStatus["doc-status"] = this.state.showMarginOpen;
    }
    return {
      ...sowDocumentsLastUpdated,
      ...marginData,
      ...docStatus,
    };
  };

  createTableFilterParams = (): ISoWMetricsFilterParams => {
    const tableFilters = this.state.tableFilters;
    let filters: ISoWMetricsFilterParams = {};
    Object.keys(tableFilters).forEach((key) => {
      if (
        tableFilters[key] &&
        key !== "updated_status" &&
        key !== "created_status"
      )
        filters[key] = tableFilters[key];
    });
    return filters;
  };

  getDateBySubtract = (
    days: moment.DurationInputArg1,
    duration: moment.unitOfTime.DurationConstructor
  ) => {
    const date = moment().subtract(days, duration).format("YYYY-MM-DD");
    return date;
  };

  calculateDate = (key: string) => {
    let to: string;
    let from: string;
    switch (key) {
      case "SOW Document updated within last 7 days":
        from = this.getDateBySubtract(7, "days");
        return {
          updated_after: from,
          updated_before: "",
        };

      case "SOW Document not updated over 7 days":
        from = this.getDateBySubtract(15, "days");
        to = this.getDateBySubtract(8, "days");
        return {
          updated_after: from,
          updated_before: to,
        };

      case "SOW Document not updated over 15 days":
        from = this.getDateBySubtract(45, "days");
        to = this.getDateBySubtract(16, "days");
        return {
          updated_after: from,
          updated_before: to,
        };

      case "SOW Document not updated over 45 days":
        to = this.getDateBySubtract(46, "days");
        return {
          updated_after: "",
          updated_before: to,
        };

      case "Last Quarter":
        from = this.getDateBySubtract(6, "months");
        to = moment()
          .subtract(3, "months")
          .subtract(1, "days")
          .format("YYYY-MM-DD");
        return {
          updated_after: from,
          updated_before: to,
        };

      case "This Month":
        from = this.getDateBySubtract(1, "months");
        return {
          updated_after: from,
          updated_before: "",
        };

      case "This Quarter":
        from = this.getDateBySubtract(3, "months");
        return {
          updated_after: from,
          updated_before: "",
        };

      default:
        return { to, from };
    }
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
      page_size: this.state.pagination.params.page_size,
    });
  };

  setRows = (nextProps: IDashboardProps) => {
    const sowData = nextProps.SOWMetricsTable;
    const data: ISowTableRow[] = sowData && sowData.results;
    const rows: ISowTableRow[] = data;

    this.setState((prevState) => ({
      reset: false,
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: sowData.count,
        totalPages: Math.ceil(
          sowData.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  appliedFilterLength = () => {
    return (
      this.state.sowByTech.filter((x) => x.filter).length +
      this.state.sowAuthors.filter((x) => x.filter).length +
      this.state.marginData.filter((x) => x.filter).length +
      this.state.sowDocumentsLastUpdated.filter((x) => x.filter).length
    );
  };

  onFilterRemove = (removedFilters: Partial<ISowTableFilters>) => {
    this.setState(
      (prevState) => ({
        tableFilters: {
          ...prevState.tableFilters,
          ...removedFilters,
        },
      }),
      () => this.debouncedFetch({ page: 1 })
    );
  };

  renderTableFilters = () => {
    const filters = this.state.tableFilters;
    const shouldRenderFilters =
      filters["doc-status"] ||
      (filters.updated_before && filters.updated_after) ||
      (filters.created_after && filters.created_before);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing metrics-table-filters">
        <label id="applied-filters-heading">Applied Table Filters: </label>
        {filters["doc-status"] && (
          <div className="section-show-filters metrics-applied-filters-row">
            <label>Status: </label>
            <div className="metrics-filter-content">
              {filters["doc-status"] + "  "}
              <span
                className="metrics-filter-clear-icon"
                onClick={() => this.onFilterRemove({ "doc-status": undefined })}
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
        {filters.updated_after && (
          <div className="section-show-filters metrics-applied-filters-row">
            <label>Updated between:</label>
            <div className="metrics-filter-content">
              {moment(filters.updated_after).format("MM/DD/YYYY")}
              {" - "}
              {moment(filters.updated_before).format("MM/DD/YYYY")}
              <span
                className="date-clear metrics-filter-clear-icon"
                onClick={() =>
                  this.onFilterRemove({
                    updated_status: "",
                    updated_before: undefined,
                    updated_after: undefined,
                  })
                }
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
        {filters.created_after && (
          <div className="section-show-filters metrics-applied-filters-row">
            <label>Created between:</label>
            <div className="metrics-filter-content">
              {moment(filters.created_after).format("MM/DD/YYYY")}
              {" - "}
              {moment(filters.created_before).format("MM/DD/YYYY")}
              <span
                className="date-clear metrics-filter-clear-icon"
                onClick={() =>
                  this.onFilterRemove({
                    created_status: "",
                    created_before: undefined,
                    created_after: undefined,
                  })
                }
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
      </div>
    ) : null;
  };

  renderTopBar = () => {
    return (
      <div className={"dashboard-sow-metrics__table-top"}>
        <div className="metrics-upper-section">
          <Input
            field={{
              label: "",
              type: "SEARCH",
              value: this.state.pagination.params.search,
              isRequired: false,
            }}
            width={12}
            placeholder="Search"
            name="searchString"
            onChange={this.onSearchStringChange}
            className="dashboard-sow-metrics__search"
          />
          <div className="total-count">
            <span>Total : </span>
            {this.props.SOWMetricsTable.count || 0}
            <span> | </span>
          </div>

          <div className="filters">
            <span>
              <img
                alt=""
                className="filter-img"
                src="/assets/icons/filter.png"
              />
              Applied Filters:
            </span>
            <div className="tiles">
              {this.filtersTiles(this.state.sowByTech, "sowByTech")}
              {this.filtersTiles(this.state.sowAuthors, "sowAuthors")}
              {this.filtersTiles(this.state.marginData, "marginData")}
              {this.filtersTiles(
                this.state.sowDocumentsLastUpdated,
                "sowDocumentsLastUpdated"
              )}
              {this.appliedFilterLength() === 0 && (
                <div className="no-filter-applied">No Filter Applied</div>
              )}
              {this.appliedFilterLength() > 1 && (
                <div
                  className="filter-tile clear-filter-applied"
                  onClick={(e) => {
                    const newstate = cloneDeep(this.state);
                    newstate.sowByTech.map((x) => (x.filter = false));
                    newstate.marginData.map((x) => (x.filter = false));
                    newstate.sowAuthors.map((x) => (x.filter = false));
                    // newstate.orderEstimatedShipDate.map(
                    //   (x) => (x.filter = false)
                    // );
                    newstate.sowDocumentsLastUpdated.map(
                      (x) => (x.filter = false)
                    );
                    const pagination = this.getEmptyState().pagination;
                    (newstate.pagination as any) = pagination;
                    this.setState(newstate, () => {
                      this.debouncedFetch({
                        page: 1,
                        page_size: this.state.pagination.params.page_size,
                      });
                      this.loadGraphsData();
                    });
                  }}
                >
                  Clear all
                </div>
              )}
            </div>
          </div>
          <div className="right-section">
            <SquareButton
              onClick={() => this.toggleFilterModal(true)}
              className={"sow-export-btn"}
              content={
                <span>
                  <img alt="Filter Funnel" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={this.props.isFetching}
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={this.handleExportClick}
              className={"sow-export-btn"}
              content={
                <span>
                  <img
                    src={
                      this.state.loadingExcelData
                        ? "/assets/icons/loading.gif"
                        : "/assets/icons/export.png"
                    }
                    alt="Downloading File"
                  />
                  Export
                </span>
              }
              bsStyle={"primary"}
              disabled={this.props.isFetching || this.state.loadingExcelData}
            />
          </div>
        </div>
        {this.renderTableFilters()}
      </div>
    );
  };

  filtersTiles = (list: IDoughnutChartObject[], name: string) => {
    return (
      <>
        {list.map((x, ind) => {
          if (!x.filter) {
            return false;
          }
          return (
            <div className="filter-tile" title={String(x.count || x.value)}>
              {x.label}
              <img
                className={"d-pointer icon-remove"}
                alt=""
                src={"/assets/icons/cross-sign.svg"}
                onClick={(e) => {
                  const newstate = cloneDeep(this.state);
                  const data = newstate[name];
                  data[ind].filter = false;
                  (newstate[name] as IDoughnutChartObject[]) = data;
                  const pagination = this.getEmptyState().pagination;
                  pagination.params.page_size =
                    this.state.pagination.params.page_size;
                  (newstate.pagination as any) = pagination;
                  this.setState(newstate, () => {
                    this.debouncedFetch({
                      page: 1,
                      page_size: this.state.pagination.params.page_size,
                    });
                    this.loadGraphsData();
                  });
                }}
              />
            </div>
          );
        })}
      </>
    );
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  toggleOpenRename = () => {
    this.setState({
      isopenRename: !this.state.isopenRename,
    });
  };

  onRenameAndDownload = (name: string) => {
    if (this.state.docType === "pdf") {
      this.downloadPDFReport(name, this.state.id);
      this.setState({ docType: "" });
    } else {
      saveAs(this.state.document, `${name}.docx`);
      this.setState({ docType: "" });
    }
  };

  onEditRowClick = (row: ISowTableRow, event: any) => {
    event.stopPropagation();
    if (row.doc_type !== "Change Request")
      this.props.history.push(`/sow/sow/${row.id}`);
    else if (row.change_request && row.change_request[0]) {
      this.props.history.push(
        `/Projects/${row.change_request[0].project}/change-requests/${row.change_request[0].id}`
      );
    } else {
      this.props.addErrorMessage("No CR object found for the associated SoW!");
    }
  };

  onCloneClick = (id: number, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/sow/${id}?cloned=true`);
  };

  downloadPDFReport = (name: string, docId: number) => {
    this.setState({
      downloadingPDFIds: [...this.state.downloadingPDFIds, docId],
    });
    this.props.downloadSOWDocumnet(docId, "pdf", name).then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          isopenRename: false,
          id: null,
          docType: "",
          downloadingPDFIds: this.state.downloadingPDFIds.filter(
            (id) => docId !== id
          ),
        });
        const url = action.response.file_path;
        const link = document.createElement("a");
        link.href = url;
        link.target = "_blank";
        link.setAttribute("download", action.response.file_name);
        document.body.appendChild(link);
        link.click();
      }
    });
  };

  toggleFilterModal = (show: boolean) => {
    this.setState({
      showFiltersModal: show,
    });
  };

  applyTableFilters = (filters: ISowTableFilters) => {
    this.toggleFilterModal(false);
    this.setState({ tableFilters: filters }, () =>
      this.fetchData({ page: 1, page_size: 25 })
    );
  };

  getStream = (doc: any) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, doc.id],
    });
    this.props.downloadSOWDocumnet(doc.id, "doc").then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        // const sourceHTML = action.response;
        // const converted = htmlDocx.asBlob(sourceHTML, {
        //   orientation: "portrait",
        //   margins: {
        //     top: 300,
        //     right: 1300,
        //     left: 1300,
        //     header: 400,
        //     footer: 200,
        //     bottom: 0,
        //   },
        // });
        // let updatedDate: string = moment(doc.updated_on).format("YYYY-MM-DD");
        // let name = `${doc.customer.name} - ${
        //   doc.name
        // } - Sow v${doc.version.replace(".", "-")} - ${updatedDate}.doc`;
        // this.setState({
        //   documentName: name,
        //   document: converted,
        //   isopenRename: true,
        //   docType: "doc",
        // });
        // this.setState({
        //   downloadingIds: this.state.downloadingIds.filter(
        //     (id) => doc.id !== id
        //   ),
        // });
      }
    });
  };

  onPDFPreviewClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, original.id],
    });
    this.props.downloadSOWDocumnet(original.id, "pdf").then((a) => {
      if (a.type === DOWNLOAD_SOW_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          id: null,
          docType: "",
          previewPDFIds: this.state.previewPDFIds.filter(
            (id) => original.id !== id
          ),
        });
      }
    });
  }

  onEmailSending(id: number, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      emailSending: [...this.state.emailSending, id],
    });
    this.props.sendEmailToAccountManager(id).then((action) => {
      if (action.type === EDIT_SOW_SUCCESS) {
        this.setState({
          emailSending: this.state.emailSending.filter((i) => i !== id),
        });
        this.props.addSuccessMessage("Sending email to account manager...");
      }
    });
  }

  handleExportClick = () => {
    const getCurrency = (s: number) =>
      s
        ? new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD",
            minimumFractionDigits: 0,
          }).format(Number(s.toFixed()))
        : "$0";

    const getPercent = (s: number) => (s ? String(s) + "%" : "0%");

    this.setState({ loadingExcelData: true });
    this.props
      .getFullTableData({
        ...this.state.pagination.params,
        ...this.createTableFilterParams(),
        page: 1,
        page_size: this.state.pagination.totalRows,
      })
      .then((action) => {
        if (action.type === GET_FULL_TABLE_SUCCESS) {
          const raw_data = action.response.results;
          const rows = raw_data.map((row: ISowTableRow) => ({
            customer: row.customer.name,
            name: row.name,
            author: row.author_name,
            doc_type: row.doc_type,
            technology: row.category_name,
            opportunity: row.quote ? row.quote.name : "",
            won_date:
              fromISOStringToFormattedDate(row.opp_won_date, "MMM DD, YYYY") ||
              "-",
            status: row.quote ? row.quote.status_name : "",
            revenue: getCurrency(row.revenue),
            cost: getCurrency(row.cost),
            margin: getCurrency(row.margin),
            margin_percent: getPercent(row.margin_percent),
            total_hours:
              Number(row.professional_service_hours) +
              Number(row.professional_service_risk_hours),
            total_resource_hours: Number(row.professional_service_hours),
            total_risk_hours: Number(row.professional_service_risk_hours),
            engineering_hours: Number(row.engineering_hours),
            engineering_risk_hours: Number(row.engineering_risk_hours),
            engineering_revenue: getCurrency(row.engineering_revenue),
            engineering_cost: getCurrency(row.engineering_cost),
            engineering_margin: getCurrency(row.engineering_margin),
            engineering_margin_percent: getPercent(
              row.engineering_margin_percent
            ),
            after_hours: Number(row.after_hours),
            after_hours_risk_hours: Number(row.after_hours_risk_hours),
            after_hours_revenue: getCurrency(row.after_hours_revenue),
            after_hours_cost: getCurrency(row.after_hours_cost),
            after_hours_margin: getCurrency(row.after_hours_margin),
            after_hours_margin_percent: getPercent(
              row.after_hours_margin_percent
            ),
            integration_technician_hours: Number(
              row.integration_technician_hours
            ),
            integration_technician_risk_hours: Number(
              row.integration_technician_risk_hours
            ),
            integration_technician_revenue: getCurrency(
              row.integration_technician_revenue
            ),
            integration_technician_cost: getCurrency(
              row.integration_technician_cost
            ),
            integration_technician_margin: getCurrency(
              row.integration_technician_margin
            ),
            integration_technician_margin_percent: getPercent(
              row.integration_technician_margin_percent
            ),
            project_management_hours: Number(row.project_management_hours),
            project_management_risk_hours: Number(
              row.project_management_risk_hours
            ),
            project_management_revenue: getCurrency(
              row.project_management_revenue
            ),
            project_management_cost: getCurrency(row.project_management_cost),
            project_management_margin: getCurrency(
              row.project_management_margin
            ),
            project_management_margin_percent: getPercent(
              row.project_management_margin_percent
            ),
            total_hourly_resource_hours: Number(
              row.total_hourly_resource_hours
            ),
            total_hourly_resource_risk_hours: Number(
              row.total_hourly_resource_risk_hours
            ),
            hourly_resources_revenue: getCurrency(row.hourly_resources_revenue),
            hourly_resources_cost: getCurrency(row.hourly_resources_cost),
            hourly_resources_margin: getCurrency(row.hourly_resources_margin),
            hourly_resources_margin_percent: getPercent(
              row.hourly_resources_margin_percent
            ),
            contractors_revenue: getCurrency(row.contractors_revenue),
            contractors_cost: getCurrency(row.contractors_cost),
            contractors_margin: getCurrency(row.contractors_margin),
            contractors_margin_percent: getPercent(
              row.contractors_margin_percent
            ),
            risk_budget_revenue: getCurrency(row.risk_budget_revenue),
            risk_budget_cost: getCurrency(row.risk_budget_cost),
            risk_budget_margin: getCurrency(row.risk_budget_margin),
            risk_budget_margin_percent: getPercent(
              row.risk_budget_margin_percent
            ),
            travel_cost: getCurrency(row.travel_cost),
            created_on:
              fromISOStringToFormattedDate(row.created_on, "MMM DD, YYYY") ||
              "-",
            updated_on:
              fromISOStringToFormattedDate(row.updated_on, "MMM DD, YYYY") ||
              "-",
          }));
          const worksheet = XLSX.utils.json_to_sheet(rows);
          const workbook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(workbook, worksheet, "SOW Metrics");

          /* fix headers */
          XLSX.utils.sheet_add_aoa(
            worksheet,
            [
              [
                "Customer",
                "Document Name",
                "Author",
                "Type",
                "Technology",
                "Opportunity",
                "Opportunity Won Date",
                "Status",
                "Total Customer Cost",
                "Total Internal Cost",
                "Total Margin",
                "Total Margin %",
                "Total Hours",
                "Total Resource Hours",
                "Total Risk Hours",
                "Engineering Hours",
                "Engineering Risk Hours",
                "Engineering Revenue",
                "Engineering Cost",
                "Engineering Margin",
                "Engineering Margin %",
                "After Hours",
                "After Hours Risk Hours",
                "After Hours Revenue",
                "After Hours Cost",
                "After Hours Margin",
                "After Hours Margin %",
                "Integration Technician Hours",
                "Integration Technician Risk Hours",
                "Integration Technician Revenue",
                "Integration Technician Cost",
                "Integration Technician Margin",
                "Integration Technician Margin %",
                "Project Management Hours",
                "Project Management Risk Hours",
                "Project Management Revenue",
                "Project Management Cost",
                "Project Management Margin",
                "Project Management Margin %",
                "Hourly Resources Hours",
                "Hourly Resources Risk Hours",
                "Hourly Resources Revenue",
                "Hourly Resources Cost",
                "Hourly Resources Margin",
                "Hourly Resources Margin %",
                "Contractors Revenue",
                "Contractors Cost",
                "Contractors Margin",
                "Contractors Margin %",
                "Risk Budget Revenue",
                "Risk Budget Cost",
                "Risk Budget Margin",
                "Risk Budget Margin %",
                "Travel Cost",
                "Create Date",
                "Last Modified Date",
              ],
            ],
            { origin: "A1" }
          );

          const max_width_arr = rows.reduce(
            (w: number[], r) => [
              Math.max(w[0], r.customer.length),
              Math.max(w[1], r.name.length),
              Math.max(w[2], r.author.length),
              Math.max(w[3], r.author.length),
              Math.max(w[4], r.technology.length),
              Math.max(w[5], r.opportunity.length),
              ...Array.from({ length: 50 }, () => 10),
            ],
            Array.from({ length: 56 }, () => 15)
          );

          worksheet["!cols"] = max_width_arr.map((el) => ({
            wch: el,
          }));
          XLSX.writeFile(
            workbook,
            `sow_metrics_${new Date().toISOString()}.xlsx`
          );
          this.setState({ loadingExcelData: false });
        } else this.setState({ loadingExcelData: false });
      })
      .catch(() => this.setState({ loadingExcelData: false }));
  };

  onPDFClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    let updatedDate: string = moment(original.updated_on).format("YYYY-MM-DD");
    let name = `${original.customer.name} - ${
      original.name
    } - Sow v${original.version.replace(".", "-")} - ${updatedDate}.pdf`;
    this.setState({
      isopenRename: !this.state.isopenRename,
      documentName: name,
      id: original.id,
      docType: "pdf",
    });
  }

  listingDashboard = () => {
    const getDOCXDownloadMarkUp = (cell) => {
      if (this.state.downloadingIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="docx_download"
            title="DOC Download"
            onClick={(e) => this.getStream(cell.original)}
          />
        );
      }
    };

    const getPDFPrevieMarkUp = (cell) => {
      if (this.state.previewPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_preview"
            title="Preview"
            onClick={(e) => this.onPDFPreviewClick(cell.original, e)}
          />
        );
      }
    };

    const getEmailsendingMarkUp = (cell) => {
      if (this.state.emailSending.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <IconButton
            icon="mail.svg"
            onClick={(e) => {
              this.onEmailSending(cell.original.id, e);
            }}
            className="mail-btn"
            title={"Send email to account manager"}
          />
        );
      }
    };

    const getPDFDownloadMarkUp = (cell) => {
      if (this.state.downloadingPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_download"
            title="PDF Download"
            onClick={(e) => this.onPDFClick(cell.original, e)}
          />
        );
      }
    };

    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 0,
    });

    const columns: ITableColumn[] = [
      {
        accessor: "po_number",
        Header: "Customer",
        id: "customer.name",
        sortable: false,
        Cell: (c) => (
          <div className="po-underline" title={c.original.customer.name}>
            {c.original.customer.name}
          </div>
        ),
      },
      {
        accessor: "name",
        Header: "Name",
        id: "name",
        sortable: true,
        Cell: (c) => <div>{c.original.name}</div>,
      },
      {
        accessor: "author_name",
        Header: "Author",
        id: "author_name",
        sortable: false,
        Cell: (c) => <div>{c.original.author_name}</div>,
      },
      {
        accessor: "category_name",
        Header: "Technology",
        id: "category_name",
        sortable: false,
        Cell: (c) => <div>{c.original.category_name}</div>,
      },
      {
        accessor: "po",
        Header: "Opportunity",
        id: "quote__name",
        sortable: false,
        Cell: (c) => <div>{c.original.quote && c.original.quote.name}</div>,
      },
      {
        accessor: "po",
        Header: "Updated On",
        id: "updated_on",
        width: 140,
        sortable: true,
        Cell: (c) => (
          <div>
            {fromISOStringToFormattedDate(
              c.original.updated_on,
              "MM/DD/YYYY h:mm A"
            )}
          </div>
        ),
      },
      {
        accessor: "po",
        Header: "Budget Hours",
        id: "budget_hours",
        width: 90,
        sortable: true,
        Cell: (c) => <div>{c.original.budget_hours}</div>,
      },
      {
        accessor: "po",
        Header: "Status",
        id: "eta",
        width: 130,
        sortable: false,
        Cell: (c) => (
          <div>{c.original.quote && c.original.quote.status_name}</div>
        ),
      },
      {
        accessor: "po",
        Header: "Margin",
        id: "margin",
        sortable: true,
        Cell: (c) => (
          <div title={c.original.margin}>
            {c.original.margin
              ? formatter.format(c.original.margin.toFixed())
              : ""}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        width: 135,
        sortable: false,
        Cell: (cell) => (
          <div className="icons-template">
            <EditButton
              onClick={(e) => this.onEditRowClick(cell.original, e)}
            />
            {cell.original.doc_type !== "Change Request" && (
              <DeleteButton
                type="cloned"
                title="Clone document"
                onClick={(e) => this.onCloneClick(cell.value, e)}
              />
            )}
            {cell.original.quote !== null && getPDFPrevieMarkUp(cell)}
            {getEmailsendingMarkUp(cell)}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Download",
        sortable: false,
        width: 80,
        Cell: (cell) => (
          <div className="icons-template">
            {cell.original.quote === null && (
              <img
                title={"Please add Opportunity for this SOW"}
                className="quote"
                alt=""
                src="/assets/icons/warning.png"
              />
            )}
            {cell.original.doc_type !== "Change Request" &&
              cell.original.quote !== null &&
              getDOCXDownloadMarkUp(cell)}
            {cell.original.quote !== null && getPDFDownloadMarkUp(cell)}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: () => {},
    };

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
      defaultPageSize: 25,
    };

    return (
      <div className="sow-metrics-listing col-md-12">
        <Table
          columns={columns}
          rows={this.state.rows || []}
          manualProps={manualProps}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`rules-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          onRowClick={() => {}}
          defaultSorted={[
            {
              id: "",
              desc: true,
            },
          ]}
          loading={this.props.isFetching}
        />
      </div>
    );
  };

  tabs = () => (
    <div className="failed-tickets__header">
      {<div className={`failed-tickets__header-link --active`}>SOW's</div>}
    </div>
  );

  handleChangeMargin = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetValue = e.target.checked;
    const newState = cloneDeep(this.state);
    (newState.showMarginOpen as any) = !targetValue ? "Open" : "Won";
    this.setState(newState, () => {
      this.debouncedFetch({
        page: 1,
        page_size: this.state.pagination.params.page_size,
      });
      this.loadGraphsData();
    });
  };

  render() {
    return (
      <div className="sow-metrics">
        <div className="open-won-switch">
          Open
          <input
            className="switch"
            type="checkbox"
            onChange={(e) => this.handleChangeMargin(e)}
          />
          Won
        </div>
        {
          <div className="charts-container">
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.sowByTech,
                "sowByTech",
                this.state.loadingSOWByTech,
                "SoW's by Technology",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.sowAuthors,
                "sowAuthors",
                this.state.loadingSOWAuthors,
                "SoW's Author",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.marginData,
                "marginData",
                this.state.loadingMarginData,
                "Margin",
                this.priorityColorMap
              )}
            </div>
            <div className="col-md-3 graph-box">
              {rawDoughnutChart(
                this,
                this.state.sowDocumentsLastUpdated,
                "sowDocumentsLastUpdated",
                this.state.loadingSOWDocLastUpdated,
                "SOW Last Update",
                this.priorityColorMap
              )}
            </div>
          </div>
        }
        {this.tabs()}
        {this.listingDashboard()}
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
        <RenameBox
          show={this.state.isopenRename}
          onClose={this.toggleOpenRename}
          onSubmit={this.onRenameAndDownload}
          name={this.state.documentName}
          isLoading={this.props.isFetching}
        />
        {this.state.showFiltersModal && (
          <Filters
            show={true}
            onClose={() => this.toggleFilterModal(false)}
            filters={this.state.tableFilters}
            onSubmit={this.applyTableFilters}
            disableUpdateDate={Boolean(
              this.state.pagination.params.updated_after ||
                this.state.pagination.params.updated_before
            )}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.sow.isFetching,
  SOWMetricsTable: state.sow.SOWMetricsTable,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  sendEmailToAccountManager: (id: number) =>
    dispatch(sendEmailToAccountManager(id)),
  getSOWAuthors: (params: ISoWMetricsFilterParams) =>
    dispatch(getSOWAuthors(params)),
  getMarginData: (params: ISoWMetricsFilterParams) =>
    dispatch(getMarginData(params)),
  getSOWByTechnologies: (params: ISoWMetricsFilterParams) =>
    dispatch(getSOWByTechnologies(params)),
  getSOWDocumentsLastUpdated: (params: ISoWMetricsFilterParams) =>
    dispatch(getSOWDocumentsLastUpdated(params)),
  getTableData: (params?: IServerPaginationParams & ISoWMetricsFilterParams) =>
    dispatch(getTabledataSOWMetrics(params)),
  getFullTableData: (
    params?: IServerPaginationParams & ISoWMetricsFilterParams
  ) => dispatch(getFullSOWMetricsTable(params)),
  downloadSOWDocumnet: (id: number, type: string, name?: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SOWMetrics);
