import moment from "moment";
import React from "react";
import { cloneDeep } from "lodash";
// import DateRangePicker from "react-daterange-picker"; (New Component)
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import ModalBase from "../../../../components/ModalBase/modalBase";
import "../../../../commonStyles/filterModal.scss";
import "./style.scss";
import { commonFunctions } from "../../../../utils/commonFunctions";

interface ISowMetricsFilterProps {
  show: boolean;
  onClose: () => void;
  filters: ISowTableFilters;
  disableUpdateDate: boolean;
  onSubmit: (filters: ISowTableFilters) => void;
}

interface ISowMetricsFilterState {
  filters: ISowTableFilters;
  updatedDateInfo: {
    isDatePickerSelected: boolean;
    isDateValueSelected: boolean;
  };
  createdDateInfo: {
    isDatePickerSelected: boolean;
    isDateValueSelected: boolean;
  };
  errors: {
    updatedDateInfo: IFieldValidation;
    createdDateInfo: IFieldValidation;
  };
}

const DateRangeOptions: string[] = [
  "This Year",
  "Last Year",
  "This Quarter",
  "Last Quarter",
  "Custom Range",
];

const StatusOptions: IPickListOptions[] = [
  { value: "Open", label: "Open" },
  { value: "Won", label: "Won" },
  { value: "Open,Won", label: "Both" },
];

interface ISowTableFilters {
  "doc-status": string;
  created_after: string;
  created_before: string;
  updated_after: string;
  updated_before: string;
  created_status: string;
  updated_status: string;
}

export default class AgreementFilter extends React.Component<
  ISowMetricsFilterProps,
  ISowMetricsFilterState
> {
  constructor(props: ISowMetricsFilterProps) {
    super(props);

    this.state = {
      filters: {
        "doc-status": "",
        created_after: "",
        created_before: "",
        updated_after: "",
        updated_before: "",
        created_status: "",
        updated_status: "",
      },
      updatedDateInfo: {
        isDatePickerSelected: false,
        isDateValueSelected: false,
      },
      createdDateInfo: {
        isDatePickerSelected: false,
        isDateValueSelected: false,
      },
      errors: {
        updatedDateInfo: commonFunctions.getErrorState(),
        createdDateInfo: commonFunctions.getErrorState(),
      },
    };
  }

  componentDidMount(): void {
    this.setState({
      filters: this.props.filters,
      updatedDateInfo: {
        isDatePickerSelected: false,
        isDateValueSelected: Boolean(
          this.props.filters.updated_status === "Custom Range"
        ),
      },
      createdDateInfo: {
        isDatePickerSelected: false,
        isDateValueSelected: Boolean(
          this.props.filters.created_status === "Custom Range"
        ),
      },
    });
  }

  calculateDate = (duration: string, key: "updated" | "created") => {
    let to: string;
    let from: string;
    const afterDuration = key + "_after";
    const beforeDuration = key + "_before";
    switch (duration) {
      case "This Year":
        from = moment()
          .startOf("year")
          .format("YYYY-MM-DD");
        to = moment().format("YYYY-MM-DD");
        break;

      case "Last Year":
        from = moment()
          .subtract(1, "year")
          .startOf("year")
          .format("YYYY-MM-DD");
        to = moment()
          .subtract(1, "year")
          .endOf("year")
          .format("YYYY-MM-DD");
        break;

      case "This Quarter":
        from = moment()
          .startOf("quarter")
          .format("YYYY-MM-DD");
        to = moment().format("YYYY-MM-DD");
        break;

      case "Last Quarter":
        from = moment()
          .subtract(1, "quarter")
          .startOf("quarter")
          .format("YYYY-MM-DD");
        to = moment()
          .subtract(1, "quarter")
          .endOf("quarter")
          .format("YYYY-MM-DD");
        break;
    }
    return {
      [afterDuration]: from,
      [beforeDuration]: to,
    };
  };

  onClose = () => {
    this.props.onClose();
  };

  onSubmit = () => {
    if (this.isValid()) {
      this.props.onSubmit(cloneDeep(this.state.filters));
    }
  };

  onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    let dateFilters = {};
    if (
      e.target.name === "updated_status" ||
      e.target.name === "created_status"
    ) {
      dateFilters = this.calculateDate(
        e.target.value,
        e.target.name === "updated_status" ? "updated" : "created"
      );
    }

    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        ...dateFilters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  showDateRangePicker = (key: "updatedDateInfo" | "createdDateInfo") => {
    const newState = cloneDeep(this.state);
    (newState[key] as any) = {
      isDatePickerSelected: !this.state[key].isDatePickerSelected,
      isDateValueSelected: false,
    };
    if (key === "updatedDateInfo") {
      newState.filters.updated_before = "";
      newState.filters.updated_after = "";
    } else {
      newState.filters.created_before = "";
      newState.filters.created_after = "";
    }
    this.setState(newState);
  };

  onDateClear = (key: "updatedDateInfo" | "createdDateInfo") => {
    const newState = cloneDeep(this.state);
    newState[key].isDateValueSelected = false;
    if (key === "updatedDateInfo") {
      newState.filters.updated_before = "";
      newState.filters.updated_after = "";
    } else {
      newState.filters.created_before = "";
      newState.filters.created_after = "";
    }
    this.setState(newState);
  };

  onSelect = (value, key: "updatedDateInfo" | "createdDateInfo") => {
    const dateAfter = value.start.format("YYYY-MM-DD");
    const dateBefore = value.end.format("YYYY-MM-DD");
    const newState = cloneDeep(this.state);
    newState[key].isDateValueSelected = true;
    newState[key].isDatePickerSelected = false;
    if (key === "updatedDateInfo") {
      newState.filters.updated_after = dateAfter;
      newState.filters.updated_before = dateBefore;
    } else {
      newState.filters.created_after = dateAfter;
      newState.filters.created_before = dateBefore;
    }
    this.setState(newState);
  };

  isValid = () => {
    let isValid: boolean = true;
    const error = cloneDeep(this.state.errors);
    if (
      this.state.filters.created_status === "Custom Range" &&
      !this.state.filters.created_after
    ) {
      isValid = false;
      error.createdDateInfo = commonFunctions.getErrorState(
        "Please select a date range"
      );
    } else error.createdDateInfo = commonFunctions.getErrorState();
    if (
      this.state.filters.updated_status === "Custom Range" &&
      !this.state.filters.updated_after
    ) {
      isValid = false;
      error.updatedDateInfo = commonFunctions.getErrorState(
        "Please select a date range"
      );
    } else error.updatedDateInfo = commonFunctions.getErrorState();
    this.setState({ errors: error });
    return isValid;
  };

  getBody = () => {
    const filters = this.state.filters;

    const updatedDateValue =
      filters.updated_after || filters.updated_before
        ? {
            start: moment(filters.updated_after),
            end: moment(filters.updated_before),
          }
        : null;

    const createdDateValue =
      filters.created_after || filters.created_before
        ? {
            start: moment(filters.created_after),
            end: moment(filters.created_before),
          }
        : null;

    return (
      <div className="filters-modal__body sow-metrics-filter col-md-12">
        <Input
          field={{
            label: "Status",
            type: "PICKLIST",
            value: filters["doc-status"],
            isRequired: false,
            options: StatusOptions,
          }}
          multi={false}
          width={6}
          placeholder="Select Status"
          name="doc-status"
          onChange={this.onFilterChange}
          className="metrics-filter-selection"
        />

        <div className="sow-metrics-filter-row col-md-12">
          <Input
            field={{
              label: "Update Date",
              type: "PICKLIST",
              value: filters.updated_status,
              isRequired: false,
              options: DateRangeOptions,
            }}
            width={6}
            placeholder="Select Date Range"
            name="updated_status"
            onChange={this.onFilterChange}
            className="filter-date-selection"
            disabled={this.props.disableUpdateDate}
            error={this.state.errors.updatedDateInfo}
          />
          {this.state.filters.updated_status === "Custom Range" && (
            <div className="col-md-6">
              <div className="field__label">
                <label className="field__label-label">Update between</label>
              </div>
              <div className="date-range-box field__input">
                <SquareButton
                  onClick={() => this.showDateRangePicker("updatedDateInfo")}
                  content=""
                  bsStyle={"default"}
                  className="date-button"
                />

                {/* {this.state.updatedDateInfo.isDatePickerSelected &&
                  !this.state.updatedDateInfo.isDateValueSelected && (
                    <DateRangePicker
                      value={updatedDateValue}
                      onSelect={(e) => this.onSelect(e, "updatedDateInfo")}
                      singleDateRange={true}
                      minimumDate={new Date("1990/01/01")}
                      maximumDate={new Date()}
                      numberOfCalendars={1}
                      showLegend={true}
                    />
                  )} */}
                {updatedDateValue &&
                  !this.state.updatedDateInfo.isDatePickerSelected &&
                  this.state.updatedDateInfo.isDateValueSelected && (
                    <>
                      <div className="date-value">
                        {updatedDateValue.start.format("MM/DD/YYYY")}
                        {" - "}
                        {updatedDateValue.end.format("MM/DD/YYYY")}
                      </div>
                      <div
                        className="date-clear"
                        onClick={() => this.onDateClear("updatedDateInfo")}
                      >
                        &#x00d7;
                      </div>
                    </>
                  )}
              </div>
            </div>
          )}
        </div>
        <div className="sow-metrics-filter-row col-md-12">
          <Input
            field={{
              label: "Create Date",
              type: "PICKLIST",
              value: filters.created_status,
              isRequired: false,
              options: DateRangeOptions,
            }}
            width={6}
            placeholder="Select Date Range"
            name="created_status"
            onChange={this.onFilterChange}
            className="filter-date-selection"
            error={this.state.errors.createdDateInfo}
          />
          {this.state.filters.created_status === "Custom Range" && (
            <div className="col-md-6">
              <div className="field__label">
                <label className="field__label-label">Update between</label>
              </div>
              <div className="date-range-box field__input">
                <SquareButton
                  onClick={() => this.showDateRangePicker("createdDateInfo")}
                  content=""
                  bsStyle={"default"}
                  className="date-button"
                />

                {/* {this.state.createdDateInfo.isDatePickerSelected &&
                  !this.state.createdDateInfo.isDateValueSelected && (
                    <DateRangePicker
                      value={createdDateValue}
                      onSelect={(e) => this.onSelect(e, "createdDateInfo")}
                      singleDateRange={true}
                      minimumDate={new Date("1990/01/01")}
                      maximumDate={new Date()}
                      numberOfCalendars={1}
                      showLegend={true}
                    />
                  )} */}
                {createdDateValue &&
                  !this.state.createdDateInfo.isDatePickerSelected &&
                  this.state.createdDateInfo.isDateValueSelected && (
                    <>
                      <div className="date-value">
                        {createdDateValue.start.format("MM/DD/YYYY")}
                        {" - "}
                        {createdDateValue.end.format("MM/DD/YYYY")}
                      </div>
                      <div
                        className="date-clear"
                        onClick={() => this.onDateClear("createdDateInfo")}
                      >
                        &#x00d7;
                      </div>
                    </>
                  )}
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
