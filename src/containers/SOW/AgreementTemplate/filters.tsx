import moment from "moment";
import React from "react";
// import DateRangePicker from "react-daterange-picker"; (New Component)

import SquareButton from "../../../components/Button/button";
import Select from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import "../../../commonStyles/filterModal.scss";

interface ITemplateFilterProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: ITemplateFilters) => void;
  prevFilters?: ITemplateFilters;
  authors: any;
}

interface ITemplateFilterState {
  filters: ITemplateFilters;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
}

export default class TemplateFilter extends React.Component<
  ITemplateFilterProps,
  ITemplateFilterState
> {
  constructor(props: ITemplateFilterProps) {
    super(props);

    this.state = {
      filters: {
        author_id: "",
        updated_before: "",
        updated_after: "",
      },
      isDatePickerSelected: false,
      isDateValueSelected: false,
    };
  }

  componentDidUpdate(prevProps: ITemplateFilterProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
        isDateValueSelected: Boolean(
          this.props.prevFilters && this.props.prevFilters.updated_before
        ),
      });
    }
  }

  onClose = (e) => {
    this.props.onClose(e);
  };

  onSubmit = (e) => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  showDateRangePicker = () => {
    const prevFilter = { ...this.state.filters };
    this.setState({
      isDatePickerSelected: !this.state.isDatePickerSelected,
      isDateValueSelected: false,
      filters: {
        ...prevFilter,
        updated_after: "",
        updated_before: "",
      },
    });
  };

  onDateClear = () => {
    const prevFilters = this.state.filters;
    this.setState({
      filters: {
        ...prevFilters,
        updated_after: "",
        updated_before: "",
      },
      isDateValueSelected: false,
    });
  };

  onSelect = (value) => {
    this.setState({
      filters: {
        ...this.state.filters,
        updated_after: value.start.format("YYYY-MM-DD"),
        updated_before: value.end.format("YYYY-MM-DD"),
      },
      isDatePickerSelected: false,
      isDateValueSelected: true,
    });
  };

  getBody = () => {
    const authors = this.props.authors
      ? this.props.authors.map((author) => ({
          value: author.author_id,
          label: author.author_name,
        }))
      : [];

    const filters = this.state.filters;

    const dateValue = filters.updated_after
      ? {
          start: moment(filters.updated_after),
          end: moment(filters.updated_before),
        }
      : null;

    return (
      <div className="filters-modal__body agr-templates-filter col-md-12">
        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Author</label>
          </div>
          <div className="field__input">
            <Select
              name="author_id"
              value={filters.author_id}
              onChange={this.onFilterChange}
              options={authors}
              multi={false}
              placeholder="Select Author"
            />
          </div>
        </div>

        <div className="field-section col-md-6">
          <div className="field__label">
            <label className="field__label-label">Updated between</label>
          </div>
          <div className="date-range-box field__input">
            <SquareButton
              onClick={this.showDateRangePicker}
              content=""
              bsStyle={"default"}
              className="date-button"
            />

            {/* {this.state.isDatePickerSelected &&
              !this.state.isDateValueSelected && (
                <DateRangePicker
                  value={dateValue}
                  onSelect={this.onSelect}
                  singleDateRange={true}
                  minimumDate={new Date("1990/01/01")}
                  maximumDate={new Date()}
                  numberOfCalendars={1}
                  showLegend={true}
                />
              )} */}
            {!this.state.isDatePickerSelected &&
              this.state.isDateValueSelected && (
                <>
                  <div className="date-value">
                    {dateValue.start.format("MM/DD/YYYY")}
                    {" - "}
                    {dateValue.end.format("MM/DD/YYYY")}
                  </div>
                  <div className="date-clear" onClick={this.onDateClear}>
                    &#x00d7;
                  </div>
                </>
              )}
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
