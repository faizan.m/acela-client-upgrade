import React from "react";
import { connect } from "react-redux";
import { debounce, DebouncedFunc } from "lodash";
import moment from "moment";

import EditButton from "../../../components/Button/editButton";
import Spinner from "../../../components/Spinner";
import Table from "../../../components/Table/table";
import {
  createAgreementTemplate,
  CREATE_TEMPLATE_SUCCESS,
  FETCH_TEMPLATES_AUTHORS_SUCCESS,
  DOWNLOAD_AGREEMENT_SUCCESS,
  getAgreementTemplateList,
  getTemplatePreview,
  getAgreementTemplateAuthorsList,
} from "../../../actions/agreement";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import ConfirmBox from "../../../components/ConfirmBox/ConfirmBox";
import Input from "../../../components/Input/input";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import ViewTemplateHistory from "./templateHistory";
import TemplatesOverallHistory from "./templatesHistory";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../components/Button/deleteButton";
import TemplateFilter from "./filters";
import IconButton from "../../../components/Button/iconButton";
import "../../../commonStyles/filtersListing.scss";
import "../../../commonStyles/versionHistory.scss";
import "./style.scss";

interface ITemplateListProps extends ICommonProps {
  Template: any;
  getTemplateList: any;
  createTemplate: any;
  editTemplate: any;
  isFetching: boolean;
  loggenInUser: ISuperUser;
  templates: any;
  createAgreementTemplate: any;
  isFetchingTemplates: boolean;
  getTemplatePreview: any;
  getAgreementTemplateAuthorsList: any;
}

interface ITemplateListState {
  errorList?: any;
  TemplateList: any[];
  rows: any[];
  isTemplatePosting: boolean;
  downloadingPDFIds: number[];
  isopenConfirm: boolean;
  id: string;
  isFilterModalOpen: boolean;
  filters: ITemplateFilters;
  showDisable: boolean;
  viewhistory: boolean;
  openPreview: boolean;
  previewPDFIds: number[];
  previewHTML: any;
  pagination: {
    totalRows: number;
    totalPages: number;
    params?: AgreementTemplatesQueryParams;
  };
  reset: boolean;
  authors: any[];
  viewOverallHistory: boolean;
}

class TemplateListing extends React.Component<
  ITemplateListProps,
  ITemplateListState
> {
  private debouncedFetch: DebouncedFunc<
    (params?: IServerPaginationParams) => void
  >;

  constructor(props: ITemplateListProps) {
    super(props);

    this.state = {
      TemplateList: [],
      isTemplatePosting: false,
      isopenConfirm: false,
      downloadingPDFIds: [],
      id: "",
      rows: [],
      isFilterModalOpen: false,
      filters: {
        author_id: "",
        updated_after: "",
        updated_before: "",
      },
      showDisable: false,
      viewhistory: false,
      viewOverallHistory: false,
      openPreview: false,
      previewPDFIds: [],
      previewHTML: null,
      pagination: {
        totalRows: 0,
        totalPages: 0,
        params: {},
      },
      reset: false,
      authors: [],
    };
    this.debouncedFetch = debounce(this.fetchData, 500);
  }

  componentDidMount() {
    this.setAuthorsList();
  }

  componentDidUpdate(prevProps: ITemplateListProps) {
    if (this.props.templates && prevProps.templates !== this.props.templates) {
      this.setRows(this.props);
    }
  }

  fetchData = (params?: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;

    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getTemplateList(this.state.showDisable, newParams);
  };

  setAuthorsList = () => {
    this.props.getAgreementTemplateAuthorsList().then((a) => {
      if (a.type === FETCH_TEMPLATES_AUTHORS_SUCCESS) {
        this.setState({
          authors: a.response,
        });
      }
    });
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  onEditRowClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/setting/agreement-template/${id}`);
  };

  onCloneClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/setting/agreement-template/${id}?cloned=true`);
  };
  onCreateAgreementClick = (id: any, event: any) => {
    event.stopPropagation();
    this.props.history.push(`/sow/agreement/0?template=${id}`);
  };

  toggleTemplatesHistory = () => {
    this.setState({
      viewOverallHistory: !this.state.viewOverallHistory,
    });
  };

  renderTopBar = () => {
    return (
      <div className="Agr-Template-users-listing__actions">
        <div className="row-action header-panel">
          <div className="left">
            <Input
              field={{
                label: "",
                type: "SEARCH",
                value: this.state.pagination.params.search,
                isRequired: false,
              }}
              width={6}
              placeholder="Search Templates"
              name="searchString"
              onChange={this.onSearchStringChange}
              className="search"
            />
            <Checkbox
              isChecked={this.state.showDisable}
              name="option"
              onChange={(e) => this.onChecboxChanged(e)}
            >
              Show disabled templates
            </Checkbox>
          </div>
          <div className="field-section actions-right">
            <SquareButton
              onClick={this.toggleTemplatesHistory}
              content={
                <span>
                  <img
                    alt=""
                    src="/assets/icons/version.svg"
                    style={{ marginRight: "3px" }}
                  />
                  History
                </span>
              }
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.templates && this.props.templates.length === 0
              }
              bsStyle={"primary"}
              className={"filter-agr"}
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  onChecboxChanged = (event: any) => {
    const showDisable = event.target.checked;
    this.setState(
      {
        showDisable,
      },
      () => {
        this.debouncedFetch({
          page: 1,
        });
      }
    );
  };
  setRows = (nextProps: ITemplateListProps) => {
    let templatesData = nextProps.templates;
    let templates: any[] = templatesData.results;

    const rows: any = templates.map((template, index) => ({
      name: template.name,
      is_disabled: template.is_disabled,
      author_name: template.author_name ? template.author_name : "N.A.",
      updated_on: template.updated_on ? template.updated_on : "N.A.",
      updated_by_name: template.updated_by_name
        ? template.updated_by_name
        : "N.A.",
      id: template.id,
      version: template.version,
      index,
    }));

    this.setState((prevState) => ({
      reset: false,
      rows,
      pagination: {
        ...prevState.pagination,
        totalRows: templatesData.count,
        totalPages: Math.ceil(
          templatesData.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onFilterRemove = (removedFilters: ITemplateFilters) => {
    this.setState(
      (prevState) => ({
        filters: {
          ...prevState.filters,
          ...removedFilters,
        },
      }),
      () => {
        this.setState((prevState) => ({
          pagination: {
            ...prevState.pagination,
            params: {
              ...prevState.pagination.params,
              ...prevState.filters,
            },
          },
        }));
      }
    );
    this.debouncedFetch({
      page: 1,
    });
  };

  renderFilters = () => {
    const { filters } = this.state;
    const shouldRenderFilters =
      filters.author_id || (filters.updated_before && filters.updated_after);

    const author_name = filters.author_id
      ? this.state.authors.find(
          (author) => author.author_id === filters.author_id
        ).author_name
      : "";

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.author_id && (
          <div className="section-show-filters agr-applied-filters-row">
            <label>Author: </label>
            <div className="agr-filter-content">
              {author_name + "  "}
              <span
                className="agr-filter-clear-icon"
                onClick={() => this.onFilterRemove({ author_id: undefined })}
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
        {filters.updated_after && (
          <div className="section-show-filters agr-applied-filters-row">
            <label>Updated between:</label>
            <div className="agr-filter-content">
              {moment(this.state.filters.updated_after).format("MM/DD/YYYY")}
              {" - "}
              {moment(this.state.filters.updated_before).format("MM/DD/YYYY")}
              <span
                className="date-clear agr-filter-clear-icon"
                onClick={() =>
                  this.onFilterRemove({
                    updated_before: undefined,
                    updated_after: undefined,
                  })
                }
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
      </div>
    ) : null;
  };

  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props
      .createAgreementTemplate("delete", { id: this.state.id })
      .then((action) => {
        if (action.type === CREATE_TEMPLATE_SUCCESS) {
          this.debouncedFetch();
          this.toggleConfirmOpen();
        }
      });
  };

  toggleHistoryPopup = () => {
    this.setState({
      viewhistory: false,
      id: "",
    });
  };

  onClickViewHistory(id: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      viewhistory: true,
      id,
    });
  }

  addTemplateClick = () => {
    this.props.history.push("/setting/agreement-template/0");
  };
  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };

  toggleFilterModal = () => {
    this.setState((prevState) => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };
  onFiltersUpdate = (filters: ITemplateFilters) => {
    if (filters) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            ...filters,
          },
        },
        filters,
      }));
      this.debouncedFetch({
        page: 1,
      });
      this.toggleFilterModal();
    }
  };

  onPDFPreviewClick(template: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, template.id],
    });
    this.props.getTemplatePreview({}, template.id).then((a) => {
      if (a.type === DOWNLOAD_AGREEMENT_SUCCESS) {
        this.setState({
          openPreview: true,
          previewHTML: a.response,
          id: "",
          previewPDFIds: this.state.previewPDFIds.filter(
            (id) => template.id !== id
          ),
        });
      } else {
        this.setState({
          id: "",
          previewPDFIds: [],
        });
      }
    });
  }
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  onPDFClick = (template: any, e: any): void => {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      downloadingPDFIds: [...this.state.downloadingPDFIds, template.id],
    });
    this.props.getTemplatePreview({}, template.id).then((action) => {
      if (action.type === DOWNLOAD_AGREEMENT_SUCCESS) {
        this.setState({
          id: "",
          downloadingPDFIds: this.state.downloadingPDFIds.filter(
            (id) => template.id !== id
          ),
        });
        const url = action.response.file_path;
        const link = document.createElement("a");
        link.href = url;
        link.target = "_blank";
        link.setAttribute("download", action.response.file_name);
        document.body.appendChild(link);
        link.click();
      } else {
        this.setState({
          id: "",
          previewPDFIds: [],
        });
      }
    });
  };

  render() {
    const getPDFPrevieMarkUp = (cell) => {
      if (this.state.previewPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <>
            <DeleteButton
              type="pdf_preview"
              title="Preview"
              onClick={(e) => this.onPDFPreviewClick(cell.original, e)}
            />
          </>
        );
      }
    };

    const getPDFDownloadMarkUp = (cell) => {
      if (this.state.downloadingPDFIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_download"
            title="PDF Download"
            onClick={(e) => this.onPDFClick(cell.original, e)}
          />
        );
      }
    };

    const columns: ITableColumn[] = [
      {
        accessor: "name",
        Header: "Name",
        id: "name",
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: "author_name",
        Header: "Author",
        id: "author_name",
        sortable: false,
        Cell: (c) => <div>{c.value}</div>,
      },
      {
        accessor: "updated_by_name",
        Header: "Updated by",
        id: "updated_by_name",
        Cell: (c) => <div>{c.value}</div>,
        sortable: false,
      },
      {
        accessor: "version",
        Header: "Current Version",
        Cell: (c) => <div>{c.original.version}</div>,
        sortable: false,
      },
      {
        accessor: "updated_on",
        Header: "Updated On",
        id: "updated_on",
        sortable: true,
        width: 110,
        Cell: (cell) => (
          <div>
            {" "}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."
            }`}
          </div>
        ),
      },
      {
        accessor: "id",
        Header: "Versions",
        sortable: false,
        width: 65,
        Cell: (cell) => (
          <IconButton
            icon="version.svg"
            onClick={(e) => {
              this.onClickViewHistory(cell.original.id, e);
            }}
            className="version-btn"
            title={"Show  versions"}
          />
        ),
      },
      {
        accessor: "id",
        Header: "Preview",
        width: 80,
        sortable: false,
        Cell: (cell) => (
          <div className="view-icons-template">{getPDFPrevieMarkUp(cell)}</div>
        ),
      },
      {
        accessor: "id",
        Header: "Actions",
        sortable: false,
        Cell: (cell) => (
          <>
            {!cell.original.is_disabled && (
              <div>
                <EditButton
                  onClick={(e) => this.onEditRowClick(cell.value, e)}
                />
                <DeleteButton onClick={(e) => this.onDeleteRowClick(cell, e)} />
                <DeleteButton
                  type="cloned"
                  title="Clone template"
                  onClick={(e) => this.onCloneClick(cell.value, e)}
                />
                <DeleteButton
                  type="create"
                  title="Create Agreement"
                  onClick={(e) => this.onCreateAgreementClick(cell.value, e)}
                />
              </div>
            )}
          </>
        ),
      },
      {
        accessor: "id",
        Header: "Download",
        sortable: false,
        width: 80,
        Cell: (cell) => (
          <div className="icons-template">{getPDFDownloadMarkUp(cell)}</div>
        ),
      },
    ];

    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.debouncedFetch,
      reset: this.state.reset,
    };

    return (
      <div className="agr-template-listing-wrapper">
        <div className="header">
          <h3>Agreement Templates</h3>
          <SquareButton
            content="Add Template"
            onClick={this.addTemplateClick}
            className="save-mapping"
            bsStyle={"primary"}
          />
        </div>
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows || []}
          customTopBar={this.renderTopBar()}
          className={`Template-users-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          loading={this.props.isFetchingTemplates}
          manualProps={manualProps}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetching}
        />

        <TemplateFilter
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          authors={this.state.authors}
          prevFilters={this.state.filters}
        />
        <ViewTemplateHistory
          show={this.state.viewhistory}
          id={this.state.id}
          onClose={this.toggleHistoryPopup}
        />
        {this.state.viewOverallHistory && (
          <TemplatesOverallHistory
            show={this.state.viewOverallHistory}
            onClose={this.toggleTemplatesHistory}
            history={this.props.history}
          />
        )}
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View Template Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  loggenInUser: state.profile.user,
  templates: state.agreement.templates,
  isFetching: state.agreement.isFetching,
  isFetchingTemplates: state.agreement.isFetchingTemplates,
});

const mapDispatchToProps = (dispatch: any) => ({
  getTemplateList: (
    showDisable: boolean,
    params: AgreementTemplatesQueryParams
  ) => dispatch(getAgreementTemplateList(showDisable, params)),
  createAgreementTemplate: (method: string, sow: any) =>
    dispatch(createAgreementTemplate(method, sow)),
  getTemplatePreview: (payload: any, id: any) =>
    dispatch(getTemplatePreview(payload, id)),
  getAgreementTemplateAuthorsList: () =>
    dispatch(getAgreementTemplateAuthorsList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TemplateListing);
