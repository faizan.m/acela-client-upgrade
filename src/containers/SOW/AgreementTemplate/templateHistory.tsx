import React from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { diff as jsonDiff } from "json-diff";

import Spinner from "../../../components/Spinner";
import { utcToLocalInLongFormat } from "../../../utils/CalendarUtil";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import {
  getAgreementTemplateHistory,
  GET_TEMPLATE_HISTORY_SUCCESS,
} from "../../../actions/agreement";
import DiffBox from "../../../components/DiffBox/diffBox";

interface IViewTemplateHistoryProps extends ICommonProps {
  id: any;
  show: boolean;
  onClose: (e: any) => void;
  getAgreementTemplateHistory: any;
}

interface IViewTemplateHistoryState {
  open: boolean;
  templateHistory: IAgreementTemplate[];
  isCollapsed: boolean[];
  loadMore: boolean;
  isLoading: boolean;
}

class ViewTemplateHistory extends React.Component<
  IViewTemplateHistoryProps,
  IViewTemplateHistoryState
> {
  static fieldsToCheck: string[] = [
    "name",
    "sections",
    "products",
    "pax8_products",
    "max_discount",
    "version_description",
  ];

  static emptyAgreementTemplate: IAgreementTemplate = {
    name: "",
    products: [],
    sections: [],
    version_description: "",
    pax8_products: {
      name: null,
      products: null,
    },
  };

  constructor(props: IViewTemplateHistoryProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    open: false,
    templateHistory: [],
    isCollapsed: [],
    loadMore: false,
    isLoading: false,
  });

  componentDidUpdate(prevProps: IViewTemplateHistoryProps) {
    if (this.props.id && prevProps.id !== this.props.id) {
      this.setState({ isLoading: true });
      this.props
        .getAgreementTemplateHistory(this.props.id)
        .then((action) => {
          if (action.type === GET_TEMPLATE_HISTORY_SUCCESS) {
            this.setState({
              templateHistory: action.response.reverse(),
              isCollapsed: Array(action.response.length).fill(false),
              isLoading: false,
            });
          } else {
            this.setState({ isLoading: false });
          }
        })
        .catch(() => this.setState({ isLoading: false }));
    }
  }

  getTitle = () => {
    return <div className="text-center">Template Versions</div>;
  };

  onClickLoadMore = () => {
    this.setState({ loadMore: !this.state.loadMore });
  };

  getProductString = (
    singleProduct: IAgreementProduct,
    modifier: "__old" | "__new",
    onlyCols: boolean = false
  ): any => {
    let name: string =
      singleProduct.name === undefined
        ? undefined
        : typeof singleProduct.name === "object"
        ? singleProduct.name![modifier]
        : singleProduct.name;
    let description =
      singleProduct.description === undefined
        ? undefined
        : typeof singleProduct.description === "object"
        ? singleProduct.description![modifier]
        : singleProduct.description;
    let internal_cost =
      singleProduct.internal_cost === undefined
        ? undefined
        : typeof singleProduct.internal_cost === "object"
        ? singleProduct.internal_cost![modifier]
        : singleProduct.internal_cost;
    let margin =
      singleProduct.margin === undefined
        ? undefined
        : typeof singleProduct.margin === "object"
        ? singleProduct.margin![modifier]
        : singleProduct.margin;
    let quantity =
      singleProduct.quantity === undefined
        ? undefined
        : typeof singleProduct.quantity === "object"
        ? singleProduct.quantity![modifier]
        : singleProduct.quantity;
    let customer_cost =
      singleProduct.customer_cost === undefined
        ? undefined
        : typeof singleProduct.customer_cost === "object"
        ? singleProduct.customer_cost![modifier]
        : singleProduct.customer_cost;
    if (onlyCols) {
      let cols: string[] = [];
      if (singleProduct.name !== undefined) cols.push("Name");
      if (singleProduct.description !== undefined) cols.push("Description");
      if (singleProduct.internal_cost !== undefined)
        cols.push("Internal Cost(in $)");
      if (singleProduct.margin !== undefined) cols.push("Margin(in %)");
      if (singleProduct.quantity !== undefined) cols.push("Quantity");
      if (singleProduct.customer_cost !== undefined)
        cols.push("Customer Cost(in $)");
      return cols;
    } else
      return (
        (name !== undefined ? `${name}\n` : "") +
        (description !== undefined ? `${description}\n` : "") +
        (internal_cost !== undefined ? `${internal_cost}\n` : "") +
        (margin !== undefined ? `${margin}\n` : "") +
        (quantity !== undefined ? `${quantity}\n` : "") +
        (customer_cost !== undefined ? `${customer_cost}\n` : "")
      );
  };

  getColumns = (diff, type: string): string[] => {
    if (type === "sections") {
      let cols = [];
      const section = diff[1];
      if (section.name !== undefined) cols.push("Name");
      if (section.content !== undefined) cols.push("Content");
      if (section.is_locked !== undefined) cols.push("Locked");
      return cols;
    } else if (type === "products") {
      let cols = [];
      const productBundle = diff[1];
      if (productBundle.billing_option !== undefined)
        cols.push("Billing Option");
      if (productBundle.product_type !== undefined) cols.push("Product Type");
      return cols;
    } else if (type === "pax8_products") {
      let cols = [];
      const section = diff[1];
      if (section.sku !== undefined) cols.push("SKU");
      if (section.product_name !== undefined) cols.push("Product Name");
      return cols;
    } else {
      return Object.keys(diff);
    }
  };

  getDiffString = (diff, type: string, diffType: "__old" | "__new") => {
    if (type === "sections") {
      let name: string, content: string, is_locked: boolean;
      const diffStatus = diff[0];
      const section = diff[1];
      if (diffStatus === "~") {
        name = section.name ? section.name[diffType] : undefined;
        content = section.content ? section.content[diffType] : undefined;
        is_locked = section.is_locked ? section.is_locked[diffType] : undefined;
      } else if (
        (diffStatus === "-" && diffType === "__old") ||
        (diffStatus === "+" && diffType === "__new")
      ) {
        name = section.name;
        content = section.content;
        is_locked = section.is_locked;
      } else if (
        (diffStatus === "+" && diffType === "__old") ||
        (diffStatus === "-" && diffType === "__new")
      ) {
        return "";
      }
      return (
        (name !== undefined ? `${name}\n` : "") +
        (content !== undefined ? `\n${content}\n` : "") +
        (is_locked !== undefined ? `\n${is_locked ? "YES" : "NO"}\n` : "")
      );
    } else if (type === "products") {
      let billing_option: string, product_type: string;
      const diffStatus = diff[0];
      const productBundle = diff[1];
      if (diffStatus === "~") {
        billing_option = productBundle.billing_option
          ? productBundle.billing_option[diffType]
          : undefined;
        product_type = productBundle.product_type
          ? productBundle.product_type[diffType]
          : undefined;
      } else if (
        (diffStatus === "-" && diffType === "__old") ||
        (diffStatus === "+" && diffType === "__new")
      ) {
        billing_option = productBundle.billing_option;
        product_type = productBundle.product_type;
      } else if (
        (diffStatus === "+" && diffType === "__old") ||
        (diffStatus === "-" && diffType === "__new")
      ) {
        return "";
      }
      return (
        (billing_option !== undefined ? `${billing_option}\n` : "") +
        (product_type !== undefined ? `${product_type}\n` : "")
      );
    } else if (type === "pax8_products") {
      let sku: string, product_name: string;
      const diffStatus = diff[0];
      const product = diff[1];
      if (diffStatus === "~") {
        sku = product.sku ? product.sku[diffType] : undefined;
        product_name = product.product_name
          ? product.product_name[diffType]
          : undefined;
      } else if (
        (diffStatus === "-" && diffType === "__old") ||
        (diffStatus === "+" && diffType === "__new")
      ) {
        sku = product.sku;
        product_name = product.product_name;
      } else if (
        (diffStatus === "+" && diffType === "__old") ||
        (diffStatus === "-" && diffType === "__new")
      ) {
        return "";
      }
      return (
        (sku !== undefined ? `${sku}\n` : "") +
        (product_name !== undefined ? `\n${product_name}\n` : "")
      );
    } else if (type === "single_product") {
      if (Array.isArray(diff)) {
        const diffStatus = diff[0];
        const singleProduct = diff[1];
        if (
          diffStatus === "~" ||
          (diffStatus === "-" && diffType === "__old") ||
          (diffStatus === "+" && diffType === "__new")
        )
          return this.getProductString(singleProduct, diffType);
        else return "";
      } else {
        return this.getProductString(diff, diffType);
      }
    } else {
      return diff[diffType] === null ? "" : String(diff[diffType]);
    }
  };

  renderHistoryContainer = (
    currentVersion: IAgreementTemplate,
    boardIndex: number,
    prevVersion: IAgreementTemplate
  ) => {
    const isCollapsed = !this.state.isCollapsed[boardIndex];

    let newPrevVersion = cloneDeep(prevVersion);
    let newCurVersion = cloneDeep(currentVersion);
    if (
      prevVersion.pax8_products.products === null &&
      currentVersion.pax8_products.products !== null
    )
      newPrevVersion.pax8_products.products = [];
    if (
      prevVersion.pax8_products.products !== null &&
      currentVersion.pax8_products.products === null
    )
      newCurVersion.pax8_products.products = [];
    const diff = jsonDiff(newPrevVersion, newCurVersion);
    const isDifference: boolean = ViewTemplateHistory.fieldsToCheck.reduce(
      (prev, current) => prev || current in diff,
      false
    );
    const commonDiffBoxProps = {
      splitView: true,
      disableWordDiff: false,
      hideLineNumbers: true,
    };
    const toggleCollapsedState = () =>
      this.setState((prevState) => ({
        isCollapsed: [
          ...prevState.isCollapsed.slice(0, boardIndex),
          !prevState.isCollapsed[boardIndex],
          ...prevState.isCollapsed.slice(boardIndex + 1),
        ],
      }));

    return (
      <div
        key={boardIndex}
        className={`collapsable-section  ${
          isCollapsed
            ? "collapsable-section--collapsed"
            : "collapsable-section--not-collapsed"
        }`}
      >
        <div
          className="col-md-12 collapsable-heading"
          onClick={toggleCollapsedState}
        >
          <div className="left col-md-9">
            <div className="name">
              <span>Updated by: </span>
              {currentVersion.updated_by_name}
            </div>
            <div className="version">
              <span>Version: </span>
              {currentVersion.version}
            </div>
            <div className="date">
              <span>Updated On: </span>
              {utcToLocalInLongFormat(currentVersion.updated_on)}
            </div>
          </div>
        </div>
        <div className="collapsable-contents">
          {isDifference ? (
            <>
              <div className="heading-version">
                <span> {` Previous Version `} </span>
                <span>{` Version ${currentVersion.version}`}</span>
              </div>
              {ViewTemplateHistory.fieldsToCheck.map((field, outerIdx) => {
                if (field in diff) {
                  if (field === "sections") {
                    return (
                      <div key={outerIdx} className="agr-version-split">
                        <div className="agr-custom-vheading">Sections</div>
                        {diff[field].map((sectionDiff, sectionIdx) =>
                          sectionDiff[0] !== " " ? (
                            <div
                              key={sectionIdx}
                              className="version-section-row"
                            >
                              <div className="version-column-split">
                                {this.getColumns(sectionDiff, field).map(
                                  (col, idx) => (
                                    <div key={idx}>{col}</div>
                                  )
                                )}
                              </div>
                              <DiffBox
                                customWrapper={"section-version-split"}
                                diffViewerProps={{
                                  oldValue: this.getDiffString(
                                    sectionDiff,
                                    field,
                                    "__old"
                                  ),
                                  newValue: this.getDiffString(
                                    sectionDiff,
                                    field,
                                    "__new"
                                  ),
                                  ...commonDiffBoxProps,
                                }}
                              />
                            </div>
                          ) : null
                        )}
                      </div>
                    );
                  } else if (field === "products") {
                    return (
                      <div key={outerIdx} className="agr-version-split">
                        <div className="agr-custom-vheading">Products</div>
                        {diff[field].map((productBundleDiff, productIdx) =>
                          productBundleDiff[0] !== " " ? (
                            <div
                              key={productIdx}
                              className="productbundle-split-version"
                            >
                              <div className="version-column-split">
                                {`PRODUCT BUNDLE ${productIdx + 1}`}
                              </div>
                              {this.getColumns(productBundleDiff, field)
                                .length !== 0 && (
                                <div className="singleproduct-split-version">
                                  <div className="version-column-split">
                                    {this.getColumns(
                                      productBundleDiff,
                                      field
                                    ).map((col, idx) => (
                                      <div key={idx}>{col}</div>
                                    ))}
                                  </div>
                                  <DiffBox
                                    diffViewerProps={{
                                      oldValue: this.getDiffString(
                                        productBundleDiff,
                                        field,
                                        "__old"
                                      ),
                                      newValue: this.getDiffString(
                                        productBundleDiff,
                                        field,
                                        "__new"
                                      ),
                                      ...commonDiffBoxProps,
                                      disableWordDiff: true,
                                    }}
                                  />
                                </div>
                              )}
                              {productBundleDiff[1].products && (
                                <div className="version-column-split">
                                  SINGLE PRODUCT CHANGES
                                </div>
                              )}
                              {productBundleDiff[1].products
                                ? productBundleDiff[1].products.map(
                                    (singleProductDiff, idx) =>
                                      (productBundleDiff[0] === "~" &&
                                        singleProductDiff[0] !== " ") ||
                                      productBundleDiff[0] === "-" ||
                                      productBundleDiff[0] === "+" ? (
                                        <div
                                          key={idx}
                                          className="singleproduct-split-version"
                                        >
                                          <div className="version-column-split">
                                            {this.getProductString(
                                              productBundleDiff[0] === "~"
                                                ? singleProductDiff[1]
                                                : singleProductDiff,
                                              "__old",
                                              true
                                            ).map((col: string, idx) => (
                                              <div key={idx}>{col}</div>
                                            ))}
                                          </div>
                                          <DiffBox
                                            diffViewerProps={{
                                              oldValue: this.getDiffString(
                                                productBundleDiff[0] !== "+"
                                                  ? singleProductDiff
                                                  : {},
                                                "single_product",
                                                "__old"
                                              ),
                                              newValue: this.getDiffString(
                                                productBundleDiff[0] !== "-"
                                                  ? singleProductDiff
                                                  : {},
                                                "single_product",
                                                "__new"
                                              ),
                                              ...commonDiffBoxProps,
                                            }}
                                          />
                                        </div>
                                      ) : null
                                  )
                                : null}
                            </div>
                          ) : null
                        )}
                      </div>
                    );
                  } else if (field === "pax8_products") {
                    return (
                      <div key={outerIdx} className="agr-version-split">
                        <div className="agr-custom-vheading">PAX8 Products</div>
                        {diff[field].name && (
                          <DiffBox
                            heading={"Quoted Products Name"}
                            diffViewerProps={{
                              oldValue: this.getDiffString(
                                diff[field].name,
                                "",
                                "__old"
                              ),
                              newValue: this.getDiffString(
                                diff[field].name,
                                "",
                                "__new"
                              ),
                              ...commonDiffBoxProps,
                            }}
                          />
                        )}
                        {diff[field].products &&
                          diff[field].products.map(
                            (productsDiff, productsIdx: number) =>
                              productsDiff[0] !== " " &&
                              this.getColumns(productsDiff, field).length !==
                                0 ? (
                                <div
                                  key={productsIdx}
                                  className={"pax8-product-split"}
                                >
                                  <div className="version-column-split">
                                    {`Product #${productsIdx + 1}`}
                                  </div>
                                  <div className="version-section-row">
                                    <div className="version-column-split">
                                      {this.getColumns(productsDiff, field).map(
                                        (col, idx) => (
                                          <div key={idx}>{col}</div>
                                        )
                                      )}
                                    </div>
                                    <DiffBox
                                      customWrapper={"section-version-split"}
                                      diffViewerProps={{
                                        oldValue: this.getDiffString(
                                          productsDiff,
                                          field,
                                          "__old"
                                        ),
                                        newValue: this.getDiffString(
                                          productsDiff,
                                          field,
                                          "__new"
                                        ),
                                        ...commonDiffBoxProps,
                                        disableWordDiff: true,
                                      }}
                                    />
                                  </div>
                                </div>
                              ) : null
                          )}
                      </div>
                    );
                  } else {
                    return (
                      <DiffBox
                        heading={field.replace(/_/g, " ")}
                        key={outerIdx}
                        diffViewerProps={{
                          oldValue: this.getDiffString(
                            diff[field],
                            field,
                            "__old"
                          ),
                          newValue: this.getDiffString(
                            diff[field],
                            field,
                            "__new"
                          ),
                          ...commonDiffBoxProps,
                          disableWordDiff: field === "max_discount",
                        }}
                      />
                    );
                  }
                } else {
                  return null;
                }
              })}
            </>
          ) : (
            <div className="no-data">NO CHANGE</div>
          )}
        </div>
      </div>
    );
  };

  getBody = () => {
    return (
      <div className="body col-md-12 col-sm-12">
        <div className="loader modal-loader">
          <Spinner show={this.state.isLoading} />
        </div>
        <div className="history-section heading  col-md-12">
          {this.state.templateHistory.length === 0 && (
            <div className="col-md-12 no-data" style={{ textAlign: "center" }}>
              {" "}
              No History available.
            </div>
          )}
          {this.state.templateHistory.length !== 0 &&
            !this.state.loadMore &&
            this.state.templateHistory
              .slice(0, 5)
              .map((history, i, versions) => {
                if (i === versions.length - 1) {
                  return this.renderHistoryContainer(history, i, {
                    ...ViewTemplateHistory.emptyAgreementTemplate,
                  });
                } else {
                  return this.renderHistoryContainer(
                    history,
                    i,
                    versions[i + 1]
                  );
                }
              })}
          {this.state.templateHistory.length !== 0 &&
            this.state.loadMore &&
            this.state.templateHistory.map((history, i, versions) => {
              if (i === versions.length - 1) {
                return this.renderHistoryContainer(history, i, {
                  ...ViewTemplateHistory.emptyAgreementTemplate,
                });
              } else {
                return this.renderHistoryContainer(history, i, versions[i + 1]);
              }
            })}
          {this.state.templateHistory.length > 5 && (
            <div
              className="col-md-12 show-more"
              onClick={(e) => this.onClickLoadMore()}
              style={{ textAlign: "center" }}
            >
              {!this.state.loadMore ? "load more..." : "show less"}
            </div>
          )}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`footer
        ${this.state.isLoading ? `loading` : ""}`}
      />
    );
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="version-history"
      />
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getAgreementTemplateHistory: (id: number) =>
    dispatch(getAgreementTemplateHistory(id)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ViewTemplateHistory)
);
