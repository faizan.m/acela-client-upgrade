import React, { useState } from "react";
import { cloneDeep, isNil } from "lodash";
import { connect } from "react-redux";
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  productCatalogCRUD,
  PRODUCT_CATALOG_SUCCESS,
  PRODUCT_CATALOG_FAILURE,
} from "../../../../actions/sales";
import {
  addInfoMessage,
  addWarningMessage,
} from "../../../../actions/appState";
import IconButton from "../../../../components/Button/iconButton";
import ModalBase from "../../../../components/ModalBase/modalBase";
import InfiniteList from "../../../../components/InfiniteList/infiniteList";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../../components/Spinner";
import { getContractLabel } from "../../../ProductCatalog/CreateProduct";
import SquareButton from "../../../../components/Button/button";
import Checkbox from "../../../../components/Checkbox/checkbox";
import Input from "../../../../components/Input/input";

interface ICatalogProductsProps {
  disabled: boolean;
  products: IQuotedCatalogProduct[];
  catalogName: string;
  nameError: IFieldValidation;
  addInfoMessage: TShowInfoMessage;
  addWarningMessage: TShowWarningMessage;
  handleChangeName: (name: string) => void;
  fetchProductDetails: (id: number) => Promise<any>;
  handleChangeProducts: (products: IQuotedCatalogProduct[]) => void;
  addToQuotedProducts: (product: IProductCatalogItem) => void;
}

const CatalogProducts: React.FC<ICatalogProductsProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [viewMode, setViewMode] = useState<boolean>(false);
  const [currentProductIdx, setCurrentProductIdx] = useState<number>(-1);
  const [currentProduct, setCurrentProduct] = useState<IQuotedCatalogProduct>();

  const formatCurrency = (
    value: number,
    noDecimal: boolean = false
  ): string => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      maximumFractionDigits: noDecimal ? 0 : 2,
    });
    return formatter.format(value);
  };

  const closeModal = () => {
    setViewMode(false);
    setCurrentProductIdx(-1);
    setCurrentProduct(undefined);
  };

  const removeProduct = (idx: number) => {
    props.handleChangeProducts(
      props.products.filter((el, index) => idx !== index)
    );
  };

  const onClickView = (product: IQuotedCatalogProduct, idx: number) => {
    setCurrentProduct(product);
    setCurrentProductIdx(idx);
    setViewMode(true);
  };

  const handleChangeRequired = (e: React.ChangeEvent<HTMLInputElement>) => {
    const updatedProduct = cloneDeep(currentProduct);
    updatedProduct.is_required = e.target.checked;
    setCurrentProduct(updatedProduct);
    props.handleChangeProducts([
      ...props.products.slice(0, currentProductIdx),
      updatedProduct,
      ...props.products.slice(currentProductIdx + 1),
    ]);
  };

  const onRefreshProductData = () => {
    const productId: number = currentProduct.id;
    if (!productId) return;
    setLoading(true);
    props
      .fetchProductDetails(productId)
      .then((action) => {
        if (action.type === PRODUCT_CATALOG_SUCCESS) {
          const productInfo: IProductCatalogItem = action.response;
          const updatedProduct = {
            ...currentProduct,
            ...productInfo,
          };
          setCurrentProduct(updatedProduct);
          props.handleChangeProducts([
            ...props.products.slice(0, currentProductIdx),
            updatedProduct,
            ...props.products.slice(currentProductIdx + 1),
          ]);
          props.addInfoMessage("Refreshed with latest changes!");
        } else if (action.type === PRODUCT_CATALOG_FAILURE) {
          props.addWarningMessage(
            "No product found in the catalog, no changes were made to current product!"
          );
        }
      })
      .finally(() => setLoading(false));
  };

  const renderCatalogProductContainer = () => {
    return (
      <section
        className={
          "pax8-products-container" +
          (props.disabled ? " pax8-container-disabled" : "")
        }
      >
        <div className="pax8-catalog-section">
          <h3>Products Catalog</h3>
          <InfiniteList
            showSearch={true}
            // showIncludeExclude={true}
            height="250px"
            id="pax8-products-catalog"
            className="pax8-infinite-list"
            url={`providers/sales/products`}
            columns={
              [
                {
                  name: "",
                  className: "width-10 align-center",
                  Cell: (product: IProductCatalogItem) => (
                    <IconButton
                      onClick={() => {
                        props.addToQuotedProducts(product);
                        props.addInfoMessage(
                          `Quoted Product ${product.product_id}!`
                        );
                      }}
                      icon="plus.svg"
                      title="Add to Quoted Products"
                      className="add-to-quoted-list"
                      newIcon={true}
                    />
                  ),
                },
                {
                  id: "product_id",
                  ordering: "product_id",
                  name: "Product ID",
                  className: "pax8-sku-col",
                },
                {
                  id: "name",
                  ordering: "name",
                  name: "Name",
                  className: "pax8-name-col",
                },
                {
                  id: "description",
                  ordering: "description",
                  name: "Description",
                  className: "pax8-desc-col text-ellipsis",
                },
                {
                  id: "product_type",
                  ordering: "product_type",
                  name: "Product Type",
                  className: "pax8-vendor-col",
                },
              ] as IColumnInfinite<IProductCatalogItem>[]
            }
          />
        </div>
        {props.products.length !== 0 && (
          <div className="pax8-quoted-products-section">
            <Spinner show={loading} className="pax8-agr-temp-spinner" />
            <h3>Catalog Quoted Products</h3>
            <Input
              field={{
                value: props.catalogName,
                label: "",
                type: "TEXT",
              }}
              width={4}
              name="catalogName"
              onChange={(e) => props.handleChangeName(e.target.value)}
              placeholder="Enter Section Name"
              className="pax8-name"
              disabled={props.disabled}
              error={!props.catalogName.trim() ? props.nameError : undefined}
            />
            <div className="quoted-products-list infinite-list-component">
              <div className="header">
                <div className="cell width-10 pax8-btns" />
                <div className="cell pax8-sku-col">Product ID</div>
                <div className="cell pax8-name-col">Name</div>
                <div className="cell pax8-desc-col">Description</div>
                <div className="cell pax8-vendor-col">Product Type</div>
              </div>
              <div className="quoted-products-rows">
                {props.products.map((product, idx) => (
                  <div className="row-panel" key={idx}>
                    <div className="cell width-10 pax8-btns">
                      <IconButton
                        title="View Product"
                        onClick={() => onClickView(product, idx)}
                        className="view-btn"
                        newIcon={true}
                        icon={"eye-fill.svg"}
                      />
                      <SmallConfirmationBox
                        className="remove"
                        showButton={true}
                        onClickOk={() => removeProduct(idx)}
                        text={"Product"}
                        title="Remove Catalog Product"
                      />
                      <CopyToClipboard
                        text={product.product_id + "\n" + product.name}
                        onCopy={() => {
                          props.addInfoMessage("Text copied!");
                        }}
                      >
                        <IconButton
                          icon="copy.svg"
                          onClick={() => null}
                          newIcon={true}
                          title="Copy line item"
                          className="product-copy"
                        />
                      </CopyToClipboard>
                    </div>
                    <div
                      className="cell pax8-sku-col text-ellipsis"
                      title={product.product_id}
                    >
                      {product.product_id}
                    </div>
                    <div
                      className="cell pax8-name-col text-ellipsis"
                      title={product.name}
                    >
                      {product.name}
                    </div>
                    <div
                      className="cell pax8-desc-col text-ellipsis"
                      title={product.description ? product.description : ""}
                    >
                      {product.description ? product.description : ""}
                    </div>
                    <div
                      className="cell pax8-vendor-col text-ellipsis"
                      title={product.product_type}
                    >
                      {product.product_type}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        )}
      </section>
    );
  };

  const renderViewProductModal = () => {
    const renderLabelWithDetails = (
      width: number,
      label: string,
      detail: string | number
    ) => {
      return (
        <div className={`field-section col-xs-${width}`}>
          <div className="field__label row">
            <div className="field__label-label">{label}</div>
          </div>
          <div className="detail">{!isNil(detail) ? detail : "-"}</div>
        </div>
      );
    };

    return (
      <ModalBase
        show={viewMode}
        onClose={closeModal}
        titleElement={"Preview " + currentProduct.product_id}
        bodyElement={
          <div className="pax8-product-edit-container product-catalog-edit-container">
            <div className="product-config">
              <SquareButton
                onClick={onRefreshProductData}
                content={
                  <img
                    alt="refresh-products"
                    src={"/assets/icons/refresh.svg"}
                  />
                }
                className="refresh-product-btn"
                bsStyle={"primary"}
                title="Refresh latest data from Product Catalog"
              />
              <Checkbox
                isChecked={currentProduct.is_required}
                name="product-required"
                onChange={handleChangeRequired}
              >
                Required Product
              </Checkbox>
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(6, "Name", currentProduct.name)}
              {renderLabelWithDetails(
                6,
                "Product Type",
                currentProduct.product_type
              )}
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(
                12,
                "Description",
                currentProduct.description
              )}
            </div>
            <div className="pax8-modal-row">
              {renderLabelWithDetails(
                4,
                "Unit of Measure",
                currentProduct.measurement_unit
              )}
              {renderLabelWithDetails(
                4,
                "Units Included",
                currentProduct.no_of_units
              )}
              {renderLabelWithDetails(
                4,
                "Revenue Rounding",
                currentProduct.revenue_rounding ? "Yes" : "No"
              )}
            </div>
            <div className="pax8-product-pricing">
              <h4>Pricing Information</h4>
              <div className="pax8-pricing-table catalog-pricing">
                <div className="pax8-pricing-header">
                  <div className="left-header">Billing Term</div>
                  <div className="right-headers">
                    <div className="col-headers">Contract Term</div>
                    <div className="col-headers">Internal Monthly Cost $</div>
                    <div className="col-headers">Margin %</div>
                    <div className="col-headers">Customer Monthly Cost $</div>
                  </div>
                </div>
                {[
                  "Monthly",
                  "Annual",
                  "One Time",
                ]
                  .filter((el) =>
                    Object.keys(currentProduct.pricing_data).includes(el)
                  )
                  .map((billingTerm: CatalogBillingTerm, idx: number) => {
                    const multiplier =
                      billingTerm === "Annual" ? 12 : 1;
                    return (
                      <div className="pax8-pricing-row" key={idx}>
                        <div className="pricing-left-col">{billingTerm}</div>
                        <div className="pricing-right-col">
                          {[
                            Monthly,
                            "12 Months",
                            "24 Months",
                            "36 Months",
                            "One Time",
                          ]
                            .filter((el) =>
                              Object.keys(
                                currentProduct.pricing_data[billingTerm]
                              ).includes(el)
                            )
                            .map(
                              (
                                contractTerm: CatalogContractTerm,
                                idx: number
                              ) => {
                                const productOption =
                                  currentProduct.pricing_data[billingTerm][
                                    contractTerm
                                  ];
                                return (
                                  <div className="rates-row" key={idx}>
                                    <div className="rates-row-info">
                                      {getContractLabel(contractTerm)}
                                    </div>
                                    <div className="rates-row-info">
                                      {formatCurrency(
                                        multiplier * productOption.internal_cost
                                      )}
                                    </div>
                                    <div className="rates-row-info">
                                      {productOption.margin_percent + "%"}
                                    </div>
                                    <div className="rates-row-info">
                                      {formatCurrency(
                                        multiplier * productOption.customer_cost
                                      )}
                                    </div>
                                  </div>
                                );
                              }
                            )}
                        </div>
                      </div>
                    );
                  })}
              </div>
            </div>
          </div>
        }
        footerElement={null}
        className={`pax8-product-edit-modal`}
      />
    );
  };

  return (
    <>
      {renderCatalogProductContainer()}
      {viewMode && renderViewProductModal()}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addInfoMessage: (msg: string) => dispatch(addInfoMessage(msg)),
  addWarningMessage: (msg: string) => dispatch(addWarningMessage(msg)),
  fetchProductDetails: (id: number) =>
    dispatch(
      productCatalogCRUD("get", undefined, {
        id,
      } as IProductCatalogItem)
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(CatalogProducts);
