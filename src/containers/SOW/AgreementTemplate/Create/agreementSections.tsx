import { cloneDeep, some } from "lodash";
import { DraggableArea } from "react-draggable-tags";
import React from "react";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../../components/Spinner";
import { QuillEditorAcela } from "../../../../components/QuillEditor/QuillEditor";
import "../../../../commonStyles/collapsable.scss";
import "./style.scss";

interface ISectionProps {
  sections: ISection[];
  setSections: any;
  isAgreement: boolean;
  disabled?: boolean;
}

interface ISectioState {
  isCollapsed: any[];
  isEditable: boolean;
  loading: boolean;
  file: any;
}

export default class Sections extends React.Component<
  ISectionProps,
  ISectioState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  creatableEl: any;

  constructor(props: ISectionProps) {
    super(props);

    this.state = {
      loading: false,
      isCollapsed: [],
      isEditable: false,
      file: null,
    };
  }

  componentDidMount(): void {
    const newState = cloneDeep(this.state);
    (newState.isCollapsed as any) = this.props.sections.map((key) => false);
    this.setState(newState);
  }

  handleChangeName = (
    e: { target: { name: string; value: string } },
    index: number
  ) => {
    const name = e.target.name;
    const newSectionList: ISection[] = cloneDeep(this.props.sections);
    (newSectionList[index][name] as string) = e.target.value;
    this.props.setSections(newSectionList);
  };

  handleChangeMarkdown = (htmlString: string, index: number) => {
    const newSectionList: ISection[] = cloneDeep(this.props.sections);
    (newSectionList[index].value_html as string) = htmlString;
    this.props.setSections(newSectionList);
  };

  handleChangeLocked = (index: number) => {
    const newSectionList: ISection[] = cloneDeep(this.props.sections);
    (newSectionList[index].is_locked as boolean) =
      !this.props.sections[index].is_locked;
    this.props.setSections(newSectionList);
  };

  handleChange = (tags) => {
    const newState = cloneDeep(this.state);
    const newSectionList: ISection[] = tags;
    (newState.isCollapsed as any) = tags.map((key) => false);
    this.props.setSections(newSectionList);
    this.setState(newState);
  };

  handleRemoveSection = (sectionIdx: number) => {
    const newSections = cloneDeep(this.props.sections);
    newSections.splice(sectionIdx, 1);
    const newState = cloneDeep(this.state);
    (newState.isCollapsed as any) = newSections.map((key) => false);
    this.props.setSections(newSections);
    this.setState(newState);
  };

  toggleCollapsedState = (e, boardIndex) => {
    e.stopPropagation();
    const prevCollapsed = [...this.state.isCollapsed];
    prevCollapsed[boardIndex] = !this.state.isCollapsed[boardIndex];

    this.setState({
      isCollapsed: prevCollapsed,
      isEditable: some(prevCollapsed),
    });
  };

  AddSection = () => {
    const newState = cloneDeep(this.state);
    const newSections = [
      ...this.props.sections,
      {
        name: "Section",
        edited: false,
        is_locked: false,
        nameError: "",
        content: "",
        value_html: "",
        id: Math.floor(Math.random() * 1000 + 1),
      },
    ];
    (newState.isCollapsed as any) = newSections.map((key) => false);
    this.props.setSections(newSections);
    this.setState(newState);
  };

  renderSections = (tag: ISection, index: number) => {
    const isCollapse = this.state.isCollapsed[index];
    const isDisabled =
      this.props.disabled ||
      !this.state.isEditable ||
      (this.props.isAgreement && tag.is_locked);

    return (
      <div
        key={index}
        className={`section-agr collapsable-section  ${
          isCollapse ? "section-agr--collapsed" : "field--not-collapsed"
        }`}
      >
        <h2
          onClick={(e) => this.toggleCollapsedState(e, index)}
          className={
            "section-agr-title collapsable-heading" +
            (tag.error && tag.error.errorState === "error"
              ? " agr-error-border"
              : "") +
            (this.state.isEditable || this.props.isAgreement
              ? " agr-cursor"
              : "")
          }
        >
          {tag.name}

          <div className="collapse-icon">{!isCollapse ? "+" : "-"}</div>
        </h2>
        <div
          className={`section-agr-contents ${
            !isCollapse ? "display-none" : "display-unset"
          }`}
        >
          {isCollapse && (
            <div className="rule-voilation">
              <div className="section tag col-md-12">
                <Input
                  field={{
                    value: tag.name,
                    label: "Section Name",
                    type: "TEXT",
                    isRequired: true,
                  }}
                  width={12}
                  name={"name"}
                  onChange={(e) => this.handleChangeName(e, index)}
                  className="inpt label-input"
                  onMouseDown={(e) => {
                    e.stopPropagation();
                  }}
                  disabled={isDisabled}
                  showErrorOnDisabled={true}
                  error={!tag.name.trim() ? tag.error : undefined}
                />
                <div className="section-markdown-loader">
                  <Spinner show={this.state.loading} />
                </div>
                <QuillEditorAcela
                  onChange={(e) => this.handleChangeMarkdown(e, index)}
                  label="Content"
                  value={tag.value_html}
                  wrapperClass={"agr-section-quill"}
                  scrollingContainer=".add-sow"
                  isRequired={true}
                  isDisabled={isDisabled}
                  error={tag.error}
                  markDownVariables={[
                    { id: "customerName", value: "{Customer Name}" },
                    { id: "pageBreak", value: "{Page Break}" },
                  ]}
                />
                <div className="section-btn-container col-md-12">
                  {this.props.sections.length !== 1 &&
                    this.state.isEditable &&
                    !this.props.disabled && (
                      <SquareButton
                        content={
                          <>
                            <span>Delete</span>
                            <SmallConfirmationBox
                              className="delete-bundle-confirmation"
                              onClickOk={() => this.handleRemoveSection(index)}
                              text={"Bundle"}
                            />
                          </>
                        }
                        bsStyle={"danger"}
                        className="remove-bundle-btn"
                        onClick={() => {}}
                      />
                    )}
                  {tag.is_locked ? (
                    <img
                      className={
                        "section-lock-status-icon" +
                        (this.props.isAgreement || this.props.disabled
                          ? " section-lock-disabled"
                          : "")
                      }
                      src="/assets/icons/lock-fill.svg"
                      alt="Locked section"
                      onClick={
                        this.props.isAgreement || this.props.disabled
                          ? null
                          : () => this.handleChangeLocked(index)
                      }
                    />
                  ) : (
                    <img
                      className={
                        "section-lock-status-icon" +
                        (this.props.isAgreement || this.props.disabled
                          ? " section-lock-disabled"
                          : "")
                      }
                      src="/assets/icons/unlock-fill.svg"
                      alt="Unlocked section"
                      onClick={
                        this.props.isAgreement || this.props.disabled
                          ? null
                          : () => this.handleChangeLocked(index)
                      }
                    />
                  )}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };
  render() {
    return (
      <section className="agreement-sections-list product-main-container">
        <h3 className="heading-three">Sections</h3>

        {!this.state.isEditable && !this.props.isAgreement ? (
          <DraggableArea
            isList
            tags={this.props.sections}
            render={({ tag, index }) => this.renderSections(tag, index)}
            onChange={(tags) => this.handleChange(tags)}
          />
        ) : (
          <>
            {this.props.sections.map((x: ISection, i: number) => {
              return this.renderSections(x, i);
            })}
          </>
        )}

        {!this.props.isAgreement && (
          <SquareButton
            onClick={() => this.AddSection()}
            content={"Add Section"}
            bsStyle={"primary"}
            className="add-product-btn section-add"
            disabled={this.props.disabled}
          />
        )}
      </section>
    );
  }
}
