import React from "react";
import { connect } from "react-redux";
import { pick, cloneDeep } from "lodash";
import moment from "moment";

import Products from "./products";
import Sections from "./agreementSections";
import Spinner from "../../../../components/Spinner";
import SquareButton from "../../../../components/Button/button";
import Input from "../../../../components/Input/input";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import {
  getTemplatePreview,
  getAgreementTemplate,
  createAgreementTemplate,
  getAgreementTemplateHistory,
  CREATE_TEMPLATE_FAILURE,
  CREATE_TEMPLATE_SUCCESS,
  DOWNLOAD_AGREEMENT_SUCCESS,
  DOWNLOAD_AGREEMENT_FAILURE,
  GET_TEMPLATE_HISTORY_SUCCESS,
} from "../../../../actions/agreement";
import {
  productCatalogCRUD,
  PRODUCT_CATALOG_SUCCESS,
} from "../../../../actions/sales";
import Slider from "../../../../components/RangeSlider/maxSlider";
import Checkbox from "../../../../components/Checkbox/checkbox";
import PromptUnsaved from "../../../../components/UnsavedWarning/PromptUnsaved";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { getCustomerCost } from "../../../../utils/agreementCalculations";
import CatalogProducts from "./catalogProducts";
import PAX8Products from "./pax8Products";
import "../../../../commonStyles/pax8_products.scss";
import "./style.scss";

interface IAgreementTemplateProps extends ICommonProps {
  isFetching: boolean;
  isFetchingTemplate: boolean;
  getAgreementTemplate: (id: number) => Promise<any>;
  fetchCatalogProductDetails: (id: number) => Promise<any>;
  getAgreementTemplateHistory: (id: number) => Promise<any>;
  getTemplatePreview: (payload: IAgreementTemplate, id: number) => Promise<any>;
  createAgreementTemplate: (
    method: string,
    template: IAgreementTemplate,
    params?: object
  ) => Promise<any>;
}

interface IAgreementTemplateState {
  open: boolean;
  loading: boolean;
  disabled: boolean;
  agt: IAgreementTemplate;
  openPreview: boolean;
  loadingPreview: boolean;
  previewHTML: IFileResponse;
  // noProduct: boolean;
  enablePAX8: boolean;
  error: {
    name: IFieldValidation;
    pax8Name: IFieldValidation;
    pax8Products: IFieldValidation;
    catalogName: IFieldValidation;
    catalogProductsName: IFieldValidation;
  };
  agtError: boolean;
  unsaved: boolean;
}

class CreateAgreementTemplate extends React.Component<
  IAgreementTemplateProps,
  IAgreementTemplateState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IAgreementTemplateProps) {
    super(props);
    this.state = this.getEmptyState();
  }
  getEmptyState = () => ({
    open: true,
    loading: false,
    disabled: false,
    unsaved: false,
    enablePAX8: false,
    agt: {
      id: 0,
      name: "",
      author_name: null,
      min_discount: 0,
      max_discount: 0,
      pax8_products: { name: null, products: null },
      catalog_products: [],
      catalog_products_name: "Quoted Catalog Products",
      products: [
        {
          is_bundle: false,
          billing_option: "Monthly",
          product_type: "Service",
          id: Math.round(Math.random() * 100000000),
          is_checked: true,
          selected_product: 0,
          is_required: true,
          comment: null,
          products: [
            {
              name: "",
              description: "",
              internal_cost: 0,
              customer_cost: 0,
              margin: 0,
              quantity: 1,
              error: { ...CreateAgreementTemplate.emptyErrorState },
            },
          ],
        },
      ],
      sections: [
        {
          name: "Section",
          edited: true,
          nameError: "",
          content: "",
          value_html: "",
          is_locked: false,
          id: Math.round(Math.random() * 100000000),
          error: { ...CreateAgreementTemplate.emptyErrorState },
        },
      ],
    },
    openPreview: false,
    loadingPreview: false,
    previewHTML: null,
    error: {
      name: { ...CreateAgreementTemplate.emptyErrorState },
      pax8Name: { ...CreateAgreementTemplate.emptyErrorState },
      pax8Products: { ...CreateAgreementTemplate.emptyErrorState },
      catalogName: { ...CreateAgreementTemplate.emptyErrorState },
      catalogProductsName: { ...CreateAgreementTemplate.emptyErrorState },
    },
    agtError: false,
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    const query = new URLSearchParams(this.props.location.search);
    const cloned = query.get("cloned");
    const disabled = query.get("disabled");
    if (id !== "0") {
      if (disabled === "true") {
        this.setState({ loading: true, disabled: true });
        this.props
          .getAgreementTemplateHistory(id)
          .then((action) => {
            if (action.type === GET_TEMPLATE_HISTORY_SUCCESS) {
              const current_template: IAgreementTemplate = action.response.find(
                (template: IAgreementTemplate) => template.id == id
              );
              this.setState({
                agt: current_template,
                enablePAX8: Boolean(
                  current_template.pax8_products &&
                    current_template.pax8_products.name
                ),
              });
            }
          })
          .finally(() => this.setState({ loading: false }));
      } else {
        this.setState({ loading: true });
        let unsaved = false;
        this.props.getAgreementTemplate(id).then((action) => {
          if (action.type === CREATE_TEMPLATE_SUCCESS) {
            const agt: IAgreementTemplate = action.response;
            if (cloned === "true") {
              agt.id = 0;
              agt.name = agt.name + " (cloned)";
              agt.major_version = 1;
              agt.minor_version = 0;
              unsaved = true;
            }
            this.setState({
              agt,
              enablePAX8:
                agt.pax8_products !== null && agt.pax8_products.name !== null,
              unsaved,
            });
          }
          this.setState({ loading: false });
        });
      }
    }
  }

  onPAX8CheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const checked = event.target.checked;
    this.setState((prevState) => ({
      enablePAX8: checked,
      agt: {
        ...prevState.agt,
        pax8_products: checked
          ? { name: "", products: [] }
          : { name: null, products: null },
      },
    }));
  };

  handleChangeName = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.agt[e.target.name] = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };
  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  previewDoc = () => {
    if (this.checkStateValidation()) {
      this.setState({
        agtError: false,
        loadingPreview: true,
      });
      const agt = this.generatePayload();

      const data = pick(agt, [
        "name",
        "version_description",
        "sections",
        "products",
        "pax8_products",
        "major_version",
        "minor_version",
        "author_name",
        "catalog_products",
        "catalog_products_name",
      ]);
      this.props.getTemplatePreview(data, 0).then((a) => {
        if (a.type === DOWNLOAD_AGREEMENT_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            loadingPreview: false,
          });
        }
        if (
          a.type === DOWNLOAD_AGREEMENT_FAILURE &&
          a.errorList &&
          a.errorList.data
        ) {
          this.setValidationErrors(a.errorList.data);
          this.setState({
            openPreview: false,
            loadingPreview: false,
          });
        }
      });
    } else {
      this.setState({ agtError: true });
    }
  };
  generatePayload = () => {
    const sections = this.state.agt.sections;
    const products = this.state.agt.products;
    const agt = cloneDeep(this.state.agt);
    if (this.state.agt && sections) {
      {
        Object.keys(sections).map((k, i) => {
          sections[k].error = undefined;
          sections[k].content = commonFunctions.convertToMarkdown(
            sections[k].value_html
          );
        });
      }
      agt.sections = sections;
    }
    if (this.state.agt && products) {
      products.forEach((bundle: IProductBundle) => {
        bundle.hasError = undefined;
        bundle.products.forEach((product) => {
          product.customer_cost = Number(getCustomerCost(product));
          product.error = undefined;
        });
      });
      agt.products = products;
    }

    return agt;
  };

  callSave = (closeDocument: boolean) => {
    if (this.checkStateValidation()) {
      const agt = this.generatePayload();
      this.setState({ loading: true, agtError: false });
      if (this.state.agt.id && this.state.agt.id !== 0) {
        this.props
          .createAgreementTemplate("put", agt, {
            set_user_as_author: agt.set_user_as_author,
            update_major_version: agt.update_major_version,
          })
          .then((a) => {
            if (a.type === CREATE_TEMPLATE_SUCCESS) {
              if (closeDocument) {
                this.setState(
                  {
                    unsaved: false,
                  },
                  () => {
                    this.props.history.push("/setting/agreement-templates");
                  }
                );
              } else {
                this.setState({ agt: a.response, unsaved: false });
                this.props.history.replace(
                  `/setting/agreement-template/${a.response.id}`
                );
              }
            }

            if (
              a.type === CREATE_TEMPLATE_FAILURE &&
              a.errorList &&
              a.errorList.data
            ) {
              this.setValidationErrors(a.errorList.data);
            }
            this.setState({ loading: false });
          });
      } else {
        this.props.createAgreementTemplate("post", agt).then((a) => {
          if (a.type === CREATE_TEMPLATE_SUCCESS) {
            this.setState(
              {
                unsaved: false,
              },
              () => {
                this.props.history.push("/setting/agreement-templates");
              }
            );
          }
          if (
            a.type === CREATE_TEMPLATE_FAILURE &&
            a.errorList &&
            a.errorList.data
          ) {
            this.setValidationErrors(a.errorList.data);
          }
          this.setState({ loading: false });
        });
      }
    } else {
      this.setState({ agtError: true });
    }
  };

  checkStateValidation = (): boolean => {
    let noError = true;
    const newState = cloneDeep(this.state);
    if (!newState.agt.name.trim()) {
      noError = false;
      newState.error.name = {
        errorState: "error",
        errorMessage: "This field may not be blank",
      };
    }
    if (this.state.enablePAX8 && !newState.agt.pax8_products.name.trim()) {
      noError = false;
      newState.error.pax8Name = {
        errorState: "error",
        errorMessage: "This field may not be blank",
      };
    }
    if (
      this.state.enablePAX8 &&
      newState.agt.pax8_products.products.length === 0
    ) {
      noError = false;
      newState.error.pax8Products = {
        errorState: "error",
        errorMessage: "Please quote at least one product",
      };
    }
    if (
      !newState.agt.catalog_products_name ||
      !newState.agt.catalog_products_name.trim()
    ) {
      noError = false;
      newState.error.catalogProductsName = {
        errorState: "error",
        errorMessage: "This field may not be blank",
      };
    }
    newState.agt.products = this.state.agt.products.map((productBundle) => {
      let productError = false;
      productBundle.products.map((product) => {
        if (
          !product.name.trim() ||
          !product.description.trim() ||
          product.internal_cost === 0
        ) {
          noError = false;
          productError = true;
          product.error = {
            errorState: "error",
            errorMessage: "This field is required",
          };
        } else {
          product.error = { ...CreateAgreementTemplate.emptyErrorState };
        }
        return product;
      });
      productBundle.hasError = productError;
      return productBundle;
    });

    newState.agt.sections = this.state.agt.sections.map((section) => {
      if (
        !section.name.trim() ||
        commonFunctions.isEditorEmpty(section.value_html)
      ) {
        noError = false;
        section.error = {
          errorState: "error",
          errorMessage: "This field is required",
        };
      } else {
        section.error = { ...CreateAgreementTemplate.emptyErrorState };
      }
      return section;
    });
    this.setState(newState);
    return noError;
  };

  setValidationErrors = (errorList) => {
    const error = this.getEmptyState().error;
    const agt = cloneDeep(this.state.agt);

    Object.keys(errorList).map((key) => {
      if (key && error[key]) {
        error[key].errorState = "error";
        error[key].errorMessage = errorList[key];
      }
    });

    const sections = agt.sections;

    if (sections) {
      Object.keys(sections).map((key, index) => {
        const boardContainerData = sections[key];
        {
          Object.keys(boardContainerData).map((k, i) => {
            if (k !== "section_label" && k !== "ordering") {
              if (
                boardContainerData[k] &&
                boardContainerData[k].type === "MARKDOWN"
              ) {
                sections[key][k].value = sections[key][k].value_html
                  ? sections[key][k].value_html
                  : sections[key][k].value;

                sections[key][k].markdownKey = Math.random();
              }
            }
          });
        }
      });
    }
    agt.sections = sections;
    this.setState({ agt, error, agtError: true });
  };

  callbackfnSections = (sections: ISection[]) => {
    const newState = cloneDeep(this.state);
    newState.agt.sections = sections;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  callbackfnProduct = (products: IProductBundle[]) => {
    const newState = cloneDeep(this.state);
    newState.agt.products = products;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangePAX8Name = (pax8Name: string) => {
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        pax8_products: { ...prevState.agt.pax8_products, name: pax8Name },
      },
      unsaved: true,
    }));
  };

  handleChangeCatalogName = (name: string) => {
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        catalog_products_name: name,
      },
      unsaved: true,
    }));
  };

  addToQuotedProducts = (product: IPAX8ProductDetail) => {
    let newProduct: IPAX8Product = {
      id: Math.round(Math.random() * 100000000),
      product_crm_id: product.product_crm_id,
      sku: product.sku,
      product_name: product.product_name,
      short_description: product.short_description,
      vendor_name: product.vendor_name,
      billing_term: "Not Set",
      commitment_term: null,
      commitment_term_in_months: null,
      pricing_type: null,
      unit_of_measurement: null,
      partner_buy_rate: 0,
      suggested_retail_price: 0,
      start_quantity_range: 0,
      end_quantity_range: 0,
      charge_type: null,
      internal_cost: 0,
      customer_cost: 0,
      margin: 0,
      revenue: 0,
      total_margin: 0,
      quantity: 0,
      min_quantity: null,
      max_quantity: null,
      is_comment: false,
      comment: null,
    };

    this.handleChangePAX8Products([
      ...this.state.agt.pax8_products.products,
      newProduct,
    ]);
  };

  addToQuotedCatalogProducts = (product: IProductCatalogItem) => {
    this.props.fetchCatalogProductDetails(product.id).then((action) => {
      if (action.type === PRODUCT_CATALOG_SUCCESS) {
        const productInfo: IProductCatalogItem = action.response;
        const quotedProduct: IQuotedCatalogProduct = {
          ...productInfo,
          billing_term: "Not Set",
          margin: 0,
          revenue: 0,
          quantity: 0,
          comment: null,
          total_margin: 0,
          internal_cost: 0,
          customer_cost: 0,
          is_comment: false,
          is_required: false,
          contract_term: null,
          vendor_name: "LookingPoint",
        };
        this.handleChangeCatalogProducts([
          ...this.state.agt.catalog_products,
          quotedProduct,
        ]);
      }
    });
  };

  handleChangePAX8Products = (products: IPAX8Product[]) => {
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        pax8_products: { ...prevState.agt.pax8_products, products },
      },
      unsaved: true,
    }));
  };

  handleChangeCatalogProducts = (products: IQuotedCatalogProduct[]) => {
    this.setState((prevState) => ({
      agt: {
        ...prevState.agt,
        catalog_products: products,
      },
      unsaved: true,
    }));
  };

  onChecboxChangeAgreement = (event: React.ChangeEvent<HTMLInputElement>) => {
    const show = event.target.checked;
    this.setState((prevState) => ({
      agt: { ...prevState.agt, [event.target.name]: show },
      unsaved: true,
    }));
  };

  render() {
    return (
      <div className="agreement-template-create">
        <div className="add-agreement-template col-md-10">
          <div className="agr-temp-add-edit-header">
            <h3>
              {this.state.agt.id && this.state.agt.id !== 0 ? "Edit " : "Add "}
              Agreement Template
            </h3>
          </div>
          <div className="loader">
            <Spinner
              show={
                this.state.loading ||
                this.props.isFetching ||
                this.props.isFetchingTemplate ||
                this.state.loadingPreview
              }
            />
          </div>
          <div className="basic-field">
            <Input
              field={{
                value: this.state.agt.name,
                label: "Name",
                type: "TEXT",
                isRequired: true,
              }}
              width={6}
              name={"name"}
              placeholder="Enter Agreement Template name"
              onChange={(e) => this.handleChangeName(e)}
              className="agreement-name"
              disabled={this.state.disabled}
              error={this.state.error.name}
            />
            {!this.state.loading && (
              <>
                <Sections
                  sections={this.state.agt.sections}
                  setSections={this.callbackfnSections}
                  isAgreement={false}
                  disabled={this.state.disabled}
                />
                {this.state.enablePAX8 && (
                  <PAX8Products
                    pax8Name={this.state.agt.pax8_products.name}
                    products={this.state.agt.pax8_products.products}
                    noNameError={this.state.error.pax8Name}
                    noProductsError={this.state.error.pax8Products}
                    handleChangeName={this.handleChangePAX8Name}
                    handleChangeProducts={this.handleChangePAX8Products}
                    addToQuotedProducts={this.addToQuotedProducts}
                    disabled={this.state.disabled}
                  />
                )}
                <CatalogProducts
                  products={this.state.agt.catalog_products}
                  nameError={this.state.error.catalogProductsName}
                  catalogName={this.state.agt.catalog_products_name}
                  handleChangeName={this.handleChangeCatalogName}
                  handleChangeProducts={this.handleChangeCatalogProducts}
                  addToQuotedProducts={this.addToQuotedCatalogProducts}
                  disabled={this.state.disabled}
                />
                <Products
                  products={this.state.agt.products}
                  setProductsFn={this.callbackfnProduct}
                  disabled={this.state.disabled}
                />
              </>
            )}

            {this.state.agt && !!this.state.agt.products.length && (
              <div className="service-cost-agreement-temp col-md-6">
                <div className="section-heading">Discount Control</div>
                <div className="sub-heading">Set Maximum Discount</div>
                <div className="slider">
                  <Slider
                    min={0}
                    max={100}
                    maxVal={this.state.agt.max_discount}
                    thumbsize={10}
                    onChange={(e) => {
                      this.setState({
                        agt: {
                          ...this.state.agt,
                          max_discount: e.maxVal,
                        },
                        unsaved: true,
                      });
                    }}
                    disabled={this.state.disabled}
                  />
                </div>
              </div>
            )}

            <Input
              field={{
                value: this.state.agt.version_description,
                label: "Version Description",
                type: "TEXTAREA",
                isRequired: false,
              }}
              width={8}
              name={"version_description"}
              placeholder=" "
              onChange={(e) => this.handleChangeName(e)}
              className="version-description"
              disabled={this.state.disabled}
            />
            <PDFViewer
              show={this.state.openPreview}
              onClose={this.toggleOpenPreview}
              titleElement={`View Agreement Template Preview`}
              previewHTML={this.state.previewHTML}
              footerElement={
                <SquareButton
                  content="Close"
                  bsStyle={"default"}
                  onClick={this.toggleOpenPreview}
                />
              }
              className=""
            />
          </div>
        </div>

        <div className="col-md-2 footer">
          <SquareButton
            content={
              this.state.loadingPreview ? (
                <>
                  Loading{" "}
                  <img
                    className="icon__loading-white"
                    src="/assets/icons/loading.gif"
                    alt="Downloading File"
                  />
                </>
              ) : (
                "Preview"
              )
            }
            onClick={() => this.previewDoc()}
            className="preview-sow-doc"
            bsStyle={"primary"}
          />
          {this.state.agt.id !== 0 && (
            <div className="doc-details col-md-12">
              <div>
                Created by <span>{this.state.agt.author_name}</span>.
              </div>
              <div>
                Last updated by <span>{this.state.agt.updated_by_name}</span> on{" "}
                <span>
                  {moment
                    .utc(this.state.agt.updated_on)
                    .local()
                    .format("MM/DD/YYYY hh:mm A")}
                </span>
              </div>
              {this.state.agt.id && (
                <div>
                  Version -
                  <span>
                    {" " + (this.state.agt.major_version || "1")}.
                    {" " + this.state.agt.minor_version}
                  </span>
                </div>
              )}
            </div>
          )}
          <div className="col-md-12 agr-template-checkboxes">
            <Checkbox
              isChecked={this.state.agt.update_major_version}
              name="update_major_version"
              onChange={this.onChecboxChangeAgreement}
              className="show-hidden-section-checkbox"
              disabled={this.state.disabled}
            >
              Create major version
            </Checkbox>
            <Checkbox
              isChecked={this.state.agt.set_user_as_author}
              name="set_user_as_author"
              onChange={this.onChecboxChangeAgreement}
              className="show-hidden-section-checkbox"
              disabled={this.state.disabled}
            >
              Make me template author
            </Checkbox>
            <Checkbox
              isChecked={this.state.enablePAX8}
              name="pax8-checkbox"
              onChange={(e) => this.onPAX8CheckboxChange(e)}
              className="no-product-checkbox"
              disabled={this.state.disabled}
            >
              Enable PAX8 Module
            </Checkbox>
          </div>

          <SquareButton
            content="Close"
            onClick={() => {
              const query = new URLSearchParams(this.props.location.search);
              const disabled = query.get("disabled");
              disabled === "true"
                ? window.close()
                : this.props.history.goBack();
            }}
            className="save-mapping"
            bsStyle={"default"}
          />

          {Boolean(this.state.agt.id && this.state.agt.id !== 0) && (
            <SquareButton
              content={
                <span>
                  {this.state.loading && (
                    <img
                      src={"/assets/icons/loading.gif"}
                      alt="Downloading File"
                    />
                  )}
                  {"Update Template"}
                </span>
              }
              onClick={() => this.callSave(false)}
              className="save-mapping"
              bsStyle={"primary"}
              disabled={this.state.loading || this.state.disabled}
              title={`Update Agreement Template`}
            />
          )}

          <SquareButton
            content={
              <span>
                {this.state.loading && (
                  <img
                    src={"/assets/icons/loading.gif"}
                    alt="Downloading File"
                  />
                )}
                {this.state.agt.id && this.state.agt.id !== 0
                  ? "Update & Close"
                  : "Save Template"}
              </span>
            }
            onClick={() => this.callSave(true)}
            className="save-mapping"
            bsStyle={"primary"}
            disabled={this.state.loading || this.state.disabled}
            title={`${
              this.state.agt.id && this.state.agt.id !== 0 ? "Update" : "Save"
            } Agreement Template`}
          />
          {this.state.agtError && (
            <div id="agreement-save-error">
              Please fill all the required fields properly
            </div>
          )}
        </div>
        <PromptUnsaved
          when={this.state.unsaved}
          navigate={(path) => this.props.history.push(path)}
          shouldBlockNavigation={(location) => {
            if (this.state.unsaved) {
              return true;
            }
            return false;
          }}
          onSaveClick={(e) => this.callSave(true)}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.agreement.isFetching,
  isFetchingTemplate: state.agreement.isFetchingTemplate,
});

const mapDispatchToProps = (dispatch: any) => ({
  createAgreementTemplate: (
    method: string,
    template: IAgreementTemplate,
    params?: object
  ) => dispatch(createAgreementTemplate(method, template, params)),
  getAgreementTemplate: (id: number) => dispatch(getAgreementTemplate(id)),
  getTemplatePreview: (payload: IAgreementTemplate, id: number) =>
    dispatch(getTemplatePreview(payload, id)),
  getAgreementTemplateHistory: (id: number) =>
    dispatch(getAgreementTemplateHistory(id)),
  fetchCatalogProductDetails: (id: number) =>
    dispatch(
      productCatalogCRUD("get", undefined, {
        id,
      } as IProductCatalogItem)
    ),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateAgreementTemplate);
