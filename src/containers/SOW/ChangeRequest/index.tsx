import { cloneDeep } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import Input from '../../../components/Input/input';
import Spinner from '../../../components/Spinner';
import AppValidators from '../../../utils/validator';
import CustomerUserNew from './../Sow/addUser';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import {
  FETCH_ALL_CUST_USERS_SUCCESS,
  fetchAllCustomerUsers,
} from '../../../actions/documentation';
import { fetchSOWDOCSetting } from '../../../actions/setting';
import {
  CREATE_CR_FAILURE,
  CREATE_CR_SUCCESS,
  CreateCR,
  getQuoteTypeList,
  UPLOAD_IMAGE_CR_FAILURE,
  UPLOAD_IMAGE_CR_SUCCESS,
  uploadImagesForCR,
} from '../../../actions/sow';
import SquareButton from '../../../components/Button/button';
import DeleteButton from '../../../components/Button/deleteButton';
import ConfirmBox from '../../../components/ConfirmBox/ConfirmBox';
import SelectInput from '../../../components/Input/Select/select';
import {
  getCustomerCostTM,
  getInternalCostTM,
  getRecommendedHours,
} from '../../../utils/sowCalculations';
import { commonFunctions } from '../../../utils/commonFunctions';
import '../../../commonStyles/tabs.scss';
import './style.scss';

enum PageType {
  Fixed,
  TNM,
}
interface ICRState {
  currentPage: {
    pageType: PageType;
  };
  name: string;
  type_id: string;
  customer_id: string;
  user_id: string;
  cr_id: string;
  revenue: number;
  cost: number;
  files: any;
  isFormValid: boolean;
  isopenConfirm: boolean;
  engineering_hours: number;
  after_hours: number;
  after_hours_rate: number;
  engineering_hourly_rate: number;
  project_management_hours: number;
  project_management_hourly_rate?: number;
  ChangeRequestProccessed: boolean;
  error: {
    name: IFieldValidation;
    type_id: IFieldValidation;
    customer_id: IFieldValidation;
    user_id: IFieldValidation;
    cost: IFieldValidation;
    revenue: IFieldValidation;
    files: IFieldValidation;
    engineering_hours?: IFieldValidation;
    after_hours?: IFieldValidation;
    after_hours_rate?: IFieldValidation;
    engineering_hourly_rate?: IFieldValidation;
    project_management_hours?: IFieldValidation;
    project_management_hourly_rate?: IFieldValidation;
  };
  isFetching: boolean;
  errorList?: any;
  isCreateUserModal: boolean;
  users: any[];
}

interface ICRProps extends ICommonProps {
  CreateCR: any;
  createtingCR: boolean;
  customers: ICustomerShort[];
  fetchAllCustomerUsers: any;
  uploadImagesForCR: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  getQuoteTypeList: any;
  qTypeList: any;
  isFetchingQTypeList: boolean;
  uploadingImages: boolean;
  fetchSOWDOCSetting: any;
  docSetting: IDOCSetting;
}

class ChangeRequest extends Component<ICRProps, ICRState> {
  constructor(props: ICRProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Fixed,
    },
    name: '',
    type_id: '',
    customer_id: '',
    user_id: '',
    cr_id: '',
    revenue: null,
    cost: null,
    files: null,
    engineering_hours: null,
    after_hours: null,
    after_hours_rate: null,
    engineering_hourly_rate: null,
    project_management_hours: null,
    project_management_hourly_rate: null,
    isFormValid: true,
    isCreateUserModal: false,
    users: [],
    isopenConfirm: false,
    ChangeRequestProccessed: false,
    error: {
      name: {
        errorState: "success",
        errorMessage: '',
      },
      type_id: {
        errorState: "success",
        errorMessage: '',
      },
      customer_id: {
        errorState: "success",
        errorMessage: '',
      },
      user_id: {
        errorState: "success",
        errorMessage: '',
      },
      cost: {
        errorState: "success",
        errorMessage: '',
      },
      revenue: {
        errorState: "success",
        errorMessage: '',
      },
      files: {
        errorState: "success",
        errorMessage: '',
      },
      engineering_hours: {
        errorState: "success",
        errorMessage: '',
      },
      after_hours_rate: {
        errorState: "success",
        errorMessage: '',
      },
      after_hours: {
        errorState: "success",
        errorMessage: '',
      },
      engineering_hourly_rate: {
        errorState: "success",
        errorMessage: '',
      },
      project_management_hours: {
        errorState: "success",
        errorMessage: '',
      },
      project_management_hourly_rate: {
        errorState: "success",
        errorMessage: '',
      },
    },
    errorList: null,
    isFetching: true,
  });

  componentDidMount() {
    this.props.getQuoteTypeList();
    this.props.fetchSOWDOCSetting();
  }

  changePage = (pageType: PageType) => {
    const newstate = this.getEmptyState();
    newstate.currentPage.pageType = pageType;
    if (pageType === PageType.TNM) {
      (newstate.engineering_hourly_rate as any) = this.props.docSetting.engineering_hourly_rate;
      (newstate.after_hours_rate as any) = this.props.docSetting.after_hours_rate;
      (newstate.project_management_hourly_rate as any) = this.props.docSetting.project_management_hourly_rate;
    }
    this.setState(newstate);
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="tab__header">
        <div
          className={`tab__header-link ${
            currentPage.pageType === PageType.Fixed
              ? 'tab__header-link--active'
              : ''
          }`}
          onClick={e => this.changePage(PageType.Fixed)}
        >
          Fixed Fee
        </div>
        <div
          className={`tab__header-link ${
            currentPage.pageType === PageType.TNM
              ? 'tab__header-link--active'
              : ''
          }`}
          onClick={e => this.changePage(PageType.TNM)}
        >
          T & M
        </div>{' '}
      </div>
    );
  };

  handleChange = (e: any): void => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  validateForm() {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.name ||
      (this.state.name && this.state.name.trim().length === 0)
    ) {
      error.name.errorState = "error";
      error.name.errorMessage = 'Name cannot be empty';
      isValid = false;
    }

    if (!this.state.type_id) {
      error.type_id.errorState = "error";
      error.type_id.errorMessage = 'Type cannot be empty';
      isValid = false;
    }
    if (!this.state.customer_id) {
      error.customer_id.errorState = "error";
      error.customer_id.errorMessage = 'Customer cannot be empty';
      isValid = false;
    }
    if (!this.state.user_id) {
      error.user_id.errorState = "error";
      error.user_id.errorMessage = 'User cannot be empty';
      isValid = false;
    }

    if (this.state.currentPage.pageType === PageType.Fixed) {
      if (!this.state.revenue) {
        error.revenue.errorState = "error";
        error.revenue.errorMessage = 'Revenue cannot be empty';
        isValid = false;
      }

      if (!this.state.cost) {
        error.cost.errorState = "error";
        error.cost.errorMessage = 'Cost cannot be empty';
        isValid = false;
      }
      if (this.state.cost && !AppValidators.integerNumber(this.state.cost)) {
        error.cost.errorState = "error";
        error.cost.errorMessage = 'Integer value only';
        isValid = false;
      }
      if (
        this.state.revenue &&
        !AppValidators.integerNumber(this.state.revenue)
      ) {
        error.revenue.errorState = "error";
        error.revenue.errorMessage = 'Integer value only';
        isValid = false;
      }
    }

    if (this.state.currentPage.pageType === PageType.TNM) {
      if (!this.state.engineering_hours) {
        error.engineering_hours.errorState = "error";
        error.engineering_hours.errorMessage =
          'engineering hours cannot be empty';
        isValid = false;
      }
      if (this.state.engineering_hours < 0) {
        error.engineering_hours.errorState = "error";
        error.engineering_hours.errorMessage = 'Invalid value';
        isValid = false;
      }
      if (!this.state.after_hours_rate) {
        error.after_hours_rate.errorState = "error";
        error.after_hours_rate.errorMessage =
          'after hours rate cannot be empty';
        isValid = false;
      }
      if (this.state.after_hours_rate < 0) {
        error.after_hours_rate.errorState = "error";
        error.after_hours_rate.errorMessage = 'Invalid value';
        isValid = false;
      }
      if (!this.state.engineering_hourly_rate) {
        error.engineering_hourly_rate.errorState = "error";
        error.engineering_hourly_rate.errorMessage =
          'engineering hourly rate cannot be empty';
        isValid = false;
      }
      if (this.state.engineering_hourly_rate < 0) {
        error.engineering_hourly_rate.errorState = "error";
        error.engineering_hourly_rate.errorMessage = 'Invalid value';
        isValid = false;
      }
      if (!this.state.project_management_hours) {
        error.project_management_hours.errorState = "error";
        error.project_management_hours.errorMessage =
          'project management hours cannot be empty';
        isValid = false;
      }
      if (this.state.project_management_hours < 0) {
        error.project_management_hours.errorState = "error";
        error.project_management_hours.errorMessage = 'Invalid value';
        isValid = false;
      }
      if (!this.state.project_management_hourly_rate) {
        error.project_management_hourly_rate.errorState =
          "error";
        error.project_management_hourly_rate.errorMessage =
          'project management hourly rate cannot be empty';
        isValid = false;
      }
      if (this.state.project_management_hourly_rate < 0) {
        error.project_management_hourly_rate.errorState =
          "error";
        error.project_management_hourly_rate.errorMessage = 'Invalid value';
        isValid = false;
      }
    }

    if (
      !this.state.files ||
      (this.state.files && this.state.files.length === 0)
    ) {
      error.files.errorState = "error";
      error.files.errorMessage = 'Please upload file(s)';
      isValid = false;
    }
    if (this.state.files && this.state.files.length > 5) {
      error.files.errorState = "error";
      error.files.errorMessage = 'You can upload maximun 5 files';
      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  }

  handleSubmit = () => {
    if (this.validateForm()) {
      const { name, type_id, customer_id, user_id, cost, revenue } = this.state;
      const data = {
        name,
        type_id,
        customer_id,
        user_id,
        cost,
        revenue,
      };
      if (this.state.currentPage.pageType === PageType.TNM) {
        (data.revenue as any) = getCustomerCostTM({
          service_cost: {
            engineering_hours: this.state.engineering_hours,
            engineering_hourly_rate: this.state.engineering_hourly_rate,
            project_management_hours: this.state.project_management_hours,
            project_management_hourly_rate: this.state
              .project_management_hourly_rate,
            after_hours_rate: this.state.after_hours_rate,
            after_hours: this.state.after_hours,
          },
        });
        (data.cost as any) = getInternalCostTM(
          {
            service_cost: {
              engineering_hours: this.state.engineering_hours,
              engineering_hourly_rate: this.state.engineering_hourly_rate,
              project_management_hours: this.state.project_management_hours,
              project_management_hourly_rate: this.state
                .project_management_hourly_rate,
              after_hours_rate: this.state.after_hours_rate,
              after_hours: this.state.after_hours,
            },
          },
          this.props.docSetting
        );
      }

      if (this.state.cr_id === '') {
        this.props.CreateCR(data).then(action => {
          if (action.type === CREATE_CR_SUCCESS) {
            this.toggleConfirmOpen();
            const formData = new FormData();
            for (const file of this.state.files) {
              formData.append('files', file, file.name);
            }
            formData.append('record_id', action.response.id);
            formData.append('record_type', 'Opportunity');
            this.props.uploadImagesForCR(formData).then(a1 => {
              if (a1.type === UPLOAD_IMAGE_CR_SUCCESS) {
                let failed = false;
                Object.entries(a1.response).forEach(([key, value]) => {
                  if (value === 'UPLOAD_FAILED') {
                    failed = true;
                  }
                });
                if (failed) {
                  const newState: ICRState = cloneDeep(this.state);
                  newState.error.files.errorState = "error";
                  const test = [];
                  Object.entries(a1.response).forEach(([key, value]) => {
                    if (value === 'UPLOAD_FAILED') {
                      test.push(`\n File - ${key} - ${value}`);
                    }
                  });
                  newState.error.files.errorMessage = test.join();
                  newState.cr_id = action.response.id;
                  this.setState(newState);
                } else {
                  this.setState({ cr_id: '', ChangeRequestProccessed: true });
                }
              }
              if (a1.type === UPLOAD_IMAGE_CR_FAILURE) {
                const newState: ICRState = cloneDeep(this.state);
                newState.error.files.errorState = "error";
                const test = [];
                Object.entries(a1.errorList.data.files).forEach(
                  ([key, value]) => {
                    test.push(`\n File - ${parseInt(key, 10) + 1} - ${value}`);
                  }
                );
                newState.error.files.errorMessage = test.join();
                newState.cr_id = action.response.id;
                this.setState(newState);
              }
            });
          } else if (action.type === CREATE_CR_FAILURE) {
            this.setState({
              ChangeRequestProccessed: false,
              isopenConfirm: !this.state.isopenConfirm,
            });
            this.setValidationErrors(action.errorList.data);
          }
        });
      }
    }
  };

  updateImages = () => {
    if (this.validateForm()) {
      const formData = new FormData();
      for (const file of this.state.files) {
        formData.append('files', file, file.name);
      }
      formData.append('record_id', this.state.cr_id);
      formData.append('record_type', 'Opportunity');
      this.props.uploadImagesForCR(formData).then(a1 => {
        if (a1.type === UPLOAD_IMAGE_CR_SUCCESS) {
          let failed = false;
          Object.entries(a1.response).forEach(([key, value]) => {
            if (value === 'UPLOAD_FAILED') {
              failed = true;
            }
          });
          if (failed) {
            const newState: ICRState = cloneDeep(this.state);
            newState.error.files.errorState = "error";
            const test = [];
            Object.entries(a1.response).forEach(([key, value]) => {
              if (value === 'UPLOAD_FAILED') {
                test.push(`\n File - ${key} - ${value}`);
              }
            });
            newState.error.files.errorMessage = test.join();
            this.setState(newState);
          } else {
            this.setState({ cr_id: '', ChangeRequestProccessed: true });
          }
        }
        if (a1.type === UPLOAD_IMAGE_CR_FAILURE) {
          const newState: ICRState = cloneDeep(this.state);
          newState.error.files.errorState = "error";
          const test = [];
          Object.entries(a1.errorList.data.files).forEach(([key, value]) => {
            test.push(`File - ${parseInt(key, 10) + 1} - ${value} \n`);
          });
          newState.error.files.errorMessage = test.join();
          this.setState(newState);
        }
      });
    }
  };
  handleChangeCustomer = e => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    (newState.user_id as string) = '';
    this.setState(newState);
    this.props.fetchAllCustomerUsers(e.target.value).then(a => {
      if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const ns = cloneDeep(this.state);
        (ns.users as any) = a.response;
        this.setState(ns);
      }
    });
  };

  handleChangeUser = e => {
    if (e.target.value === 0) {
      this.toggleCreateUserModal();
    } else {
      const newState = cloneDeep(this.state);
      newState[e.target.name] = e.target.value;
      this.setState(newState);
    }
  };

  toArray = fileList => {
    return Array.prototype.slice.call(fileList);
  };

  handleFileSelect = (event: any) => {
    let files = event.target.files;
    if (this.state.files) {
      const oldFiles = this.state.files;
      files = this.toArray(oldFiles).concat(this.toArray(files));
    }
    let fileSize = 0;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < files.length; i++) {
      fileSize = fileSize + files[i].size;
    }
    if (fileSize > 50000000 || files.length > 5) {
      const newState: ICRState = cloneDeep(this.state);
      newState.error.files.errorState = "error";
      // tslint:disable-next-line:max-line-length
      newState.error.files.errorMessage =
        'Max. total files size limit is 50Mb & max. 5 files can upload';
      newState.isFormValid = false;
      this.setState(newState);
    } else {
      const newState: ICRState = cloneDeep(this.state);
      newState.error.files.errorState = "success";
      newState.error.files.errorMessage = '';
      newState.files = files;
      this.setState(newState);
    }
  };

  deleteFile = index => {
    const files = this.toArray(this.state.files);
    files.splice(index, 1);
    let fileSize = 0;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < files.length; i++) {
      fileSize = fileSize + files[i].size;
    }
    if (fileSize > 50000000 || files.length > 5) {
      const newState: ICRState = cloneDeep(this.state);
      newState.error.files.errorState = "error";
      // tslint:disable-next-line:max-line-length
      newState.error.files.errorMessage =
        'Max. total files size limit is 50Mb & max. 5 files can upload';
      newState.isFormValid = false;
      this.setState(newState);
    } else {
      const newState: ICRState = cloneDeep(this.state);
      newState.error.files.errorState = "success";
      newState.error.files.errorMessage = '';
      newState.files = files;
      this.setState(newState);
    }
  };

  toggleCreateUserModal = () => {
    this.setState(prevState => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
  };

  setValidationErrors = errorList => {
    let newState: ICRState = cloneDeep(this.state);
    newState = commonFunctions.errorStateHandle(errorList,newState)
    newState.isFormValid = false;
    this.setState(newState);
  }
  getCustomerOptions = () => {
    if (this.props.customers && this.props.customers.length > 0) {
      return this.props.customers.map(role => ({
        value: role.id,
        label: role.name,
        disabled: false,
      }));
    } else {
      return [];
    }
  };
  getCustomerUserOptions = () => {
    const users = this.state.users
      ? this.state.users.map(t => ({
          value: t.id,
          label: `${t.first_name} ${t.last_name}`,
          disabled: false,
        }))
      : [];

    return users;
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
    });
  };
  submitConfirmOpen = () => {
    if (this.validateForm()) {
      this.setState({
        isopenConfirm: !this.state.isopenConfirm,
      });
    }
  };

  closeUserModal = () => {
    this.setState(prevState => ({
      isCreateUserModal: !prevState.isCreateUserModal,
    }));
    this.props.fetchAllCustomerUsers(this.state.customer_id).then(a => {
      if (a.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        const ns = cloneDeep(this.state);
        (ns.users as any) = a.response;
        this.setState(ns);
      }
    });
  };

  startNewCR = () => {
    const newstate = this.getEmptyState();
    newstate.currentPage.pageType = this.state.currentPage.pageType;
    this.setState(newstate);
  };

  renderFixedFee = () => {
    const types = this.props.qTypeList
      ? this.props.qTypeList.map(site => ({
          value: site.id,
          label: site.label,
        }))
      : [];

    return this.state.ChangeRequestProccessed === false ? (
      <div
        className={`cr-service-processing ${
          this.props.createtingCR ||
          this.props.createtingCR ||
          this.props.uploadingImages
            ? 'loading'
            : ''
        }`}
      >
        <Input
          field={{
            label:
              // tslint:disable-next-line:max-line-length
              'Enter Opportunity Name: * (CR- ProjectName - ChangeName)',
            type: "TEXT",
            value: `${this.state.name}`,
          }}
          error={this.state.error.name}
          width={6}
          name="name"
          placeholder="Enter Opportunity name"
          onChange={this.handleChange}
          disabled={this.state.cr_id !== '' ? true : false}
        />
        <div
          className="select-manufacturer
       field-section  col-md-6 col-xs-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Select Type
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${
              this.state.error.type_id.errorMessage ? `error-input` : ''
            }`}
          >
            <SelectInput
              name="type_id"
              value={this.state.type_id}
              onChange={this.handleChange}
              options={types}
              searchable={true}
              placeholder="Select type"
              clearable={false}
              disabled={this.state.cr_id !== '' ? true : false}
            />
          </div>
          {this.state.error.type_id.errorMessage && (
            <div className="field-error">
              {this.state.error.type_id.errorMessage}
            </div>
          )}
        </div>
        <div
          className="select-type select-row
      field-section col-xs-6 col-md-6"
        >
          <div className="field__label row">
            <label className="field__label-label" title="">
              Select Customer
            </label>
            <span className="field__label-required" />
          </div>
          <div
            className={`${
              this.state.error.customer_id.errorMessage ? `error-input` : ''
            }`}
          >
            <SelectInput
              name="customer_id"
              value={this.state.customer_id}
              onChange={e => this.handleChangeCustomer(e)}
              options={this.getCustomerOptions()}
              multi={false}
              searchable={true}
              placeholder="Select Customer"
              disabled={this.state.cr_id !== '' ? true : false}
            />
          </div>
          {this.state.error.customer_id.errorMessage && (
            <div className="field__error">
              {this.state.error.customer_id.errorMessage}
            </div>
          )}
        </div>
        <div
          className="select-type select-row
           opportunity add-new-option-section
      field-section col-xs-6 col-md-6"
        >
          <div className="add-new-option-box">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select User
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${
                this.state.error.user_id.errorMessage ? `error-input` : ''
              }`}
            >
              <SelectInput
                name="user_id"
                value={this.state.user_id}
                onChange={e => this.handleChangeUser(e)}
                options={this.getCustomerUserOptions()}
                multi={false}
                searchable={true}
                placeholder="Select User"
                disabled={
                  this.state.cr_id !== '' || !this.state.customer_id
                    ? true
                    : false
                }
              />
            </div>
            {this.state.error.user_id.errorMessage && (
              <div className="field__error">
                {this.state.error.user_id.errorMessage}
              </div>
            )}
          </div>

          <SquareButton
            content="+"
            onClick={e => this.toggleCreateUserModal()}
            className="add-new-option-sp"
            bsStyle={"primary"}
            title="Add New User"
            disabled={
              this.state.cr_id !== '' || !this.state.customer_id ? true : false
            }
          />
        </div>
        {this.state.currentPage.pageType === PageType.Fixed && (
          <>
            <Input
              field={{
                label: 'Revenue (in $)',
                type: "NUMBER",
                value: `${this.state.revenue}`,
              }}
              error={this.state.error.revenue}
              width={6}
              name="revenue"
              placeholder="Enter Revenue"
              onChange={this.handleChange}
              disabled={this.state.cr_id !== '' ? true : false}
            />
            <Input
              field={{
                label: 'Cost (in $)',
                type: "NUMBER",
                value: `${this.state.cost}`,
              }}
              error={this.state.error.cost}
              width={6}
              name="cost"
              placeholder="Enter Cost"
              onChange={this.handleChange}
              disabled={this.state.cr_id !== '' ? true : false}
            />
          </>
        )}
        {this.state.currentPage.pageType === PageType.TNM &&
          this.renderStaticFields()}{' '}
        <div
          className="upload-documents col-md-10 col-xs-10"
        >
          <label
            className="btn square-btn btn-primary
            file-button import-button add-new-document"
          >
            +
            <input
              type="file"
              name="files"
              accept=""
              onChange={this.handleFileSelect}
              onClick={(event: any)=>{
                event.target.value = ''
              }}
              multiple
            />
          </label>
          <label
            className="field__label-label
          add-new-document-label"
            title=""
          >
            Add Documents
          </label>

          {this.state.files && (
            <div className="file-name-list">
              {Array.from(this.state.files).map((file: any, i: number) => {
                return (
                  <div className="file-name" key={i}>
                    {i + 1}. {file && file.name}{' '}
                    <DeleteButton onClick={e => this.deleteFile(i)} />
                  </div>
                );
              })}
            </div>
          )}
          {this.state.error.files.errorMessage && (
            <div className="field__error">
              {this.state.error.files.errorMessage}
            </div>
          )}
        </div>
        <div className="erp__actions">
          {this.state.cr_id === '' && (
            <SquareButton
              content="Save"
              bsStyle={"primary"}
              onClick={this.submitConfirmOpen}
            />
          )}
          {this.state.cr_id !== '' && (
            <SquareButton
              content="Update Document"
              bsStyle={"primary"}
              onClick={this.updateImages}
            />
          )}
        </div>
      </div>
    ) : (
      <div className="cr c-r-proccessed">
        <div className="header">Change Request processed successfully</div>
        <div>
          {' '}
          Want to start with another Process Change?{' '}
          <SquareButton
            content="Click here"
            bsStyle={"default"}
            onClick={this.startNewCR}
          />{' '}
        </div>
      </div>
    );
  };

  handleChangeStatic = e => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = this.getValues(e);
    this.setState(newState);
  };

  getValues = e => {
    const value =
      e.target.type === 'number'
        ? parseFloat(e.target.value) > 0
          ? parseFloat(e.target.value)
          : 0
        : e.target.value;

    return value;
  };

  renderStaticFields = () => {
    return (
      <div className="service-proccesing col-md-12">
        <div className="section-heading">
          {'SERVICE COST '}
          <div className="action-collapse">{''}</div>
        </div>
        <div className="service-cost-body">
          <div className="estimated-hours-rates">
            <div className="header">
              <div className="sub-heading col-md-4">
                <div className="text">ESTIMATED HOURS</div>
                <div className="column-service-cost">
                  <Input
                    field={{
                      value: this.state.engineering_hours,
                      label: 'Engineering Hours (Estimate)',
                      type: "NUMBER",
                      isRequired: true,
                    }}
                    width={12}
                    name={'engineering_hours'}
                    placeholder="Enter Hours"
                    onChange={e => this.handleChangeStatic(e)}
                    className="select-type"
                    error={this.state.error.engineering_hours}
                  />
                  <Input
                    field={{
                      value: this.state.after_hours,
                      label: 'After Hours  (Sat, Sun, and M-F after 6pm)',
                      type: "NUMBER",
                      isRequired: false,
                    }}
                    width={12}
                    name={'after_hours'}
                    placeholder="Enter Hours"
                    onChange={e => this.handleChangeStatic(e)}
                    className="select-type"
                    error={this.state.error.after_hours}
                  />
                  <div className="project-mng-box col-md-12">
                    <Input
                      field={{
                        value: this.state.project_management_hours,
                        label: 'Project Managment Hours (Estimate)',
                        type: "NUMBER",
                        isRequired: true,
                      }}
                      width={12}
                      name={'project_management_hours'}
                      placeholder="Enter Hours"
                      onChange={e => this.handleChangeStatic(e)}
                      className="select-type"
                      error={this.state.error.project_management_hours}
                    />
                    <span className="info-recomended">
                      Recommended Hours:{' '}
                      {getRecommendedHours(this.state.engineering_hours)}
                    </span>
                  </div>
                </div>
              </div>

              <div className="sub-heading col-md-4">
                <div className="text">RATES</div>
                <div className="column-service-cost">
                  <Input
                    field={{
                      value: this.state.engineering_hourly_rate,
                      label: 'Engineering Hourly Rate(In $)',
                      type: "NUMBER",
                      isRequired: true,
                    }}
                    width={12}
                    name={'engineering_hourly_rate'}
                    placeholder="Enter Rate"
                    onChange={e => this.handleChangeStatic(e)}
                    className="select-type"
                    error={this.state.error.engineering_hourly_rate}
                  />
                  <Input
                    field={{
                      value: this.state.after_hours_rate,
                      label:
                        'After Hours Rate (Sat, Sun, and M-F after 6pm)(In $)',
                      type: "NUMBER",
                      isRequired: true,
                    }}
                    width={12}
                    name={'after_hours_rate'}
                    placeholder="Enter Rate"
                    onChange={e => this.handleChangeStatic(e)}
                    className="select-type"
                    error={this.state.error.after_hours_rate}
                  />
                  <Input
                    field={{
                      value: this.state.project_management_hourly_rate,
                      label: 'Project Management Hourly Rate(In $)',
                      type: "NUMBER",
                      isRequired: true,
                    }}
                    width={12}
                    name={'project_management_hourly_rate'}
                    placeholder="Enter Rate"
                    onChange={e => this.handleChangeStatic(e)}
                    className="select-type"
                    error={this.state.error.project_management_hourly_rate}
                  />
                </div>
              </div>
              <div className="sub-heading col-md-4">
                <div className="text">SERVICE COST (T&M)</div>
                <div className="column-service-cost">
                  <Input
                    field={{
                      value: getCustomerCostTM({
                        service_cost: {
                          engineering_hours: this.state.engineering_hours,
                          engineering_hourly_rate: this.state
                            .engineering_hourly_rate,
                          project_management_hours: this.state
                            .project_management_hours,
                          project_management_hourly_rate: this.state
                            .project_management_hourly_rate,
                          after_hours_rate: this.state.after_hours_rate,
                          after_hours: this.state.after_hours,
                        },
                      }),
                      label: 'Customer Cost(In $)',
                      type: "NUMBER",
                      isRequired: true,
                    }}
                    width={12}
                    disabled={true}
                    name={'customer_cost_t_and_m_fee'}
                    placeholder=" "
                    onChange={e => null}
                    className="select-type"
                    error={this.state.error.revenue}
                  />
                  <Input
                    field={{
                      value: getInternalCostTM(
                        {
                          service_cost: {
                            engineering_hours: this.state.engineering_hours,
                            project_management_hours: this.state
                              .project_management_hours,
                            after_hours: this.state.after_hours,
                          },
                        },
                        this.props.docSetting
                      ),
                      label: 'Internal Cost(In $)',
                      type: "NUMBER",
                      isRequired: true,
                    }}
                    width={12}
                    disabled={true}
                    name={'internal_cost_t_and_m_fee'}
                    placeholder=" "
                    onChange={e => null}
                    className="select-type"
                    error={this.state.error.cost}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="cr-service-processing-container tab">
        <h3>Service Processing</h3>
        <div className="loader">
          <Spinner
            show={
              this.props.createtingCR ||
              this.props.createtingCR ||
              this.props.uploadingImages
            }
          />
        </div>
        {this.renderTopBar()}
        {this.renderFixedFee()}
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.handleSubmit}
          isLoading={this.props.createtingCR}
          title="Do you want to process this service request?"
          okText="Yes"
          cancelText="No"
        />
        {this.state.isCreateUserModal && (
          <CustomerUserNew
            isVisible={this.state.isCreateUserModal}
            close={this.closeUserModal}
            customerId={this.state.customer_id}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
  qTypeList: state.sow.qTypeList,
  createtingCR: state.sow.createtingCR,
  isFetchingQTypeList: state.sow.isFetchingQTypeList,
  uploadingImages: state.sow.uploadingImages,
  docSetting: state.setting.docSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  CreateCR: (data: any) => dispatch(CreateCR(data)),
  fetchAllCustomerUsers: (id: number, params?: IServerPaginationParams) =>
    dispatch(fetchAllCustomerUsers(id)),
  uploadImagesForCR: (files: any) => dispatch(uploadImagesForCR(files)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeRequest);
