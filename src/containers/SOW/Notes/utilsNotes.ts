export const getTechnologyTypes = serviceType => {
  const technologyTypes =
    serviceType &&
    serviceType.reduce(
      (a, { technology_types }) => a.concat([], technology_types),
      []
    );
  const types =
    technologyTypes &&
    technologyTypes.map(c => ({
      value: c.id,
      label: `${c.name}`,
    }));
  return types;
};

export const getTechnologyTypesForQuiell = serviceType => {
  const technologyTypes =
    serviceType &&
    serviceType.reduce(
      (a, { technology_types }) => a.concat([], technology_types),
      []
    );
  const types =
    technologyTypes &&
    technologyTypes
    .filter(t => t.is_disabled !== true)
    .map(c => ({
      id: c.id,
      value: `${c.name}`,
    }));
  return types;
};

export const getTechnologyTypesForQuiellMension = serviceType => {
  const technologyTypes =
    serviceType &&
    serviceType.reduce(
      (a, { technology_types }) => a.concat([], technology_types),
      []
    );
  const types =
    technologyTypes &&
    technologyTypes.map(c => ({
      id: c.id,
      display: `${c.name}`,
    }));
  return types;
};

export const getValuesForSelect = (list, value, label) => {
  const types =
    list &&
    list.map(c => ({
      value: c[value],
      label: c[label],
    }));
  return types;
};

export const getValuesForQuill = (list, value, label) => {
  const types =
    list &&
    list.map(c => ({
      id: c[value],
      value: c[label],
    }));
  return types;
};
export const getValuesForMension = (list, value, label) => {
  const types =
    list &&
    list.map(c => ({
      id: c[value],
      display: c[label],
    }));
  return types;
};

export const getNotesTextFromList = NotesList => {
  let text = [];
  text =
    NotesList &&
    NotesList.results.map((notes: ISalesNote) => {
      return notes.body;
    });
  return text && text.join(' ');
};
