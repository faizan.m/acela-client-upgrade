import React, { Component } from 'react';
import SquareButton from '../../../components/Button/button';
import ModalBase from '../../../components/ModalBase/modalBase';
import './style.scss';
interface INotesProps {
  close: (e: any) => void;
  Notes: INotes;
  isVisible: boolean;
}

export default class NotesDetails extends Component<
  INotesProps
> {
  constructor(props: INotesProps) {
    super(props);
  }

  renderBody = () => {
    const {
      name,
      notes,
    } = this.props.Notes;

    return (
      <div className="detail__body">
        <div className="body">
          <div className="field">
            <p className="heading">NAME</p>
            <p>{`${name}`}</p>
          </div>{' '}
                 <div className="field">
            <p className="heading">Note</p>
            <p>{notes ? `${notes}` : `N/A`}</p>
          </div>
        </div>
        <div />
      </div>
    );
  };

  renderFooter = () => {
    return (
      <SquareButton
        content="Close"
        bsStyle={"primary"}
        onClick={this.props.close}
      />
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Service Catalog Details"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="notes-detail-container"
      />
    );
  }
}
