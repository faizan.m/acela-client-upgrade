import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  POST_DOCUMENT_FAILURE,
  POST_DOCUMENT_SUCCESS,
  postDocument,
  postDocumentPU,
} from '../../actions/serviceRequest';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';

interface IDetailUploadProps extends ICommonProps {
  close?: (e: any) => void;
  isVisible: boolean;
  ticketId: number;
  postDocument?: TPostDocument;
  user: ISuperUser;
  postDocumentPU?: any;
}

interface IDetailUploadState {
  dataFile: any;
  isDocumentPosting: boolean;
  error: string;
}

class DetailUpload extends Component<IDetailUploadProps, IDetailUploadState> {
  constructor(props: IDetailUploadProps) {
    super(props);
    this.state = {
      dataFile: '',
      error: '',
      isDocumentPosting: false,
    };
  }

  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile = files[0];
    this.setState({ dataFile });
  };

  onSubmit = () => {
    if (this.state.dataFile) {
      this.setState({ isDocumentPosting: true });
      const data = new FormData();
      data.append('title', this.state.dataFile.name);
      data.append('file', this.state.dataFile);

      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === 'customer'
      ) {
        this.props.postDocument(this.props.ticketId, data).then(action => {
          if (action.type === POST_DOCUMENT_SUCCESS) {
            this.setState({
              dataFile: null,
              isDocumentPosting: false,
              error: '',
            });
            this.props.close(null);
          }
          if (action.type === POST_DOCUMENT_FAILURE) {
            this.setState({
              isDocumentPosting: false,
              error:
                action.errorList.data.file.length > 0 &&
                action.errorList.data.file[0],
            });
          }
        });
      } else {
        const {
          match: { params },
        } = this.props;
        this.props
          .postDocumentPU(params.customerId, this.props.ticketId, data)
          .then(action => {
            if (action.type === POST_DOCUMENT_SUCCESS) {
              this.setState({
                dataFile: null,
                isDocumentPosting: false,
                error: '',
              });
              this.props.close(null);
            }
            if (action.type === POST_DOCUMENT_FAILURE) {
              this.setState({
                isDocumentPosting: false,
                error: action.errorList.data.file
                  ? action.errorList.data.file[0]
                  : action.errorList.data.detail,
              });
            }
          });
      }
    }
  };

  renderBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.state.isDocumentPosting} />
        </div>
        <div className={`${this.state.isDocumentPosting ? `loading` : ''}`}>
          <label className="btn btn-primary  square-btn file-button">
            <img
              className="attachments__icon__upload"
              src="/assets/icons/cloud.png"
            />
            Upload Data File
            <input type="file" onChange={this.handleFile} />
          </label>
          {this.state.dataFile && <p>{this.state.dataFile.name}</p>}
        </div>
        {this.state.error !== '' && (
          <div className="file-type-error">{this.state.error}</div>
        )}
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div className={`${this.state.isDocumentPosting ? `loading` : ''}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.props.close}
        />
        <SquareButton
          content="Submit"
          bsStyle={"primary"}
          onClick={this.onSubmit}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Upload Attachment"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="service-upload-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  postDocument: (ticketId: number, file: any) =>
    dispatch(postDocument(ticketId, file)),
  postDocumentPU: (customerId: number, ticketId: number, file: any) =>
    dispatch(postDocumentPU(customerId, ticketId, file)),
});

export default connect<any, any>(
  null,
  mapDispatchToProps
)(DetailUpload);
