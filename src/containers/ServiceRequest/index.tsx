import React from 'react';
import { connect } from 'react-redux';

import {
  closeTicket,
  getTickets,
  getTicketsPU,
  setIsCompletedTickets,
  updateTicket,
  updateTicketPU,
} from '../../actions/serviceRequest';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { fromISOStringToFormattedDate } from '../../utils/CalendarUtil';
import ServiceNew from './new';

import './style.scss';

interface IServiceRequestProps extends ICommonProps {
  requests: IServiceTicket[];
  getTickets: TFetchTickets;
  getTicketsPU: any;
  isTicketsFetching: boolean;
  prevLocation: string;
  setIsCompletedTickets: any;
  completedTickets: boolean;
  customerId: number;
  user: ISuperUser;
  updateTicket: any;
  updateTicketPU: any;
  isNotePosting: boolean;
  closeTicket: any;
}

interface IServiceRequestState {
  search: string;
  isCompletedVisible: boolean;
  isNewVisible: boolean;
}

class ServiceRequest extends React.Component<
  IServiceRequestProps,
  IServiceRequestState
> {
  constructor(props: IServiceRequestProps) {
    super(props);
    this.state = {
      search: '',
      isCompletedVisible: false,
      isNewVisible: false,
    };
  }

  componentDidMount() {
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.getTickets(this.props.completedTickets);
      this.props.setIsCompletedTickets(this.props.completedTickets);
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'provider' &&
      this.props.customerId
    ) {
      this.props.getTicketsPU(
        this.props.customerId,
        this.props.completedTickets
      );
      this.props.setIsCompletedTickets(this.props.completedTickets);
    }
  }

  componentDidUpdate(prevProps: IServiceRequestProps) {
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.getTicketsPU(this.props.customerId, this.props.completedTickets);
    }

    if (this.props.user && this.props.user !== prevProps.user) {
      if (this.props.user.type && this.props.user.type === 'customer') {
        this.props.getTickets(this.props.completedTickets);
        this.props.setIsCompletedTickets(this.props.completedTickets);
      }
    }
  }
  
  handleSearchInput = e => {
    this.setState({ search: e.target.value });
  };

  toggleCompleted = () => {
    this.setState(
      { isCompletedVisible: !this.state.isCompletedVisible },
      () => {
        if (
          this.props.user &&
          this.props.user.type &&
          this.props.user.type === 'customer'
        ) {
          this.props.getTickets(!this.props.completedTickets);
        } else {
          this.props.getTicketsPU(
            this.props.customerId,
            !this.props.completedTickets
          );
        }
        this.props.setIsCompletedTickets(!this.props.completedTickets);
      }
    );
  };

  renderTopBar = () => {
    const btnText = `${
      this.props.completedTickets === true ? `Hide` : `Show`
    } Completed Requests`;

    return (
      <div
        className={
          this.props.customerId ||
          (this.props.user &&
            this.props.user.type &&
            this.props.user.type === 'customer')
            ? 'request__actions  header-panel'
            : 'request__actions   header-panel disable-div'
        }
      >
        <div className="search">
          <Input
            field={{
              label: '',
              type: "TEXT",
              isRequired: false,
              value: `${this.state.search}`,
            }}
            width={8}
            name="search"
            placeholder="Search"
            onChange={this.handleSearchInput}
          />
        </div>
        <SquareButton
          content={btnText}
          className="btn__toggle1"
          onClick={this.toggleCompleted}
        />
        <SquareButton
          content="&#43; Create Service Request"
          bsStyle={"primary"}
          className="btn__add"
          onClick={this.toggleModal}
        />
      </div>
    );
  };

  onRowClick = rowInfo => {
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.history.push(`/service-requests/${rowInfo.original.id}`);
    } else {
      this.props.history.push(
        `/service-requests-provider/${this.props.customerId}/${
          rowInfo.original.id
        }`
      );
    }
  };

  filterRows = req => {
    const searchValue = this.state.search.toLowerCase();
    if (
      (req.name &&
        req.name
          .toString()
          .toLowerCase()
          .includes(searchValue)) ||
      (req.id &&
        req.id
          .toString()
          .toLowerCase()
          .includes(searchValue)) ||
      (req.update &&
        req.update
          .toString()
          .toLowerCase()
          .includes(searchValue))
    ) {
      return req;
    }
  };

  toggleModal = () => {
    this.setState({ isNewVisible: !this.state.isNewVisible });
  };

  closeModal = () => {
    this.setState({ isNewVisible: false });
  };

  afterSave = () => {
    this.setState({ isNewVisible: false });
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.getTickets(this.props.completedTickets);
    } else {
      this.props.getTicketsPU(
        this.props.customerId,
        this.props.completedTickets
      );
    }
  };

  render() {
    const columns: any = [
      {
        accessor: 'id',
        Header: 'Request',
        width: 150,
        Cell: id => <div className="pl-15">{id.value}</div>,
      },
      {
        accessor: 'summary',
        Header: 'Summary',
        width: 200,
      },
      {
        accessor: 'update',
        Header: 'Last Update',
        Cell: update => (
          <div className="cell__update">{`${
            update.value && update.value.length > 50
              ? update.value.substring(0, 50) + '...'
              : update.value
                ? update.value
                : 'No updates'
          }`}</div>
        ),
      },
      {
        accessor: 'created_on',
        Header: 'Created',
        width: 110,
        Cell: created => (
          <div>{fromISOStringToFormattedDate(created.value)}</div>
        ),
      },
      {
        accessor: 'last_updated_on',
        Header: 'Last Modified',
        width: 110,
        Cell: modified => (
          <div>{fromISOStringToFormattedDate(modified.value)}</div>
        ),
      },
      {
        accessor: 'status',
        Header: 'Status',
        Cell: Status => (
          <div
            className={`status__service-request ${Status.value
              .toLowerCase()
              .split(' ')
              .join('')}`}
          >
            {Status.value}
          </div>
        ),
      },
    ];
    const rows = this.props.requests.filter(this.filterRows);

    return (
      <div className="service-request-container">
        <h3>Service Request&#40;s&#41;</h3>
        <div className="loader">
          <Spinner
            show={this.props.isTicketsFetching || this.props.isNotePosting}
          />
        </div>
        <Table
          columns={columns}
          rows={rows}
          customTopBar={this.renderTopBar()}
          onRowClick={this.onRowClick}
          className={`${this.props.isTicketsFetching ? 'loading' : ''}`}
          loading={this.props.isTicketsFetching}
        />
        {this.state.isNewVisible && (
          <ServiceNew
            isVisible={this.state.isNewVisible}
            close={this.closeModal}
            save={this.afterSave}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  requests: state.serviceRequest.requests,
  isTicketsFetching: state.serviceRequest.isTicketsFetching,
  prevLocation: state.appState.prevLocation,
  completedTickets: state.serviceRequest.completedTickets,
  customerId: state.customer.customerId,
  user: state.profile.user,
  isNotePosting: state.serviceRequest.isNotePosting,
});

const mapDispatchToProps = (dispatch: any) => ({
  closeTicket: (id: string) => dispatch(closeTicket(id)),
  getTickets: (closed?: boolean) => dispatch(getTickets(closed)),
  setIsCompletedTickets: (is: boolean) => dispatch(setIsCompletedTickets(is)),
  getTicketsPU: (customerId: number, closed?: boolean) =>
    dispatch(getTicketsPU(customerId, closed)),
  updateTicket: (id: number) => dispatch(updateTicket(id)),
  updateTicketPU: (customerId: number, id: number) =>
    dispatch(updateTicketPU(customerId, id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceRequest);
