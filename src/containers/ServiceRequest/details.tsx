import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";

import {
  getServiceTicketStatusList,
  getTicket,
  getTicketPU,
  POST_NOTE_FAILURE,
  sendNote,
  sendNotePU,
  UPDATE_TICKET_FAILURE,
  UPDATE_TICKET_SUCCESS,
  updateTicket,
  updateTicketPU,
} from "../../actions/serviceRequest";
import BackButton from "../../components/BackLink";
import SquareButton from "../../components/Button/button";
import Comment from "../../components/Comment";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import Downloads from "./download";
import UploadDetailsFile from "./uploadDetailsFile";
import { fetchProviderProfile } from "../../actions/provider";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";
import { fromISOStringToDateTimeString } from "../../utils/CalendarUtil";
import "./style.scss";
import { getUserProfile } from "../../utils/AuthUtil";

interface IServiceDetailProps extends ICommonProps {
  user: ISuperUser;
  statusList: any[];
  isNotePosting: boolean;
  isTicketFetching: boolean;
  ticket: IServiceTicket;
  sendNote: TPostNote;
  getTicket: TFetchTicket;
  fetchProviderProfile?: () => Promise<any>;
  updateTicket: (id: number) => Promise<any>;
  getServiceTicketStatusList: () => Promise<any>;
  getTicketPU: (customerId: number, id: number) => Promise<any>;
  updateTicketPU: (customerId: number, id: number) => Promise<any>;
  sendNotePU: (
    customerId: number,
    id: number,
    text: string,
    internal: boolean
  ) => Promise<any>;
}

interface IServiceDetailState {
  comment: string;
  commentType: NoteType;
  errorMessage: string;
  created_by: string;
  error: {
    comment: IFieldValidation;
    created_by: IFieldValidation;
  };
  isUploadVisible: boolean;
  errorOnClose: string;
  status: number;
  closeTicket: boolean;
}

type NoteType = "Discussion" | "Internal";
class ServiceDetails extends React.Component<
  IServiceDetailProps,
  IServiceDetailState
> {
  userType: USER_TYPE = getUserProfile().scopes.type;

  constructor(props: IServiceDetailProps) {
    super(props);
    this.state = {
      comment: "",
      created_by: "",
      commentType: "Internal",
      error: {
        comment: {
          errorState: "success",
          errorMessage: "",
        },
        created_by: {
          errorState: "success",
          errorMessage: "",
        },
      },
      isUploadVisible: false,
      errorMessage: "",
      errorOnClose: "",
      status: 0,
      closeTicket: false,
    };
  }

  componentDidMount() {
    this.getLatest();
    if (this.props.statusList.length === 0) {
      this.props.getServiceTicketStatusList();
    }
  }

  getLatest = () => {
    const {
      match: { params },
    } = this.props;

    if (this.userType === "PROVIDER" && params.requestId) {
      this.props.getTicketPU(params.customerId, params.requestId);
    } else {
      this.props.getTicket(params.requestId);
    }
  };

  updateTicketStatus = () => {
    const {
      match: { params },
    } = this.props;
    this.setState({
      errorOnClose: "",
    });
    if (this.userType === "PROVIDER" && params.requestId) {
      this.props
        .updateTicketPU(params.customerId, params.requestId)
        .then((action) => {
          if (action.type === UPDATE_TICKET_SUCCESS) {
            this.props.getTicketPU(params.customerId, params.requestId);
          }
          if (action.type === UPDATE_TICKET_FAILURE) {
            this.setState({
              errorOnClose: action.errorList && action.errorList.data.detail,
            });
          }
        });
    }

    if (
      this.userType === "CUSTOMER" &&
      params.requestId &&
      params.requestId
    ) {
      this.props.updateTicket(params.requestId).then((action) => {
        if (action.type === UPDATE_TICKET_SUCCESS) {
          this.props.getTicket(params.requestId);
        }
        if (action.type === UPDATE_TICKET_FAILURE) {
          this.setState({
            errorOnClose: action.errorList && action.errorList.data.detail,
          });
        }
      });
    }
    this.toggleConfirmOpen();
  };

  handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = { ...this.state };
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  submitComment = () => {
    if (this.validateForm()) {
      if (this.userType === "CUSTOMER") {
        this.props
          .sendNote(this.props.ticket.id, this.state.comment)
          .then((action) => {
            if (action.type === POST_NOTE_FAILURE) {
              this.setState({
                errorMessage: action.errorList.data[0],
              });
            } else {
              this.getLatest();
            }
          });
      } else {
        const {
          match: { params },
        } = this.props;
        this.props
          .sendNotePU(
            params.customerId,
            this.props.ticket.id,
            this.state.comment,
            this.state.commentType === "Internal"
          )
          .then((action) => {
            if (action.type === POST_NOTE_FAILURE) {
              this.setState({
                errorMessage: action.errorList.data[0],
              });
            } else {
              this.getLatest();
            }
          });
      }

      this.setState({ comment: "" });
    }
  };

  validateForm() {
    const newState: IServiceDetailState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.comment) {
      newState.error.comment.errorState = "error";
      newState.error.comment.errorMessage = "Comment cannot be empty";
      isValid = false;
    }
    this.setState(newState);

    return isValid;
  }

  openUpload = () => {
    this.setState({ isUploadVisible: true });
  };

  closeUpload = () => {
    this.getLatest();
    this.setState({ isUploadVisible: false });
  };

  toggleConfirmOpen = () => {
    this.setState({
      closeTicket: false,
    });
  };

  onClickConfirm = () => {
    this.setState({
      closeTicket: true,
    });
  };

  render() {
    const { ticket } = this.props;

    if (ticket) {
      return (
        <div
          className={`service-details-container ${
            this.props.isTicketFetching ? "loading" : ""
          }`}
        >
          <div className="loader">
            <Spinner
              show={this.props.isTicketFetching || this.props.isNotePosting}
            />
          </div>
          <BackButton path="/service-requests" />
          {this.state.errorOnClose && (
            <div className="error-on-close"> {this.state.errorOnClose}</div>
          )}
          <div className="details">
            <div className="details__header">
              <div className="left">
                <h4>
                  Service Request&nbsp;&nbsp;&nbsp;&#x23;
                  {ticket.id}
                </h4>
                <p>
                  (Last Modified on{" "}
                  {fromISOStringToDateTimeString(ticket.last_updated_on)})
                </p>
              </div>
              {ticket.can_close === true && (
                <SquareButton
                  content="Close Ticket"
                  bsStyle={"primary"}
                  onClick={this.onClickConfirm}
                  className="close-ticket col-md-3"
                  disabled={this.props.isNotePosting}
                />
              )}
              <ConfirmBox
                show={this.state.closeTicket}
                onClose={this.toggleConfirmOpen}
                onSubmit={this.updateTicketStatus}
                isLoading={false}
                title="Are you sure, want to close ticket ?"
              />
            </div>
            <div className="details__body">
              <h5>Summary</h5>
              <p>{ticket.summary}</p>
              <div className="inline">
                <div>
                  <h5>Status</h5>
                  <div>
                    <p>{ticket.status}</p>
                  </div>
                </div>
                <div className="created">
                  <h5>Created On</h5>
                  <p>{fromISOStringToDateTimeString(ticket.created_on)}</p>
                </div>
                <div className="resources">
                  <h5>Requestor</h5>
                  <p>{ticket.resource}</p>
                </div>
                <div className="resources">
                  <h5>Service Request Owner</h5>
                  <p>{ticket.owner ? ticket.owner : "Unassigned"}</p>
                </div>
                <div className="resources">
                  <h5>Priority</h5>
                  <p>{ticket.priority ? ticket.priority : "Unassigned"}</p>
                </div>
              </div>
            </div>
          </div>
          <div className="details details-Description">
            <div className="details__header">
              <h4>Description</h4>
            </div>
            <div className="details__body">
              <p className="text-description">{ticket.description}</p>
            </div>
          </div>
          <Downloads openUpload={this.openUpload} {...this.props} />
          <div className={`details__footer`}>
            <div className="comment__loader">
              <Spinner show={this.props.isNotePosting} />
            </div>
            <div className={`${this.props.isNotePosting ? "loading" : ""}`}>
              <div className="footer__header">
                <h5>Comments</h5>
              </div>
              <div className="footer__body">
                <div className="comment-edit-section">
                  <Input
                    field={{
                      label: "Enter Comment",
                      type: "TEXTAREA",
                      isRequired: true,
                      value: `${this.state.comment}`,
                    }}
                    error={this.state.error.comment}
                    width={12}
                    name="comment"
                    placeholder="Enter Comment"
                    onChange={this.handleInput}
                  />
                  {this.state.errorMessage && (
                    <div className="error-message">
                      {this.state.errorMessage}
                    </div>
                  )}
                  {this.userType === "PROVIDER" && (
                    <Input
                      field={{
                        options: ["Internal", "Discussion"],
                        label: "Comment Type",
                        type: "RADIO",
                        value: this.state.commentType,
                      }}
                      name={`commentType`}
                      className="comment-type-radio"
                      width={6}
                      onChange={this.handleInput}
                    />
                  )}
                  <SquareButton
                    content="Send"
                    bsStyle={"primary"}
                    onClick={this.submitComment}
                    className="footer__action"
                    disabled={this.props.isNotePosting}
                  />
                </div>
                <div className="comments-list-container">
                  {ticket.notes.map((note, i) => {
                    return (
                      <Comment
                        key={i}
                        comment={note}
                        showType={this.userType === "PROVIDER"}
                      />
                    );
                  })}
                </div>
              </div>
            </div>
          </div>
          <UploadDetailsFile
            isVisible={this.state.isUploadVisible}
            close={this.closeUpload}
            ticketId={this.props.ticket.id}
            user={this.props.user}
            {...this.props}
          />
        </div>
      );
    } else {
      return (
        <div className="loader">
          <Spinner show={this.props.isTicketFetching} />
        </div>
      );
    }
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  ticket: state.serviceRequest.requestDetail,
  user: state.profile.user,
  isTicketFetching: state.serviceRequest.isTicketFetching,
  isNotePosting: state.serviceRequest.isNotePosting,
  statusList: state.serviceRequest.statusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  updateTicket: (id: number) => dispatch(updateTicket(id)),
  updateTicketPU: (customerId: number, id: number) =>
    dispatch(updateTicketPU(customerId, id)),
  getServiceTicketStatusList: () => dispatch(getServiceTicketStatusList()),
  getTicket: (id: number) => dispatch(getTicket(id)),
  sendNote: (id: number, text: string) => dispatch(sendNote(id, text)),
  getTicketPU: (customerId: number, id: number) =>
    dispatch(getTicketPU(customerId, id)),
  sendNotePU: (
    customerId: number,
    id: number,
    text: string,
    internal: boolean
  ) => dispatch(sendNotePU(customerId, id, text, internal)),
  fetchProviderProfile: () => dispatch(fetchProviderProfile()),
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(ServiceDetails);
