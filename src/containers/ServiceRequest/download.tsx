import React from 'react';
import { connect } from 'react-redux';

import {
  FETCH_DOCUMENT_FAILURE,
  FETCH_DOCUMENT_SUCCESS,
  getDocument,
  getDocumentPU,
} from '../../actions/serviceRequest';

import IconButton from '../../components/Button/iconButton';
import Table from '../../components/Table/table';
import { saveBlob } from '../../utils/download';

interface IDownloadProps extends ICommonProps {
  ticket?: IServiceTicket;
  getDocument?: TFetchDocument;
  getDocumentPU?: any;
  openUpload: any;
  user?: ISuperUser;
}

interface IDownlaodState {
  downloadingIds: number[];
}

class Downloads extends React.Component<IDownloadProps, IDownlaodState> {
  constructor(props: IDownloadProps) {
    super(props);
    this.state = {
      downloadingIds: [],
    };
  }

  getStream = (document: ITicketDocument) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, document.id],
    });

    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.getDocument(this.props.ticket.id, document.id).then(action => {
        if (action.type === FETCH_DOCUMENT_SUCCESS) {
          saveBlob(document.file_name, action.response);
          this.setState({
            downloadingIds: this.state.downloadingIds.filter(
              id => document.id !== id
            ),
          });
        }
        if (action.type === FETCH_DOCUMENT_FAILURE) {
          this.setState({
            downloadingIds: this.state.downloadingIds.filter(
              id => document.id !== id
            ),
          });
        }
      });
    } else {
      const {
        match: { params },
      } = this.props;
      this.props
        .getDocumentPU(params.customerId, this.props.ticket.id, document.id)
        .then(action => {
          if (action.type === FETCH_DOCUMENT_SUCCESS) {
            saveBlob(document.file_name, action.response);
            this.setState({
              downloadingIds: this.state.downloadingIds.filter(
                id => document.id !== id
              ),
            });
          }
          if (action.type === FETCH_DOCUMENT_FAILURE) {
            this.setState({
              downloadingIds: this.state.downloadingIds.filter(
                id => document.id !== id
              ),
            });
          }
        });
    }
  };

  render() {
    const columns: any = [
      {
        accessor: 'title',
        Header: 'Title',
        Cell: cell => <div className="pl-15">{cell.value}</div>,
      },
      {
        accessor: 'file_name',
        Header: 'Request',
      },
      {
        accessor: 'id',
        Header: 'Download',
        width: 100,
        sortable: false,
        Cell: cell => <div>{getLoadingMarkUp(cell)}</div>,
      },
    ];

    const getLoadingMarkUp = cell => {
      if (this.state.downloadingIds.includes(cell.value)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <IconButton
            icon="download.png"
            onClick={() => {
              this.getStream(cell.original);
            }}
          />
        );
      }
    };

    let rows: ITicketDocument[] = [];
    if (this.props.ticket.documents.length > 0) {
      rows = this.props.ticket.documents;
    }

    return (
      <div className="attachment-details">
        <div className="attachment-header">
          <h5>Attachments</h5>
          <label
            className="btn btn-primary square-btn file-button"
            onClick={this.props.openUpload}
          >
            <img
              className="attachments__icon__upload"
              src="/assets/icons/cloud.png"
            />
            Upload Attachment
          </label>
        </div>
        <div className="attachment-body">
          <Table columns={columns} rows={rows} />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  ticket: state.serviceRequest.requestDetail,
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getDocument: (ticketId: number, documentId: number) =>
    dispatch(getDocument(ticketId, documentId)),
  getDocumentPU: (customerId: number, ticketId: number, documentId: number) =>
    dispatch(getDocumentPU(customerId, ticketId, documentId)),
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(Downloads);
