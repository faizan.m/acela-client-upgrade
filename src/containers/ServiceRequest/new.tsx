import { cloneDeep } from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";

import {
  addTicket,
  addTicketPU,
  getContacts,
  getContactsPU,
  getNote,
  getNotePU,
  POST_TICKET_FAILURE,
  POST_TICKET_SUCCESS,
  postDocument,
  postDocumentPU,
} from "../../actions/serviceRequest";

import { addErrorMessage } from "../../actions/appState";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import SelectInput from "../../components/Input/Select/select";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import NewUploadFile from "./newUploadFile";

interface IServiceNewProps {
  close: (e: any) => void;
  save: (e: any) => void;
  isVisible: boolean;
  addTicket?: TPostTicket;
  getContacts?: TFetchContacts;
  contacts?: Array<{ name: string; id: string }>;
  postDocument?: TPostDocument;
  user?: ISuperUser;
  getNote?: TFetchNote;
  note?: string;
  addTicketPU?: any;
  getContactsPU?: any;
  postDocumentPU?: any;
  getNotePU?: any;
  customerId?: number;
  addErrorMessage?: any;
}

interface IServiceNewState {
  summary: string;
  description: string;
  attachments: any[];
  isFormValid: boolean;
  error: {
    summary: IFieldValidation;
    description: IFieldValidation;
    created_by: IFieldValidation;
  };
  resource: string;
  isUploadVisible: boolean;
  uploadFile: null;
  isPostingTicket: boolean;
  dataFile: any;
  created_by: string;
  errorMessage: string;
}

class ServiceNew extends Component<IServiceNewProps, IServiceNewState> {
  resources = [{ value: "", label: "" }];
  constructor(props: IServiceNewProps) {
    super(props);
    this.state = {
      summary: "",
      description: "",
      created_by: "",
      isFormValid: false,
      attachments: [],
      error: {
        summary: {
          errorState: "success",
          errorMessage: "",
        },
        description: {
          errorState: "success",
          errorMessage: "",
        },
        created_by: {
          errorState: "success",
          errorMessage: "",
        },
      },
      resource: this.props.user.id,
      isUploadVisible: false,
      uploadFile: null,
      isPostingTicket: false,
      dataFile: "",
      errorMessage: "",
    };
  }

  componentDidMount() {
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === "customer"
    ) {
      this.props.getContacts();
      this.props.getNote();
    } else {
      this.props.getContactsPU(this.props.customerId);
      this.props.getNotePU(this.props.customerId);
    }
  }

  handleChange = (e: any): void => {
    const newState = { ...this.state };
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  submitRequest = () => {
    const { summary, description, resource } = this.state;

    if (this.validateForm()) {
      this.setState({ isPostingTicket: true });
      const newRequest: IServiceTicketNew = {
        summary,
        description,
      };
      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === "customer"
      ) {
        newRequest.primary_contact = resource ? resource : this.props.user.id;
        this.props.addTicket(newRequest).then((action) => {
          if (action.type === POST_TICKET_SUCCESS) {
            this.setState({ isPostingTicket: false });
            if (this.state.uploadFile) {
              this.props.postDocument(
                action.response.id,
                this.state.uploadFile
              );
            }
            this.props.save(null);
          }
          if (action.type === POST_TICKET_FAILURE) {
            this.setState({
              errorMessage: action.errorList.data[0],
              isPostingTicket: false,
            });
          }
        });
      } else {
        newRequest.primary_contact = resource ? resource : this.props.user.id;
        this.props
          .addTicketPU(this.props.customerId, newRequest)
          .then((action) => {
            if (action.type === POST_TICKET_SUCCESS) {
              this.setState({ isPostingTicket: false });
              if (this.state.uploadFile) {
                this.props.postDocumentPU(
                  this.props.customerId,
                  action.response.id,
                  this.state.uploadFile
                );
              }
              this.props.save(null);
            }
            if (action.type === POST_TICKET_FAILURE) {
              this.setState({
                errorMessage: action.errorList.data[0],
                isPostingTicket: false,
              });
            }
          });
      }
    }
  };

  validateForm() {
    const newState: IServiceNewState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.summary || this.state.summary.trim().length === 0) {
      newState.error.summary.errorState = "error";
      newState.error.summary.errorMessage = "Summary cannot be empty";
      isValid = false;
    } else if (this.state.summary.length > 100) {
      newState.error.summary.errorState = "error";
      newState.error.summary.errorMessage =
        "Summary should be less than 100 chars.";
      isValid = false;
    }

    if (!this.state.description || this.state.description.trim().length === 0) {
      newState.error.description.errorState = "error";
      newState.error.description.errorMessage = "Description cannot be empty";
      isValid = false;
    }

    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  }

  createContactList = () => {
    this.resources = this.props.contacts.map((c) => {
      return { label: c.name, value: c.id };
    });
  };

  closeUpload = () => {
    this.setState({ isUploadVisible: false });
  };

  handleFileData = (uploadFile) => {
    this.setState({ uploadFile, isUploadVisible: false });
  };

  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile: any = files[0];
    this.setState({ dataFile });
    const data = new FormData();
    data.append("title", dataFile.name);
    data.append("file", dataFile);
    this.handleFileData(data);
  };

  renderBody = () => {
    return (
      <div className="new__body">
        <div className="loader modal-loader">
          <Spinner show={this.state.isPostingTicket} />
        </div>
        <div className={`${this.state.isPostingTicket ? `loading` : ""}`}>
          <div className="top-row">
          <p className="text__note">
            {this.props.note ? `Note: ${this.props.note}` : ``}
          </p>
          </div>
          <Input
            field={{
              label: "Summary ",
              type: "TEXT",
              isRequired: true,
              value: `${this.state.summary}`,
            }}
            error={this.state.error.summary}
            width={100}
            name="summary"
            placeholder="Enter Summary"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: "Description",
              type: "TEXTAREA",
              isRequired: true,
              value: `${this.state.description}`,
            }}
            error={this.state.error.description}
            width={100}
            name="description"
            placeholder="Enter Description"
            onChange={this.handleChange}
          />
          <div className="attachment__wrap">
            <label className="btn btn-primary square-btn file-button">
              <img
                className="attachments__icon__upload"
                src="/assets/icons/cloud.png"
              />
              Upload Data File
              <input type="file" onChange={this.handleFile} />
            </label>
            {this.state.dataFile && <p>{this.state.dataFile.name}</p>}
          </div>

          <div className="new__select">
            <p>Primary Contact</p>
            <SelectInput
              name="resource"
              value={this.state.resource}
              options={this.resources}
              onChange={this.handleChange}
              searchable={true}
            />
          </div>
          {this.state.errorMessage && (
            <div className="error-message">{this.state.errorMessage}</div>
          )}

          <NewUploadFile
            isVisible={this.state.isUploadVisible}
            close={this.closeUpload}
            onSubmit={this.handleFileData}
          />
        </div>
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div
        className={`
            ${this.state.isPostingTicket ? `loading` : ""}`}
      >
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.props.close}
        />
        <SquareButton
          content="Create"
          bsStyle={"primary"}
          onClick={this.submitRequest}
        />
      </div>
    );
  };

  render() {
    if (this.props.contacts) {
      this.createContactList();
    }

    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Create Service Request"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="service-new-container"
      />
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  contacts: state.serviceRequest.contactList,
  user: state.profile.user,
  note: state.serviceRequest.note,
  customerId: state.customer.customerId,
});

const mapDispatchToProps = (dispatch: any) => ({
  addTicket: (ticket: IServiceTicketNew) => dispatch(addTicket(ticket)),
  getContacts: () => dispatch(getContacts()),
  postDocument: (ticketId: number, file: any) =>
    dispatch(postDocument(ticketId, file)),
  getNote: () => dispatch(getNote()),
  addTicketPU: (customerId: number, ticket: IServiceTicketNew) =>
    dispatch(addTicketPU(customerId, ticket)),
  getContactsPU: (customerId: number) => dispatch(getContactsPU(customerId)),
  postDocumentPU: (customerId: number, ticketId: number, file: any) =>
    dispatch(postDocumentPU(customerId, ticketId, file)),
  getNotePU: (customerId: number) => dispatch(getNotePU(customerId)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(ServiceNew);
