import React from 'react';

import SquareButton from '../../components/Button/button';
import Select from '../../components/Input/Select/select';
import ModalBase from '../../components/ModalBase/modalBase';

import './style.scss';

interface IAddDeviceFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (filters: ISubscriptionFilters) => void;
  prevFilters: ISubscriptionFilters;
  productMappings: any[];
  contractStatuses: IContractStatus[];
  manufacturers?: any;
  statusList?: any[];
}

interface IAddDeviceFormState {
  filters: ISubscriptionFilters;
}

export default class AddDeviceForm extends React.Component<
  IAddDeviceFormProps,
  IAddDeviceFormState
> {
  constructor(props: IAddDeviceFormProps) {
    super(props);

    this.state = {
      filters: {
        productMappings: [],
        status: [],
        contractStatus: [],
        manufacturer: [],
      },
    };
  }

  componentDidUpdate(prevProps: IAddDeviceFormProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
      });
    }
  }

  onClose = e => {
    this.props.onClose(e);
  };

  onSubmit = e => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return 'Filters';
  };
  getAllTechnoType = () => {
    const types1 = [];

    if (this.props.productMappings) {
      this.props.productMappings.map(t =>
        types1.push({
          value: t.id,
          label: `${t.name}`,
          // tslint:disable-next-line: prettier
          disabled: false,
        })
      );
    }

    return types1;
  };
  getBody = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((el) => ({
          value: el.id,
          label: el.label,
        }))
      : [];

    const contractStatuses = this.props.contractStatuses
      ? this.props.contractStatuses.map(contractStatus => ({
          value: contractStatus.contract_status_id,
          label:
            contractStatus.contract_status === 'N.A.'
              ? 'Not Covered'
              : contractStatus.contract_status,
        }))
      : [];

    const statusList = this.props.statusList
      ? this.props.statusList.map(s => ({
          value: s.id,
          label: s.description,
        }))
      : [];
    const filters = this.state.filters;

    return (
      <div className="filters-modal__body col-md-12">
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Contract Status</label>
            <div className="field__input">
              <Select
                name="contractStatus"
                value={filters.contractStatus}
                onChange={this.onFilterChange}
                options={contractStatuses}
                multi={true}
                placeholder="Select contract status"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Status</label>
            <div className="field__input">
              <Select
                name="status"
                value={filters.status}
                onChange={this.onFilterChange}
                options={statusList}
                multi={true}
                placeholder="Select Status"
              />
            </div>
          </div>
        </div>
        <div className="field-section col-md-4">
          <div className="field__label">
            <label className="field__label-label">Type</label>
            <div className="field__input">
              <Select
                name="productMappings"
                value={filters.productMappings}
                onChange={this.onFilterChange}
                options={this.getAllTechnoType()}
                multi={true}
                placeholder="Select Type"
              />
            </div>
          </div>
        </div>
        {this.props.manufacturers &&
          this.props.manufacturers.length !== 0 && (
            <div className="field-section col-md-4">
              <div className="field__label">
                <label className="field__label-label">Service Provider</label>
                <div className="field__input">
                  <Select
                    name="manufacturer"
                    value={filters.manufacturer}
                    onChange={this.onFilterChange}
                    options={manufacturers}
                    searchable={true}
                    placeholder="Select Service Provider"
                    clearable={false}
                    multi={true}
                  />
                </div>
              </div>
            </div>
          )}
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
