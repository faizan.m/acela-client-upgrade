import React from 'react';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { fromISOStringToFormattedDate } from '../../utils/CalendarUtil';

interface IViewSubscriptionProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  manufacturers: any[];
  subscription: ISubscription;
  isFetching: boolean;
  userType: string;
}

interface  IViewSubscriptionState {
  sites: any[];
  subscription: ISubscription;
  open: boolean;
  openMigrationInfo: boolean;
  openCircuitInfo: boolean;
  openMigrationDetails: boolean;
  openServiceInfo: boolean;
}

export default class ViewSubscription extends React.Component<
  IViewSubscriptionProps,
   IViewSubscriptionState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IViewSubscriptionProps) {
    super(props);
  }

  renderDeviceDetails = () => {
    const subscription = this.props.subscription;

    return (
      <div className="subscription-details-modal__body">
        <div className="subscription-details-modal__body-field">
          <label>subscription id #</label>
          <label>
            {subscription.serial_number
              ? subscription.serial_number
              : 'Unknown'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Start Date</label>
          <label>
            {subscription.installation_date
              ? fromISOStringToFormattedDate(subscription.installation_date)
              : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>End Date</label>
          <label>
            {subscription.expiration_date
              ? fromISOStringToFormattedDate(subscription.expiration_date)
              : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Service Provider</label>
          <label>
            {subscription.manufacturer_name
              ? subscription.manufacturer_name
              : 'Unknown'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>contract status</label>
          <label>
            {subscription.contract_status
              ? subscription.contract_status
              : 'Unknown'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Product</label>

          <label>{subscription.device_name ? subscription.device_name : '-'}</label>
        </div>
        <div className="subscription-details-modal__body-field details-name">
          <label>Type</label>

          <label>
            {subscription.product_name ? subscription.product_name : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Term Length</label>
          <label>
            {subscription.term_length ? subscription.term_length : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Auto Renewal Term</label>
          <label>
            {subscription.auto_renewal_term
              ? subscription.auto_renewal_term
              : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field details-name">
          <label>License Qty</label>
          <label>
            {subscription.licence_qty ? subscription.licence_qty : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>contract Number</label>
          <label>
            {subscription.service_contract_number
              ? subscription.service_contract_number
              : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>billing model</label>
          <label>
            {subscription.billing_model ? subscription.billing_model : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>True Forward Date</label>
          <label>
            {subscription.true_forward_date ? fromISOStringToFormattedDate(subscription.true_forward_date) : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Notes</label>
          <label className="note-description">
            {subscription.notes ? subscription.notes : '-'}
          </label>
        </div>
      </div>
    );
  };

  getTitle = () => {
    return (
      <div className="subscription-details">
        Subscription Details
        <div className={`status--${this.props.subscription.status}`}>
          {this.props.subscription.status === 'Active'
            ? 'In Service '
            : 'Decommissioned'}
        </div>
      </div>
    );
  };

  getBody = () => {
    return (
      <div className="edit-subscription">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className={`${this.props.isFetching ? `loading` : ''}`}>
          {this.props.subscription && this.renderDeviceDetails()}
        </div>
        <div className="subscription-detail__header">
          <h5>Items</h5>
          <div className="items-list">
            <div className="item"> No Items</div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-subscription__footer
        ${this.props.isFetching ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.props.subscription && this.getTitle()}
        bodyElement={this.props.subscription && this.getBody()}
        footerElement={this.props.subscription && this.getFooter()}
        className="add-subscription"
      />
    );
  }
}
