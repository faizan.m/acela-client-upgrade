import React from 'react';
import { connect } from 'react-redux';
import * as XLSX from 'xlsx';
import { addErrorMessage } from '../../actions/appState';
import {
  fetchContractStatuses,
  fetchDeviceCategories,
  fetchManufacturers,
  fetchProviderIntegrationsForFilter,
  fetchSites,
  fetchSitesCustomersUser,
  fetchStatuses,
  fetchTypes, FETCH_XLSX_REQUEST, FETCH_XLSX__SUCCESS, setXlsxData
} from '../../actions/inventory';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import FilterModal from './filterSubs';
import ViewSubscription from './viewSubscription';

import {
  fetchAllProviderIntegrations,
  fetchProviderConfigStatus, FETCH_PROVIDER_CONFIG_STATUS_SUCCESS
} from '../../actions/provider/integration';
import {
  editSubscriptions,
  editSubscriptionsCustomerUser, EDIT_SUBSCRIPTION_FAILURE,
  EDIT_SUBSCRIPTION_SUCCESS, fetchProductMappings,
  fetchProductMappingsCU,
  fetchSubscriptions,
  fetchSubscriptionsCustomerUser
} from '../../actions/subscription';
import "../../commonStyles/filterModal.scss";
import SquareButton from '../../components/Button/button';
import EditButton from '../../components/Button/editButton';
import Checkbox from '../../components/Checkbox/checkbox';
import store from '../../store';
import { fromISOStringToFormattedDate } from '../../utils/CalendarUtil';
import { allowPermission } from '../../utils/permissions';
import { searchInFields } from '../../utils/searchListUtils';
import EditSubscription from './edit';
import './style.scss';

interface ISubscriptionProps extends ICommonProps {
  fetchSubscriptions: TFetchDevices;
  fetchSubscriptionsCustomerUser: TFetchDevicesCustomerUser;
  fetchManufacturers: TFetchManufacturers;
  fetchSites: TFetchSites;
  fetchSitesCustomersUser: TFetchSitesCustomerUser;
  fetchTypes: TFetchTypes;
  subscriptions: ISubscription[];
  manufacturers: any[];
  sites: any[];
  types: any[];
  customerId: number;
  subscription: ISubscription;
  contractStatuses: any[];
  user: ISuperUser;
  isFetchingSubscriptions: boolean;
  isDeviceFetching: boolean;
  addErrorMessage: TShowErrorMessage;
  fetchProviderConfigStatus: any;
  isFetchingSubscription: boolean;
  fetchDeviceCategories: any;
  deviceCategoryList: any;
  fetchProviderIntegrationsForFilter: any;
  providerIntegration: any;
  allProviderIntegrations: IProviderIntegration[];
  fetchAllProviderIntegrations: any;
  updateSitesCU: any;
  fetchContractStatuses: any;
  updateSitesPU: any;
  fetchProductMappings: any;
  productMappings: any;
  setXlsxData: any;
  fetchStatuses: any;
  statusList: any[];
  editSubscriptions: any;
  editSubscriptionsCustomerUser: any;
  fetchProductMappingsCU: any;
}

interface ISubscriptionState {
  rows: ISubscription[];
  selectedRows: number[];
  errorList?: any;
  isAddDeviceModalOpen: boolean;
  isEditSubscriptionModalOpen: boolean;
  isViewDeviceModalOpen: boolean;
  isFilterModalOpen: boolean;
  customerId?: number;
  subscription?: ISubscription;
  searchString: string;
  showInactiveDevices: boolean;
  filters: ISubscriptionFilters;
  typesLabelIds: { [id: string]: string };
  statusLabelIds: { [id: number]: string };
  contractStatusLabelIds: { [id: number]: string };
  manufacturerLabelIds: { [id: string]: string };
  isDevicePosting: boolean;
  isPostingBulkUpdate: boolean;
  deviceCategories: any;
  isBulkUpdateModalOpen: boolean;
  clearAdd: boolean;
  fileReading: boolean;
}

class SubscriptionManagement extends React.Component<
  ISubscriptionProps,
  ISubscriptionState
> {
  constructor(props: ISubscriptionProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    rows: [],
      selectedRows: [],
      isAddDeviceModalOpen: false,
      isEditSubscriptionModalOpen: false,
      isViewDeviceModalOpen: false,
      isBulkUpdateModalOpen: false,
      isFilterModalOpen: false,
      searchString: '',
      showInactiveDevices: false,
      deviceCategories: null,
      filters: {
        status: [],
        productMappings: [],
        manufacturer: [],
        contractStatus: [],
      },
      typesLabelIds: {},
      statusLabelIds: {},
      contractStatusLabelIds: {},
      manufacturerLabelIds: {},
      isDevicePosting: false,
      isPostingBulkUpdate: false,
      clearAdd: false,
      fileReading: false,
      subscription: {
        service_contract_number: 0,
        installation_date: '',
        expiration_date: '',
        notes: '',
        term_length: 0,
        auto_renewal_term: 0,
        category_id: null,
        serial_number: null,
        manufacturer_id: null,
        billing_model: '',
        licence_qty: 0,
      },
  });

  componentDidMount() {
    this.props.fetchProviderConfigStatus().then(action => {
      if (
        action.type === FETCH_PROVIDER_CONFIG_STATUS_SUCCESS &&
        (action.response && action.response.crm_authentication_configured)
      ) {
        this.props.fetchManufacturers();
        this.props.fetchDeviceCategories();
        this.props.fetchContractStatuses();
      }
    });
    this.props.fetchTypes();
    if (this.props.statusList.length === 0) {
      this.props.fetchStatuses();
    }
    if (this.props.customerId) {
      this.props.fetchSubscriptions(
        this.props.customerId,
        this.state.showInactiveDevices
      );
      this.props.fetchSites(this.props.customerId);
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.fetchSubscriptionsCustomerUser(this.state.showInactiveDevices);
      this.props.fetchSitesCustomersUser();
    }
    if (!this.props.allProviderIntegrations) {
      this.props.fetchAllProviderIntegrations();
    }

    if (!this.props.productMappings) {
      if (
        this.props.user &&
        this.props.user.type &&
        this.props.user.type === 'customer'
      ) {
        this.props.fetchProductMappingsCU();
      } else {
        this.props.fetchProductMappings();
      }
    } else {
      this.setLabelsType(this.props);
    }
    if (this.props.manufacturers) {
      this.setLabelsManufacturer(this.props);
    }
    if (this.props.productMappings) {
      this.setLabelsType(this.props);
    }
    if (this.props.statusList) {
      this.setLabelsStatus(this.props);
    }
  }

  componentDidUpdate(prevProps: ISubscriptionProps) {
    if (this.props.subscriptions !== prevProps.subscriptions) {
      const rows = this.getRows(this.props, '', this.state.filters);
      this.setState({
        rows,
      });
    }
    if (
      this.props.customerId &&
      this.props.customerId !== prevProps.customerId
    ) {
      this.props.fetchSubscriptions(
        this.props.customerId,
        this.state.showInactiveDevices
      );
      this.props.fetchSites(this.props.customerId);
      this.setState({
        filters: {
          status: [],
          contractStatus: [],
          productMappings: [],
          manufacturer: [],
        },
      });
    }
    if (this.props.subscription !== prevProps.subscription) {
      this.setState({ subscription: this.props.subscription });
    }
    if (this.props.manufacturers !== prevProps.manufacturers) {
      this.setLabelsManufacturer(this.props);
    }
    if (this.props.productMappings !== prevProps.productMappings) {
      this.setLabelsType(this.props);
    }
    if (this.props.contractStatuses !== prevProps.contractStatuses) {
      const contractStatusLabelIds = this.props.contractStatuses.reduce(
        (statusIds, contractStatus) => {
          statusIds[contractStatus.contract_status_id] =
            contractStatus.status === 'N.A.'
              ? 'Not Covered'
              : contractStatus.contract_status;
          return statusIds;
        },
        {}
      );
      this.setState({
        contractStatusLabelIds,
      });
    }
    if (this.props.statusList !== prevProps.statusList) {
      this.setLabelsStatus(this.props);
    }
    if (this.props.user && this.props.user !== prevProps.user) {
      if (this.props.user.type && this.props.user.type === 'customer') {
        this.props.fetchSubscriptionsCustomerUser(this.state.showInactiveDevices);
        this.props.fetchSitesCustomersUser();
      }
    }
    if (
      this.props.allProviderIntegrations && 
      this.props.allProviderIntegrations !== prevProps.allProviderIntegrations
    ) {
      const provider = this.props.allProviderIntegrations.filter(
        int => int.type === "CONNECTWISE"
      );
      if (provider.length > 0) {
        this.props.fetchProviderIntegrationsForFilter(provider[0].id);
      }
    }
  }

  setLabelsStatus = props => {
    const statusLabelIds = props.statusList.reduce(
      (statusIds, contractStatus) => {
        statusIds[contractStatus.id] = `Status: ${contractStatus.description}`;

        return statusIds;
      },
      {}
    ); // tslint:disable-line

    this.setState({
      statusLabelIds,
    });
  };

  setLabelsManufacturer = props => {
    const mapping = props.manufacturers;
    let manufacturerLabelIds = {};
    mapping.forEach((el) => {
      manufacturerLabelIds[String(el.id)] = el.label;
    });
    this.setState({
      manufacturerLabelIds,
    });
  };

  setLabelsType = props => {
    const typesLabelIds = props.productMappings.reduce((labelIds, type) => {
      labelIds[type.id] = type.name;

      return labelIds;
    }, {}); // tslint:disable-line

    this.setState({
      typesLabelIds,
    });
  };
  getRows = (
    nextProps: ISubscriptionProps,
    searchString?: string,
    filters?: ISubscriptionFilters,
    showInactiveDevices?: boolean
  ) => {
    let subscriptions = nextProps.subscriptions;
    const search = searchString ? searchString : this.state.searchString;

    if (filters) {
      // Add filter for type once included.
      const filterStatus = filters.status;
      const filterContractStatus = filters.contractStatus;
      const filterManufacturer = filters.manufacturer;
      const filterTypes = filters.productMappings;

      subscriptions = subscriptions.filter(device => {
        // If there are no filters for the
        // specific key then includes will
        // give false, hence below code is used
        // used to handle that.
        const hasFilteredStatus =
          filterStatus.length > 0
            ? filterStatus.includes(device.status_id)
            : true;
        const hasFilteredContractStatus =
          filterContractStatus.length > 0
            ? filterContractStatus.includes(
                device.contract_status &&
                  device.contract_status.contract_status_id
              )
            : true;
        const hasFilteredType =
          filterTypes.length > 0
            ? filterTypes.includes(device.product_id)
            : true;

        const hasFilteredManufacturer =
          filterManufacturer.length > 0
            ? filterManufacturer.includes(device.manufacturer_id)
            : true;

        return (
          hasFilteredStatus &&
          hasFilteredContractStatus &&
          hasFilteredType &&
          hasFilteredManufacturer
        );
      });
    }
    if (search && search.length > 0) {
      subscriptions = subscriptions.filter(
        row =>
          searchInFields(row, search,[
          'product_name',
          'device_name',
          'device_type',
          'status',
          'contract_status.contract_status',
          'serial_number',
          'category_name',
          'manufacturer_name',
          'type_name'
        ])
      );
    }

    const rows: any = subscriptions.map((device, index) => ({
      serial_number: device.serial_number,
      product_id: device.product_id,
      product_name: device.product_name,
      type_name: device.type_name,
      manufacturer_name: device.manufacturer_name,
      manufacturer_id: device.manufacturer_id,
      category_name: device.category_name,
      category_id: device.category_id,
      model: device.model,
      device_type: device.device_type,
      site: device.name,
      service_contract_number: device.service_contract_number,
      EOL_date: device.EOL_date,
      EOS_date: device.EOS_date,
      LDOS_date: device.LDOS_date,
      notes: device.notes,
      status: device.status,
      status_id: device.status_id,
      contract_status:
        device.contract_status && device.contract_status.contract_status,
      contract_status_id:
        device.contract_status && device.contract_status.contract_status_id,
      product_bulletin_url: device.product_bulletin_url,
      id: device.id,
      installation_date: device.installation_date,
      expiration_date: device.expiration_date,
      purchase_date: device.purchase_date,
      host_name:
        device && device.monitoring_data && device.monitoring_data.host_name,
      migration_info: device.migration_info,
      asset_tag: device.asset_tag,
      licence_qty: device.licence_qty,
      auto_renewal_term: device.auto_renewal_term,
      term_length: device.term_length,
      billing_model: device.billing_model,
      true_forward_date: device.true_forward_date,
      device_name: device.device_name,
      is_managed:
        (device &&
          device.monitoring_data &&
          device.monitoring_data.is_managed) ||
        false,
      index,
    }));

    return rows;
  };

  onRowClick = rowInfo => {
    this.setState(() => ({
      isViewDeviceModalOpen: true,
      subscription: this.state.rows[rowInfo.index],
    }));
  };

  toggleEditSubscriptionModal = () => {
    this.setState(prevState => ({
      isEditSubscriptionModalOpen: !prevState.isEditSubscriptionModalOpen,
      subscription: this.getEmptyState().subscription,
    }));
  };

  toggleViewDeviceModal = () => {
    this.setState(prevState => ({
      isViewDeviceModalOpen: !prevState.isViewDeviceModalOpen,
    }));
  };

  toggleFilterModal = () => {
    this.setState(prevState => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onRowsToggle = selectedRows => {
    this.setState({
      selectedRows,
    });
  };

  onFiltersUpdate = (filters: ISubscriptionFilters) => {
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState({
      rows,
      filters,
      isFilterModalOpen: false,
    });
  };

  handleChange = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    this.setState({ searchString }, () => {
      this.handleRows();
    });
  };

  handleRows = () => {
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters
    );

    this.setState({ rows });
  };

  onChecboxChanged = (event: any) => {
    const showInactiveDevices = event.target.checked;
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters,
      showInactiveDevices
    );

    this.setState({
      showInactiveDevices,
      rows,
    });
    if (this.props.user.type && this.props.user.type === 'customer') {
      this.props.fetchSubscriptionsCustomerUser(showInactiveDevices);
    } else {
      this.props.fetchSubscriptions(this.props.customerId, showInactiveDevices);
    }
  };

  deleteFilter = (filterName: string, attributeIndex: number) => {
    const prevFilters = this.state.filters;
    const filters = {
      ...prevFilters,
      [filterName]: [
        ...prevFilters[filterName].slice(0, attributeIndex),
        ...prevFilters[filterName].slice(attributeIndex + 1),
      ],
    };
    const rows = this.getRows(this.props, this.state.searchString, filters);

    this.setState(() => ({
      filters,
      rows,
    }));
  };

  // Render methods

  renderFilters = () => {
    const {
      filters,
      typesLabelIds,
      statusLabelIds,
      contractStatusLabelIds,
      manufacturerLabelIds,
    } = this.state;
    const shouldRenderFilters = filters
      ? (filters.productMappings && filters.productMappings.length > 0) ||
        (filters.contractStatus && filters.contractStatus.length > 0) ||
        (filters.status && filters.status.length > 0) ||
        (filters.manufacturer && filters.manufacturer.length > 0)
      : false;

    return shouldRenderFilters ? (
      <div className="subscription-management__table-filters">
        <label>Applied Filters: </label>
        {Object.keys(filters).map((filterName, filterIndex) => {
          const filterValues = filters[filterName];
          let labelIds = {};
          if (filterName === 'status') {
            labelIds = statusLabelIds;
          } else if (filterName === 'contractStatus') {
            labelIds = contractStatusLabelIds;
          } else if (filterName === 'productMappings') {
            labelIds = typesLabelIds;
          } else if (filterName === 'manufacturer') {
            labelIds = manufacturerLabelIds;
          }

          if (filterValues.length > 0) {
            return filterValues.map((id, valueIndex) => (
              <div
                key={`${filterIndex}.${valueIndex}`}
                className="subscription-management__filter"
              >
                <label>{labelIds[id] ? labelIds[id] : 'N.A.'}</label>
                <span
                  onClick={() => this.deleteFilter(filterName, valueIndex)}
                />
              </div>
            ));
          }
        })}
      </div>
    ) : null;
  };

  renderTopBar = () => {
    return (
      <div
        className={
          this.props.customerId ||
          (this.props.user &&
            this.props.user.type &&
            this.props.user.type === 'customer')
            ? 'subscription-management__table-top '
            : 'subscription-management__table-top '
        }
      >
        <div className="subscription-management__table-actions">
          <div
            className={
              this.props.customerId ||
              (this.props.user &&
                this.props.user.type &&
                this.props.user.type === 'customer')
                ? 'subscription-management__table-header-search'
                : 'subscription-management__table-header-search disable-div'
            }
          >
            {allowPermission('search_device') && (
              <Input
                field={{
                  label: '',
                  type: "SEARCH",
                  value: this.state.searchString,
                  isRequired: false,
                }}
                width={12}
                placeholder="Search"
                name="searchString"
                onChange={this.handleChange}
                className="subscription-management__search"
              />
            )}
            <Checkbox
              isChecked={this.state.showInactiveDevices}
              name="option"
              onChange={e => this.onChecboxChanged(e)}
            >
              Show Out of Service
            </Checkbox>
          </div>
          <div className="subscription-management__table-header-btn-group">
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                !this.props.subscriptions ||
                (this.props.subscriptions &&
                  this.props.subscriptions.length === 0)
              }
              bsStyle={"primary"}
            />
            {allowPermission('import') &&
              this.props.user &&
              this.props.user.type &&
              this.props.user.type !== 'customer' && (
                <label
                  className="btn square-btn btn-primary
                file-button import-button"
                >
                  <img src="/assets/icons/import.png" />
                  Import
                  <input
                    type="file"
                    name="xlsxFile"
                    accept=".xls,.xlsx"
                    onChange={this.handleFileSelect}
                    onClick={(event: any) => {
                      event.target.value = null;
                    }}
                  />
                </label>
              )}
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };
  handleFileSelect = (event: any) => {
    this.setState({ fileReading: true });
    const file = event.target.files[0];
    if (file.name.includes('xls')) {
      store.dispatch({ type: FETCH_XLSX_REQUEST });
      const reader = new FileReader();
      reader.onload = (e: any) => {
        const data = e.target.result;
        const workbook = XLSX.read(data, {
          type: 'binary',
        });
        // Here is your object
        workbook.SheetNames.forEach(sheetName => {
          const xlRowObject = XLSX.utils.sheet_to_json(
            workbook.Sheets[sheetName],
            {
              defval: '',
              blankrows: false,
            }
          );
          store.dispatch({ type: FETCH_XLSX__SUCCESS });
          if (xlRowObject.length > 0) {
            this.props.setXlsxData(xlRowObject, file);
            this.setState({ fileReading: false });
            this.props.history.push(
              '/subscription-management/import-subscription'
            );
          } else {
            this.setState({ fileReading: false });
            this.props.addErrorMessage('Import file cannot be empty');
          }
        });
      };

      reader.onerror = ex => {
        console.info(ex);
        this.setState({ fileReading: false });
      };

      reader.readAsBinaryString(file);
    } else {
      this.setState({ fileReading: false });
      this.props.addErrorMessage('Incorrect file type');
    }
  };

  onEditRowClick = (id: number, event: any) => {
    event.stopPropagation();
    const row = this.state.rows.filter(sub => sub.id === id)[0];
    this.setState(() => ({
      isEditSubscriptionModalOpen: true,
      subscription: row,
    }));
  };

  onDeviceEditSave = (subscription: ISubscription) => {
    this.setState({ isDevicePosting: true });
    const customerId = this.props.customerId;
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.editSubscriptionsCustomerUser(subscription).then(action => {
        if (action.type === EDIT_SUBSCRIPTION_SUCCESS) {
          this.setState({
            isEditSubscriptionModalOpen: false,
            isDevicePosting: false,
          });
          if (
            this.props.user &&
            this.props.user.type &&
            this.props.user.type === 'customer'
          ) {
            this.props.fetchSubscriptionsCustomerUser(
              this.state.showInactiveDevices
            );
          }
        } else if (action.type === EDIT_SUBSCRIPTION_FAILURE) {
          this.setState({
            isEditSubscriptionModalOpen: true,
            errorList: action.errorList.data,
            isDevicePosting: false,
          });
        }
      });
    } else {
      this.props
        .editSubscriptions(customerId, subscription.id, subscription)
        .then(action => {
          if (action.type === EDIT_SUBSCRIPTION_SUCCESS) {
            this.setState({
              isEditSubscriptionModalOpen: false,
              isDevicePosting: false,
            });

            this.props.fetchSubscriptions(
              customerId,
              this.state.showInactiveDevices
            );
          } else if (action.type === EDIT_SUBSCRIPTION_FAILURE) {
            this.setState({
              isEditSubscriptionModalOpen: true,
              errorList: action.errorList.data,
              isDevicePosting: false,
            });
          }
        });
    }
  };

  render() {
    const columns: any = [
      {
        accessor: 'status',
        Header: 'Status',
        sortable: false,
        width: 120,
        Cell: status => (
          <div className={`subscription-status-icons`}>
            {status.original.contract_status === 'Active' && (
              <img
                src={`/assets/icons/verified.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.contract_status === 'Expiring' && (
              <img
                src={`/assets/icons/caution.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.contract_status === 'N.A.' && (
              <img
                src={`/assets/icons/cancel.svg`}
                className="status-svg-images"
                alt=""
                title={'Not covered'}
              />
            )}
            {status.original.contract_status === 'Expired' && (
              <img
                src={`/assets/icons/HIGH.svg`}
                className="status-svg-images"
                alt=""
                title={status.original.contract_status}
              />
            )}
            {status.original.status !== 'Active' && (
              <img
                src={`/assets/icons/in-active-right.svg`}
                alt=""
                className="status-svg-images"
                title={`${
                  status.value === 'Active' ? 'In Service ' : 'Decommissioned'
                }`}
              />
            )}
            {status.original.status === 'Active' && (
              <img
                src={`/assets/icons/active-right.svg`}
                alt=""
                className="status-svg-images"
                title={'In Service'}
              />
            )}
            { (!status.original.auto_renewal_term || status.original.auto_renewal_term === 0) && (
              <img
                src={`/assets/icons/red-flag.png`}
                alt=""
                className="status-svg-images"
                title={'No Auto Renew'}
              />
            )}
          </div>
        ),
      },
      {
        accessor: 'serial_number',
        Header: 'Subscription ID',
        Cell: s => (
          <a>{s.original.serial_number ? s.original.serial_number : ''}</a>
        ),
      },
      {
        accessor: 'product_name',
        Header: 'Type',
        Cell: cell => <div> {`${cell.value ? cell.value : '-'}`}</div>,
      },
      {
        accessor: 'manufacturer_name',
        Header: 'Service Provider',
        Cell: cell => (
          <div>
            {`${cell.value ? cell.value : cell.value && cell.value.toLowerCase().startsWith('sub') ? 'Cisco' : 'Unknown'}`}
          </div>
        ),
      },
      {
        accessor: 'device_name',
        Header: 'Product',
        Cell: cell => <div> {`${cell.value ? cell.value : '-'}`}</div>,
      },
      {
        accessor: 'installation_date',
        Header: 'Start date',
        width: 130,
        Cell: s => (
          <div>
            {' '}
            {s.original.installation_date
              ? fromISOStringToFormattedDate(s.original.installation_date)
              : ''}
          </div>
        ),
      },
      {
        accessor: 'expiration_date',
        Header: 'End date',
        width: 130,
        Cell: s => (
          <div>
            {' '}
            {s.original.expiration_date
              ? fromISOStringToFormattedDate(s.original.expiration_date)
              : ''}
          </div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Edit',
        width: 60,
        sortable: false,
        show: allowPermission('edit_device'),
        Cell: cell => (
          <EditButton onClick={e => this.onEditRowClick(cell.original.id, e)} />
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };

    return (
      <div className="subscription-management subscription-manage">
        <h3>Subscription Management</h3>
        <div className="loader">
          <Spinner
            show={
              this.props.isFetchingSubscription ||
              this.props.isFetchingSubscriptions ||
              this.state.fileReading
            }
          />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`device-listing__table ${
            this.props.isFetchingSubscription ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          loading={this.props.isFetchingSubscriptions}
        />
        <ViewSubscription
          {...this.props}
          show={this.state.isViewDeviceModalOpen}
          onClose={this.toggleViewDeviceModal}
          manufacturers={this.props.manufacturers}
          subscription={this.state.subscription}
          isFetching={this.props.isDeviceFetching}
          userType={this.props.user && this.props.user.type}
        />
        <EditSubscription
          show={this.state.isEditSubscriptionModalOpen}
          onClose={this.toggleEditSubscriptionModal}
          onSubmit={this.onDeviceEditSave}
          manufacturers={this.props.manufacturers}
          subscriptionDetails={this.state.subscription}
          isFetching={this.state.isDevicePosting}
          userType={this.props.user && this.props.user.type}
          errorList={this.state.errorList}
        />
        <FilterModal
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          productMappings={this.props.productMappings}
          contractStatuses={this.props.contractStatuses}
          statusList={this.props.statusList}
          prevFilters={this.state.filters}
          manufacturers={this.props.manufacturers}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  subscriptions: state.subscription.subscriptions,
  manufacturers: state.inventory.manufacturers,
  sites: state.inventory.sites,
  types: state.inventory.types,
  contractStatuses: state.inventory.contractStatuses,
  customerId: state.customer.customerId,
  subscription: state.subscription.subscription,
  user: state.profile.user,
  isFetchingSubscriptions: state.subscription.isFetchingSubscriptions,
  isFetchingSubscription: state.subscription.isFetchingSubscription,
  isDeviceFetching: state.inventory.isDeviceFetching,
  deviceCategoryList: state.inventory.deviceCategoryList,
  providerIntegration: state.inventory.providerIntegration,
  allProviderIntegrations: state.providerIntegration.allProviderIntegrations,
  productMappings: state.subscription.productMappings,
  statusList: state.inventory.statusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProductMappings: () => dispatch(fetchProductMappings()),
  fetchProductMappingsCU: () => dispatch(fetchProductMappingsCU()),
  fetchDeviceCategories: () => dispatch(fetchDeviceCategories()),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchContractStatuses: () => dispatch(fetchContractStatuses()),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  fetchSitesCustomersUser: () => dispatch(fetchSitesCustomersUser()),
  fetchTypes: () => dispatch(fetchTypes()),
  fetchSubscriptionsCustomerUser: (show: boolean) =>
    dispatch(fetchSubscriptionsCustomerUser(show)),
  fetchSubscriptions: (id: number, show: boolean) =>
    dispatch(fetchSubscriptions(id, show)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
  fetchProviderIntegrationsForFilter: (id: number) =>
    dispatch(fetchProviderIntegrationsForFilter(id)),
  fetchAllProviderIntegrations: () => dispatch(fetchAllProviderIntegrations()),
  setXlsxData: (data: any, file: any) => dispatch(setXlsxData(data, file)),
  fetchStatuses: () => dispatch(fetchStatuses()),
  editSubscriptions: (
    customerId: number,
    id: number,
    subs: ISubscription,
    show: boolean
  ) => dispatch(editSubscriptions(customerId, id, subs, show)),
  editSubscriptionsCustomerUser: (subs: ISubscription) =>
    dispatch(editSubscriptionsCustomerUser(subs)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SubscriptionManagement);
