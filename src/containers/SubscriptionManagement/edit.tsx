import { cloneDeep } from 'lodash';
import React from 'react';

import SquareButton from '../../components/Button/button';
import Checkbox from '../../components/Checkbox/checkbox';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { commonFunctions } from '../../utils/commonFunctions';

interface IEditSubscriptionFormProps {
  show: boolean;
  onClose: (e: any) => void;
  onSubmit: (newDevice: ISubscription) => void;
  manufacturers: any[];
  subscriptionDetails: any;
  isFetching: boolean;
  errorList: any;
  userType?: string;
}

interface IEditSubscriptionFormState {
  isNoAutoRenew: boolean;
  subscription: ISubscription;
  error: {
    serial_number: IFieldValidation;
    manufacturer_id: IFieldValidation;
    service_contract_number: IFieldValidation;
    installation_date: IFieldValidation;
    expiration_date: IFieldValidation;
    notes: IFieldValidation;
    term_length: IFieldValidation;
    auto_renewal_term: IFieldValidation;
    category_id: IFieldValidation;
    true_forward_date: IFieldValidation;
  };
}

export default class EditSubscriptionForm extends React.Component<
  IEditSubscriptionFormProps,
  IEditSubscriptionFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: IEditSubscriptionFormProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    isNoAutoRenew: false,
    subscription: {
      service_contract_number: 0,
      installation_date: '',
      expiration_date: '',
      notes: '',
      term_length: 0,
      auto_renewal_term: 0,
      category_id: null,
      serial_number: null,
      manufacturer_id: null,
      billing_model: '',
      licence_qty: 0,
    },
    error: {
      serial_number: { ...EditSubscriptionForm.emptyErrorState },
      manufacturer_id: { ...EditSubscriptionForm.emptyErrorState },
      service_contract_number: { ...EditSubscriptionForm.emptyErrorState },
      installation_date: { ...EditSubscriptionForm.emptyErrorState },
      expiration_date: { ...EditSubscriptionForm.emptyErrorState },
      notes: { ...EditSubscriptionForm.emptyErrorState },
      term_length: { ...EditSubscriptionForm.emptyErrorState },
      auto_renewal_term: { ...EditSubscriptionForm.emptyErrorState },
      category_id: { ...EditSubscriptionForm.emptyErrorState },
      true_forward_date: { ...EditSubscriptionForm.emptyErrorState },
    },
  });

  componentDidMount() {
    if (this.props.subscriptionDetails) {
      this.setState({
        ...cloneDeep(this.getEmptyState()),
        subscription: this.props.subscriptionDetails,
        isNoAutoRenew: !this.props.subscriptionDetails.auto_renewal_term || 
          this.props.subscriptionDetails.auto_renewal_term === 0
      });
    }
  }

  componentDidUpdate(prevProps: IEditSubscriptionFormProps) {
    if (
      this.props.subscriptionDetails &&
      prevProps.subscriptionDetails !==
        this.props.subscriptionDetails
    ) {
      this.setState({
        ...cloneDeep(this.getEmptyState()),
        subscription: this.props.subscriptionDetails,
        isNoAutoRenew:
          !this.props.subscriptionDetails.auto_renewal_term ||
          this.props.subscriptionDetails.auto_renewal_term === 0,
      });
    }
    if (
      this.props.errorList &&
      prevProps.errorList !== this.props.errorList
    ) {
      this.setValidationErrors(this.props.errorList);
    }
  }

  setValidationErrors = errorList => {
    const newState: IEditSubscriptionFormState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };
  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = this.getValues(event);

    this.setState(prevState => ({
      subscription: {
        ...prevState.subscription,
        [targetName]: targetValue,
      },
    }));
  };

  getValues = e => {
    const value =
      e.target.type === 'number'
        ? parseInt(e.target.value, 10) > 0
          ? parseInt(e.target.value, 10)
          : 0
        : e.target.value;

    return value;
  };

  onClose = e => {
    this.setState({
      ...cloneDeep(this.getEmptyState()),
    });
    this.props.onClose(e);
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      this.state.subscription.serial_number &&
      this.state.subscription.serial_number.length > 300
    ) {
      error.serial_number.errorState = "error";
      error.serial_number.errorMessage =
        'Serial No. should be greater than 300 chars.';

      isValid = false;
    }

    if (!this.state.subscription.manufacturer_id) {
      error.manufacturer_id.errorState = "error";
      error.manufacturer_id.errorMessage = 'Please select service provider';

      isValid = false;
    }

    if (
      this.state.subscription.manufacturer_id &&
      this.state.subscription.manufacturer_id < 0
    ) {
      error.manufacturer_id.errorState = "error";
      error.manufacturer_id.errorMessage = 'Enter a valid service provider';

      isValid = false;
    }
    if (
      this.state.subscription.term_length < 1 ||
      this.state.subscription.term_length > 90
    ) {
      error.term_length.errorState = "error";
      error.term_length.errorMessage = 'Enter a value between 1 to 90';

      isValid = false;
    }
    if (
      this.state.subscription.auto_renewal_term < 1 ||
      this.state.subscription.auto_renewal_term > 60
    ) {
      error.auto_renewal_term.errorState = "error";
      error.auto_renewal_term.errorMessage = 'Enter a value between 1 to 60';
      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  onSubmit = e => {
    if (this.isValid()) {
      const subs = this.state.subscription;
      if (
        this.state.isNoAutoRenew ||
        (this.state.subscription.auto_renewal_term &&
        isNaN(this.state.subscription.auto_renewal_term))
      ) {
        subs.auto_renewal_term = null;
      }
      if (
        this.state.subscription.term_length &&
        isNaN(this.state.subscription.term_length)
      ) {
        subs.term_length = null;
      }
      this.props.onSubmit(subs);
    }
  };

  renderDeviceDetails = () => {
    const subscriptionDetails = this.state.subscription;

    return (
      <div
        className="subscription-details-modal__body
      subscription-details-modal__body-edit"
      >
        <div className="subscription-details-modal__body-field">
          <label>Subscription ID #</label>
          <label>
            {subscriptionDetails.serial_number
              ? subscriptionDetails.serial_number
              : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>category</label>
          <label>
            {subscriptionDetails.category_name
              ? subscriptionDetails.category_name
              : 'unknown'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>PRODUCT </label>
          <label>
            {subscriptionDetails.device_name
              ? subscriptionDetails.device_name
              : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>status </label>
          <label>
            {subscriptionDetails.status ? subscriptionDetails.status : '-'}
          </label>
        </div>
        <div className="subscription-details-modal__body-field">
          <label>Type </label>
          <label>
            {subscriptionDetails.product_name
              ? subscriptionDetails.product_name
              : '-'}
          </label>
        </div>
      </div>
    );
  };

  getTitle = () => {
    return 'Edit Subscription Details';
  };

  getBody = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(manufacturer => ({
        value: manufacturer.id,
        label: manufacturer.label,
      }))
      : [];

    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div
          className={`edit-subscription
           ${this.props.isFetching ? `loading` : ''}`}
        >
          {this.state.subscription && this.renderDeviceDetails()}
          <div className="editable-fields">
            <Input
              field={{
                label: 'Contract Number',
                type: "TEXT",
                value: this.state.subscription.service_contract_number,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter Contract Number"
              name="service_contract_number"
              error={this.state.error.service_contract_number}
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: 'Service Provider',
                type: "PICKLIST",
                value: this.state.subscription.manufacturer_id,
                isRequired: true,
                options: manufacturers,
              }}
              error={this.state.error.manufacturer_id}
              width={6}
              name="manufacturer_id"
              onChange={this.handleChange}
            />
            <Input
              field={{
                value: this.state.subscription.installation_date,
                label: 'Start Date',
                type: "DATE",
                isRequired: false,
              }}
              width={6}
              name="installation_date"
              onChange={this.handleChange}
              placeholder="Select Date"
              showTime={false}
              showDate={true}
              error={this.state.error.installation_date}
            />
            <Input
              field={{
                value: this.state.subscription.expiration_date,
                label: 'End Date',
                type: "DATE",
                isRequired: false,
              }}
              width={6}
              name="expiration_date"
              onChange={this.handleChange}
              placeholder="Select Date"
              showTime={false}
              showDate={true}
              error={this.state.error.expiration_date}
            />
            <Input
              field={{
                label: 'Term Length',
                type: "NUMBER",
                value: this.state.subscription.term_length,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter Term Length"
              name="term_length"
              error={this.state.error.term_length}
              onChange={this.handleChange}
            />{' '}
            <Input
              field={{
                label: 'Billing Model',
                type: "TEXT",
                value: this.state.subscription.billing_model,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter Billing Model"
              name="billing_model"
              onChange={this.handleChange}
            />
            <Input
              field={{
                value: this.state.subscription.true_forward_date,
                label: 'True Forward Date',
                type: "DATE",
                isRequired: false,
              }}
              width={6}
              name="true_forward_date"
              onChange={this.handleChange}
              placeholder="Select Date"
              showTime={false}
              showDate={true}
              error={this.state.error.true_forward_date}
            />
            <Input
              field={{
                label: 'License Qty',
                type: "NUMBER",
                value: this.state.subscription.licence_qty,
                isRequired: false,
              }}
              width={6}
              placeholder="Enter License Qty"
              name="licence_qty"
              onChange={this.handleChange}
            />
            {this.props.userType !== "customer" && (
              <Input
                field={{
                  label: 'Notes',
                  type: "TEXTAREA",
                  value: this.state.subscription.notes,
                  isRequired: false,
                }}
                disabled={this.props.userType === "customer"}
                width={6}
                placeholder="Enter notes"
                name="notes"
                error={this.state.error.notes}
                onChange={this.handleChange}
              />
            )}{' '}
            <div className="auto-renew-input col-md-6">
              <Checkbox
                isChecked={this.state.isNoAutoRenew}
                name="isNoAutoRenew"
                onChange={e => {
                  this.setState({
                    isNoAutoRenew: e.target.checked
                  })}
                }
                className="auto-renew-check"
              >
                No Auto Renew
              </Checkbox>
              { !this.state.isNoAutoRenew && (
                  <Input
                    field={{
                      label: 'Auto Renewal Term',
                      type: "NUMBER",
                      value: this.state.subscription.auto_renewal_term,
                      isRequired: false,
                    }}
                    width={10}
                    placeholder="Enter Auto Renewal Term"
                    name="auto_renewal_term"
                    error={this.state.error.auto_renewal_term}
                    onChange={this.handleChange}
                  />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-subscription__footer
      ${this.props.isFetching ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Update"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="add-subscription"
      />
    );
  }
}
