import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  getQuickbaseStatuses,
  fetchOrUpdateVRWHSettings,
  VRWH_SETTING_SUCCESS,
  VRWH_SETTING_FAILURE,
} from "../../actions/virtualWarehouse";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import { commonFunctions } from "../../utils/commonFunctions";
import "./style.scss";

interface DeviceListingSettingsProps {
  statusOptions: string[];
  isFetchingStatus: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getStatusOptions: () => Promise<any>;
  settingsCreateUpdate: (
    method: HTTPMethods,
    data?: IVRWarehouseSettings
  ) => Promise<any>;
}

const EmptySettings: IVRWarehouseSettings = {
  in_transit_status: {
    status: "",
    label: "",
  },
  in_stock_status: {
    status: "",
    label: "",
  },
  in_configuration_status: {
    status: "",
    label: "",
  },
  shipped_status: {
    status: "",
    label: "",
  },
  extra_statuses: [],
};

const DeviceListingSettings: React.FC<DeviceListingSettingsProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [settings, setSettings] = useState<IVRWarehouseSettings>(
    cloneDeep(EmptySettings)
  );
  const [errors, setErrors] = useState<IErrorValidation>({
    in_transit_status: commonFunctions.getErrorState(),
    in_transit_label: commonFunctions.getErrorState(),
    in_stock_status: commonFunctions.getErrorState(),
    in_stock_label: commonFunctions.getErrorState(),
    in_configuration_status: commonFunctions.getErrorState(),
    in_configuration_label: commonFunctions.getErrorState(),
    shipped_status: commonFunctions.getErrorState(),
    shipped_label: commonFunctions.getErrorState(),
  });

  useEffect(() => {
    fetchSettings();
    props.getStatusOptions();
  }, []);

  const handleChangeStatus = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newSettings = cloneDeep(settings);
    newSettings[e.target.name].status = e.target.value;
    setSettings(newSettings);
  };

  const handleChangeLabel = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newSettings = cloneDeep(settings);
    newSettings[e.target.name].label = e.target.value;
    setSettings(newSettings);
  };

  const fetchSettings = () => {
    setLoading(true);
    props
      .settingsCreateUpdate("get")
      .then((action) => {
        if (action.type === VRWH_SETTING_SUCCESS) {
          setSettings(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const validate = (): boolean => {
    let isValid = true;
    const errors: IErrorValidation = {};
    const parentKeys = ["in_transit", "in_stock", "in_configuration"];
    parentKeys.forEach((key) => {
      const parentKey = key + "_status";
      const statusKey = key + "_status";
      const labelKey = key + "_label";
      // check for status
      if (!settings[parentKey].status.length) {
        isValid = false;
        errors[statusKey] = commonFunctions.getErrorState(
          "This field is required"
        );
      } else errors[statusKey] = commonFunctions.getErrorState();
      // check for label
      if (!settings[parentKey].label.trim()) {
        isValid = false;
        errors[labelKey] = commonFunctions.getErrorState(
          "This field is required"
        );
      } else errors[labelKey] = commonFunctions.getErrorState();
    });
    setErrors(errors);
    return isValid;
  };

  const onSave = () => {
    if (validate()) {
      setLoading(true);
      props
        .settingsCreateUpdate("put", settings)
        .then((action) => {
          if (action.type === VRWH_SETTING_SUCCESS) {
            props.addSuccessMessage("Settings saved successfully!");
          } else if (action.type === VRWH_SETTING_FAILURE) {
            props.addErrorMessage("Error saving settings!");
          }
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <div className="vr-warehouse-setting-container">
      <Spinner show={loading} className="vrwh-setting-spinner" />
      <h3>Status Settings</h3>
      <div className="setting-row">
        <h4>
          <span>In Transit Setting</span>
          <img
            src={`/assets/new-icons/delivery-truck.svg`}
            className="vr-setting-logo"
            alt="Transit status"
          />
        </h4>
        <div className="col-md-12">
          <Input
            field={{
              label: "Status",
              type: "PICKLIST",
              value: settings.in_transit_status.status,
              options: props.statusOptions,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            multi={false}
            name="in_transit_status"
            labelIcon="info"
            labelTitle={"This status would be considered In Transit"}
            onChange={handleChangeStatus}
            placeholder={`Select Status`}
            loading={props.isFetchingStatus}
            error={errors.in_transit_status}
          />
          <Input
            field={{
              label: "Label",
              type: "TEXT",
              value: settings.in_transit_status.label,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            name="in_transit_status"
            onChange={handleChangeLabel}
            labelIcon="info"
            labelTitle={
              "This label will be shown on hover for Transit Icon and in Device Details"
            }
            placeholder={`Enter label to show on Product Details screen`}
            error={errors.in_transit_label}
          />
        </div>
      </div>
      <div className="setting-row">
        <h4>
          <span>In Stock Setting</span>
          <img
            src={`/assets/new-icons/warehouse.svg`}
            className="vr-setting-logo"
            alt="In Stock status"
          />
        </h4>
        <div className="col-md-12">
          <Input
            field={{
              label: "Status",
              type: "PICKLIST",
              value: settings.in_stock_status.status,
              options: props.statusOptions,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            multi={false}
            name="in_stock_status"
            onChange={handleChangeStatus}
            labelIcon="info"
            labelTitle={"This status would be considered In Stock"}
            placeholder={`Select Status`}
            loading={props.isFetchingStatus}
            error={errors.in_stock_status}
          />
          <Input
            field={{
              label: "Label",
              type: "TEXT",
              value: settings.in_stock_status.label,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            name="in_stock_status"
            onChange={handleChangeLabel}
            labelIcon="info"
            labelTitle={
              "This label will be shown on hover for Warehouse Icon and in Device Details"
            }
            placeholder={`Enter label to show on Product Details screen`}
            error={errors.in_stock_label}
          />
        </div>
      </div>
      <div className="setting-row">
        <h4>
          <span>In Configuration Setting</span>
          <img
            src={`/assets/new-icons/config-screw.png`}
            className="vr-setting-logo"
            alt="Configuration status"
          />
        </h4>
        <div className="col-md-12">
          <Input
            field={{
              label: "Status",
              type: "PICKLIST",
              value: settings.in_configuration_status.status,
              options: props.statusOptions,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            multi={false}
            name="in_configuration_status"
            onChange={handleChangeStatus}
            placeholder={`Select Status`}
            loading={props.isFetchingStatus}
            labelIcon="info"
            labelTitle={"These status would be considered In Configuration"}
            error={errors.in_configuration_status}
          />
          <Input
            field={{
              label: "Label",
              type: "TEXT",
              value: settings.in_configuration_status.label,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            name="in_configuration_status"
            onChange={handleChangeLabel}
            labelIcon="info"
            labelTitle={
              "This label will be shown on hover for Config Icon and in Device Details"
            }
            placeholder={`Enter label to show on Product Details screen`}
            error={errors.in_configuration_label}
          />
        </div>
      </div>
      <div className="setting-row">
        <h4>
          <span>Shipped Setting</span>
          <img
            src={`/assets/new-icons/shipped-truck.png`}
            className="vr-setting-logo"
            alt="Shipped status"
          />
        </h4>
        <div className="col-md-12">
          <Input
            field={{
              label: "Status",
              type: "PICKLIST",
              value: settings.shipped_status.status,
              options: props.statusOptions,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            multi={false}
            name="shipped_status"
            onChange={handleChangeStatus}
            placeholder={`Select Status`}
            loading={props.isFetchingStatus}
            labelIcon="info"
            labelTitle={"This status would be considered as Shipped"}
            error={errors.shipped_status}
          />
          <Input
            field={{
              label: "Label",
              type: "TEXT",
              value: settings.shipped_status.label,
              isRequired: true,
            }}
            className="vrwh-setting-input"
            width={6}
            name="shipped_status"
            onChange={handleChangeLabel}
            labelIcon="info"
            labelTitle={"This label will be shown in Device Details"}
            placeholder={`Enter label`}
            error={errors.shipped_label}
          />
        </div>
      </div>
      <SquareButton
        onClick={onSave}
        content={"Save"}
        bsStyle={"primary"}
        className="vr-setting-save-btn"
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  statusOptions: state.virtualWarehouse.quickbaseStatus,
  isFetchingStatus: state.virtualWarehouse.isFetchingStatus,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  getStatusOptions: () => dispatch(getQuickbaseStatuses()),
  settingsCreateUpdate: (method: HTTPMethods, data?: IVRWarehouseSettings) =>
    dispatch(fetchOrUpdateVRWHSettings(method, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DeviceListingSettings);
