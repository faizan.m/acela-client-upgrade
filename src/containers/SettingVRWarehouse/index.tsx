import React from "react";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import QuickbaseSettings from "./quickbaseSettings";
import DeviceListingSettings from "./virtualWarehouseSettings";

const VRWarehouseSettings = () => {
  return (
    <HorizontalTabSlider className="vrwr-tabs">
      <Tab title="Virtual Warehouse">
        <DeviceListingSettings />
      </Tab>
      <Tab title="Quickbase Data Sync">
        <QuickbaseSettings />
      </Tab>
    </HorizontalTabSlider>
  );
};

export default VRWarehouseSettings;
