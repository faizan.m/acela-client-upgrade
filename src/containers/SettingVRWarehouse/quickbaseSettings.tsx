import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import { CopyToClipboard } from "react-copy-to-clipboard";
import {
  testQuickbaseURL,
  quickbaseSyncTrigger,
  fetchOrUpdateQuickbaseSettings,
  QUICKBASE_SETTING_SUCCESS,
  QUICKBASE_SETTING_FAILURE,
  TEST_QUICKBASE_URL_SUCCESS,
  TEST_QUICKBASE_URL_FAILURE,
  QUICKBASE_SYNC_TRIGGER_SUCCESS,
  QUICKBASE_SYNC_TRIGGER_FAILURE,
} from "../../actions/virtualWarehouse";
import {
  addInfoMessage,
  addErrorMessage,
  addSuccessMessage,
} from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import AppValidators from "../../utils/validator";
import { commonFunctions } from "../../utils/commonFunctions";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";

interface QuickbaseSettingsProps {
  triggerSync: (fullSync?: boolean) => Promise<any>;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  testURL: (url: string) => Promise<any>;
  settingsCreateUpdate: (
    method: HTTPMethods,
    data?: IQuickbaseSettings
  ) => Promise<any>;
}

const QuickbaseSettings: React.FC<QuickbaseSettingsProps> = (props) => {
  const [showFullSyncConfirm, setFullSyncConfirm] = useState<boolean>(false);
  const [settings, setSettings] = useState<IQuickbaseSettings>({
    past_days: 0,
    acela_webhook_url: "",
    carrier_exclude_filter: [],
    qb_customer_pipeline_url: "",
    qb_parts_pipeline_url: "",
  });
  const [loading, setLoading] = useState<boolean>(false);
  const [editedURL, setEditedURL] = useState({
    qb_customer_pipeline_url: false,
    qb_parts_pipeline_url: false,
  });
  const [testStatus, setTestStatus] = useState({
    qb_customer_pipeline_url: "",
    qb_parts_pipeline_url: "",
  });
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [errors, setErrors] = useState<IErrorValidation>({});

  useEffect(() => {
    fetchSettings();
  }, []);

  const fetchSettings = () => {
    setLoading(true);
    props
      .settingsCreateUpdate("get")
      .then((action) => {
        if (action.type === QUICKBASE_SETTING_SUCCESS) {
          setSettings(action.response);
        } else if (action.type === QUICKBASE_SETTING_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChangeSettings = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (
      e.target.name === "qb_customer_pipeline_url" ||
      e.target.name === "qb_parts_pipeline_url"
    )
      setEditedURL((prev) => ({
        ...prev,
        [e.target.name]: true,
      }));
    setSettings((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  // const handleChangeTags = (carriers: string[]) => {
  //   setSettings((prev) => ({
  //     ...prev,
  //     carrier_exclude_filter: carriers,
  //   }));
  // };

  const validate = (): boolean => {
    let isValid = true;
    const errors: IErrorValidation = {};
    const urls = ["qb_customer_pipeline_url", "qb_parts_pipeline_url"];
    urls.forEach((url) => {
      if (!AppValidators.isValidUrl(settings[url])) {
        isValid = false;
        errors[url] = commonFunctions.getErrorState("Please enter a valid URL");
      } else errors[url] = commonFunctions.getErrorState();
    });
    if (!settings.past_days) {
      isValid = false;
      errors.past_days = commonFunctions.getErrorState(
        "Please enter number of days"
      );
    } else errors.past_days = commonFunctions.getErrorState();
    setErrors(errors);
    return isValid;
  };

  const testQuickbaseURL = (urlType: string) => {
    const testUrl: string = settings[urlType];
    if (!AppValidators.isValidUrl(testUrl)) {
      setErrors((prev) => ({
        ...prev,
        [urlType]: {
          errorState: "error",
          errorMessage: "Please enter a valid URL",
        },
      }));
      return;
    }
    setLoading(true);
    props
      .testURL(testUrl)
      .then((action) => {
        if (action.type === TEST_QUICKBASE_URL_SUCCESS) {
          if (action.response.is_valid) {
            props.addSuccessMessage("Entered URL is valid!");
            setTestStatus((prev) => ({ ...prev, [urlType]: "Passed" }));
          } else {
            props.addErrorMessage("URL is not valid for Quickbase!");
            setTestStatus((prev) => ({ ...prev, [urlType]: "Failed" }));
          }
        } else if (action.type === TEST_QUICKBASE_URL_FAILURE) {
          props.addErrorMessage("Bad request!");
        }
      })
      .finally(() => setLoading(false));
  };

  const handleTriggerSync = (fullSync?: boolean) => {
    if (fullSync) toggleConfirmationBox();
    props.triggerSync(fullSync).then((action) => {
      if (action.type === QUICKBASE_SYNC_TRIGGER_SUCCESS) {
        props.addInfoMessage(`${fullSync ? "Full" : ""} Sync task started...`);
      } else if (action.type === QUICKBASE_SYNC_TRIGGER_FAILURE) {
        props.addErrorMessage("Error initiating sync task on server!");
      }
    });
  };

  const toggleConfirmationBox = () => {
    setFullSyncConfirm((prev) => !prev);
  };

  const onSave = () => {
    if (validate()) {
      setLoading(true);
      props
        .settingsCreateUpdate("put", settings)
        .then((action) => {
          if (action.type === QUICKBASE_SETTING_SUCCESS) {
            props.addSuccessMessage("Quickbase Settings saved successfully!");
          } else if (action.type === QUICKBASE_SETTING_FAILURE) {
            props.addErrorMessage("Error saving settings!");
          }
        })
        .finally(() => {
          setLoading(false);
          setTestStatus({
            qb_customer_pipeline_url: "",
            qb_parts_pipeline_url: "",
          });
          setEditedURL({
            qb_customer_pipeline_url: false,
            qb_parts_pipeline_url: false,
          });
        });
    }
  };

  const customerUrlPassed = testStatus.qb_customer_pipeline_url === "Passed";
  const partsUrlPassed = testStatus.qb_parts_pipeline_url === "Passed";

  return (
    <div className="quickbase-settings-container">
      <Spinner show={loading} className="qb-setting-spinner" />
      {!firstSave && (
        <div className="qb-tr-btn-container">
          <CopyToClipboard
            text={settings.acela_webhook_url}
            onCopy={() => {
              props.addInfoMessage("URL copied to clipboard!");
            }}
          >
            <SquareButton
              onClick={() => {}}
              content={
                <>
                  <img
                    src="/assets/new-icons/copy.svg"
                    alt="Copy Webhook URL Icon"
                    className="webhook-url-copy"
                  />
                  <span>Acela Webhook URL</span>
                </>
              }
              title="Copy Acela Webhook URL"
              bsStyle={"link"}
              className="qb-copy-btn"
            />
          </CopyToClipboard>
          <SquareButton
            onClick={toggleConfirmationBox}
            content={"Trigger Full Sync"}
            title="Manual Sync Trigger for CW FIRE and Customer Sites data to Quickbase (No date filter will be applied during this)"
            bsStyle={"primary"}
            className="qb-sync-btn"
          />
          <SquareButton
            onClick={() => handleTriggerSync()}
            content={"Trigger Sync"}
            title="Manual Sync Trigger for CW FIRE and Customer Sites data to Quickbase (This will sync for Received POs Duration)"
            bsStyle={"primary"}
            className="qb-sync-btn"
          />
        </div>
      )}
      <div className="setting-row">
        <Input
          field={{
            label: "Quickbase Customer Pipeline URL",
            type: "TEXT",
            value: settings.qb_customer_pipeline_url,
            isRequired: true,
          }}
          className="qb-setting-input"
          width={6}
          name="qb_customer_pipeline_url"
          disabled={customerUrlPassed}
          onChange={handleChangeSettings}
          placeholder={"Enter URL"}
          labelIcon="info"
          labelTitle={
            "URL for making connection to Quickbase for customer data sync"
          }
          error={errors.qb_customer_pipeline_url}
        />
        {editedURL.qb_customer_pipeline_url && (
          <SquareButton
            onClick={() => testQuickbaseURL("qb_customer_pipeline_url")}
            content={
              !customerUrlPassed ? (
                "Test"
              ) : (
                <>
                  <img
                    src="/assets/new-icons/success.svg"
                    alt="Valid URL icon"
                    className="tick-icon"
                  />
                  <span>Valid URL</span>
                </>
              )
            }
            bsStyle={"default"}
            className="qb-test-url-btn"
            title={
              customerUrlPassed
                ? "Entered URL is valid, you can save the settings"
                : "Test the Webhook URL before updating settings"
            }
            disabled={customerUrlPassed}
          />
        )}
      </div>
      <div className="setting-row">
        <Input
          field={{
            label: "Quickbase Parts Pipeline URL",
            type: "TEXT",
            value: settings.qb_parts_pipeline_url,
            isRequired: true,
          }}
          className="qb-setting-input"
          width={6}
          name="qb_parts_pipeline_url"
          disabled={partsUrlPassed}
          onChange={handleChangeSettings}
          placeholder={"Enter URL"}
          labelIcon="info"
          labelTitle={
            "URL for making connection to Quickbase for Parts and FIRE data sync"
          }
          error={errors.qb_parts_pipeline_url}
        />
        {editedURL.qb_parts_pipeline_url && (
          <SquareButton
            onClick={() => testQuickbaseURL("qb_parts_pipeline_url")}
            content={
              !partsUrlPassed ? (
                "Test"
              ) : (
                <>
                  <img
                    src="/assets/new-icons/success.svg"
                    alt="Valid URL icon"
                    className="tick-icon"
                  />
                  <span>Valid URL</span>
                </>
              )
            }
            bsStyle={"default"}
            className="qb-test-url-btn"
            title={
              partsUrlPassed
                ? "Entered URL is valid, you can save the settings"
                : "Test the Webhook URL before updating settings"
            }
            disabled={partsUrlPassed}
          />
        )}
      </div>
      <div className="setting-row">
        <Input
          field={{
            label: "Sync for Received POs Duration",
            type: "NUMBER",
            value: settings.past_days,
            isRequired: true,
          }}
          className="qb-setting-input"
          width={6}
          name="past_days"
          onChange={handleChangeSettings}
          labelIcon="info"
          labelTitle={
            "CW FIRE and Customer Sites data will be synced to Quickbase for the Purchase Orders that were received within these days in the past"
          }
          placeholder={"Enter Days"}
          error={errors.past_days}
        />
      </div>
      <div className="setting-row">
        <Input
          field={{
            label: "Carriers to Exclude",
            type: "CUSTOM",
            value: "",
            isRequired: false,
          }}
          width={8}
          labelIcon={"info"}
          labelTitle={
            "Entered carriers will be excluded from the FIRE and Customer Sites data that will be synced to the Quickbase"
          }
          name="carrier_exclude_filter"
          onChange={(e) => null}
          className="carrier-exclude-wrapper"
          customInput={
            // <TagsInput
            //   value={settings.carrier_exclude_filter}
            //   onChange={handleChangeTags}
            //   inputProps={{
            //     className: "react-tagsinput-input carrier-exclude-term",
            //     placeholder: "Enter Carrier to Exclude",
            //   }}
            //   addOnBlur={true}
            // />
            null
          }
        />
      </div>
      <SquareButton
        onClick={onSave}
        content={"Save"}
        bsStyle={"primary"}
        className="qb-setting-save-btn"
        title={
          !partsUrlPassed || !customerUrlPassed
            ? "Please test the Webhook URL before saving settings"
            : ""
        }
        disabled={
          (editedURL.qb_customer_pipeline_url ||
            editedURL.qb_parts_pipeline_url) &&
          !customerUrlPassed &&
          !partsUrlPassed
        }
      />
      <ConfirmBox
        show={showFullSyncConfirm}
        title="Confirm Full Sync"
        message={
          "Are you sure you want to trigger the full sync of all CW customer sites and FIRE Data to Quickbase(No date filter will be applied during this)?"
        }
        onClose={toggleConfirmationBox}
        onSubmit={() => handleTriggerSync(true)}
        isLoading={false}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  triggerSync: (fullSync?: boolean) => dispatch(quickbaseSyncTrigger(fullSync)),
  testURL: (url: string) => dispatch(testQuickbaseURL(url)),
  addInfoMessage: (msg: string) => dispatch(addInfoMessage(msg)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  settingsCreateUpdate: (method: HTTPMethods, data?: IQuickbaseSettings) =>
    dispatch(fetchOrUpdateQuickbaseSettings(method, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(QuickbaseSettings);
