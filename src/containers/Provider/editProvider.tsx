import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import validator from 'validator';

import {
  createProvider,
  editProvider,
  PROVIDER_EDIT_FAILURE,
  PROVIDER_EDIT_SUCCESS,
  fetchTimezones,
} from '../../actions/provider';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';

import AppValidators from '../../utils/validator';
import './style.scss';

interface ICreateProviderState {
  provider: IProvider;
  isFormValid: boolean;
  show: boolean;
  error: {
    name: IFieldValidation;
    url: IFieldValidation;
    email: IFieldValidation;
    is_active: IFieldValidation;
    logo: IFieldValidation;
    timezone: IFieldValidation;
    address: {
      address_1: IFieldValidation;
      address_2: IFieldValidation;
      city: IFieldValidation;
      state: IFieldValidation;
      zip_code: IFieldValidation;
    };
    accounting_contact: {
      first_name: IFieldValidation;
      last_name: IFieldValidation;
      email: IFieldValidation;
      phone: IFieldValidation;
      website: IFieldValidation;
    };
    support_contact: IFieldValidation;
    http_protocol: IFieldValidation;
  };
  errorList?: any; //TODO
  isEdit: boolean;
  isPosting: boolean;
}

interface ICreateProviderProps extends ICommonProps {
  providerToEdit?: IProvider;
  provider: IProvider;
  createProvider: TCreateProvider;
  editProvider: TCreateProvider;
  show?: boolean;
  onClose: any;
  timezones: any;
  fetchTimezones: any;
}

class EditProvider extends React.Component<
  ICreateProviderProps,
  ICreateProviderState
> {
  constructor(props: ICreateProviderProps) {
    super(props);

    this.state = this.getEmptyState();
  }
  componentDidMount() {
    if(!this.props.timezones || (this.props.timezones && this.props.timezones.length === 0)){
      this.props.fetchTimezones();
    }
  }
  
  componentDidUpdate(prevProps: ICreateProviderProps) {
    const { show, providerToEdit } = this.props;
  
    if (show !== prevProps.show && providerToEdit) {
      this.setState({
        provider: providerToEdit,
        isEdit: true,
      });
      this.onToggleModal();
    } else if (show !== prevProps.show && !providerToEdit) {
      this.onToggleModal();
    }
  }  

  setDefaultState() {
    this.setState(this.getEmptyState());
  }

  getEmptyState = () => ({
    provider: {
      name: '',
      url: '',
      email: '',
      is_active: false,
      is_two_fa_enabled: false,
      timezone: "America/Los_Angeles",
      logo: '',
      address: {
        address_1: '',
        address_2: '',
        city: '',
        state: '',
        zip_code: '',
      },
      accounting_contact: {
        first_name: '',
        last_name: '',
        email: '',
        phone: '',
        website: '',
      },
      support_contact: '',
      http_protocol: 'http://',
    },
    isFormValid: false,
    show: false,
    error: {
      name: {
        errorState: "success",
        errorMessage: '',
      },
      url: {
        errorState: "success",
        errorMessage: '',
      },
      email: {
        errorState: "success",
        errorMessage: '',
      },
      is_active: {
        errorState: "success",
        errorMessage: '',
      },
      logo: {
        errorState: "success",
        errorMessage: '',
      },
      timezone: {
        errorState: "success",
        errorMessage: '',
      },
      address: {
        address_1: {
          errorState: "success",
          errorMessage: '',
        },
        address_2: {
          errorState: "success",
          errorMessage: '',
        },
        city: {
          errorState: "success",
          errorMessage: '',
        },
        state: {
          errorState: "success",
          errorMessage: '',
        },
        zip_code: {
          errorState: "success",
          errorMessage: '',
        },
      },
      accounting_contact: {
        first_name: {
          errorState: "success",
          errorMessage: '',
        },
        last_name: {
          errorState: "success",
          errorMessage: '',
        },
        email: {
          errorState: "success",
          errorMessage: '',
        },
        phone: {
          errorState: "success",
          errorMessage: '',
        },
        website: {
          errorState: "success",
          errorMessage: '',
        },
      },
      support_contact: {
        errorState: "success",
        errorMessage: '',
      },
      http_protocol: {
        errorState: "success",
        errorMessage: '',
      },
    },
    errorList: null,
    isEdit: false,
    isPosting: false,
  });

  setValidationErrors = errorList => {
    const newState: ICreateProviderState = cloneDeep(this.state);

    Object.keys(errorList).map(key => {
      if (key === 'accounting_contact' || key === 'address') {
        Object.keys(errorList[key]).map(childKey => {
          newState.error[key][childKey].errorState = "error";
          newState.error[key][childKey].errorMessage = errorList[key][childKey];
        });
      } else if (key !== 'detail') {
        newState.error[key].errorState = "error";
        newState.error[key].errorMessage = errorList[key];
      }
    });

    newState.isFormValid = false;
    this.setState(newState);
  };
  getTimezones = () => {
    const timezones = this.props.timezones
      ? this.props.timezones.map(t => ({
          value: t.timezone,
          label: t.display_name,
          disabled: false,
        }))
      : [];

    return timezones;
  };
  onToggleModal = () => {
    this.setState({ show: !this.state.show });
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.provider.name || this.state.provider.name.trim() === '') {
      error.name.errorState = "error";
      error.name.errorMessage = 'Name cannot be empty';

      isValid = false;
    } else if (
      this.state.provider.name &&
      !AppValidators.isValidName(this.state.provider.name)
    ) {
      error.name.errorState = "error";
      error.name.errorMessage =
        'Name can not start with number and Special charactor not allowed.';

      isValid = false;
    }

    if (!this.state.provider.email) {
      error.email.errorState = "error";
      error.email.errorMessage = 'Email cannot be empty';

      isValid = false;
    }

    if (!this.state.provider.address.city) {
      error.address.city.errorState = "error";
      error.address.city.errorMessage = 'City cannot be empty';

      isValid = false;
    }
    if (!this.state.provider.timezone) {
      error.timezone.errorState = "error";
      error.timezone.errorMessage = 'Please select timezone';

      isValid = false;
    }
    if (
      this.state.provider.email &&
      !validator.isEmail(this.state.provider.email)
    ) {
      error.email.errorState = "error";
      error.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }

    if (!this.state.provider.address.state) {
      error.address.state.errorState = "error";
      error.address.state.errorMessage = 'State cannot be empty';

      isValid = false;
    }

    if (!this.state.provider.address.zip_code) {
      error.address.zip_code.errorState = "error";
      error.address.zip_code.errorMessage = 'Zip code cannot be empty';

      isValid = false;
    }

    if (!this.state.provider.accounting_contact.first_name) {
      error.accounting_contact.first_name.errorState = "error";
      error.accounting_contact.first_name.errorMessage =
        'First name cannot be empty';

      isValid = false;
    }

    if (!this.state.provider.accounting_contact.last_name) {
      error.accounting_contact.last_name.errorState = "error";
      error.accounting_contact.last_name.errorMessage =
        'Last name cannot be empty';

      isValid = false;
    }

    if (!this.state.provider.accounting_contact.email) {
      error.accounting_contact.email.errorState = "error";
      error.accounting_contact.email.errorMessage = 'Email cannot be empty';

      isValid = false;
    }

    if (
      this.state.provider.accounting_contact.email &&
      !validator.isEmail(this.state.provider.accounting_contact.email)
    ) {
      error.accounting_contact.email.errorState = "error";
      error.accounting_contact.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }

    if (!this.state.provider.address.address_1) {
      error.address.address_1.errorState = "error";
      error.address.address_1.errorMessage = 'Address cannot be empty';

      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  handleChange = (event: any, parentName?: any) => {
    const newState = cloneDeep(this.state);

    if (!parentName) {
      newState.provider[event.target.name] = event.target.value;
    } else {
      newState.provider[parentName][event.target.name] = event.target.value;
    }

    this.setState(newState);
  };

  handleChange2FA = (event: any, parentName?: any) => {
    const newState = cloneDeep(this.state);

    newState.provider[event.target.name] = JSON.parse(event.target.value);

    this.setState(newState);
  };
  getBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.state.isPosting} />
        </div>
        <div
          className={`provider-details ${
            this.state.isPosting ? `loading` : ''
          }`}
        >
          <Input
            field={{
              label: 'Name',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.name,
            }}
            error={this.state.error.name}
            width={6}
            placeholder="Enter provider name"
            name="name"
            onChange={this.handleChange}
            className="provider-details__item provider-details__name"
          />
          <div className="field-section structured-url-wrapper">
            <div className=" structured-url-title">
              <span>URL</span>
            </div>
            <div className="only-show-url">
              <label>{this.state.provider.url}</label>
            </div>
          </div>
          <Input
            field={{
              label: 'Email',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.email,
            }}
            error={this.state.error.email}
            width={6}
            placeholder="Enter Email"
            name="email"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: 'Status',
              type: "RADIO",
              isRequired: true,
              value: this.state.provider.is_active ? 'Enable' : 'Disable',
              options: [
                { value: 'Enable', label: 'Enable' },
                { value: 'Disable', label: 'Disable' },
              ],
            }}
            error={this.state.error.is_active}
            width={6}
            placeholder=""
            name="is_active"
            onChange={event => {
              (event.target as any).value =
                (event.target as any).value === 'Enable' ? true : false;
              this.handleChange(event);
            }}
          />
          <Input
            field={{
              label: 'Two-Factor-Authentication',
              type: "RADIO",
              isRequired: true,
              value: this.state.provider.is_two_fa_enabled,
              options: [
                { value: true, label: 'Enable' },
                { value: false, label: 'Disable' },
              ],
            }}
            width={6}
            placeholder=""
            name="is_two_fa_enabled"
            onChange={event => {
              this.handleChange2FA(event);
            }}
          />
        </div>
        <div className="provider-address">
          <Input
            field={{
              label: 'Address 1',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.address.address_1,
            }}
            error={this.state.error.address.address_1}
            width={6}
            placeholder="Enter address 1"
            name="address_1"
            onChange={event => this.handleChange(event, 'address')}
          />

          <Input
            field={{
              label: 'Address 2',
              type: "TEXT",
              isRequired: false,
              value: this.state.provider.address.address_2,
            }}
            error={this.state.error.address.address_2}
            width={6}
            placeholder="Enter address 2"
            name="address_2"
            onChange={event => this.handleChange(event, 'address')}
          />
          <Input
            field={{
              label: 'City',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.address.city,
            }}
            error={this.state.error.address.city}
            width={6}
            placeholder="Enter city name"
            name="city"
            onChange={event => this.handleChange(event, 'address')}
          />

          <Input
            field={{
              label: 'State',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.address.state,
            }}
            error={this.state.error.address.state}
            width={6}
            placeholder="Enter state name"
            name="state"
            onChange={event => this.handleChange(event, 'address')}
          />
          <Input
            field={{
              label: 'Zip code',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.address.zip_code,
            }}
            error={this.state.error.address.zip_code}
            width={6}
            placeholder="Enter zip code"
            name="zip_code"
            onChange={event => this.handleChange(event, 'address')}
          />
          <Input
            field={{
              label: 'Timezone',
              type: "PICKLIST",
              isRequired: true,
              value: this.state.provider.timezone,
              options: this.getTimezones()
            }}
            error={this.state.error.timezone}
            width={6}
            placeholder="Select timezone"
            name="timezone"
            onChange={event => this.handleChange(event)}
          />
        </div>
        <div className="accounting-contact-row">
          <span className="font-weight-bold">Accounting Contact</span>
        </div>
        <div className="accounting-contact">
          <Input
            field={{
              label: 'First Name',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.accounting_contact.first_name,
            }}
            error={this.state.error.accounting_contact.first_name}
            width={6}
            placeholder="Enter first name"
            name="first_name"
            onChange={event => this.handleChange(event, 'accounting_contact')}
          />
          <Input
            field={{
              label: 'Last Name',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.accounting_contact.last_name,
            }}
            error={this.state.error.accounting_contact.last_name}
            width={6}
            placeholder="Enter last name"
            name="last_name"
            onChange={event => this.handleChange(event, 'accounting_contact')}
          />
          <Input
            field={{
              label: 'Email Address',
              type: "TEXT",
              isRequired: true,
              value: this.state.provider.accounting_contact.email,
            }}
            error={this.state.error.accounting_contact.email}
            width={6}
            placeholder="Enter email address"
            name="email"
            onChange={event => this.handleChange(event, 'accounting_contact')}
          />
          <Input
            field={{
              label: 'Phone',
              type: "TEXT",
              isRequired: false,
              value: this.state.provider.accounting_contact.phone,
            }}
            error={this.state.error.accounting_contact.phone}
            width={6}
            placeholder="Enter Phone Number"
            name="phone"
            onChange={event => this.handleChange(event, 'accounting_contact')}
          />
          <Input
            field={{
              label: 'Website',
              type: "TEXT",
              isRequired: false,
              value: this.state.provider.accounting_contact.website,
            }}
            error={this.state.error.accounting_contact.website}
            width={6}
            placeholder="Enter Website url"
            name="website"
            onChange={event => this.handleChange(event, 'accounting_contact')}
          />
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`user-form__actions
            ${this.state.isPosting ? `loading` : ''}`}
      >
        <SquareButton
          onClick={e => {
            this.onToggleModal();
            this.setDefaultState();
          }}
          content={<span>Cancel</span>}
          bsStyle={"default"}
          className="modal__cancel-btn"
        />
        <SquareButton
          onClick={this.onSubmit}
          content={<span>Save Changes</span>}
          bsStyle={"primary"}
          className="modal__save-btn"
        />
      </div>
    );
  };

  onSubmit = () => {
    if (this.isValid()) {
      this.setState({ isPosting: true });
      const provider = this.state.provider;
      this.props.editProvider(provider).then(action => {
        const currentState = this.state.provider;
        this.setState({ provider: currentState });

        if (action.type === PROVIDER_EDIT_SUCCESS) {
          this.props.history.push('/providers');
          this.setDefaultState();
        }
        if (action.type === PROVIDER_EDIT_FAILURE) {
          this.setState({
            show: true,
            errorList: action.errorList.data,
            isPosting: false,
          });
          this.setValidationErrors(this.state.errorList);
        }
      });
    }
  };

  render() {
    const title = 'Edit  Provider';

    return (
      <div>
        <div>
          <ModalBase
            show={this.state.show}
            onClose={this.onToggleModal}
            titleElement={title}
            bodyElement={this.getBody()}
            footerElement={this.getFooter()}
            className="create-provider-modal"
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  provider:
    state.provider && state.provider.provider ? state.provider.provider : null,
    timezones: state.provider.timezones,
});

const mapDispatchToProps = (dispatch: any) => ({
  createProvider: (provider: IProvider) => dispatch(createProvider(provider)),
  editProvider: (provider: IProvider) => dispatch(editProvider(provider)),
  fetchTimezones: () => dispatch(fetchTimezones()),
});

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(EditProvider)
);
