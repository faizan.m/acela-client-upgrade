import { debounce } from "lodash";
import React from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import SquareButton from "../../components/Button/button";
import EditButton from "../../components/Button/editButton";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import Table from "../../components/Table/table";
import { allowPermission } from "../../utils/permissions";
import UserDetails from "../User/userDetails";
import UserForm from "../User/userForm";
import EditProvider from "./editProvider";

import {
  ACTIVATE_USER_EMAIL_FAILURE,
  ACTIVATE_USER_EMAIL_SUCCESS,
  sendActivationEmail,
} from "../../actions/email";
import { fetchProvider } from "../../actions/provider";
import {
  addProviderAdminUser,
  deleteProviderAdminUser,
  editProviderAdminUser,
  fetchProviderAdmins,
  PROVIDER_ADMIN_ADD_FAILURE,
  PROVIDER_ADMIN_ADD_SUCCESS,
  PROVIDER_ADMIN_DELETE_SUCCESS,
  PROVIDER_ADMIN_EDIT_FAILURE,
  PROVIDER_ADMIN_EDIT_SUCCESS,
} from "../../actions/providerAdminUser";
import { fetchUserTypes } from "../../actions/userType";

import { fetchUserForCustomer } from "../../actions/profile";
import DeleteButton from "../../components/Button/deleteButton";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";
import { phoneNumberInFormat } from "../../utils/CalendarUtil";
import FeatureAccess from "./featureAccess";
import "../Customer/style.scss";
import "./style.scss";

interface IProviderAdminUserTableRow {
  index: number;
  id: string;
  name: string;
  email: string;
  status: string;
  can_edit: boolean;
  can_delete: boolean;
  is_two_fa_enabled: boolean;
}

interface IProviderAdminUserListingProps extends ICommonProps {
  provider: IProvider;
  providerAdminUsers: IProviderAdminPaginated;
  fetchProviderAdmins: TFetchProviderAdminUserList;
  fetchProviderWithId: TFetchProvider;
  fetchUserTypes: TFetchUserTypes;
  sendActivationEmail: TPostEmailActivation;
  userTypes: IUserTypeRole[];
  addProviderAdminUser: (
    providerId: number,
    providerAdminUser: ISuperUser
  ) => Promise<any>;
  editProviderAdminUser: (
    providerId: number,
    providerAdminUser: ISuperUser
  ) => Promise<any>;
  isProviderFetching: boolean;
  isFetchingAdmins: boolean;
  isAction: boolean;
  deleteProviderAdminUser: any;
  fetchUserForCustomer: (id: number) => Promise<any>;
}

interface IProviderAdminUserListingState {
  isProviderEditModal: boolean;
  isDetailsModalOpen: boolean;
  isCreateEditModalOpen: boolean;
  rows: IProviderAdminUserTableRow[];
  selectedRows: number[];
  currentProviderDetails: any;
  errorList?: any;
  user: ISuperUser;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  providerAdmins: ISuperUser[];
  isPostingUser: boolean;
  emailActivatingIndex: number;
  isopenConfirm: boolean;
  id: string;
  reset: boolean;
}

class ProviderDetails extends React.Component<
  IProviderAdminUserListingProps,
  IProviderAdminUserListingState
> {
  private debouncedFetch;
  constructor(props: IProviderAdminUserListingProps) {
    super(props);

    this.state = {
      isCreateEditModalOpen: false,
      isProviderEditModal: false,
      isDetailsModalOpen: false,
      rows: [],
      selectedRows: [],
      currentProviderDetails: null,
      user: null,
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      providerAdmins: [],
      isPostingUser: false,
      emailActivatingIndex: null,
      isopenConfirm: false,
      id: "",
      reset: false,
    };

    // set page to 1 for
    // filtering
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  componentDidMount() {
    const providerId = this.props.match.params.providerId;

    this.props.fetchProviderWithId(providerId);
    this.props.fetchUserTypes();
  }

  componentDidUpdate(prevProps: IProviderAdminUserListingProps) {
    if (
      this.props.providerAdminUsers &&
      prevProps.providerAdminUsers !== this.props.providerAdminUsers
    ) {
      this.setRows(this.props);
    }
    if (this.props.provider && prevProps.provider !== this.props.provider) {
      this.setState({
        currentProviderDetails: this.props.provider,
      });
    }
  }

  setRows = (nextProps: IProviderAdminUserListingProps) => {
    const providerAdminResponse = nextProps.providerAdminUsers;
    const providerAdmins = providerAdminResponse.results;
    const rows: IProviderAdminUserTableRow[] = providerAdmins.map(
      (providerAdminUser, index) => ({
        id: providerAdminUser.id,
        name: `${providerAdminUser.first_name} ${providerAdminUser.last_name}`,
        email: providerAdminUser.email,
        can_edit: providerAdminUser.can_edit,
        role: providerAdminUser.role_display_name,
        can_delete: providerAdminUser.can_delete,
        is_two_fa_enabled: providerAdminUser.is_two_fa_enabled,
        status: providerAdminUser.is_active ? "Enabled" : "Disabled",
        index,
        action: this.getActivationButton(providerAdminUser),
      })
    );
    
    this.setState((prevState) => ({
      reset: false,
      rows,
      providerAdmins,
      pagination: {
        ...prevState.pagination,
        totalRows: providerAdminResponse.count,
        currentPage: providerAdminResponse.links.page_number - 1,
        totalPages: Math.ceil(
          providerAdminResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onRowsToggle = (selectedRows: number[]) => {
    this.setState({ selectedRows });
  };

  onEditRowClick = (superUserIndex: number, event: any) => {
    event.stopPropagation();

    this.setState({
      isCreateEditModalOpen: true,
      user: this.state.providerAdmins[superUserIndex],
    });
  };

  toggleCreateEditModal = () => {
    this.setState((prevState) => ({
      isCreateEditModalOpen: !prevState.isCreateEditModalOpen,
      user: null,
    }));
  };

  toggleProviderEditModal = () => {
    this.setState((prevState) => ({
      isProviderEditModal: !prevState.isProviderEditModal,
    }));
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    const providerId = this.props.match.params.providerId;
    this.props.fetchProviderAdmins(providerId, newParams);
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  fetchUsers = (num?: number) => {
    const search = this.state.pagination.params.search;
    const page = num ? num + 1 : 0;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            page,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;
      newParams.page = 0;

      this.setState((prevState) => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: page ? page : 1,
    });
  };

  goBack = () => {
    this.props.history.push(`/providers`);
  };

  renderProvideDetails = () => {
    const providerDetails = this.state.currentProviderDetails;

    return (
      <div>
        <h3 className="d-pointer" onClick={this.goBack}>
          &lt; Back to Provider Listing
        </h3>
        <div className="loader">
          <Spinner show={this.props.isProviderFetching} />
        </div>
        <div
          className={`customer-user__details ${
            this.props.isProviderFetching ? "loading" : ""
          }`}
        >
          <div className="customer-user__details-header">
            <h5>Provider Details</h5>
            {allowPermission("edit_provider") && (
              <SquareButton
                content="Edit"
                onClick={this.toggleProviderEditModal}
                bsStyle={"primary"}
              />
            )}
          </div>
          {providerDetails && (
            <div className="customer-user__details-body">
              <div className="provider_section">
                <div className="customer-user__details-field">
                  <label>Name</label>
                  <label>{`${providerDetails.name}`}</label>
                </div>
                <div className="customer-user__details-field">
                  <label>URL</label>
                  <label>{providerDetails.url}</label>
                </div>
                <div className="customer-user__details-field">
                  <label>Logo</label>
                  <img className="logo-img" src={providerDetails.logo} alt="" />
                </div>
                <div
                  className={
                    `customer-user__details-field ` +
                    `customer-user__details-field--address`
                  }
                >
                  <label>Address</label>
                  <label>{`${providerDetails.address.address_1} ${providerDetails.address.address_2} ${providerDetails.address.city} ${providerDetails.address.state}`}</label>
                </div>
                <div className="customer-user__details-field">
                  <label>Status</label>
                  <label>
                    {providerDetails.is_active ? "Enabled" : "Disabled"}
                  </label>
                </div>{" "}
                <div className="customer-user__details-field">
                  <label>Two-Factor-Authentication</label>
                  <label>
                    {providerDetails.is_two_fa_enabled ? "Enabled" : "Disabled"}
                  </label>
                </div>
                <div className="customer-user__details-field">
                  <label>Timezone</label>
                  <label>
                    {providerDetails.timezone ? providerDetails.timezone : "-"}
                  </label>
                </div>
              </div>
              <div className="accounting-contact-section__title">
                <label>Accounting Contact</label>
              </div>
              <div className="accounting-contact-section">
                <div className="customer-user__details-field">
                  <label>Name</label>
                  <label>{providerDetails.accounting_contact.first_name}</label>
                </div>
                <div className="customer-user__details-field">
                  <label>Email Address</label>
                  <label>{providerDetails.accounting_contact.email}</label>
                </div>
                <div className="customer-user__details-field">
                  <label>Phone Number </label>
                  <label>
                    {phoneNumberInFormat(
                      "",
                      providerDetails.accounting_contact.phone
                    )}
                  </label>
                </div>
                <div className="customer-user__details-field" />
                <div className="customer-user__details-field">
                  <label>Website </label>
                  <label>{providerDetails.accounting_contact.website}</label>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  };

  renderTopBar = () => {
    return (
      <div className="superusers-listing__actions header-panel">
        <Input
          field={{
            value: this.state.pagination.params.search,
            label: "",
            type: "SEARCH",
          }}
          width={6}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search"
          className="search"
        />
        {allowPermission("add_admin_user") && (
          <SquareButton
            onClick={this.toggleCreateEditModal}
            content="+ Add Admin Users"
            bsStyle={"primary"}
            className="superusers-listing__actions-create"
          />
        )}
      </div>
    );
  };

  getProviderOwnerId = () => {
    const userTypes = this.props.userTypes;
    if (userTypes) {
      const providerType = userTypes.find(
        (userType) => userType.user_type === "provider"
      );

      return providerType.roles.find(
        (providerRole) => providerRole.role_name === "owner"
      ).id;
    } else {
      return null;
    }
  };

  getProviderUserRoles = () => {
    const userTypes = this.props.userTypes;
    if (userTypes) {
      const providerUserType = userTypes.find(
        (userType) => userType.user_type === "provider"
      );

      return providerUserType.roles.map((role) => ({
        value: role.id,
        label: role.display_name,
      }));
    }

    return [];
  };

  onCreateProviderAdminUser = (providerAdminUser: ISuperUser) => {
    this.setState({ isPostingUser: true });
    const providerId = this.props.match.params.providerId;
    this.props
      .addProviderAdminUser(providerId, providerAdminUser)
      .then((action) => {
        if (action.type === PROVIDER_ADMIN_ADD_SUCCESS) {
          this.setState({
            isCreateEditModalOpen: false,
            isPostingUser: false,
            reset: true,
          });
          this.debouncedFetch({ page: 1 });
        }
        if (action.type === PROVIDER_ADMIN_ADD_FAILURE) {
          this.setState({
            isCreateEditModalOpen: true,
            errorList: action.errorList.data,
            isPostingUser: false,
          });
        }
      });
  };

  onEditeProviderAdminUser = (providerAdminUser: ISuperUser) => {
    this.setState({ isPostingUser: true });
    const providerId = this.props.match.params.providerId;
    this.props
      .editProviderAdminUser(providerId, providerAdminUser)
      .then((action) => {
        if (action.type === PROVIDER_ADMIN_EDIT_SUCCESS) {
          this.debouncedFetch();
          this.setState({
            isCreateEditModalOpen: false,
            isPostingUser: false,
          });
        }
        if (action.type === PROVIDER_ADMIN_EDIT_FAILURE) {
          this.setState({
            isCreateEditModalOpen: true,
            errorList: action.errorList.data,
            isPostingUser: false,
          });
        }
      });
  };

  onRowClick = (rowInfo) => {
    const providerAdminUserIndex = rowInfo.original.index;

    this.setState({
      isDetailsModalOpen: true,
      user: this.state.providerAdmins[providerAdminUserIndex],
    });
  };

  toggleDetailsModal = () => {
    this.setState((prevState) => ({
      isDetailsModalOpen: !prevState.isDetailsModalOpen,
    }));
  };

  getActivationButton = (user) => {
    let actionType = "showActive";
    if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 0 &&
      user.activation_mails_sent_count < 6
    ) {
      actionType = "showResend";
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 5
    ) {
      actionType = "showResendStop";
    }

    return actionType;
  };

  onclickSendMail = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState({ emailActivatingIndex: userIndex });
    this.props
      .sendActivationEmail(this.state.providerAdmins[userIndex].id)
      .then((action) => {
        if (action.type === ACTIVATE_USER_EMAIL_SUCCESS) {
          this.setState({ emailActivatingIndex: null });
        }
        if (action.type === ACTIVATE_USER_EMAIL_FAILURE) {
          this.setState({ emailActivatingIndex: null });
        }
      });
  };

  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    const providerId = this.props.match.params.providerId;

    this.props
      .deleteProviderAdminUser(providerId, this.state.id)
      .then((action) => {
        if (action.type === PROVIDER_ADMIN_DELETE_SUCCESS) {
          this.fetchUsers();
          this.toggleConfirmOpen();
        }
      });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: "",
    });
  };

  render() {
    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: this.onRowsToggle,
    };
    const providerOwnerId = this.getProviderOwnerId();

    const columns: any = [
      {
        accessor: "name",
        Header: "Name",
        id: "first_name",
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      { accessor: "email", Header: "Email", id: "email" },
      {
        accessor: "status",
        id: "is_active",
        Header: "Status",
        Cell: (status) => (
          <div className={`status status--${status.value}`}>{status.value}</div>
        ),
      },
      {
        accessor: "index",
        Header: "Actions",
        sortable: false,
        Cell: (cell) =>
          cell.original.can_edit && (
            <div>
              {" "}
              <EditButton onClick={(e) => this.onEditRowClick(cell.value, e)} />
              <DeleteButton onClick={(e) => this.onDeleteRowClick(cell, e)} />
            </div>
          ),
      },
      {
        accessor: "action",
        Header: "User Activation",
        sortable: false,
        Cell: (action) => (
          <div>
            {action.value === "showResend" &&
              action.index !== this.state.emailActivatingIndex && (
                <SquareButton
                  content="Resend e-Mail"
                  onClick={(e) => this.onclickSendMail(action.index, e)}
                  bsStyle={"primary"}
                  className="activate-btn"
                />
              )}
            {action.value === "showResend" &&
              action.index === this.state.emailActivatingIndex && (
                <Spinner show={true} className="email__spinner" />
              )}
            {action.value === "showResendStop" && (
              <div>No more Emails can sent</div>
            )}
            {action.value === "showActive" && "Activated"}
          </div>
        ),
      },
    ];
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };

    return (
      <div className="customer-user provider-user-details">
        {this.renderProvideDetails()}
        <div className="providers-listing superusers-listing">
          <UserForm
            show={this.state.isCreateEditModalOpen}
            onClose={this.toggleCreateEditModal}
            onSubmit={
              this.state.user
                ? this.onEditeProviderAdminUser
                : this.onCreateProviderAdminUser
            }
            userRoles={this.getProviderUserRoles()}
            userType="Admin"
            isProvideAdmin={true}
            userRoleInitial={{
              disabled: true,
              initialValue: providerOwnerId,
            }}
            user={this.state.user}
            errorList={this.state.errorList}
            isLoading={this.state.isPostingUser}
            fetchUserForCustomer={this.props.fetchUserForCustomer}
          />
          <div className="customer-user__listing">
            <div className="customer-user__listing-header">
              <h5>Provider Admin Users</h5>
            </div>
            <div className="loader">
              <Spinner show={this.props.isFetchingAdmins} />
            </div>
            <Table
              columns={columns}
              rows={this.state.rows}
              onRowClick={this.onRowClick}
              rowSelection={rowSelectionProps}
              customTopBar={this.renderTopBar()}
              className={`superusers-listing__table ${
                this.props.isFetchingAdmins ? "loading" : ""
              }`}
              manualProps={manualProps}
              loading={this.props.isFetchingAdmins}
            />
          </div>
          <FeatureAccess
            providerId={Number(this.props.match.params.providerId)}
          />
          <UserDetails
            show={this.state.isDetailsModalOpen}
            user={this.state.user}
            onClose={this.toggleDetailsModal}
            title="Admin User Details"
            fetchUserForCustomer={this.props.fetchUserForCustomer}
          />
          <EditProvider
            show={this.state.isProviderEditModal}
            onClose={this.toggleProviderEditModal}
            providerToEdit={this.state.currentProviderDetails}
          />
          <ConfirmBox
            show={this.state.isopenConfirm}
            onClose={this.toggleConfirmOpen}
            onSubmit={this.onClickConfirm}
            isLoading={this.props.isAction}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  providerAdminUsers: state.providerAdminUser.providerAdminUsers,
  provider: state.provider.provider,
  userTypes: state.userType.userTypes,
  isProviderFetching: state.provider.isProviderFetching,
  isFetchingAdmins: state.providerAdminUser.isFetchingAdmins,
  isAction: state.providerAdminUser.isAction,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProviderAdmins: (providerId: number, params?: IServerPaginationParams) =>
    dispatch(fetchProviderAdmins(providerId, params)),
  fetchProviderWithId: (id: number) => dispatch(fetchProvider(id)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
  addProviderAdminUser: (providerId: number, providerAdminUser: ISuperUser) =>
    dispatch(addProviderAdminUser(providerId, providerAdminUser)),
  editProviderAdminUser: (providerId: number, providerAdminUser: ISuperUser) =>
    dispatch(editProviderAdminUser(providerId, providerAdminUser)),
  sendActivationEmail: (userId: string) =>
    dispatch(sendActivationEmail(userId)),
  deleteProviderAdminUser: (providerId: number, userId: any) =>
    dispatch(deleteProviderAdminUser(providerId, userId)),
  fetchUserForCustomer: (id: number) => dispatch(fetchUserForCustomer(id)),
});

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(ProviderDetails)
);
