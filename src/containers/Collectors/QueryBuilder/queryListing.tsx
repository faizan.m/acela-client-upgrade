import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import  ControlledEditor  from '@monaco-editor/react'; // New Component
import { cloneDeep, debounce } from 'lodash';
import SquareButton from '../../../components/Button/button';
import ConfirmBox from '../../../components/ConfirmBox/ConfirmBox';
import Input from '../../../components/Input/input';
import ModalBase from '../../../components/ModalBase/modalBase';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import { fromISOStringToDateTimeString } from '../../../utils/CalendarUtil';
import { addSuccessMessage } from '../../../actions/appState';
import DeleteButton from '../../../components/Button/deleteButton';
import { fetchQueryList, fetchQuery, saveQuery, deleteQuery, FETCH_QUERY_SUCCESS } from '../../../actions/collector';
import { commonFunctions } from '../../../utils/commonFunctions';
import './style.scss';

interface IQueryProps extends ICommonProps {
  fetchQueryList: any;
  fetchQuery: any;
  saveQuery: any;
  deleteQuery: any;
  addSuccessMessage: any;
  query: ICollectorQuery;
  queryList: any;
  isFetchingQueryList: boolean;
  isFetchingQuery: boolean;
  isFetching: boolean;
  customerId: any;
  loggenInUser: any;
  user: any;
}

interface IQueryState {
  query: ICollectorQuery;
  open: boolean;
  error: {
    payload: IFieldValidation;
    description: IFieldValidation;
  };
  isopenConfirm: boolean;
  queryList?: any[];
  rows: ICollectorQuery[];
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
  reset: boolean;
  id?: any;
  isOpenDetails: boolean;
}

class QueryList extends React.Component<IQueryProps, IQueryState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  private debouncedFetch;

  constructor(props: IQueryProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  getEmptyState = () => ({
    querList: [],
    rows: [],
    open: false,
    download: true,
    copied: false,
    query: {
      id: 0,
      payload: '',
      status: '',
      updated_on: '',
      created_on: '',
    },
    error: {
      payload: { ...QueryList.emptyErrorState },
      description: { ...QueryList.emptyErrorState },
    },
    isOpenDetails: false,
    isopenConfirm: false,
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    reset: false,
    isFilterModalOpen: false,
    filters: {
      type: [],
      option: [],
    },
  });

  componentDidMount() {
    if (this.props.queryList.length === 0 && this.props.customerId && this.props.match.params.collectorId) {
      this.props.fetchQueryList(this.props.customerId, this.props.match.params.collectorId);
    }
  }

  componentDidUpdate(prevProps: IQueryProps) {
    const { queryList, customerId, match } = this.props;
    
    if (queryList && queryList !== prevProps.queryList) {
      this.setRows(this.props);
    }
  
    if (customerId && customerId !== prevProps.customerId) {
      this.props.fetchQueryList(customerId, match.params.collectorId);
    }
  }
  

  // Server side searching, sorting, ordering
  fetchData = (params?: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));
    if (this.props.customerId && this.props.match.params.collectorId) {
      this.props.fetchQueryList(this.props.customerId, this.props.match.params.collectorId, newParams);
    }
  };

  setRows = (nextProps: IQueryProps) => {
    const customersResponse = nextProps.queryList;
    const queryList: any[] = customersResponse.results;
    const rows: any[] =
      queryList &&
      queryList.map((query, index) => ({
        collector: query.collector,
        description: query.description,
        status: query.status,
        created_on: query.created_on,
        updated_on: query.updated_on,
        id: query.id,
        index,
      }));

    this.setState(prevState => ({
      reset: false,
      rows,
      queryList,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onRowClick = rowInfo => {
    event.stopPropagation();
    // tslint:disable-next-line: max-line-length
    this.setState({ open: true, id: rowInfo.original.id });
    this.props.fetchQuery(this.props.customerId, this.props.match.params.collectorId, rowInfo.original.id)
      .then(action => {
        if (action.type === FETCH_QUERY_SUCCESS) {
          const newState = cloneDeep(this.state);
          (newState.query.id as any) = action.response.id;
          (newState.query.status as any) = action.response.status;
          (newState.query.collector as any) = action.response.collector;
          (newState.query.description as any) = action.response.description;
          (newState.query.result as any) = action.response.result;
          (newState.query.updated_on as any) = action.response.updated_on;
          (newState.query.created_on as any) = action.response.created_on;
          (newState.query.payload as any) = action.response.payload.query;
          this.setState(newState);
        }
      });
  };

  onDeleteRowClick(original: any, e: any): any {
    if (e) {
      e.stopPropagation();
    }
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: original.original.id,
    });
  }

  onClickConfirm = () => {
    this.props.deleteQuery(this.props.customerId, this.props.match.params.collectorId, this.state.id).then(action => {
      if (action.type === FETCH_QUERY_SUCCESS) {
        this.setState({
          reset: true,
        });
        this.debouncedFetch({
          page: 1,
        });
        this.toggleConfirmOpen();
      }
    });
  };

  toggleConfirmOpen = () => {
    this.setState({
      isopenConfirm: !this.state.isopenConfirm,
      id: '',
    });
  };

  handleChange = e => {
    const newState = cloneDeep(this.state);
    (newState.query[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = e;
    (newState.query as any) = this.getEmptyState().query;
    (newState.error as any) = this.getEmptyState().error;
    (newState.isopenConfirm as any) = false;
    this.setState(newState);
  };

  validateForm() {
    const error: any = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.query.payload || this.state.query.payload.trim() === '') {
      error.payload.errorState = "error";
      error.payload.errorMessage = 'Please enter query';
      isValid = false;
    }
    if (!this.state.query.description || this.state.query.description.trim() === '') {
      error.description.errorState = "error";
      error.description.errorMessage = 'Please enter description';
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }
  onSaveClick = () => {
    if (this.validateForm()) {
      const query = this.state.query.payload;
      const description = this.state.query.description;
      const collectorId = this.props.match.params.collectorId;
      const data = {
        "payload": { "query": query },
        description
      };
      this.props.saveQuery(this.props.customerId, collectorId, data).then(action => {
        if (action.type === FETCH_QUERY_SUCCESS) {
          this.setState({reset: true})
          this.debouncedFetch({page:1});
          this.clearPopUp();
        }
        if (action.type === 'UPDATE_CUS_COLLTS_FAILURE') {
          this.setValidationErrors(action.errorList.data);
        }
      });
    }
  };

  setValidationErrors = errorList => {
    const newState: IQueryState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };

  onAddClick = () => {
    const newState = cloneDeep(this.state);
    (newState.open as boolean) = true;
    this.setState(newState);
  };

  renderTopBar = () => {
    return (
      <div className={'configuration-management__table-top '}>
        <div className="query__table-actions header-panel">
          <div>
            <SquareButton
              onClick={e => this.props.history.push('/collectors')}
              content={'Back to collector list'}
              bsStyle={"default"}
              className="add-query"
            />
          </div>
          <div>

            <SquareButton
              onClick={e => this.fetchData()}
              content={'Reload'}
              bsStyle={"primary"}
              className="add-query"
              disabled={
                _.get(this.props.loggenInUser, 'type') === 'provider' &&
                  !this.props.customerId
                  ? true
                  : false
              }
            />
            <SquareButton
              onClick={this.onAddClick}
              content={'+ Add Query'}
              bsStyle={"primary"}
              className="add-query"
              disabled={
                _.get(this.props.loggenInUser, 'type') === 'provider' &&
                  !this.props.customerId
                  ? true
                  : false
              }
            />
          </div>

        </div>
      </div>
    );
  };

  handleEditorDidMount = (_valueGetter, editor) => {
    editor.focus();
    editor.getAction('actions.find').run();
  };

  render() {
    const columns: ITableColumn[] = [
      {
        accessor: 'id',
        Header: 'ID',
        width: 80,
        id: 'id',
        sortable: false,
        Cell: c => <div>{c.value}</div>,
      },
      {
        accessor: 'status',
        Header: 'Status',
        id: 'status',
        width: 65,
        sortable: false,
        Cell: c => <div className="text-capitalize">
          {c.value === 'Completed' && (
            <img
              src={`/assets/icons/active-right.svg`}
              className="status-svg-images"
              alt=""
              title={'Completed'}
            />
          )}
          {c.value === 'Pending' && (
            <img
              src={`/assets/icons/clock.svg`}
              alt=""
              className="status-svg-images"
              title={`Pending`}
            />
          )}
          {c.value === 'Submitted' && (
            <img
              src={`/assets/icons/tick-gray.svg`}
              alt=""
              className="status-svg-images"
              title={`Submitted`}
            />
          )}
        </div>,
      },
      {
        accessor: 'description',
        Header: 'Description',
        id: 'description',
        sortable: false,
        Cell: c => <div>{c.value ? c.value : '-'}</div>,
      },
      {
        accessor: 'created_on',
        Header: 'Created On',
        id: 'created_on',
        sortable: false,
        Cell: c => (
          <div>{c.value ? fromISOStringToDateTimeString(c.value) : '-'}</div>
        ),
      },
      {
        accessor: 'id',
        Header: 'Actions',
        width: 120,
        sortable: false,
        Cell: cell => (
          <div>
            <DeleteButton
              title="Delete query" onClick={e => this.onDeleteRowClick(cell, e)} />
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: 'id',
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };
    const options = {
      selectOnLineNumbers: false,
      readOnly: true,
    };

    return (
      <div className="customer-query-listing-parent">
        <h3>{this.state.queryList && this.state.queryList.length > 0 ? `${this.state.queryList[0].collector} : ` : ''}  Collector Query List</h3>

        <div className="loader">
          <Spinner show={this.props.isFetchingQueryList} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetching ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingQueryList}
        />
        <ConfirmBox
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.props.isFetchingQuery}
        />
        <ModalBase
          show={this.state.open}
          onClose={() => this.clearPopUp(!this.state.open)}

          titleElement={` ${
            this.state.query.id ? `View Query - (${this.state.query.collector})` : 'Add Query'
            }`}
          bodyElement={
            <div className="view-query-pop-up body">
              <div className="loader">
                <Spinner
                  show={
                    this.props.isFetchingQuery
                  }
                />
              </div>
              <Input
                field={{
                  label: 'Query',
                  type: "TEXTAREA",
                  value: this.state.query.payload,
                  isRequired: true,
                }}
                width={12}
                name="payload"
                onChange={e => this.handleChange(e)}
                error={this.state.error.payload}
                placeholder={`Enter query`}
              />
              <Input
                field={{
                  label: 'Description',
                  type: "TEXTAREA",
                  value: this.state.query.description,
                  isRequired: true,
                }}
                width={12}
                name="description"
                onChange={e => this.handleChange(e)}
                error={this.state.error.description}
                placeholder={`Enter description`}
              />
              {
                this.state.query.id !== 0 &&
                <>
                  <div className="col-md-12 details-section">
                    <div className="body-field">
                      <label>Status</label>
                      <label>
                        {this.state.query.status ? this.state.query.status : '-'}
                      </label>
                    </div>
                    <div className="body-field">
                      <label>Created On</label>
                      <label>
                        {this.state.query.status ? fromISOStringToDateTimeString(this.state.query.created_on) : '-'}
                      </label>
                    </div>
                    <div className="body-field">
                      <label>Updated On</label>
                      <label>
                        {this.state.query.status ? fromISOStringToDateTimeString(this.state.query.updated_on) : '-'}
                      </label>
                    </div>
                  </div>
                  <div className="body-field">
                    <label className="created-on">Result</label>
                  </div>
                  <div className="result-text">
                    <ControlledEditor
                      height="400px"
                      width="805px"
                      value={JSON.stringify(this.state.query.result, null, '  ')}
                      onChange={a => null}
                      language="json"
                      options={options}
                      // editorDidMount={this.handleEditorDidMount}
                      theme="dark"
                    />
                  </div>
                </>
              }
            </div>
          }
          footerElement={
            <div className="col-md-12">
              {
                !this.state.query.id &&
                <SquareButton
                  content={'Save'}
                  bsStyle={"primary"}
                  onClick={e => this.onSaveClick()}
                  className=""
                />
              }
              <SquareButton
                content={'Close'}
                bsStyle={"default"}
                onClick={e => this.clearPopUp(!this.state.open)}
                className=""
              />

            </div>
          }
          className="add-edit-query"
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  queryList: state.collector.queryList,
  query: state.collector.query,
  isFetchingQueryList: state.collector.isFetchingQueryList,
  isFetchingQuery: state.collector.isFetchingQuery,
  isFetching: state.collector.isFetching,
  customerId: state.customer.customerId,
  loggenInUser: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchQueryList: (customerId: any, collectorId: any, params?: IServerPaginationParams) => dispatch(fetchQueryList(customerId, collectorId, params)),
  saveQuery: (customerId: any, collectorId: any, data: any) => dispatch(saveQuery(customerId, collectorId, data)),
  fetchQuery: (customerId: any, collectorId: any, queryId: any) => dispatch(fetchQuery(customerId, collectorId, queryId)),
  deleteQuery: (customerId: any, collectorId: any, queryId: any) => dispatch(deleteQuery(customerId, collectorId, queryId)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(QueryList);
