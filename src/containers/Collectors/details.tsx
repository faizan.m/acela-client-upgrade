import React from 'react';
import _, { isEqual } from 'lodash';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { fromISOStringToDateTimeString } from '../../utils/CalendarUtil';
import Input from '../../components/Input/input';
import ToggleButton from '../../components/ToggleButton';
import { exportCSVFile } from '../../utils/download';

interface ICollectorDetailsProps extends ICommonProps {
  show: boolean;
  onClose: (e: any) => void;
  onSave: (collector, manufacturer, serviceType) => void;
  collector: any;
  serviceTypes: any,
  manufacturers: any,
  isFetching: boolean;
  user: any;
}

interface ICollectorDetailsState {
  openSolarwind: boolean;
  openService: boolean;
  manufacturer: any,
  serviceType: any,
  integrations: any;
  submitted: boolean;
  error: {
    manufacturer: IFieldValidation;
    serviceType: IFieldValidation;
  }
  openServices: boolean;
  openIntegrations: boolean;
}

export default class CollectorDetails extends React.Component<
  ICollectorDetailsProps,
  ICollectorDetailsState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  props: any;

  constructor(props: ICollectorDetailsProps) {
    super(props);
    this.state =
      this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    openSolarwind: true,
    openService: false,
    manufacturer: null,
    serviceType: null,
    error: {
      manufacturer: {
        ...CollectorDetails.emptyErrorState,
      },
      serviceType: {
        ...CollectorDetails.emptyErrorState,
      },
    },
    integrations: null,
    submitted: false,
    openServices: true,
    openIntegrations: true
  });
  
  componentDidUpdate(prevProps: ICollectorDetailsProps) {
    const { collector } = this.props;
  
    if (collector && prevProps.collector !== collector) {
      this.setState({
        integrations: collector.integrations,
      });
    }
  }
  

  onClose = (event: any) => {
    const integrations = this.getEmptyState().integrations;
    this.setState(integrations, () => {
      this.props.onClose(event);
    });
  };
  onSave = () => {
    this.props.onSave(this.props.collector, this.state.integrations);
  };

  manufacturers = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(c => ({
        value: c.id,
        label: c.label,
      }))
      : [];

    return manufacturers;
  };
  changeMappingObject = (mappingObject, integration, newMF, vendor) => {
    const manufacturer = isNaN(newMF) ? null : parseInt(newMF);
    const serviceTypeData = {
      "solarwinds_vendor": vendor,
      "cw_mnf_id": manufacturer,
      "cw_mnf_name": this.getManufaturerName(manufacturer),
      "queries": this.getSolarwindsVendorsQueries().filter(m => m.label === vendor)[0].value,
    }
    mappingObject[integration].meta_config.device_mappings[manufacturer] = serviceTypeData;

    this.setState({ integrations: mappingObject });
  }

  getManufaturerName = id => {
    let cw_mnf_name = '';
    cw_mnf_name = this.props.manufacturers &&
      (this.props.manufacturers.filter(m => m.id === id).length > 0) ?
      this.props.manufacturers.filter(m => m.id === id)[0].label : null
    return cw_mnf_name;
  }

  changeMappingObjectManufacturer = (mappingObject, integration, mf, vendor, oldMf) => {
    const manufacturer = isNaN(mf) ? null : parseInt(mf);
    const serviceTypeData = {
      "solarwinds_vendor": vendor,
      "cw_mnf_id": manufacturer,
      "cw_mnf_name": this.getManufaturerName(manufacturer),
      "queries": this.getSolarwindsVendorsQueries().filter(m => m.label === vendor)[0].value,
    }
    delete mappingObject[integration].meta_config.device_mappings[oldMf]
    mappingObject[integration].meta_config.device_mappings[manufacturer] = serviceTypeData;

    this.setState({ integrations: mappingObject });
  }
  toggleIntegrationQuery = (mappingObject, integration, mapping, query) => {

    mappingObject[integration].meta_config.device_mappings[mapping].queries[query].is_enabled = !mappingObject[integration].meta_config.device_mappings[mapping].queries[query].is_enabled;
    this.setState({ integrations: mappingObject });
  }
  getSolarwindsVendors = () => {
    const meta = this.props.serviceTypes.filter(s => s.name === 'solarwinds')[0].meta_config;
    const vendors = [];
    Object.keys(meta).map(m =>
      vendors.push({ label: m, value: m })
    );
    return vendors
  }

  getSolarwindsVendorsQueries = () => {
    const meta = this.props.serviceTypes.filter(s => s.name === 'solarwinds')[0].meta_config;
    const vendors = [];
    Object.keys(meta).map(m =>
      vendors.push({ label: m, value: meta[m].queries })
    );
    return vendors
  }
  isValid = () => {
    let isValid = true;
    const mappingObject = this.state.integrations
    if (!mappingObject.solarwinds.device_mappings) {
      isValid = false
    }
    if (mappingObject.solarwinds.device_mappings) {

      Object.keys(mappingObject.solarwinds.device_mappings).map(m => {
        const mapping = mappingObject.solarwinds.device_mappings[m];
        if (mapping && (mapping.cw_mnf_id === null || mapping.solarwinds_vendor === '')) {
          isValid = false
        }
      });

    }
    this.setState({
      submitted: true
    });

    return isValid;
  };
  download = (serial_numbers, hostName, ipAddress) => {
    let headers = {
      name: 'Serial Numbers',
      hostName: 'Host name',
      ipAddress: 'IP address',
    };
    let itemsFormatted = [];
    let itemsNotFormatted = serial_numbers;
    itemsNotFormatted.forEach((item) => {
      itemsFormatted.push({
        name: item || 'N.A.',
        hostName: hostName || 'N.A.',
        ipAddress: ipAddress || 'N.A.',
      });
    });

    var fileTitle = 'SOLARWINDS-DEVICE-SERIAL-NUMBERS';
    exportCSVFile(headers, itemsFormatted, fileTitle);
  }
  renderDeviceDetails = () => {
    const collectorData = this.props.collector;
    const version = _.get(collectorData, 'current_version');
    const hostMetadata = _.get(collectorData, 'host_metadata');
    const otherMetadata = _.get(collectorData, 'other_metadata');
    const serial_numbers = otherMetadata['solarwinds'] && otherMetadata['solarwinds'].serial_numbers || [];

    return (
      <div className="collector-details-modal__body">
        <div className="body-field-token">
          <label>Token </label>
          <label>{collectorData.token ? collectorData.token : '-'}</label>
        </div>
        <div className="collector-fields">
          <div className="body-field ">
            <label>Configuration Status </label>
            <label>{collectorData.status ? collectorData.status : '-'}</label>
          </div>{' '}
          <div className="body-field ">
            <label>Status </label>
            <label>{collectorData.heartbeat_status ? collectorData.heartbeat_status : '-'}</label>
          </div>{' '}
          <div className="body-field ">
            <label>Name </label>
            <label>{collectorData.name ? collectorData.name : '-'}</label>
          </div>{' '}
          <div className="body-field ">
            <label>Version </label>
            <label>{version ? version : '-'}</label>
          </div>
          <div className="body-field ">
            <label>hostname </label>
            <label>{hostMetadata.hostname ? hostMetadata.hostname : '-'}</label>
          </div>
          <div className="body-field ">
            <label>host IP </label>
            <label>{hostMetadata.ip_address ? hostMetadata.ip_address : '-'}</label>
          </div>
          <div className="body-field ">
            <label>architecture </label>
            <label>{hostMetadata.architecture ? hostMetadata.architecture : '-'}</label>
          </div>
          <div className="body-field ">
            <label>os type </label>
            <label>{hostMetadata.os_type ? hostMetadata.os_type : '-'}</label>
          </div>
          <div className="body-field ">
            <label>Last Heart Beat </label>
            <label>
              {collectorData.last_heart_beat
                ? fromISOStringToDateTimeString(collectorData.last_heart_beat)
                : '-'}
            </label>
          </div>
        </div>
        <div className="body-field-serial-numbers ">
          <p>Solarwinds Device Serial Numbers
          {
              serial_numbers.length > 0 &&
              <SquareButton
                onClick={() => this.download(serial_numbers, hostMetadata.hostname, hostMetadata.ip_address)}
                content="Download"
                bsStyle={"default"}
                className="save-mapping-collector"
              />
            }
          </p>
          <label>
            {serial_numbers ? serial_numbers.join(', ') : 'N.A.'}
          </label>
        </div>
        <div className="device-fields col-md-12 row">
          <div
            className={`field ${this.state.openServices ? 'field--collapsed' : ''
              }`}
            key={1}
          >
            <div
              className={`section-heading valid-configuration
                   ${isEqual(otherMetadata, {}) ? 'disable-heading' : ''}`}
              onClick={() =>
                this.setState({
                  openServices: !this.state.openServices,
                })
              }
            >
              Services
                    {isEqual(otherMetadata, {}) ? (
                ''
              ) : (
                  <div className="action-collapse">
                    {!this.state.openServices ? '+' : '-'}
                  </div>
                )}
            </div>
            <div className="services">
              {this.state.openServices && !isEqual(otherMetadata, {}) &&
                <>
                  <div className="service" >
                    <div className="name"> Solarwinds  </div>
                    <div className="last-run"> <span>last run at : </span>
                      {otherMetadata['solarwinds'] ?
                        fromISOStringToDateTimeString(otherMetadata['solarwinds'].lastRunAt) : 'N.A.'}
                    </div>
                    <div className="last-run"> <span>Created Devices : </span>
                      {otherMetadata['solarwinds'] ? otherMetadata['solarwinds'].created_devices_count : 'N.A.'}
                    </div>
                    <div className="last-run"> <span>Updated Devices : </span>
                      {otherMetadata['solarwinds'] ? otherMetadata['solarwinds'].updated_devices_count : 'N.A.'}
                    </div>
                  </div>
                  <div className="service" >
                    <div className="name"> Host information  </div>
                    <div className="last-run"> <span>last run at : </span>
                      {otherMetadata['HostInfoService'] ?
                        fromISOStringToDateTimeString(otherMetadata['HostInfoService'].lastRunAt) : 'N.A.'}
                    </div>
                  </div>
                </>
              }
            </div>
            {
              isEqual(otherMetadata, {}) && (<div className="section-padding">No data found</div>)
            }

          </div>
          <div
            className={`field ${this.state.openIntegrations ? 'field--collapsed' : ''
              }`}
            key={2}
          >
            <div
              className={`section-heading valid-configuration`}
              onClick={() =>
                this.setState({
                  openIntegrations: !this.state.openIntegrations,
                })
              }
            >
              Integrations
                    {
                <div className="action-collapse">
                  {!this.state.openIntegrations ? '+' : '-'}
                </div>
              }
            </div>
            {
              this.state.openIntegrations && collectorData.integrations && collectorData.integrations.map((integration, index) => (
                <div className={`mapping-collector ${_.get(this.props.user, 'type') === 'provider' ? '' : 'disabled-section'}`} key={integration.id}>
                  <div className="heading">
                    {integration.integration_type}
                  </div>
                  {
                    integration.meta_config ?
                      null : <span className={`error-message-collector`}>
                        Please complete field mapping.
                                </span>
                  }
                  {this.state.integrations && Object.keys(this.state.integrations).map((integration, index) => (
                    <div className={`integration`} key={index}>
                      {
                        Object.keys(
                          this.state.integrations[integration].meta_config && this.state.integrations[integration].meta_config.device_mappings
                            ? this.state.integrations[integration].meta_config.device_mappings
                            : { null: { "cw_mnf_id": '', "cw_mnf_name": "", "solarwinds_vendor": "" } }
                        ).map((m, ind) => {
                          const mapping = this.state.integrations[integration].meta_config.device_mappings[m];

                          return (
                            <div className="matching-filds" key={ind}>
                              <div className="fields">
                                <Input
                                  field={{
                                    label: 'Vendor',
                                    type: "PICKLIST",
                                    value: mapping.solarwinds_vendor,
                                    options: this.getSolarwindsVendors(),
                                    isRequired: true,
                                  }}
                                  width={5}
                                  name="serviceType"
                                  onChange={e => this.changeMappingObject(this.state.integrations, integration, m, e.target.value)}
                                  error={!mapping.solarwinds_vendor && this.state.submitted ? {
                                    errorState: "error",
                                    errorMessage: 'Please select vendor'
                                  } :
                                    {
                                      errorState: "success",
                                      errorMessage: '',
                                    }}
                                  placeholder={`Select vendor`}
                                />
                                <Input
                                  field={{
                                    label: 'Manufacturers',
                                    type: "PICKLIST",
                                    value: m,
                                    options: this.manufacturers(),
                                    isRequired: true,
                                  }}
                                  width={5}
                                  name="manufacturer"
                                  onChange={e => this.changeMappingObjectManufacturer(this.state.integrations, integration, e.target.value, mapping.solarwinds_vendor, m)}
                                  error={m === 'null' && this.state.submitted ? {
                                    errorState: "error",
                                    errorMessage: 'Please select manufacturer'
                                  } :
                                    {
                                      errorState: "success",
                                      errorMessage: '',
                                    }} placeholder={`Select manufacturer`}
                                />
                              </div>
                              <div className="query-heading">Queries</div>
                              <div className="queries">
                                {
                                  Object.keys(mapping.queries).map(q => {
                                    const query = mapping.queries[q];
                                    return (
                                      <div className="query">{query.display_name} :
                                        <div
                                          className={'query-toggle'}
                                        >
                                          {
                                            query.allow_is_enabled_flag_edit &&
                                            <span>
                                              Disable
                                            </span>
                                          }
                                          <ToggleButton
                                            enabled={query.is_enabled}
                                            key={query.display_name}
                                            onChange={e => this.toggleIntegrationQuery(this.state.integrations, integration, m, q)}
                                            disabled={!query.allow_is_enabled_flag_edit}
                                          />
                                          <span
                                          >
                                            Enable
                                         </span>
                                        </div>
                                      </div>
                                    )
                                  })
                                }
                                {
                                  !mapping.queries || (mapping.queries && Object.keys(mapping.queries).length === 0) &&
                                  <div className="np-query">
                                    No Query Available
                                  </div>
                                }
                              </div>
                            </div>
                          )
                        })
                      }
                      {
                        _.get(this.props.user, 'type') === 'provider' &&
                        <SquareButton
                          onClick={this.onSave}
                          content="Save"
                          bsStyle={"primary"}
                          className="save-mapping-collector"
                        />
                      }
                    </div>
                  ))
                  }
                </div>
              ))
            }
            {!collectorData.integrations || collectorData.integrations && collectorData.integrations.length === 0 && (
              <div className={`mapping-collector error-message-collector`}>
                Please Integrate service from Collector App.
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  getTitle = () => {
    return <div className="collector-details">Collector </div>;
  };

  getBody = () => {
    return (
      <div className="collector-details">
        <div className="loader modal-loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div className={`${this.props.isFetching ? `loading` : ''}`}>
          {this.props.collector && this.renderDeviceDetails()}
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div
        className={`add-collector__footer
        ${this.props.isFetching ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="collector-details"
      />
    );
  }
}
