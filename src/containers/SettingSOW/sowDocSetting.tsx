import React, { Component } from "react";
import { connect } from "react-redux";
import { cloneDeep, get } from "lodash";
import { Sources } from "quill";
import ReactQuill from "react-quill";
import { Mention, MentionsInput } from "react-mentions";
import {
  editSOWDOCSetting,
  fetchSOWDOCSetting,
  EDIT_DOC_SETTING_SUCCESS,
  updateSowTemplateTerms,
  UPDATE_TEMPLATE_TERMS_SUCCESS,
  UPDATE_TEMPLATE_TERMS_FAILURE,
} from "../../actions/setting";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";
import { commonFunctions } from "../../utils/commonFunctions";
import {
  addErrorMessage,
  addInfoMessage,
  addSuccessMessage,
} from "../../actions/appState";
import ConfirmBox from "../../components/ConfirmBox/ConfirmBox";
import {
  CONFIG_TASK_STATUS_SUCCESS,
  fetchTaskStatusConfig,
} from "../../actions/configuration";

interface ISowDocSettingProps {
  docSetting: IDOCSetting;
  isFetchingDocSet: boolean;
  updateTerms: () => Promise<any>;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchSOWDOCSetting: () => Promise<any>;
  fetchTaskStatus: (id: number) => Promise<any>;
  editSOWDOCSetting: (data: IDOCSetting) => Promise<any>;
}
interface ISowDocSettingState {
  ongoingTask: boolean;
  docSetting: IDOCSetting;
  showConfirmation: boolean;
  error: {
    staging_text: IFieldValidation;
    default_ps_risk: IFieldValidation;
    risk_low_watermark: IFieldValidation;
    engineering_hourly_cost: IFieldValidation;
    pm_hourly_cost: IFieldValidation;
    engineering_hourly_rate: IFieldValidation;
    after_hours_rate: IFieldValidation;
    after_hours_cost: IFieldValidation;
    integration_technician_hourly_rate: IFieldValidation;
    integration_technician_hourly_cost: IFieldValidation;
    project_management_hourly_rate: IFieldValidation;
    terms_t_and_m: IFieldValidation;
    pm_type_t_and_m_terms: IFieldValidation;
    pm_type_t_and_m_name: IFieldValidation;
    project_management_t_and_m: IFieldValidation;
    terms_fixed_fee: IFieldValidation;
    project_management_fixed_fee: IFieldValidation;
    total_no_of_cutovers_statement: IFieldValidation;
    total_no_of_sites_statement: IFieldValidation;
    engineering_hours_description: IFieldValidation;
    after_hours_description: IFieldValidation;
    integration_technician_description: IFieldValidation;
    project_management_hours_description: IFieldValidation;
    exclude_bill_travel_text: IFieldValidation;
    include_bill_travel_text: IFieldValidation;
    hidden_bill_travel_text: IFieldValidation;
  };
}

class SowDocSetting extends Component<
  ISowDocSettingProps,
  ISowDocSettingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  quillRef: React.RefObject<ReactQuill>;
  constructor(props: ISowDocSettingProps) {
    super(props);
    this.state = this.getEmptyState();
    this.quillRef = React.createRef<ReactQuill>();
  }

  getEmptyState = () => ({
    isOpen: true,
    ongoingTask: false,
    showConfirmation: false,
    docSetting: {
      staging_text: "",
      default_ps_risk: null,
      risk_low_watermark: null,
      engineering_hourly_cost: null,
      pm_hourly_cost: null,
      engineering_hourly_rate: null,
      after_hours_rate: null,
      after_hours_cost: null,
      integration_technician_hourly_rate: null,
      integration_technician_hourly_cost: null,
      project_management_hourly_rate: null,
      terms_t_and_m: "",
      project_management_t_and_m: "",
      pm_type_t_and_m_terms: "",
      pm_type_t_and_m_name: "",
      terms_fixed_fee: "",
      project_management_fixed_fee: "",
      total_no_of_sites_statement: "",
      total_no_of_cutovers_statement: "",
      engineering_hours_description: "",
      after_hours_description: "",
      integration_technician_description: "",
      project_management_hours_description: "",
      exclude_bill_travel_text: "",
      include_bill_travel_text: "",
      hidden_bill_travel_text: "",
    },
    error: {
      staging_text: { ...SowDocSetting.emptyErrorState },
      default_ps_risk: { ...SowDocSetting.emptyErrorState },
      risk_low_watermark: { ...SowDocSetting.emptyErrorState },
      engineering_hourly_cost: { ...SowDocSetting.emptyErrorState },
      pm_hourly_cost: { ...SowDocSetting.emptyErrorState },
      engineering_hourly_rate: { ...SowDocSetting.emptyErrorState },
      after_hours_rate: { ...SowDocSetting.emptyErrorState },
      after_hours_cost: { ...SowDocSetting.emptyErrorState },
      integration_technician_hourly_rate: { ...SowDocSetting.emptyErrorState },
      integration_technician_hourly_cost: { ...SowDocSetting.emptyErrorState },
      project_management_hourly_rate: { ...SowDocSetting.emptyErrorState },
      terms_t_and_m: { ...SowDocSetting.emptyErrorState },
      pm_type_t_and_m_terms: { ...SowDocSetting.emptyErrorState },
      pm_type_t_and_m_name: { ...SowDocSetting.emptyErrorState },
      project_management_t_and_m: { ...SowDocSetting.emptyErrorState },
      terms_fixed_fee: { ...SowDocSetting.emptyErrorState },
      project_management_fixed_fee: { ...SowDocSetting.emptyErrorState },
      total_no_of_sites_statement: { ...SowDocSetting.emptyErrorState },
      total_no_of_cutovers_statement: { ...SowDocSetting.emptyErrorState },
      engineering_hours_description: { ...SowDocSetting.emptyErrorState },
      after_hours_description: { ...SowDocSetting.emptyErrorState },
      integration_technician_description: { ...SowDocSetting.emptyErrorState },
      exclude_bill_travel_text: { ...SowDocSetting.emptyErrorState },
      include_bill_travel_text: { ...SowDocSetting.emptyErrorState },
      hidden_bill_travel_text: { ...SowDocSetting.emptyErrorState },
      project_management_hours_description: {
        ...SowDocSetting.emptyErrorState,
      },
    },
  });

  componentDidMount() {
    this.props.fetchSOWDOCSetting();
  }

  componentDidUpdate(prevProps: ISowDocSettingProps) {
    if (
      this.props.docSetting &&
      this.props.docSetting !== prevProps.docSetting
    ) {
      this.setState({
        docSetting: {
          ...this.props.docSetting,
          default_ps_risk: this.props.docSetting.default_ps_risk * 100,
          risk_low_watermark: this.props.docSetting.risk_low_watermark * 100,
        },
      });
    }
  }

  OnSaveDocSetting = () => {
    if (this.isValid()) {
      const payload: IDOCSetting = {
        ...this.state.docSetting,
        risk_low_watermark: this.state.docSetting.risk_low_watermark / 100,
        default_ps_risk: this.state.docSetting.default_ps_risk / 100,
      };
      this.props.editSOWDOCSetting(payload).then((action) => {
        if (action.type === EDIT_DOC_SETTING_SUCCESS) {
          this.props.addSuccessMessage("Settings saved successfully!");
          this.props.fetchSOWDOCSetting();
        }
      });
    }
  };

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["#"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = [
          { id: "project_name", value: "{Project Name}" },
          { id: "project_date", value: "{Project Date}" },
        ];
      }

      renderList(values, searchTerm);
    },
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangesTM = (content: string, source: Sources, editor: any) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.docSetting
      .change_request_t_and_m_terms as string) = this.getContentFromObject(
      object
    );
    (newState.docSetting
      .change_request_t_and_m_terms_markdown as string) = content;
    this.setState({ ...newState });
  };

  handleChangesFF = (content: string, source: Sources, editor: any) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.docSetting
      .change_request_fixed_fee_terms as string) = this.getContentFromObject(
      object
    );
    (newState.docSetting
      .change_request_fixed_fee_terms_markdown as string) = content;
    this.setState({ ...newState });
  };

  handleChangeSow = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.docSetting[event.target.name] = event.target.value;
    this.setState(newState);
  };

  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;
    let number_fields = [
      "after_hours_rate",
      "after_hours_cost",
      "integration_technician_hourly_rate",
      "integration_technician_hourly_cost",
      "engineering_hourly_cost",
      "engineering_hourly_rate",
      "default_ps_risk",
      "risk_low_watermark",
      "pm_hourly_cost",
      "project_management_hourly_rate",
    ];
    number_fields.forEach((field) => {
      if (Number(this.state.docSetting[field]) <= 0) {
        error[field].errorState = "error";
        error[field].errorMessage = "Positive Number Required";

        isValid = false;
      }
    });

    let text_fields = [
      "staging_text",
      "pm_type_t_and_m_name",
      "total_no_of_cutovers_statement",
      "total_no_of_sites_statement",
      "engineering_hours_description",
      "after_hours_description",
      "integration_technician_description",
      "project_management_hours_description",
    ];
    text_fields.forEach((field) => {
      if (
        !this.state.docSetting[field] ||
        this.state.docSetting[field].trim().length === 0
      ) {
        error[field].errorState = "error";
        error[field].errorMessage = "Required field";

        isValid = false;
      }
    });

    let html_fields = [
      "exclude_bill_travel_text",
      "include_bill_travel_text",
      "hidden_bill_travel_text",
      "project_management_t_and_m",
      "terms_t_and_m",
      "pm_type_t_and_m_terms",
      "project_management_fixed_fee",
      "terms_fixed_fee",
    ];

    html_fields.forEach((field) => {
      if (commonFunctions.isEditorEmpty(this.state.docSetting[field])) {
        error[field].errorState = "error";
        error[field].errorMessage = "Required field";

        isValid = false;
      }
    });

    if (
      this.state.docSetting.risk_low_watermark >
      this.state.docSetting.default_ps_risk
    ) {
      error.risk_low_watermark.errorState = "error";
      error.risk_low_watermark.errorMessage =
        "Risk Low Watermark should be less than Default PS Risk";

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };

  toggleConfirmationBox = () => {
    this.setState((prev) => ({ showConfirmation: !prev.showConfirmation }));
  };

  fetchTaskStatus = (taskId: number) => {
    if (taskId) {
      this.props.fetchTaskStatus(taskId).then((action) => {
        if (action.type === CONFIG_TASK_STATUS_SUCCESS) {
          if (action.response.status === "SUCCESS") {
            this.setState({ ongoingTask: false });
            this.props.addSuccessMessage(
              `Terms have been updated for each Sow Template!`
            );
          }
          if (action.response.status === "PENDING") {
            setTimeout(() => this.fetchTaskStatus(taskId), 10000);
          }
          if (action.response.status === "FAILURE") {
            this.setState({ ongoingTask: false });
            this.props.addErrorMessage(
              `Error while updating terms in Sow Template, Task Failed!`
            );
          }
        }
      });
    }
  };

  updateTerms = () => {
    this.toggleConfirmationBox();
    this.props.updateTerms().then((action) => {
      if (action.type === UPDATE_TEMPLATE_TERMS_SUCCESS) {
        this.props.addInfoMessage("Terms update task started...");
        this.setState({ ongoingTask: true });
        this.fetchTaskStatus(action.response.task_id);
      } else if (action.type === UPDATE_TEMPLATE_TERMS_FAILURE) {
        this.props.addErrorMessage("Error triggering the task!");
      }
    });
  };

  handleChangeSowMarkDown = (html: string, name: string) => {
    const newState = cloneDeep(this.state);
    newState.docSetting[name] = html;
    this.setState(newState);
  };

  rangeVariables = () => {
    const variablesList = [`#{no_of_sites}`, `#{no_of_cutovers}`];
    const globalVariables = variablesList.map((c) => ({
      id: c,
      display: c,
    }));

    return globalVariables;
  };

  render() {
    return (
      <div className="doc-setting-container">
        <ConfirmBox
          show={this.state.showConfirmation}
          okText="Yes"
          title="Confirm Full Sync"
          message={
            "Are you sure you would like to update all template terms to match the configured settings?"
          }
          onClose={this.toggleConfirmationBox}
          onSubmit={this.updateTerms}
          isLoading={false}
        />
        <SquareButton
          content="Push Terms to Template"
          onClick={this.toggleConfirmationBox}
          title="This will push all the current terms in settings to the templates"
          bsStyle={"primary"}
          className="btn-term-trigger"
          disabled={this.state.ongoingTask}
        />
        <div className="header">
          <label className="field__label-label" title="">
            Document Setting
          </label>
        </div>
        <div className="service-catagories">
          <Input
            field={{
              label: "Engineering Hours Default Description",
              type: "TEXT",
              isRequired: true,
              value: this.state.docSetting.engineering_hours_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="engineering_hours_description"
            onChange={this.handleChangeSow}
            error={this.state.error.engineering_hours_description}
          />
          <Input
            field={{
              label: "Engineering Hourly Rate",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.engineering_hourly_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering hourly rate"
            name="engineering_hourly_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.engineering_hourly_rate}
          />
          <Input
            field={{
              label: "Engineering Hourly Cost",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.engineering_hourly_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering hourly cost"
            name="engineering_hourly_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.engineering_hourly_cost}
          />
          <Input
            field={{
              label: "Engineering After Hours Default Description",
              type: "TEXT",
              isRequired: true,
              value: this.state.docSetting.after_hours_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="after_hours_description"
            onChange={this.handleChangeSow}
            error={this.state.error.after_hours_description}
          />
          <Input
            field={{
              label: "Engineering After Hours Rate",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.after_hours_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering after hours rate"
            name="after_hours_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.after_hours_rate}
          />
          <Input
            field={{
              label: "Engineering After Hours Cost",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.after_hours_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter engineering after hours cost"
            name="after_hours_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.after_hours_cost}
          />
          <Input
            field={{
              label: "Integration Technician Default Description",
              type: "TEXT",
              isRequired: true,
              value: this.state.docSetting.integration_technician_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="integration_technician_description"
            onChange={this.handleChangeSow}
            error={this.state.error.integration_technician_description}
          />
          <Input
            field={{
              label: "Integration Technician Hourly Rate",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.integration_technician_hourly_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter Hourly Rate"
            name="integration_technician_hourly_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.integration_technician_hourly_rate}
          />
          <Input
            field={{
              label: "Integration Technician Hourly Cost",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.integration_technician_hourly_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter Hourly Cost"
            name="integration_technician_hourly_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.integration_technician_hourly_cost}
          />
          <Input
            field={{
              label: "Project Management Hours Default Description",
              type: "TEXT",
              isRequired: true,
              value: this.state.docSetting.project_management_hours_description,
            }}
            width={4}
            placeholder="Enter Description"
            name="project_management_hours_description"
            onChange={this.handleChangeSow}
            error={this.state.error.project_management_hours_description}
          />
          <Input
            field={{
              label: "Project Management Hourly Rate",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.project_management_hourly_rate,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter project management hourly rate"
            name="project_management_hourly_rate"
            onChange={this.handleChangeSow}
            error={this.state.error.project_management_hourly_rate}
          />
          <Input
            field={{
              label: "PM Hourly Cost",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.pm_hourly_cost,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter PM hourly cost"
            name="pm_hourly_cost"
            onChange={this.handleChangeSow}
            error={this.state.error.pm_hourly_cost}
          />
          <Input
            field={{
              label: "Default PS Risk (in %)",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.default_ps_risk,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter fixed fee variable"
            name="default_ps_risk"
            onChange={this.handleChangeSow}
            error={this.state.error.default_ps_risk}
          />
          <Input
            field={{
              label: "Risk Low Watermark (in %)",
              type: "NUMBER",
              isRequired: true,
              value: this.state.docSetting.risk_low_watermark,
            }}
            width={4}
            minimumValue="0"
            placeholder="Enter Risk Low Watermark"
            name="risk_low_watermark"
            onChange={this.handleChangeSow}
            error={this.state.error.risk_low_watermark}
          />
          <Input
            field={{
              label: "Default T&M (Fee based PM) Name",
              type: "TEXT",
              isRequired: true,
              value: this.state.docSetting.pm_type_t_and_m_name,
            }}
            width={4}
            labelIcon="info"
            labelTitle={
              "Default name for Fee based Project Management Type T&M SoW Doc"
            }
            placeholder="Enter Default Name"
            name="pm_type_t_and_m_name"
            onChange={this.handleChangeSow}
            error={this.state.error.pm_type_t_and_m_name}
          />
          <Input
            field={{
              label: "Total no. of cutovers statement",
              type: "CUSTOM",
              value: "",
              isRequired: true,
            }}
            width={4}
            labelIcon={""}
            name="total_no_of_cutovers_statement"
            onChange={(e) => null}
            customInput={
              <MentionsInput
                markup="[__display__]"
                value={this.state.docSetting.total_no_of_cutovers_statement}
                onChange={(event) => {
                  const newState = cloneDeep(this.state);
                  newState.docSetting.total_no_of_cutovers_statement =
                    event.target.value;
                  this.setState(newState);
                }}
                className={"outer"}
              >
                <Mention
                  trigger="#"
                  data={this.rangeVariables()}
                  className={"inner-drop"}
                  markup="#__display__"
                />
              </MentionsInput>
            }
            placeholder={`Status`}
            error={this.state.error.total_no_of_cutovers_statement}
            disabled={false}
          />

          <Input
            field={{
              label: "Total no. of sites statement",
              type: "CUSTOM",
              value: "",
              isRequired: true,
            }}
            width={4}
            labelIcon={""}
            name="to"
            onChange={(e) => null}
            customInput={
              <MentionsInput
                markup="[__display__]"
                value={this.state.docSetting.total_no_of_sites_statement}
                onChange={(event) => {
                  const newState = cloneDeep(this.state);
                  newState.docSetting.total_no_of_sites_statement =
                    event.target.value;
                  this.setState(newState);
                }}
                className={"outer"}
              >
                <Mention
                  trigger="#"
                  data={this.rangeVariables()}
                  className={"inner-drop"}
                  markup="#__display__"
                />
              </MentionsInput>
            }
            placeholder={`Status`}
            error={this.state.error.total_no_of_sites_statement}
            disabled={false}
          />
          <Input
            field={{
              label: "SoW Staging Text",
              type: "TEXTAREA",
              value: this.state.docSetting.staging_text,
              isRequired: true,
            }}
            width={4}
            labelIcon="info"
            labelTitle={
              "This text will appear in SoW Preview if Staging at LookingPoint option is selected in a SoW Document"
            }
            className="staging-text-box"
            name="staging_text"
            onChange={this.handleChangeSow}
            placeholder="Enter staging text"
            error={this.state.error.staging_text}
          />
          <div className="col-md-12 sow-settings-editor">
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "include_bill_travel_text"
                  )
                }
                label={"Bill Expenses on Actuals Text"}
                value={this.state.docSetting.include_bill_travel_text}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.include_bill_travel_text}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "exclude_bill_travel_text"
                  )
                }
                label={"Expenses are Included Text"}
                value={this.state.docSetting.exclude_bill_travel_text}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.exclude_bill_travel_text}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "hidden_bill_travel_text"
                  )
                }
                label={"Travels are not Expected Text"}
                value={this.state.docSetting.hidden_bill_travel_text}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.hidden_bill_travel_text}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "project_management_t_and_m"
                  )
                }
                label={"Project Management T & M"}
                value={this.state.docSetting.project_management_t_and_m}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.project_management_t_and_m}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(value, "terms_t_and_m")
                }
                label={"Terms & Conditions T & M"}
                value={this.state.docSetting.terms_t_and_m}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.terms_t_and_m}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(value, "pm_type_t_and_m_terms")
                }
                label={
                  "Terms & Conditions T & M (Fee based Project Management)"
                }
                value={this.state.docSetting.pm_type_t_and_m_terms}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.pm_type_t_and_m_terms}
              />
            </div>
            <div className="field-section mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(
                    value,
                    "project_management_fixed_fee"
                  )
                }
                label={"Project Management Fixed Fee"}
                value={this.state.docSetting.project_management_fixed_fee}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.project_management_fixed_fee}
              />
            </div>
            <div className="field-section  mde-editor">
              <QuillEditorAcela
                onChange={(value) =>
                  this.handleChangeSowMarkDown(value, "terms_fixed_fee")
                }
                label={"Terms & Conditions Fixed Fee"}
                value={this.state.docSetting.terms_fixed_fee}
                wrapperClass={"sow-settings-quill"}
                isRequired={true}
                scrollingContainer=".ql-container .app-body"
                error={this.state.error.terms_fixed_fee}
              />
            </div>
            <div className="field-section  mde-editor">
              <QuillEditorAcela
                onChange={this.handleChangesTM}
                label={"Change Request T&M Terms"}
                value={
                  this.state.docSetting.change_request_t_and_m_terms_markdown
                }
                wrapperClass={"sow-settings-quill"}
                scrollingContainer=".ql-container .app-body"
                customMentionModule={this.mentionModule}
              />
            </div>
            <div className="field-section  mde-editor">
              <QuillEditorAcela
                onChange={this.handleChangesFF}
                label={"Change Request Fixed Fee Terms"}
                value={
                  this.state.docSetting.change_request_fixed_fee_terms_markdown
                }
                wrapperClass={"sow-settings-quill"}
                scrollingContainer=".ql-container .app-body"
                customMentionModule={this.mentionModule}
              />
            </div>
          </div>
        </div>
        <SquareButton
          onClick={() => this.OnSaveDocSetting()}
          content="Save"
          bsStyle={"primary"}
          className="save-button-doc-setting"
          disabled={
            this.props.isFetchingDocSet ||
            this.state.docSetting === this.props.docSetting
          }
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  docSetting: state.setting.docSetting,
  isFetchingDocSet: state.setting.isFetchingDocSet,
});

const mapDispatchToProps = (dispatch: any) => ({
  updateTerms: () => dispatch(updateSowTemplateTerms()),
  fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
  addInfoMessage: (msg: string) => dispatch(addInfoMessage(msg)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  fetchTaskStatus: (id: number) => dispatch(fetchTaskStatusConfig(id)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  editSOWDOCSetting: (data: IDOCSetting) => dispatch(editSOWDOCSetting(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SowDocSetting);
