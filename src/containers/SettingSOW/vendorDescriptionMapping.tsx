import React, { useEffect, useMemo, useState } from "react";
import { random } from "lodash";
import { connect } from "react-redux";
import { getVendorsList } from "../../actions/sow";
import {
  vendorAliasMappingCRUD,
  VENDOR_ALIAS_MAPPING_FAILURE,
  VENDOR_ALIAS_MAPPING_SUCCESS,
} from "../../actions/setting";
import InfiniteListing from "../../components/InfiniteList/infiniteList";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import EditButton from "../../components/Button/editButton";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import SquareButton from "../../components/Button/button";
import VendorMappingModal from "./vendorMappingModal";

interface VendorDescriptionMappingProps {
  getVendorsList: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  vendorDescriptionCRUD: (
    request: HTTPMethods,
    data?: IVendorAliasMapping
  ) => Promise<any>;
}

const EmptyVendorAliasMapping: IVendorAliasMapping = {
  vendor_name: "",
  vendor_crm_id: null,
  resource_description: "",
};

const VendorDescriptionMapping: React.FC<VendorDescriptionMappingProps> = (
  props
) => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [reloadKey, setReloadKey] = useState<number>(random(1, 1000));
  const [currentMapping, setCurrentMapping] = useState<IVendorAliasMapping>({
    ...EmptyVendorAliasMapping,
  });

  useEffect(() => {
    props.getVendorsList();
  }, []);

  const closeModal = (refresh: boolean = false) => {
    setShowModal(false);
    setCurrentMapping({ ...EmptyVendorAliasMapping });
    if (refresh) setReloadKey(random(1, 1000));
  };

  const onClickEdit = (mapping: IVendorAliasMapping) => {
    setCurrentMapping(mapping);
    setShowModal(true);
  };

  const removeMapping = (mapping: IVendorAliasMapping) => {
    props.vendorDescriptionCRUD("delete", mapping).then((action) => {
      if (action.type === VENDOR_ALIAS_MAPPING_SUCCESS) {
        props.addSuccessMessage("Vendor Mapping Deleted Successfully!");
        setReloadKey(random(1, 1000));
      } else if (action.type === VENDOR_ALIAS_MAPPING_FAILURE) {
        props.addErrorMessage("Error deleting vendor alias mapping!");
      }
    });
  };

  const columns: IColumnInfinite<IVendorAliasMapping>[] = useMemo(
    () => [
      {
        name: "Vendor",
        ordering: "vendor_name",
        className: "vendor-name-col",
        id: "vendor_name",
      },
      {
        id: "resource_description",
        ordering: "resource_description",
        name: "Default Description",
        className: "vendor-alias-col",
      },
      {
        name: "",
        className: "vendor-mapping-actions",
        Cell: (mapping) => {
          return (
            <>
              <EditButton
                title="Edit Product"
                onClick={() => onClickEdit(mapping)}
              />
              <SmallConfirmationBox
                className="remove"
                showButton={true}
                onClickOk={() => removeMapping(mapping)}
                text={"Vendor Description Mapping"}
              />
            </>
          );
        },
      },
    ],
    []
  );

  return (
    <>
      <VendorMappingModal
        show={showModal}
        closeModal={closeModal}
        mapping={currentMapping}
      />
      <div className="vendor-description-mapping-container">
        <SquareButton
          content={
            <>
              <span className="add-plus">+</span>
              <span className="add-text">Add Mapping</span>
            </>
          }
          bsStyle={"link"}
          onClick={() => {
            setShowModal(true);
          }}
          className="add-new-vendor-alias add-btn"
        />
        <InfiniteListing
          id="VendorAliasMapping"
          url={"providers/document/sow/resource-description"}
          key={reloadKey}
          columns={columns}
          className="vendor-description-mapping-list"
          showSearch={true}
          height={"68vh"}
        />
      </div>
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getVendorsList: () => dispatch(getVendorsList()),
  vendorDescriptionCRUD: (request: HTTPMethods, data?: IVendorAliasMapping) =>
    dispatch(vendorAliasMappingCRUD(request, data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendorDescriptionMapping);
