import React, { Component } from "react";
import { connect } from "react-redux";
import Spinner from "../../components/Spinner";
import PMOCollapsible from "../../components/PMOCollapsible";
import HorizontalTabSlider, {
  Tab,
} from "../../components/HorizontalTabSlider/HorizontalTabSlider";
import SalesActivityStatusSetting from "./salesActivityStatusSetting";
import SalesActivityPrioritySetting from "./salesActivityPrioritySetting";
import VendorSettings from "./vendorSettings";
import SowCreatorRole from "./sowCreatorRole";
import CatalogSetting from "./catalogSetting";
import AgreementSetting from "./agreementSetting";

import "./style.scss";
import SowDocSetting from "./sowDocSetting";
import VendorDescriptionMapping from "./vendorDescriptionMapping";

interface ISOWSettingState {
  isOpen: boolean;
  defaultTab: string;
}

interface ISOWSettingProps extends ICommonProps {
  isFetching: boolean;
  isFetchingType: boolean;
  isFetchingDocSet: boolean;
}

class SOWSettings extends Component<ISOWSettingProps, ISOWSettingState> {
  constructor(props: ISOWSettingProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    isOpen: true,
    defaultTab: undefined,
  });

  static getDerivedStateFromProps(
    nextProps: ISOWSettingProps,
    prevState: ISOWSettingState
  ) {
    const query = new URLSearchParams(nextProps.location.search);
    const defaultTab = query.get("tab");
    if (!prevState.defaultTab && defaultTab) {
      return {
        defaultTab: defaultTab,
      };
    }
    return null;
  }

  render() {
    return (
      <div className="service-catalog">
        {(this.props.isFetching ||
          this.props.isFetchingType ||
          this.props.isFetchingDocSet) && (
          <div className="loader">
            <Spinner
              show={
                this.props.isFetching ||
                this.props.isFetchingType ||
                this.props.isFetchingDocSet
              }
            />
          </div>
        )}
        <h3>SOW Settings</h3>
        <div className="sow-setting">
          <HorizontalTabSlider defaultTab={this.state.defaultTab}>
            <Tab title={"Document"}>
              <SowDocSetting />
            </Tab>
            <Tab title={"Catalog"}>
              <CatalogSetting />
            </Tab>
            <Tab title={"Sales Activity"}>
              <>
                <PMOCollapsible
                  label={"Status Setting"}
                  isOpen={this.state.isOpen}
                >
                  <ul className="namespace-list">
                    <SalesActivityStatusSetting />
                  </ul>
                </PMOCollapsible>
                <PMOCollapsible
                  label="Priority Setting"
                  isOpen={this.state.isOpen}
                >
                  <ul className="namespace-list">
                    <SalesActivityPrioritySetting />
                  </ul>
                </PMOCollapsible>
              </>
            </Tab>
            <Tab title={"Agreement"}>
              <AgreementSetting />
            </Tab>
            <Tab title={"Vendor"}>
              <VendorSettings />
            </Tab>
            <Tab title={"SoW Creator Role"}>
              <SowCreatorRole />
            </Tab>
            <Tab title={"Vendor Description Mapping"}>
              <VendorDescriptionMapping />
            </Tab>
          </HorizontalTabSlider>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.setting.isFetching,
  isFetchingType: state.setting.isFetchingType,
  isFetchingDocSet: state.setting.isFetchingDocSet,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(SOWSettings);
