import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import {
  createCWTeamRole,
  sowCreatorSettingCRU,
  fetchCWTeamRoleOptions,
  SOW_ROLE_SETTING_SUCCESS,
  SOW_ROLE_SETTING_FAILURE,
  CREATE_CW_TEAM_ROLE_SUCCESS,
  CREATE_CW_TEAM_ROLE_FAILURE,
} from "../../actions/setting";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import RenameBox from "../../components/RenameBox/RenameBox";
import Spinner from "../../components/Spinner";

interface DocumentDefaultRoleProps {
  isFetchingRoles: boolean;
  cwTeamRoleOptions: IPickListOptions[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchCWTeamRoleOptions: () => Promise<any>;
  createRole: (role_name: string) => Promise<any>;
  sowCreatorSettingCRU: (
    request: "get" | "post" | "put",
    data?: {
      default_sow_creator_role_id: number;
      default_change_request_creator_role_id: number;
    }
  ) => Promise<any>;
}

const DocumentDefaultRole: React.FC<DocumentDefaultRoleProps> = (props) => {
  const [crRole, setCRRole] = useState<number>(null);
  const [sowRole, setSowRole] = useState<number>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [firstSave, setFirstSave] = useState<boolean>(false);

  useEffect(() => {
    getSOWRoleSettings();
    props.fetchCWTeamRoleOptions();
  }, []);

  const toggleModal = (show: boolean) => {
    setShowModal(show);
  };

  const saveSOWRoleSettings = () => {
    setLoading(true);
    let payload = {
      default_sow_creator_role_id: sowRole,
      default_change_request_creator_role_id: crRole,
    };
    props
      .sowCreatorSettingCRU(firstSave ? "post" : "put", payload)
      .then((action) => {
        if (action.type === SOW_ROLE_SETTING_SUCCESS) {
          setFirstSave(false);
          props.addSuccessMessage(
            `Default Role Settings ${
              firstSave ? "Saved" : "Updated"
            } Successfully!`
          );
        } else if (action.type === SOW_ROLE_SETTING_FAILURE) {
          props.addErrorMessage("Error while saving settings!");
        }
      })
      .finally(() => setLoading(false));
  };

  const getSOWRoleSettings = () => {
    setLoading(true);
    props
      .sowCreatorSettingCRU("get")
      .then((action) => {
        if (action.type === SOW_ROLE_SETTING_SUCCESS) {
          setSowRole(action.response.default_sow_creator_role_id);
          setCRRole(action.response.default_change_request_creator_role_id);
        } else if (action.type === SOW_ROLE_SETTING_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChangeSOWRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSowRole(Number(event.target.value));
  };

  const handleChangeCRRole = (event: React.ChangeEvent<HTMLInputElement>) => {
    setCRRole(Number(event.target.value));
  };

  const addRole = (name: string) => {
    setLoading(true);
    props
      .createRole(name)
      .then((action) => {
        if (action.type === CREATE_CW_TEAM_ROLE_SUCCESS) {
          props.fetchCWTeamRoleOptions();
          props.addSuccessMessage("Role created successfully!");
          toggleModal(false);
        } else if (action.type === CREATE_CW_TEAM_ROLE_FAILURE) {
          props.addErrorMessage("Error while creating role!");
        }
      })
      .finally(() => setLoading(false));
  };

  return (
    <div className="default-role-setting-container">
      <RenameBox
        show={showModal}
        onClose={() => toggleModal(false)}
        onSubmit={addRole}
        isLoading={loading}
        name={""}
        okText={"Create"}
        title={"Create New Role"}
        labelName={"Role"}
      />
      <Spinner show={loading} />
      <SquareButton
        bsStyle={"link"}
        content={"Create New Role"}
        onClick={() => toggleModal(true)}
        className="create-role-btn"
      />
      <div className="col-md-12">
        <Input
          field={{
            label: "Default SoW Creator Role",
            type: "PICKLIST",
            value: sowRole,
            options: props.cwTeamRoleOptions,
            isRequired: true,
          }}
          width={6}
          name="sowRole"
          loading={props.isFetchingRoles}
          onChange={(e) => handleChangeSOWRole(e)}
          placeholder={`Select Default Role`}
          labelIcon="info"
          labelTitle={"The SoW author is added to customer team for this role"}
        />
        <Input
          field={{
            label: "Default Change Request Creator Role",
            type: "PICKLIST",
            value: crRole,
            options: props.cwTeamRoleOptions,
            isRequired: true,
          }}
          width={6}
          name="crRole"
          loading={props.isFetchingRoles}
          onChange={(e) => handleChangeCRRole(e)}
          placeholder={`Select Default Role`}
          labelIcon="info"
          labelTitle={
            "The Change Request author is added to customer team for this role"
          }
        />
      </div>
      <SquareButton
        bsStyle={"primary"}
        content={firstSave ? "Save" : "Update"}
        onClick={saveSOWRoleSettings}
        className="default-role-btn"
        disabled={!sowRole || !crRole || loading}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  cwTeamRoleOptions: state.setting.cwTeamRoles,
  isFetchingRoles: state.setting.isFetchingCWTeamRoles,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  fetchCWTeamRoleOptions: () => dispatch(fetchCWTeamRoleOptions()),
  createRole: (role_name: string) => dispatch(createCWTeamRole(role_name)),
  sowCreatorSettingCRU: (
    request: "get" | "post" | "put",
    data?: {
      default_sow_creator_role_id: number;
      default_change_request_creator_role_id: number;
    }
  ) => dispatch(sowCreatorSettingCRU(request, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentDefaultRole);
