// import { cloneDeep } from "lodash";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import {
  editServiceCatalog,
  editServiceTypeSTT,
  EDIT_SERVICE_CATALOG_SUCCESS,
  EDIT_SERVICE_TYPE_SUCCESS,
  fetchServiceCatalogCategories,
  fetchServiceTypeSTT,
} from "../../actions/setting";
import SquareButton from "../../components/Button/button";

interface CatalogSettingProps {
  isFetching: boolean;
  isFetchingType: boolean;
  serviceType: ICategoryList[];
  serviceCatalogCategories: string[];
  fetchServiceTypeSTT: () => Promise<any>;
  fetchServiceCatalogCategories: () => Promise<any>;
  editServiceCatalog: (data: any) => Promise<any>;
  editServiceTypeSTT: (data: any) => Promise<any>;
}

const CatalogSetting: React.FC<CatalogSettingProps> = (props) => {
  const [serviceCatalogCategories, setServiceCatalogCategories] = useState<
    string[]
  >([]);
  const [serviceType, setServiceType] = useState<ICategoryList[]>([]);

  useEffect(() => {
    props.fetchServiceTypeSTT();
    props.fetchServiceCatalogCategories();
  }, []);

  useEffect(() => {
    if (props.serviceCatalogCategories)
      setServiceCatalogCategories(props.serviceCatalogCategories);
  }, [props.serviceCatalogCategories]);

  useEffect(() => {
    if (props.serviceType) {
      const fetchedServiceType = props.serviceType;
      fetchedServiceType.map((type, index) => {
        if (!Array.isArray(fetchedServiceType[index].technology_types_list)) {
          fetchedServiceType[index].technology_types_list = [];
        }
        type.technology_types.map((techType, i) => {
          if (techType.is_disabled !== true) {
            fetchedServiceType[index].technology_types_list.push(techType.name);
          }
        });
      });
      setServiceType(fetchedServiceType);
    }
  }, [props.serviceType]);

  // const handleChangeTypes = (arr: string[], i: number) => {
  //   let serviceTypes = cloneDeep(serviceType);
  //   const list = arr.filter(
  //     (value, index, self) => self.indexOf(value) === index
  //   );
  //   serviceTypes[i].technology_types_list = list;
  //   setServiceType(serviceTypes);
  // };

  // const handleChange = (arr: string[]) => {
  //   const list = arr.filter(
  //     (value, index, self) => self.indexOf(value) === index
  //   );
  //   setServiceCatalogCategories(list);
  // };

  const onSaveCatagory = () => {
    props.editServiceCatalog(serviceCatalogCategories).then((action) => {
      if (action.type === EDIT_SERVICE_CATALOG_SUCCESS) {
        props.fetchServiceCatalogCategories();
      }
    });
  };

  const onSaveServiceType = () => {
    serviceType.map((type, index) => {
      serviceType[index].technology_types = [];
      type.technology_types_list.map((techType, i) =>
        serviceType[index].technology_types.push({ name: techType })
      );
    });
    props.editServiceTypeSTT(serviceType[0]);
    props.editServiceTypeSTT(serviceType[1]);
    props.editServiceTypeSTT(serviceType[2]);
    props.editServiceTypeSTT(serviceType[3]).then((action) => {
      if (action.type === EDIT_SERVICE_TYPE_SUCCESS) {
        props.fetchServiceTypeSTT();
      }
    });
  };

  return (
    <>
      <div className="service-catalog-container">
        <div className="service-catagories">
          <div className="field__label row">
            <label className="field__label-label" title="">
              Service Categories
            </label>
            <span className="field__label-required" />
          </div>
          {/* <TagsInput
            value={serviceCatalogCategories}
            onChange={(e) => handleChange(e)}
            inputProps={{
              className: "react-tagsinput-input",
              placeholder: "Enter Category",
            }}
            addOnBlur={true}
          /> */}
          <SquareButton
            onClick={() => onSaveCatagory()}
            content="Save"
            bsStyle={"primary"}
            className="save-button-catalog"
            disabled={
              props.isFetching ||
              serviceCatalogCategories === props.serviceCatalogCategories
            }
          />
        </div>
      </div>
      <div className="service-catalog-container">
        <div className="service-catagories">
          <div className="field__label row">
            <label className="field__label-label" title="">
              Service Technology Type
            </label>
            <span className="field__label-required" />
          </div>
          <div className="service-technology-type-list">
            <div className="service-technology-type">
              {serviceType &&
                serviceType.map((option, i) => {
                  return (
                    <div key={option.id} className="category">
                      <div className="category-inner">
                        <div className="heading">{option.name}</div>
                        {/* <TagsInput
                          value={option.technology_types_list || []}
                          onChange={(e) => handleChangeTypes(e, i)}
                          inputProps={{
                            className: "react-tagsinput-input",
                            placeholder: "Enter Type",
                          }}
                          addOnBlur={true}
                        /> */}
                      </div>
                    </div>
                  );
                })}
            </div>
            <SquareButton
              onClick={() => onSaveServiceType()}
              content="Save"
              bsStyle={"primary"}
              className="save-button-catalog"
              disabled={
                props.isFetchingType || serviceType === props.serviceType
              }
            />
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.setting.isFetching,
  serviceType: state.setting.serviceType,
  isFetchingType: state.setting.isFetchingType,
  serviceCatalogCategories: state.setting.serviceCatalogCategories,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchServiceTypeSTT: () => dispatch(fetchServiceTypeSTT()),
  editServiceTypeSTT: (data: any) => dispatch(editServiceTypeSTT(data)),
  fetchServiceCatalogCategories: () =>
    dispatch(fetchServiceCatalogCategories()),
  editServiceCatalog: (data: any) => dispatch(editServiceCatalog(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CatalogSetting);
