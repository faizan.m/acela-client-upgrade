import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";
// import ColorPicker from 'rc-color-picker'; (New Component)
import Input from '../../components/Input/input';
import { getConvertedColorWithOpacity } from "../../utils/CommonUtils";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import TooltipCustom from '../../components/Tooltip/tooltip';
import Spinner from "../../components/Spinner";
import "./style.scss";
import {
  SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
  SALES_ACTIVITY_PRIORITY_SETTING_FAILURE,
  getSalesActivityPrioritySetting,
  postSalesActivityPrioritySetting,
  editSalesActivityPrioritySetting,
  deleteSalesActivityPrioritySetting,
} from "../../actions/sales";

interface ISalesActivitySettingProps {
  getSalesActivityPrioritySetting: any;
  postSalesActivityPrioritySetting: any;
  editSalesActivityPrioritySetting: any;
  deleteSalesActivityPrioritySetting: any;
}

interface ISalesActivitySettingState {
  statusList: any[];
  open: boolean;
  loading: boolean;
}

class SalesActivitySetting extends React.Component<
  ISalesActivitySettingProps,
  ISalesActivitySettingState
> {

  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: ISalesActivitySettingProps) {
    super(props);

    this.state = {
      statusList: [{
        title: "",
        color: "#f8af03",
        edited: true,
        titleError: '',
        rank: 1,
        rankError: ''
      }],
      open: true,
      loading: false,
    };
  }

  componentDidMount() {
    this.setState({ loading: true })
    this.props.getSalesActivityPrioritySetting().then(
      action => {
        this.setState({ loading: false, statusList: action.response })
      }
    )
  }

  addNew = () => {
    const newState = cloneDeep(this.state);
    newState.statusList.push({
      title: "",
      color: "#f8af03",
      rank: "1"
    });

    this.setState(newState);
  };

  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    if (rowData.id) {
      this.props.deleteSalesActivityPrioritySetting(rowData, rowData.id).then(
        action => {
          if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS) {
            newState.statusList.splice(index, 1);
          }
          if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_FAILURE) {
            newState.statusList[index].titleError = `Couldn't delete`;
          }
          this.setState(newState);
        }
      )
    } else {
      newState.statusList.splice(index, 1);
      this.setState(newState);
    }
  };

  validateForm = (rowData, index) => {
    const newState: ISalesActivitySettingState = cloneDeep(this.state);
    let isValid = true;

    const isDuplicatePriorityTitle = this.state.statusList.filter((obj) =>
      obj.id && obj.id !== rowData.id && obj.title === rowData.title);

    const isDuplicatePriorityRank = this.state.statusList.filter((obj) =>
      obj.id && obj.id !== rowData.id && obj.rank === rowData.rank);

    if (isDuplicatePriorityTitle.length > 0) {
      newState.statusList[index].edited = true;
      newState.statusList[index].titleError = "Status with this title already exists.";
      isValid = false;
    }

    if (isDuplicatePriorityRank.length > 0) {
      newState.statusList[index].edited = true;
      newState.statusList[index].rankError = "Status with this rank already exists.";
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  onSaveRowClick = (e, index, rowData) => {
    const newState = cloneDeep(this.state);

    if (!this.validateForm(rowData, index)) return;

    if (rowData.id) {
      this.props.editSalesActivityPrioritySetting(rowData, rowData.id).then(
        action => {
          if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS) {
            newState.statusList[index].edited = false;
            newState.statusList[index].titleError = '';
            newState.statusList[index].rankError = '';
          }
          if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_FAILURE) {
            newState.statusList[index].edited = true;
            newState.statusList[index].titleError = action.errorList.data.title && action.errorList.data.title.join(' ');
            newState.statusList[index].rankError = action.errorList.data.rank && action.errorList.data.rank.join(' ');
          }
          this.setState(newState);
        }
      )
    } else {
      this.props.postSalesActivityPrioritySetting(rowData).then(
        action => {
          if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS) {
            newState.statusList[index].edited = false;
            newState.statusList[index].titleError = '';
            newState.statusList[index].rankError = '';
            newState.statusList[index].id = action.response.id;

          }
          if (action.type === SALES_ACTIVITY_PRIORITY_SETTING_FAILURE) {
            newState.statusList[index].edited = true;
            newState.statusList[index].titleError = action.errorList.data.title && action.errorList.data.title.join(' ');
            newState.statusList[index].rankError = action.errorList.data.rank && action.errorList.data.rank.join(' ');
          }
          this.setState(newState);
        }
      )
    }
  };

  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].edited = true;
    newState.statusList[index][e.target.name] = e.target.value;
    newState.statusList[index].titleError = "";
    this.setState(newState);
  };

  handleChangeColor = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].color = e.color;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };

  seDefault = (row, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList.map((x, sIndex) => {
      newState.statusList[sIndex].is_default = false;
    });
    newState.statusList[index].is_default = true;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };

  handleRankChange = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].rank = e.target.value;
    newState.statusList[index].edited = true;
    newState.statusList[index].rankError = "";
    this.setState(newState);
  };

  render() {
    const statusList = this.state.statusList || [];

    return (
      <div className="sales-activity-container">
        <div className="activity-status">
          {statusList && statusList.length > 0 && !this.state.loading && (
            <div className="col-md-12 status-row">
              <label className="col-md-3 field__label-label">Label</label>
              <label className="col-md-2 field__label-label color-picker">
                Choose Color
              </label>
              <label className="col-md-2 field__label-label">Preview</label>
              <label className="col-md-2 field__label-label">
                <div style={{ display: "flex"}}>
                  Rank
                  <TooltipCustom>Lower Value means Higher Priority</TooltipCustom>
                </div>
              </label>
            </div>
          )}
          {this.state.loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          )}
          {statusList.length === 0 && !this.state.loading && (
            <div className="no-status col-md-6">No status available</div>
          )}
          {statusList &&
            !this.state.loading &&
            statusList.map((row, index) => (
              <div className="col-md-12 status-row" key={index}>
                <Input
                  field={{
                    label: "",
                    type: "TEXT",
                    value: row.title,
                    isRequired: false,
                  }}
                  width={3}
                  labelIcon={"info"}
                  name="title"
                  onChange={(e) => this.handleChangeList(e, index)}
                  placeholder={`Enter Status`}
                  error={
                    row.titleError && {
                      errorState: "error",
                      errorMessage: row.titleError,
                    }
                  }
                  disabled={row.title === "Completed"}
                />
                <div className="field-section color-picker  col-md-2">
                  {/* <ColorPicker
                    color={row.color}
                    onChange={(e) => this.handleChangeColor(e, index)}
                    placement="topLeft"
                    defaultColor="#f8af03"
                    enableAlpha={false}
                  /> */}
                </div>
                <div className="color-review-tag  col-md-2">
                  <div
                    className="field-section color-preview"
                    title={row.title}
                  >
                    <div
                      style={{
                        backgroundColor: row.color,
                        borderColor: row.color,
                      }}
                      className="left-column"
                    />
                    <div
                      style={{
                        color: "black",
                        background: getConvertedColorWithOpacity(row.color),
                      }}
                      className="text"
                    >
                      {row.title}
                    </div>
                  </div>
                  {
                    (row.is_default ? (
                      <div className="default"> Default</div>
                    ) : (
                      <div
                        className="set-default"
                        onClick={(e) => {
                          this.seDefault(row, index);
                        }}
                      >
                        Set Default
                      </div>
                  ))}
                
                </div>
                <Input
                  field={{
                    label: '',
                    type: "NUMBER",
                    value: row.rank,
                    isRequired: false,

                  }}
                  minimumValue={"1"}
                  width={1}
                  name="rank"
                  placeholder=" "
                  onChange={e => this.handleRankChange(e, index)}
                  loading={this.state.loading}
                  error={
                    row.rankError && {
                      errorState: "error",
                      errorMessage: row.rankError,
                    }
                  }
                  className="sales-rank"
                />
                {row.edited && row.title && (
                  <img
                    className="saved col-md-1"
                    alt=""
                    src={"/assets/icons/tick-blue.svg"}
                    onClick={(e) => this.onSaveRowClick(e, index, row)}
                  />
                )}
                {row.title !== "Completed" && !row.is_default && !row.in_use && (
                  <SmallConfirmationBox
                    className="remove col-md-1"
                    onClickOk={() => this.onDeleteRowClick(index, row)}
                    text={"Priority"}
                  />
                )}
              </div>
            ))}
          <div onClick={this.addNew} className="add-new-status">
            Add Priority
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getSalesActivityPrioritySetting: (data: any) =>
    dispatch(getSalesActivityPrioritySetting()),
  postSalesActivityPrioritySetting: (data: any) =>
    dispatch(postSalesActivityPrioritySetting(data)),
  editSalesActivityPrioritySetting: (data: any, id: number) =>
    dispatch(editSalesActivityPrioritySetting(data, id)),
  deleteSalesActivityPrioritySetting: (data: any, id: number) =>
    dispatch(deleteSalesActivityPrioritySetting(data, id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SalesActivitySetting);
