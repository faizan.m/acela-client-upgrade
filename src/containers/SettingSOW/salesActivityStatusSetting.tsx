import { cloneDeep } from "lodash";
import React from "react";
import { connect } from "react-redux";
// import ColorPicker from 'rc-color-picker'; (New Component)
import Input from '../../components/Input/input';
import { getConvertedColorWithOpacity } from "../../utils/CommonUtils";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import Checkbox from "../../components/Checkbox/checkbox";
import Spinner from "../../components/Spinner";
import "./style.scss";
import {
  SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
  SALES_ACTIVITY_STATUS_SETTING_FAILURE,
  getSalesActivityStatusSetting,
  postSalesActivityStatusSetting,
  editSalesActivityStatusSetting,
  deleteSalesActivityStatusSetting,
} from "../../actions/sales";

interface ISalesActivitySettingProps {
  getSalesActivityStatusSetting: any;
  postSalesActivityStatusSetting: any;
  editSalesActivityStatusSetting: any;
  deleteSalesActivityStatusSetting: any;
}

interface ISalesActivitySettingState {
  statusList: any[];
  open: boolean;
  loading: boolean;
  defaultCloseStatusId: undefined;
}

class SalesActivitySetting extends React.Component<
  ISalesActivitySettingProps,
  ISalesActivitySettingState
> {

  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: ISalesActivitySettingProps) {
    super(props);

    this.state = {
      statusList: [{
        title: "",
        color: "#f8af03",
        edited: true,
        titleError: '',
        is_closed: false,
        is_default_closed: false,
        is_saving: false,
      }],
      open: true,
      loading: false,
      defaultCloseStatusId: undefined
    };
  }

  componentDidMount() {
    this.getSalesActivityStatusSetting();
  }

  getSalesActivityStatusSetting = () => {
    this.setState({ loading: true });
    this.props.getSalesActivityStatusSetting().then(
      action => {

        const defaultCloseStatusId = action.response.filter((val) => { 
          return val.is_default_closed_status
        }).map((obj) => {
            return obj.id;
        });

        this.setState({
          loading: false,
          statusList: action.response,
          defaultCloseStatusId: defaultCloseStatusId.length > 0 && defaultCloseStatusId[0]
        })
      }
    )
  }

  addNew = () => {
    const newState = cloneDeep(this.state);
    newState.statusList.push({
      title: "",
      color: "#f8af03",
    });

    this.setState(newState);
  };

  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    if (rowData.id) {
      this.props.deleteSalesActivityStatusSetting(rowData, rowData.id).then(
        action => {
          if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
            newState.statusList.splice(index, 1);
          }
          if (action.type === SALES_ACTIVITY_STATUS_SETTING_FAILURE) {
            newState.statusList[index].titleError = `Couldn't delete`;
          }
          this.setState(newState);
        }
      )
    } else {
      newState.statusList.splice(index, 1);
      this.setState(newState);
    }
  };

  validateForm = (rowData, index) => {
    const newState: ISalesActivitySettingState = cloneDeep(this.state);
    let isValid = true;

    const isDuplicateStatusTitle = this.state.statusList.filter((obj) =>
      obj.id && obj.id !== rowData.id && obj.title === rowData.title
    );

    if (isDuplicateStatusTitle.length > 0) {
      newState.statusList[index].edited = true;
      newState.statusList[index].titleError = "Status with this title already exists.";
      isValid = false;
    }

    this.setState(newState);

    return isValid;
  }

  onSaveRowClick = (e, index, rowData) => {
    this.setState({ loading: true });
    const newState = cloneDeep(this.state);
    (newState as any).loading = true;
    this.setState(newState);

    if (!this.validateForm(rowData, index)) return;

    if (rowData.id) {
      this.props.editSalesActivityStatusSetting(rowData, rowData.id).then(
        action => {

          if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
            newState.statusList[index].edited = false;
            newState.statusList[index].titleError = '';
            this.getSalesActivityStatusSetting();
          }

          if (action.type === SALES_ACTIVITY_STATUS_SETTING_FAILURE) {
            newState.statusList[index].edited = true;
            newState.statusList[index].titleError = action.errorList.data.title.join(' ');
          }
          (newState as any).loading = false;
          this.setState(newState);
        }
      )
    } else {
      this.props.postSalesActivityStatusSetting(rowData).then(
        action => {
          if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
            newState.statusList[index].edited = false;
            newState.statusList[index].titleError = '';
            newState.statusList[index].id = action.response.id;
            this.getSalesActivityStatusSetting();
          }

          if (action.type === SALES_ACTIVITY_STATUS_SETTING_FAILURE) {
            newState.statusList[index].edited = true;
            newState.statusList[index].titleError = action.errorList.data.title.join(' ');
          }
          (newState as any).loading = false;
          this.setState(newState);
        }
      )
    }
  };

  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].edited = true;
    newState.statusList[index][e.target.name] = e.target.value;
    newState.statusList[index].titleError = "";
    this.setState(newState);
  };

  handleChangeColor = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].color = e.color;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };

  seDefault = (row, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList.map((x, sIndex) => {
      newState.statusList[sIndex].is_default = false;
    });
    newState.statusList[index].is_default = true;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };

  handleChangeCheckBox = (e, index) => {
    const newState = cloneDeep(this.state);
    const value = e.target.checked;
    newState.statusList[index].is_closed = value;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };

  handleDefaultChangeCheckBox = (e, index) => {
    const newState = cloneDeep(this.state);
    const value = e.target.checked;
    newState.statusList[index].is_default_closed_status = value;
    newState.statusList[index].edited = true;
    (newState.defaultCloseStatusId as any) = newState.statusList[index].id;
    this.setState(newState);
  };

  render() {
    const statusList = this.state.statusList || [];

    return (
      <div className="sales-activity-container">
        <div className="activity-status">
          {statusList && statusList.length > 0 && !this.state.loading && (
            <div className="col-md-12 status-row">
              <label className="col-md-3 field__label-label">Label</label>
              <label className="col-md-2 field__label-label color-picker">
                Choose Color
              </label>
              <label className="col-md-2 field__label-label">Preview</label>
              <label className="col-md-2 field__label-label sales-status-check">Is Closed Status</label>
              <label className="col-md-2 field__label-label sales-status-check">Is Default Closed Status</label>
            </div>
          )}
          {this.state.loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          )}
          {statusList.length === 0 && !this.state.loading && (
            <div className="no-status col-md-6">No status available</div>
          )}
          {statusList &&
            statusList.map((row, index) => (
              <div className="col-md-12 status-row" key={index}>
                <Input
                  field={{
                    label: "",
                    type: "TEXT",
                    value: row.title,
                    isRequired: false,
                  }}
                  width={3}
                  labelIcon={"info"}
                  name="title"
                  onChange={(e) => this.handleChangeList(e, index)}
                  placeholder={`Enter Status`}
                  error={
                    row.titleError && {
                      errorState: "error",
                      errorMessage: row.titleError,
                    }
                  }
                  disabled={row.title === "Completed"}
                />
                <div className="field-section color-picker  col-md-2">
                  {/* <ColorPicker
                    color={row.color}
                    onChange={(e) => this.handleChangeColor(e, index)}
                    placement="topLeft"
                    defaultColor="#f8af03"
                    enableAlpha={false}
                  /> */}
                </div>
                <div className="color-review-tag  col-md-2">
                  <div
                    className="field-section color-preview"
                    title={row.title}
                  >
                    <div
                      style={{
                        backgroundColor: row.color,
                        borderColor: row.color,
                      }}
                      className="left-column"
                    />
                    <div
                      style={{
                        color: "black",
                        background: getConvertedColorWithOpacity(row.color ? row.color : "#f8af03"),
                      }}
                      className="text"
                    >
                      {row.title}
                    </div>
                  </div>
                  {
                    (row.is_default ? (
                      <div className="default"> Default</div>
                    ) : (
                      <div
                        className="set-default"
                        onClick={(e) => {
                          this.seDefault(row, index);
                        }}
                      >
                        Set Default
                      </div>
                  ))}
                </div>
                <div className="field-section col-md-1 searchbox_container">
                  <Checkbox
                    isChecked={row.is_closed}
                    name="is_closed"
                    onChange={(e) => this.handleChangeCheckBox(e, index)}
                  />
                </div>
                <div className="field-section col-md-1 searchbox_container">
                  <Checkbox
                    isChecked={row.id === this.state.defaultCloseStatusId}
                    name="defaultCloseStatusId"
                    onChange={(e) => this.handleDefaultChangeCheckBox(e, index)}
                    disabled={!row.is_closed}
                  />
                </div>
                {row.edited && row.title && (
                  <img
                    className="saved col-md-1"
                    alt=""
                    src={"/assets/icons/tick-blue.svg"}
                    onClick={(e) => this.onSaveRowClick(e, index, row)}
                  />
                )} 
                {row.title !== "Completed" && !row.is_default && !row.in_use && 
                  row.id !== this.state.defaultCloseStatusId && (
                    <SmallConfirmationBox
                      className="remove col-md-1"
                      onClickOk={() => this.onDeleteRowClick(index, row)}
                      text={"Status"}
                    />
                )}
              </div>
            ))}
          <div onClick={this.addNew} className="add-new-status">
            Add Status
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getSalesActivityStatusSetting: () =>
    dispatch(getSalesActivityStatusSetting()),
  postSalesActivityStatusSetting: (data: any) =>
    dispatch(postSalesActivityStatusSetting(data)),
  editSalesActivityStatusSetting: (data: any, id: number) =>
    dispatch(editSalesActivityStatusSetting(data, id)),
  deleteSalesActivityStatusSetting: (data: any, id: number) =>
    dispatch(deleteSalesActivityStatusSetting(data, id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SalesActivitySetting);
