import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  vendorAliasMappingCRUD,
  VENDOR_ALIAS_MAPPING_FAILURE,
  VENDOR_ALIAS_MAPPING_SUCCESS,
} from "../../actions/setting";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";

interface VendorMappingModalProps {
  show: boolean;
  isFetchingVendors: boolean;
  vendorOptions: IPickListOptions[];
  mapping: IVendorAliasMapping;
  closeModal: (refresh?: boolean) => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  vendorDescriptionCRUD: (
    request: HTTPMethods,
    data?: IVendorAliasMapping
  ) => Promise<any>;
}

const EmptyVendorAliasMapping: IVendorAliasMapping = {
  vendor_name: "",
  vendor_crm_id: null,
  resource_description: "",
};

const VendorMappingModal: React.FC<VendorMappingModalProps> = (props) => {
  const [saving, setSaving] = useState<boolean>(false);
  const [currentMapping, setCurrentMapping] = useState<IVendorAliasMapping>({
    ...EmptyVendorAliasMapping,
  });

  useEffect(() => {
    setCurrentMapping(props.mapping);
  }, [props.mapping]);

  const handleChangeVendorName = (e: React.ChangeEvent<HTMLInputElement>) => {
    const vendor_crm_id: number = Number(e.target.value);
    const vendor_name: string = props.vendorOptions.find(
      (el) => el.value === vendor_crm_id
    ).label as string;
    setCurrentMapping((prevState) => ({
      ...prevState,
      vendor_name,
      vendor_crm_id,
    }));
  };

  const handleChangeDescription = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCurrentMapping((prevState) => ({
      ...prevState,
      resource_description: e.target.value,
    }));
  };

  const onSaveMapping = () => {
    setSaving(true);
    props
      .vendorDescriptionCRUD(
        currentMapping.id ? "put" : "post",
        currentMapping
      )
      .then((action) => {
        if (action.type === VENDOR_ALIAS_MAPPING_SUCCESS) {
          props.addSuccessMessage(
            `Vendor Mapping ${
              currentMapping.id ? "Updated" : "Created"
            } Successfully!`
          );
          props.closeModal(true);
        } else if (action.type === VENDOR_ALIAS_MAPPING_FAILURE) {
          const errorMessage: string = action.errorList.data.message;
          props.addErrorMessage(
            errorMessage ? errorMessage : "Error saving vendor alias mapping!"
          );
        }
      })
      .finally(() => setSaving(false));
  };

  return (
    <ModalBase
      show={props.show}
      onClose={() => props.closeModal(false)}
      hideCloseButton={saving}
      titleElement={`${
        currentMapping.id ? "Update" : "Create"
      } Vendor Description Mapping`}
      bodyElement={
        <div className="vendor-alias-edit-container">
          <div className="loader">
            <Spinner show={saving} />
          </div>
          <Input
            field={{
              label: "Vendor Name",
              type: "PICKLIST",
              value: currentMapping.vendor_crm_id,
              options: currentMapping.id
                ? [
                    {
                      value: currentMapping.vendor_crm_id,
                      label: currentMapping.vendor_name,
                    },
                  ]
                : props.vendorOptions,
              isRequired: true,
            }}
            className="vendor-name"
            width={6}
            multi={false}
            name="vendor_name"
            onChange={handleChangeVendorName}
            placeholder={`Select Vendor Name`}
            loading={props.isFetchingVendors}
            disabled={Boolean(currentMapping.id)}
          />
          <Input
            field={{
              label: "Description",
              type: "TEXT",
              value: currentMapping.resource_description,
              isRequired: true,
            }}
            className="resource-description"
            width={6}
            name="resource_description"
            onChange={handleChangeDescription}
            placeholder={`Enter Vendor Default Description`}
          />
        </div>
      }
      footerElement={
        <div>
          <SquareButton
            content={`Cancel`}
            bsStyle={"default"}
            onClick={() => props.closeModal(false)}
            disabled={saving}
          />
          <SquareButton
            content={currentMapping.id ? "Update" : "Save"}
            bsStyle={"primary"}
            onClick={onSaveMapping}
            disabled={
              saving ||
              !currentMapping.vendor_crm_id ||
              !currentMapping.resource_description
            }
          />
        </div>
      }
      className={"vendor-mapping-modal"}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  vendorOptions: state.sow.vendorOptions,
  isFetchingVendors: state.sow.isFetchingVendors,
});

const mapDispatchToProps = (dispatch: any) => ({
  vendorDescriptionCRUD: (request: HTTPMethods, data?: IVendorAliasMapping) =>
    dispatch(vendorAliasMappingCRUD(request, data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(VendorMappingModal);
