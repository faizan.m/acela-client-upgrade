import { debounce } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { getReportDownloadStats } from '../../actions/report';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { utcToLocalInLongFormat } from '../../utils/CalendarUtil';
import './style.scss';

enum PageType {
  Inventory,
}
interface IStatisticsProps {
  getReportDownloadStats: any;
  isFetching: any;
  reportDownloadStats: any;
}

interface IStatisticsState {
  currentPage: {
    pageType: PageType;
  };
  rows: any[];
  reset: boolean;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationServiceCatlogFilterParams;
  };
}

class StatisticsList extends React.Component<
  IStatisticsProps,
  IStatisticsState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  private debouncedFetch;

  constructor(props: IStatisticsProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Inventory,
    },
    variables: [],
    rows: [],
    pagination: {
      totalRows: 0,
      currentPage: 0,
      totalPages: 0,
      params: {},
    },
    reset: false,
  });

  componentDidUpdate(prevProps: IStatisticsProps) {
    if (
      this.props.reportDownloadStats &&
      this.props.reportDownloadStats !== prevProps.reportDownloadStats
    ) {
      this.setRows(this.props);
    }
  }

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getReportDownloadStats(newParams);
  };
  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  setRows = (nextProps: IStatisticsProps) => {
    const customersResponse = nextProps.reportDownloadStats;
    const reportDownloadStats: any[] = customersResponse.results;
    const rows: any[] = reportDownloadStats;

    this.setState(prevState => ({
      ...prevState,
      reset: false,
      rows,
      reportDownloadStats,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  renderTopBarTable = () => {
    return (
      <div className="service-catalog-listing__actions ">
        <div className="header-panel">
          <Input
            field={{
              value: this.state.pagination.params.search,
              label: '',
              type: "SEARCH",
            }}
            width={4}
            name="searchString"
            onChange={this.onSearchStringChange}
            placeholder="Search"
            className="search"
          />
        </div>
      </div>
    );
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="report__header">
        {
          <div
            className={`report__header-link ${
              currentPage.pageType === PageType.Inventory
                ? 'report__header-link--active'
                : ''
            }`}
            onClick={e => this.changePage(PageType.Inventory)}
          >
            All Customer statistics
          </div>
        }
      </div>
    );
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  render() {
    const ReportDownloadLogColumns: any = [
      {
        accessor: 'name',
        Header: 'Name',
        sortable: false,
      },
      {
        accessor: 'report_type',
        Header: 'Report Type',
        sortable: false,
      },
      {
        accessor: 'report_sub_type',
        Header: 'Report Sub Type',
        sortable: false,
      },
      {
        accessor: 'download_count',
        Header: 'Download Count',
        sortable: false,
      },
      {
        accessor: 'last_downloaded_on',
        Header: 'Last Downloaded On',
        sortable: false,

        width: 200,
        Cell: id => (
          <div className="pl-15">
            {id.value ? utcToLocalInLongFormat(id.value) : 'N.A.'}
          </div>
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: 'id',
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
      reset: this.state.reset,
    };

    return (
      <div className="report-statistics report">
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        {this.renderTopBar()}
        <Table
          columns={ReportDownloadLogColumns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBarTable()}
          className={`customer-listing__table ${
            this.props.isFetching ? `loading` : ``
          }`}
          onRowClick={null}
          manualProps={manualProps}
          loading={this.props.isFetching}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  reportDownloadStats: state.report.reportDownloadStats,
  isFetching: state.report.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  getReportDownloadStats: (params?: IServerPaginationParams) =>
    dispatch(getReportDownloadStats(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StatisticsList);
