import React, { useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  deviceReportBundleRequest,
  DEVICE_REPORT_BUNDLE_SUCCESS,
  DEVICE_REPORT_BUNDLE_FAILURE,
} from "../../actions/report";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";

interface DeviceReportBundleProps {
  customers: ICustomerShort[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  sendReport: (customerId: number) => Promise<any>;
}

const DeviceReportBundle: React.FC<DeviceReportBundleProps> = (props) => {
  const [customer, setCustomer] = useState<number>(0);
  const [downloading, setDownloading] = useState<boolean>(false);
  const [error, setError] = useState<IFieldValidation>({
    errorState: "success",
    errorMessage: "",
  });
  const customerOptions: IPickListOptions[] = useMemo(
    () =>
      props.customers
        ? props.customers.map((cust) => ({
            value: cust.id,
            label: cust.name,
          }))
        : [],
    [props.customers]
  );

  const handleChangeCustomer = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCustomer(Number(e.target.value));
  };

  const validate = () => {
    let isValid: boolean = true;
    if (!customer) {
      isValid = false;
      setError({
        errorState: "error",
        errorMessage: "This field is required",
      });
    } else
      setError({
        errorState: "success",
        errorMessage: "",
      });
    return isValid;
  };

  const sendReportEmail = () => {
    if (validate()) {
      setDownloading(true);
      props
        .sendReport(customer)
        .then((action) => {
          if (action.type === DEVICE_REPORT_BUNDLE_SUCCESS) {
            props.addSuccessMessage(
              "Report generation is in progress, it will take some time. Email notification will be sent."
            );
          } else if (action.type === DEVICE_REPORT_BUNDLE_FAILURE) {
            props.addErrorMessage("An error occurred!");
          }
        })
        .finally(() => setDownloading(false));
    }
  };

  return (
    <div className="fire-report-container">
      <Spinner show={downloading} className="fire-report-loader" />
      <div className="fire-report-filters">
        <div className="fire-report-row">
          <Input
            field={{
              label: "Customer",
              type: "PICKLIST",
              value: customer,
              options: customerOptions,
              isRequired: true,
            }}
            width={6}
            multi={false}
            name="customer"
            onChange={handleChangeCustomer}
            placeholder={`Select Customer`}
            disabled={downloading}
            error={error}
          />
        </div>
      </div>
      <SquareButton
        onClick={sendReportEmail}
        content="Send Report"
        bsStyle={"primary"}
        className="download-report-bundle"
        title="Device Report Bundle will be sent via email"
        disabled={downloading}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  sendReport: (customerId: number) =>
    dispatch(deviceReportBundleRequest(customerId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(DeviceReportBundle);
