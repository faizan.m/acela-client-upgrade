import moment from "moment";
import React, { Component } from "react";
import { connect } from "react-redux";
// import DateRangePicker from "react-daterange-picker"; (New Component)
import {
  downloadFinanceReport,
  fetchFinanceReportStatuses,
  FETCH_FINANCE_REPORT_SUCCESS,
  FETCH_FINANCE_REPORT_FAILURE,
} from "../../actions/report";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";

interface IFinanceReportProps {
  isFetching: boolean;
  customers: ICustomerShort[];
  statuses: IPickListOptions[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchStatuses: () => void;
  downloadFinanceReport: (data: IFinanceReportFilters) => Promise<any>;
}

interface IFinanceReportState {
  status: number;
  customer: number;
  dateFilter: string;
  dateError: IFieldValidation;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
  filterDateValue: any;
  filterDateFrom: string;
  filterDateTo: string;
  downloadingReport: boolean;
}

const DateOptions: IPickListOptions[] = [
  {
    value: "po_date",
    label: "Purchase Date",
  },
  {
    value: "opportunity_won_date",
    label: "Opportunity Won Date",
  },
  {
    value: "expected_ship_date",
    label: "Estimated Ship Date",
  },
  {
    value: "payment_date",
    label: "Invoice Paid Date",
  },
  {
    value: "received_date",
    label: "Item Received Date",
  },
  {
    value: "created_date",
    label: "Invoice Created Date",
  },
];

const getFieldValidation = (msg?: string): IFieldValidation => {
  return {
    errorState: msg ? "error" : "success",
    errorMessage: msg ? msg : "",
  };
};

class FinanceReport extends Component<
  IFinanceReportProps,
  IFinanceReportState
> {
  constructor(props: IFinanceReportProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    status: 1,
    customer: null,
    dateFilter: null,
    dateError: getFieldValidation(),
    isDatePickerSelected: false,
    isDateValueSelected: false,
    filterDateValue: undefined,
    filterDateFrom: "",
    filterDateTo: "",
    downloadingReport: false,
  });

  componentDidMount() {
    this.props.fetchStatuses();
  }

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const updatedState: Partial<IFinanceReportState> = {
      [event.target.name]: event.target.value,
    };
    this.setState(updatedState as IFinanceReportState);
  };

  getCustomerOptions = (): IPickListOptions[] => {
    return this.props.customers
      ? this.props.customers.map((el) => ({
          value: el.id,
          label: el.name,
        }))
      : [];
  };

  onDateRangeSelect = (value, states) => {
    this.setState({
      filterDateFrom: fromISOStringToFormattedDate(
        value.start._d,
        "YYYY-MM-DD"
      ),
      filterDateTo: fromISOStringToFormattedDate(value.end._d, "YYYY-MM-DD"),
      filterDateValue: value,
      isDatePickerSelected: false,
      isDateValueSelected: true,
    });
  };

  resetDate = () => {
    this.setState({
      filterDateFrom: "",
      filterDateTo: "",
      filterDateValue: undefined,
      isDatePickerSelected: false,
      isDateValueSelected: false,
    });
  };

  onDownloadFinanceReport = () => {
    if (this.state.dateFilter && !this.state.filterDateFrom) {
      this.setState({
        dateError: getFieldValidation("Please select a date range"),
      });
      return;
    }
    this.setState({
      downloadingReport: true,
      dateError: getFieldValidation(),
    });

    let payload: IFinanceReportFilters = {
      status: this.state.status === 1 ? "" : String(this.state.status), // For Status Type "All" with id = 1, send empty string
      customer_id: this.state.customer ? this.state.customer : undefined,
    };
    if (this.state.dateFilter)
      payload = Object.assign(payload, {
        [this.state.dateFilter + "_after"]: this.state.filterDateFrom
          ? moment(this.state.filterDateFrom)
              .subtract(1, "day")
              .format("YYYY-MM-DD")
          : "",
        [this.state.dateFilter + "_before"]: this.state.filterDateTo
          ? moment(this.state.filterDateTo)
              .add(1, "day")
              .format("YYYY-MM-DD")
          : "",
      });

    this.props
      .downloadFinanceReport(payload)
      .then((action) => {
        if (action.type === FETCH_FINANCE_REPORT_SUCCESS) {
          const text = action.response;
          let dateString: string;
          let customerName: string = "";
          if (this.state.customer)
            customerName =
              "_" +
              this.props.customers.find((el) => el.id == this.state.customer)
                .name;

          if (this.state.dateFilter && this.state.filterDateFrom)
            dateString = `_${this.state.dateFilter}_${moment(
              this.state.filterDateFrom
            ).format("MM-DD-YYYY")}_to_${moment(this.state.filterDateTo).format(
              "MM-DD-YYYY"
            )}`;
          else dateString = `_${new Date().toISOString()}`;
          const filename = `FinanceReport${customerName}${dateString}.csv`;

          var blob = new Blob([text], { type: "text/csv;charset=utf-8;" });
          var link = document.createElement("a");
          if (link.download !== undefined) {
            // feature detection
            // Browsers that support HTML5 download attribute
            var url = URL.createObjectURL(blob);
            link.setAttribute("href", url);
            link.setAttribute("download", filename);
            link.style.visibility = "hidden";
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            this.props.addSuccessMessage("Report downloaded successfully!");
          }
          this.setState({ status: 1, dateFilter: null, customer: null });
          this.resetDate();
        } else if (action.type === FETCH_FINANCE_REPORT_FAILURE) {
          this.props.addErrorMessage("Error downloading finance report!");
        }
      })
      .finally(() => this.setState({ downloadingReport: false }));
  };

  render() {
    const currentDateLabel: string = this.state.dateFilter
      ? (DateOptions.find((el) => el.value === this.state.dateFilter)
          .label as string)
      : "";
    return (
      <div className="finance-report-container">
        <Spinner
          show={this.state.downloadingReport}
          className="finance-report-loader"
        />
        <div className="finance-report-filters">
          <div className="finance-report-row">
            <Input
              field={{
                label: "Status",
                type: "PICKLIST",
                value: this.state.status,
                options: this.props.statuses,
                isRequired: true,
              }}
              width={5}
              name="status"
              onChange={this.handleChange}
              placeholder={`Select Status`}
              loading={this.props.isFetching}
            />
            <Input
              field={{
                label: "Customer",
                type: "PICKLIST",
                value: this.state.customer,
                options: this.getCustomerOptions(),
                isRequired: false,
              }}
              width={5}
              name="customer"
              onChange={this.handleChange}
              placeholder={`Select Customer`}
            />
          </div>
          <div className="date-filter-container">
            <Input
              field={{
                label: "Date Filter",
                type: "PICKLIST",
                value: this.state.dateFilter,
                options: DateOptions,
                isRequired: false,
              }}
              width={5}
              name="dateFilter"
              onChange={this.handleChange}
              placeholder={`Select Date Filter`}
              error={this.state.dateError}
            />
            {this.state.dateFilter && (
              <div className="date-range-box">
                <SquareButton
                  onClick={() =>
                    this.setState({
                      isDatePickerSelected: !this.state.isDatePickerSelected,
                      isDateValueSelected: false,
                    })
                  }
                  content=""
                  bsStyle={"default"}
                  className="date-button"
                />
                {/* {this.state.isDatePickerSelected &&
                  !this.state.isDateValueSelected && (
                    <DateRangePicker
                      value={this.state.filterDateValue}
                      onSelect={this.onDateRangeSelect}
                      singleDateRange={true}
                      minimumDate={new Date("1990/01/01")}
                      maximumDate={new Date()}
                      numberOfCalendars={1}
                      showLegend={true}
                      clearAriaLabel={"reset"}
                      ClearIcon={"reset"}
                    />
                  )} */}
                {!this.state.isDatePickerSelected &&
                  this.state.isDateValueSelected && (
                    <div className="date-value" title={"MM-DD-YYYY"}>
                      {fromISOStringToFormattedDate(
                        this.state.filterDateFrom,
                        "MM-DD-YYYY"
                      )}
                      {" \u2013 "}
                      {fromISOStringToFormattedDate(
                        this.state.filterDateTo,
                        "MM-DD-YYYY"
                      )}
                    </div>
                  )}
                {!this.state.isDatePickerSelected &&
                  this.state.isDateValueSelected && (
                    <div
                      onClick={(e) => this.resetDate()}
                      className="reset-button"
                    >
                      x
                    </div>
                  )}
                {!this.state.isDatePickerSelected &&
                  !this.state.isDateValueSelected && (
                    <div className="date-value">{`Filter on ${currentDateLabel}`}</div>
                  )}
              </div>
            )}
          </div>
        </div>
        <SquareButton
          onClick={this.onDownloadFinanceReport}
          content="Download"
          bsStyle={"primary"}
          className="download-finance-report"
          disabled={this.state.downloadingReport}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.report.isFetching,
  statuses: state.report.financeStatuses,
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  downloadFinanceReport: (data: IFinanceReportFilters) =>
    dispatch(downloadFinanceReport(data)),
  fetchStatuses: () => dispatch(fetchFinanceReportStatuses()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FinanceReport);
