import React from 'react';
import { connect } from 'react-redux';
import { cloneDeep } from 'lodash';

import {
  CIRCUIT_REPORT_SUCCESS,
  downloadCircuitReportCU,
  downloadCircuitReportPU,
  getCircuitInfoReportTypes,
} from '../../actions/report';
import SquareButton from '../../components/Button/button';
import Select from '../../components/Input/Select/select';
import Spinner from '../../components/Spinner';
import './style.scss';

enum PageType {
  circuitReport,
}
interface IProvideReportProps extends ICommonProps {
  reportProviderCustomerUser: any;
  user: ISuperUser;
  reportError: string;
  isFetching: boolean;
  customers: ICustomerShort[];
  downloadCircuitReportPU: any;
  downloadCircuitReportCU: any;
  reportDownloading: boolean;
  getCircuitInfoReportTypes: any;
  cuircuitInfoReportTypes: any[];
}

interface IProvideReportState {
  currentPage: {
    pageType: PageType;
  };
  reportError: string;
  customerId: any;
  type: string;
}

class ProvideReport extends React.Component<
  IProvideReportProps,
  IProvideReportState
> {
  constructor(props: IProvideReportProps) {
    super(props);

    this.state = {
      customerId: '',
      type: '',
      currentPage: {
        pageType: PageType.circuitReport,
      },
      reportError: '',
    };
  }

  componentDidMount() {
    this.props.getCircuitInfoReportTypes();
  }

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
      reportError: '',
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="report__header">
        {
          <div
            className={`report__header-link ${
              currentPage.pageType === PageType.circuitReport
                ? 'report__header-link--active'
                : ''
            }`}
            onClick={() => this.changePage(PageType.circuitReport)}
          >
            Circuit Report
          </div>
        }
      </div>
    );
  };

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };

  renderCircuitReport = () => {
    const customers = this.props.customers
      ? this.props.customers.map(cust => ({
          value: cust.id,
          label: cust.name,
        }))
      : [];

    const types = this.props.cuircuitInfoReportTypes
      ? Object.keys(this.props.cuircuitInfoReportTypes).map(
          (reportType, index) => ({
            label: this.props.cuircuitInfoReportTypes[reportType].name,
            value: reportType,
          })
        )
      : [];

    return (
      <div className="inventory">
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.reportDownloading} />
        </div>
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === 'provider' && (
            <div
              className={`provider-circuit-report__body ${
                this.props.reportDownloading ? `loading` : ``
              }`}
            >
              <div className="field__label customers">
                <label className="field__label-label">Customer</label>
                <span className="field__label-required" />
                <div className="field__input">
                  <Select
                    name="customerId"
                    value={this.state.customerId}
                    onChange={this.handleChange}
                    options={customers}
                    searchable={true}
                    multi={false}
                    placeholder="Select Customer"
                  />
                </div>
              </div>
              <div className="field__label types">
                <label className="field__label-label">Report Type</label>
                <span className="field__label-required" />
                <div className="field__input">
                  <Select
                    name="type"
                    value={this.state.type}
                    onChange={this.handleChange}
                    options={types}
                    searchable={true}
                    multi={false}
                    placeholder="Select type"
                  />
                </div>
              </div>
              <SquareButton
                onClick={() => this.downloadReport()}
                content="Download"
                bsStyle={"primary"}
                disabled={!this.state.customerId || !this.state.type}
                className="download-report"
              />
            </div>
          )}{' '}
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === 'customer' && (
            <div
              className={`customer-circuit-report__body ${
                this.props.reportDownloading ? `loading` : ``
              }`}
            >
              <div className="field__label types">
                <label className="field__label-label">Report Type</label>
                <span className="field__label-required" />
                <div className="field__input">
                  <Select
                    name="type"
                    value={this.state.type}
                    onChange={this.handleChange}
                    options={types}
                    searchable={true}
                    multi={false}
                    placeholder="Select type"
                  />
                </div>
              </div>
              <SquareButton
                onClick={() => this.downloadReport()}
                content="Download"
                bsStyle={"primary"}
                disabled={!this.state.type}
                className="download-report"
              />
            </div>
          )}
      </div>
    );
  };
  downloadReport = () => {
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'provider' &&
      this.state.customerId
    ) {
      this.props
        .downloadCircuitReportPU(this.state.customerId, this.state.type)
        .then(action => {
          if (action.type === CIRCUIT_REPORT_SUCCESS) {
            if (action.response && action.response !== '') {
              const url = action.response.file_path;
              const link = document.createElement('a');
              link.href = url;
              link.target = '_blank';
              link.setAttribute('download', action.response.file_name);
              document.body.appendChild(link);
              link.click();
            } else {
              this.setState({
                reportError: 'No circuits found for selected customer.',
              });
            }
          }
        });
    }
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.downloadCircuitReportCU(this.state.type).then(action => {
        if (action.type === CIRCUIT_REPORT_SUCCESS) {
          if (action.response && action.response !== '') {
            const url = action.response.file_path;
            const link = document.createElement('a');
            link.href = url;
            link.target = '_blank';
            link.setAttribute('download', action.response.file_name);
            document.body.appendChild(link);
            link.click();
          } else {
            this.setState({
              reportError: 'No circuits found for selected customer.',
            });
          }
        }
      });
    }
  };
  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="provider-report">
        {this.props.user &&
          this.props.user.type && (
            <div className="report">
              {this.renderTopBar()}
              {currentPage.pageType === PageType.circuitReport && (
                <div>{this.renderCircuitReport()}</div>
              )}
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  reportError: state.report.reportError,
  isFetching: state.report.isFetching,
  customers: state.customer.customersShort,
  reportDownloading: state.report.isFetching,
  cuircuitInfoReportTypes: state.report.cuircuitInfoReportTypes,
});

const mapDispatchToProps = (dispatch: any) => ({
  downloadCircuitReportPU: (customerId: number, type: string) =>
    dispatch(downloadCircuitReportPU(customerId, type)),
  downloadCircuitReportCU: (type: string) =>
    dispatch(downloadCircuitReportCU(type)),
  getCircuitInfoReportTypes: () => dispatch(getCircuitInfoReportTypes()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProvideReport);
