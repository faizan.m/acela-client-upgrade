import React from 'react';
import { cloneDeep } from 'lodash';
import { connect } from 'react-redux';

import {
  fetchContractStatuses,
  fetchSitesCustomersUser,
  fetchTypes,
} from '../../actions/inventory';
import {
  BATCH_REPORT_SUCCESS_CUSTOMER,
  editScheduledReport,
  fetchFrequencySHReport,
  getCustomerReportTypes,
  getReportCategories,
  GetScheduledReport,
  getScheduledReportByCat,
  POST_SH_REPORT_FAILURE,
  POST_SH_REPORT_SUCCESS,
  postScheduledReport,
  reportCustomerUser,
} from '../../actions/report';
import SquareButton from '../../components/Button/button';
import Select from '../../components/Input/Select/select';
import Spinner from '../../components/Spinner';
import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import './style.scss';

enum PageType {
  AdvanceReport,
  PreBuildReport,
  scheduled,
}

export interface IFiltersReport {
  site_id?: number[];
  device_type?: any[];
  contract_status_id?: number[];
  customer_id?: number[];
  report_type?: {
    name: string;
    category: string;
    type: string;
  };
}

interface IReportProps extends ICommonProps {
  fetchDevicesCustomerUser: TFetchDevicesCustomerUser;
  fetchSitesCustomersUser: TFetchSitesCustomerUser;
  fetchContractStatuses: TFetchContractStatuses;
  reportCustomerUser: any;
  fetchTypes: any;
  sites: any[];
  types: any[];
  contractStatuses: IContractStatus[];
  user: ISuperUser;
  reportError: string;
  isFetching: boolean;
  reportTypes: any[];
  getCustomerReportTypes: any;
  reportCategories: any;
  scheduledReportByCat: any;
  reportFrequencies: any;
  isFetchingSH: boolean;
  getReportCategories: any;
  getScheduledReportByCat: any;
  fetchFrequencySHReport: any;
  postScheduledReport: any;
  GetScheduledReport: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  scheduledReportList: any;
  editScheduledReport: any;
}

interface IReportState {
  currentPage: {
    pageType: PageType;
  };
  error: {
    [fieldName: string]: IFieldValidation;
  };
  filters: IFiltersReport;
  reportError: string;
  report: any;
  reportTypeList: any[];
  reportType: string;
  category: string;
  reportCatgoryType: string;
  frequency: string;
  scheduledReportByCat: any;
}

class Report extends React.Component<IReportProps, IReportState> {
  constructor(props: IReportProps) {
    super(props);

    this.state = {
      currentPage: {
        pageType: PageType.PreBuildReport,
      },
      error: {},
      filters: {
        site_id: [],
        device_type: [],
        contract_status_id: [],
        customer_id: [],
        report_type: {
          name: '',
          category: '',
          type: '',
        },
      },
      report: '',
      reportType: '',
      reportError: '',
      reportTypeList: [],
      category: '',
      reportCatgoryType: '',
      frequency: '',
      scheduledReportByCat: [],
    };
  }

  componentDidMount() {
    this.props.fetchContractStatuses();
    this.props.getCustomerReportTypes();
    this.props.fetchTypes();
    this.props.getScheduledReportByCat('DEVICES');
    this.props.GetScheduledReport();
    this.props.fetchFrequencySHReport();
    if (
      this.props.user &&
      this.props.user.type &&
      this.props.user.type === 'customer'
    ) {
      this.props.fetchSitesCustomersUser();
    }
  }

  componentDidUpdate(prevProps: IReportProps) {
    const { user, reportError, scheduledReportByCat, scheduledReportList } = this.props;
  
    if (user && user !== prevProps.user) {
      if (user.type && user.type === 'customer') {
        this.props.fetchSitesCustomersUser();
      }
    }
  
    if (reportError && reportError !== prevProps.reportError) {
      this.setState({ reportError });
    }
  
    if (scheduledReportByCat && scheduledReportByCat !== prevProps.scheduledReportByCat) {
      this.setState({ scheduledReportByCat });
    }
  
    if (scheduledReportList && scheduledReportList !== prevProps.scheduledReportList) {
      const scheduledReportByCat = cloneDeep(this.state.scheduledReportByCat);
  
      scheduledReportByCat.forEach((type, index) => {
        scheduledReportByCat[index].frequency = 'NEVER';
        scheduledReportByCat[index].report_id = '';
      });
  
      scheduledReportByCat.forEach((type, index) => {
        this.props.scheduledReportList.forEach((report, i) => {
          if (type.type === report.scheduled_report_type.type) {
            scheduledReportByCat[index].frequency = report.frequency;
            scheduledReportByCat[index].report_id = report.id;
          }
        });
      });
  
      this.setState({ scheduledReportByCat });
    }
  }
  

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  onFilterChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;

    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
      reportError: '',
    }));
  };

  onReportChange = e => {
    const reportTypeList = this.props.reportTypes[e.target.value];
    this.setState({
      report: e.target.value,
      reportTypeList,
      reportType: '',
      reportError: '',
    });
  };

  onReportTypeChange = e => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    const reportTypeList = this.state.reportTypeList[targetValue];

    this.setState(prevState => ({
      filters: {
        ...prevState.filters,
        [targetName]: reportTypeList,
      },
      reportType: targetValue,
      reportError: '',
    }));
  };

  onResetClick = () => {
    this.setState(prevState => ({
      filters: {
        site_id: [],
        device_type: [],
        contract_status_id: [],
        customer_id: [],
        report_type: {
          name: '',
          category: '',
          type: '',
        },
      },
      report: '',
      reportType: '',
      reportError: '',
    }));
  };

  onRunClick = () => {
    if (this.state.filters) {
      const filters: IFiltersReport = {};
      if (
        this.state.filters.report_type !== null &&
        this.state.filters.report_type
      ) {
        filters.report_type = this.state.filters.report_type;
      }
      if (
        this.state.filters.site_id !== null &&
        this.state.filters.site_id.length !== 0 &&
        this.state.filters.site_id !== []
      ) {
        filters.site_id = this.state.filters.site_id;
      }
      if (
        this.state.filters.device_type !== null &&
        this.state.filters.device_type.length !== 0 &&
        this.state.filters.device_type !== []
      ) {
        filters.device_type = this.state.filters.device_type;
      }
      if (
        this.state.filters.contract_status_id !== null &&
        this.state.filters.contract_status_id.length !== 0 &&
        this.state.filters.contract_status_id !== []
      ) {
        filters.contract_status_id = this.state.filters.contract_status_id;
      }
      this.props.reportCustomerUser(filters).then(action => {
        if (action.type === BATCH_REPORT_SUCCESS_CUSTOMER) {
          if (action.response && action.response !== '') {
            const url = action.response.file_path;
            const link = document.createElement('a');
            link.href = url;
            link.target = '_blank';
            link.setAttribute('download', action.response.file_name);
            document.body.appendChild(link);
            link.click();
          } else {
            this.setState({
              reportError: 'No device found for selected filter.',
            });
          }
        }
      });
    }
  };

  onReportDownloadClick = data => {
    this.setState({
      reportError: '',
    });
    this.props.reportCustomerUser({ report_type: data }).then(action => {
      if (action.type === BATCH_REPORT_SUCCESS_CUSTOMER) {
        if (action.response && action.response !== '') {
          const url = action.response.file_path;
          const link = document.createElement('a');
          link.href = url;
          link.target = '_blank';
          link.setAttribute('download', action.response.file_name);
          document.body.appendChild(link);
          link.click();
        } else {
          this.setState({
            reportError: 'No device found for selected filter.',
          });
        }
      }
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="report__header">
        <div
          className={`report__header-link ${
            currentPage.pageType === PageType.PreBuildReport
              ? 'report__header-link--active'
              : ''
          }`}
          onClick={e => this.changePage(PageType.PreBuildReport)}
        >
          Standard Report
        </div>
        <div
          className={`report__header-link ${
            currentPage.pageType === PageType.AdvanceReport
              ? 'report__header-link--active'
              : ''
          }`}
          onClick={e => this.changePage(PageType.AdvanceReport)}
        >
          Advanced Reporting
        </div>{' '}
        <div
          className={`report__header-link ${
            currentPage.pageType === PageType.scheduled
              ? 'report__header-link--active'
              : ''
          }`}
          onClick={e => this.changePage(PageType.scheduled)}
        >
          Scheduled Reports
        </div>{' '}
      </div>
    );
  };

  renderAdvanceReport = () => {
    const filters = this.state.filters;
    const sites = this.props.sites
      ? this.props.sites.map(site => ({
          value: site.site_id,
          label: site.name,
        }))
      : [];
    const types = this.props.types
      ? this.props.types.map((type, index) => ({
          value: type,
          label: type ? type : 'N.A.',
        }))
      : [];

    const reportList = this.props.reportTypes
      ? Object.keys(this.props.reportTypes).map((reportType, index) => ({
          value: reportType,
          label: reportType,
        }))
      : [];
    const contractStatuses = this.props.contractStatuses
      ? this.props.contractStatuses.map(contractStatus => ({
          value: contractStatus.contract_status_id,
          label: contractStatus.contract_status,
        }))
      : [];

    const reportTypeList = this.state.reportTypeList
      ? Object.keys(this.state.reportTypeList).map((reportType, index) => ({
          value: this.state.reportTypeList[reportType].type,
          label: this.state.reportTypeList[reportType].name,
        }))
      : [];

    return (
      <div className="inventory">
        <h5 className="filter__header">FILTER</h5>
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div
          className={`filter__body ${this.props.isFetching ? `loading` : ``}`}
        >
          <div className="field-section report-mandatory">
            <div className="field__label">
              <label className="field__label-label">Report Type</label>
              <span className="field__label-required" />
              <div className="field__input">
                <Select
                  name="report"
                  value={this.state.report}
                  onChange={this.onReportChange}
                  options={reportList}
                  multi={false}
                  placeholder="Select Report"
                />
              </div>
            </div>
          </div>
          <div className="field-section report-mandatory">
            <div className="field__label">
              <label className="field__label-label">Report Sub-type</label>
              <span className="field__label-required" />
              <div className="field__input">
                <Select
                  name="report_type"
                  value={this.state.reportType}
                  onChange={this.onReportTypeChange}
                  options={reportTypeList}
                  multi={false}
                  placeholder={
                    reportTypeList.length === 0
                      ? 'Select Report'
                      : 'Select Report Type'
                  }
                  disabled={reportTypeList.length === 0 ? true : false}
                />
              </div>
            </div>
          </div>
          <div className="field-section">
            <div className="field__label">
              <label className="field__label-label">Contract Status</label>
              <div className="field__input">
                <Select
                  name="contract_status_id"
                  value={filters.contract_status_id}
                  onChange={this.onFilterChange}
                  options={contractStatuses}
                  multi={true}
                  placeholder="Select Status"
                />
              </div>
            </div>
          </div>
          <div className="field-section">
            <div className="field__label">
              <label className="field__label-label">Site</label>
              <div className="field__input">
                <Select
                  name="site_id"
                  value={filters.site_id}
                  onChange={this.onFilterChange}
                  options={sites}
                  multi={true}
                  placeholder="Select Site"
                />
              </div>
            </div>
          </div>
          <div className="field-section">
            <div className="field__label">
              <label className="field__label-label">Device Type</label>
              <div className="field__input">
                <Select
                  name="device_type"
                  value={filters.device_type}
                  onChange={this.onFilterChange}
                  options={types}
                  multi={true}
                  placeholder="Select Type"
                />
              </div>
            </div>
          </div>
          <div className="field-section-button">
            <SquareButton
              onClick={e => this.onRunClick()}
              content="Run"
              bsStyle={"primary"}
              disabled={
                this.state.reportType === '' || this.state.report === ''
              }
            />
            <SquareButton
              onClick={e => this.onResetClick()}
              content="Reset"
              bsStyle={"default"}
            />
          </div>
        </div>
      </div>
    );
  };

  renderPreBuildReport = () => {
    return (
      <div className="pre-build-report">
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetching} />
        </div>
        <div
          className={`filter__body ${this.props.isFetching ? `loading` : ``}`}
        >
          {this.props.reportTypes &&
            Object.keys(this.props.reportTypes).map((row, index) => (
              <div key={index} className="field-section box">
                <label className="box-label">{row}</label>
                <div className="field__input">
                  {Object.keys(this.props.reportTypes[row]).map((data, i) => (
                    <div
                      key={i}
                      className="field-section report-row"
                      onClick={e =>
                        this.onReportDownloadClick(
                          this.props.reportTypes[row][data]
                        )
                      }
                    >
                      <div className="name">
                        {this.props.reportTypes[row][data].name}
                      </div>
                      <div className="icon" />
                    </div>
                  ))}
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  };

  renderscheduledReport = () => {
    const reportFrequencies = this.props.reportFrequencies
      ? Object.keys(this.props.reportFrequencies).map((reportType, index) => ({
          value: reportType,
          label: reportType,
        }))
      : [];

    return (
      <div className="inventory">
        {this.state.reportError && (
          <div className="no-device-error">{this.state.reportError}</div>
        )}
        <div className="loader">
          <Spinner show={this.props.isFetchingSH} />
        </div>
        <div
          className={`filter__body ${this.props.isFetchingSH ? `loading` : ``}`}
        >
        </div>
        <div className="scheduled-report-list ">
          {this.state.scheduledReportByCat &&
            this.state.scheduledReportByCat.map((row, index) => (
              <div key={index} className="scheduled-report row">
                <div className="name col-md-8">{row.display_name}</div>
                <div className="field-section  col-md-4">
                  <div className="field__label">
                    <label className="field__label-label">Frequency</label>
                    <span className="field__label-required" />
                    <div className="field__input">
                      <Select
                        name="frequency"
                        value={this.state.scheduledReportByCat[index].frequency}
                        onChange={e => this.onRunClickScheduled(e, index)}
                        options={reportFrequencies}
                        multi={false}
                        placeholder="Select Frequency"
                      />
                    </div>{' '}
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  };

  onRunClickScheduled = (e, index) => {
    const targetValue = e.target.value;
    const newState = cloneDeep(this.state);
    newState.scheduledReportByCat[index].frequency = targetValue;

    this.setState(newState);

    if (
      this.state.scheduledReportByCat &&
      this.state.scheduledReportByCat[index].report_id
    ) {
      this.props
        .editScheduledReport(
          this.state.scheduledReportByCat[index].report_id,
          targetValue,
          this.state.scheduledReportByCat[index].id
        )
        .then(action => {
          if (action.type === POST_SH_REPORT_SUCCESS) {
            if (action.response && action.response !== '') {
              this.props.addSuccessMessage('Scheduled report updated.');
            }
          }
          if (action.type === POST_SH_REPORT_FAILURE) {
            this.props.addErrorMessage(
              action.errorList &&
                action.errorList.data &&
                action.errorList.data.detail
            );
          }
        });
    } else {
      this.props
        .postScheduledReport(
          targetValue,
          this.state.scheduledReportByCat[index].id
        )
        .then(action => {
          if (action.type === POST_SH_REPORT_SUCCESS) {
            if (action.response && action.response !== '') {
              this.props.addSuccessMessage('Sheduled report subscribed.');
              const scheduledReportByCat = cloneDeep(
                this.state.scheduledReportByCat
              );
              scheduledReportByCat.map((type, i) => {
                if (type.id === action.response.scheduled_report_type) {
                  scheduledReportByCat[i].frequency = action.response.frequency;
                  scheduledReportByCat[i].report_id = action.response.id;
                }
              });
              this.setState({ scheduledReportByCat });
            }
          }
          if (action.type === POST_SH_REPORT_FAILURE) {
            this.props.addErrorMessage(
              action.errorList &&
                action.errorList.data &&
                action.errorList.data.detail
            );
          }
        });
    }
  };
  
  render() {
    const currentPage = this.state.currentPage;

    return (
      <div>
        {this.props.user &&
          this.props.user.type &&
          this.props.user.type === 'customer' && (
            <div className="report">
              {this.renderTopBar()}
              {currentPage.pageType === PageType.AdvanceReport && (
                <div>{this.renderAdvanceReport()}</div>
              )}
              {currentPage.pageType === PageType.PreBuildReport && (
                <div>{this.renderPreBuildReport()}</div>
              )}
              {currentPage.pageType === PageType.scheduled && (
                <div>{this.renderscheduledReport()}</div>
              )}
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  sites: state.inventory.sites,
  types: state.inventory.types,
  contractStatuses: state.inventory.contractStatuses,
  user: state.profile.user,
  isFetching: state.report.isFetching,
  reportTypes: state.report.reportTypes,
  reportCategories: state.report.reportCategories,
  scheduledReportByCat: state.report.scheduledReportByCat,
  reportFrequencies: state.report.reportFrequencies,
  isFetchingSH: state.report.isFetchingSH,
  scheduledReportList: state.report.scheduledReportList,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTypes: () => dispatch(fetchTypes()),
  fetchSitesCustomersUser: () => dispatch(fetchSitesCustomersUser()),
  fetchContractStatuses: () => dispatch(fetchContractStatuses()),
  reportCustomerUser: (filter: any) => dispatch(reportCustomerUser(filter)),
  getCustomerReportTypes: () => dispatch(getCustomerReportTypes()),
  getReportCategories: () => dispatch(getReportCategories()),
  fetchFrequencySHReport: () => dispatch(fetchFrequencySHReport()),
  GetScheduledReport: () => dispatch(GetScheduledReport()),
  getScheduledReportByCat: (cat: string) =>
    dispatch(getScheduledReportByCat(cat)),
  postScheduledReport: (freq: string, type: string) =>
    dispatch(postScheduledReport(freq, type)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  editScheduledReport: (id: string, freq: string, type: string) =>
    dispatch(editScheduledReport(id, freq, type)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Report);
