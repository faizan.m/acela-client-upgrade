import React, { Component } from 'react';
import { connect } from 'react-redux';

import { DATA_RENEWAL_SUCCESS, uploadRenewalData } from '../../actions/renewal';

import { SHOW_SUCCESS_MESSAGE } from '../../actions/appState';
import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import store from '../../store';

interface IRenewalUploadProps extends ICommonProps {
  close?: (e: any) => void;
  isVisible: boolean;
  uploadRenewalData?: (fileReq: any, custId: number) => Promise<any>;
  fetchData?: any;
}

interface IRenewalUploadState {
  dataFile: any;
  isUploading: boolean;
  showError: string;
}

class RenewalUpload extends Component<
  IRenewalUploadProps,
  IRenewalUploadState
> {
  constructor(props: IRenewalUploadProps) {
    super(props);
    this.state = {
      dataFile: '',
      isUploading: false,
      showError: '',
    };
  }

  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile = files[0];
    this.setState({
      dataFile,
      showError: '',
    });
  };

  onSubmit = () => {
    this.setState({ isUploading: true });
    if (this.state.dataFile) {
      const data = new FormData();
      data.append('renewal_file', this.state.dataFile);
      this.props
        .uploadRenewalData(data, this.props.location.pathname.split('/').pop())
        .then(action => {
          if (action.type === DATA_RENEWAL_SUCCESS) {
            store.dispatch({
              type: SHOW_SUCCESS_MESSAGE,
              message: 'File is Uploaded Successfully.',
            });
            this.props.fetchData();
            this.setState({
              dataFile: null,
              isUploading: false,
            });
            this.props.close(null);
          } else {
            this.setState({
              showError: action.errorList.data.renewal_file,
              isUploading: false,
            });
          }
        });
    }
  };

  renderBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.state.isUploading} />
        </div>
        <div className={`${this.state.isUploading ? `loading` : ''}`}>
          <label className="square-btn  btn btn-primary">
            <img
              className="attachments__icon__upload"
              src="/assets/icons/cloud.png"
            />
            Upload Data File
            <input
              type="file"
              accept=".xls ,.xlsx"
              onChange={this.handleFile}
            />
          </label>
          {this.state.dataFile && <p>{this.state.dataFile.name}</p>}
          {this.state.dataFile &&
            this.state.dataFile.type !==
              // tslint:disable-next-line:max-line-length
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && (
              <p className="file-type__error">
                File Type Not Supported (only *.xlsx files)
              </p>
            )}

          {this.state.showError !== '' && (
            <p className="file-type__error">{this.state.showError}</p>
          )}
        </div>
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div
        className={`
            ${this.state.isUploading ? `loading` : ''}`}
      >
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.props.close}
        />
        <SquareButton
          content="Submit"
          bsStyle={"primary"}
          onClick={this.onSubmit}
          disabled={
            this.state.dataFile &&
            this.state.dataFile.type ===
              // tslint:disable-next-line:max-line-length
              'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
              ? false
              : true
          }
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Upload Renewal Data"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="renewal-upload-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  uploadRenewalData: (fileReq, custId: number) =>
    dispatch(uploadRenewalData(fileReq, custId)),
});

export default connect<any, any>(
  null,
  mapDispatchToProps
)(RenewalUpload);
