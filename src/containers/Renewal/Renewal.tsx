import React from 'react';
import { connect } from 'react-redux';

import CustomerRenewal from './customer/customerRenewal';
import ProviderRenewal from './provider/providerRenewal';

interface IRenewalProps extends ICommonProps {
  user: ISuperUser;
  fetchSingleDevice: any;
}

class Renewal extends React.Component<IRenewalProps, null> {
  constructor(props: IRenewalProps) {
    super(props);
  }
  render() {
    if (this.props.user && this.props.user.type === 'provider') {
      return <ProviderRenewal {...this.props} />;
    } else if (this.props.user && this.props.user.type === 'customer') {
      return <CustomerRenewal {...this.props} />;
    } else {
      return <div />;
    }
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

export default connect<any, any>(
  mapStateToProps,
  null
)(Renewal);
