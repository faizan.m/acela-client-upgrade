import React from 'react';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import './style.scss';

interface IShowNoteProps {
  show: boolean;
  onClose: (e: any) => void;
  note: string;
  title: string;
}

export default class AddNoteForm extends React.Component<IShowNoteProps> {
  constructor(props: IShowNoteProps) {
    super(props);
  }

  onClose = e => {
    this.props.onClose(e);
  };

  getBody = () => {
    return (
      <div className="customer-note-format">
        <p>{this.props.note}</p>
      </div>
    );
  };

  getFooter = () => {
    return (
      <SquareButton
        onClick={this.props.onClose}
        content="Close"
        bsStyle={"primary"}
      />
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={this.props.title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
      />
    );
  }
}
