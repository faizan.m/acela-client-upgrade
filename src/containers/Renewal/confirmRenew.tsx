import React from 'react';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';

import './style.scss';

interface IConfirmRenewFormProps {
  show: boolean;
  onClose: (e: any) => void;
  data?: any;
  onSubmit: any;
  isLoading: boolean;
}

export default class DevicerRenewal extends React.Component<
  IConfirmRenewFormProps,
  { count: any }
> {
  constructor(props: IConfirmRenewFormProps) {
    super(props);
    this.state = {
      count: [],
    };
  }
  onSubmit = () => {
    this.props.onSubmit();
  };

  getDetails = data => {
    const count = [];
    Object.keys(this.props.data).map(key => {
      count.push({
        action: key,
        c: this.props.data[key],
      });
    });

    return count;
  };
  getBody = () => {
    if (this.props.data) {
      const count = this.getDetails(this.props.data);

      return (
        <div>
          <div className="loader modal-loader">
            <Spinner show={this.props.isLoading} />
          </div>
          <div className={` ${this.props.isLoading ? `loading` : ''}`}>
            {count.length > 0 &&
              count.map((data, index) => (
                <p key={index}>
                  You have selected {data.c} devices for {data.action}.
                </p>
              ))}
          </div>
        </div>
      );
    } else {
      return null;
    }
  };

  getFooter = () => {
    return (
      <div
        className={`customer-renew__actions
        ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.props.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Confirm"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    const title = ' Are you sure ?';

    return (
      <ModalBase
        show={this.props.show}
        onClose={this.props.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="customer-renew"
      />
    );
  }
}
