import React from 'react';

import IconButton from '../../../components/Button/iconButton';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import { allowPermission } from '../../../utils/permissions';

const pendingTable = ({
  state,
  fetchDataPending,
  onAction,
  openNote,
  isFetching,
  onRowClick,
}) => {
  const columns: any = [
    {
      accessor: 'device_serial_number',
      Header: 'Serial#',
      Cell: cell => (
        <a title="" onClick={e => onRowClick(cell)}>
          {cell.value ? cell.value : ' N.A.'}
        </a>
      ),
    },
    {
      accessor: 'product_id',
      Header: 'Product ID',
      sortable: false,
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'device_type',
      Header: 'Type',
      sortable: false,
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'target_service_level',
      Header: 'Requested Service Level',
      sortable: true,
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'renewal_type',
      Header: 'Request Action',
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'notes',
      Header: 'Customer Notes',
      sortable: false,
      Cell: cell => <div>{getMarkUp(cell.value)}</div>,
    },
    {
      accessor: 'action',
      Header: 'Action',
      sortable: false,
      show: allowPermission('reject_renewal_action'),
      Cell: cell => (
        <IconButton icon="close-circle.png" onClick={() => onAction(cell)} />
      ),
    },
  ];
  const manualProps = {
    manual: true,
    pages: state.pagination.totalPages,
    onFetchData: fetchDataPending,
  };

  const getMarkUp = note => {
    if (note) {
      if (note.length > 8) {
        return (
          <p>
            {note.slice(0, 8)}..&nbsp;
            <a onClick={() => openNote(note)}>Read More</a>
          </p>
        );
      } else {
        return <p>{note}</p>;
      }
    } else {
      return ``;
    }
  };

  return (
    <div className="inventory">
      <div className="loader">
        <Spinner show={isFetching} />
      </div>
      <Table
        columns={columns}
        rows={state.pendingRowsMap}
        className={`pending-request__table ${isFetching ? `loading` : ``}`}
        manualProps={manualProps}
        loading={isFetching}
      />
    </div>
  );
};

export default pendingTable;
