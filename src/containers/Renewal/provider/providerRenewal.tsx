import React from 'react';
import { debounce } from 'lodash';
import { connect } from 'react-redux';

import {
  exportPendingRenewalRequests,
  getCompletedDevicesForProviders,
  getNewRenewalsDevicesForProviders,
  getPendingDevicesForProvider,
  getRenewalTypes,
  getServiceLevelTypes,
  POST_RENEWAL_SUCCESS,
  REJECT_PROVIDER_RENEWAL_SUCCESS,
  rejectRenewal,
  submitRenewalRequestProvider,
} from '../../../actions/renewal';
import BackLink from '../../../components/BackLink';
import SquareButton from '../../../components/Button/button';
import Tag from '../../../components/Button/roundedButton';
import FilterModal from '../../InventoryManagement/filterModal';
import ConfirmRenewal from '../confirmRenew';
import ShowNote from '../showNote';
import RenewalUpload from '../uploadData';
import AddNoteForm from './../addNotes';
import CompletedTable from './completedTable';
import PendingTable from './pendingTable';
import { addErrorMessage } from '../../../actions/appState';
import {
  FETCH_SINGLE_DEVICE_FAILURE,
  FETCH_SINGLE_DEVICE_SUCCESS,
  fetchContractStatuses,
  fetchExistingDeviceAssociation,
  fetchSingleDevicePU,
  fetchSites,
  fetchTypes,
  resetManufacturerApiRetryCount,
} from '../../../actions/inventory';
import Checkbox from '../../../components/Checkbox/checkbox';
import Input from '../../../components/Input/input';
import Select from '../../../components/Input/Select/select';
import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';
import { allowPermission } from '../../../utils/permissions';
import ViewDevice from '../../InventoryManagement/viewDevice';
import { searchInFields } from '../../../utils/searchListUtils';
import '../style.scss';

enum PageType {
  NEWREQUEST,
  PENDING,
  COMPLETED,
}

interface IProviderRenewalProps extends ICommonProps {
  getNewRenewalsDevicesForProviders?: any;
  submitRenewalRequestProvider?: any;
  getRenewalTypes?: any;
  getServiceLevelTypes?: any;
  newRenewals?: any;
  renewalTypes?: any;
  serviceLevelTypes?: any;
  isNewDevicesFetching?: boolean;
  getPendingDevicesForProvider?: TFetchProviderPendingRenwals;
  getCompletedDevicesForProviders?: TFetchProviderCompletedRenwals;
  pending?: any;
  completed?: any;
  user?: ISuperUser;
  renewalCustomerList?: any;
  renewalCustomerId?: any;
  commpletedTable?: any;
  rejectRenewal?: TRejectRenewal;
  isCompletedFetching?: boolean;
  isPendingFetching?: boolean;
  fetchSingleDevice: any;
  deviceDetails?: IDevice;
  isDeviceFetching?: any;
  addErrorMessage?: TShowErrorMessage;
  resetManufacturerApiRetryCount?: any;
  contractStatuses?: IContractStatus[];
  fetchContractStatuses?: TFetchContractStatuses;
  fetchSites?: TFetchSites;
  fetchTypes?: TFetchTypes;
  sites?: any[];
  types?: any[];
  exportPendingRenewalRequests?: any;
  fetchExistingDeviceAssociation?: any;
  existingAssociationList?: any;
}

interface IRenewalState {
  newRequestRows?: any;
  pendingRows?: any;
  completedRows?: any;
  currentPage: {
    pageType: PageType;
  };
  error: {
    [fieldName: string]: IFieldValidation;
  };
  reportError: string;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
    search?: string;
  };
  isopenConfirm: boolean;
  currentRowId?: string;
  pendingRowsMap?: any;
  completedRowsMap?: any;
  isUploadVisible: boolean;
  selectedRejectionReqId: number;
  selectedRejectionCustomerId: number;
  isRejectNoteVisible: boolean;
  note: string;
  isNoteDetailVisible: boolean;
  noteDetails: string;
  isRejectPosting: boolean;
  renewalCustomerId?: number;
  isViewDeviceModalOpen: boolean;
  selectedRows: number[];
  serviceLevels?: any;
  actions?: any;
  rowServiceLevel?: any;
  rowActions?: any;
  searchString: string;
  showUncovered: boolean;
  filters: IInventoryFilters;
  isFilterModalOpen: boolean;
  siteLabelIds: { [id: number]: string };
  typesLabelIds: { [id: string]: string };
  statusLabelIds: { [id: number]: string };
  managedLabelIds: { [id: string]: string };
  isRenewPosting: boolean;
  selectionCount?: any;
  isNotesPopUpOpen: boolean;
}

class ProviderRenewal extends React.Component<
  IProviderRenewalProps,
  IRenewalState
> {
  private debouncedFetchPending;
  private debouncedFetchCompleted;

  constructor(props: IProviderRenewalProps) {
    super(props);

    this.state = {
      currentPage: {
        pageType: PageType.NEWREQUEST,
      },
      error: {},
      reportError: '',
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: { search: '' },
      },
      rowActions: [],
      isopenConfirm: false,
      isUploadVisible: false,
      selectedRejectionReqId: null,
      selectedRejectionCustomerId: null,
      isRejectNoteVisible: false,
      note: '',
      isNoteDetailVisible: false,
      noteDetails: '',
      isRejectPosting: false,
      isViewDeviceModalOpen: false,
      selectedRows: [],
      rowServiceLevel: [],
      searchString: '',
      showUncovered: false,
      filters: {
        site: [],
        status: [],
        type: [],
      },
      siteLabelIds: {},
      typesLabelIds: {},
      statusLabelIds: {},
      managedLabelIds: {
        MANAGED: 'MANAGED',
        NOTMANAGED: 'NOTMANAGED',
      },
      isFilterModalOpen: false,
      isRenewPosting: false,
      isNotesPopUpOpen: false,
      newRequestRows: [],
    };
    this.debouncedFetchPending = debounce(this.fetchDataPending, 1000);
    this.debouncedFetchCompleted = debounce(this.fetchDataCompleted, 1000);
  }

  componentDidMount() {
    const customerId = this.props.renewalCustomerId
      ? this.props.renewalCustomerId
      : this.props.location.pathname.substr(
          this.props.location.pathname.lastIndexOf('/') + 1
        );
    this.props.getRenewalTypes();
    this.props.getServiceLevelTypes();
    this.props.fetchContractStatuses();
    this.props.fetchTypes();
    this.props.fetchSites(customerId);
    this.props.getNewRenewalsDevicesForProviders(
      this.state.showUncovered,
      customerId
    );
  }

  componentDidUpdate(prevProps: IProviderRenewalProps) {
    const { newRenewals, pending, completed, sites, types, contractStatuses } = this.props;
  
    if (newRenewals !== prevProps.newRenewals) {
      const rows = this.getRows(this.props, '');
      this.setState({
        newRequestRows: rows,
        filters: {
          site: [],
          status: [],
          type: [],
        },
      });
    }
  
    if (pending !== prevProps.pending) {
      this.setRowsPending(this.props);
    }
  
    if (completed !== prevProps.completed) {
      this.setRowsCompleted(this.props);
    }
  
    if (sites !== prevProps.sites) {
      const siteLabelIds = sites.reduce((labelIds, site) => {
        labelIds[site.site_id] = site.name;
  
        return labelIds;
      }, {});
  
      this.setState({
        siteLabelIds,
      });
    }
  
    if (types !== prevProps.types) {
      const typesLabelIds = types.reduce((labelIds, type) => {
        labelIds[type] = type;
  
        return labelIds;
      }, {});
  
      this.setState({
        typesLabelIds,
      });
    }
  
    if (contractStatuses !== prevProps.contractStatuses) {
      const statusLabelIds = contractStatuses.reduce((statusIds, contractStatus) => {
        statusIds[contractStatus.contract_status_id] = contractStatus.contract_status;
  
        return statusIds;
      }, {});
  
      this.setState({
        statusLabelIds,
      });
    }
  }  

  getRows = (
    nextProps: IProviderRenewalProps,
    searchString?: string,
    filters?: IInventoryFilters,
    showInactiveDevices?: boolean
  ) => {
    let devices = nextProps.newRenewals;
    const search = searchString ? searchString : this.state.searchString;

    if (filters) {
      // Add filter for type once included.
      const filterSites = filters.site;
      const filterStatus = filters.status;
      const filterTypes = filters.type;
      const filterManaged = filters.is_managed;

      devices =
        devices &&
        devices.filter(device => {
          // If there are no filters for the
          // specific key then includes will
          // give false, hence below code is used
          // used to handle that.
          const hasFilteredSite =
            filterSites.length > 0
              ? filterSites.includes(device.site_id)
              : true;
          const hasFilteredStatus =
            filterStatus.length > 0
              ? filterStatus.includes(device.contract_status_id)
              : true;
          const hasFilteredType =
            filterTypes.length > 0
              ? filterTypes.includes(device.device_type)
              : true;
          const hasFilteredManaged =
            filterManaged && filterManaged.length > 0
              ? filterManaged.includes(
                  device &&
                  device.monitoring_data &&
                  device.monitoring_data.is_managed
                    ? 'MANAGED'
                    : 'NOTMANAGED'
                )
              : true;

          return (
            hasFilteredSite &&
            hasFilteredStatus &&
            hasFilteredType &&
            hasFilteredManaged
          );
        });
    }
    if (search && search.length > 0) {
      devices =
        devices &&
        devices.filter(
          row =>
              searchInFields(row, search,[
              'device_name',
              'site',
              'status',
              'serial_number',
              'category_name',
              'manufacturer_name',
              'contract_status',
              'service_contract_number',
              'instance_id'
            ])
        );
    }

    const rows: any = devices.map((device, index) => ({
      serial_number: device.serial_number,
      product_Id: device.product_id,
      category_name: device.category_name,
      model_number: device.model_number,
      device_type: device.device_type,
      site: device.name,
      site_id: device.site_id,
      model: device.model_number,
      expiration_date: device.expiration_date,
      service_contract_number: device.service_contract_number,
      EOL_date: device.EOL_date,
      EOS_date: device.EOS_date,
      LDOS_date: device.LDOS_date,
      contract_status: device.contract_status,
      status: device.status,
      product_bulletin_url: device.product_bulletin_url,
      id: device.id,
      instance_id: device.instance_id,
      endOfSupport: device.LDOS_date,
      serviceLevel: device.serviceLevel,
      action: device.action,
      errorAction: device.errorAction,
      errorService: device.errorService,
      note: device.note,
      device_id: device.id,
      index,
    }));

    return rows;
  };

  deleteFilter = (filterName: string, attributeIndex: number) => {
    const prevFilters = this.state.filters;
    const filters = {
      ...prevFilters,
      [filterName]: [
        ...prevFilters[filterName].slice(0, attributeIndex),
        ...prevFilters[filterName].slice(attributeIndex + 1),
      ],
    };
    const newRequestRows = this.getRows(
      this.props,
      this.state.searchString,
      filters
    );

    this.setState(prevState => ({
      filters,
      newRequestRows,
    }));
  };

  onRowsToggle = selectedRows => {
    this.setState({
      selectedRows,
    });
  };

  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;

    this.setState(_prevState => ({
      [targetName]: targetValue,
    }));
  };

  handleChangeSelectService = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const newRequestRows = this.state.newRequestRows;
    newRequestRows.map((data, index) => {
      if (data.id === parseInt(targetName, 10)) {
        newRequestRows[index].serviceLevel = targetValue;
      }
    });

    this.setState(_prevState => ({
      newRequestRows,
    }));
  };

  handleChangeSelectAction = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const newRequestRows = this.state.newRequestRows;
    newRequestRows.map((data, index) => {
      if (data.id === parseInt(targetName, 10)) {
        newRequestRows[index].action = targetValue;
      }
    });

    this.setState(_prevState => ({
      newRequestRows,
    }));
  };

  onClickApply = () => {
    const newRequestRows = this.state.newRequestRows;
    this.state.selectedRows.map((row, _i) => {
      newRequestRows.map((data, index) => {
        if (data.id === row) {
          newRequestRows[index].action = this.state.actions;
          newRequestRows[index].serviceLevel = this.state.serviceLevels;
        }
      });
    });

    this.setState(_prevState => ({
      newRequestRows,
    }));
  };

  onClickReset = () => {
    const newRequestRows = this.state.newRequestRows;
    this.state.selectedRows.map((row, _i) => {
      newRequestRows.map((data, index) => {
        if (data.id === row) {
          newRequestRows[index].action = '';
          newRequestRows[index].serviceLevel = '';
        }
      });
    });

    this.setState(_prevState => ({
      newRequestRows,
      actions: '',
      serviceLevels: '',
    }));
  };

  onClickCancel = () => {
    this.props.history.push(`/`);
  };

  isValidRows = () => {
    const newRequestRows = this.state.newRequestRows;
    newRequestRows.map((_data, index) => {
      newRequestRows[index].errorService = false;
      if (
        newRequestRows[index].action &&
        newRequestRows[index].action !== 'Hold' &&
        newRequestRows[index].action !== 'Do Not Renew' &&
        newRequestRows[index].action !== 'Terminate'
      ) {
        newRequestRows[index].errorService = newRequestRows[index].serviceLevel
          ? false
          : true;
      }
    });
    this.setState(_prevState => ({
      newRequestRows,
    }));

    const isValidServiceLevel = newRequestRows.filter(
      row => row.errorService === true
    );
    const isValidActions = newRequestRows.filter(row => row.action);

    if (isValidActions && isValidActions.length === 0) {
      this.props.addErrorMessage('please Select Service level and action.');
    }
    if (isValidServiceLevel && isValidServiceLevel.length > 0) {
      this.props.addErrorMessage('please Select Service level.');
    }

    return isValidServiceLevel.length === 0 && isValidActions.length > 0
      ? true
      : false;
  };

  onClickConfirm = () => {
    this.setState({ isRenewPosting: true });
    let isValid = false;
    isValid = this.isValidRows();
    if (isValid === true) {
      const newRequestRows = this.state.newRequestRows;
      const postData = [];
      newRequestRows.map((data, _index) => {
        if (data.action) {
          postData.push({
            device_serial_number: data.serial_number || '',
            device_crm_id: data.id || '',
            device_info: {
              device_type: data.device_type || '',
              LDOS_date: data.LDOS_date || '',
              site: data.site || '',
              site_id: data.site_id || '',
              service_contract_number: data.service_contract_number || '',
              expiration_date: data.expiration_date || '',
              product_id: data.product_Id || '',
            },
            target_service_level: data.serviceLevel,
            renewal_type: data.action,
            notes: data.note || '',
          });
        }
      });
      const customerId = this.props.renewalCustomerId
        ? this.props.renewalCustomerId
        : this.props.location.pathname.substr(
            this.props.location.pathname.lastIndexOf('/') + 1
          );
      this.props
        .submitRenewalRequestProvider(postData, customerId)
        .then(action => {
          if (action.type === POST_RENEWAL_SUCCESS) {
            this.toggleConfirmOpen();
            this.props.getNewRenewalsDevicesForProviders(
              this.state.showUncovered,
              this.props.renewalCustomerId
                ? this.props.renewalCustomerId
                : this.props.location.pathname.substr(
                    this.props.location.pathname.lastIndexOf('/') + 1
                  )
            );
          }
          this.setState({ isRenewPosting: false });
        })
        .catch(() => {
          this.setState({ isRenewPosting: false });
        });
    }
  };

  onClickSubmitRenewRequest = _e => {
    let isValid = false;
    isValid = this.isValidRows();
    if (isValid === true) {
      this.toggleConfirmOpen();
      if (this.state.newRequestRows.length > 0) {
        const arr = this.state.newRequestRows;
        const counts = {};
        // tslint:disable-next-line:prefer-for-of
        for (let i = 0; i < arr.length; i++) {
          if (arr[i].action) {
            counts[arr[i].action] = 1 + (counts[arr[i].action] || 0);
          }
        }
        this.setState({ selectionCount: counts });
      }
    }
  };

  toggleConfirmOpen = () => {
    this.setState(prevState => ({
      isopenConfirm: !prevState.isopenConfirm,
    }));
  };

  setRowsPending = (nextProps: IProviderRenewalProps) => {
    const pending: any = nextProps.pending;
    const pendingRows: any = pending.results;

    const pendingRowsMap: any = pendingRows.map((device, index) => ({
      device_serial_number: device.device_serial_number,
      product_id: device.device_info.product_id,
      device_type: device.device_info.device_type,
      target_service_level: device.target_service_level,
      renewal_type: device.renewal_type,
      notes: device.notes,
      instance_id: device.instance_id,
      id: device.id,
      device_id: device.device_crm_id,
      customer_id: device.customer,
      index,
    }));

    this.setState(prevState => ({
      pendingRowsMap,
      pendingRows,
      pagination: {
        ...prevState.pagination,
        totalRows: pending.count,
        totalPages: Math.ceil(
          pending.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  setRowsCompleted = (nextProps: IProviderRenewalProps) => {
    const completed: any = nextProps.completed;
    const completedRows: any = completed.results;

    const completedRowsMap: any = completedRows.map((device, index) => ({
      device_serial_number: device.device_serial_number,
      product_id: device.device_info.product_id,
      device_type: device.device_info.device_type,
      target_service_level: device.renewal_info
        ? device.renewal_info.target_service_level
        : 'N.A.',
      renewal_type: device.renewal_info
        ? device.renewal_info.renewal_type
        : 'N.A.',
      instance_id: device.instance_id,
      notes: device.notes,
      status: device.status,
      ex_date: device.renewal_info.expiration_date
        ? device.renewal_info.expiration_date
        : null,
      id: device.id,
      device_id: device.device_crm_id,
      index,
    }));

    this.setState(prevState => ({
      completedRowsMap,
      completedRows,
      pagination: {
        ...prevState.pagination,
        totalRows: completed.count,
        totalPages: Math.ceil(
          completed.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };
  changePage = (pageType: PageType) => {
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: {
          ...prevState.pagination.params,
          search: '',
        },
      },
      currentPage: {
        pageType,
      },
    }));
  };

  // Server side searching, sorting, ordering
  fetchDataPending = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getPendingDevicesForProvider(
      this.props.renewalCustomerId
        ? this.props.renewalCustomerId
        : this.props.location.pathname.substr(
            this.props.location.pathname.lastIndexOf('/') + 1
          ),
      newParams
    );
  };

  fetchDataCompleted = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.getCompletedDevicesForProviders(
      this.props.renewalCustomerId
        ? this.props.renewalCustomerId
        : this.props.location.pathname.substr(
            this.props.location.pathname.lastIndexOf('/') + 1
          ),
      newParams
    );
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="renewal__header">
        {
          <div
            className={`renewal__header-link ${
              currentPage.pageType === PageType.NEWREQUEST
                ? 'renewal__header-link--active'
                : ''
            }`}
            onClick={_e => this.changePage(PageType.NEWREQUEST)}
          >
            New Request
          </div>
        }
        {
          <div
            className={`renewal__header-link ${
              currentPage.pageType === PageType.PENDING
                ? 'renewal__header-link--active'
                : ''
            }`}
            onClick={_e => this.changePage(PageType.PENDING)}
          >
            Pending
          </div>
        }
        {
          <div
            className={`renewal__header-link ${
              currentPage.pageType === PageType.COMPLETED
                ? 'renewal__header-link--active'
                : ''
            }`}
            onClick={_e => this.changePage(PageType.COMPLETED)}
          >
            Completed
          </div>
        }
      </div>
    );
  };

  exportPendingRenewalRequests = () => {
    const customerId = this.props.renewalCustomerId
      ? this.props.renewalCustomerId
      : this.props.location.pathname.substr(
          this.props.location.pathname.lastIndexOf('/') + 1
        );
    this.props.exportPendingRenewalRequests(customerId);
  };

  openUpload = () => {
    this.setState({ isUploadVisible: true });
  };

  closeUpload = () => {
    this.setState({ isUploadVisible: false });
  };

  openRejectNote = cell => {
    event.stopPropagation();
    this.setState({
      isRejectNoteVisible: true,
      selectedRejectionReqId: cell.original.id,
      selectedRejectionCustomerId: cell.original.customer_id,
      note: '',
    });
  };

  submitRejection = () => {
    this.setState({ isRejectPosting: true });
    this.props
      .rejectRenewal(
        this.state.selectedRejectionReqId,
        this.state.note,
        this.state.selectedRejectionCustomerId
      )
      .then(action => {
        if (action.type === REJECT_PROVIDER_RENEWAL_SUCCESS) {
          this.fetchDataPending(this.state.pagination.params);
          this.setState({
            isRejectNoteVisible: false,
            selectedRejectionReqId: null,
            note: '',
          });
        }
        this.setState({
          isRejectPosting: false,
        });
      })
      .catch(() => {
        this.setState({
          isRejectPosting: false,
        });
      });
  };

  closeNote = () => {
    this.setState({
      isRejectNoteVisible: false,
      selectedRejectionReqId: null,
      note: '',
    });
  };

  openNoteDetails = (note: string) => {
    this.setState({
      isNoteDetailVisible: true,
      noteDetails: note,
    });
  };

  closeNoteDetails = () => {
    this.setState({
      isNoteDetailVisible: false,
      noteDetails: '',
    });
  };

  toggleViewDeviceModal = () => {
    this.setState(prevState => ({
      isViewDeviceModalOpen: !prevState.isViewDeviceModalOpen,
    }));
  };

  openURL = (event: any, url) => {
    event.stopPropagation();
    if (url) {
      window.open(url);
    }
  };

  onRowClick = rowInfo => {
    event.stopPropagation();
    const customerId = this.props.renewalCustomerId
      ? this.props.renewalCustomerId
      : this.props.location.pathname.substr(
          this.props.location.pathname.lastIndexOf('/') + 1
        );
    if (allowPermission('view_device')) {
      this.props
        .fetchSingleDevice(customerId, rowInfo.original.device_id)
        .then(action => {
          if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
            if (customerId) {
              this.props.fetchExistingDeviceAssociation(
                customerId,
                rowInfo.original.device_id
              );
            }
            this.setState(prevState => ({
              isViewDeviceModalOpen: true,
            }));
          } else if (action.type === FETCH_SINGLE_DEVICE_FAILURE) {
            this.props.addErrorMessage(
              action.errorList && action.errorList.data.message
            );
          }
        });
    }
  };

  renderTopBarNewRequest = () => {
    const serviceLevelList = this.props.serviceLevelTypes
      ? this.props.serviceLevelTypes.map((type, _index) => ({
          value: type.label,
          label: type.label,
        }))
      : [];

    const actionList = this.props.renewalTypes
      ? this.props.renewalTypes.map((type, _index) => ({
          value: type,
          label: type,
        }))
      : [];

    return (
      <div className="">
        <div className="new-request__tabel-header">
          <Input
            field={{
              label: '',
              type: "TEXT",
              value: this.state.searchString,
              isRequired: false,
            }}
            width={3}
            placeholder="Search"
            name="searchString"
            onChange={this.handleChangeSearch}
            className="inventory-management__search"
          />
          <div className="field-section show-uncoverd">
            <Checkbox
              isChecked={this.state.showUncovered}
              name="option"
              onChange={e => this.onChecboxChanged(e)}
            >
              Show Un-coverd only
            </Checkbox>
          </div>
          <div className="field-section filter-button">
            <SquareButton
              onClick={this.toggleFilterModal}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              disabled={
                this.props.newRenewals && this.props.newRenewals.length === 0
              }
              bsStyle={"primary"}
              className="filter-renewal"
            />
          </div>
          {allowPermission('renewal_actions') && (
            <div className="field-section">
              <Select
                name="serviceLevels"
                value={this.state.serviceLevels}
                onChange={this.handleChange}
                options={serviceLevelList}
                multi={false}
                placeholder="Select Service Level"
              />
            </div>
          )}
          {allowPermission('renewal_actions') && (
            <div className="field-section">
              <Select
                name="actions"
                value={this.state.actions}
                onChange={this.handleChange}
                options={
                  this.state.showUncovered
                    ? actionList.filter(
                        f => !['Hold', 'Do Not Renew'].includes(f.value)
                      )
                    : actionList
                }
                multi={false}
                placeholder="Select Action"
              />
            </div>
          )}
          {allowPermission('renewal_actions') && (
            <div className="field-section">
              <SquareButton
                onClick={_e => this.onClickApply()}
                content="Apply"
                bsStyle={"primary"}
                disabled={
                  this.state.selectedRows.length > 0 &&
                  this.state.actions &&
                  (this.state.actions === 'Hold' ||
                    this.state.actions === 'Do Not Renew' ||
                    this.state.actions === 'Terminate' ||
                    this.state.serviceLevels)
                    ? false
                    : true
                }
                className="apply-renewal"
              />

              <SquareButton
                onClick={_e => this.onClickReset()}
                content="Reset"
                bsStyle={"primary"}
                disabled={
                  this.state.selectedRows.length > 0 ||
                  this.state.actions ||
                  this.state.serviceLevels
                    ? false
                    : true
                }
                className="reset-renewal"
              />
            </div>
          )}
        </div>
        {this.renderFilters()}
      </div>
    );
  };
  renderFilters = () => {
    const {
      filters,
      siteLabelIds,
      typesLabelIds,
      managedLabelIds,
      statusLabelIds,
    } = this.state;
    const shouldRenderFilters = filters
      ? (filters.site && filters.site.length > 0) ||
        (filters.type && filters.type.length > 0) ||
        (filters.is_managed && filters.is_managed.length > 0) ||
        (filters.status && filters.status.length > 0)
      : false;

    return shouldRenderFilters ? (
      <div className="inventory-management__table-filters">
        <label>Applied Filters: </label>
        {Object.keys(filters).map((filterName, filterIndex) => {
          const filterValues = filters[filterName];
          let labelIds = {};
          if (filterName === 'site') {
            labelIds = siteLabelIds;
          } else if (filterName === 'status') {
            labelIds = statusLabelIds;
          } else if (filterName === 'type') {
            labelIds = typesLabelIds;
          } else if (filterName === 'is_managed') {
            labelIds = managedLabelIds;
          }

          if (filterValues.length > 0) {
            return filterValues.map((id, valueIndex) => (
              <div
                key={`${filterIndex}.${valueIndex}`}
                className="inventory-management__filter"
              >
                <label>{labelIds[id] ? labelIds[id] : 'N.A.'}</label>
                <span
                  onClick={() => this.deleteFilter(filterName, valueIndex)}
                />
              </div>
            ));
          }
        })}
      </div>
    ) : null;
  };
  handleChangeSearch = (event: any) => {
    const searchString = event.target.value.toLowerCase();
    let newRequestRows = this.props.newRenewals;

    newRequestRows = newRequestRows.filter(
      row =>
      searchInFields(row, searchString,[
        'serial_number',
        'product_Id',
        'device_type',
        'site',
        'LDOS_date',
        'expiration_date',
        'service_contract_number',
        'model_number',
        'errorAction',
        'errorService',
        'note',
        'instance_id'])
    );

    this.setState({ newRequestRows, searchString });
  };

  onChecboxChanged = (event: any) => {
    const showUncovered = event.target.checked ? true : false;
    const rows = this.getRows(
      this.props,
      this.state.searchString,
      this.state.filters,
      showUncovered
    );
    this.setState({
      showUncovered,
      newRequestRows: rows,
    });
    this.props.getNewRenewalsDevicesForProviders(
      showUncovered,
      this.props.renewalCustomerId
        ? this.props.renewalCustomerId
        : this.props.location.pathname.substr(
            this.props.location.pathname.lastIndexOf('/') + 1
          )
    );
  };

  toggleFilterModal = () => {
    this.setState(prevState => ({
      isFilterModalOpen: !prevState.isFilterModalOpen,
    }));
  };

  onFiltersUpdate = (filters: IInventoryFilters) => {
    const newRequestRows = this.getRows(
      this.props,
      this.state.searchString,
      filters
    );

    this.setState({
      newRequestRows,
      filters,
      isFilterModalOpen: false,
    });
  };

  onRowClickNew = rowInfo => {
    event.stopPropagation();
    const customerId = this.props.renewalCustomerId
      ? this.props.renewalCustomerId
      : this.props.location.pathname.substr(
          this.props.location.pathname.lastIndexOf('/') + 1
        );
    if (allowPermission('view_device')) {
      this.props
        .fetchSingleDevice(customerId, rowInfo.original.id)
        .then(action => {
          if (action.type === FETCH_SINGLE_DEVICE_SUCCESS) {
            if (customerId) {
              this.props.fetchExistingDeviceAssociation(
                customerId,
                rowInfo.original.id
              );
            }
            this.setState(prevState => ({
              isViewDeviceModalOpen: true,
            }));
          } else if (action.type === FETCH_SINGLE_DEVICE_FAILURE) {
            this.props.addErrorMessage(
              action.errorList && action.errorList.data.message
            );
          }
        });
    }
  };

  renderNewRequest = () => {
    const serviceLevelList = this.props.serviceLevelTypes
      ? this.props.serviceLevelTypes.map((type, _index) => ({
          value: type.label,
          label: type.label,
        }))
      : [];

    const actionList = this.props.renewalTypes
      ? this.props.renewalTypes.map((type, _index) => ({
          value: type,
          label: type,
        }))
      : [];
    const columns: any = [
      {
        accessor: 'serial_number',
        Header: 'Serial#',
        sortable: true,
        width: 120,
        Cell: s => (
          <a title="" onClick={e => this.onRowClickNew(s)}>
            {s.original.serial_number ? s.original.serial_number : ' N.A.'}
          </a>
        ),
      },
      {
        accessor: 'product_Id',
        Header: 'Product ID',
        sortable: true,
        Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
      },
      {
        accessor: 'device_type',
        Header: 'Type',
        sortable: true,
        Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
      },
      {
        accessor: 'model_number',
        Header: 'Model No.',
        sortable: true,
        Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
      },
      {
        accessor: 'LDOS_date',
        Header: 'LDOS',
        sortable: true,
        Cell: cell => (
          <div>
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : ' N.A.'
            }`}
          </div>
        ),
      },
      {
        accessor: 'site',
        Header: 'Site',
        sortable: true,
        Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
      },
      {
        accessor: 'service_contract_number',
        Header: 'Contract',
        sortable: true,
        Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
      },
      {
        accessor: 'expiration_date',
        Header: 'Expiry Date',
        sortable: true,
        width: 110,
        Cell: cell => (
          <div>
            {' '}
            {`${
              cell.value ? fromISOStringToFormattedDate(cell.value) : ' N.A.'
            }`}
          </div>
        ),
      },
      {
        accessor: 'contract_status',
        Header: 'Contract Status',
        width: 100,
        Cell: status => (
          <div className={`status status--${status.value}`}>
            {status.value ? status.value : ' N.A.'}
          </div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Service Level',
        width: 150,
        sortable: false,
        show: allowPermission('renewal_actions'),
        Cell: cell => (
          <div
            className={
              cell.row._original.errorService === true ? 'required-error' : ''
            }
          >
            <Select
              name={cell.row._original.id.toString()}
              value={cell.row._original.serviceLevel}
              onChange={e => this.handleChangeSelectService(e)}
              options={serviceLevelList}
              multi={false}
              placeholder="Select Service Level"
            />
          </div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Action',
        width: 140,
        sortable: false,
        show: allowPermission('renewal_actions'),
        Cell: cell => (
          <div
            className={
              cell.row._original.errorAction === true ? 'required-error' : ''
            }
          >
            <Select
              name={cell.row._original.id.toString()}
              value={cell.row._original.action}
              onChange={e => this.handleChangeSelectAction(e)}
              options={
                this.state.showUncovered
                  ? actionList.filter(
                      f => !['Hold', 'Do Not Renew'].includes(f.value)
                    )
                  : actionList
              }
              multi={false}
              placeholder="Select Action"
            />
          </div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Notes',
        width: 120,
        show: allowPermission('renewal_actions'),
        sortable: false,
        Cell: cell => (
          <Tag
            content={
              <span>
                <img alt="" src="/assets/icons/writing.png" />
              </span>
            }
            active={false}
            onClick={e =>
              this.onClickaddNote(
                e,
                cell.row._original.note,
                cell.row._original.id
              )
            }
          />
        ),
      },
    ];

    const rowSelectionProps = {
      showCheckbox: allowPermission('renewal_actions'),
      selectIndex: 'id',
      onRowsToggle: this.onRowsToggle,
    };

    return (
      <div className="new-request">
        <div className="loader">
          <Spinner show={this.props.isNewDevicesFetching} />
        </div>
        <Table
          columns={columns}
          rows={this.state.newRequestRows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBarNewRequest()}
          className={`new-request__table ${
            this.props.isNewDevicesFetching ? `loading` : ``
          }`}
          pageSize={this.state.newRequestRows.length}
          loading={this.props.isNewDevicesFetching}
        />
        {this.state.newRequestRows &&
          this.state.newRequestRows.length > 0 &&
          allowPermission('renewal_actions') && (
            <div className="new-request__footer">
              <SquareButton
                onClick={_e => this.onClickCancel()}
                content="Cancel"
                bsStyle={"default"}
                className="cancel"
              />
              <SquareButton
                onClick={e => this.onClickSubmitRenewRequest(e)}
                content="Submit Renew Request"
                bsStyle={"primary"}
                className="submit"
              />
            </div>
          )}
      </div>
    );
  };
  toggleNotesPopUpOpen = () => {
    this.setState(prevState => ({
      isNotesPopUpOpen: !prevState.isNotesPopUpOpen,
    }));
  };

  onClickaddNote = (_e, data, id) => {
    event.stopPropagation();
    this.setState(prevState => ({
      note: data,
      currentRowId: id,
      isNotesPopUpOpen: !prevState.isNotesPopUpOpen,
    }));
  };

  onSubmitNotes = () => {
    const newRequestRows = this.state.newRequestRows;
    newRequestRows.map((data, index) => {
      if (data.id === parseInt(this.state.currentRowId, 10)) {
        newRequestRows[index].note = this.state.note;
      }
    });

    this.setState(_prevState => ({
      newRequestRows,
    }));
    this.toggleNotesPopUpOpen();
  };

  onSearchStringChangePending = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    this.debouncedFetchPending({
      page: 1,
    });
  };

  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    this.debouncedFetchCompleted({
      page: 1,
    });
  };
  render() {
    const currentPage = this.state.currentPage;
    const renewalCustomerList = this.props.renewalCustomerList;
    const renewalUser =
      renewalCustomerList &&
      renewalCustomerList.filter(
        request => request.customer_id === this.props.renewalCustomerId
      );

    return (
      <div>
        <BackLink path="/customer-renewals" name="Back to List" />
        <div className="Renewal">
          <div className="renewal-header__provider">
            <h4>
              {renewalUser &&
                renewalUser.length > 0 &&
                renewalUser[0].customer_name}{' '}
              Renewal Requests
            </h4>
            {allowPermission('upload_renewal_data') &&
              currentPage.pageType === PageType.PENDING && (
                <div className="pending-actions">
                  <SquareButton
                    onClick={this.exportPendingRenewalRequests}
                    content={
                      <span>
                        <img alt="" src="/assets/icons/export.png" />
                        Export Pending Renewal Data
                      </span>
                    }
                    disabled={
                      this.state.pendingRows &&
                      this.state.pendingRows.length === 0
                    }
                    bsStyle={"primary"}
                    className="upload-btn"
                  />
                  <SquareButton
                    onClick={this.openUpload}
                    content={
                      <span>
                        <img alt="" src="/assets/icons/import.png" />
                        Upload Renewal Data
                      </span>
                    }
                    disabled={
                      this.state.pendingRows &&
                      this.state.pendingRows.length === 0
                    }
                    bsStyle={"primary"}
                    className="upload-btn"
                  />
                </div>
              )}
          </div>
          {this.renderTopBar()}
          {currentPage.pageType === PageType.NEWREQUEST && (
            <div>{this.renderNewRequest()}</div>
          )}
          {currentPage.pageType === PageType.PENDING && (
            <div className="pending">
              <div>
                <Input
                  field={{
                    value: this.state.pagination.params.search,
                    label: '',
                    type: "SEARCH",
                  }}
                  width={5}
                  name="searchString"
                  onChange={this.onSearchStringChangePending}
                  placeholder="Search"
                  className="superusers-listing__actions-search"
                />
              </div>
              <PendingTable
                state={this.state}
                fetchDataPending={this.fetchDataPending}
                onAction={this.openRejectNote}
                openNote={this.openNoteDetails}
                isFetching={this.props.isPendingFetching}
                onRowClick={this.onRowClick}
              />
            </div>
          )}
          {currentPage.pageType === PageType.COMPLETED && (
            <div className="completed">
              <div>
                <Input
                  field={{
                    value: this.state.pagination.params.search,
                    label: '',
                    type: "SEARCH",
                  }}
                  width={5}
                  name="searchString"
                  onChange={this.onSearchStringChange}
                  placeholder="Search"
                  className="superusers-listing__actions-search"
                />
              </div>
              <CompletedTable
                state={this.state}
                fetchDataCompleted={this.fetchDataCompleted}
                openNote={this.openNoteDetails}
                isFetching={this.props.isCompletedFetching}
                onRowClick={this.onRowClick}
              />
            </div>
          )}
        </div>
        <RenewalUpload
          {...this.props}
          isVisible={this.state.isUploadVisible}
          close={this.closeUpload}
          fetchData={this.fetchDataPending}
        />
        <AddNoteForm
          title="Add Note for Rejection"
          show={this.state.isRejectNoteVisible}
          onClose={this.closeNote}
          onSubmit={this.submitRejection}
          note={this.state.note}
          handleChange={e => this.handleChange(e)}
          isLoading={this.state.isRejectPosting}
        />
        <AddNoteForm
          title="Add Notes"
          show={this.state.isNotesPopUpOpen}
          onClose={this.toggleNotesPopUpOpen}
          onSubmit={this.onSubmitNotes}
          note={this.state.note}
          handleChange={e => this.handleChange(e)}
          isLoading={this.state.isRejectPosting}
        />
        <ShowNote
          title="Note"
          onClose={this.closeNoteDetails}
          show={this.state.isNoteDetailVisible}
          note={this.state.noteDetails}
        />
        <ViewDevice
          {...this.props}
          show={this.state.isViewDeviceModalOpen}
          onClose={this.toggleViewDeviceModal}
          manufacturers={[]}
          sites={[]}
          fetchSites={null}
          deviceDetails={this.props.deviceDetails}
          openURL={this.openURL}
          isFetching={this.props.isDeviceFetching}
          resetRetry={this.props.resetManufacturerApiRetryCount}
          userType={this.props.user && this.props.user.type}
          existingAssociationList={this.props.existingAssociationList}
        />
        <FilterModal
          show={this.state.isFilterModalOpen}
          onClose={this.toggleFilterModal}
          onSubmit={this.onFiltersUpdate}
          sites={this.props.sites}
          types={this.props.types}
          contractStatuses={this.props.contractStatuses}
          prevFilters={this.state.filters}
        />
        <ConfirmRenewal
          data={this.state.selectionCount}
          show={this.state.isopenConfirm}
          onClose={this.toggleConfirmOpen}
          onSubmit={this.onClickConfirm}
          isLoading={this.state.isRenewPosting}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  newRenewals: state.renewal.newRenewals,
  pending: state.renewal.pending,
  completed: state.renewal.completed,
  user: state.profile.user,
  renewalCustomerList: state.renewal.renewalCustomerList,
  renewalCustomerId: state.renewal.renewalCustomerId,
  isCompletedFetching: state.renewal.isCompletedFetching,
  isPendingFetching: state.renewal.isPendingFetching,
  isNewDevicesFetching: state.renewal.isNewDevicesFetching,
  deviceDetails: state.inventory.device,
  isDeviceFetching: state.inventory.isDeviceFetching,
  sites: state.inventory.sites,
  types: state.inventory.types,
  contractStatuses: state.inventory.contractStatuses,
  serviceLevelTypes: state.renewal.serviceLevelTypes,
  renewalTypes: state.renewal.renewalTypes,
  existingAssociationList: state.inventory.existingAssociationList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getPendingDevicesForProvider: (
    id: number,
    params?: IServerPaginationParams
  ) => dispatch(getPendingDevicesForProvider(id, params)),
  getCompletedDevicesForProviders: (
    id: number,
    params?: IServerPaginationParams
  ) => dispatch(getCompletedDevicesForProviders(id, params)),
  rejectRenewal: (id: number, notes: string, customerId: number) =>
    dispatch(rejectRenewal(id, notes, customerId)),
  fetchSingleDevice: (customerId: number, id: number) =>
    dispatch(fetchSingleDevicePU(customerId, id)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  resetManufacturerApiRetryCount: (num: string) =>
    dispatch(resetManufacturerApiRetryCount(num)),
  fetchContractStatuses: () => dispatch(fetchContractStatuses()),
  fetchTypes: () => dispatch(fetchTypes()),
  fetchSites: (customerId: number) => dispatch(fetchSites(customerId)),
  getRenewalTypes: () => dispatch(getRenewalTypes()),
  getServiceLevelTypes: () => dispatch(getServiceLevelTypes()),
  submitRenewalRequestProvider: (data: any, customerId: number) =>
    dispatch(submitRenewalRequestProvider(data, customerId)),

  getNewRenewalsDevicesForProviders: (
    showUncovered: boolean,
    customerId: number
  ) => dispatch(getNewRenewalsDevicesForProviders(showUncovered, customerId)),
  exportPendingRenewalRequests: (customerId: number) =>
    dispatch(exportPendingRenewalRequests(customerId)),
  fetchExistingDeviceAssociation: (contractNo: any, deviceId: number) =>
    dispatch(fetchExistingDeviceAssociation(contractNo, deviceId)),
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(ProviderRenewal);
