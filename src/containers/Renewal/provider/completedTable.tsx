import React from 'react';

import Spinner from '../../../components/Spinner';
import Table from '../../../components/Table/table';
import { fromISOStringToFormattedDate } from '../../../utils/CalendarUtil';

const completedTable = ({
  state,
  fetchDataCompleted,
  openNote,
  isFetching,
  onRowClick,
}) => {
  const columns: any = [
    {
      accessor: 'device_serial_number',
      Header: 'Serial#',
      Cell: s => (
        <a title="" onClick={e => onRowClick(s)}>
          {s.original.device_serial_number
            ? s.original.device_serial_number
            : ' N.A.'}
        </a>
      ),
    },
    {
      accessor: 'product_id',
      Header: 'Product ID',
      sortable: false,
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'device_type',
      Header: 'Type',
      sortable: false,
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'target_service_level',
      sortable: false,
      Header: 'Requested Service Level',
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'renewal_type',
      Header: 'Request Action',
      sortable: false,
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'ex_date',
      Header: 'New Expiration Date',
      width: 100,
      sortable: false,
      Cell: cell => (
        <div>
          {`${cell.value ? fromISOStringToFormattedDate(cell.value) : ' N.A.'}`}
        </div>
      ),
    },
    {
      accessor: 'status',
      Header: 'Renewal Status',
      Cell: cell => <div> {`${cell.value ? cell.value : ' N.A.'}`}</div>,
    },
    {
      accessor: 'notes',
      sortable: false,
      Header: 'Provider Notes',
      width: 200,
      Cell: cell => <div>{getMarkUp(cell.value)}</div>,
    },
  ];
  const manualProps = {
    manual: true,
    pages: state.pagination.totalPages,
    onFetchData: fetchDataCompleted,
  };

  const getMarkUp = note => {
    if (note) {
      if (note.length > 8) {
        return (
          <p>
            {note.slice(0, 8)}..&nbsp;
            <a onClick={() => openNote(note)}>Read More</a>
          </p>
        );
      } else {
        return <p>{note}</p>;
      }
    } else {
      return ``;
    }
  };

  return (
    <div className="inventory">
      <div className="loader">
        <Spinner show={isFetching} />
      </div>
      <Table
        columns={columns}
        rows={state.completedRowsMap}
        customTopBar={null}
        className={`completed-request__table ${isFetching ? `loading` : ``}`}
        manualProps={manualProps}
        loading={isFetching}
      />
    </div>
  );
};

export default completedTable;
