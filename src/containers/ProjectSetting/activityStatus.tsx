import React from "react";

import { cloneDeep, debounce } from "lodash";
import Input from "../../components/Input/input";
// import ColorPicker from 'rc-color-picker'; (New Component)
import { connect } from "react-redux";
import { deleteActivityStatus, editActivityStatus, getActivityStatusList, NEW_ACTIVITY_STATUS_FAILURE, NEW_ACTIVITY_STATUS_SUCCESS, postActivityStatus } from "../../actions/pmo";
import Spinner from "../../components/Spinner";
import { getConvertedColorWithOpacity } from "../../utils/CommonUtils";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";

interface IActivityStatusProps {
  getActivityStatusList: any;
  postActivityStatus: any;
  editActivityStatus: any;
  deleteActivityStatus: any;
  milestone?: boolean;
  url?: string;
  purchaseOrderSetting: any;
}

interface IActivityStatusState {
  statusList: any[];
  open: boolean;
  loading: boolean;
}

class ActivityStatus extends React.Component<
  IActivityStatusProps,
  IActivityStatusState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  private debouncedFetch;

  constructor(props: IActivityStatusProps) {
    super(props);

    this.state = {
      statusList: [{
        title: "",
        color: "#f8af03",
        edited: true,
        titleError: '',
      }],
      open: true,
      loading: false,
    };
    this.debouncedFetch = debounce(this.passObjectToParent, 1000);
  }
  componentDidMount() {
    this.setState({ loading: true })
    this.props.getActivityStatusList(this.props.url).then(
      action => {
        this.setState({ loading: false, statusList: action.response })
      }
    )
  }

  addNew = () => {
    const newState = cloneDeep(this.state);
    newState.statusList.push({
      title: "",
      color: "#f8af03",
    });

    this.setState(newState);
  };

  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    if (rowData.id) {
      this.props.deleteActivityStatus(rowData, this.props.url).then(
        action => {
          if (action.type === NEW_ACTIVITY_STATUS_SUCCESS) {
            newState.statusList.splice(index, 1);
          }
          if (action.type === NEW_ACTIVITY_STATUS_FAILURE) {
            newState.statusList[index].titleError = `Couldn't delete`;
          }
          this.setState(newState);
        }
      )
    } else {
      newState.statusList.splice(index, 1);
      this.setState(newState);
    }
  };

  onSaveRowClick = (e, index, rowData) => {
    const newState = cloneDeep(this.state);
    const existingStatus = newState.statusList.findIndex(
      (el, idx) =>
        index !== idx && (el.title.trim() === rowData.title.trim())
    );
    if (existingStatus !== -1) {
      newState.statusList[index].edited = true;
      newState.statusList[index].titleError = "Status with same name already exists";
      this.setState(newState);
      return;
    }
    if (rowData.id) {
      this.props.editActivityStatus(rowData, this.props.url).then(
        action => {
          if (action.type === NEW_ACTIVITY_STATUS_SUCCESS) {
            newState.statusList[index].edited = false;
            newState.statusList[index].titleError = '';

          }
          if (action.type === NEW_ACTIVITY_STATUS_FAILURE) {
            newState.statusList[index].edited = true;
            newState.statusList[index].titleError = action.errorList.data.title.join(' ');
          }
          this.setState(newState);
        }
      )
    } else {
      this.props.postActivityStatus(rowData, this.props.url).then(
        action => {
          if (action.type === NEW_ACTIVITY_STATUS_SUCCESS) {
            newState.statusList[index].edited = false;
            newState.statusList[index].titleError = '';
            newState.statusList[index].id = action.response.id;

          }
          if (action.type === NEW_ACTIVITY_STATUS_FAILURE) {
            newState.statusList[index].edited = true;
            newState.statusList[index].titleError = action.errorList.data.title.join(' ');
          }
          this.setState(newState);
        }
      )
    }

  };

  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].edited = true;
    newState.statusList[index][e.target.name] = e.target.value;
    this.setState(newState);
    this.debouncedFetch(newState.statusList);
  };

  passObjectToParent = (list) => {
    const mappedObject = {};
    list.map((data, i) => {
      mappedObject[data.name] = data;
    });
  };
  isValid = () => {
    const error = {
      subscription_category_id: { ...ActivityStatus.emptyErrorState },
      device_category_row: { ...ActivityStatus.emptyErrorState },
    };
    let isValid = true;

    if (this.state.statusList && this.state.statusList.length > 0) {
      this.state.statusList.map((e) => {
        if (!e.connectwise_shipper || !e.name) {
          error.device_category_row.errorState = "error";
          // tslint:disable-next-line:max-line-length
          error.device_category_row.errorMessage = `Please enter required fields.`;
          isValid = false;
        }
      });
    }

    return isValid;
  };


  handleChangeColor = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.statusList[index].color = e.color;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };

  seDefault = (row, index, option: "is_default" | "is_overdue") => {
    const newState = cloneDeep(this.state);
    newState.statusList.map((x, sIndex) => {
      newState.statusList[sIndex][option] = false;
    });
    newState.statusList[index][option] = true;
    newState.statusList[index].edited = true;
    this.setState(newState);
  };
  render() {
    const statusList = this.state.statusList || [];

    return (
      <div className="activity-status">
        {
          statusList && statusList.length > 0 && !this.state.loading && (
            <div className="col-md-12 status-row" >

              <label className="col-md-3 field__label-label" >
                Label
             </label>
              <label className="col-md-2 field__label-label color-picker">
                Choose Color
              </label>
              <label className="col-md-2 field__label-label">
                Preview
             </label>

            </div>)}
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {
          statusList.length === 0 && !this.state.loading && (
            <div className="no-status col-md-6">
              No status available
            </div>
          )
        }
        {
          statusList && !this.state.loading &&
          statusList.map((row, index) => (
            <div className="col-md-12 status-row" key={index}>
              <Input
                field={{
                  label: "",
                  type: "TEXT",
                  value: row.title,
                  isRequired: false,
                }}
                width={3}
                labelIcon={'info'}
                name="title"
                onChange={(e) => this.handleChangeList(e, index)}
                placeholder={`Enter Status`}
                error={row.titleError && {
                  errorState: "error",
                  errorMessage: row.titleError,
                }}
                disabled={
                  (row.title === "Completed" || row.title === "Overdue") &&
                  !row.edited &&
                  row.id
                }
              />
              <div className="field-section color-picker  col-md-2">
                {/* <ColorPicker
                  color={row.color}
                  onChange={e => this.handleChangeColor(e, index)}
                  placement="topLeft"
                  defaultColor="#f8af03"
                  enableAlpha={false}
                /> */}
              </div>
              <div className="color-review-tag  col-md-2">
                <div className="field-section color-preview"
                  title={row.title}
                >
                  <div style={{ backgroundColor: row.color, borderColor: row.color }} className="left-column" />
                  <div style={{
                    color: this.props.milestone ? 'black' : '',
                    background: `${this.props.milestone ? row.color : getConvertedColorWithOpacity(row.color)}`
                  }} className="text">
                    {row.title}
                  </div>
                </div>
                {!this.props.milestone && (
                  <div className="set-container">
                    {row.is_default ? <div className="default"> Default</div> :
                      <div className="set-default" onClick={e => {
                        this.seDefault(row, index, "is_default")
                      }}>Set Default</div>}
                    {row.is_overdue ? <div className="default"> Overdue</div> :
                      <div className="set-default" onClick={e => {
                        this.seDefault(row, index, "is_overdue")
                      }}>Set Overdue</div>}
                  </div>
                )
                }
              </div>
              {
                row.edited && row.title && (
                  <img
                    className="saved col-md-1"
                    alt=""
                    src={'/assets/icons/tick-blue.svg'}
                    onClick={(e) => this.onSaveRowClick(e, index, row)}
                  />)
              }
              {((row.title !== "Completed" && row.title !== "Overdue") ||
                row.edited ||
                !row.id) &&
                !row.is_default && (
                  <SmallConfirmationBox
                    className="remove col-md-1"
                    onClickOk={() => this.onDeleteRowClick(index, row)}
                    text={"Status"}
                  />
                )}

            </div>
          ))
        }
        <div
          onClick={this.addNew}
          className="add-new-status"
        >Add Status</div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  getActivityStatusList: (url: string) => dispatch(getActivityStatusList(url)),
  postActivityStatus: (data: any, url: string) => dispatch(postActivityStatus(data, url)),
  editActivityStatus: (data: any, url: string) => dispatch(editActivityStatus(data, url)),
  deleteActivityStatus: (data: any, url: string) => dispatch(deleteActivityStatus(data, url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ActivityStatus);

