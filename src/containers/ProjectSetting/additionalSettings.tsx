import _cloneDeep from 'lodash/cloneDeep';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
import Input from '../../components/Input/input';
import { ADDITIONAL_SETTING_SUCCESS, boardStatusListPMO, GetAdditionalSetting, getServiceBoardPMO, GetWorkRoles, GetWorkTypes, updteAdditionalSetting } from '../../actions/pmo';
import { fetchProviderUsers } from '../../actions/provider/user';
import { getQuoteBusinessList, getQuoteStageList, getQuoteTypeList } from '../../actions/sow';

interface IAdditionalSettingsState {
  additionalSettings: any;
  additionalSettingsError: any;
  loading: boolean;
  statusList: any;
  boardList: any;
  businessList: any;
  workTypeList: any;
  workRoleList: any;
  opportunityStageList: any[];
  opportunityTypeList: any[];
}

interface IIProjectSettingProps extends ICommonProps {
  getContactRoles: any;
  createContactRole: any;
  deleteContactRole: any;
  fetchProviderUsers: any;
  providerUsers: any[];
  GetAdditionalSetting: any;
  getServiceBoardPMO: any;
  boardStatusListPMO: any;
  updteAdditionalSetting: any;
  getQuoteBusinessList: any;
  GetWorkRoles: any;
  GetWorkTypes: any;
  getQuoteStageList: any;
  getQuoteTypeList: any;
}

class AdditionalSettings extends Component<
  IIProjectSettingProps,
  IAdditionalSettingsState
> {
  customerMailBodyRef: any;

  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    additionalSettings: {
      default_project_manager: '',
      project_service_board: '',
      service_ticket_complete_status: '',
      project_acceptance_message: '',
      project_completed_item_threshold: 7,
      project_item_due_date_threshold: 7,
      default_work_role: '',
      default_work_type: '',
      default_business_unit: '',
      default_opportunity_type: '',
      default_opportunity_stage: '',
      engineer_default_work_role: '',
      engineer_default_work_type: '',
      engineer_default_business_unit: '',
    },
    additionalSettingsError: {
      default_project_manager: '',
      project_service_board: '',
      service_ticket_complete_status: '',
      default_opportunity_type: '',
      default_opportunity_stage: '',
    },
    loading: true,
    statusList: [],
    boardList: [],
    businessList: [],
    workTypeList: [],
    workRoleList: [],
    opportunityStageList: [],
    opportunityTypeList: [],
  });

  componentDidMount() {
    this.GetAdditionalSetting();
    this.props.fetchProviderUsers({ pagination: false });

    this.props.getServiceBoardPMO().then(
      action => {
        this.setState({ loading: false, boardList: action.response })
      }
    )
    this.props.GetWorkRoles().then(
      action => {
        this.setState({ loading: false, workRoleList: action.response })
      }
    )
    this.props.GetWorkTypes().then(
      action => {
        this.setState({ loading: false, workTypeList: action.response })
      }
    )

    this.props.getQuoteTypeList().then(
      a => {
        this.setState({ loading: false, opportunityTypeList: a.response })
      }
    )
    this.props.getQuoteStageList().then(
      a => {
        this.setState({ loading: false, opportunityStageList: a.response })
      }
    )
    this.props.getQuoteBusinessList().then(
      a => {
        this.setState({ loading: false, businessList: a.response })
      }
    )
  }
  GetAdditionalSetting = async () => {
    const projectRoles: any = await this.props.GetAdditionalSetting();
    this.setState({ additionalSettings: projectRoles.response });
    if (projectRoles.response && projectRoles.response.project_service_board) {
      this.props.boardStatusListPMO(projectRoles.response.project_service_board).then(
        action => {
          this.setState({ loading: false, statusList: action.response })
        }
      )
    }
  }

  handleChange = async (event: any) => {
    const newState = _cloneDeep(this.state);
    (newState.additionalSettings[event.target.name] as any) = event.target.value;
    (newState.additionalSettings.project_acceptance_message as any) = 'test';
    this.setState(newState, async () => {
      this.props.updteAdditionalSetting(this.state.additionalSettings, "put").then(
        action => {
          if (action.type === ADDITIONAL_SETTING_SUCCESS) {
            this.setState({ additionalSettings: action.response, additionalSettingsError: this.getEmptyState().additionalSettingsError });
          } else {
            this.setState({ additionalSettingsError: action.errorList.data });
          }
        }
      )
    });
  };

  onSubmitNote = (close?: boolean) => {
    this.props.updteAdditionalSetting(this.state.additionalSettings, "put").then(
      action => {
        if (action.type === ADDITIONAL_SETTING_SUCCESS) {
          this.setState({ additionalSettings: action.response, additionalSettingsError: this.getEmptyState().additionalSettingsError });
        } else {
          this.setState({ additionalSettingsError: action.errorList.data });
        }
      }
    )
  }
  handleChangeServiceBoard = async (event: any) => {
    const newState = _cloneDeep(this.state);
    (newState.additionalSettings[event.target.name] as any) = event.target.value;
    (newState.additionalSettings.service_ticket_complete_status as any) = '';
    (newState.loading as boolean) = true;
    this.setState(newState);
    this.props.boardStatusListPMO(event.target.value).then(
      action => {
        this.setState({ loading: false, statusList: action.response })
      }
    )
  };

  EngineerRoleMapping = () => {

    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row additional-setting">
          <Input
            field={{
              label: 'Project Service Board',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.project_service_board,
              options: this.state.boardList && this.state.boardList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="project_service_board"
            onChange={e => this.handleChangeServiceBoard(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.project_service_board && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.project_service_board,
            }}
          />
          <Input
            field={{
              label: 'Project Ticket Complete Status',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.service_ticket_complete_status,
              options: this.state.statusList && this.state.statusList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            loading={this.state.loading}
            name="service_ticket_complete_status"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.service_ticket_complete_status && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.service_ticket_complete_status,
            }}
          />
          <Input
            field={{
              label: 'Default Project Manager',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.default_project_manager,
              options: this.props.providerUsers && this.props.providerUsers.length && this.props.providerUsers.map(user => ({
                value: user.id,
                label: `${user.first_name} ${user.last_name}`,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_project_manager"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.default_project_manager && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.default_project_manager,
            }}
          />
          <Input
            field={{
              label: 'Due date threshold (days)',
              type: "NUMBER",
              value: this.state.additionalSettings && this.state.additionalSettings.project_item_due_date_threshold,
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="project_item_due_date_threshold"
            labelIcon="info"
            labelTitle={
              "Denotes the number before due_date a critical path item or\naction item should be counted as pending item."
            }            
            onChange={e => this.handleChange(e)}
            placeholder={``}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.project_item_due_date_threshold && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.project_item_due_date_threshold,
            }}
          />
          <Input
            field={{
              label: 'Completed items ageing period (days)',
              type: "NUMBER",
              value: this.state.additionalSettings && this.state.additionalSettings.project_completed_item_threshold,
              isRequired: true,
            }}
            labelIcon="info"
            labelTitle={
              "Denotes the number after which a completed critical path item,\naction item and risk item should be moved to completed item list"
            }
            width={4}
            multi={false}
            name="project_completed_item_threshold"
            onChange={e => this.handleChange(e)}
            placeholder={``}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.project_completed_item_threshold && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.project_completed_item_threshold,
            }}
          />
          <Input
            field={{
              label: 'Default Work Role',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.default_work_role,
              options: this.state.workRoleList && this.state.workRoleList.map(s => ({
                value: s.id,
                label: s.name,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_work_role"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.default_work_role && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.default_work_role,
            }}
          />
          <Input
            field={{
              label: 'Default Work Type',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.default_work_type,
              options: this.state.workTypeList && this.state.workTypeList.map(s => ({
                value: s.id,
                label: s.name,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_work_type"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.default_work_type && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.default_work_type,
            }}
          />
          <Input
            field={{
              label: 'Default Business Unit',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.default_business_unit,
              options: this.state.businessList && this.state.businessList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_business_unit"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.default_business_unit && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.default_business_unit,
            }}
          />
             <Input
            field={{
              label: 'Engineer Default Work Role',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.engineer_default_work_role,
              options: this.state.workRoleList && this.state.workRoleList.map(s => ({
                value: s.id,
                label: s.name,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="engineer_default_work_role"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.engineer_default_work_role && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.engineer_default_work_role,
            }}
          />
          <Input
            field={{
              label: 'Engineer Default Work Type',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.engineer_default_work_type,
              options: this.state.workTypeList && this.state.workTypeList.map(s => ({
                value: s.id,
                label: s.name,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="engineer_default_work_type"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.engineer_default_work_type && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.engineer_default_work_type,
            }}
          />
          <Input
            field={{
              label: 'Engineer Default Business Unit',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.engineer_default_business_unit,
              options: this.state.businessList && this.state.businessList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="engineer_default_business_unit"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.engineer_default_business_unit && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.engineer_default_business_unit,
            }}
          />
          <Input
            field={{
              label: 'Default Opportunity Stage',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.default_opportunity_stage,
              options: this.state.opportunityStageList && this.state.opportunityStageList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_opportunity_stage"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.default_opportunity_stage && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.default_opportunity_stage,
            }}
          />
          <Input
            field={{
              label: 'Default Opportunity Type',
              type: "PICKLIST",
              value: this.state.additionalSettings && this.state.additionalSettings.default_opportunity_type,
              options: this.state.opportunityTypeList && this.state.opportunityTypeList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_opportunity_type"
            onChange={e => this.handleChange(e)}
            placeholder={`Select`}
            error={this.state.additionalSettingsError && this.state.additionalSettingsError.default_opportunity_type && {
              errorState: "error",
              errorMessage: this.state.additionalSettingsError.default_opportunity_type,
            }}
          />
        </div>
      </div>
    );
  };
  render() {

    return (
      <div className="col-md-12">
        {this.EngineerRoleMapping()}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  providerUsers: state.providerUser.providerUsers,
});

const mapDispatchToProps = (dispatch: any) => ({
  GetAdditionalSetting: () => dispatch(GetAdditionalSetting()),
  updteAdditionalSetting: (data: any, method: string) => dispatch(updteAdditionalSetting(data, method)),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  getServiceBoardPMO: () => dispatch(getServiceBoardPMO()),
  getQuoteBusinessList: () => dispatch(getQuoteBusinessList()),
  GetWorkRoles: () => dispatch(GetWorkRoles()),
  GetWorkTypes: () => dispatch(GetWorkTypes()),
  boardStatusListPMO: (id: any) => dispatch(boardStatusListPMO(id)),
  getQuoteStageList: () => dispatch(getQuoteStageList()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdditionalSettings);