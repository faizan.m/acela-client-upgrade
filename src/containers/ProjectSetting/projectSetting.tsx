import React, { Component } from "react";
import _cloneDeep from "lodash/cloneDeep";
import { connect } from "react-redux";
import { addSuccessMessage, addErrorMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import PMOCollapsible from "../../components/PMOCollapsible";
import ActivityStatus from "./activityStatus";
import ThresholdsIndicator from "./thresholdsIndicator";
import ProjectOverallStatusMapping from "./overallStatusMapping";
import ProjectTypes from "./projectTypes";
import EngineerTimesheetsSetting from "./timesheetSetting";
import {
  GetWorkTypes,
  getProjectRoles,
  GetAdditionalSetting,
  getProjectRolesMapping,
  updteAdditionalSetting,
  getPreSalesRolesMapping,
  updateProjectRolesMapping,
  getMeetingTemplateSetting,
  getProjectAfterHoursSetting,
  createMeetingTemplateSetting,
  updateMeetingTemplateSetting,
  saveProjectAfterHoursSetting,
  updateProjectPreSalesRolesMapping,
} from "../../actions/pmo";
import { fetchNoteTypes } from "../../actions/documentation";
import CustomerContactRole from "./CustomerContactRole";
import MeetingTemplate from "./meetingTemplate";
import AdditionalSettings from "./additionalSettings";
import EmailTemplate from "./EmailTemplate";
import HistoryPMOSetting from "./HistorySetting";
import CancelledCR from "./cancelledCRSetting";
import TimeEntrySetting from "./timeEntrySetting";
import MeetingEmailTemplate from "./meetingEmailTemplate";
import "./style.scss";

enum PageType {
  General,
  Meeting,
  MeetingTemplates,
  History,
  Timesheet,
}
interface IIProjectSettingState {
  engineerRoleMapping: number[];
  note_type_ids: number[];
  work_type_ids: number[];
  preSalesEngineerRoleMapping: number[];
  loading: boolean;
  isOpen: boolean;
  projectRoles?: any;
  projectNoteTypes?: any;
  projectWorkTypes?: any;
  currentPage: {
    pageType: PageType;
  };
  kickoffMeeting: IMeetingEmailTemplate;
  statusMeeting: IMeetingEmailTemplate;
  internalKickoff: IMeetingEmailTemplate;
  customerTouchMeeting: IMeetingEmailTemplate;
  closeOutMeeting: IMeetingEmailTemplate;
}

interface IIProjectSettingProps extends ICommonProps {
  addSuccessMessage: any;
  addErrorMessage: any;
  getProjectRoles: any;
  getProjectRolesMapping: any;
  getPreSalesRolesMapping: any;
  updateProjectRolesMapping: any;
  updateProjectPreSalesRolesMapping: any;
  getMeetingTemplateSetting: any;
  createMeetingTemplateSetting: any;
  updateMeetingTemplateSetting: any;
  fetchNoteTypes: any;
  updteAdditionalSetting: any;
  GetAdditionalSetting: any;
  GetWorkTypes: any;
  saveProjectAfterHoursSetting: any;
  getProjectAfterHoursSetting: any;
}

const EmptyMeetingTemplate = {
  email_subject: "",
  email_body_text: "",
  email_body_markdown: "",
  project_update: "",
  add_internal_contacts: false,
  add_customer_contacts: false,
  add_additional_contacts: false,
};

const meetingTypes = {
  Kickoff: "kickoffMeeting",
  Status: "statusMeeting",
  "Internal Kickoff": "internalKickoff",
  "Customer Touch": "customerTouchMeeting",
  "Close Out": "closeOutMeeting",
};

class ProjectSettings extends Component<
  IIProjectSettingProps,
  IIProjectSettingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Meeting,
    },
    isOpen: false,
    loading: false,
    note_type_ids: [],
    work_type_ids: [],
    projectWorkTypes: [],
    engineerRoleMapping: [],
    preSalesEngineerRoleMapping: [],
    statusMeeting: { ...EmptyMeetingTemplate },
    kickoffMeeting: { ...EmptyMeetingTemplate },
    internalKickoff: { ...EmptyMeetingTemplate },
    closeOutMeeting: { ...EmptyMeetingTemplate },
    customerTouchMeeting: { ...EmptyMeetingTemplate },
  });

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const defaultTab = query.get("tab");
    if (defaultTab) {
      this.changePage(PageType[defaultTab]);
    }
    this.fetchNoteTypes();
    this.getProjectRoleList();
    this.getProjectRoleMapping();
    this.getPreSalesRolesMapping();
    this.getMeetingTemplateSetting();
    this.GetAdditionalSetting();
    this.getWorkTypes();
    this.getAfterHoursSetting();
  }

  getProjectRoleList = async () => {
    const projectRoles: any = await this.props.getProjectRoles();
    this.setState({
      projectRoles: projectRoles.response.map((x) => ({
        label: x.name,
        value: x.id,
      })),
    });
  };

  getProjectRoleMapping = async () => {
    const mapping: any = await this.props.getProjectRolesMapping();
    this.setState({ engineerRoleMapping: mapping.response.role_ids });
  };

  getPreSalesRolesMapping = async () => {
    const mapping: any = await this.props.getPreSalesRolesMapping();
    this.setState({ preSalesEngineerRoleMapping: mapping.response.role_ids });
  };

  fetchNoteTypes = async () => {
    const projectNoteTypes: any = await this.props.fetchNoteTypes();
    this.setState({
      projectNoteTypes: projectNoteTypes.response.map((x) => ({
        label: x.note_type_name,
        value: x.note_type_id,
      })),
    });
  };

  GetAdditionalSetting = async () => {
    const projectRoles: any = await this.props.GetAdditionalSetting();
    this.setState({ note_type_ids: projectRoles.response.note_type_ids });
  };

  getWorkTypes = async () => {
    const data: any = await this.props.GetWorkTypes();
    this.setState({ projectWorkTypes: data.response });
  };

  getAfterHoursSetting = async () => {
    const data: any = await this.props.getProjectAfterHoursSetting();
    this.setState({ work_type_ids: data.response.work_type_ids });
  };

  getMeetingTemplateSetting = async () => {
    this.setState({ loading: true });
    const meetingMailTemplateResponse: any = await this.props.getMeetingTemplateSetting();
    const { results } = meetingMailTemplateResponse.response;

    const newState = _cloneDeep(this.state);
    results.map((data) => {
      (newState[meetingTypes[data.meeting_type]] as any) = data;
    });

    this.setState(newState);
    this.setState({ loading: false });
  };

  handleBlurRoleMapping = async () => {
    const mapping: any = await this.props.updateProjectRolesMapping(
      this.state.engineerRoleMapping,
      "put"
    );
    this.setState({ engineerRoleMapping: mapping.response.role_ids });
  };

  handleBlurPreSalesRoleMapping = async () => {
    const mapping: any = await this.props.updateProjectPreSalesRolesMapping(
      this.state.preSalesEngineerRoleMapping,
      "put"
    );
    this.setState({ preSalesEngineerRoleMapping: mapping.response.role_ids });
  };

  handleBlurProjectNoteTypeSetting = async () => {
    const data = { note_type_ids: this.state.note_type_ids };
    const response: any = await this.props.updteAdditionalSetting(
      data,
      "patch"
    );
    this.setState({ note_type_ids: response.response.note_type_ids });
  };

  handleBlurProjectAfterHoursSetting = async () => {
    const data = { work_type_ids: this.state.work_type_ids };
    const response: any = await this.props.saveProjectAfterHoursSetting(data);
    this.setState({ work_type_ids: response.response.work_type_ids });
  };

  handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newState = _cloneDeep(this.state);
    newState[event.target.name] = event.target.value;
    this.setState(newState);
  };

  EngineerRoleMapping = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label:
                "Choose Connectwise role to display the name of the Engineer for a project",
              type: "PICKLIST",
              value: this.state.engineerRoleMapping,
              options: this.state.projectRoles,
              isRequired: false,
            }}
            width={6}
            multi={true}
            name="engineerRoleMapping"
            onChange={this.handleChange}
            onBlur={this.handleBlurRoleMapping}
            placeholder={`Select`}
            loading={this.state.loading}
            className="engineer-role-mapping"
          />
        </div>
      </div>
    );
  };

  PreSalesEngineerRoleMapping = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Pre Sales Engineer for a project",
              type: "PICKLIST",
              value: this.state.preSalesEngineerRoleMapping,
              options: this.state.projectRoles,
              isRequired: false,
            }}
            width={6}
            multi={true}
            name="preSalesEngineerRoleMapping"
            onChange={this.handleChange}
            onBlur={this.handleBlurPreSalesRoleMapping}
            placeholder={`Select`}
            loading={this.state.loading}
            className="engineer-role-mapping"
          />
        </div>
      </div>
    );
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  updateMeetingTemplate = (data: IMeetingEmailTemplate) => {
    this.setState({ loading: true });
    if (data.isCreated) {
      // update template
      delete data.isCreated;
      const meetingMailTypeId =
        this.state[meetingTypes[data.meeting_type]] &&
        this.state[meetingTypes[data.meeting_type]].id;
      this.props
        .updateMeetingTemplateSetting(meetingMailTypeId, data)
        .then((action) => {
          this.getMeetingTemplateSetting();
          this.props.addSuccessMessage(
            `${data.meeting_type} Meeting Template updated!`
          );
          this.setState({ loading: false });
        });
    } else {
      // create template
      delete data.isCreated;
      this.props.createMeetingTemplateSetting(data).then((action) => {
        this.getMeetingTemplateSetting();
        this.props.addSuccessMessage(
          `${data.meeting_type} Meeting Template saved!`
        );
        this.setState({ loading: false });
      });
    }
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="pmo-tab__header">
        <div
          className={`pmo-tab__header-link ${
            currentPage.pageType === PageType.Meeting
              ? "pmo-tab__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Meeting)}
        >
          Meeting Settings
        </div>
        <div
          className={`pmo-tab__header-link ${
            currentPage.pageType === PageType.General
              ? "pmo-tab__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.General)}
        >
          General
        </div>
        <div
          className={`pmo-tab__header-link ${
            currentPage.pageType === PageType.MeetingTemplates
              ? "pmo-tab__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.MeetingTemplates)}
        >
          Meeting Email Templates
        </div>
        <div
          className={`pmo-tab__header-link ${
            currentPage.pageType === PageType.History
              ? "pmo-tab__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.History)}
        >
          History
        </div>
        <div
          className={`pmo-tab__header-link ${
            currentPage.pageType === PageType.Timesheet
              ? "pmo-tab__header-link--active"
              : ""
          }`}
          onClick={() => this.changePage(PageType.Timesheet)}
        >
          Timesheet
        </div>
      </div>
    );
  };

  renderProjectNoteType = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label:
                "Choose Connectwise note type to include in project notes.",
              type: "PICKLIST",
              value: this.state.note_type_ids,
              options: this.state.projectNoteTypes,
              isRequired: false,
            }}
            width={6}
            multi={true}
            name="note_type_ids"
            onChange={this.handleChange}
            onBlur={this.handleBlurProjectNoteTypeSetting}
            placeholder={`Select`}
            loading={this.state.loading}
            className="engineer-role-mapping"
          />
        </div>
      </div>
    );
  };

  renderProjectAfterHours = () => {
    return (
      <div className="tac-email col-md-12">
        <div className="col-md-12 row">
          <Input
            field={{
              label: "Choose Connectwise work type.",
              type: "PICKLIST",
              value: this.state.work_type_ids,
              options: this.state.projectWorkTypes.map((s) => ({
                value: s.id,
                label: s.name,
              })),
              isRequired: false,
            }}
            width={6}
            multi={true}
            name="work_type_ids"
            onChange={this.handleChange}
            onBlur={this.handleBlurProjectAfterHoursSetting}
            placeholder={`Select`}
            loading={this.state.loading}
            className="engineer-role-mapping"
          />
        </div>
      </div>
    );
  };

  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="pmo-settings">
        {this.renderTopBar()}
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {currentPage.pageType === PageType.General && (
          <div className="operations-setting">
            <PMOCollapsible
              label={"Engineer Role  Mapping"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">{this.EngineerRoleMapping()}</ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Pre Sales Engineer Role  Mapping"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                {this.PreSalesEngineerRoleMapping()}
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Customer Contact Roles "}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                <CustomerContactRole />
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Activity Status"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                <ActivityStatus />
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Milestone Status"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                <ActivityStatus
                  milestone={true}
                  url={"pmo/settings/milestone-status"}
                />
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Project Status Indicator Thresholds"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                <ThresholdsIndicator />
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Additional Settings"}
              isOpen={this.state.isOpen}
            >
              <AdditionalSettings />
            </PMOCollapsible>
            <PMOCollapsible
              label={"Project Overall Status Mapping"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                <ProjectOverallStatusMapping />
              </ul>
            </PMOCollapsible>
            <PMOCollapsible label={"Project Types"} isOpen={this.state.isOpen}>
              <ul className="namespace-list">
                <ProjectTypes />
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Project Note Types"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">{this.renderProjectNoteType()}</ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Project After Hours Mapping"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                {this.renderProjectAfterHours()}
              </ul>
            </PMOCollapsible>
            <PMOCollapsible
              label={"Cancelled Change Request Opportunity"}
              isOpen={this.state.isOpen}
            >
              <ul className="namespace-list">
                <CancelledCR />
              </ul>
            </PMOCollapsible>
          </div>
        )}
        {currentPage.pageType === PageType.Meeting && (
          <div className="operations-setting">
            <PMOCollapsible
              label={"Kick off Meeting"}
              isOpen={this.state.isOpen}
            >
              <MeetingTemplate meetingType={"Kickoff"} />
            </PMOCollapsible>
            <PMOCollapsible label={"Status Meeting"} isOpen={this.state.isOpen}>
              <MeetingTemplate meetingType={"Status"} />
            </PMOCollapsible>
            <PMOCollapsible
              label={"Time Entry Default Note"}
              isOpen={this.state.isOpen}
            >
              <TimeEntrySetting />
            </PMOCollapsible>
          </div>
        )}
        {currentPage.pageType === PageType.MeetingTemplates && (
          <div className="operations-setting">
            <PMOCollapsible
              label={"Customer Touch Meeting"}
              isOpen={this.state.isOpen}
            >
              <MeetingEmailTemplate
                meetingType={"Customer Touch"}
                mailTemplateData={this.state.customerTouchMeeting}
                setData={this.updateMeetingTemplate}
              />
            </PMOCollapsible>
            <PMOCollapsible
              label={"Kickoff Meeting"}
              isOpen={this.state.isOpen}
            >
              <MeetingEmailTemplate
                meetingType={"Kickoff"}
                mailTemplateData={this.state.kickoffMeeting}
                setData={this.updateMeetingTemplate}
              />
            </PMOCollapsible>
            <PMOCollapsible
              label={"Internal Kickoff Meeting"}
              isOpen={this.state.isOpen}
            >
              <MeetingEmailTemplate
                meetingType={"Internal Kickoff"}
                mailTemplateData={this.state.internalKickoff}
                setData={this.updateMeetingTemplate}
              />
            </PMOCollapsible>
            <PMOCollapsible label={"Status Meeting"} isOpen={this.state.isOpen}>
              <MeetingEmailTemplate
                meetingType={"Status"}
                mailTemplateData={this.state.statusMeeting}
                setData={this.updateMeetingTemplate}
              />
            </PMOCollapsible>
            <PMOCollapsible
              label={"Close Out Meeting"}
              isOpen={this.state.isOpen}
            >
              <MeetingEmailTemplate
                meetingType={"Close Out"}
                mailTemplateData={this.state.closeOutMeeting}
                setData={this.updateMeetingTemplate}
              />
            </PMOCollapsible>
            <PMOCollapsible label={"Change Request"} isOpen={this.state.isOpen}>
              <EmailTemplate url={"settings/change-request-mail-template"} />
            </PMOCollapsible>
          </div>
        )}
        {currentPage.pageType === PageType.Timesheet && (
          <PMOCollapsible label={"Timesheet Setting"} isOpen={true}>
            <ul className="namespace-list">
              <EngineerTimesheetsSetting />
            </ul>
          </PMOCollapsible>
        )}
        {currentPage.pageType === PageType.History && <HistoryPMOSetting />}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getProjectRoles: () => dispatch(getProjectRoles()),
  fetchNoteTypes: () => dispatch(fetchNoteTypes()),
  getProjectRolesMapping: () => dispatch(getProjectRolesMapping()),
  getPreSalesRolesMapping: () => dispatch(getPreSalesRolesMapping()),
  getMeetingTemplateSetting: () => dispatch(getMeetingTemplateSetting()),
  createMeetingTemplateSetting: (data) =>
    dispatch(createMeetingTemplateSetting(data)),
  updateMeetingTemplateSetting: (meetingTypeId, data) =>
    dispatch(updateMeetingTemplateSetting(meetingTypeId, data)),
  updateProjectRolesMapping: (data: any, method: string) =>
    dispatch(updateProjectRolesMapping(data, method)),
  updateProjectPreSalesRolesMapping: (data: any, method: string) =>
    dispatch(updateProjectPreSalesRolesMapping(data, method)),
  GetAdditionalSetting: () => dispatch(GetAdditionalSetting()),
  updteAdditionalSetting: (data: any, method: string) =>
    dispatch(updteAdditionalSetting(data, method)),
  GetWorkTypes: () => dispatch(GetWorkTypes()),
  saveProjectAfterHoursSetting: (data) =>
    dispatch(saveProjectAfterHoursSetting(data)),
  getProjectAfterHoursSetting: () => dispatch(getProjectAfterHoursSetting()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectSettings);
