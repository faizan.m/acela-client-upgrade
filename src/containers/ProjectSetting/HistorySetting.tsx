import React, { ReactNode } from "react";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { pmoCommonAPI } from "../../actions/pmo";
import Spinner from "../../components/Spinner";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import "./historyStyle.scss";

interface IProjectProps {
  pmoCommonAPI: (
    api: string,
    type: string,
    data: any,
    params?: IScrollPaginationFilters
  ) => Promise<any>;
}

interface IProjectState {
  historyList: IProjectHistory[];
  pagination: IScrollPaginationFilters;
  noData: boolean;
  loading: boolean;
}

class HistoryPMOSetting extends React.Component<IProjectProps, IProjectState> {
  constructor(props: IProjectProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    historyList: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
    },
    noData: false,
    loading: true,
  });

  componentDidMount() {
    this.fetchMoreData(true);
  }

  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
      };
    }
    if (prevParams.nextPage !== null) {
      this.props
        .pmoCommonAPI(`settings/history`, "get", "", prevParams)
        .then((action) => {
          if (action.response) {
            let historyList = [];
            if (clearData) {
              historyList = [...action.response.results];
            } else {
              historyList = [
                ...this.state.historyList,
                ...action.response.results,
              ];
            }
            const newPagination = {
              currentPage: action.response.links.page_number,
              nextPage: action.response.links.next_page_number,
              page_size: this.state.pagination.page_size,
            };

            this.setState({
              pagination: newPagination,
              historyList,
              noData: historyList.length === 0 ? true : false,
              loading: false,
            });
          } else {
            this.setState({
              noData: true,
              loading: false,
            });
          }
        });
    }
  };

  listingRows = (history: IProjectHistory, index: number) => {
    return (
      <div className={`history-card`} key={index}>
        <div className="field updated-fields">
          <div className="tags">
            <div className={`action-tag ${history.action}`}>
              {history.action}
            </div>
            <div className={`name-tag ${history.model_display_name}`}>
              {history.model_display_name}
            </div>
            <div className="actor"> {history.actor} </div>
            <div className="date">
              {fromISOStringToFormattedDate(
                history.timestamp,
                "MMM DD, YYYY hh:mm a"
              )}
            </div>
          </div>
          {history &&
            history.changes_display_dict &&
            Object.entries(history.changes_display_dict).map(([key, value]) => {
              if (key.includes("markdown")) {
                return null;
              }
              return (
                <div className="data-row" key={key}>
                  <div className="key"> {key} </div>
                  <div className="old"> {showValue(value[0], key)} </div>
                  <div className="new"> {showValue(value[1], key)} </div>
                </div>
              );
            })}
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="list-panel-history">
        <div className="header-actions">
          <div className="left"></div>
        </div>
        <div className="list">
          <div className={`header columns`}>
            <div className={`field data-row  updated-fields`}>
              <div className="keys">Field Name</div>
              <div className="keys">Old Value</div>
              <div className="keys">New Value</div>
            </div>
          </div>
          <div
            id="scrollableDiv"
            className="custom-scroll"
            style={{ height: "72vh", overflow: "auto" }}
          >
            <InfiniteScroll
              dataLength={this.state.historyList.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
            >
              {this.state.historyList.map((history, index) =>
                this.listingRows(history, index)
              )}
              {!this.state.loading &&
                this.state.pagination.nextPage &&
                this.state.historyList.length > 0 && (
                  <div
                    className="load-more"
                    onClick={() => {
                      this.fetchMoreData(false);
                    }}
                  >
                    load more data...
                  </div>
                )}
            </InfiniteScroll>
            {this.state.noData && this.state.historyList.length === 0 && (
              <div className="no-data">No History Available</div>
            )}
            {this.state.loading && (
              <div className="loader">
                <Spinner show={true} />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  pmoCommonAPI: (
    api: string,
    type: string,
    data: any,
    params?: IScrollPaginationFilters
  ) => dispatch(pmoCommonAPI(api, type, data, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryPMOSetting);

export const showValue = (value: string, key: string): ReactNode => {
  if (key === "color") {
    return (
      <div
        style={{ background: value }}
        className="color-show"
        title={replaceOriginalValue(value)}
      />
    );
  }

  return replaceOriginalValue(value);
};

export const replaceOriginalValue = (v: string): string => {
  const value = v.includes("]") ? "array" : v;
  switch (value) {
    case "None":
      return "-";
    case "True":
      return "Yes";
    case "False":
      return "No";

    case "array":
      return v
        .replace(/['\[\]]/g, "")
        .split(",")
        .join(",\n");

    default:
      return value;
  }
};
