import { cloneDeep } from "lodash";
import _ from "lodash";
import React, { Component } from "react";
import { Mention, MentionsInput } from "react-mentions";
import ReactQuill from "react-quill"; // ES6
import "react-quill/dist/quill.snow.css"; // ES6
import "quill-mention";
import { connect } from "react-redux";
import "./style.scss";
import SquareButton from "../../components/Button/button";
import Spinner from "../../components/Spinner";
import { pmoCommonAPI } from "../../actions/pmo";
import { commonFunctions } from "../../utils/commonFunctions";

interface IEmailTemplateState {
  meeting: IMeetingEmailTemplate;
  savedSuccess: boolean;
  loading: boolean;
}

interface IEmailTemplateProps extends ICommonProps {
  pmoCommonAPI: (api: string, type: string, data?: any) => Promise<any>;
}
interface IPassedProps {
  url?: string;
}

class EmailTemplate extends Component<
  IPassedProps & IEmailTemplateProps,
  IEmailTemplateState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  customerMailBodyRef: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    meeting: {
      email_subject: "",
      email_body_text: "",
      email_body_markdown: "",
    },
    savedSuccess: false,
    loading: false,
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariablesCR;
      }

      renderList(values, searchTerm);
    },
  };

  componentDidMount() {
    this.getMeetingTemplateSetting();
  }

  handleChangeMailSubject = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    (newState.meeting.email_subject as string) = e.target.value;
    this.setState(newState);
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChange = (content, delta, source, editor) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    (newState.meeting.email_body_text as any) = this.getContentFromObject(
      object
    );
    (newState.meeting.email_body_markdown as any) = content;
    this.setState({ ...newState });
  };

  getMeetingTemplateSetting = async () => {
    const res: any = await this.props.pmoCommonAPI(this.props.url, "get");
    const newState = cloneDeep(this.state);
    (newState.meeting as any) = res.response;
    this.setState({ loading: false });
    this.setState(newState);
  };

  updateTemplate = (data: any) => {
    this.setState({ loading: true });

    const mailBodyTxtArr = data.email_body_text.split(" ");

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith("{")) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    });

    data.email_body_text = mailBodyTxtFinalArr.join(" ");

    const regex = /#RULE#/gi;
    data.email_subject = data.email_subject.replace(regex, "");

    if (data.id) {
      // update template
      delete data.isCreated;
      this.props.pmoCommonAPI(this.props.url, "put", data).then((action) => {
        this.getMeetingTemplateSetting();
        this.setState({ loading: false });
      });
    } else {
      // create template
      delete data.isCreated;
      this.props.pmoCommonAPI(this.props.url, "post", data).then((action) => {
        this.getMeetingTemplateSetting();
        this.setState({ loading: false });
      });
    }
  };
  render() {
    return (
      <div className="meeting-email-template-container col-md-12">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div className="email-template-inputs">
          <div className="col-md-12 row customer-email-field">
            <label className="col-md-3 field__label-label email-subject">
              <span>
                <b>Email Subject</b> (The subject that wil be populated by
                default in the Change Request Email)
              </span>
            </label>
            <div className="mail-subject-mention-input">
              <MentionsInput
                markup="[__display__]"
                value={
                  (this.state.meeting && this.state.meeting.email_subject) || ""
                }
                onChange={(e) => this.handleChangeMailSubject(e)}
                className={"outer"}
              >
                <Mention
                  trigger="#"
                  data={commonFunctions.emailTemplateOptions()}
                  className={"inner-drop"}
                  markup="#RULE#__display__"
                />
              </MentionsInput>
            </div>
          </div>
          <div className="col-md-12 row customer-email-field">
            <label className="col-md-3 field__label-label email-subject">
              <span>
                <b>Email Message</b> (The default message that will be shown in
                the email sent for Change Request )
              </span>
            </label>
            <div className="email-editor-wrapper">
              <ReactQuill
                className="email-body-editor"
                ref={this.customerMailBodyRef}
                value={
                  (this.state.meeting &&
                    this.state.meeting.email_body_markdown) ||
                  ""
                }
                onChange={this.handleChange}
                modules={{ mention: this.mentionModule }}
              />
            </div>
          </div>
          {this.state.meeting &&
            this.state.meeting.email_body_markdown !== "" &&
            this.state.meeting.email_subject !== "" && (
              <SquareButton
                onClick={(e) => this.updateTemplate(this.state.meeting)}
                content="Save"
                bsStyle={"primary"}
                className="save-mail-btn"
              />
            )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore, ownProps: IPassedProps) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  pmoCommonAPI: (api: string, type: string, data?: any) =>
    dispatch(pmoCommonAPI(api, type, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailTemplate);
