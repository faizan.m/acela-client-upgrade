import React from "react";

import { cloneDeep } from "lodash";
import Input from "../../components/Input/input";
import { connect } from "react-redux";
import Spinner from "../../components/Spinner";
import { getConvertedColorWithOpacity } from "../../utils/CommonUtils";
import Checkbox from "../../components/Checkbox/checkbox";
import { fetchProviderBoardStatus } from "../../actions/provider/integration";
import { deleteProjectBoard, deleteProjectBoardStatus, editProjectBoard, getProjectBoardList, getProjectBoardNames, getProjectStatusList, NEW_PROJECT_BOARD_FAILURE, NEW_PROJECT_BOARD_SUCCESS, postProjectBoard } from "../../actions/pmo";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import ColorPicker from "../../components/SmallColorPicker/SmallColorPicker";

interface IProjectOverallStatusMappingProps {
  getProjectBoardList: any;

}

interface IProjectOverallStatusMappingState {
  boardList: IProjectBoard[];
  open: boolean;
  loading: boolean;
  boardNames?: any[];
  statusList: any[];
}
interface IProjectBoard {
  name: string,
  connectwise_id: number,
  provider: number,
  id?: number,
  statuses?: IBoardStatus[],
  boardStatusList?: any[];
  loading?: boolean;
  edited?: boolean;
  errorMessage?: string;
}
interface IBoardStatus {
  title: string,
  titleError?: string,
  connectwise_id?: number,
  is_default: boolean,
  is_open: boolean,
  color: string,
  edited?: boolean,
  id?: number,
}

class ProjectOverallStatusMapping extends React.Component<
  any,
  IProjectOverallStatusMappingState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  constructor(props: IProjectOverallStatusMappingProps) {
    super(props);

    this.state = {
      boardList: [{
        name: '',
        connectwise_id: null,
        provider: null,
        edited: true,
        statuses: [{
          title: "",
          is_default: true,
          is_open: true,
          color: "#f8af03",
          edited: true,
        }],
      }],
      statusList: [],
      open: true,
      loading: false,
    };
  }
  componentDidMount() {
    this.setState({ loading: true })
    this.props.getProjectBoardList().then(
      action => {
        this.setState({ loading: false, boardList: action.response }, () => {
        });
      }
    )
    this.getProjectBoardNameList();
    this.getProjectStatusList();
  }

  getProjectBoardNameList = async () => {
    const boardNames: any = await this.props.getProjectBoardNames();
    this.setState({ boardNames: boardNames.response.map(x => ({ 'label': x.label, 'value': x.id })) })
  }

  getProjectStatusList = async () => {
    const list: any = await this.props.getProjectStatusList();
    this.setState({ statusList: list && list.response && list.response.map(x => ({ 'label': x.label, 'value': x.id })) })
  }
  addNewStaus = (boardIndex) => {
    const newState = cloneDeep(this.state);
    newState.boardList[boardIndex].edited = true;
    newState.boardList[boardIndex].statuses.push({
      title: "",
      color: "#f8af03",
      is_default: true,
      is_open: true,
    });

    this.setState(newState);
  };

  addProjectBoard = () => {
    const newState = cloneDeep(this.state);
    newState.boardList.push({
      name: '',
      connectwise_id: null,
      provider: null,
      edited: true,
      statuses: [{
        title: "",
        is_default: false,
        is_open: true,
        color: "#f8af03",
      }],
    });

    this.setState(newState);
  };
  getBoardStatus = async (index, id) => {
    const newState = cloneDeep(this.state);
    newState.boardList[index].name = this.state.boardNames.find(x => x.value === id).label;
    newState.boardList[index].connectwise_id = id;
    this.setState(newState);
  };
  onDeleteRowClick = (boardIndex, index, bordData, status) => {
    const newState = cloneDeep(this.state);
    if (status.id) {
      this.props.deleteProjectBoardStatus(bordData,status.id).then(
        action => {
          if (action.type === NEW_PROJECT_BOARD_SUCCESS) {
            newState.boardList[boardIndex].statuses.splice(index, 1);
          }
          this.setState(newState);
        }
      )
    } else {
      newState.boardList[boardIndex].statuses.splice(index, 1);
      this.setState(newState);
    }
    
  };

  onDeleteProject = (index, bordData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    if (bordData.id) {
      this.props.deleteProjectBoard(bordData).then(
        action => {
          if (action.type === NEW_PROJECT_BOARD_SUCCESS) {
            newState.boardList.splice(index, 1);
          }
          this.setState(newState);
        }
      )
    } else {
      newState.boardList.splice(index, 1);
    }
  };

  onSaveRowClick = (e, index, bordData) => {
    const newState = cloneDeep(this.state);
    if(this.isValidProjectOverallStatusMapping(index)){
    delete bordData.edited;
    if (bordData.id) {
      this.props.editProjectBoard(bordData).then(
        action => {
          let statusesNew = newState.boardList[index].statuses;
          if (action.type === NEW_PROJECT_BOARD_SUCCESS) {
            newState.boardList[index].edited = false;
            statusesNew.map(s => s.titleError = '');
            newState.boardList[index].statuses = statusesNew;
            newState.boardList[index].errorMessage = '';
          }
          if (action.type === NEW_PROJECT_BOARD_FAILURE) {
            newState.boardList[index].edited = true;
            statusesNew.map((status, i) => {
              statusesNew[i].titleError = action.errorList.data.statuses[i].title && action.errorList.data.statuses[i].title.join(' ');
            })
          }
          newState.boardList[index].statuses = statusesNew;
          this.setState(newState);
        }
      )
    } else {
      this.props.postProjectBoard(bordData).then(
        action => {
          let statusesNew = newState.boardList[index].statuses;

          if (action.type === NEW_PROJECT_BOARD_SUCCESS) {
            newState.boardList[index].edited = false;
            newState.boardList[index].id = action.response.id;
            statusesNew.map(s => s.titleError = '');
          }
          if (action.type === NEW_PROJECT_BOARD_FAILURE) {
            newState.boardList[index].edited = true;
            statusesNew.map((status, i) => {
              statusesNew[i].titleError = action.errorList.data.statuses[i].title && action.errorList.data.statuses[i].title.join(' ');
            })
          }
          newState.boardList[index].statuses = statusesNew;
          this.setState(newState);
        }
      )
    }
  }

  };

  showDelete = (statuses, row) => {
    const statusLength = statuses.filter(s => s.connectwise_id === row.connectwise_id).length;
    if (statusLength > 1) {
      return !row.is_default
    }
    return true;
  };

  handleChangeList = (e, boardIndex, index) => {
    const newState = cloneDeep(this.state);
    const value = e.target.value;
    newState.boardList[boardIndex].edited = true;
    newState.boardList[boardIndex].statuses[index][e.target.name] = value;
    if (e.target.name === 'connectwise_id') {
      if (newState.boardList[boardIndex].statuses.filter(s => s.connectwise_id === value).length > 1) {
        newState.boardList[boardIndex].statuses.map((x, sIndex) => {
          if (x.connectwise_id === value) {
            newState.boardList[boardIndex].statuses[sIndex].is_default = false;
          }
        });
        newState.boardList[boardIndex].statuses[index].is_default = true;
      }
    }
    this.setState(newState);
  };
  onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };
  
  isValidProjectOverallStatusMapping = (boardIndex) => {
    const newState = cloneDeep(this.state);
    const cwIds = newState.boardList[boardIndex].statuses.map(x=>x.connectwise_id).filter(this.onlyUnique);
    const defs = newState.boardList[boardIndex].statuses.filter(x=>x.is_default === true);
    let isValid = true;
    if(cwIds.length === defs.length){
      newState.boardList[boardIndex].errorMessage = '';
      isValid = true;
    } else {
      newState.boardList[boardIndex].errorMessage = 'Please check each group of Connectwise Status has Default set';
      isValid =  false;
    }
    this.setState(newState);
    return isValid;

  };
  handleChangeCheckBox = (e, boardIndex, index) => {
    const newState = cloneDeep(this.state);
    const value = e.target.checked;
    newState.boardList[boardIndex].edited = true;
    newState.boardList[boardIndex].statuses[index][e.target.name] = value;
    this.setState(newState);
  };
  seDefault = (row, boardIndex, index) => {
    const newState = cloneDeep(this.state);
    newState.boardList[boardIndex].edited = true;
    newState.boardList[boardIndex].statuses.map((x, sIndex) => {
      if (x.connectwise_id === row.connectwise_id) {
        newState.boardList[boardIndex].statuses[sIndex].is_default = false;
      }
    });
    newState.boardList[boardIndex].statuses[index].is_default = true;
    this.setState(newState);
  };

  handleChangeColor = (e, boardIndex, index) => {
    const newState = cloneDeep(this.state);
    newState.boardList[boardIndex].statuses[index].color = e;
    newState.boardList[boardIndex].edited = true;
    this.setState(newState);
  };

  statuses = (boardIndex) => {
    const statuses = this.state.boardList[boardIndex].statuses
    return (
      <div className="activity-status">
        {
          this.state.boardList[boardIndex].name &&
          this.state.boardList[boardIndex].statuses.length > 0 && !this.state.loading && (
            <div className="col-md-12 status-row" >
              <label className="col-md-3 field__label-label">
                Connectwise Status
              </label>
              <label className="col-md-3 field__label-label">
                Acela Status Label
             </label>
              <label className="col-md-2 field__label-label color-picker">
                Choose Color
              </label>
              <label className="col-md-2 field__label-label">
                Preview
             </label>
              <label className="col-md-2 field__label-label" >
                Is Open Status
             </label>
            </div>)}
        {
          this.state.boardList[boardIndex].name &&
          statuses && !this.state.loading &&
          statuses.sort((a, b) => a.connectwise_id - b.connectwise_id)
            .map((row, index) => (
              <div className="col-md-12 status-row" key={index}>
                <Input
                  field={{
                    label: '',
                    type: "PICKLIST",
                    value: row.connectwise_id,
                    options: this.state.statusList,
                    isRequired: false,
                  }}
                  width={3}
                  name="connectwise_id"
                  onChange={(e) => this.handleChangeList(e, boardIndex, index)}
                  placeholder={`Select`}
                  loading={this.state.boardList[boardIndex].loading}
                />
                <Input
                  field={{
                    label: "",
                    type: "TEXT",
                    value: row.title,
                    isRequired: false,
                  }}
                  width={3}
                  name="title"
                  onChange={(e) => this.handleChangeList(e, boardIndex, index)}
                  placeholder={`Enter Status`}
                  error={row.titleError && {
                    errorState: "error",
                    errorMessage: row.titleError,
                  }}
                />
                <div className="field-section color-picker  col-md-2">
                  <ColorPicker
                    iconClass="status-svg-images-black"
                    color={row.color}
                    onSelectColor={e => this.handleChangeColor(e, boardIndex, index)}
                  />
                </div>
                <div className="col-md-2 color-review-tag">
                  <div className="color-preview"
                    title={row.title}
                  >
                    <div style={{ backgroundColor: row.color, borderColor: row.color }} className="left-column" />
                    <div style={{ background: `${getConvertedColorWithOpacity(row.color)}` }} className="text">
                      {row.title}
                    </div>
                  </div>
                  {
                      row.is_default ? <div className="default"  onClick={e => {
                        this.seDefault(row, boardIndex, index)
                      }}> Default</div> :
                        <div className="set-default" onClick={e => {
                          this.seDefault(row, boardIndex, index)
                        }}>Set Default</div>
                  }
                </div>


                <div className="field-section col-md-1 searchbox_container">
                  <Checkbox
                    isChecked={row.is_open}
                    name="is_open"
                    onChange={(e) => this.handleChangeCheckBox(e, boardIndex, index)}
                  >

                  </Checkbox>
                </div>
                {
                  this.showDelete(statuses, row) && (
                    <SmallConfirmationBox
                      className='delete-icon-board remove col-md-1  delete-icon'
                      onClickOk={() => this.onDeleteRowClick(boardIndex, index,this.state.boardList[boardIndex], row)}
                      text={'Board Status'}
                    />
                  )
                }

              </div>
            ))

        }
        {
          this.state.boardList[boardIndex].name &&
          <div
            onClick={() => this.addNewStaus(boardIndex)}
            className="add-new-status"
          >Add Status</div>
        }

      </div>)

  }
  render() {
    const boardList = this.state.boardList;

    return (
      <div className="project-board board-list-status">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {
          boardList.length === 0 && !this.state.loading && (
            <div className="no-status col-md-6">
              No status available
            </div>
          )
        }
        {
          this.state.boardList && !this.state.loading &&
          this.state.boardList.map((row, index) => (
            <div className="col-md-12 board-row" key={index}>
              <div className="header">
                <Input
                  field={{
                    label: 'Board Name',
                    type: "PICKLIST",
                    value: this.state.boardList[index].connectwise_id,
                    options: this.state.boardNames &&
                      this.state.boardNames.map(
                        x => ({ ...x, disabled: this.state.boardList.map(b => b.connectwise_id).includes(x.value) })
                      ) || [],
                    isRequired: false,
                  }}
                  width={4}
                  name="connectwise_id"
                  onChange={e => this.getBoardStatus(index, e.target.value)}
                  placeholder={`Select`}
                  loading={this.state.loading}
                />
                <div className="error-message">{row.errorMessage}</div>
                <div className="row-action">
                  {
                    row.edited && (
                      <img
                        className="saved col-md-1"
                        alt=""
                        src={'/assets/icons/tick-blue.svg'}
                        onClick={(e) => this.onSaveRowClick(e, index, row)}
                      />)
                  }
                  <SmallConfirmationBox
                    className='delete-icon-board'
                    onClickOk={() => this.onDeleteProject(index, row)}
                    text={'Board'}
                  />
                </div>
              </div>
              {this.statuses(index)}
            </div>
          ))
        }
        {
          this.state.boardNames && this.state.boardNames.length !== this.state.boardList.length &&
          <div
            onClick={this.addProjectBoard}
            className="add-new-project-board"
          >Add Project Board</div>
        }

      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProviderBoardStatus: (id: number) =>
    dispatch(fetchProviderBoardStatus(id)),
  getProjectBoardList: () => dispatch(getProjectBoardList()),
  postProjectBoard: (data: any) => dispatch(postProjectBoard(data)),
  editProjectBoard: (data: any) => dispatch(editProjectBoard(data)),
  deleteProjectBoard: (data: any) => dispatch(deleteProjectBoard(data)),
  getProjectBoardNames: () => dispatch(getProjectBoardNames()),
  getProjectStatusList: () => dispatch(getProjectStatusList()),
  deleteProjectBoardStatus: (data: any,StatusId:any) => dispatch(deleteProjectBoardStatus(data,StatusId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectOverallStatusMapping);

