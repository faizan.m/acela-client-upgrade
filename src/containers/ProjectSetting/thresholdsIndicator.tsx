import React from "react";

import { cloneDeep } from "lodash";
import Input from "../../components/Input/input";
import { connect } from "react-redux";
import { FETCH_DATE_S_LIST_SUCCESS, FETCH_TIME_S_LIST_SUCCESS, getDateStatusList, getTimeStatusList, NEW_DATE_S_SUCCESS, NEW_TIME_S_SUCCESS, postDateStatusList, postTimeStatusList } from "../../actions/pmo";
import Spinner from "../../components/Spinner";

interface IThresholdsIndicatorProps {
  getActivityStatusList: any;

}

interface IThresholdsIndicatorState {
  dateList: any[];
  timeList: any[];
  loading: boolean;
  timeEdited: boolean;
  dateEdited: boolean;
}

class ThresholdsIndicator extends React.Component<
  any,
  IThresholdsIndicatorState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IThresholdsIndicatorProps) {
    super(props);

    this.state = {
      dateList: [
        {
          "slot_range": {
            "lower": 0,
            "upper": 20,
          },
          "color": "#dba629"
        },
        {
          "slot_range": {
            "lower": 21,
            "upper": 10000,
          },
          "color": "#176f07"
        }
      ],
      timeList: [
        {
          "slot_range": {
            "lower": 0,
            "upper": 10,
          },
          "color": "#db1643"
        },
        {
          "slot_range": {
            "lower": 10,
            "upper": 20,
          },
          "color": "#dba629"
        },
        {
          "slot_range": {
            "lower": 20,
            "upper": 101,
          },
          "color": "#176f07"
        }
      ],
      loading: false,
      timeEdited: true,
      dateEdited: true,
    };
  }
  componentDidMount() {
    this.setState({ loading: true })
    this.props.getTimeStatusList().then(
      action => {
        if (action.type === FETCH_TIME_S_LIST_SUCCESS && action.response.length !== 0) {
          this.setState({ timeEdited: false, loading: false, timeList: action.response })
        }
        this.setState({ loading: false })
      }
    );
    this.props.getDateStatusList().then(
      action => {
        if (action.type === FETCH_DATE_S_LIST_SUCCESS && action.response.length !== 0) {
          this.setState({dateEdited : false, loading: false, dateList:  action.response })
        }
        this.setState({ loading: false })
      }
    );
  }

  onSaveTimeClick = (e) => {
    const newState = cloneDeep(this.state);
    this.props.postTimeStatusList(newState.timeList).then(
      action => {
        if (action.type === NEW_TIME_S_SUCCESS) {
          (newState.timeEdited as boolean) = false;
        }
        this.setState(newState);
      }
    )
  };
  onSaveDateClick = (e) => {
    const newState = cloneDeep(this.state);
    this.props.postDateStatusList(newState.dateList).then(
      action => {
        if (action.type === NEW_DATE_S_SUCCESS) {
          (newState.dateEdited as boolean) = false;
        }
        this.setState(newState);
      }
    )
  };

  handleChangeColor = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.dateList[index].color = e.color;
    this.setState(newState);
  };
  handleChangeFirst = (event: any, type, rangeType, index) => {
    const newState = cloneDeep(this.state);
    if(parseInt(event.target.value) > 0 &&  parseInt(event.target.value) < 99){
      newState[type][index]['slot_range'][rangeType] =  parseInt(event.target.value) ;
      if( newState[type][1]['slot_range'].upper < newState[type][0]['slot_range'].upper ){
        newState[type][1]['slot_range'].upper  = parseInt(event.target.value) + 1;
        newState[type][2]['slot_range'].lower = parseInt(event.target.value) + 1 ;
      }
      newState[type][1]['slot_range'].lower = parseInt(event.target.value);
      newState[type][2]['slot_range'].upper = 101;
      delete  newState[type][1]['slot_range'].empty;
      (newState.timeEdited as boolean) = true;
      this.setState(newState);
    }
  };
  handleChangeSecond = (event: any, type, rangeType, index) => {
    const newState = cloneDeep(this.state);
    if(parseInt(event.target.value) > 0 &&  parseInt(event.target.value) < 99){
      newState[type][index]['slot_range'][rangeType] =  parseInt(event.target.value) ;
      if( newState[type][1]['slot_range'].upper < newState[type][0]['slot_range'].upper ){
        newState[type][1]['slot_range'].upper  =newState[type][1]['slot_range'].upper + 1;
      }
      if( newState[type][1]['slot_range'].upper < newState[type][0]['slot_range'].upper ){
        newState[type][1]['slot_range'].upper  =newState[type][1]['slot_range'].upper + 1;
      }
      newState[type][2]['slot_range'].lower = parseInt(event.target.value) ;
      newState[type][2]['slot_range'].upper = 101;
      (newState.timeEdited as boolean) = true;
      this.setState(newState);
    }
  };
  handleChangeTime = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.dateList[0]['slot_range'].upper =  parseInt(event.target.value) ;
    newState.dateList[1]['slot_range'].lower =  parseInt(event.target.value) ;
    (newState.dateEdited as boolean) = true;
    this.setState(newState);
  };
  render() {
    const timeList = this.state.timeList;

    return (
      <>
        <div className="project-status-indicator-thresholds col-md-12">
          <div className="sub-heading">
            <img
              className="clock-circular"
              alt=""
              src={'/assets/icons/clock-circular-gross-outline-of-time.svg'}
            />
          Time
          <div className="gray-text">[Budget (hrs) - Actual(hrs)] / Budget (hrs)]</div>
          </div>
          <div className="gray-text-bold action-treshold">Threshold (hours remaining)
          
              {
                this.state.timeEdited &&  (
                  <img
                    className="saved col-md-1"
                    alt=""
                    src={'/assets/icons/tick-blue.svg'}
                    onClick={(e) => this.onSaveTimeClick(e)}

                  />)
              }     
               </div>

          <div className="col-md-12 row">
            <div className="row-threshold">
            <div className="color-circle pm-danger" />
              <div className="date-text"> Less than</div>
              <Input
                field={{
                  label: '',
                  type: "NUMBER",
                  value: timeList[0].slot_range.upper,
                  isRequired: false,
                }}
                width={1}
                name="customerContactRoles"
                placeholder=" "
                onChange={e => this.handleChangeFirst(e, 'timeList', 'upper', 0)}
                loading={this.state.loading}
              />
              <div className="symbols">%</div>
            </div>
            <div className="row-threshold">
              <div className="color-circle pm-warning" />
             <div className="date-text"> Less than</div>
              <Input
                field={{
                  label: '',
                  type: "NUMBER",
                  value: timeList[1].slot_range.upper,
                  isRequired: false,
                }}
                width={1}
                name="customerContactRoles"
                placeholder=" "
                onChange={e => this.handleChangeSecond(e, 'timeList', 'upper', 1)}
                loading={this.state.loading}
              />
              <div className="symbols">%</div>
            </div>
            <div className="row-threshold">
              <div className="color-circle pm-success  " />

              <div className="date-text"> Greater than </div>
              <div className="date-text"> {timeList[2].slot_range.lower}</div>
              <div className="symbols">%</div>
            </div>
            <div className="sub-heading">
            Note: 
          <div className="gray-text">The thresholds not include the value of the range.</div>
          </div>
          </div>

        </div>
        <div className="project-status-indicator-thresholds col-md-12">
          <div className="sub-heading">
            <img
              className="clock-circular"
              alt=""
              src={'/assets/icons/calendar-silhouette.svg'}
            />
          Date
          <div className="gray-text"> [Estimated End Date]</div>
          </div>
           <div className="gray-text-bold action-treshold">Estimated Close Date
              {
                this.state.dateEdited &&  (
                  <img
                    className="saved col-md-1"
                    alt=""
                    src={'/assets/icons/tick-blue.svg'}
                    onClick={(e) => this.onSaveDateClick(e)}
                  />)
              }     
               </div>
          <div className="col-md-12 row">
            <div className="row-threshold">
              <div className="color-circle pm-danger " />
              <div className="date-text">Close date in the past</div>
            </div>
            <div className="row-threshold">
              <div className="color-circle pm-warning" />

              <div className="date-text">Less than</div>
              <Input
                field={{
                  label: '',
                  type: "NUMBER",
                  value: this.state.dateList[0].slot_range.upper,
                  isRequired: false,
                }}
                width={1}
                name="customerContactRoles"
                placeholder=" "
                onChange={e => this.handleChangeTime(e)}
                loading={this.state.loading}
              />
              <div className="gray-date-text">Days</div>
            </div>
            <div className="row-threshold">
              <div className="color-circle pm-success" />
              <div className="date-text">Greater than</div>
              <Input
                field={{
                  label: '',
                  type: "NUMBER",
                  value: this.state.dateList[1].slot_range.lower,
                  isRequired: false,
                }}
                width={1}
                name="customerContactRoles"
                placeholder=" "
                onChange={e => null}
                loading={this.state.loading}
                disabled={true}
              />
              <div className="gray-date-text">Days</div>
            </div>          
          </div>
        </div>
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  purchaseOrderSetting: state.setting.purchaseOrderSetting,
});

const mapDispatchToProps = (dispatch: any) => ({
  postDateStatusList: (data: any) => dispatch(postDateStatusList(data)),
  postTimeStatusList: (data: any) => dispatch(postTimeStatusList(data)),
  getTimeStatusList: () => dispatch(getTimeStatusList()),
  getDateStatusList: () => dispatch(getDateStatusList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ThresholdsIndicator);

