import React, { Component } from "react";
import _ from "lodash";
import "quill-mention";
import { cloneDeep } from "lodash";
import ReactQuill from "react-quill"; // ES6
import { Mention, MentionsInput } from "react-mentions";
import SquareButton from "../../components/Button/button";
import { commonFunctions } from "../../utils/commonFunctions";
import "./style.scss";
import Checkbox from "../../components/Checkbox/checkbox";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";

interface IMeetingEmailTemplateState {
  savedSuccess: boolean;
  showActionButtons: boolean;
  meetingTemplate: IMeetingEmailTemplate;
}

interface IMeetingEmailTemplateProps {
  meetingType: MeetingType;
  mailTemplateData: IMeetingEmailTemplate;
  setData: (data: IMeetingEmailTemplate) => void;
}

class MeetingEmailTemplate extends Component<
  IMeetingEmailTemplateProps,
  IMeetingEmailTemplateState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  customerMailBodyRef: React.LegacyRef<ReactQuill>;

  constructor(props: IMeetingEmailTemplateProps) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    meetingTemplate: {
      email_subject: "",
      email_body_text: "",
      email_body_markdown: "",
      project_update: "",
      add_internal_contacts: false,
      add_customer_contacts: false,
      add_additional_contacts: false,
    },
    showActionButtons: false,
    savedSuccess: false,
  });

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);
    },
  };

  componentDidMount() {
    this.setState({
      meetingTemplate: cloneDeep(this.props.mailTemplateData),
      savedSuccess: this.props.mailTemplateData.email_body_text !== "",
    });
  }

  componentDidUpdate(prevProps: IMeetingEmailTemplateProps) {
    if (
      this.props.mailTemplateData &&
      this.props.mailTemplateData !== prevProps.mailTemplateData
    ) {
      this.setState({
        meetingTemplate: cloneDeep(this.props.mailTemplateData),
        savedSuccess: this.props.mailTemplateData.email_body_text !== "",
      });
    }
  }

  handleChangeMailSubject = (e: React.ChangeEvent<HTMLInputElement>) => {
    const meetingTemplate = cloneDeep(this.state.meetingTemplate);
    (meetingTemplate.email_subject as string) = e.target.value;
    this.setState({ meetingTemplate, showActionButtons: true });
  };

  onEmailControlChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const meetingTemplate = cloneDeep(this.state.meetingTemplate);
    meetingTemplate[e.target.name] = e.target.checked;
    this.setState({ meetingTemplate, showActionButtons: true });
  };

  createMeetingTemplate = () => {
    const { savedSuccess, meetingTemplate } = this.state;

    const regex = /#RULE#/gi;
    const mailSubject = meetingTemplate.email_subject.replace(regex, "");
    const mailBodyTxtArr = meetingTemplate.email_body_text.split(" ");

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith("{")) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    });

    this.props.setData({
      ...meetingTemplate,
      meeting_type: this.props.meetingType,
      email_subject: mailSubject,
      email_body_text: mailBodyTxtFinalArr.join(" "),
      isCreated: savedSuccess,
    });
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangeQuill = (
    content: string,
    editor,
    key: "project_update" | "email_body_markdown"
  ) => {
    const object = editor.getContents(content);
    const newState = cloneDeep(this.state);
    if (key === "email_body_markdown")
      (newState.meetingTemplate
        .email_body_text as string) = this.getContentFromObject(object);
    (newState.meetingTemplate[key] as string) = content;
    (newState.showActionButtons as boolean) = true;
    this.setState(newState);
  };

  render() {
    return (
      <div className="meeting-email-template-container col-md-12">
        <div className="email-template-inputs">
          {this.props.meetingType === "Customer Touch" && (
            <h3>Project Acceptance</h3>
          )}
          <div className="col-md-12 row customer-email-field">
            <label className="col-md-3 field__label-label email-subject">
              <b>Email Subject</b> (The subject that will be populated by
              default in the {this.props.meetingType} Meeting)
            </label>
            <div className="mail-subject-mention-input">
              <MentionsInput
                markup="[__display__]"
                value={this.state.meetingTemplate.email_subject || ""}
                onChange={(e) => this.handleChangeMailSubject(e)}
                className={"outer"}
              >
                <Mention
                  trigger="#"
                  data={commonFunctions.rangeVariables()}
                  className={"inner-drop"}
                  markup="#RULE#__display__"
                />
              </MentionsInput>
            </div>
          </div>
          <div className="col-md-12 row customer-email-field">
            <label className="col-md-3 field__label-label email-subject">
              <span>
                <b>Email Message</b> (The default message that will be shown in
                the email sent for {this.props.meetingType} meeting)
              </span>
            </label>
            <div className="email-editor-wrapper">
              <ReactQuill
                className="email-body-editor"
                ref={this.customerMailBodyRef}
                value={this.state.meetingTemplate.email_body_markdown || ""}
                onChange={(content, delta, source, editor) =>
                  this.handleChangeQuill(content, editor, "email_body_markdown")
                }
                modules={{ mention: this.mentionModule }}
              />
            </div>
          </div>
          <div className="col-md-12 row customer-email-field">
            <label className="col-md-3 field__label-label email-subject">
              <span>
                <b>Project Updates</b> (The default project updates for the
                meeting preview PDF)
              </span>
            </label>
            <div className="email-editor-wrapper">
              <QuillEditorAcela
                customMentionModule={this.mentionModule}
                scrollingContainer=".app-body"
                onChange={(content, source, editor) =>
                  this.handleChangeQuill(content, editor, "project_update")
                }
                value={this.state.meetingTemplate.project_update}
                wrapperClass="email-template-project-updates"
              />
            </div>
          </div>
          {this.state.showActionButtons &&
            this.state.meetingTemplate.email_body_markdown !== "" &&
            this.state.meetingTemplate.email_subject !== "" && (
              <SquareButton
                onClick={(e) => this.createMeetingTemplate()}
                content="Save"
                bsStyle={"primary"}
                className="save-mail-btn"
              />
            )}
        </div>
        {this.props.meetingType !== "Internal Kickoff" && (
          <div className="email-template-controls">
            <div className="et-checkbox-container">
              <h4>Email Controls</h4>
              <Checkbox
                isChecked={this.state.meetingTemplate.add_internal_contacts}
                name="add_internal_contacts"
                onChange={this.onEmailControlChange}
                title={
                  "Iternal Contacts (LookingPoint members) will be included in recipients"
                }
              >
                Include Internal Contacts
              </Checkbox>
              <Checkbox
                isChecked={this.state.meetingTemplate.add_customer_contacts}
                name="add_customer_contacts"
                onChange={this.onEmailControlChange}
                title={"Customer Contacts will be included in recipients"}
              >
                Include the Customer
              </Checkbox>
              <Checkbox
                isChecked={this.state.meetingTemplate.add_additional_contacts}
                name="add_additional_contacts"
                onChange={this.onEmailControlChange}
                title={"Vendor Resources will be included in recipients"}
              >
                Include Vendor Resources
              </Checkbox>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default MeetingEmailTemplate;
