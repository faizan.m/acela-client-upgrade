import React from 'react';
import { connect } from 'react-redux';
// import TagsInput from 'react-tagsinput'; (New Component)
import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import { GetWorkTypes, timeSheetSettings } from '../../actions/pmo';
import { fetchTimezones } from '../../actions/provider';
import { getQuoteBusinessList } from '../../actions/sow';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import TimePicker from 'react-time-picker';

import './style.scss';



interface IEngineerTimesheetProps extends ICommonProps {
  addErrorMessage: any;
  addSuccessMessage: any;
  GetWorkTypes: any;
  getQuoteBusinessList: any;
  fetchTimezones: any;
}

interface IEngineerTimesheetState {
  loading: boolean;
  WorkTypeList: any[];
  default_work_type_id: number;
  time_period_status: string[];
  default_hours: number;
  businessList: any;
  default_business_unit_id: any;
  daily_hours_threshold: number;
  default_start_time: number;
  default_timezone: string
  weekly_hours_threshold: number;
  timezones: any[];
}

class EngineerTimesheetsSetting extends React.Component<
  any,
  IEngineerTimesheetState
> {
  constructor(props: IEngineerTimesheetProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    loading: false,
    WorkTypeList: [],
    default_work_type_id: 0,
    time_period_status: [],
    default_hours: 0,
    businessList: [],
    default_business_unit_id: '',
    daily_hours_threshold: 0,
    default_start_time: 0,
    default_timezone: "America/Los_Angeles",
    weekly_hours_threshold: 0,
    timezones: [],
  });

  componentDidMount() {
    this.getTimesheetSetting();
    this.props.GetWorkTypes().then(
      action => {
        this.setState({ WorkTypeList: action.response })
      }
    )
    this.props.getQuoteBusinessList().then(
      a => {
        this.setState({ loading: false, businessList: a.response })
      }
    )
    this.props.fetchTimezones().then(
      action => {
        this.setState({ timezones: action.response })
      }
    )
  }
  getTimesheetSetting = () => {
    this.setState({ loading: true })
    this.props.timeSheetSettings('', 'get').then(
      action => {
        if (action.type === 'TIMESHEET_SETTING_SUCCESS') {
          this.setState({ ...action.response })
        }
      }
    )
  }

  saveTimesheetSetting = () => {
    if (this.state.default_work_type_id && this.state.time_period_status && this.state.default_hours && this.state.default_business_unit_id) {
      this.setState({ loading: true })
      this.props.timeSheetSettings(this.state, 'put').then(
        action => {
          if (action.type === 'TIMESHEET_SETTING_SUCCESS') {
            this.setState({
              default_work_type_id: action.response.default_work_type_id,
              time_period_status: action.response.time_period_status,
              default_hours: action.response.default_hours, loading: false
            })
          }
        }
      )
    }
  }
  render() {
    return (
      <div className="engineer-timesheet">
        <div className="loader modal-loader">
          <Spinner show={this.state.loading} />
        </div>
        <div>

          <Input
            field={{
              label: "Default Hours",
              type: "PICKLIST",
              value: this.state.default_hours,
              isRequired: false,
              options: [`1`, '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24'],
            }}
            width={4}
            labelIcon={''}
            name="hours"
            onChange={(e) => this.setState({ default_hours: e.target.value }, () => {
              this.saveTimesheetSetting()
            })}
            placeholder={`Hours`}
            disabled={false}
          />
          <Input
            field={{
              label: "Default Work Type",
              type: "PICKLIST",
              value: this.state.default_work_type_id,
              isRequired: false,
              options: this.state.WorkTypeList && this.state.WorkTypeList.map(project => ({
                value: project.id,
                label: `${project.name}`,
              })),
            }}
            width={4}
            labelIcon={''}
            name="workType"
            onChange={(e) => this.setState({ default_work_type_id: e.target.value }, () => {
              this.saveTimesheetSetting()
            })}
            placeholder={`Type`}
            disabled={false}
          />
          <Input
            field={{
              label: "Time Period Status",
              type: "CUSTOM",
              value: '',
              isRequired: false,
            }}
            width={4}
            labelIcon={''}
            name="time_period_status"
            onChange={(e) => null}
            customInput={
            // <TagsInput
            //   value={this.state.time_period_status}
            //   onChange={e => {
            //     this.setState({ time_period_status: e }, () => {
            //       this.saveTimesheetSetting()
            //     })
            //   }
            //   }
            //   inputProps={{
            //     className: 'react-tagsinput-input',
            //     placeholder: 'Enter Status',
            //   }}
            //   addOnBlur={true}
            // />
            null
          }
            placeholder={`Status`}
            disabled={false}
          />
          <Input
            field={{
              label: 'Default Business Unit',
              type: "PICKLIST",
              value: this.state.default_business_unit_id,
              options: this.state.businessList && this.state.businessList.map(s => ({
                value: s.id,
                label: s.label,
              })),
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="default_business_unit"
            onChange={(e) => this.setState({ default_business_unit_id: e.target.value }, () => {
              this.saveTimesheetSetting()
            })} placeholder={`Select`}
          />
          <Input
            field={{
              label: 'Timezone',
              type: "PICKLIST",
              isRequired: false,
              value: this.state.default_timezone,
              options: this.state.timezones.map(t => ({
                value: t.timezone,
                label: t.display_name,
                disabled: false,
              }))
            }}
            width={4}
            placeholder="Select timezone"
            name="timezone"
            onChange={e => this.setState({ default_timezone: e.target.value })}
            onBlur={() => { this.saveTimesheetSetting() }}
          />
          <Input
            field={{
              label: 'Daily Hours',
              type: "NUMBER",
              value: this.state.daily_hours_threshold,
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="daily_hours_threshold"
            onChange={(e) => this.setState({ daily_hours_threshold: e.target.value })}
            onBlur={() => { this.saveTimesheetSetting() }}
            placeholder={`Enter`}
          />
          <Input
            field={{
              label: 'Weekly Hours',
              type: "NUMBER",
              value: this.state.weekly_hours_threshold,
              isRequired: true,
            }}
            width={4}
            multi={false}
            name="weekly_hours_threshold"
            onChange={(e) => this.setState({ weekly_hours_threshold: e.target.value })}
            onBlur={() => { this.saveTimesheetSetting() }}
            placeholder={`Enter`}
          />
          <Input
            field={{
              label: 'Default Start Time',
              type: "CUSTOM",
              value: this.state.default_start_time,
              isRequired: false,
            }}
            width={2}
            labelIcon={''}
            name="start_time"
            customInput={
              <TimePicker
              value={this.state.default_start_time}
              onChange={(e) => this.setState({ default_start_time: e })}
                onBlur={() => { this.saveTimesheetSetting() }}
                clockIcon={null}
                clearIcon={null}
                format={"h:m a"}
              />
            }
            onChange={(e) => null}
            placeholder={'Start Time'}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  GetWorkTypes: () => dispatch(GetWorkTypes()),
  timeSheetSettings: (data: any, method: any) => dispatch(timeSheetSettings(data, method)),
  getQuoteBusinessList: () => dispatch(getQuoteBusinessList()),
  fetchTimezones: () => dispatch(fetchTimezones()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EngineerTimesheetsSetting);
