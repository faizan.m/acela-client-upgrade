import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { getQuoteStatusesList } from "../../actions/sow";
import Input from "../../components/Input/input";
import {
  cancelledCRSettingCRU,
  CANCELLED_CR_SETTING_SUCCESS,
  CANCELLED_CR_SETTING_FAILURE,
} from "../../actions/pmo";
import {
  getQuoteAllStages,
  GET_QUOTE_STAGES_SUCCESS,
} from "../../actions/setting";
import { commonFunctions } from "../../utils/commonFunctions";

interface CancelledCRProps {
  isFetchingQStatuses: boolean;
  qStatusList: IDLabelObject[];
  getQuoteStageList: () => Promise<any>;
  getQuoteStatusesList: () => Promise<any>;
  cancelledCRSettingCRU: (
    method: HTTPMethods,
    data?: ICancelledCRSetting
  ) => Promise<any>;
}

const CancelledCR: React.FC<CancelledCRProps> = (props) => {
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [fetchingStages, setFetchingStages] = useState<boolean>(false);
  const [quoteStages, setQuoteStages] = useState<IPickListOptions[]>([]);
  const [settings, setSettings] = useState<ICancelledCRSetting>({
    opp_stage_crm_id: null,
    opp_status_crm_id: null,
  });
  const [error, setError] = useState<IErrorValidation>({
    opp_stage_crm_id: commonFunctions.getErrorState(),
    opp_status_crm_id: commonFunctions.getErrorState(),
  });

  const quoteStatuses = useMemo(
    () =>
      props.qStatusList
        ? props.qStatusList.map((el) => ({
            value: el.id,
            label: el.label,
          }))
        : [],
    [props.qStatusList]
  );

  useEffect(() => {
    fetchCancelledCRSetting();
    fetchQuoteStages();
    if (!props.qStatusList) props.getQuoteStatusesList();
  }, []);

  const fetchQuoteStages = () => {
    setFetchingStages(true);
    props
      .getQuoteStageList()
      .then((action) => {
        if (action.type === GET_QUOTE_STAGES_SUCCESS) {
          setQuoteStages(
            action.response.map((stage: IDLabelObject) => ({
              value: stage.id,
              label: stage.label,
            }))
          );
        }
      })
      .finally(() => setFetchingStages(false));
  };

  const fetchCancelledCRSetting = () => {
    props.cancelledCRSettingCRU("get").then((action) => {
      if (action.type === CANCELLED_CR_SETTING_SUCCESS) {
        setSettings(action.response);
      } else if (action.type === CANCELLED_CR_SETTING_FAILURE) {
        setFirstSave(true);
      }
    });
  };

  const handleChangeSetting = (e: React.ChangeEvent<HTMLInputElement>) => {
    const updatedSettings: ICancelledCRSetting = {
      ...settings,
      [e.target.name]: e.target.value,
    };
    setSettings(updatedSettings);
    saveSettings(updatedSettings);
  };

  const validate = (settings: ICancelledCRSetting): boolean => {
    let isValid = true;
    const errors: IErrorValidation = {};
    const keys = ["opp_status_crm_id", "opp_stage_crm_id"];
    keys.forEach((key) => {
      if (!settings[key]) {
        isValid = false;
        errors[key] = commonFunctions.getErrorState("This field is required");
      } else errors[key] = commonFunctions.getErrorState();
    });
    setError(errors);
    return isValid;
  };

  const saveSettings = (settings: ICancelledCRSetting) => {
    if (validate(settings)) {
      props
        .cancelledCRSettingCRU(
          firstSave ? "post" : "put",
          settings
        )
        .then((action) => {
          if (action.type === CANCELLED_CR_SETTING_SUCCESS) {
            setFirstSave(false);
          }
        });
    }
  };

  return (
    <div className="cancelled-cr-setting-container">
      <div className="col-md-12">
        <Input
          field={{
            label: "Cancelled Opportunity Status",
            type: "PICKLIST",
            value: settings.opp_status_crm_id,
            options: quoteStatuses,
            isRequired: true,
          }}
          className="cr-select"
          width={6}
          multi={false}
          placeholder={`Select Opportunity Status`}
          name="opp_status_crm_id"
          onChange={handleChangeSetting}
          labelIcon="info"
          labelTitle={
            "Opportunities linked to the Cancelled Change Request will be updated with this Status"
          }
          loading={props.isFetchingQStatuses}
          error={error.opp_status_crm_id}
        />
        <Input
          field={{
            label: "Cancelled Opportunity Stage",
            type: "PICKLIST",
            value: settings.opp_stage_crm_id,
            options: quoteStages,
            isRequired: true,
          }}
          className="cr-select"
          width={6}
          multi={false}
          placeholder={`Select Opportunity Stage`}
          name="opp_stage_crm_id"
          onChange={handleChangeSetting}
          labelIcon="info"
          labelTitle={
            "Opportunities linked to the Cancelled Change Request will be updated with this Stage"
          }
          loading={fetchingStages}
          error={error.opp_stage_crm_id}
        />
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  qStatusList: state.sow.qStatusList,
  isFetchingQStatuses: state.sow.isFetchingQStatusList,
});

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStageList: () => dispatch(getQuoteAllStages()),
  getQuoteStatusesList: () => dispatch(getQuoteStatusesList()),
  cancelledCRSettingCRU: (method: HTTPMethods, data?: ICancelledCRSetting) =>
    dispatch(cancelledCRSettingCRU(method, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CancelledCR);
