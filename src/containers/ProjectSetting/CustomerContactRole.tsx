import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.scss';
import Input from '../../components/Input/input';
import { createContactRole, deleteContactRole, getContactRoles, NEW_PROJECT_BOARD_SUCCESS } from '../../actions/pmo';
import cloneDeep from 'lodash/cloneDeep';
import SmallConfirmationBox from '../../components/SmallConfirmationBox/confirmation';

interface IICustomerContactRoleState {
  customerContactRoles: string;
  customerContactRolesList: any[];
}

interface IIProjectSettingProps extends ICommonProps {
  getContactRoles: any;
  createContactRole: any;
  deleteContactRole: any;
}

class CustomerContactRole extends Component<
  IIProjectSettingProps,
  IICustomerContactRoleState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    customerContactRoles: '',
    customerContactRolesList: [],
  });

  componentDidMount() {
    this.getContactRoles();
  }

  getContactRoles = async () => {
    const mapping: any = await this.props.getContactRoles();
    this.setState({ customerContactRolesList: mapping.response });
  }


  onNewOptionClick = (e) => {
    const name = this.state.customerContactRoles;
    const newState = cloneDeep(this.state);
    const allTitles = newState.customerContactRolesList && newState.customerContactRolesList.map(p => p.role);
    if (!allTitles.includes(name)) {
      this.props.createContactRole({ role: name }).then(
        action => {
          if (action.type === NEW_PROJECT_BOARD_SUCCESS) {
            newState.customerContactRolesList.push({ role: name, id: action.response.id });
          }
          (newState.customerContactRoles as string) = '';
          this.setState(newState);
        }
      )
    }
  };

  deleteContactRole = (data, index) => {
    const newState = cloneDeep(this.state);
    this.props.deleteContactRole({ id: data.id }).then(
      action => {
        if (action.type === NEW_PROJECT_BOARD_SUCCESS) {
          newState.customerContactRolesList.splice(index, 1);
        }
        this.setState(newState);
      }
    )
  };

  render() {

    return (
      <div className="customer-contact-role col-md-12">
        <div className="col-md-12 row">
          <p className="label-text">The added roles will be used to select roles for customer contacts in project details</p>
          <div className="roles-all">
            <div className="customer-contact">
              {this.state.customerContactRolesList.map((contact, i) => {
                return (
                  <div className="contact" key={i}>{contact.role}
                    <SmallConfirmationBox
                      className='remove'
                      onClickOk={() => this.deleteContactRole(contact, i)}
                      text={'Contact Role'}
                    /></div>
                )
              })}
            </div>

            {
              this.state.customerContactRoles !== '' &&
              <>
                <Input
                  field={{
                    label: '',
                    type: "TEXT",
                    value: this.state.customerContactRoles && this.state.customerContactRoles,
                    isRequired: false,
                  }}
                  width={4}
                  multi={true}
                  name="customerContactRoles"
                  onChange={e => this.setState({
                    customerContactRoles: e.target.value
                  })}
                  placeholder={`Enter Role `}
                />
                {
                  this.state.customerContactRoles && this.state.customerContactRoles.trim() && (
                    <img
                      className="saved col-md-1"
                      alt=""
                      src={'/assets/icons/tick-blue.svg'}
                      onClick={(e) => this.onNewOptionClick(e)}

                    />)
                }
                        {
                  this.state.customerContactRoles && this.state.customerContactRoles.trim() && (
                    <img
                      className="icon-remove-role col-md-1"
                      alt=""
                      src={'/assets/icons/cross-sign.svg'}
                      onClick={(e) =>{
                        this.setState({customerContactRoles:''})
                      }}

                    />)
                }
              </>
            }

          </div>


        </div>
        { this.state.customerContactRoles === '' &&
          <div
            onClick={e => this.setState({ customerContactRoles: ' ' })}
            className="add-new-status"
          >Add Role</div>
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getContactRoles: () => dispatch(getContactRoles()),
  createContactRole: (data: any) => dispatch(createContactRole(data)),
  deleteContactRole: (data: any) => dispatch(deleteContactRole(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerContactRole);
