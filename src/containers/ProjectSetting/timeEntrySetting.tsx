import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import {
  timeEntrySetting,
  TIME_ENTRY_SETTING_FAILURE,
  TIME_ENTRY_SETTING_SUCCESS,
} from "../../actions/pmo";
import { addSuccessMessage } from "../../actions/appState";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import { commonFunctions } from "../../utils/commonFunctions";

interface TimeEntrySettingProps {
  addSuccessMessage: TShowSuccessMessage;
  timeEntrySetting: (
    method: HTTPMethods,
    data?: TimeEntrySetting
  ) => Promise<any>;
}

const TimeEntrySetting: React.FC<TimeEntrySettingProps> = (props) => {
  const [firstSave, setFirstSave] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [settings, setSettings] = useState<TimeEntrySetting>({
    default_engineer_note: "",
    default_pm_note: "",
  });
  const [error, setError] = useState<IErrorValidation>({});

  useEffect(() => {
    fetchSettings();
  }, []);

  const fetchSettings = () => {
    setLoading(true);
    props
      .timeEntrySetting("get")
      .then((action) => {
        if (action.type === TIME_ENTRY_SETTING_SUCCESS) {
          setSettings(action.response);
        } else if (action.type === TIME_ENTRY_SETTING_FAILURE) {
          setFirstSave(true);
        }
      })
      .finally(() => setLoading(false));
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSettings((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const isValid = () => {
    let isValid = true;
    const err = cloneDeep(error);
    const requiredFields = ["default_engineer_note", "default_pm_note"];
    requiredFields.forEach((field) => {
      if (!settings[field].trim()) {
        isValid = false;
        err[field] = commonFunctions.getErrorState("This field is required");
      } else err[field] = commonFunctions.getErrorState();
    });
    setError(err);
    return isValid;
  };

  const onSave = () => {
    if (isValid()) {
      setLoading(true);
      props
        .timeEntrySetting(
          firstSave ? "post" : "put",
          settings
        )
        .then((action) => {
          if (action.type === TIME_ENTRY_SETTING_SUCCESS) {
            props.addSuccessMessage(
              `Time Entry Setting ${
                firstSave ? "saved" : "updated"
              } successfully!`
            );
            setFirstSave(false);
          }
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <div className="time-entry-setting-container">
      <div className="time-entry-setting-row col-md-12">
        <Input
          field={{
            label: "Default PM Note",
            type: "TEXT",
            value: settings.default_pm_note,
            isRequired: true,
          }}
          width={6}
          labelIcon="info"
          labelTitle={
            "This text will be used to populate the PM Time Entry Note by default"
          }
          placeholder="Enter Note"
          error={error.default_pm_note}
          name="default_pm_note"
          onChange={handleChange}
        />
        <Input
          field={{
            label: "Default Engineer Note",
            type: "TEXT",
            value: settings.default_engineer_note,
            isRequired: true,
          }}
          width={6}
          labelIcon="info"
          labelTitle={
            "This text will be used to populate the Engineer Time Entry Note by default"
          }
          placeholder="Enter Note"
          error={error.default_engineer_note}
          name="default_engineer_note"
          onChange={handleChange}
        />
      </div>
      <SquareButton
        onClick={onSave}
        content={firstSave ? "Save" : "Update"}
        bsStyle={"primary"}
        disabled={loading}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  timeEntrySetting: (method: HTTPMethods, data?: TimeEntrySetting) =>
    dispatch(timeEntrySetting(method, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TimeEntrySetting);
