import { debounce } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import {
  ACTIVATE_USER_EMAIL_FAILURE,
  ACTIVATE_USER_EMAIL_SUCCESS,
  sendActivationEmail,
} from '../../actions/email';
import {
  createSuperUser,
  editSuperUser,
  fetchSuperUsers,
  SUPERUSER_CREATE_FAILURE,
  SUPERUSER_CREATE_SUCCESS,
  SUPERUSER_EDIT_FAILURE,
  SUPERUSER_EDIT_SUCCESS,
} from '../../actions/superuser';
import { fetchUserTypes } from '../../actions/userType';
import SquareButton from '../../components/Button/button';
import EditButton from '../../components/Button/editButton';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import { allowPermission } from '../../utils/permissions';
import UserDetails from '../User/userDetails';
import UserForm from '../User/userForm';

import './style.scss';

interface ISuperUserTableRow {
  id: string;
  name: string;
  email: string;
  role: string;
  status: string;
}

interface ISuperUserListingProps extends ICommonProps {
  formError: string;

  userTypes: IUserTypeRole[];
  superusers: ISuperUserPaginated;
  fetchUserTypes: TFetchUserTypes;
  fetchSuperUsers: TFetchSuperUsers;
  createSuperUser: any; // TODO;
  editSuperUser: any; // TODO;
  isFetchingUsers: boolean;
  sendActivationEmail: TPostEmailActivation;
}

interface ISuperUserListingState {
  isUserModalOpen: boolean;
  isDetailsModalOpen: boolean;
  rows: ISuperUserTableRow[];
  selectedRows: number[];
  user: ISuperUser;
  errorList?: any;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  superusers: ISuperUser[];
  isPostingUser: boolean;
  reset: boolean;
  emailActivatingIndex: number;
}

class SuperUserListing extends React.Component<
  ISuperUserListingProps,
  ISuperUserListingState
  > {
  private debouncedFetch;
  constructor(props: ISuperUserListingProps) {
    super(props);

    this.state = {
      isDetailsModalOpen: false,
      isUserModalOpen: false,
      rows: [],
      selectedRows: [],
      user: null,
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      superusers: [],
      isPostingUser: false,
      emailActivatingIndex: null,
      reset: false,
    };
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }

  componentDidMount() {
    this.props.fetchUserTypes();
  }

  componentDidUpdate(prevProps: ISuperUserListingProps) {
    if (prevProps.superusers !== this.props.superusers) {
      this.setRows(this.props);
    }
  }

  setRows = (nextProps: ISuperUserListingProps) => {
    const superUsersResponse: ISuperUserPaginated = nextProps.superusers;
    const superusers: ISuperUser[] = superUsersResponse.results;
    const rows: ISuperUserTableRow[] = superusers.map((superuser, index) => ({
      id: superuser.id,
      name: `${superuser.first_name} ${superuser.last_name}`,
      email: superuser.email,
      role: superuser.role_display_name,
      can_edit: superuser.can_edit,
      status: superuser.is_active ? 'Enabled' : 'Disabled',
      index,
      action: this.getActivationButton(superuser),
    }));

    this.setState(prevState => ({
      reset: false,
      rows,
      superusers,
      pagination: {
        ...prevState.pagination,
        totalRows: superUsersResponse.count,
        currentPage: superUsersResponse.links.page_number - 1,
        totalPages: Math.ceil(
          superUsersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  getSuperUserRoles = () => {
    const userTypes = this.props.userTypes;
    if (userTypes) {
      const superUserType = userTypes.find(
        userType => userType.user_type === 'accelavar'
      );

      return superUserType.roles.map(role => ({
        value: role.id,
        label: role.display_name,
      }));
    }

    return [];
  };

  onEditRowClick = (superUserIndex: number, event: any) => {
    event.stopPropagation();

    this.setState({
      isUserModalOpen: true,
      user: this.state.superusers[superUserIndex],
    });
  };

  onRowClick = rowInfo => {
    const superUserIndex = rowInfo.original.index;

    this.setState({
      isDetailsModalOpen: true,
      user: this.state.superusers[superUserIndex],
    });
  };

  openCreateSuperUserModal = e => {
    this.setState({
      isUserModalOpen: true,
      user: null,
    });
  };

  onCreateSuperUser = (superUser: ISuperUser) => {
    this.setState({ isPostingUser: true });
    this.props.createSuperUser(superUser).then(action => {
      if (action.type === SUPERUSER_CREATE_SUCCESS) {
        this.fetchSuperUsers();
        this.setState({
          isUserModalOpen: false,
          isPostingUser: false,
        });
      }
      if (action.type === SUPERUSER_CREATE_FAILURE) {
        this.setState({
          isUserModalOpen: true,
          errorList: action.errorList.data
            ? action.errorList.data
            : this.state.errorList,
          isPostingUser: false,
        });
      }
    });
  };

  onEditSuperUserSubmit = (superUser: ISuperUser) => {
    this.setState({ isPostingUser: true });
    this.props.editSuperUser(superUser.id, superUser).then(action => {
      if (action.type === SUPERUSER_EDIT_SUCCESS) {
        this.fetchSuperUsers(this.state.pagination.currentPage);
        this.setState({
          isUserModalOpen: false,
          isPostingUser: false,
        });
      }
      if (action.type === SUPERUSER_EDIT_FAILURE) {
        this.setState({
          isUserModalOpen: true,
          errorList: action.errorList.data,
          isPostingUser: false,
        });
      }
    });
  };

  toggleCreateEditModal = () => {
    this.setState(prevState => ({
      isUserModalOpen: !prevState.isUserModalOpen,
    }));
  };

  toggleDetailsModal = () => {
    this.setState(prevState => ({
      isDetailsModalOpen: !prevState.isDetailsModalOpen,
    }));
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchSuperUsers(newParams);
  };

  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  fetchSuperUsers = (num?: number) => {
    const search = this.state.pagination.params.search;
    const page = num ? num + 1 : 1;
    if (search && search.length > 0) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
            page,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;
      newParams.page = page;

      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page,
    });
  };

  onclickSendMail = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState({ emailActivatingIndex: userIndex });
    this.props
      .sendActivationEmail(this.state.superusers[userIndex].id)
      .then(action => {
        if (action.type === ACTIVATE_USER_EMAIL_SUCCESS) {
          this.setState({ emailActivatingIndex: null });
        }
        if (action.type === ACTIVATE_USER_EMAIL_FAILURE) {
          this.setState({ emailActivatingIndex: null });
        }
        this.fetchSuperUsers();
      });
  };

  getActivationButton = user => {
    let actionType = 'showActive';
    if (
      user.is_active === false &&
      user.is_password_set === false &&
      (user.activation_mails_sent_count > 0 &&
        user.activation_mails_sent_count < 6)
    ) {
      actionType = 'showResend';
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 5
    ) {
      actionType = 'showResendStop';
    }

    return actionType;
  };

  // render methods
  renderTopBar = () => {
    return (
      <div>
        <h3>Accelvar Users</h3>
        <div className="superusers-listing__actions">
          {allowPermission('search_acelavar_user') && (
            <Input
              field={{
                value: this.state.pagination.params.search,
                label: '',
                type: "SEARCH",
              }}
              width={6}
              name="searchString"
              onChange={this.onSearchStringChange}
              placeholder="Search"
              className="superusers-listing__actions-search"
            />
          )}
          {allowPermission('add_acelavar_user') && (
            <SquareButton
              onClick={this.openCreateSuperUserModal}
              content="+ Add AcelaVAR user"
              bsStyle={"primary"}
              className="superusers-listing__actions-create"
            />
          )}
        </div>
      </div>
    );
  };
  render() {
    // ids are for sorting and filtering.
    const columns: any = [
      {
        accessor: 'name',
        Header: 'Name',
        id: 'first_name',
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      { accessor: 'email', Header: 'Email', id: 'email' },
      { accessor: 'role', Header: 'Role', id: 'role' },
      {
        accessor: 'status',
        Header: 'Status',
        width: 150,
        id: 'is_active',
        Cell: status => (
          <div className={`status status--${status.value}`}>{status.value}</div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Edit',
        width: 150,
        sortable: false,
        Cell: cell =>
          cell.original.can_edit && (
            <EditButton onClick={e => this.onEditRowClick(cell.value, e)} />
          ),
      },
      {
        accessor: 'action',
        Header: 'User Activation',
        Cell: action => (
          <div>
            {action.value === 'showResend' &&
              action.index !== this.state.emailActivatingIndex && (
                <SquareButton
                  content="Resend e-Mail"
                  onClick={e => this.onclickSendMail(action.index, e)}
                  bsStyle={"primary"}
                  className="activate-btn"
                />
              )}
            {action.value === 'showResend' &&
              action.index === this.state.emailActivatingIndex && (
                <Spinner show={true} className="email__spinner" />
              )}
            {action.value === 'showResendStop' && (
              <div>No more Emails can sent</div>
            )}
            {action.value === 'showActive' && (
              'Activated'
            )}
          </div>
        ),
      },
    ];
    const isEdit = this.state.user ? true : false;
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };

    return (
      <div className="superusers-listing">
        <UserForm
          show={this.state.isUserModalOpen}
          onClose={this.toggleCreateEditModal}
          onSubmit={
            isEdit ? this.onEditSuperUserSubmit : this.onCreateSuperUser
          }
          user={this.state.user}
          userRoles={this.getSuperUserRoles()}
          userType="Acelavar"
          errorList={this.state.errorList}
          isLoading={this.state.isPostingUser}
        />
        <UserDetails
          show={this.state.isDetailsModalOpen}
          user={this.state.user}
          onClose={this.toggleDetailsModal}
          title="Acelavar User Details"
        />
        <div className="loader">
          <Spinner show={this.props.isFetchingUsers} />
        </div>
        <Table
          columns={columns}
          onRowClick={this.onRowClick}
          rows={this.state.rows}
          customTopBar={this.renderTopBar()}
          className={`superusers-listing__table ${
            this.props.isFetchingUsers ? `loading` : ``
            }`}
          manualProps={manualProps}
          loading={this.props.isFetchingUsers}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  superusers: state.superuser.superusers,
  userTypes: state.userType.userTypes,
  isFetchingUsers: state.superuser.isFetchingUsers,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSuperUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchSuperUsers(params)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
  createSuperUser: (superUser: ISuperUser) =>
    dispatch(createSuperUser(superUser)),
  editSuperUser: (superUserId: number, superUser: ISuperUser) =>
    dispatch(editSuperUser(superUserId, superUser)),
  sendActivationEmail: (userId: string) =>
    dispatch(sendActivationEmail(userId)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SuperUserListing);
