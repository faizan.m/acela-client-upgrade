import React from 'react';
import { Link } from 'react-router-dom';
import './style.scss';

const NotFound = () => (
  <div className="not-found">
    <h1>404 - Not Found!</h1>
    <p>The page you are looking for does not exist or is removed.</p>
    <Link to="/">
      Go Home
    </Link>
  </div>
);

export default NotFound;