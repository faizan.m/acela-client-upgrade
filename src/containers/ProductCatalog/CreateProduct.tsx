import React, { useEffect, useState } from "react";
import { cloneDeep, isObject } from "lodash";
import { connect } from "react-redux";
import {
  productCatalogCRUD,
  PRODUCT_CATALOG_FAILURE,
  PRODUCT_CATALOG_SUCCESS,
} from "../../actions/sales";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import Checkbox from "../../components/Checkbox/checkbox";
import ModalBase from "../../components/ModalBase/modalBase";
import { commonFunctions } from "../../utils/commonFunctions";
import { getSingleProductCalculation } from "./productCalculations";
import Spinner from "../../components/Spinner";

interface CreateProductProps {
  productId?: number;
  productInfo?: IProductCatalogItem;
  mode: "create" | "edit" | "clone" | "preview";
  onClose: (saved?: boolean) => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  productCatalogCRUD: (
    method: HTTPMethods,
    data: IProductCatalogItem
  ) => Promise<any>;
}

const EmptyPricing: IProductPricing = {
  margin: 0,
  customer_cost: 0,
  internal_cost: 0,
  margin_percent: 0,
  annual_customer_cost: 0,
};

const getBillingLabel = (billingTerm: CatalogBillingTerm) => {
  switch (billingTerm) {
    case "Monthly":
      return "Monthly Billing";
    case "Annual":
      return "Annual Billing";
    case "One Time":
      return "One Time Service Billing";
    default:
      return "Billing";
  }
};

export const getContractLabel = (contract: CatalogContractTerm) => {
  switch (contract) {
    case Monthly:
      return <span>Monthly Term</span>;
    case "12 Months":
      return (
        <>
          <span>12 Month Term</span>
          <span>(1 Year)</span>
        </>
      );
    case "24 Months":
      return (
        <>
          <span>24 Month Term</span>
          <span>(2 Years)</span>
        </>
      );
    case "36 Months":
      return (
        <>
          <span>36 Month Term</span>
          <span>(3 Years)</span>
        </>
      );
    case "One Time":
      return <span>One Time Service</span>;
    default:
      return <span>N/A</span>;
  }
};

const CreateProduct: React.FC<CreateProductProps> = (props) => {
  const [errMsg, setErrMsg] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<IErrorValidation>({});
  const [product, setProduct] = useState<IProductCatalogItem>({
    product_id: "",
    name: "",
    description: "",
    product_type: "Not Set",
    revenue_rounding: true,
    measurement_unit: "",
    no_of_units: null,
    pricing_data: {},
  });
  const isEditMode = props.mode === "edit";

  useEffect(() => {
    if (props.productId) fetchProduct(props.productId);
  }, [props.productId]);

  useEffect(() => {
    if (props.productInfo) setProduct(props.productInfo);
  }, [props.productInfo]);

  const fetchProduct = (id: number) => {
    setLoading(true);
    props
      .productCatalogCRUD("get", { id } as IProductCatalogItem)
      .then((action) => {
        if (action.type === PRODUCT_CATALOG_SUCCESS) {
          const productRes: IProductCatalogItem = action.response;
          if (props.mode === "clone") {
            delete productRes.id;
            productRes.name = productRes.name + "(cloned)";
          }
          setProduct(productRes);
        }
      })
      .finally(() => setLoading(false));
  };

  const isValid = () => {
    let isValid = true;
    const errs = cloneDeep(error);
    let errMsg = "";
    const billingTerms = Object.keys(product.pricing_data);
    if (!product.product_id.trim()) {
      isValid = false;
      errs.product_id = commonFunctions.getErrorState("This field is required");
    } else errs.product_id = commonFunctions.getErrorState();
    if (!product.name.trim()) {
      isValid = false;
      errs.name = commonFunctions.getErrorState("This field is required");
    } else errs.name = commonFunctions.getErrorState();
    if (product.product_type === "Not Set") {
      isValid = false;
      errs.product_type = commonFunctions.getErrorState(
        "This field is required"
      );
    } else if (!billingTerms.length) {
      isValid = false;
      errs.billing_term = commonFunctions.getErrorState(
        "This field is required"
      );
      errMsg = "Please select a billing term";
      errs.product_type = commonFunctions.getErrorState();
    } else if (
      billingTerms.length > 0 &&
      !Object.keys(product.pricing_data[billingTerms[0]]).length
    ) {
      isValid = false;
      errs.contract_term = commonFunctions.getErrorState(
        "This field is required"
      );
      errMsg = "Please select a contract term";
      errs.product_type = commonFunctions.getErrorState();
      errs.billing_term = commonFunctions.getErrorState();
    } else {
      errs.product_type = commonFunctions.getErrorState();
      errs.billing_term = commonFunctions.getErrorState();
      errs.contract_term = commonFunctions.getErrorState();
    }
    setErrMsg(errMsg);
    setError(errs);
    return isValid;
  };

  const onSave = () => {
    if (isValid()) {
      setLoading(true);
      const payload = cloneDeep(product);
      payload.description = !payload.description ? null : payload.description;
      payload.no_of_units =
        payload.no_of_units === null ? null : Number(payload.no_of_units);
      payload.measurement_unit = !payload.measurement_unit
        ? null
        : payload.measurement_unit;
      props
        .productCatalogCRUD(
          isEditMode ? "put" : "post",
          payload
        )
        .then((action) => {
          if (action.type === PRODUCT_CATALOG_SUCCESS) {
            props.addSuccessMessage(
              `Product ${isEditMode ? "updated" : "created"} successfully!`
            );
            props.onClose(true);
          } else if (action.type === PRODUCT_CATALOG_FAILURE) {
            const err = action.errorList.data; // check for duplicate product id
            if (isObject(err) && Object.keys(err).length > 0) {
              const errs = cloneDeep(error);
              Object.keys(err).forEach((key) => {
                if (Array.isArray(err[key])) {
                  errs[key] = commonFunctions.getErrorState(err[key][0]);
                } else if (err[key].message) {
                  errs[key] = commonFunctions.getErrorState(err[key].message);
                }
              });
              setError(errs);
            } else
              props.addErrorMessage(
                `Error ${isEditMode ? "updating" : "creating"} product!`
              );
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const handleProductDetailChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    let value: string | number = e.target.value;
    setProduct((prev) => ({
      ...prev,
      [e.target.name]: value,
    }));
  };

  const handleChangeRevenueRounding = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const clonedProduct = cloneDeep(product);
    const checked = e.target.checked; // This will be reverse value as it is called "Disable" Rounding Revenue
    clonedProduct.revenue_rounding = !checked;
    Object.keys(clonedProduct.pricing_data).forEach(
      (billingTerm: CatalogBillingTerm) => {
        Object.keys(clonedProduct.pricing_data[billingTerm]).forEach(
          (contract: CatalogContractTerm) => {
            clonedProduct.pricing_data[billingTerm][
              contract
            ] = getSingleProductCalculation(
              product.pricing_data[billingTerm][contract],
              true,
              !checked,
              billingTerm === "One Time"
            );
          }
        );
      }
    );
    setProduct(clonedProduct);
  };

  const handleChangeProductType = (e: React.ChangeEvent<HTMLInputElement>) => {
    const clonedProduct = cloneDeep(product);
    const productType = e.target.value as CatalogProductType;
    clonedProduct.product_type = productType;
    if (productType === "One Time Service")
      clonedProduct.pricing_data = {
        ["One Time"]: {
          ["One Time"]: { ...EmptyPricing },
        },
      };
    else if (productType === "Consumption")
      clonedProduct.pricing_data = {
        ["Monthly"]: {},
      };
    else clonedProduct.pricing_data = {};
    setErrMsg("");
    setError((prev) => ({
      ...prev,
      contract_term: commonFunctions.getErrorState(),
      billing_term: commonFunctions.getErrorState(),
    }));
    setProduct(clonedProduct);
  };

  const handleChangeBillingTerm = (e: React.ChangeEvent<HTMLInputElement>) => {
    const checked = e.target.checked;
    const clonedProduct = cloneDeep(product);
    const billingTerm = e.target.name as CatalogBillingTerm;

    // If we change a billing term, remove all the pricing info and selected Contracts
    Object.keys(product.pricing_data).forEach((term: CatalogBillingTerm) => {
      clonedProduct.pricing_data[term] = {};
    });
    if (checked) clonedProduct.pricing_data[billingTerm] = {};
    else delete clonedProduct.pricing_data[billingTerm];
    setProduct(clonedProduct);
  };

  const handleChangeContractTerm = (e: React.ChangeEvent<HTMLInputElement>) => {
    const checked = e.target.checked;
    const clonedProduct = cloneDeep(product);
    const contractTerm = e.target.name as CatalogContractTerm;
    // Add or remove the contract term for each billing option
    Object.keys(product.pricing_data).forEach((term: CatalogBillingTerm) => {
      if (checked) {
        clonedProduct.pricing_data[term][contractTerm] = { ...EmptyPricing };
      } else delete clonedProduct.pricing_data[term][contractTerm];
    });
    setProduct(clonedProduct);
  };

  const handleChangePricing = (
    e: React.ChangeEvent<HTMLInputElement>,
    billingTerm: CatalogBillingTerm,
    contractTerm: CatalogContractTerm
  ) => {
    const clonedProduct = cloneDeep(product);
    const pricing = clonedProduct.pricing_data[billingTerm][contractTerm];
    pricing[e.target.name] = Number(e.target.value);
    clonedProduct.pricing_data[billingTerm][
      contractTerm
    ] = getSingleProductCalculation(
      pricing,
      e.target.name !== "margin",
      product.revenue_rounding,
      billingTerm === "One Time"
    );
    setProduct(clonedProduct);
  };

  const getBody = () => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });
    const billingTerms = new Set(Object.keys(product.pricing_data)) as Set<
      CatalogBillingTerm
    >;
    let contractTerms: Set<CatalogContractTerm> = new Set();
    if (billingTerms.size > 0) {
      const billingTerm = Array.from(billingTerms)[0];
      // Contract Terms will be same for each billing option
      contractTerms = new Set(
        Object.keys(product.pricing_data[billingTerm]) as CatalogContractTerm[]
      );
    }

    return (
      <>
        {errMsg && <div className="pc-validation-error">{errMsg}</div>}
        <Spinner show={loading} className="create-product-loader" />
        <div className="edit-top-section">
          <div className="product-unique-details">
            <Input
              field={{
                value: product.product_id,
                label: "Product ID",
                type: "TEXT",
                isRequired: true,
              }}
              width={4}
              name={"product_id"}
              placeholder="Enter Product ID"
              onChange={handleProductDetailChange}
              error={error.product_id}
            />
            <Input
              field={{
                value: product.name,
                label: "Product Name",
                type: "TEXT",
                isRequired: true,
              }}
              width={8}
              name={"name"}
              placeholder="Enter Name"
              onChange={handleProductDetailChange}
              error={error.name}
            />
            <Input
              field={{
                value: product.description,
                label: "Product Description",
                type: "TEXTAREA",
                isRequired: false,
              }}
              width={12}
              name={"description"}
              placeholder="Enter Description"
              onChange={handleProductDetailChange}
            />
          </div>
          <div className="product-bundle-options">
            <Input
              field={{
                options: [
                  "Service Contract",
                  "Consumption",
                  "One Time Service",
                ],
                label: "Product Type",
                type: "RADIO",
                value: `${product.product_type}`,
              }}
              name={`product_type`}
              width={4}
              onChange={handleChangeProductType}
              className={"product-type-radio"}
              error={error.product_type}
            />
            <div
              className={
                "bundle-checkboxes field-section col-md-4" +
                (error.billing_term &&
                error.billing_term.errorState === "error"
                  ? " bundle-error-border"
                  : "")
              }
            >
              <div className="field__label row">
                <label className="field__label-label">Billing Options</label>
              </div>
              <div className="checkbox-group">
                <Checkbox
                  isChecked={billingTerms.has("Monthly")}
                  name={"Monthly"}
                  onChange={handleChangeBillingTerm}
                  disabled={
                    product.product_type !== "Service Contract"
                  }
                >
                  Monthly
                </Checkbox>
                <Checkbox
                  isChecked={billingTerms.has("Annual")}
                  name={"Annual"}
                  onChange={handleChangeBillingTerm}
                  disabled={
                    product.product_type !== "Service Contract"
                  }
                >
                  Annual
                </Checkbox>
                <Checkbox
                  isChecked={billingTerms.has("One Time")}
                  name={"One Time"}
                  onChange={handleChangeBillingTerm}
                  disabled={true}
                >
                  One Time
                </Checkbox>
              </div>
            </div>
            <div
              className={
                "bundle-checkboxes field-section col-md-4" +
                (error.contract_term &&
                error.contract_term.errorState === "error"
                  ? " bundle-error-border"
                  : "")
              }
            >
              <div className="field__label row">
                <label className="field__label-label">Contract Term</label>
              </div>
              <div className="checkbox-group">
                <Checkbox
                  isChecked={contractTerms.has(Monthly)}
                  name={Monthly}
                  onChange={handleChangeContractTerm}
                  disabled={
                    billingTerms.size === 0 ||
                    billingTerms.has("One Time")
                  }
                >
                  Monthly
                </Checkbox>
                <Checkbox
                  isChecked={contractTerms.has("12 Months")}
                  name={"12 Months"}
                  onChange={handleChangeContractTerm}
                  disabled={
                    billingTerms.size === 0 ||
                    billingTerms.has("One Time")
                  }
                >
                  12 Months
                </Checkbox>
                <Checkbox
                  isChecked={contractTerms.has("24 Months")}
                  name={"24 Months"}
                  onChange={handleChangeContractTerm}
                  disabled={
                    billingTerms.size === 0 ||
                    billingTerms.has("One Time")
                  }
                >
                  24 Months
                </Checkbox>
                <Checkbox
                  isChecked={contractTerms.has("36 Months")}
                  name={"36 Months"}
                  onChange={handleChangeContractTerm}
                  disabled={
                    billingTerms.size === 0 ||
                    billingTerms.has("One Time")
                  }
                >
                  36 Months
                </Checkbox>
              </div>
            </div>
          </div>
        </div>
        <div className="product-control-section field-section">
          <div className="field__label row">
            <label className="field__label-label">Product Controls</label>
          </div>
          <Checkbox
            isChecked={!product.revenue_rounding}
            name="revenue_rounding"
            onChange={handleChangeRevenueRounding}
          >
            Disable Revenue Rounding
          </Checkbox>
          <Input
            field={{
              value: product.measurement_unit,
              label: "Unit of Measure",
              type: "TEXT",
              isRequired: false,
            }}
            width={12}
            name={"measurement_unit"}
            placeholder="Enter Unit"
            onChange={handleProductDetailChange}
            error={error.measurement_unit}
          />
          <Input
            field={{
              value: product.no_of_units === null ? "" : product.no_of_units,
              label: "Units Included",
              type: "NUMBER",
              isRequired: false,
            }}
            width={12}
            name={"no_of_units"}
            className="hide-number-stepper"
            placeholder="Enter No. of Units"
            onChange={handleProductDetailChange}
            error={error.no_of_units}
          />
        </div>
        {Boolean(billingTerms.size && contractTerms.size) && (
          <div className="catalog-pricing-section">
            <h4>Pricing</h4>
            {[
              "Monthly",
              "Annual",
              "One Time",
            ]
              .filter((el) => billingTerms.has(el))
              .map((billingTerm, index) => (
                <div className="pricing-rows-container" key={index}>
                  <h5>{getBillingLabel(billingTerm)}</h5>
                  {[
                    Monthly,
                    "12 Months",
                    "24 Months",
                    "36 Months",
                    "One Time",
                  ]
                    .filter((el) => contractTerms.has(el))
                    .map((contractTerm, idx) => {
                      const pricing: IProductPricing =
                        product.pricing_data[billingTerm][contractTerm];
                      return (
                        <div className="catalog-pricing-row" key={idx}>
                          <div className="billing-col">
                            {getContractLabel(contractTerm)}
                          </div>
                          <Input
                            field={{
                              value: pricing.internal_cost,
                              label:
                                billingTerm === "One Time"
                                  ? "Internal Cost($)"
                                  : "Internal Monthly Cost($)",
                              type: "NUMBER",
                              isRequired: false,
                            }}
                            width={12}
                            defaultValue={0}
                            minimumValue="0"
                            name={"internal_cost"}
                            placeholder="Enter Cost"
                            onChange={(e) =>
                              handleChangePricing(e, billingTerm, contractTerm)
                            }
                            className={"catalog-pricing-rate hide-number-stepper"}
                          />
                          <Input
                            field={{
                              value: pricing.margin_percent,
                              label: "Margin %",
                              type: "NUMBER",
                              isRequired: false,
                            }}
                            width={12}
                            defaultValue={0}
                            minimumValue="0"
                            maximumValue="100"
                            name={"margin_percent"}
                            placeholder="Enter Margin %"
                            onChange={(e) =>
                              handleChangePricing(e, billingTerm, contractTerm)
                            }
                            className={"catalog-pricing-rate hide-number-stepper"}
                          />
                          <Input
                            field={{
                              value: pricing.margin,
                              label: "Margin($)",
                              type: "NUMBER",
                              isRequired: false,
                            }}
                            width={12}
                            name={"margin"}
                            placeholder="Enter Margin"
                            onChange={(e) =>
                              handleChangePricing(e, billingTerm, contractTerm)
                            }
                            className={"catalog-pricing-rate hide-number-stepper"}
                          />
                          <Input
                            field={{
                              value: formatter.format(pricing.customer_cost),
                              label:
                                billingTerm === "One Time"
                                  ? "Customer Cost($)"
                                  : "Customer Monthly Cost($)",
                              type: "TEXT",
                              isRequired: false,
                            }}
                            width={12}
                            name={"customer_cost"}
                            placeholder="Enter Cost"
                            onChange={() => null}
                            disabled={true}
                            className={"catalog-pricing-rate hide-number-stepper"}
                          />
                          {billingTerm !== "One Time" && (
                            <Input
                              field={{
                                value: formatter.format(
                                  pricing.annual_customer_cost
                                ),
                                label: "Customer Annual Cost($)",
                                type: "TEXT",
                                isRequired: false,
                              }}
                              width={12}
                              name={"annual_customer_cost"}
                              placeholder="Enter Cost"
                              onChange={() => null}
                              disabled={true}
                              className={"catalog-pricing-rate hide-number-stepper"}
                            />
                          )}
                        </div>
                      );
                    })}
                </div>
              ))}
          </div>
        )}
      </>
    );
  };

  return (
    <ModalBase
      show={true}
      onClose={props.onClose}
      titleElement={`${
        isEditMode ? "Edit" : props.mode === "preview" ? "Preview" : "Create"
      } Product`}
      bodyElement={getBody()}
      footerElement={
        props.mode !== "preview" && (
          <>
            <SquareButton
              content={"Close"}
              bsStyle={"default"}
              onClick={props.onClose}
              disabled={loading}
            />
            <SquareButton
              content={isEditMode ? "Update" : "Create"}
              bsStyle={"primary"}
              onClick={onSave}
              disabled={loading}
            />
          </>
        )
      }
      className={
        "product-edit-modal" +
        (props.mode === "preview" ? " product-preview" : "")
      }
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  productCatalogCRUD: (method: HTTPMethods, data: IProductCatalogItem) =>
    dispatch(productCatalogCRUD(method, undefined, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateProduct);
