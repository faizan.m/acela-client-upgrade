import React, { useMemo, useState } from "react";
import { isEmpty, isNil } from "lodash";
import { connect } from "react-redux";
import {
  bulkCreateProducts,
  bulkUpdateProducts,
  uploadProductImportCSV,
  PC_IMPORT_CSV_FAILURE,
  BULK_EDIT_PRODUCT_SUCCESS,
  BULK_EDIT_PRODUCT_FAILURE,
} from "../../actions/sales";
import {
  addErrorMessage,
  addInfoMessage,
  addSuccessMessage,
} from "../../actions/appState";
import SimpleList from "../../components/List";
import SquareButton from "../../components/Button/button";
import Checkbox from "../../components/Checkbox/checkbox";
import ModalBase from "../../components/ModalBase/modalBase";
import DeleteButton from "../../components/Button/deleteButton";
import CreateProduct from "./CreateProduct";
import "./style.scss";
import Spinner from "../../components/Spinner";

interface ImportProductProps extends ICommonProps {
  uploading: boolean;
  validationInfo: IProductValidation;
  addInfoMessage: TShowInfoMessage;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  uploadImportFile: (data: FormData) => Promise<any>;
  bulkUpdateProducts: (data: IProductCatalogItem[]) => Promise<any>;
  bulkCreateProducts: (data: IProductCatalogItem[]) => Promise<any>;
}

const ErrorLogsModal = ({
  show,
  errors,
  onClose,
}: {
  show: boolean;
  errors: string[];
  onClose: () => void;
}) => {
  const getBody = () => {
    return errors && errors.length ? (
      <ul className="product-errors-list">
        {errors.map((msg, idx) => (
          <li key={idx}>{msg}</li>
        ))}
      </ul>
    ) : (
      <div className="no-data">No errors</div>
    );
  };
  return (
    <ModalBase
      show={show}
      onClose={onClose}
      titleElement={"Error Logs"}
      bodyElement={getBody()}
      footerElement={
        <SquareButton
          content="Close"
          onClick={onClose}
          bsStyle={"default"}
        />
      }
      className="pc-validation-error-logs"
    />
  );
};

const ImportProduct: React.FC<ImportProductProps> = (props) => {
  const [saving, setSaving] = useState<boolean>(false);
  const [previewProduct, setPreviewProduct] = useState<IProductCatalogItem>();
  const [errorModalInfo, setErrorModalInfo] = useState<{
    show: boolean;
    errors: string[];
  }>({
    show: false,
    errors: [],
  });
  // Object storing the productIds of the products to be ignored
  const [ignoreInfo, setIgnoreInfo] = useState<{
    create: string[];
    update: string[];
  }>({ create: [], update: [] });

  const hasValidationData = Boolean(props.validationInfo);
  const showPreviewModal = Boolean(previewProduct);
  const hasFileErrors = Boolean(
    props.validationInfo &&
      props.validationInfo.file_errors &&
      props.validationInfo.file_errors.length
  );
  const hasProductErrors = Boolean(
    props.validationInfo &&
      props.validationInfo.product_errors &&
      !isEmpty(props.validationInfo.product_errors)
  );
  const hasProductsToCreate = Boolean(
    props.validationInfo &&
      props.validationInfo.valid_products &&
      props.validationInfo.valid_products.products_to_create &&
      props.validationInfo.valid_products.products_to_create.length
  );
  const hasProductsToUpdate = Boolean(
    props.validationInfo &&
      props.validationInfo.valid_products &&
      props.validationInfo.valid_products.products_to_update &&
      props.validationInfo.valid_products.products_to_update.length
  );

  const rowErrors: IProductRowError[] = useMemo(
    () =>
      props.validationInfo && props.validationInfo.product_errors
        ? Object.values(props.validationInfo.product_errors).reduce(
            (rows, obj) => {
              return [...rows, ...obj.row_errors.filter((el) => !el.is_valid)];
            },
            []
          )
        : [],
    [props.validationInfo]
  );

  const togglePreviewModal = (product?: IProductCatalogItem) => {
    setPreviewProduct(product);
  };

  const productErrors: {
    productId: string;
    errors: string[];
  }[] = useMemo(
    () =>
      props.validationInfo && props.validationInfo.product_errors
        ? Object.keys(props.validationInfo.product_errors)
            .filter(
              (productId) =>
                props.validationInfo.product_errors[productId].config_errors
                  .length
            )
            .reduce((errorArr, productId) => {
              return [
                ...errorArr,
                {
                  productId,
                  errors:
                    props.validationInfo.product_errors[productId]
                      .config_errors,
                },
              ];
            }, [])
        : [],
    [props.validationInfo]
  );

  const bulkCreateUpdateProducts = () => {
    const ignoredCreateProducts = new Set(ignoreInfo.create);
    const ignoredUpdateProducts = new Set(ignoreInfo.update);
    const validProducts = props.validationInfo.valid_products;
    const productsToCreate = validProducts.products_to_create.filter(
      (el) => !ignoredCreateProducts.has(el.product_id)
    );
    const productsToUpdate = validProducts.products_to_update.filter(
      (el) => !ignoredUpdateProducts.has(el.product_id)
    );

    if (productsToCreate.length + productsToUpdate.length === 0) {
      props.addInfoMessage("All products are ignored!");
      return;
    }
    let promises = [
      productsToCreate.length
        ? props.bulkCreateProducts(productsToCreate)
        : Promise.resolve({}),
      productsToUpdate.length
        ? props.bulkUpdateProducts(productsToUpdate)
        : Promise.resolve({}),
    ];

    setSaving(true);
    Promise.all(promises)
      .then(([createRes, updateRes]) => {
        let redirect = true;
        if (
          createRes.type === BULK_EDIT_PRODUCT_SUCCESS &&
          updateRes.type === BULK_EDIT_PRODUCT_SUCCESS
        )
          props.addSuccessMessage("Products created & overridden successfully!");
        else if (createRes.type === BULK_EDIT_PRODUCT_SUCCESS)
          props.addSuccessMessage("Products created successfully!");
        else if (updateRes.type === BULK_EDIT_PRODUCT_SUCCESS)
          props.addSuccessMessage("Products overridden successfully!");
        if (createRes.type === BULK_EDIT_PRODUCT_FAILURE) {
          redirect = false;
          props.addErrorMessage("Error creating the products!");
        }
        if (updateRes.type === BULK_EDIT_PRODUCT_FAILURE) {
          redirect = false;
          props.addErrorMessage("Error overriding the products!");
        }
        if (redirect) props.history.push("/product-catalog");
      })
      .finally(() => setSaving(false));
  };

  const openErrorModal = (errors: string[]) => {
    setErrorModalInfo({
      show: true,
      errors,
    });
  };

  const closeErrorModal = () => {
    setErrorModalInfo({
      show: false,
      errors: null,
    });
  };

  const handleFileSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file: File = e.target.files[0];
    const formData = new FormData();
    formData.append("file", file);
    props.uploadImportFile(formData).then((action) => {
      if (action.type === PC_IMPORT_CSV_FAILURE) {
        props.addErrorMessage("Error uploading file");
      }
    });
  };

  const onChangeIgnoreCheckbox = (
    ignore: boolean,
    mode: "create" | "update",
    productId: string
  ) => {
    const ignoreSet = new Set(ignoreInfo[mode]);
    if (ignore) ignoreSet.add(productId);
    else ignoreSet.delete(productId);
    setIgnoreInfo((prev) => ({
      ...prev,
      [mode]: Array.from(ignoreSet),
    }));
  };

  const rowErrorColumns: IListColumn<IProductRowError>[] = [
    {
      name: "Row #",
      className: "cell-center-align",
      id: "row_number",
    },
    {
      name: "Product ID",
      Cell: (row) => (
        <span title={row.data.product_id ? row.data.product_id : "-"}>
          {row.data.product_id ? row.data.product_id : "-"}
        </span>
      ),
    },
    {
      name: "Name",
      Cell: (row) => (
        <span title={row.data.name ? row.data.name : "-"}>
          {row.data.name ? row.data.name : "-"}
        </span>
      ),
    },
    {
      name: "Description",
      Cell: (row) => (
        <span title={row.data.description ? row.data.description : "-"}>
          {row.data.description ? row.data.description : "-"}
        </span>
      ),
    },
    {
      name: "Product Type",
      Cell: (row) => (
        <span title={row.data.product_type ? row.data.product_type : "-"}>
          {row.data.product_type ? row.data.product_type : "-"}
        </span>
      ),
    },
    {
      name: "Billing",
      Cell: (row) => (
        <span title={row.data.billing_option ? row.data.billing_option : "-"}>
          {row.data.billing_option ? row.data.billing_option : "-"}
        </span>
      ),
    },
    {
      name: "Measurement",
      Cell: (row) => (
        <span
          title={row.data.measurement_unit ? row.data.measurement_unit : "-"}
        >
          {row.data.measurement_unit ? row.data.measurement_unit : "-"}
        </span>
      ),
    },
    {
      name: "Units",
      className: "cell-center-align",
      Cell: (row) => (
        <span title={row.data.no_of_units ? String(row.data.no_of_units) : "-"}>
          {row.data.no_of_units ? row.data.no_of_units : "-"}
        </span>
      ),
    },
    {
      name: "Rounding",
      className: "cell-center-align cell-capitalize",
      Cell: (row) => (
        <span
          title={
            !isNil(row.data.revenue_rounding)
              ? String(row.data.revenue_rounding)
              : "-"
          }
        >
          {!isNil(row.data.revenue_rounding)
            ? String(row.data.revenue_rounding)
            : "-"}
        </span>
      ),
    },
    {
      name: "Internal Cost $",
      className: "cell-center-align",
      Cell: (row) => (
        <span
          title={
            !isNil(row.data.internal_cost)
              ? String(row.data.internal_cost)
              : "-"
          }
        >
          {!isNil(row.data.internal_cost) ? row.data.internal_cost : "-"}
        </span>
      ),
    },
    {
      name: "Margin %",
      className: "cell-center-align",
      Cell: (row) => (
        <span
          title={
            !isNil(row.data.margin_percent)
              ? String(row.data.margin_percent)
              : "-"
          }
        >
          {!isNil(row.data.margin_percent)
            ? row.data.margin_percent + "%"
            : "-"}
        </span>
      ),
    },
    {
      name: "Errors",
      className: "cell-center-align",
      Cell: (row) => (
        <DeleteButton
          type="pdf_preview"
          title="View Errors"
          onClick={() => openErrorModal(row.errors)}
        />
      ),
    },
  ];

  const productErrorColumns: IListColumn<{
    productId: string;
    errors: string[];
  }>[] = [
    {
      id: "productId",
      name: "Product ID",
    },
    {
      name: "Errors",
      Cell: (row) => (
        <ul className="product-errors-list">
          {row.errors.map((msg, idx) => (
            <li key={idx}>{msg}</li>
          ))}
        </ul>
      ),
    },
  ];

  const validProductsColumns: IListColumn<IProductCatalogItem>[] = [
    {
      id: "product_id",
      name: "Product ID",
    },
    {
      id: "name",
      name: "Name",
    },
    {
      id: "description",
      name: "Description",
    },
    {
      id: "product_type",
      name: "Product Type",
    },
    {
      name: "Mode",
      className: "cell-center-align",
      Cell: (row) => (row.id ? "OVERRIDE" : "CREATE"),
    },
    {
      name: "Preview",
      className: "cell-center-align",
      Cell: (row) => (
        <DeleteButton
          type="pdf_preview"
          title="Preview Product"
          onClick={() => togglePreviewModal(row)}
        />
      ),
    },
    {
      name: "Ignore",
      className: "cell-center-align",
      Cell: (row) => {
        const mode = row.id ? "update" : "create";
        return (
          <Checkbox
            name="ignore"
            isChecked={ignoreInfo[mode].includes(row.product_id)}
            onChange={(e) =>
              onChangeIgnoreCheckbox(e.target.checked, mode, row.product_id)
            }
          />
        );
      },
    },
  ];

  return (
    <>
      <div className="product-import-validation-container">
        <Spinner show={saving} className="pci-spinner" />
        <div className="product-import-header">
          <h2>Product Import Validation Info</h2>
          <div className="right-section">
            <label className="btn square-btn btn-primary file-button pc-icon-btn">
              <span className="img-text-in-btn">
                <img
                  className={props.uploading ? "icon__loading" : ""}
                  src={
                    props.uploading
                      ? "/assets/icons/loading.gif"
                      : "/assets/icons/import.png"
                  }
                  alt="Upload File"
                />
                <span>Import</span>
                <input
                  type="file"
                  name="csvFile"
                  accept=".csv"
                  onChange={handleFileSelect}
                  onClick={(event: any) => {
                    event.target.value = null;
                  }}
                  disabled={props.uploading}
                />
              </span>
            </label>
            <SquareButton
              content="Back"
              onClick={() => props.history.push("/product-catalog")}
              className="pc-icon-btn"
              bsStyle={"default"}
            />
          </div>
        </div>
        {hasValidationData ? (
          <>
            {(hasFileErrors || hasProductErrors) && (
              <section className="import-errors-section">
                <h3>Error Summary</h3>
                {hasFileErrors && (
                  <div className="error-type-section file-errors-section">
                    <h4>File Errors</h4>
                    <ul className="product-errors-list">
                      {props.validationInfo.file_errors.map((errMsg, idx) => (
                        <li key={idx}>{errMsg}</li>
                      ))}
                    </ul>
                  </div>
                )}
                {Boolean(rowErrors.length) && (
                  <div className="error-type-section">
                    <h4>Row Errors</h4>
                    <SimpleList
                      uniqueKey={"row_number"}
                      columns={rowErrorColumns}
                      rows={rowErrors}
                      className="pvi-row-error-table"
                    />
                  </div>
                )}
                {Boolean(productErrors.length) && (
                  <div className="error-type-section product-errors-section">
                    <h4>Product Errors</h4>
                    <SimpleList
                      uniqueKey={"productId"}
                      columns={productErrorColumns}
                      rows={productErrors}
                      className="pvi-product-error-table"
                    />
                  </div>
                )}
              </section>
            )}
            {(hasProductsToCreate || hasProductsToUpdate) && (
              <section className="valid-products-creation">
                <h3 title="These are the valid products which have all the sufficient data to create/update">
                  <span>Valid Products</span>
                  <SquareButton
                    onClick={bulkCreateUpdateProducts}
                    content="Create/Update Products"
                    title="Create/Update the non-ignored products"
                    bsStyle={"primary"}
                    className="create-update-product-btn"
                    disabled={saving}
                  />
                </h3>
                <SimpleList
                  uniqueKey={"product_id"}
                  columns={validProductsColumns}
                  rows={[
                    ...props.validationInfo.valid_products.products_to_create,
                    ...props.validationInfo.valid_products.products_to_update,
                  ]}
                  className="pvi-edit-table"
                />
              </section>
            )}
          </>
        ) : (
          <div className="no-validation-info">
            {props.uploading ? "Uploading..." : "Please upload a CSV file"}
          </div>
        )}
      </div>
      <ErrorLogsModal {...errorModalInfo} onClose={closeErrorModal} />
      {showPreviewModal && (
        <CreateProduct
          mode="preview"
          productInfo={previewProduct}
          onClose={() => togglePreviewModal()}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  uploading: state.sales.isFetching,
  validationInfo: state.sales.productValidationInfo,
});

const mapDispatchToProps = (dispatch: any) => ({
  addInfoMessage: (message: string) => dispatch(addInfoMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  uploadImportFile: (data: FormData) => dispatch(uploadProductImportCSV(data)),
  bulkCreateProducts: (data: IProductCatalogItem[]) =>
    dispatch(bulkCreateProducts(data)),
  bulkUpdateProducts: (data: IProductCatalogItem[]) =>
    dispatch(bulkUpdateProducts(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ImportProduct);
