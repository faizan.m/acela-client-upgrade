import { round } from "lodash";

export const getSingleProductCalculation = (
  pricing: IProductPricing,
  calculateFromMarginPercent: boolean = true,
  revenueRounding: boolean = true,
  isOneTime: boolean = false
): IProductPricing => {
  const newPricing: IProductPricing = { ...pricing };
  newPricing.internal_cost = round(newPricing.internal_cost, 2);
  if (calculateFromMarginPercent) {
    newPricing.margin_percent = round(newPricing.margin_percent, 2);
    if (newPricing.margin_percent === 100) newPricing.margin_percent = 99.99;
    newPricing.customer_cost = round(
      newPricing.internal_cost / (1 - newPricing.margin_percent / 100),
      revenueRounding ? 0 : 2
    );
    newPricing.margin = round(
      newPricing.customer_cost - newPricing.internal_cost,
      2
    );
  } else {
    newPricing.margin = round(newPricing.margin, 2);
    newPricing.customer_cost = round(
      newPricing.internal_cost + newPricing.margin,
      revenueRounding ? 0 : 2
    );
    newPricing.margin_percent =
      newPricing.customer_cost === 0
        ? 0
        : round(
            (1 - newPricing.internal_cost / newPricing.customer_cost) * 100,
            2
          );
  }
  newPricing.annual_customer_cost = isOneTime
    ? null
    : round(12 * newPricing.customer_cost, revenueRounding ? 0 : 2);
  return newPricing;
};
