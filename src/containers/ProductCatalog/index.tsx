import React, { useState } from "react";
import { connect } from "react-redux";
import { cloneDeep, isNil } from "lodash";
import {
  productCatalogCRUD,
  fetchProductsToExport,
  uploadProductImportCSV,
  PC_IMPORT_CSV_SUCCESS,
  PC_IMPORT_CSV_FAILURE,
  PC_EXPORT_CSV_SUCCESS,
  PC_EXPORT_CSV_FAILURE,
  PRODUCT_CATALOG_SUCCESS,
  PRODUCT_CATALOG_FAILURE,
} from "../../actions/sales";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import Table from "../../components/Table/table";
import Select from "../../components/Input/Select/select";
import SquareButton from "../../components/Button/button";
import EditButton from "../../components/Button/editButton";
import DeleteButton from "../../components/Button/deleteButton";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import CreateProduct from "./CreateProduct";
import { CatalogFilters } from "./filters";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import { exportCSVFile } from "../../utils/download";
import "./style.scss";

interface ProductCatalogProps extends ICommonProps {
  uploading: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  deleteProduct: (id: number) => Promise<any>;
  uploadImportFile: (data: FormData) => Promise<any>;
  fetchExportFile: (
    params: IServerPaginationParams & IProductCatalogFilters
  ) => Promise<any>;
  fetchProductCatalog: (
    params: IServerPaginationParams & IProductCatalogFilters
  ) => Promise<any>;
}

interface PaginationState {
  reset: boolean;
  totalRows: number;
  totalPages: number;
  filters: IProductCatalogFilters;
  params: IServerPaginationParams;
}

const EmptyPagination: PaginationState = {
  filters: {},
  totalRows: 0,
  reset: false,
  totalPages: 0,
  params: { page: 1, page_size: 25 },
};

enum ModalType {
  FILTER,
  CREATE_PRODUCT,
}

const ProductTypeOptions = [
  {
    value: "Service Contract",
    label: "Service Contract",
  },
  {
    value: "Consumption",
    label: "Consumption",
  },
  {
    value: "One Time Service",
    label: "One Time Service",
  },
];

const ProductCatalog: React.FC<ProductCatalogProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [rows, setRows] = useState<IProductCatalogItem[]>([]);
  const [searchString, setSearchString] = useState<string>("");
  const [editProductId, setEditProductId] = useState<number>(null);
  const [cloneProductId, setCloneProductId] = useState<number>(null);
  const [showModal, setShowModal] = useState<null | ModalType>(null);
  const [fetchingFile, setFetchingFile] = useState<boolean>(false);
  const [pagination, setPagination] = useState<PaginationState>({
    ...EmptyPagination,
  });

  // Server side searching, sorting, ordering
  const fetchData = (
    params: IServerPaginationParams = {},
    filters: IProductCatalogFilters = {}
  ) => {
    const newPaginationParams = cloneDeep({ ...pagination.params, ...params });
    const newFilterParams = cloneDeep({ ...pagination.filters, ...filters });
    const apiParams: IServerPaginationParams & IProductCatalogFilters = {
      ...newPaginationParams,
      ...newFilterParams,
    };
    if (apiParams.product_type)
      apiParams.product_type = (apiParams.product_type as string[]).join(",");
    setLoading(true);
    props
      .fetchProductCatalog(apiParams)
      .then((action) => {
        if (action.type === PRODUCT_CATALOG_SUCCESS) {
          const res: IPaginatedDefault & { results: IProductCatalogItem[] } =
            action.response;
          const rows = res.results;
          setRows(rows);
          setPagination({
            reset: false,
            totalRows: res.count,
            totalPages: Math.ceil(res.count / pagination.params.page_size),
            filters: newFilterParams,
            params: newPaginationParams,
          });
        }
      })
      .finally(() => setLoading(false));
  };

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value.toLowerCase();
    const newPagination = cloneDeep(pagination);
    setSearchString(e.target.value);
    if (search && search.length > 0) {
      newPagination.reset = true;
      newPagination.params.search = search;
    } else {
      newPagination.reset = true;
      newPagination.params.page = 1;
      newPagination.params.search = "";
    }
    setPagination(newPagination);
    fetchData(newPagination.params);
  };

  const handleFileSelect = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file: File = Array.from(e.target.files)[0];
    const formData = new FormData();
    formData.append("file", file);
    props.uploadImportFile(formData).then((action) => {
      if (action.type === PC_IMPORT_CSV_SUCCESS) {
        props.history.push("/product-catalog/import");
      } else if (action.type === PC_IMPORT_CSV_FAILURE) {
        // handle this
        props.addErrorMessage("Error uploading file");
      }
    });
  };

  const handleDownloadTemplate = () => {
    let headers = {
      product_id: "Product ID",
      name: "Name",
      description: "Description",
      product_type: "Product Type",
      billing_option: "Billing Option",
      contract_term: "Contract Term",
      measurement_unit: "Measurement Unit",
      no_of_units: "Units",
      revenue_rounding: "Revenue Rounding",
      internal_cost: "Internal Cost",
      margin_percent: "Margin Percent",
      margin: "Margin",
      customer_cost: "Customer Cost",
      annual_customer_cost: "Annual Customer Cost",
    };
    let fileTitle = `ProductCatalog`;
    exportCSVFile(headers, [], fileTitle);
  };

  const handleExportClick = () => {
    const apiParams: IServerPaginationParams &
      IProductCatalogFilters = cloneDeep({
      ...pagination.params,
      ...pagination.filters,
    });
    delete apiParams.page;
    delete apiParams.page_size;
    if (apiParams.product_type)
      apiParams.product_type = (apiParams.product_type as string[]).join(",");
    setFetchingFile(true);
    props
      .fetchExportFile(apiParams)
      .then((action) => {
        if (action.type === PC_EXPORT_CSV_SUCCESS) {
          const rows: IProductRowItem[] = action.response;
          let headers = {
            product_id: "Product ID",
            name: "Name",
            description: "Description",
            product_type: "Product Type",
            billing_option: "Billing Option",
            contract_term: "Contract Term",
            measurement_unit: "Measurement Unit",
            no_of_units: "Units",
            revenue_rounding: "Revenue Rounding",
            internal_cost: "Internal Cost",
            margin_percent: "Margin Percent",
            margin: "Margin",
            customer_cost: "Customer Cost",
            annual_customer_cost: "Annual Customer Cost",
          };
          let dataRows = [];
          rows.forEach((row) => {
            // This is done to ensure the order of keys in objects
            dataRows.push({
              product_id: row.product_id,
              name: row.name,
              description: row.description ? '"' + row.description + '"' : "",
              product_type: row.product_type,
              billing_option: row.billing_option,
              contract_term: row.contract_term,
              measurement_unit: row.measurement_unit
                ? row.measurement_unit
                : "",
              no_of_units: !isNil(row.no_of_units) ? row.no_of_units : "",
              revenue_rounding: row.revenue_rounding,
              internal_cost: row.internal_cost,
              margin_percent: row.margin_percent,
              margin: row.margin,
              customer_cost: row.customer_cost,
              annual_customer_cost: row.annual_customer_cost
                ? row.annual_customer_cost
                : "",
            });
          });

          let fileTitle = `ProductCatalog_${new Date().toISOString()}`;
          exportCSVFile(headers, dataRows, fileTitle);
          props.addSuccessMessage("File downloaded successfully!");
        } else if (action.type === PC_EXPORT_CSV_FAILURE) {
          props.addErrorMessage("Error exporting file!");
        }
      })
      .finally(() => setFetchingFile(false));
  };

  const onEditClick = (id: number) => {
    setEditProductId(id);
    setShowModal(ModalType.CREATE_PRODUCT);
  };

  const onCloneClick = (id: number) => {
    setCloneProductId(id);
    setShowModal(ModalType.CREATE_PRODUCT);
  };

  const onDeleteRowClick = (id: number) => {
    setLoading(true);
    props.deleteProduct(id).then((action) => {
      if (action.type === PRODUCT_CATALOG_SUCCESS) {
        props.addSuccessMessage("Product deleted successfully!");
        resetFetch();
      } else if (action.type === PRODUCT_CATALOG_FAILURE) {
        props.addErrorMessage("Error deleting product!");
      }
    });
  };

  const resetFetch = () => {
    const newPagination = cloneDeep(pagination);
    newPagination.params.page = 1;
    newPagination.reset = true;
    setPagination(newPagination);
    fetchData(newPagination.params);
  };

  const onCloseCreateProduct = (saved?: boolean) => {
    setShowModal(null);
    setEditProductId(null);
    setCloneProductId(null);
    if (saved === true) resetFetch();
  };

  const applyFilters = (filters: IProductCatalogFilters) => {
    const newPagination = cloneDeep(pagination);
    newPagination.params.page = 1;
    newPagination.reset = true;
    newPagination.filters = filters;
    setShowModal(null);
    setPagination(newPagination);
    fetchData(newPagination.params, newPagination.filters);
  };

  // Rendering methods
  const renderFilters = () => {
    const filters = pagination.filters;
    const shouldRenderFilters =
      filters.product_type && filters.product_type.length;

    return shouldRenderFilters ? (
      <div className="custom-filters-listing pc-filters">
        <label className="pc-filters-heading">Applied Filters: </label>
        {Boolean(filters.product_type && filters.product_type.length) && (
          <div className="section-show-filters pc-applied-filters-row">
            <label>Product Type: </label>
            <Select
              name="product_type"
              value={filters.product_type}
              onChange={(e) => applyFilters({ product_type: e.target.value })}
              options={ProductTypeOptions}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };

  const renderTopBar = () => {
    return (
      <div className={"pc__table-top "}>
        <div className="pc__table-actions">
          <div className="pc__table-header-search">
            <Input
              field={{
                label: "",
                type: "SEARCH",
                value: searchString,
                isRequired: false,
              }}
              width={3}
              placeholder="Search"
              name="searchString"
              onChange={handleSearchChange}
              className="pc__search"
            />
          </div>
          <div className="pc__table-header-btn-group">
            <SquareButton
              onClick={() => setShowModal(ModalType.FILTER)}
              content={
                <span className="img-text-in-btn">
                  <img alt="" src="/assets/icons/filter.png" />
                  <span>Filters</span>
                </span>
              }
              bsStyle={"primary"}
              className="pc-icon-btn"
            />
            <SquareButton
              onClick={handleDownloadTemplate}
              content={
                <span className="img-text-in-btn">
                  <img
                    src={"/assets/new-icons/copy.svg"}
                    alt="Downloading File"
                    className="copy-icon"
                  />
                  <span>Template</span>
                </span>
              }
              bsStyle={"primary"}
              className="pc-icon-btn"
            />
            <label className="btn square-btn btn-primary file-button pc-icon-btn">
              <span className="img-text-in-btn">
                <img
                  className={props.uploading ? "icon__loading" : ""}
                  src={
                    props.uploading
                      ? "/assets/icons/loading.gif"
                      : "/assets/icons/import.png"
                  }
                  alt="Upload File"
                />
                <span>Import</span>
                <input
                  type="file"
                  name="csvFile"
                  accept=".csv"
                  onChange={handleFileSelect}
                  onClick={(event: any) => {
                    event.target.value = null;
                  }}
                  disabled={props.uploading}
                />
              </span>
            </label>
            <SquareButton
              onClick={handleExportClick}
              content={
                <span className="img-text-in-btn">
                  <img
                    className={fetchingFile ? "icon__loading" : ""}
                    src={
                      fetchingFile
                        ? "/assets/icons/loading.gif"
                        : "/assets/icons/export.png"
                    }
                    alt="Downloading File"
                  />
                  <span>Export</span>
                </span>
              }
              bsStyle={"primary"}
              className="pc-icon-btn"
              disabled={fetchingFile || loading || rows.length === 0}
            />
            <SquareButton
              onClick={() => setShowModal(ModalType.CREATE_PRODUCT)}
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Add Product</span>
                </>
              }
              bsStyle={"link"}
              className="add-btn add-product-btn"
            />
          </div>
        </div>
        {renderFilters()}
      </div>
    );
  };

  const rowSelectionProps = {
    showCheckbox: false,
    selectIndex: "id",
    onRowsToggle: () => {},
  };

  const manualProps = {
    manual: true,
    pages: pagination.totalPages,
    onFetchData: fetchData,
    reset: pagination.reset,
    defaultPageSize: 25,
  };

  const columns: ITableColumn[] = [
    {
      accessor: "product_id",
      Header: "Product ID",
      width: 150,
      sortable: true,
    },
    {
      accessor: "name",
      Header: "Name",
      width: 250,
      sortable: true,
      Cell: (cell) => (
        <div className="name-desc-col" title={cell.value}>
          {cell.value}
        </div>
      ),
    },

    {
      accessor: "description",
      Header: "Description",
      sortable: true,
      Cell: (cell) => (
        <div className="name-desc-col" title={cell.value ? cell.value : ""}>
          {cell.value ? cell.value : ""}
        </div>
      ),
    },
    {
      accessor: "product_type",
      Header: "Product Type",
      width: 150,
      sortable: true,
    },
    {
      accessor: "created_on",
      Header: "Created On",
      id: "created_on",
      sortable: true,
      width: 110,
      Cell: (cell) => (
        <div>
          {`${cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."}`}
        </div>
      ),
    },
    {
      accessor: "updated_on",
      Header: "Updated On",
      id: "updated_on",
      sortable: true,
      width: 110,
      Cell: (cell) => (
        <div>
          {`${cell.value ? fromISOStringToFormattedDate(cell.value) : " N.A."}`}
        </div>
      ),
    },
    {
      accessor: "id",
      Header: "Actions",
      sortable: false,
      width: 110,
      Cell: (cell) => (
        <div className="pc-action-btns">
          <EditButton
            title="Edit Product"
            onClick={() => onEditClick(cell.value)}
          />
          <DeleteButton
            type="cloned"
            title="Clone Product"
            onClick={() => onCloneClick(cell.value)}
          />
          <SmallConfirmationBox
            className="delete-product"
            text="product"
            title="Delete Product"
            onClickOk={() => onDeleteRowClick(cell.value)}
            elementToCLick={<DeleteButton onClick={() => null} />}
          />
        </div>
      ),
    },
  ];

  return (
    <>
      <div className="product-catalog-container">
        <h2>Product Catalog</h2>
        <Spinner show={loading} className="pc-listing-loader" />
        <Table
          rows={rows}
          columns={columns}
          manualProps={manualProps}
          rowSelection={rowSelectionProps}
          customTopBar={renderTopBar()}
          className={`product-catalog-table`}
          loading={loading}
        />
      </div>
      {showModal === ModalType.CREATE_PRODUCT && (
        <CreateProduct
          productId={editProductId || cloneProductId}
          mode={editProductId ? "edit" : cloneProductId ? "clone" : "create"}
          onClose={onCloseCreateProduct}
        />
      )}
      <CatalogFilters
        show={showModal === ModalType.FILTER}
        onSubmit={applyFilters}
        filters={pagination.filters}
        onClose={() => setShowModal(null)}
      />
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  uploading: state.sales.isFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  fetchProductCatalog: (
    params: IServerPaginationParams & IProductCatalogFilters
  ) => dispatch(productCatalogCRUD("get", params)),
  uploadImportFile: (data: FormData) => dispatch(uploadProductImportCSV(data)),
  fetchExportFile: (params: IServerPaginationParams & IProductCatalogFilters) =>
    dispatch(fetchProductsToExport(params)),
  deleteProduct: (id: number) =>
    dispatch(
      productCatalogCRUD("delete", undefined, {
        id,
      } as IProductCatalogItem)
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCatalog);
