import React, { useEffect, useState } from "react";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import Input from "../../components/Input/input";
import "../../commonStyles/filtersListing.scss";
import "../../commonStyles/filterModal.scss";
import "./style.scss";

interface VRWarehouseFiltersProps {
  show: boolean;
  filters: IProductCatalogFilters;
  onClose: () => void;
  onSubmit: (filters: IProductCatalogFilters) => void;
}

export const CatalogFilters: React.FC<VRWarehouseFiltersProps> = (props) => {
  const [filters, setFilters] = useState<IProductCatalogFilters>({
    product_type: [],
  });

  useEffect(() => {
    setFilters(props.filters);
  }, [props.filters]);

  const onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    setFilters((prev) => ({
      ...prev,
      [targetName]: targetValue,
    }));
  };

  const onSubmit = () => {
    props.onSubmit(filters);
  };

  const getBody = () => {
    return (
      <div className="filters-modal__body pc-filter col-md-12">
        <Input
          field={{
            label: "Product Type",
            type: "PICKLIST",
            value: filters.product_type,
            options: [
              "Service Contract",
              "Consumption",
              "One Time Service",
            ],
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="product_type"
          onChange={onFilterChange}
          placeholder={`Select Type`}
        />
      </div>
    );
  };

  const getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={props.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  return (
    <ModalBase
      show={props.show}
      onClose={props.onClose}
      titleElement={"Filters"}
      bodyElement={getBody()}
      footerElement={getFooter()}
      className="filters-modal"
    />
  );
};
