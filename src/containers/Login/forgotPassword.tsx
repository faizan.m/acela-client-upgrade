import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import {
  FORGOT_PASSWORD_FAILURE,
  FORGOT_PASSWORD_SUCCESS,
  forgotPasswordRequest,
} from '../../actions/auth';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import { isValidVal } from '../../utils/CommonUtils';
import AppValidators from '../../utils/validator';

import { fetchLogoColor } from '../../actions/provider';
import Spinner from '../../components/Spinner';
import './style.scss';

interface IForgotPasswordProps extends ICommonProps {
  forgotPasswordRequest: any; // TODO:
  logo_url: string;
  themeColor: string;
  fetchLogoColor: any;
  updated_on: string;
}

interface IForgotPasswordState {
  email: string;
  isFormValid: boolean;
  showInfo: boolean;
  fetching: boolean;
  error: {
    email: IFieldValidation;
  };
  logo_url: string;
  themeColor: string;
}

class ForgotPassword extends React.Component<
  IForgotPasswordProps,
  IForgotPasswordState
> {
  constructor(props: IForgotPasswordProps) {
    super(props);

    this.state = {
      email: '',
      isFormValid: true,
      showInfo: false,
      fetching: false,
      logo_url: '',
      themeColor: '',
      error: {
        email: {
          errorState: "success",
          errorMessage: '',
        },
      },
    };
  }

  componentDidMount() {
    this.props.fetchLogoColor();
  }

  componentDidUpdate(prevProps: IForgotPasswordProps) {
    if (this.props.updated_on !== prevProps.updated_on) {
      this.setState({
        themeColor: this.props.themeColor,
        logo_url: `${this.props.logo_url}`,
      });
      if (isValidVal(this.props.themeColor)) {
        document.documentElement.style.setProperty(
          '--primary',
          this.props.themeColor
        );
      }
    }
  }
  

  clearValidationStatus() {
    const cleanState = {
      email: {
        errorState: "success",
        errorMessage: '',
      },
    };

    const newState: IForgotPasswordState = cloneDeep(this.state);
    newState.error = cleanState;

    this.setState(newState);
  }

  validateForm() {
    this.clearValidationStatus();
    const newState: IForgotPasswordState = cloneDeep(this.state);
    let isValid = true;

    if (!this.state.email) {
      newState.error.email.errorState = "error";
      newState.error.email.errorMessage = 'Email cannot be empty';

      isValid = false;
    }
    if (this.state.email && !AppValidators.isValidEmail(this.state.email)) {
      newState.error.email.errorState = "error";
      newState.error.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }

    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };

  submitEmail = e => {
    if (this.validateForm()) {
      this.setState({ fetching: true });

      this.props
        .forgotPasswordRequest({ email: this.state.email })
        .then(action => {
          if (action.type === FORGOT_PASSWORD_SUCCESS) {
            this.setState({
              showInfo: true,
              fetching: false,
            });
            setTimeout(() => this.props.history.push('/login'), 3000);
          } else if (action.type === FORGOT_PASSWORD_FAILURE) {
            this.clearValidationStatus();
            const newState: IForgotPasswordState = this.state;
            newState.error.email.errorState = "error";
            newState.error.email.errorMessage = action.errorList.data.email[0];
            newState.fetching = false;
            this.setState(newState);
          }
        });
    }
  };

  onKeyDown = e => {
    if (e.key === 'Enter') {
      this.submitEmail(e);
    }
  };

  render() {
    return (
      <div className="login-container login-container--forgot-password">
        <div className={this.state.fetching ? 'loader' : ''}>
          <Spinner show={this.state.fetching} />
        </div>
        <div className="login-form" onKeyDown={this.onKeyDown}>
          <div
            className="login-form__logo"
            style={{
              backgroundImage: `url(${
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              })`,
            }}
          >
            {/* <img
              src={
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}?${new Date().getTime()}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              }
            /> */}
          </div>
          <div className="login-form__fields">
            <h3>Forgot Password</h3>
            <Input
              field={{
                label: 'Email Address',
                type: "TEXT",
                isRequired: false,
                value: this.state.email,
              }}
              error={this.state.error.email}
              width={12}
              placeholder="Email"
              name="email"
              onChange={this.handleChange}
            />
          </div>
          <div className="login-form__actions">
            <SquareButton
              bsStyle={"primary"}
              content="Reset Password"
              onClick={this.submitEmail}
            />
            <Link to="/login">Back to Login Page</Link>
          </div>
          <label
            className={`login-form__info ${
              this.state.showInfo ? 'login-form__info--show' : ''
            }`}
          >
            Password reset email sent on the entered email
          </label>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  logo_url: state.provider.logo_url,
  themeColor: state.provider.themeColor,
  updated_on: state.provider.updated_on,
});

const mapDispatchToProps = (dispatch: any) => ({
  forgotPasswordRequest: (forgotRequest: IForgotPasswordRequest) =>
    dispatch(forgotPasswordRequest(forgotRequest)),
  fetchLogoColor: () => dispatch(fetchLogoColor()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ForgotPassword);
