import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { userLogOut } from '../../utils/AuthUtil';

import { addErrorMessage, addSuccessMessage } from '../../actions/appState';
import {
  logOutRequest,
  SET_PASSWORD_FAILURE,
  SET_PASSWORD_SUCCESS,
  setPasswordRequest,
  VALIDATE_LINK_FAILURE,
  validatePasswordRequest,
} from '../../actions/auth';
import { fetchLogoColor } from '../../actions/provider';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import { isValidVal } from '../../utils/CommonUtils';
import AppValidators from '../../utils/validator';
import Alert from '../Alert/alert';
import './style.scss';

interface IResetPasswordProps extends ICommonProps {
  resetPasswordRequest: TSetPasswordRequest;
  logOutRequest: TLogout;
  addSuccessMessage: TShowSuccessMessage;
  addErrorMessage: any;
  logo_url: string;
  themeColor: string;
  fetchLogoColor: any;
  updated_on: string;
  validatePasswordRequest: any;
}

interface IResetPasswordState {
  password: string;
  confirmPassword: string;
  isFormValid: boolean;
  fetching: boolean;
  logo_url: string;
  themeColor: string;
  error: {
    password: IFieldValidation;
    confirmPassword: IFieldValidation;
  };
}

class ResetPassword extends React.Component<
  IResetPasswordProps,
  IResetPasswordState
> {
  constructor(props: IResetPasswordProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    password: '',
    confirmPassword: '',
    isFormValid: true,
    fetching: false,
    logo_url: '',
    themeColor: '',
    error: {
      password: {
        errorState: "success",
        errorMessage: '',
      },
      confirmPassword: {
        errorState: "success",
        errorMessage: '',
      },
    },
  });
  componentDidMount() {
    this.props.logOutRequest();
    this.props.fetchLogoColor();
    this.validatePasswordRequest();
  }

  componentDidUpdate(prevProps: IResetPasswordProps) {
    if (prevProps.updated_on !== this.props.updated_on) {
      this.setState({
        themeColor: this.props.themeColor,
        logo_url: `${this.props.logo_url}`,
      });
      if (isValidVal(this.props.themeColor)) {
        document.documentElement.style.setProperty(
          '--primary',
          this.props.themeColor
        );
      }
    }
  }

  validatePasswordRequest() {
    const uid = this.props.match.params.uid;
    const token = this.props.match.params.token;
    this.props.validatePasswordRequest({ uid, token }).then(action => {
      if (action.type === VALIDATE_LINK_FAILURE) {
        this.setState({ fetching: false });
        this.props.addErrorMessage('Link expired.');
        setTimeout(() => this.props.history.push('/login'), 3000);
      }
    });
  }
  clearValidationStatus() {
    const cleanState = {
      password: {
        errorState: "success",
        errorMessage: '',
      },
      confirmPassword: {
        errorState: "success",
        errorMessage: '',
      },
    };

    const newState: IResetPasswordState = cloneDeep(this.state);
    newState.error = cleanState;

    this.setState(newState);
  }

  validateForm() {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.password) {
      error.password.errorState = "error";
      error.password.errorMessage = 'Password cannot be empty';

      isValid = false;
    }
    if (
      this.state.password &&
      !AppValidators.isValidPassword(this.state.password)
    ) {
      error.password.errorState = "error";
      // tslint:disable-next-line:max-line-length
      error.password.errorMessage = `Password should contain one or more lowercase, uppercase,
      numeric and special character. Length should be minimum 8 character.`;
      error.confirmPassword.errorState = "success";
      error.confirmPassword.errorMessage = '';

      isValid = false;
    }
    if (
      AppValidators.isValidPassword(this.state.password) &&
      this.state.password !== this.state.confirmPassword
    ) {
      error.confirmPassword.errorState = "error";
      error.confirmPassword.errorMessage = 'Passwords do not match';
      error.password.errorState = "success";
      error.password.errorMessage = '';
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };

  submitResetPassword = e => {
    const uid = this.props.match.params.uid;
    const token = this.props.match.params.token;
    userLogOut();
    if (this.validateForm()) {
      this.setState({ fetching: true });
      this.props
        .resetPasswordRequest({
          uid,
          token,
          new_password: this.state.password,
        })
        .then(action => {
          if (action.type === SET_PASSWORD_SUCCESS) {
            this.setState({ fetching: false });

            this.props.addSuccessMessage(
              'Password Set!! Redirecting to Login Page.'
            );
            setTimeout(() => this.props.history.push('/login'), 3000);
          }

          if (action.type === SET_PASSWORD_FAILURE) {
            this.clearValidationStatus();
            const newState: IResetPasswordState = this.state;
            newState.error.password.errorState = "error";
            newState.error.password.errorMessage =
              action.errorList.data.message[0];
            newState.fetching = false;
            this.setState(newState);
          }
        });
    }
  };

  onKeyDown = e => {
    if (e.key === 'Enter') {
      this.submitResetPassword(e);
    }
  };

  render() {
    const inputFieldType: any = 'password';

    return (
      <div className="login-container login-container--reset-password">
        <div className={this.state.fetching ? 'loader' : ''}>
          <Spinner show={this.state.fetching} />
        </div>
        <div className="login-form" onKeyDown={this.onKeyDown}>
          <div
            className="login-form__logo"
            style={{
              backgroundImage: `url(${
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              })`,
            }}
          >
            {/* <img
              src={
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}?${new Date().getTime()}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              }
            /> */}
          </div>
          <div className="login-form__fields">
            <h3>Set Password</h3>
            <Alert />
            <Input
              field={{
                label: 'Password',
                type: inputFieldType,
                isRequired: false,
                value: this.state.password,
              }}
              error={this.state.error.password}
              width={12}
              placeholder=""
              name="password"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: 'Confirm Password',
                type: inputFieldType,
                isRequired: false,
                value: this.state.confirmPassword,
              }}
              error={this.state.error.confirmPassword}
              width={12}
              placeholder=""
              name="confirmPassword"
              onChange={this.handleChange}
            />
          </div>
          <div className="login-form__actions login-form--set-btn">
            <SquareButton
              bsStyle={"primary"}
              content="Set Password"
              onClick={this.submitResetPassword}
            />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  logo_url: state.provider.logo_url,
  themeColor: state.provider.themeColor,
  updated_on: state.provider.updated_on,
});

const mapDispatchToProps = (dispatch: any) => ({
  resetPasswordRequest: (resetRequest: ISetPasswordRequest) =>
    dispatch(setPasswordRequest(resetRequest)),
  logOutRequest: () => dispatch(logOutRequest()),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchLogoColor: () => dispatch(fetchLogoColor()),
  validatePasswordRequest: (data: any) =>
    dispatch(validatePasswordRequest(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ResetPassword);
