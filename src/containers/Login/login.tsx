import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import {
  LOGIN_FAILURE,
  LOGIN_SUCCESS,
  loginRequest,
  loginSuccess,
} from '../../actions/auth';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import { getUserProfile, isLoggedIn } from '../../utils/AuthUtil';
import { getConvertedColorWithOpacity, isValidVal } from '../../utils/CommonUtils';

import { fetchLogoColor } from '../../actions/provider';
import Spinner from '../../components/Spinner';
import AppValidators from '../../utils/validator';
import './style.scss';

interface ILoginState {
  email: string;
  password: string;
  agencyId?: number;
  logo_url: string;
  themeColor: string;
  error: {
    email: IFieldValidation;
    password: IFieldValidation;
  };
  fetching: boolean;
}

interface ILoginProps extends ICommonProps {
  loginError: string;
  userProfile: IUserProfile;
  loginSuccess: TLoginSuccess;
  loginRequest: TLoginRequest;
  fetchLogoColor: any;
  logo_url: string;
  themeColor: string;
  updated_on: string;
}

class Login extends React.Component<ILoginProps, ILoginState> {
  constructor(props: ILoginProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    email: '',
    password: '',
    fetching: false,
    logo_url: '',
    themeColor: '',
    error: {
      email: {
        errorState: "success",
        errorMessage: '',
      },
      password: {
        errorState: "success",
        errorMessage: '',
      },
    },
  });

  componentDidMount() {
    this.props.fetchLogoColor();
    if (isLoggedIn()) {
      this.redirectUser();
    }
    // const colourCode = localStorage.getItem('colour_code');
    // if (isValidVal(colourCode)) {
    //   document.documentElement.style.setProperty('--primary', colourCode);
    // }
  }

  componentDidUpdate(prevProps: ILoginProps) {
    if (this.props.updated_on !== prevProps.updated_on) {
      this.setState({
        themeColor: this.props.themeColor,
        logo_url: `${this.props.logo_url}`,
      });
      if (isValidVal(this.props.themeColor)) {
        document.documentElement.style.setProperty(
          '--primary',
          this.props.themeColor
        );
        document.documentElement.style.setProperty(
          "--primary-light",
          getConvertedColorWithOpacity(this.props.themeColor)
        );
      }
    }
  }  

  redirectUser = () => {
    const userProfile: IUserProfile = getUserProfile();
    this.props.loginSuccess(userProfile);
    this.props.history.push(userProfile.default_landing_page  ||  '/dashboard');
  };

  validateForm() {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.email) {
      error.email.errorState = "error";
      error.email.errorMessage = 'Email cannot be empty';

      isValid = false;
    }

    if (!this.state.password) {
      error.password.errorState = "error";
      error.password.errorMessage = 'Password cannot be empty';

      isValid = false;
    }

    if (this.state.email && !AppValidators.isValidEmail(this.state.email)) {
      error.email.errorState = "error";
      error.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }

  loginFailed() {
    const error = this.getEmptyState().error;

    error.email.errorState = "error";
    error.password.errorState = "error";
    error.email.errorMessage = 'Email and password do not match';
    error.password.errorMessage = 'Email and password do not match';

    this.setState({ error });
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };

  login = () => {
    this.validateForm();

    if (this.validateForm()) {
      sessionStorage.removeItem('eyJ0eX1QiLCJ2fa');
      sessionStorage.removeItem('is_two_fa_enabled');
      this.setState({ fetching: true });
      this.props
        .loginRequest(this.state.email, this.state.password)
        .then(action => {
          if (action.type === LOGIN_SUCCESS) {
            this.setState({ fetching: false });
            this.redirectUser();
          } else if (action.type === LOGIN_FAILURE) {
            if (action.errorResponse && action.errorResponse.status === 412) {
              this.setState({ fetching: false });
              sessionStorage.setItem(
                'eyJ0eX1QiLCJ2fa',
                action.errorResponse.data && action.errorResponse.data.token
              );
              sessionStorage.setItem(
                'countryCode',
                action.errorResponse.data &&
                  action.errorResponse.data.country_code
              );
              sessionStorage.setItem(
                'phone',
                action.errorResponse.data && action.errorResponse.data.phone
              );
              sessionStorage.setItem(
                'is_two_fa_enabled',
                action.errorResponse.data &&
                  action.errorResponse.data.is_two_fa_enabled
              );
              this.props.history.push('/eyJ0eXAiOiJKdV1QiLCJ2fa');
            } else {
              this.setState({ fetching: false });
              this.loginFailed();
            }
          }
        });
    }
  };

  onKeyDown = e => {
    if (e.key === 'Enter') {
      this.login();
    }
  };

  render() {
    return (
      <div className="login-container">
        <div className={this.state.fetching ? 'loader' : ''}>
          <Spinner show={this.state.fetching} />
        </div>
        <div className="login-form" onKeyDown={this.onKeyDown}>
          <div
            className="login-form__logo"
            style={{
              backgroundImage: `url(${
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}`
                  : window.location.hostname === 'admin.acela.io'
                  ? '/assets/logo/logo.png'
                  : ''
              })`,
            }}
          >
            {/* <img
              src={
                isValidVal(this.state.logo_url)
                  ? `${this.state.logo_url}?${new Date().getTime()}`
                  : window.location.hostname === 'admin.acela.io'
                    ? '/assets/logo/logo.png'
                    : ''
              }
              alt="logo"
            /> */}
          </div>
          <div className="login-form__fields">
            <Input
              field={{
                label: 'Email Address',
                type: "TEXT",
                isRequired: false,
                value: this.state.email,
              }}
              error={this.state.error.email}
              width={12}
              placeholder="Email"
              name="email"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: 'Password',
                type: "PASSWORD",
                value: this.state.password,
              }}
              error={this.state.error.password}
              width={12}
              placeholder="Password"
              name="password"
              onChange={this.handleChange}
            />
          </div>
          <div className="login-form__actions">
            <SquareButton
              bsStyle={"primary"}
              content="Login"
              onClick={this.login}
            />
            <span>
              <a href="/forgot-password">Forgot Password?</a>
            </span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  userProfile: state.auth.userProfile,
  loginError: state.auth.error,
  logo_url: state.provider.logo_url,
  themeColor: state.provider.themeColor,
  updated_on: state.provider.updated_on,
});

const mapDispatchToProps = (dispatch: any) => ({
  loginRequest: (email: string, password: string) =>
    dispatch(loginRequest(email, password)),
  loginSuccess: (userProfile: IUserProfile) =>
    dispatch(loginSuccess(userProfile)),
  fetchLogoColor: () => dispatch(fetchLogoColor()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
