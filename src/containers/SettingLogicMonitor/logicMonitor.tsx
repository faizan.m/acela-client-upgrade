import { cloneDeep, flatten, uniq } from "lodash";
import React, { Component } from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import Select from "../../components/Input/Select/select";

import { ERROR, SHOW_SUCCESS_MESSAGE } from "../../actions/appState";
import {
  FETCH_GROUPS_CAT_SUCCESS,
  FETCH_GROUPS_SUCCESS,
  FETCH_INTEGRATIONS_CAT_SUCCESS,
  fetchDefaultKeyMapping,
  fetchDeviceMonitoringGroups,
  fetchIntegrationsCategories,
  fetchIntegrationsCategoriesTypes,
  fetchSystemCategoriesByGroupId,
  TEST_INTEGRATION_SUCCESS,
  testIntegration,
} from "../../actions/integration";
import { fetchManufacturers } from "../../actions/inventory";
import {
  CREATE_PROVIDER_INTEGRATIONS_FAILURE,
  CREATE_PROVIDER_INTEGRATIONS_SUCCESS,
  createProviderIntegrations,
  editProviderIntegrations,
  fetchAllProviderIntegrationsWithFilter,
  updateProviderIntegration,
} from "../../actions/provider/integration";
import SquareButton from "../../components/Button/button";
import DeleteButton from "../../components/Button/deleteButton";
import EditButton from "../../components/Button/editButton";
import Checkbox from "../../components/Checkbox/checkbox";
import Input from "../../components/Input/input";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";
import store from "../../store";
import { commonFunctions } from "../../utils/commonFunctions";
import SmallConfirmationBox from "../../components/SmallConfirmationBox/confirmation";
import "./style.scss";
import { fetchEncryptionPublicKey } from "../../actions/setting";
import { encryptTextUsingRSA } from "../../utils/CommonUtils";

interface ILogivMonitorState {
  isFormValid: boolean;
  error: {
    [fieldName: string]: IFieldValidation;
  };
  errorKeyMapping: {
    [fieldName: string]: IFieldValidation;
  };
  errorOther: {
    group_id: IFieldValidation;
    categories: IFieldValidation;
    cw_mnf_id: IFieldValidation;
    mapping: IFieldValidation;
  };
  errorConfigNames: IFieldValidation;
  isFetching: boolean;
  errorList?: any;
  deviceInfoId?: number;
  integrations: any;
  providerIntegration: IProviderIntegration;
  editedItemIndex: any;
  isEdit: boolean;
  isEditPopUp: boolean;
  showNotification: boolean;
  integrationVerified: boolean;
  integrationsCatgories?: any;
  isVerificationSuccess: boolean;
  allProviderIntegrations: any;
  selectedIntegrationFields: any;
  selectedIntegration: any;
  openAddAccount: boolean;
  connectwiseManufacturer: any;
  rows: any;
  CRMId?: number;
  disableConnectwiseManufacturer: boolean;
  groupList?: any;
  groupCategoryList?: any;
  data: {
    group_id: "";
    system_categories?: Array<{ name: string; value: any; active: boolean }>;
  };
  integrationObject?: any;
  mappingList: ILogicMonitorMapping[];
  mappingObject: ILogicMonitorMapping;
  noNotification: boolean;
  index: number;
  config_names: any[];
  loading: boolean;
}

interface ILogivMonitorProps extends ICommonProps {
  publicKey: string;
  fetchPublicKey: () => Promise<any>;
  fetchIntegrationsCategories: TFetchIntegrationsCategories;
  fetchIntegrationsCategoriesTypes: TFetchIntegrationsCategoriesTypes;
  providerIntegration: IProviderIntegration;
  fetchAllProviderIntegrationsWithFilter: any; // TODO;
  isFetching: number;
  integrations: IIntegrationType[];
  allProviderIntegrations: IProviderIntegration[];
  testIntegration: TTestIntegration;
  createProviderIntegrations: TCreateProviderIntegrations;
  editProviderIntegrations: any;
  fetchManufacturers: TFetchManufacturers;
  manufacturers: any[];
  fetchManufacturerIntegrations: TFetchIntegrationsCategoriesTypes;
  updateProviderIntegration: any; // TODO;
  fetchDeviceMonitoringGroups: any;
  fetchSystemCategoriesByGroupId: any;
  isFetchingGroupList: boolean;
  fetchDefaultKeyMapping: any;
  defaultKeyMapping: any;
}

class LogicMonitor extends Component<ILogivMonitorProps, ILogivMonitorState> {
  static emptyState: ILogivMonitorState = {
    integrations: [],
    isFormValid: false,
    error: {},
    errorKeyMapping: {},
    errorList: null,
    isFetching: true,
    deviceInfoId: null,
    providerIntegration: null,
    showNotification: false,
    integrationVerified: false,
    isVerificationSuccess: false,
    editedItemIndex: null,
    allProviderIntegrations: [],
    isEdit: true,
    selectedIntegrationFields: [],
    selectedIntegration: "",
    connectwiseManufacturer: "",
    openAddAccount: false,
    rows: [],
    isEditPopUp: false,
    disableConnectwiseManufacturer: false,
    groupList: [],
    groupCategoryList: [],
    data: {
      group_id: "",
      system_categories: [],
    },
    errorOther: {
      group_id: {
        errorState: "success",
        errorMessage: "",
      },
      categories: {
        errorState: "success",
        errorMessage: "",
      },
      cw_mnf_id: {
        errorState: "success",
        errorMessage: "",
      },
      mapping: {
        errorState: "success",
        errorMessage: "",
      },
    },
    mappingList: [],
    mappingObject: {
      group_ids: null,
      system_categories: [],
      cw_mnf_name: "",
      cw_mnf_id: null,
      key_mappings: null,
      non_cisco_devices: "cisco_devices_key_mappings",
    },
    config_names: [],
    errorConfigNames: {
      errorState: "success",
      errorMessage: "",
    },
    noNotification: false,
    index: null,
    loading: false,
  };

  constructor(props: any) {
    super(props);
    this.state = cloneDeep(LogicMonitor.emptyState);
  }
  componentDidMount() {
    this.props.fetchPublicKey();
    this.props.fetchManufacturers();
    this.props.fetchDefaultKeyMapping();
    this.props.fetchIntegrationsCategories().then((action) => {
      if (action.type === FETCH_INTEGRATIONS_CAT_SUCCESS) {
        const deviceInfoId = action.response.findIndex(
          (x) => x.name === "DEVICE_MONITOR"
        );
        this.setState({ deviceInfoId: action.response[deviceInfoId].id });
        this.props.fetchIntegrationsCategoriesTypes(this.state.deviceInfoId);
      }
    });
  }

  componentDidUpdate(prevProps: ILogivMonitorProps) {
    const { integrations, allProviderIntegrations } = this.props;

    if (integrations && integrations !== prevProps.integrations) {
      this.setState({
        integrations,
      });

      this.props.fetchAllProviderIntegrationsWithFilter(
        "DEVICE_MONITOR"
      );
    }

    if (
      allProviderIntegrations &&
      allProviderIntegrations !== prevProps.allProviderIntegrations
    ) {
      this.setState(
        {
          allProviderIntegrations,
        },
        this.loadData
      );

      this.setRows(this.props);
    }

    // check this
    // if (
    //   publicKey &&
    //   publicKey !== prevProps.publicKey &&
    //   allProviderIntegrations.length
    // ) {
    //   this.testData();
    // }
  }

  setRows = (nextProps: ILogivMonitorProps) => {
    const allProviderIntegrations = nextProps.allProviderIntegrations;

    const rows = allProviderIntegrations
      .filter((user) => user.category === "DEVICE_MONITOR")
      .map((user, index) => ({
        type: user.type,
        authentication_info: user.authentication_info,
        id: user.id,
        index,
      }));
    const deviceMonitorIntegration = allProviderIntegrations.filter(
      (user) => user.category === "DEVICE_MONITOR"
    );
    const otheConfig =
      deviceMonitorIntegration &&
      deviceMonitorIntegration.length > 0 &&
      deviceMonitorIntegration[0].other_config.device_mappings;

    const mappingList = flatten(Object.values(otheConfig)) as any;

    // tslint:disable-next-line: no-string-literal
    let configNames = [];
    if (
      deviceMonitorIntegration &&
      deviceMonitorIntegration.length > 0 &&
      deviceMonitorIntegration[0].other_config.config_mappings &&
      deviceMonitorIntegration[0].other_config.config_mappings.config_names
    ) {
      configNames =
        deviceMonitorIntegration[0].other_config.config_mappings.config_names;
    }
    this.setState({ mappingList, rows, config_names: configNames });
  };

  setEditStateAdd = () => {
    const selectedIntegrationFields = cloneDeep(
      this.state.selectedIntegrationFields
    );
    selectedIntegrationFields.authentication_config.access_key.value = "";
    this.setState({
      selectedIntegrationFields,
      showNotification: false,
      editedItemIndex: null,
      isEdit: true,
      integrationVerified: false,
      integrationObject: null,
      noNotification: false,
    });
  };

  addAccount = () => {
    const emptyState = cloneDeep(LogicMonitor.emptyState);

    emptyState.mappingObject.key_mappings = this.props.defaultKeyMapping[
      emptyState.mappingObject.non_cisco_devices
    ];
    this.setState({
      openAddAccount: !this.state.openAddAccount,
      mappingObject: emptyState.mappingObject,
      groupCategoryList: [],
      errorOther: emptyState.errorOther,
      errorKeyMapping: emptyState.errorKeyMapping,
      index: null,
    });
  };

  onCancelAccount = () => {
    this.setState({
      showNotification: false,
      openAddAccount: false,
    });
  };

  renderNotification = () => {
    const { showNotification, isVerificationSuccess } = this.state;
    const onNotificationClick = () =>
      this.setState({
        showNotification: false,
      });

    if (showNotification && !this.state.noNotification) {
      return (
        <div
          onClick={onNotificationClick}
          className={`settings__notification ${
            isVerificationSuccess
              ? "settings__notification--success"
              : "settings__notification--fail"
          }`}
        >
          {isVerificationSuccess ? "Test success. Please Save." : "Test Failed"}
        </div>
      );
    } else {
      return null;
    }
  };

  getManufacturer = () => {
    let manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map((item) => ({
          value: item.id,
          label: item.label,
          disabled: false,
        }))
      : [];

    manufacturers = manufacturers.map((data) => ({
      value: data.value,
      label: data.label,
      disabled: this.isManufacturerDisabled(data.value),
    }));

    return manufacturers;
  };

  isManufacturerDisabled = (manufacturerId) => {
    let disabled = false;
    const selectedGroups = this.state.mappingList.filter(
      (e) => this.state.mappingObject.group_ids === e.group_ids
    );
    disabled = !!selectedGroups.find((x) => x.cw_mnf_id === manufacturerId);
    return disabled;
  };

  handleSelectManufacturerFromConnectwise = (event: any) => {
    const targetValue = event.target.value;

    this.setState(() => ({
      connectwiseManufacturer: targetValue,
    }));
  };

  handleChangeAdd = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const selectedIntegrationFields = this.state.selectedIntegrationFields;
    selectedIntegrationFields.authentication_config[
      targetName
    ].value = targetValue;

    this.setState(() => ({
      selectedIntegrationFields: cloneDeep(selectedIntegrationFields),
    }));
  };

  handleChangeKeyMapping = (event: any, name) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject.key_mappings[name].value = event;
    this.setState(newState);
  };

  isValidAdd = () => {
    const errorOther = cloneDeep(LogicMonitor.emptyState.errorOther);
    const errorConfigNames = cloneDeep(
      LogicMonitor.emptyState.errorConfigNames
    );

    const selectedIntegrationFields = this.state.selectedIntegrationFields
      .authentication_config;
    let isValid = true;

    const error = Object.keys(selectedIntegrationFields).reduce(
      (errorObj, fieldName) => {
        const attributes = selectedIntegrationFields[fieldName];

        if (attributes.is_required && attributes.value === "") {
          isValid = false;

          errorObj[fieldName] = {
            errorState: "error",
            errorMessage: `${attributes.label} is required`,
          };
        }

        return errorObj;
      },
      {}
    );

    if (
      this.state.integrationVerified &&
      this.state.mappingList &&
      this.state.mappingList.length === 0
    ) {
      errorOther.mapping.errorState = "error";
      errorOther.mapping.errorMessage = "Please add one mapping.";
      isValid = false;
    }
    if (
      this.state.integrationVerified &&
      this.state.config_names &&
      this.state.config_names.length === 0
    ) {
      errorConfigNames.errorState = "error";
      errorConfigNames.errorMessage = "Please add one config name.";
      isValid = false;
    }
    this.setState({
      error,
      errorOther,
      errorConfigNames,
    });

    return isValid;
  };

  isValidExtraConfig = () => {
    const errorOther = cloneDeep(LogicMonitor.emptyState.errorOther);
    let errorKeyMapping = cloneDeep(LogicMonitor.emptyState.errorKeyMapping);

    let isValid = true;

    if (this.state.mappingObject && !this.state.mappingObject.group_ids) {
      errorOther.group_id.errorState = "error";
      errorOther.group_id.errorMessage = "Please select Group.";
      isValid = false;
    }
    if (this.state.mappingObject && !this.state.mappingObject.cw_mnf_id) {
      errorOther.cw_mnf_id.errorState = "error";
      errorOther.cw_mnf_id.errorMessage = "Please select Manufacturer.";
      isValid = false;
    }

    if (
      this.state.mappingObject &&
      this.state.mappingObject.group_ids &&
      this.state.groupCategoryList.length > 0 &&
      this.state.mappingObject.system_categories.filter(
        (x) => x.active === true
      ).length === 0
    ) {
      errorOther.categories.errorState = "error";
      errorOther.categories.errorMessage = "Please select one category";
      isValid = false;
    }

    errorKeyMapping =
      this.state.mappingObject &&
      this.state.mappingObject.key_mappings &&
      Object.keys(this.state.mappingObject.key_mappings).reduce(
        (errorObj, fieldName) => {
          const attributes = this.state.mappingObject.key_mappings[fieldName];

          if (attributes.value && attributes.value.length === 0) {
            isValid = false;

            errorObj[fieldName] = {
              errorState: "error",
              errorMessage: `${attributes.name} is required`,
            };
          }

          return errorObj;
        },
        {}
      );

    this.setState({ errorKeyMapping, errorOther });

    return isValid;
  };
  onTestWithNotification = () => {
    this.setState(
      {
        noNotification: false,
      },
      this.onTestAdd(true)
    );
  };

  onTestAdd = (encryptKey: boolean) => {
    if (!this.isValidAdd()) {
      return null;
    }
    this.setState({
      integrationVerified: false,
      showNotification: false,
      isVerificationSuccess: false,
      isEdit: false,
    });

    const integration = this.state.selectedIntegrationFields;
    const typeId = integration.id;
    const authenticationConfig = integration.authentication_config;
    const integrationObject: any = Object.keys(authenticationConfig).reduce(
      (accumulator, key) => {
        accumulator[key] = authenticationConfig[key].value;

        return accumulator;
      },
      {}
    ); // tslint:disable-line

    if (encryptKey)
      integrationObject.access_key = encryptTextUsingRSA(
        integrationObject.access_key,
        this.props.publicKey
      );

    this.props
      .testIntegration(this.state.deviceInfoId, typeId, integrationObject)
      .then((action) => {
        if (action.type === TEST_INTEGRATION_SUCCESS) {
          const verified = action.response.verified;

          this.setState({
            integrationVerified: verified,
            showNotification: true,
            isVerificationSuccess: verified,
            isEdit: !verified,
            integrationObject,
          });
          if (verified) {
            this.props
              .fetchDeviceMonitoringGroups(integrationObject)
              .then((a) => {
                if (a.type === FETCH_GROUPS_SUCCESS) {
                  this.setState({ groupList: a.response });
                }
              });

            if (this.state.data && this.state.data.group_id) {
              this.props
                .fetchSystemCategoriesByGroupId(
                  this.state.data.group_id,
                  this.state.integrationObject
                )
                .then((a) => {
                  if (a.type === FETCH_GROUPS_CAT_SUCCESS) {
                    const options = Object.entries(a.response).map(
                      ([key, name]) => ({
                        key,
                        name,
                        active: false,
                        value: [],
                        not_value: [],
                      })
                    );

                    this.setState({
                      groupCategoryList: this.mergeArrays(
                        this.state.groupCategoryList,
                        options,
                        "name"
                      ),
                    });
                  }
                });
            }
          }
        }
      });
  };

  mergeArrays = (array1, array2, prop) => {
    return array2.map((item2) => {
      const item1 = array1.find((i) => {
        return i[prop] === item2[prop];
      });

      return Object.assign({}, item2, item1);
    });
  };

  onSaveAdd = () => {
    if (!this.isValidAdd()) {
      return null;
    }
    const integration = this.state.selectedIntegrationFields;
    const typeId = integration.id;
    const authenticationConfig = integration.authentication_config;
    const integrationObject: any = Object.keys(authenticationConfig).reduce(
      (accumulator, key) => {
        accumulator[key] = authenticationConfig[key].value;

        return accumulator;
      },
      {}
    ); // tslint:disable-line

    integrationObject.access_key = encryptTextUsingRSA(
      integrationObject.access_key,
      this.props.publicKey
    );

    const deviceMappings = this.state.mappingList.reduce(function(r, a) {
      r[a.cw_mnf_id] = r[a.cw_mnf_id] || [];
      r[a.cw_mnf_id].push(a);
      return r;
    }, Object.create(null));

    const providerIntegration: any = {
      integration_type: typeId,
      authentication_info: integrationObject,
      other_config: {
        device_mappings: deviceMappings,
        config_mappings: {
          config_names: this.state.config_names,
        },
      },
    };

    const allProviderIntegrations = this.props.allProviderIntegrations.filter(
      (user) => user.category === "DEVICE_MONITOR"
    );
    const otherConfig =
      allProviderIntegrations &&
      allProviderIntegrations.length > 0 &&
      allProviderIntegrations[0].other_config;

    if (otherConfig) {
      const providerId = allProviderIntegrations[0].id;
      this.props
        .editProviderIntegrations(providerId, providerIntegration)
        .then((action) => {
          if (action.type === CREATE_PROVIDER_INTEGRATIONS_SUCCESS) {
            store.dispatch({
              type: SHOW_SUCCESS_MESSAGE,
              message: "Account Updated Successfully!",
            });
            this.setState({
              integrationVerified: false,
              showNotification: false,
              isVerificationSuccess: false,
              openAddAccount: false,
              selectedIntegrationFields: [],
              selectedIntegration: "",
              integrationObject: null,
              isEdit: true,
            });
            this.props.fetchAllProviderIntegrationsWithFilter(
              "DEVICE_MONITOR"
            );
          } else if (action.type === CREATE_PROVIDER_INTEGRATIONS_FAILURE) {
            store.dispatch({
              type: ERROR,
              errorMessage: action.errorList.data.integration_type[0],
            });
          }
        });
    } else {
      this.props
        .createProviderIntegrations(providerIntegration)
        .then((action) => {
          if (action.type === CREATE_PROVIDER_INTEGRATIONS_SUCCESS) {
            store.dispatch({
              type: SHOW_SUCCESS_MESSAGE,
              message: "Account added Successfully!",
            });
            this.setState({
              integrationVerified: false,
              showNotification: false,
              isVerificationSuccess: false,
              openAddAccount: false,
              selectedIntegrationFields: [],
              selectedIntegration: "",
              isEdit: true,
            });
            this.props.fetchAllProviderIntegrationsWithFilter(
              "DEVICE_MONITOR"
            );
          } else if (action.type === CREATE_PROVIDER_INTEGRATIONS_FAILURE) {
            store.dispatch({
              type: ERROR,
              errorMessage: action.errorList.data.integration_type[0],
            });
          }
        });
    }
  };

  toggleNewModal = () => {
    this.setState({ openAddAccount: !this.state.openAddAccount });
  };

  renderPopUpFooter = () => {
    return (
      <div className="device-manufacturer__body-actions">
        <SquareButton
          onClick={(e) => this.saveMapping()}
          content={this.state.index === null ? "Add" : "Update"}
          bsStyle={"primary"}
        />
        <SquareButton
          content="Cancel"
          onClick={this.onCancelAccount}
          bsStyle={"default"}
        />
      </div>
    );
  };
  renderFooter = () => {
    return (
      <div className="device-manufacturer__body-actions">
        {!this.state.integrationVerified &&
          this.state.isEdit &&
          this.state.selectedIntegrationFields &&
          this.state.selectedIntegrationFields.authentication_config && (
            <SquareButton
              onClick={() => this.onTestWithNotification()}
              content="Test"
              bsStyle={"primary"}
            />
          )}
        {!this.state.isEdit && this.state.integrationVerified && (
          <SquareButton
            onClick={() => this.onSaveAdd()}
            content="Save"
            bsStyle={"primary"}
          />
        )}

        {this.state.openAddAccount && (
          <SquareButton
            content="Cancel"
            onClick={this.onCancelAccount}
            bsStyle={"default"}
          />
        )}
      </div>
    );
  };

  renderAddDeviceIntegration = () => {
    const selectedIntegrationFields =
      this.state.selectedIntegrationFields &&
      this.state.selectedIntegrationFields.authentication_config;

    return (
      <div className="integration-list integration-list__add">
        {(this.props.isFetching > 0 || this.props.isFetchingGroupList) && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div
          className={
            this.state.isEdit
              ? "device-manufacturer__body-fields"
              : "device-manufacturer__body-fields disable-div"
          }
        >
          {this.renderNotification()}

          <div className="device-fields-group">
            {selectedIntegrationFields &&
              Object.keys(selectedIntegrationFields).map((key, fieldIndex) => {
                const name = key;
                const attributes = selectedIntegrationFields[key];

                return (
                  <Input
                    key={fieldIndex}
                    field={{
                      label: attributes.label,
                      type: attributes.type,
                      isRequired: attributes.is_required,
                      value: attributes.value,
                      options: attributes.options,
                    }}
                    width={6}
                    disabled={JSON.parse(attributes.disabled) ? true : false}
                    placeholder={`Enter ${attributes.label}`}
                    name={name}
                    onChange={(event) => this.handleChangeAdd(event)}
                    error={this.state.error[name]}
                  />
                );
              })}
          </div>
        </div>
      </div>
    );
  };

  manufacturerMapPopUp = () => {
    return (
      <div className="integration-list integration-list__add">
        {(this.props.isFetching > 0 ||
          this.props.isFetchingGroupList ||
          this.state.loading) && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}

        {this.props.isFetching > 0 && (
          <div className="loader">
            <Spinner show={this.props.isFetchingGroupList} />
          </div>
        )}

        <div className="extra-cofig">
          <div className="manufacturer-group-box">
            <div
              className={`select-input
               field-section  col-md-12 col-xs-12`}
            >
              <div className="field__label row">
                <label className="field__label-label" title="">
                  Select Group
                </label>
                <span className="field__label-required" />
              </div>
              <div
                className={`${
                  this.state.errorOther.group_id.errorMessage
                    ? `error-input`
                    : ""
                }`}
              >
                <Select
                  name="group_ids"
                  value={this.state.mappingObject.group_ids}
                  onChange={this.handleChange}
                  options={this.getGroups()}
                  searchable={true}
                  placeholder="Select Group"
                  clearable={false}
                  multi={true}
                />
              </div>
              {this.state.errorOther.group_id.errorMessage && (
                <div className="select-error">
                  {this.state.errorOther.group_id.errorMessage}
                </div>
              )}
            </div>
            <div
              className="select-input
            field-section arrow col-md-12 col-xs-12"
            >
              <div className="select-div-1">
                <label className="manufacturer-label" htmlFor="">
                  Manufacturer
                </label>
                <span className="field__label-required" />
                <div
                  className={`${
                    this.state.errorOther.cw_mnf_id.errorMessage
                      ? `error-input`
                      : ""
                  }`}
                >
                  <Select
                    name="cw_mnf_id"
                    value={this.state.mappingObject.cw_mnf_id}
                    options={this.getManufacturer()}
                    isOptionDisabled={(option) => option.disabled === true}
                    onChange={this.handleChangeManufacturer}
                    disabled={
                      this.state.mappingObject.group_ids &&
                      this.state.mappingObject.group_ids.length === 0
                    }
                  />
                </div>
              </div>
              {this.state.errorOther.cw_mnf_id.errorMessage && (
                <div className="select-error">
                  {this.state.errorOther.cw_mnf_id.errorMessage}
                </div>
              )}
            </div>
            <Checkbox
              isChecked={
                this.state.mappingObject.non_cisco_devices ===
                "non_cisco_devices_key_mappings"
                  ? true
                  : false
              }
              name={"non_cisco_devices"}
              onChange={(e) => this.onCheckboxChangedNonCiscoDevice(e)}
              disabled={false}
              className="non-cisco-device"
            >
              Non Cisco Devices
            </Checkbox>
          </div>

          {this.state.mappingObject.system_categories &&
            this.renderCategories(this.state.mappingObject.system_categories)}
        </div>
        <div className="key-mappings">
          <label className="heading" title="">
            Key Mappings:
          </label>
          <div className="key-mappings-inner row">
            {this.state.mappingObject &&
              this.state.mappingObject.key_mappings &&
              Object.keys(this.state.mappingObject.key_mappings).map(
                (key, fieldIndex) => {
                  const name = key;
                  const attributes = this.state.mappingObject.key_mappings[key];

                  return (
                    <div
                      className="col-md-6 col-xs-6 key-tags"
                      key={fieldIndex}
                    >
                      <div className="field__label row">
                        <label className="field__label-label" title="">
                          {attributes.name}
                        </label>
                        <span className="field__label-required" />
                      </div>
                      {/* <TagsInput
                        value={attributes.value}
                        onChange={(e) => this.handleChangeKeyMapping(e, name)}
                        inputProps={{
                          className: "react-tagsinput-input",
                          placeholder: "Enter Value",
                        }}
                        addOnBlur={true}
                      /> */}
                      {this.state.errorKeyMapping[name] && (
                        <div className="show-error">
                          {this.state.errorKeyMapping[name] &&
                            this.state.errorKeyMapping[name].errorMessage}
                        </div>
                      )}
                    </div>
                  );
                }
              )}
          </div>
        </div>
        <div className="category-inputs">
          {this.state.mappingObject.system_categories &&
            this.state.mappingObject.system_categories
              .filter((x) => x.active === true)
              .map((data, index) => {
                return (
                  <div
                    key={index}
                    className="email-tag field-section
             col-md-12 col-xs-12"
                  >
                    <div className="field__label row">
                      <label className="field__label-label heading" title="">
                        {data.name}:
                      </label>
                      <span className="field__label-required" />
                    </div>
                    <div className="contains-not-contains">
                      <div className="col-md-6">
                        <div className="field__label row">
                          <label className="field__label-label" title="">
                            Contains
                          </label>
                        </div>
                        {/* <TagsInput
                          value={
                            this.state.mappingObject.system_categories[
                              this.state.mappingObject.system_categories
                                .map((e) => e.name)
                                .indexOf(data.name)
                            ].value
                          }
                          onChange={(e) =>
                            this.handleChangeRegex(e, index, data.name)
                          }
                          inputProps={{
                            className: "react-tagsinput-input",
                            placeholder: "Enter Value",
                          }}
                          addOnBlur={true}
                        /> */}
                      </div>
                      <div className="col-md-6">
                        <div className="field__label row">
                          <label className="field__label-label" title="">
                            Not Contains
                          </label>
                        </div>
                        {/* <TagsInput
                          value={
                            this.state.mappingObject.system_categories[
                              this.state.mappingObject.system_categories
                                .map((e) => e.name)
                                .indexOf(data.name)
                            ].not_value
                          }
                          onChange={(e) =>
                            this.handleChangeNotRegex(e, index, data.name)
                          }
                          inputProps={{
                            className: "react-tagsinput-input",
                            placeholder: "Enter Value",
                          }}
                          addOnBlur={true}
                        /> */}
                      </div>{" "}
                    </div>
                  </div>
                );
              })}
        </div>
      </div>
    );
  };

  renderMappingList = () => {
    const mappingList = this.state.mappingList;

    return (
      <div className="col-md-12 mapping-list">
        <SquareButton
          onClick={() => this.addAccount()}
          content="+ Add New"
          bsStyle={"primary"}
          className="add-new-mapping"
        />
        <h4>Mappings</h4>

        {mappingList &&
          mappingList.map(
            (data, index) =>
              data.cw_mnf_id && (
                <div className="row mapping-row" key={index}>
                  <div className="left-panel field-section arrow col-md-12">
                    <div className="text">
                      <div className="label-row">
                        Manufacturer:
                        <div className="bold-heading"> {data.cw_mnf_name}</div>
                      </div>
                      <div className="separator">|</div>
                      <div className="label-row">
                        Group:{" "}
                        <div className="bold-heading">
                          {data.group_ids
                            .map((y) =>
                              commonFunctions.getKeyByValue(
                                this.state.groupList,
                                y
                              )
                            )
                            .join(", ")}
                        </div>
                      </div>
                    </div>
                    <div className="actions">
                      <EditButton
                        onClick={(e) => this.onEditRowClick(e, data, index)}
                      />
                      <SmallConfirmationBox
                        onClickOk={() => this.onDeleteRowClick(index)}
                        text={"mapping"}
                        elementToCLick={<DeleteButton onClick={null} />}
                        className="remove-device-mapping"
                      />
                    </div>
                  </div>
                  <div
                    className="field-section
                   arrow col-md-12 box-body"
                  >
                    <div className="system_categories">
                      {data.system_categories &&
                        data.system_categories.map((option, i) => {
                          return (
                            <div
                              key={i + Math.random()}
                              className="category col-md-3"
                            >
                              <div className="category-inner">
                                <div className="box-heading">
                                  Category:{" "}
                                  <span className="sub-head">
                                    {option.name}
                                  </span>
                                </div>

                                <div className="bottom">
                                  <div className="contains box">
                                    <div className="regex-heading">
                                      Contains{" "}
                                    </div>
                                    {/* <TagsInput
                                      value={option.value}
                                      onChange={(e) => null}
                                      inputProps={{
                                        className: "react-tagsinput-input",
                                        placeholder: "",
                                      }}
                                      addOnBlur={false}
                                      disabled={true}
                                    /> */}
                                  </div>
                                  <div className="not-contains box">
                                    <div className="regex-heading">
                                      Not Contains{" "}
                                    </div>
                                    {/* <TagsInput
                                      value={option.not_value}
                                      onChange={(e) => null}
                                      inputProps={{
                                        className: "react-tagsinput-input",
                                        placeholder: "",
                                      }}
                                      addOnBlur={false}
                                      disabled={true}
                                    /> */}
                                  </div>
                                </div>
                              </div>
                            </div>
                          );
                        })}

                      {data.system_categories &&
                        data.system_categories.length === 0 && (
                          <div className="category no-category col-md-4">
                            No Category Available
                          </div>
                        )}
                    </div>
                  </div>
                </div>
              )
          )}
        {mappingList.length === 0 && (
          <div className="no-data"> No Mappings Available</div>
        )}
        {this.state.errorOther.mapping.errorMessage && (
          <div className="select-error">
            {this.state.errorOther.mapping.errorMessage}
          </div>
        )}
      </div>
    );
  };

  renderConfigName = () => {
    return (
      <div className="col-md-12 config-names">
        <h4>Config Names</h4>
        {/* <TagsInput
          value={this.state.config_names}
          onChange={(e) => this.onChangeConfigNames(e)}
          inputProps={{
            className: "react-tagsinput-input",
            placeholder: "",
          }}
          addOnBlur={true}
          disabled={false}
        /> */}
        {this.state.errorConfigNames.errorMessage && (
          <div className="select-error">
            {this.state.errorConfigNames.errorMessage}
          </div>
        )}
      </div>
    );
  };
  onChangeConfigNames = (e) => {
    const newState = cloneDeep(this.state);
    let list = e;
    list = list.filter((value, index, self) => self.indexOf(value) === index);
    (newState.config_names as any) = list;

    this.setState(newState);
  };
  getAllData = async (object: any) => {
    const emptyState = cloneDeep(LogicMonitor.emptyState);
    const list = object.group_ids;
    const currentState = cloneDeep(this.state);
    const data = list.map((x) =>
      this.props.fetchSystemCategoriesByGroupId(x, this.state.integrationObject)
    );
    let promiseList: any = await await Promise.all(data);
    promiseList = promiseList.map((x) => x.response);
    promiseList = uniq(flatten(promiseList));
    const options = Object.entries(promiseList).map(([key, name]) => ({
      key,
      name,
      active: false,
      value: [],
      not_value: [],
    }));
    if (promiseList) {
      (currentState.loading as any) = false;
    }

    (currentState.groupCategoryList as any) = this.mergeArrays(
      object.system_categories,
      options,
      "name"
    );
    (currentState.mappingObject.system_categories as any) = this.mergeArrays(
      object.system_categories,
      options,
      "name"
    );
    (currentState.errorOther as any) = emptyState.errorOther;
    this.setState(currentState);
  };

  onEditRowClick = (e, data, index) => {
    const newState = cloneDeep(this.state);
    const emptyState = cloneDeep(LogicMonitor.emptyState);
    (newState.loading as any) = true;
    (newState.mappingObject as any) = data;
    (newState.errorKeyMapping as any) = emptyState.errorKeyMapping;
    (newState.openAddAccount as boolean) = true;
    (newState.index as number) = index;
    if (data && !data.key_mappings) {
      (newState.mappingObject
        .key_mappings as number) = this.props.defaultKeyMapping[
        this.state.mappingObject.non_cisco_devices
      ];
    }
    this.setState(newState, () => {
      this.getAllData(data);
    });
  };

  onDeleteRowClick = (index) => {
    const newState = cloneDeep(this.state);
    newState.mappingList.splice(index, 1);
    this.setState(newState);
  };

  saveMapping = () => {
    const newState = cloneDeep(this.state);
    const mappingObject = this.state.mappingObject;
    // tslint:disable-next-line:max-line-length

    if (this.isValidExtraConfig()) {
      mappingObject.key_mappings = this.state.mappingObject.key_mappings;
      // tslint:disable-next-line:max-line-length
      mappingObject.system_categories = this.state.mappingObject.system_categories.filter(
        (x) => x.active === true
      );
      if (this.state.index !== null) {
        newState.mappingList[this.state.index] = mappingObject;
      } else {
        newState.mappingList.push(mappingObject);
      }

      (newState.openAddAccount as boolean) = false;
      (newState.index as number) = null;
      this.setState(newState);
    }
  };

  getGroups = () => {
    const groupList = this.state.groupList && Object.keys(this.state.groupList);
    if (groupList && groupList.length > 0) {
      return groupList.map((a) => ({
        value: this.state.groupList[a],
        label: a,
      }));
    } else {
      return [];
    }
  };

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject[event.target.name] = event.target.value;
    (newState.loading as any) = true;

    this.setState(newState, () => {
      this.getAllData({
        group_ids: event.target.value,
        system_categories: this.state.mappingObject.system_categories,
      });
    });
  };

  handleChangeManufacturer = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject[event.target.name] = event.target.value;
    if (event.target.name === "cw_mnf_id") {
      this.props.manufacturers.map((data) => {
        if (event.target.value === data.id) {
          newState.mappingObject.cw_mnf_name = data.label;
        }
      });
    }
    this.setState(newState);
  };

  handleChangeRegex = (event: any, i, name) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject.system_categories[
      this.state.mappingObject.system_categories
        .map((e) => e.name)
        .indexOf(name)
    ].value = event;
    this.setState(newState);
  };

  handleChangeNotRegex = (event: any, i, name) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject.system_categories[
      this.state.mappingObject.system_categories
        .map((e) => e.name)
        .indexOf(name)
    ].not_value = event;
    this.setState(newState);
  };

  renderCategories = (options) => {
    return (
      <div className="mapping__options select-input">
        <div className="select-div-1">
          <label className="mapping__options-title"> Select Categories</label>
          <span className="field__label-required" />
        </div>
        <div className="mapping__options-content">
          {this.state.errorOther.categories.errorMessage && (
            <div className="select-error">
              {this.state.errorOther.categories.errorMessage}
            </div>
          )}
          {options &&
            options.map((option, index) => {
              return (
                <div key={index} className="mapping__options-option">
                  <Checkbox
                    isChecked={option.active}
                    name={option.name}
                    onChange={(e) => this.onCheckboxChanged(option, index, e)}
                    disabled={false}
                  >
                    {option.name}
                  </Checkbox>
                </div>
              );
            })}
          {options.length === 0 && !this.state.mappingObject.group_ids && (
            <div className="no-data-categories"> Please Select Group </div>
          )}
          {options.length === 0 && this.state.mappingObject.group_ids && (
            <div className="no-data-categories">
              {" "}
              No categories available for selected group
            </div>
          )}
        </div>
      </div>
    );
  };
  onCheckboxChangedNonCiscoDevice = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject.non_cisco_devices = event.target.checked;
    newState.mappingObject.non_cisco_devices = event.target.checked
      ? "non_cisco_devices_key_mappings"
      : "cisco_devices_key_mappings";
    (newState.mappingObject.key_mappings as any) = this.props.defaultKeyMapping[
      newState.mappingObject.non_cisco_devices
    ];
    this.setState(newState);
  };

  onCheckboxChanged = (option, index, event: any) => {
    const newState = cloneDeep(this.state);
    newState.mappingObject.system_categories[index].active =
      event.target.checked;
    newState.mappingObject.system_categories[index].value = [];
    this.setState(newState);
  };

  loadData = () => {
    const prevIntegrations = this.state.integrations;

    const integrationToRender = prevIntegrations.filter(
      (int) => int.name === "LOGIC_MONITOR"
    );

    const selectedIntegrationFields = integrationToRender[0];
    if (selectedIntegrationFields) {
      Object.keys(selectedIntegrationFields.authentication_config).map(
        (key) => {
          selectedIntegrationFields.authentication_config[key].value = "";
        }
      );
    }

    const integrations = this.state.integrations;
    const allProviderIntegrations = this.state.allProviderIntegrations.filter(
      (user) => user.category === "DEVICE_MONITOR"
    );

    if (integrations.length === 0) {
      return null;
    }

    const integrationFields = integrations[0].authentication_config;

    if (allProviderIntegrations.length > 0) {
      Object.keys(integrationFields).map((key) => {
        integrationFields[key].value =
          allProviderIntegrations[0].authentication_info[key];
      });
    }

    this.setState(
      {
        selectedIntegrationFields: cloneDeep(selectedIntegrationFields),
        selectedIntegration: "LOGIC_MONITOR",
        noNotification: true,
      },
      this.testData
    );
  };

  testData = () => {
    const allProviderIntegrations = this.state.allProviderIntegrations.filter(
      (user) => user.category === "DEVICE_MONITOR"
    );

    if (allProviderIntegrations.length > 0 && this.props.publicKey) {
      this.onTestAdd(false);
    }
  };

  render() {
    return (
      <div className="device-logic-monitor">
        <h3>Device Monitoring </h3>
        <div className="device-logic-monitor-container">
          <ModalBase
            show={this.state.openAddAccount}
            onClose={this.toggleNewModal}
            titleElement={`${
              this.state.index === null ? "Add New" : "Update"
            } Mapping`}
            bodyElement={
              this.state.openAddAccount && this.manufacturerMapPopUp()
            }
            footerElement={this.renderPopUpFooter()}
            className="add-device-logic-monitor-setting"
          />
          {this.renderAddDeviceIntegration()}
          {!this.state.isEdit && (
            <SquareButton
              content="Edit"
              onClick={() => this.setEditStateAdd()}
              bsStyle={"primary"}
              className="edit-btn-auth"
            />
          )}
          {this.state.integrationVerified && this.renderConfigName()}
          {this.state.integrationVerified && this.renderMappingList()}
          {this.renderFooter()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  publicKey: state.setting.publicKey,
  integrations: state.integration.integrations,
  integrationsCatgories: state.integration.integrationsCatgories,
  providerIntegration: state.providerIntegration.providerIntegration,
  isFetching: state.providerIntegration.isFetching,
  allProviderIntegrations: state.providerIntegration.allProviderIntegrations,
  manufacturers: state.inventory.manufacturers,
  isFetchingGroupList: state.integration.isFetchingGroupList,
  defaultKeyMapping: state.integration.defaultKeyMapping,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPublicKey: () => dispatch(fetchEncryptionPublicKey()),
  fetchSystemCategoriesByGroupId: (
    id: number,
    integration: Array<{ [name: string]: string }>
  ) => dispatch(fetchSystemCategoriesByGroupId(id, integration)),
  fetchDeviceMonitoringGroups: (
    integration: Array<{ [name: string]: string }>
  ) => dispatch(fetchDeviceMonitoringGroups(integration)),
  fetchIntegrationsCategories: () => dispatch(fetchIntegrationsCategories()),
  fetchIntegrationsCategoriesTypes: (id: number) =>
    dispatch(fetchIntegrationsCategoriesTypes(id)),
  fetchAllProviderIntegrationsWithFilter: (type: string) =>
    dispatch(fetchAllProviderIntegrationsWithFilter(type)),
  testIntegration: (
    catId: number,
    typeId: number,
    integration: Array<{ [name: string]: string }>
  ) => dispatch(testIntegration(catId, typeId, integration)),
  createProviderIntegrations: (
    providerIntegration: IProviderIntegrationRequest
  ) => dispatch(createProviderIntegrations(providerIntegration)),
  editProviderIntegrations: (
    id: number,
    providerIntegration: IProviderIntegrationRequest
  ) => dispatch(editProviderIntegrations(id, providerIntegration)),
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  updateProviderIntegration: (
    providerId: number,
    providerIntegration: IProviderIntegration
  ) => dispatch(updateProviderIntegration(providerId, providerIntegration)),
  fetchDefaultKeyMapping: () => dispatch(fetchDefaultKeyMapping()),
});

export default connect(mapStateToProps, mapDispatchToProps)(LogicMonitor);
