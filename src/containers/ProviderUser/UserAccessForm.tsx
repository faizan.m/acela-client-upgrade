import React from "react";
import { cloneDeep } from "lodash";

import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import ModalBase from "../../components/ModalBase/modalBase";
import Spinner from "../../components/Spinner";

import "./style.scss";

interface IUserAccessFormState {
  accessList: any;
}

interface IUserAccessFormProps {
  show: boolean;
  onClose: (e: any) => void;
  accessList?: any;
  onSubmit: (user: Icustomer) => void;
  isLoading: boolean;
}

export default class UserAccessForm extends React.Component<
  IUserAccessFormProps,
  IUserAccessFormState
> {
  static emptyState: IUserAccessFormState = {
    accessList: [],
  };

  constructor(props: IUserAccessFormProps) {
    super(props);

    this.state = {
      ...UserAccessForm.emptyState,
    };
  }

  componentDidUpdate(prevProps: IUserAccessFormProps) {
    if (this.props.accessList && prevProps.accessList !== this.props.accessList) {
      this.setState({
        accessList: this.props.accessList,
      });
    }
  }

  handleIsActiveChange = (event: any, index: number) => {
    const targetValue = event.target.value === 'true' ? true : false;
    const newState = cloneDeep(this.state);
    (newState.accessList[index].enabled as boolean) = targetValue;
    this.setState(newState);
  };

  onClose = (e) => {
    this.setState({
      ...UserAccessForm.emptyState,
    });

    this.props.onClose(e);
  };

  onSubmit = () => {
    this.props.onSubmit(this.state.accessList);
  };

  getBody = () => {
    return this.state.accessList ? (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`user-modal__body ${
            this.props.isLoading ? `loading` : ""
          }`}
        >
          {this.state.accessList.map((x,i) => {
            return (
              <Input
                field={{
                  value: x.enabled,
                  label: x.feature,
                  type: "RADIO",
                  isRequired: false,
                  options: [
                    { value: true, label: "Enable" },
                    { value: false, label: "Disable" },
                  ],
                }}
                width={12}
                name={x.feature}
                onChange={e=>this.handleIsActiveChange(e,i)}
              />
            );
          })}
        </div>
      </div>
    ) : null;
  };

  getFooter = () => {
    return (
      <div
        className={`user-modal__footer
      ${this.props.isLoading ? `loading` : ""}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Save Changes"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    const title = "Edit User access";

    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="user-modal__customer-access-form"
      />
    );
  }
}
