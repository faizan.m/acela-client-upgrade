import _, { cloneDeep } from "lodash";
import moment from "moment";
import React from "react";
import { connect } from "react-redux";
import validator from "validator";
import {
  editUserProfile,
  fetchUserForCustomer,
  fetchUserProfile,
  PROFILE_EDIT_FAILURE,
  PROFILE_EDIT_SUCCESS,
  PROFILE_UPLOAD_SUCCESS,
  uploadProfile,
} from "../../actions/profile";
import { fetchUserTypes } from "../../actions/userType";
import SquareButton from "../../components/Button/button";
import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import { countryCodes } from "../../utils/countryCodes";
import Validator from "../../utils/validator";

import "moment-timezone";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import { ADD_CIRCUIT_INFO_FAILURE } from "../../actions/documentation";
import {
  fetchOutlookEmails,
  FETCH_OUTLOOK_EMAIL_SUCCESS,
} from "../../actions/email";
import {
  uploadImage,
  UPLOAD_IMAGE_FAILURE,
  UPLOAD_IMAGE_SUCCESS,
} from "../../actions/sow";
import { isValidVal } from "../../utils/CommonUtils";
import AppValidators from "../../utils/validator";
import "./style.scss";
import { QuillEditorAcela } from "../../components/QuillEditor/QuillEditor";

interface IProfileProps {
  user: ISuperUser;
  userType: string;
  uploadImage: any;
  uploadProfile: any;
  isFetching: boolean;
  errorList?: any; //TODO
  isUsersFetching: boolean;
  fetchUserForCustomer: any;
  userTypes: IUserTypeRole[];
  customers: ICustomerShort[];
  customerUsers: ISuperUser[];
  userRoles: Array<{ value: number; label: string }>;
  fetchEmails: () => Promise<any>;
  fetchUserTypes: TFetchUserTypes;
  editUserProfile: TEditUserProfile;
  addErrorMessage: TShowErrorMessage;
  fetchUserProfile: TFetchUserProfile;
  addSuccessMessage: TShowSuccessMessage;
}

interface IProfileState {
  user: ISuperUser;
  isFormValid: boolean;
  fetchingEmailOptions: boolean;
  emailOptions: IPickListOptions[];
  error: {
    first_name: IFieldValidation;
    last_name: IFieldValidation;
    email: IFieldValidation;
    role: IFieldValidation;
    department: IFieldValidation;
    office_phone: IFieldValidation;
    title: IFieldValidation;
    cell_phone_number: IFieldValidation;
    country_code: IFieldValidation;
    phone_number: IFieldValidation;
    twitter_profile_url: IFieldValidation;
    linkedin_profile_url: IFieldValidation;
    attachment: IFieldValidation;
    cco_id: IFieldValidation;
    outlook_email: IFieldValidation;
  };
  errorList: any;
  attachment: any;
  loading: boolean;
}

//TODO: Remove update state durung render error
class Profile extends React.Component<IProfileProps, IProfileState> {
  static validator = new Validator();
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  customerMailBodyRef: any;

  constructor(props: IProfileProps) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({
    user: {
      id: "",
      email: "",
      first_name: "",
      last_name: "",
      phone_number: "",
      user_role: null,
      is_active: false,
      last_login: "",
      previous_login: "",
      created_on: "",
      profile_pic: "",
      profile: {
        department: "",
        title: "",
        office_phone: "",
        cell_phone_number: "",
        country_code: "1",
        twitter_profile_url: "",
        linkedin_profile_url: "",
        outlook_email: "",
      },
    },
    emailOptions: [],
    isFormValid: true,
    fetchingEmailOptions: false,
    error: {
      first_name: {
        ...Profile.emptyErrorState,
      },
      last_name: {
        ...Profile.emptyErrorState,
      },
      email: {
        ...Profile.emptyErrorState,
      },
      role: {
        ...Profile.emptyErrorState,
      },
      department: {
        ...Profile.emptyErrorState,
      },
      title: {
        ...Profile.emptyErrorState,
      },
      office_phone: {
        ...Profile.emptyErrorState,
      },
      cell_phone_number: {
        ...Profile.emptyErrorState,
      },
      twitter_profile_url: {
        ...Profile.emptyErrorState,
      },
      linkedin_profile_url: {
        ...Profile.emptyErrorState,
      },
      phone_number: {
        ...Profile.emptyErrorState,
      },
      attachment: {
        ...Profile.emptyErrorState,
      },
      country_code: {
        ...Profile.emptyErrorState,
      },
      cco_id: {
        ...Profile.emptyErrorState,
      },
      outlook_email: {
        ...Profile.emptyErrorState,
      },
    },

    errorList: [],
    attachment: null,
    test: "",
    loading: false,
  });

  componentDidMount() {
    this.fetchEmailOptions();
    this.props.fetchUserProfile();
    this.props.fetchUserTypes();
  }

  componentDidUpdate(prevProps: IProfileProps) {
    if (this.props.user && prevProps.user !== this.props.user) {
      this.setState({
        user: this.props.user,
      });
    }
  }

  clearValidationStatus = () => {
    const cleanState = this.getEmptyState().error;
    const newState: IProfileState = cloneDeep(this.state);
    newState.error = cleanState;

    this.setState(newState);
  };

  fetchEmailOptions = () => {
    this.setState({ fetchingEmailOptions: true });
    this.props
      .fetchEmails()
      .then((action) => {
        if (action.type === FETCH_OUTLOOK_EMAIL_SUCCESS) {
          const response: IOutlookEmailItem[] = action.response;
          const emailOptions: IPickListOptions[] = response
            .filter((el) => el.mail)
            .map((el) => ({
              value: el.mail,
              label: el.mail,
            }));
          this.setState({ emailOptions });
        }
      })
      .finally(() => this.setState({ fetchingEmailOptions: false }));
  };

  getCountryCodes = () => {
    if (countryCodes && countryCodes.length > 0) {
      return countryCodes.map((country) => ({
        value: country.code,
        label: `${country.code} (${country.name})`,
      }));
    } else {
      return [];
    }
  };
  validateForm = () => {
    this.clearValidationStatus();
    const error = this.getEmptyState().error;
    let isValid = true;

    if (
      !this.state.user.first_name ||
      this.state.user.first_name.trim().length === 0
    ) {
      error.first_name.errorState = "error";
      error.first_name.errorMessage = "First Name cannot be empty";
      isValid = false;
    } else if (this.state.user.first_name.length > 300) {
      error.first_name.errorState = "error";
      error.first_name.errorMessage =
        "First name should be less than 300 chars.";
      isValid = false;
    }

    if (!Validator.isAlphaWithSpaces(this.state.user.first_name)) {
      error.first_name.errorState = "error";
      error.first_name.errorMessage = "Not valid name";

      isValid = false;
    }

    if (
      !this.state.user.last_name ||
      this.state.user.last_name.trim().length === 0
    ) {
      error.last_name.errorState = "error";
      error.last_name.errorMessage = "Last Name cannot be empty";

      isValid = false;
    } else if (this.state.user.last_name.length > 300) {
      error.last_name.errorState = "error";
      error.last_name.errorMessage = "Last name should be less than 300 chars.";
      isValid = false;
    }

    if (!Validator.isAlphaWithSpaces(this.state.user.last_name)) {
      error.last_name.errorState = "error";
      error.last_name.errorMessage = "Not valid name";

      isValid = false;
    }

    if (!this.state.user.email) {
      error.email.errorState = "error";
      error.email.errorMessage = "Email cannot be empty";

      isValid = false;
    }

    if (this.state.user.email && !validator.isEmail(this.state.user.email)) {
      error.email.errorState = "error";
      error.email.errorMessage = "Enter a valid email";

      isValid = false;
    }

    if (!this.state.user.user_role) {
      error.role.errorState = "error";
      error.role.errorMessage = "Role cannot be empty";

      isValid = false;
    }

    if (
      this.state.user.phone_number &&
      !Profile.validator.isValidPhoneNumber(this.state.user.phone_number)
    ) {
      error.phone_number.errorState = "error";
      error.phone_number.errorMessage = "Enter a valid phone number";

      isValid = false;
    }

    if (
      this.state.user.profile.cell_phone_number &&
      !Profile.validator.isValidPhoneNumber(
        this.state.user.profile.cell_phone_number
      )
    ) {
      error.cell_phone_number.errorState = "error";
      error.cell_phone_number.errorMessage = "Enter a valid phone number";

      isValid = false;
    }

    if (!this.state.user.profile.country_code) {
      error.country_code.errorState = "error";
      error.country_code.errorMessage = "Select country code";

      isValid = false;
    }

    if (
      this.state.user.type === "customer" &&
      _.get(this.state.user, "profile.cco_id") &&
      !AppValidators.noSpaceAllowed(_.get(this.state.user, "profile.cco_id"))
    ) {
      error.cco_id.errorState = "error";
      error.cco_id.errorMessage = "No Spaces allowed";

      isValid = false;
    }
    this.setState({ error });

    return isValid;
  };

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.user[event.target.name] = event.target.value;

    this.setState(newState);
  };

  handleProfileChange = (event: any) => {
    const newState = cloneDeep(this.state);
    const targetName = event.target.name;
    const targetValue = event.target.value;
    newState.user.profile[targetName] = targetValue;
    this.setState(newState);
  };

  handleProfileChangeCCO = (event: any) => {
    const newState = cloneDeep(this.state);
    const targetName = event.target.name;
    const targetValue = event.target.value;
    newState.user.profile[targetName] = targetValue.trim();
    this.setState(newState);
  };

  setValidationErrors = (errorList) => {
    const newState: IProfileState = cloneDeep(this.state);

    Object.keys(errorList).map((key) => {
      if (key === "profile") {
        Object.keys(errorList[key]).map((childKey) => {
          newState.error[childKey].errorState = "error";
          newState.error[childKey].errorMessage = errorList[key][childKey];
        });
      } else if (key !== "detail") {
        newState.error[key].errorState = "error";
        newState.error[key].errorMessage = errorList[key];
      }
    });

    newState.isFormValid = false;
    this.setState(newState);
  };

  editUserProfile = (user: ISuperUser) => {
    if (this.validateForm()) {
      this.clearValidationStatus();
      const user = this.state.user;
      // const convert = new showdown.Converter();
      // user.profile.email_signature.email_signature = convert.makeHtml(
      //   user.profile.email_signature.email_signature_markdown
      // );
      this.props.editUserProfile(user).then((action) => {
        if (action.type === PROFILE_EDIT_FAILURE) {
          this.setValidationErrors(action.errorList.data);
        } else if (action.type === PROFILE_EDIT_SUCCESS) {
          this.props.addSuccessMessage("Profile updated successfully!");
        }
      });
    }
  };

  onResetClick = () => {
    this.clearValidationStatus();
    this.props.fetchUserProfile();
  };
  getIntance = (instance) => {
    instance.togglePreview();
  };
  onImageUpload = (e) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    this.setState({ loading: true });
    if (
      attachment &&
      attachment.type &&
      ["image/png", "image/jpg", "image/jpeg"].includes(attachment.type)
    ) {
      const data = new FormData();
      if (attachment) {
        data.append("filename", attachment);
        this.props.uploadImage(data, attachment).then((action) => {
          if (action.type === UPLOAD_IMAGE_SUCCESS) {
            this.props.addSuccessMessage("Image uploaded successfully!");
            const newState = cloneDeep(this.state);
            ((newState.user.profile.email_signature
              .email_signature_markdown as any) as any) =
              (newState.user.profile.email_signature
                .email_signature_markdown as any) +
              `![](${action.response.file_path})`;
            (newState.loading as boolean) = false;
            this.setState(newState);
          }
          if (action.type === UPLOAD_IMAGE_FAILURE) {
            this.setState({ loading: false });
            this.props.addErrorMessage("Upload Failed !!!");
          }
        });
      }
    } else {
      this.setState({ loading: false });
      this.props.addErrorMessage("Invalid image format.");
    }
  };
  handleChangeMarkdown = (e) => {
    const newState = cloneDeep(this.state);
    (newState.user.profile.email_signature.email_signature as any) = e;
    this.setState(newState);
  };
  handleFile = (e: any) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    if (
      attachment &&
      attachment.type &&
      ["image/png", "image/jpg", "image/jpeg"].includes(attachment.type)
    ) {
      const data = new FormData();
      if (attachment) {
        data.append("profile_pic", attachment);
        this.props.uploadProfile(data).then((action) => {
          if (action.type === PROFILE_UPLOAD_SUCCESS) {
            this.props.addSuccessMessage("Profile Pic updated successfully!");
            this.props.fetchUserProfile();
          }
          if (action.type === ADD_CIRCUIT_INFO_FAILURE) {
            this.props.addErrorMessage("Upload failed!");
          }
        });
      }
    } else {
      this.props.addErrorMessage("Invalid image format.");
    }
  };

  getBody = () => {
    return (
      <div className="profile__details">
        <div className="profile__details-header">
          <h5>Profile Details</h5>
          <div className="left">
            (Last login :
            {this.state.user.previous_login &&
            this.state.user.previous_login !== ""
              ? moment(this.state.user.previous_login).format(
                  "MM/DD/YYYY h:mm A"
                )
              : "N.A"}
            )
          </div>
          <div className="right">
            Account created on :
            {moment(this.state.user.created_on).format("MM/DD/YYYY")}
          </div>
        </div>
        <div className="profile__details-body">
          <div className="profile__details-body col-md-9">
            <Input
              field={{
                label: "First Name",
                type: "TEXT",
                isRequired: true,
                value: this.state.user.first_name,
              }}
              error={this.state.error.first_name}
              width={4}
              placeholder="Enter First Name"
              name="first_name"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: "Last Name",
                type: "TEXT",
                isRequired: true,
                value: this.state.user.last_name,
              }}
              error={this.state.error.last_name}
              width={4}
              placeholder="Enter Last Name"
              name="last_name"
              onChange={this.handleChange}
            />

            <Input
              field={{
                label: "Title",
                type: "TEXT",
                isRequired: false,
                value: this.state.user.profile.title,
              }}
              error={this.state.error.title}
              width={4}
              placeholder="Enter Title"
              name="title"
              onChange={this.handleProfileChange}
            />
            <Input
              field={{
                label: "Office Country code",
                isRequired: false,
                type: "PICKLIST",
                options: this.getCountryCodes(),
                value: this.state.user.profile.office_phone_country_code,
              }}
              error={this.state.error.country_code}
              width={4}
              placeholder="code"
              name="office_phone_country_code"
              onChange={this.handleProfileChange}
              className="country-code"
            />
            <Input
              field={{
                label: "Office Phone Number",
                type: "TEXT",
                isRequired: false,
                value: this.state.user.profile.office_phone,
              }}
              error={this.state.error.office_phone}
              width={4}
              placeholder="Enter Office Phone Number"
              name="office_phone"
              onChange={this.handleProfileChange}
            />
            <Input
              field={{
                label: "CCO ID",
                type: "TEXT",
                isRequired: false,
                value: _.get(this.state.user, "profile.cco_id"),
              }}
              error={this.state.error.cco_id}
              width={4}
              placeholder="Enter CCO ID"
              name="cco_id"
              onChange={this.handleProfileChangeCCO}
            />
            <Input
              field={{
                label: "Country code",
                isRequired: false,
                type: "PICKLIST",
                options: this.getCountryCodes(),
                value: this.state.user.profile.country_code,
              }}
              error={this.state.error.country_code}
              width={4}
              placeholder="code"
              name="country_code"
              onChange={this.handleProfileChange}
              className="country-code"
            />
            <Input
              field={{
                label: "Cell Phone Number",
                type: "TEXT",
                isRequired: false,
                value: this.state.user.profile.cell_phone_number,
              }}
              error={this.state.error.cell_phone_number}
              width={4}
              placeholder="Enter Cell Phone Number"
              name="cell_phone_number"
              onChange={this.handleProfileChange}
            />

            <Input
              field={{
                label: "Department",
                type: "TEXT",
                isRequired: false,
                value: this.state.user.profile.department,
              }}
              error={this.state.error.department}
              width={4}
              placeholder="Enter Department"
              name="department"
              onChange={this.handleProfileChange}
            />
            <Input
              field={{
                label: "Outlook Email",
                type: "PICKLIST",
                isRequired: false,
                value: this.state.user.profile.outlook_email,
                options: this.state.emailOptions,
              }}
              loading={this.state.fetchingEmailOptions}
              error={this.state.error.outlook_email}
              width={4}
              placeholder="Select Outlook Email"
              name="outlook_email"
              onChange={this.handleProfileChange}
            />
            <Input
              field={{
                label: "Twitter Profile URL",
                type: "TEXT",
                isRequired: false,
                value: this.state.user.profile.twitter_profile_url,
              }}
              error={this.state.error.twitter_profile_url}
              width={4}
              placeholder="Enter Twitter Profile URL"
              name="twitter_profile_url"
              onChange={this.handleProfileChange}
            />
            <Input
              field={{
                label: "Linkedin Profile URL",
                type: "TEXT",
                isRequired: false,
                value: this.state.user.profile.linkedin_profile_url,
              }}
              error={this.state.error.linkedin_profile_url}
              width={4}
              placeholder="Enter Linkedin Profile URL"
              name="linkedin_profile_url"
              onChange={this.handleProfileChange}
            />

            <div className="customer-email">
              <div className="markdown-profile">
                <QuillEditorAcela
                  onChange={(e) => this.handleChangeMarkdown(e)}
                  value={
                    this.state.user.profile &&
                    this.state.user.profile.email_signature &&
                    this.state.user.profile.email_signature.email_signature
                  }
                  customToolbar={[
                    ["bold", "italic", "underline", "strike"], // toggled buttons
                    ["image"],
                    [{ color: [] }, { background: [] }], // dropdown with defaults from theme
                    [{ header: [1, 2, 3, 4, 5, 6, false] }],
                  ]}
                  wrapperClass={`template-config-quill`}
                  label={`
                      Email Signature (This Signature will be shown in the
                      sent email)`}
                  isRequired={false}
                  scrollingContainer=".add-template"
                  markDownVariables={[]}
                  hideTable={true}
                />
              </div>
            </div>
          </div>
          <div className="profile-left col-md-3">
            <div className="profile-pic">
              <label
                className="btn square-btn btn-primary
                file-button import-button upload-button"
              >
                <img
                  className="icon-upload"
                  src="/assets/icons/photo-camera.png"
                  title="Upload image"
                />
                <Input
                  field={{
                    label: "",
                    type: "FILE",
                    value: `${
                      this.state.attachment ? this.state.attachment : ""
                    }`,
                    id: "attachment",
                  }}
                  error={this.state.error.attachment}
                  width={6}
                  name=""
                  accept="image/png, image/jpg, image/jpeg"
                  onChange={this.handleFile}
                  className="custom-file-input upload-btn"
                />
              </label>
              <img
                alt=""
                src={
                  isValidVal(this.state.user.profile.profile_pic)
                    ? `${this.state.user.profile.profile_pic}`
                    : "/assets/icons/attachment-placeholder.png"
                }
              />
            </div>
            <Input
              field={{
                label: "Email Address",
                type: "TEXT",
                isRequired: true,
                value: this.state.user.email,
              }}
              error={this.state.error.email}
              disabled={true}
              width={12}
              placeholder="Enter Email"
              name="email"
              onChange={this.handleChange}
            />
            <Input
              field={{
                label: "Role",
                type: "TEXT",
                isRequired: true,
                value: this.state.user.role_display_name,
              }}
              disabled={true}
              width={12}
              name="role"
              onChange={null}
            />

            <Input
              field={{
                label: "Browser Timezone",
                type: "TEXT",
                isRequired: false,
                value: moment.tz.guess(),
              }}
              disabled={true}
              title={`${new Date().getTimezoneOffset()}`}
              width={12}
              name="role"
              onChange={null}
            />
          </div>
        </div>
        {this.getFooter()}
      </div>
    );
  };
  getFooter = () => {
    return (
      <div className="profile__footer">
        {this.state.user !== this.props.user && (
          <SquareButton
            onClick={this.onResetClick}
            content="Reset"
            bsStyle={"default"}
          />
        )}
        <SquareButton
          onClick={this.editUserProfile}
          content={"Save Changes"}
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div className="profile">
        <div className="profile__header">
          <h3>Profile</h3>
          <div className="loader">
            <Spinner
              show={
                this.props.isFetching ||
                this.props.isUsersFetching ||
                this.state.loading
              }
            />
          </div>
          <div className="profile__Body">{this.getBody()}</div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  userTypes: state.userType.userTypes,
  isFetching: state.profile.isFetching,
  customers: state.customer.customersShort,
  customerUsers: state.profile.customerUsers,
  isUsersFetching: state.profile.isUsersFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchEmails: () => dispatch(fetchOutlookEmails()),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
  fetchUserProfile: () => dispatch(fetchUserProfile()),
  editUserProfile: (profile: ISuperUser) => dispatch(editUserProfile(profile)),
  uploadProfile: (data: any) => dispatch(uploadProfile(data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchUserForCustomer: (id: number) => dispatch(fetchUserForCustomer(id)),
  uploadImage: (file: any, name: string) => dispatch(uploadImage(file, name)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
