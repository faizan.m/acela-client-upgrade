import cloneDeep from 'lodash/cloneDeep';
import React from 'react';
import { connect } from 'react-redux';
import { fetchAllCustomerUsers } from '../../actions/documentation';
import { getQuoteStageList, getQuoteTypeList, createQuote, CREATE_QUOTES_SUCCESS } from '../../actions/sow';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import SelectInput from '../../components/Input/Select/select';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { commonFunctions } from '../../utils/commonFunctions';
import './styles.scss';
interface ICreateOpportunityState {
  types: any[];
  quote: any;
  error: {
    stage_id: IFieldValidation;
    type_id: IFieldValidation;
    name: IFieldValidation;
    user_id: IFieldValidation;
  };
  loading: boolean;
}

interface ICreateOpportunityProps {
  close: (e: any) => void;
  isVisible: boolean;
  customerId: any;
  stages?: any[];
  types?: any[];
  isLoading?: boolean;
  errorList?: any;
  quote?: any;
  getQuoteStageList?: any;
  getQuoteTypeList?: any;
  createQuote?: any;
  fetchAllCustomerUsers?: any;
  qStageList?: any;
  qTypeList?: any;
  quoteFetching?: any;
  isFetchingQStageList?: any;
  isFetchingQTypeList?: any;
  users?: any;
}
class CreateOpportunity extends React.Component<ICreateOpportunityProps, ICreateOpportunityState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };
  constructor(props: ICreateOpportunityProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    quote: {
      stage_id: null,
      type_id: null,
      name: null,
      user_id: null,
      customer_id: this.props.customerId,
    },
    loading: false,
    error: {
      stage_id: { ...CreateOpportunity.emptyErrorState },
      type_id: { ...CreateOpportunity.emptyErrorState },
      name: { ...CreateOpportunity.emptyErrorState },
      user_id: { ...CreateOpportunity.emptyErrorState },
    },
    types: [],
  });

  componentDidMount() {
    this.props.fetchAllCustomerUsers(this.props.customerId);
    this.props.getQuoteTypeList();
    this.props.getQuoteStageList();
  }

  componentDidUpdate(prevProps: ICreateOpportunityProps) {
    if (prevProps.errorList !== this.props.errorList) {
      this.setValidationErrors(this.props.errorList);
    }
  }




  handleChange = (event: any) => {
    const targetName = event.target.name;
    const targetValue = event.target.value;
    const newState = cloneDeep(this.state);
    (newState.quote[targetName] as any) = targetValue;
    this.setState(newState);
  };

  onClose = e => {
    this.props.close(e);
  };

  setValidationErrors = errorList => {
    const newState: ICreateOpportunityState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList,newState));
  };
  isValid = () => {
    const error = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.quote.name || this.state.quote.name.trim().length === 0) {
      error.name.errorState = "error";
      error.name.errorMessage = 'Enter a valid opportunity name';

      isValid = false;
    } else if (this.state.quote.name.length > 300) {
      error.name.errorState = "error";
      error.name.errorMessage =
        'Opportunity name should be less than 300 chars.';

      isValid = false;
    }

    if (this.state.quote && !this.state.quote.stage_id) {
      error.stage_id.errorState = "error";
      error.stage_id.errorMessage = 'Please select stage';

      isValid = false;
    }
    if (this.state.quote && !this.state.quote.type_id) {
      error.type_id.errorState = "error";
      error.type_id.errorMessage = 'Please select type';

      isValid = false;
    }

    if (this.state.quote && this.state.quote.stage_id < 0) {
      error.stage_id.errorState = "error";
      error.stage_id.errorMessage = 'Please select stage';

      isValid = false;
    }
    if (this.state.quote && this.state.quote.type_id < 0) {
      error.type_id.errorState = "error";
      error.type_id.errorMessage = 'Please select type';

      isValid = false;
    }

    if (this.state.quote && !this.state.quote.user_id) {
      error.user_id.errorState = "error";
      error.user_id.errorMessage = 'Please select user';

      isValid = false;
    }

    this.setState({
      error,
    });

    return isValid;
  };
  getCustomerUserOptions = () => {
    const users = this.props.users
      ? this.props.users.map(t => ({
        value: t.id,
        label: `${t.first_name} ${t.last_name}`,
        disabled: false,
      }))
      : [];

    return users;
  };
  renderBody = () => {
    const stages = this.props.qStageList
      ? this.props.qStageList.map(manufacturer => ({
        value: manufacturer.id,
        label: manufacturer.label,
      }))
      : [];
    const types = this.props.qTypeList
      ? this.props.qTypeList.map(site => ({
        value: site.id,
        label: site.label,
      }))
      : [];

    return (
      <div className="add-quote">
        <div className="loader modal-loader">
          <Spinner show={this.state.loading} />
        </div>
        <div
          className={`add-quote__body ${this.props.isLoading ? `loading` : ''}`}
        >
          <div className="add-new-option-box field-section col-md-12 col-xs-12">
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select User
                </label>
              <span className="field__label-required" />
            </div>
            <div
              className={` ${this.state.error.user_id.errorMessage ? `error-input` : ''
                }`}
            >
              <SelectInput
                name="user_id"
                value={this.state.quote.user_id}
                onChange={e => this.handleChange(e)}
                options={this.getCustomerUserOptions()}
                multi={false}
                searchable={true}
                placeholder="Select User"
              />
            </div>
            {this.state.error.user_id.errorMessage && (
              <div className="field-error">
                {this.state.error.user_id.errorMessage}
              </div>
            )}
          </div>
          <Input
            field={{
              label: 'Name',
              type: "TEXT",
              value: this.state.quote.name,
              isRequired: true,
            }}
            width={12}
            placeholder="Enter name"
            error={this.state.error.name}
            name="name"
            onChange={this.handleChange}
          />

          <div
            className="select-manufacturer
           field-section  col-md-12 col-xs-12"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Type
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${this.state.error.type_id.errorMessage ? `error-input` : ''
                }`}
            >
              <SelectInput
                name="type_id"
                value={this.state.quote.type_id}
                onChange={this.handleChange}
                options={types}
                searchable={true}
                placeholder="Select type"
                clearable={false}
              />
            </div>
            {this.state.error.type_id.errorMessage && (
              <div className="field-error">
                {this.state.error.type_id.errorMessage}
              </div>
            )}
          </div>
          <div
            className="
          select-manufacturer field-section  col-md-12 col-xs-12"
          >
            <div className="field__label row">
              <label className="field__label-label" title="">
                Select Stage
              </label>
              <span className="field__label-required" />
            </div>
            <div
              className={`${this.state.error.stage_id.errorMessage ? `error-input` : ''
                }`}
            >
              <SelectInput
                name="stage_id"
                value={this.state.quote.stage_id}
                onChange={this.handleChange}
                options={stages}
                searchable={true}
                placeholder="Select stage"
                clearable={false}
              />
            </div>
            {this.state.error.stage_id.errorMessage && (
              <div className="field-error">
                {this.state.error.stage_id.errorMessage}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div
        className={`add-quote__footer
      ${this.state.loading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Add"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  onSubmit = () => {
    const customerId = this.props.customerId;
    if (this.isValid() && customerId) {
      this.setState({ loading: true });
      this.props.createQuote(customerId, this.state.quote).then(action => {
        if (action.type === CREATE_QUOTES_SUCCESS) {
          this.setState({ loading: false });
          this.props.close(true);
        } else {
          this.setState({ loading: false });
        }
      });
    }
  };


  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Add New Opportunity"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-new-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  getQuoteStageList: () => dispatch(getQuoteStageList()),
  getQuoteTypeList: () => dispatch(getQuoteTypeList()),
  createQuote: (id: number, q: any) => dispatch(createQuote(id, q)),
  fetchAllCustomerUsers: (id: number, params?: IServerPaginationParams) =>
    dispatch(fetchAllCustomerUsers(id))
});

const mapStateToProps = (state: IReduxStore) => ({
  qStageList: state.sow.qStageList,
  qTypeList: state.sow.qTypeList,
  quoteFetching: state.dashboard.quoteFetching,
  isFetchingQStageList: state.sow.isFetchingQStageList,
  isFetchingQTypeList: state.sow.isFetchingQTypeList,
  users: state.documentation.users,
});
export default connect<any, any>
  (mapStateToProps, mapDispatchToProps)(CreateOpportunity);
