import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import Input from "../../components/Input/input";
import "../../commonStyles/filtersListing.scss";
import "../../commonStyles/filterModal.scss";
import "./style.scss";

interface VRWarehouseFiltersProps {
  show: boolean;
  filters: IVRWarehouseFilters;
  statusOptions: string[];
  isFetchingStatus: boolean;
  warehouseOptions: string[];
  isFetchingWarehouse: boolean;
  onClose: () => void;
  onSubmit: (filters: IVRWarehouseFilters) => void;
}

const VRWarehouseFilters: React.FC<VRWarehouseFiltersProps> = (props) => {
  const [filters, setFilters] = useState<IVRWarehouseFilters>({
    customer_id: null,
    status: null,
    warehouse: null,
  });

  useEffect(() => {
    setFilters(props.filters);
  }, [props.filters]);

  const onFilterChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    setFilters((prev) => ({
      ...prev,
      [targetName]: targetValue,
    }));
  };

  const onSubmit = () => {
    props.onSubmit(filters);
  };

  const getBody = () => {
    return (
      <div className="filters-modal__body agr-filter col-md-12">
        <Input
          field={{
            label: "Status",
            type: "PICKLIST",
            value: filters.status,
            options: props.statusOptions,
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="status"
          onChange={onFilterChange}
          placeholder={`Select Status`}
          loading={props.isFetchingStatus}
        />
        <Input
          field={{
            label: "Warehouse",
            type: "PICKLIST",
            value: filters.warehouse,
            options: props.warehouseOptions,
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="warehouse"
          onChange={onFilterChange}
          loading={props.isFetchingWarehouse}
          placeholder={`Select Warehouse`}
        />
      </div>
    );
  };

  const getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={props.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  return (
    <ModalBase
      show={props.show}
      onClose={props.onClose}
      titleElement={"Filters"}
      bodyElement={getBody()}
      footerElement={getFooter()}
      className="filters-modal"
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  statusOptions: state.virtualWarehouse.quickbaseStatus,
  isFetchingStatus: state.virtualWarehouse.isFetchingStatus,
  warehouseOptions: state.virtualWarehouse.quickbaseWareHouses,
  isFetchingWarehouse: state.virtualWarehouse.isFetchingWarehouse,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(VRWarehouseFilters);
