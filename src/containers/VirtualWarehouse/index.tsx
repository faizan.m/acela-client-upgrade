import React, { useEffect, useMemo, useRef, useState } from "react";
import { connect } from "react-redux";
import * as XLSX from "xlsx";
import { cloneDeep, debounce } from "lodash";
import {
  getQuickbaseStatuses,
  getQuickbaseWarehouses,
  fetchOrUpdateVRWHSettings,
  fetchVRWarehouseDataListing,
  fetchVRWarehouseDeviceDetail,
  FETCH_VRWH_DEVICE_DATA_SUCCESS,
  FETCH_VRWH_DEVICE_DATA_FAILURE,
  VRWH_SETTING_SUCCESS,
  FETCH_VRWH_DEVICE_DETAIL_SUCCESS,
  FETCH_VRWH_DEVICE_DETAIL_FAILURE,
} from "../../actions/virtualWarehouse";
import { addErrorMessage, addWarningMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Table from "../../components/Table/table";
import Input from "../../components/Input/input";
import Checkbox from "../../components/Checkbox/checkbox";
import SquareButton from "../../components/Button/button";
import Filters from "./filters";
import DeviceDetails from "./deviceDetails";
import { getUserProfile } from "../../utils/AuthUtil";
import { commonFunctions } from "../../utils/commonFunctions";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import "./style.scss";
import Select from "../../components/Input/Select/select";

enum FetchingStatus {
  FETCHING_LIST,
  FETCHING_EXCEL,
  FETCHING_DEVICE_DETAIL,
}

enum ModalType {
  FILTER,
  DEVICE_DETAIL,
}

interface VRWarehouseListingProps extends ICommonProps {
  customerId: number;
  customerProfile: Icustomer;
  customers: ICustomerShort[];
  fetchSettings: () => Promise<any>;
  getStatusOptions: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addWarningMessage: TShowWarningMessage;
  getWarehouseOptions: () => Promise<any>;
  fetchDeviceDetail: (id: number) => Promise<any>;
  fetchListing: (
    params: IServerPaginationParams & IVRWarehouseFilters
  ) => Promise<any>;
}

interface PaginationState {
  reset: boolean;
  totalRows: number;
  totalPages: number;
  filters: IVRWarehouseFilters;
  params: IServerPaginationParams;
}

interface IVRWarehouseRow {
  index: number;
  location: string;
  status: string;
  po_number: string;
  warehouse: string;
  updated_on: string;
  part_number: string;
  customer_id: string;
  customer_po: string;
  serial_number: string;
  customer_name: string;
  consigned_date: string;
  part_description: string;
  opportunity_name: string;
  opportunity_number: number;
  sales_order_crm_id: number;
  shipping_address: string;
  shipping_date: string;
  tracking_number: string;
  carrier: string;
}

const EmptyPagination: PaginationState = {
  filters: {
    include_shipped: false,
  },
  totalRows: 0,
  reset: false,
  totalPages: 0,
  params: { page: 1, page_size: 25 },
};

const VirtualWarehouseListing: React.FC<VRWarehouseListingProps> = (props) => {
  const [rows, setRows] = useState<IVRWarehouseRow[]>([]);
  const [searchString, setSearchString] = useState<string>("");
  const [showModal, setShowModal] = useState<null | ModalType>(null);
  const [settings, setSettings] = useState<IVRWarehouseSettings>(null);
  const [showAllCustomers, setShowAllCustomers] = useState<boolean>(false);
  const [deviceDetail, setDeviceDetail] = useState<IVRWHDeviceDetail>(null);
  const [fetchStatus, setFetchStatus] = useState<null | FetchingStatus>(null);
  const [pagination, setPagination] = useState<PaginationState>({
    ...EmptyPagination,
  });

  const fetchDataDebounced = useRef(
    debounce(
      (params?: IServerPaginationParams, filters?: IVRWarehouseFilters) => {
        fetchData(params, filters);
      },
      500
    )
  );

  const customerCRMID: number = useMemo(() => {
    if (getUserProfile().scopes.type === "CUSTOMER")
      return props.customerProfile && props.customerProfile.crm_id
        ? Number(props.customerProfile.crm_id)
        : null;
    else
      return commonFunctions.getCustomerCRMID(
        props.customerId,
        props.customers
      );
  }, [props.customers, props.customerProfile, props.customerId]);

  useEffect(() => {
    fetchOrUpdateVRWHSettings();
    props.getStatusOptions();
    props.getWarehouseOptions();
  }, []);

  useEffect(() => {
    if (customerCRMID) {
      const newPagination = cloneDeep(EmptyPagination);
      newPagination.reset = true;
      newPagination.filters = {
        customer_id: customerCRMID,
        status: undefined,
        warehouse: undefined,
      };
      setPagination(newPagination);
      fetchData(newPagination.params, newPagination.filters);
    }
  }, [customerCRMID]);

  const fetchOrUpdateVRWHSettings = () => {
    props.fetchSettings().then((action) => {
      if (action.type === VRWH_SETTING_SUCCESS) {
        setSettings(action.response);
      }
    });
  };

  // Server side searching, sorting, ordering
  const fetchData = (
    params: IServerPaginationParams = {},
    filters: IVRWarehouseFilters = {}
  ) => {
    const newPaginationParams = cloneDeep({ ...pagination.params, ...params });
    const newFilterParams = cloneDeep({ ...pagination.filters, ...filters });
    const apiParams: IServerPaginationParams & IVRWarehouseFilters = {
      ...newPaginationParams,
      ...newFilterParams,
    };
    if (apiParams.status)
      apiParams.status = (apiParams.status as string[]).join(",");
    if (apiParams.warehouse)
      apiParams.warehouse = (apiParams.warehouse as string[]).join(",");
    setFetchStatus(FetchingStatus.FETCHING_LIST);
    props
      .fetchListing(apiParams)
      .then((action) => {
        if (action.type === FETCH_VRWH_DEVICE_DATA_SUCCESS) {
          const res: IPaginatedDefault & { results: IVRWarehouseRow[] } =
            action.response;
          const rows = res.results;
          setRows(rows);
          setPagination({
            reset: false,
            totalRows: res.count,
            totalPages: Math.ceil(res.count / pagination.params.page_size),
            filters: newFilterParams,
            params: newPaginationParams,
          });
        } else if (action.type === FETCH_VRWH_DEVICE_DATA_FAILURE) {
          props.addWarningMessage(
            "Virtual Warehouse Settings not configured for the Provider!"
          );
        }
      })
      .finally(() => setFetchStatus(null));
  };

  // Event Handlers
  const handleExportClick = () => {
    if (customerCRMID) {
      setFetchStatus(FetchingStatus.FETCHING_EXCEL);
      // do fetch the excel file
      const newPaginationParams = cloneDeep(pagination.params);
      const newFilterParams = cloneDeep(pagination.filters);
      const apiParams: IServerPaginationParams & IVRWarehouseFilters = {
        ...newPaginationParams,
        ...newFilterParams,
        page: 1,
        page_size: pagination.totalRows,
      };
      if (apiParams.status)
        apiParams.status = (apiParams.status as string[]).join(",");
      if (apiParams.warehouse)
        apiParams.warehouse = (apiParams.warehouse as string[]).join(",");
      const getString = (s: string | number) => (s ? String(s) : "-");
      props
        .fetchListing(apiParams)
        .then((action) => {
          if (action.type === FETCH_VRWH_DEVICE_DATA_SUCCESS) {
            const raw_data: IVRWarehouseRow[] = action.response.results;
            let customerName: string;
            if (showAllCustomers) customerName = "ALL_CUSTOMERS";
            else
              customerName =
                getUserProfile().scopes.type === "CUSTOMER"
                  ? props.customerProfile.name
                  : props.customers.find((el) => el.crm_id == customerCRMID)
                      .name;
            // Add also consigned date
            const rows = raw_data.map((row: IVRWarehouseRow) => ({
              location: getString(row.location),
              status: getString(row.status),
              part_number: getString(row.part_number),
              serial_number: getString(row.serial_number),
              part_description: getString(row.part_description),
              warehouse: getString(row.warehouse),
              opportunity_name: getString(row.opportunity_name),
              customer_po: getString(row.customer_po),
              po_number: getString(row.po_number),
              consigned_date:
                fromISOStringToFormattedDate(
                  row.consigned_date,
                  "MMM DD, YYYY"
                ) || "-",
              customer_id: getString(row.customer_id),
              customer_name: getString(row.customer_name),
              sales_order: getString(row.sales_order_crm_id),
              shipping_address: getString(row.shipping_address),
              shipping_date:
                fromISOStringToFormattedDate(
                  row.shipping_date,
                  "MMM DD, YYYY"
                ) || "-",
              tracking_number: getString(row.tracking_number),
              carrier: getString(row.carrier),
              updated_on:
                fromISOStringToFormattedDate(row.updated_on, "MMM DD, YYYY") ||
                "-",
            }));
            const worksheet = XLSX.utils.json_to_sheet(rows);
            const workbook = XLSX.utils.book_new();
            XLSX.utils.book_append_sheet(
              workbook,
              worksheet,
              "Virtual Warehouse Devices"
            );

            /* fix headers */
            XLSX.utils.sheet_add_aoa(
              worksheet,
              [
                [
                  "Location",
                  "Status",
                  "Part Number",
                  "Serial Number",
                  "Part Description",
                  "Warehouse",
                  "Project Name",
                  "Customer PO",
                  "LP PO",
                  "Consigned Date",
                  "Customer ID",
                  "Customer Name",
                  "Sales Order",
                  "Shipping Address",
                  "Shipping Date",
                  "Tracking Number",
                  "Carrier",
                  "Updated On",
                ],
              ],
              { origin: "A1" }
            );

            /* calculate each column width */
            const max_width_arr = rows.reduce(
              (w, r) => [
                Math.max(w[0], r.location.length),
                Math.max(w[1], r.status.length),
                Math.max(w[2], r.part_number.length),
                Math.max(w[3], r.serial_number.length),
                Math.max(w[4], r.part_description.length),
                Math.max(w[5], r.warehouse.length),
                Math.max(w[6], r.opportunity_name.length),
                Math.max(w[7], r.customer_po.length),
                Math.max(w[8], r.po_number.length),
                Math.max(w[9], r.consigned_date.length),
                10,
                customerName.length,
                15,
                Math.max(w[13], r.shipping_address.length),
                Math.max(w[14], r.shipping_date.length),
                Math.max(w[15], r.tracking_number.length),
                Math.max(w[16], r.carrier.length),
                Math.max(w[17], r.updated_on.length),
              ],
              new Array(18).fill(10)
            );

            worksheet["!cols"] = max_width_arr.map((el) => ({
              wch: el,
            }));
            XLSX.writeFile(
              workbook,
              `Warehouse_Devices_${customerName}_${new Date().toISOString()}.xlsx`
            );
          }
        })
        .finally(() => setFetchStatus(null));
    }
  };

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value.toLowerCase();
    const newPagination = cloneDeep(pagination);
    setSearchString(e.target.value);
    if (search && search.length > 0) {
      newPagination.reset = true;
      newPagination.params.search = search;
    } else {
      newPagination.reset = true;
      newPagination.params.page = 1;
      newPagination.params.search = "";
    }
    setPagination(newPagination);
    fetchDataDebounced.current(newPagination.params, newPagination.filters);
  };

  const onShowShippedInventoryChange = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    const show = e.target.checked;
    const newPagination = cloneDeep(pagination);
    newPagination.reset = true;
    newPagination.params.page = 1;
    newPagination.filters.include_shipped = show;
    setPagination(newPagination);
    fetchData(newPagination.params, newPagination.filters);
  };

  const onShowAllCustomers = (e: React.ChangeEvent<HTMLInputElement>) => {
    const show = e.target.checked;
    const newPagination = cloneDeep(pagination);
    newPagination.reset = true;
    newPagination.params.page = 1;
    newPagination.filters.customer_id = show ? null : customerCRMID;
    setShowAllCustomers(show);
    setPagination(newPagination);
    fetchData(newPagination.params, newPagination.filters);
  };

  const onRowClick = (rowInfo: { original: IVRWarehouseRow }) => {
    setFetchStatus(FetchingStatus.FETCHING_DEVICE_DETAIL);
    props
      .fetchDeviceDetail(rowInfo.original.index)
      .then((action) => {
        if (action.type === FETCH_VRWH_DEVICE_DETAIL_SUCCESS) {
          setDeviceDetail(action.response);
          setShowModal(ModalType.DEVICE_DETAIL);
        } else if (action.type === FETCH_VRWH_DEVICE_DETAIL_FAILURE) {
          props.addErrorMessage("Error fetching Device Details!");
        }
      })
      .finally(() => setFetchStatus(null));
  };

  const onCloseDeviceDetail = () => {
    setShowModal(null);
    setDeviceDetail(null);
  };

  const applyFilters = (filters: IVRWarehouseFilters) => {
    const newPagination = cloneDeep(pagination);
    newPagination.params.page = 1;
    newPagination.reset = true;
    newPagination.filters = filters;
    setShowModal(null);
    setPagination(newPagination);
    fetchData(newPagination.params, newPagination.filters);
  };

  // Rendering methods
  const renderFilters = () => {
    const filters = pagination.filters;
    const shouldRenderFilters =
      (filters.status && filters.status.length) ||
      (filters.warehouse && filters.warehouse.length);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing vrwh-filters">
        <label className="vrwh-filters-heading">Applied Filters: </label>
        {Boolean(filters.status && filters.status.length) && (
          <div className="section-show-filters vrwh-applied-filters-row">
            <label>Status: </label>
            <Select
              name="status"
              value={filters.status}
              onChange={(e) => applyFilters({ status: e.target.value })}
              options={(filters.status as string[]).map((el) => ({
                value: el,
                label: el,
              }))}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
        {Boolean(filters.warehouse && filters.warehouse.length) && (
          <div className="section-show-filters vrwh-applied-filters-row">
            <label>Warehouse: </label>
            <Select
              name="warehouse"
              value={filters.warehouse}
              onChange={(e) => applyFilters({ warehouse: e.target.value })}
              options={(filters.warehouse as string[]).map((el) => ({
                value: el,
                label: el,
              }))}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };

  const renderTopBar = () => {
    return (
      <div className={"vr-warehouse__table-top "}>
        <div className="vr-warehouse__table-actions">
          <div className="vr-warehouse__table-header-search">
            <Input
              field={{
                label: "",
                type: "SEARCH",
                value: searchString,
                isRequired: false,
              }}
              width={3}
              placeholder="Search"
              name="searchString"
              onChange={handleSearchChange}
              className="vr-warehouse__search"
            />

            <Checkbox
              isChecked={pagination.filters.include_shipped}
              name="option"
              onChange={onShowShippedInventoryChange}
              className="vr-actions-checkbox"
            >
              Show Shipped Inventory
            </Checkbox>
            {getUserProfile().scopes.type !== "CUSTOMER" && (
              <Checkbox
                isChecked={showAllCustomers}
                name="option"
                onChange={onShowAllCustomers}
                className="vr-actions-checkbox"
              >
                All Customers
              </Checkbox>
            )}
          </div>
          <div className="vr-warehouse__table-header-btn-group">
            <SquareButton
              onClick={() => setShowModal(ModalType.FILTER)}
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              bsStyle={"primary"}
            />
            <SquareButton
              onClick={handleExportClick}
              content={
                <span>
                  <img
                    className={
                      fetchStatus === FetchingStatus.FETCHING_EXCEL
                        ? "icon__loading"
                        : ""
                    }
                    src={
                      fetchStatus === FetchingStatus.FETCHING_EXCEL
                        ? "/assets/icons/loading.gif"
                        : "/assets/icons/export.png"
                    }
                    alt="Downloading File"
                  />
                  Export
                </span>
              }
              bsStyle={"primary"}
              disabled={
                !Boolean(rows && rows.length > 0) ||
                fetchStatus === FetchingStatus.FETCHING_EXCEL
              }
            />
          </div>
        </div>
        {renderFilters()}
      </div>
    );
  };

  const rowSelectionProps = {
    showCheckbox: false,
    selectIndex: "id",
    onRowsToggle: () => {},
  };

  const manualProps = {
    manual: true,
    pages: pagination.totalPages,
    onFetchData:
      pagination.filters.customer_id || showAllCustomers
        ? fetchData
        : () => null,
    reset: pagination.reset,
    defaultPageSize: 25,
  };

  const columns: any = [
    {
      accessor: "status",
      Header: "Status",
      sortable: false,
      width: 120,
      Cell: (status) => (
        <div className={`vr-warehouse-status-icons`}>
          <img
            src={`/assets/new-icons/delivery-truck.svg`}
            className={`status-svg-images ${
              isGreenStatus("Transit", status.original, settings)
                ? " green-filter"
                : ""
            }`}
            alt="Transit status"
            title={settings ? settings.in_transit_status.label : ""}
          />
          <img
            src={`/assets/new-icons/warehouse.svg`}
            className={`status-svg-images ${
              isGreenStatus("In Stock", status.original, settings)
                ? " green-filter"
                : ""
            }`}
            alt="Warehouse status"
            title={settings ? settings.in_stock_status.label : ""}
          />
          <img
            src={`/assets/new-icons/config-screw.png`}
            className={`status-svg-images ${
              isGreenStatus("Config", status.original, settings)
                ? " green-filter"
                : ""
            }`}
            alt="In Configuration Status"
            title={settings ? settings.in_configuration_status.label : ""}
          />
        </div>
      ),
    },
    {
      accessor: "part_number",
      Header: "Part Number",
      width: 150,
      sortable: true,
      Cell: (cell) => <div> {`${cell.value ? cell.value : "N.A."}`}</div>,
    },
    {
      accessor: "serial_number",
      Header: "Serial",
      width: 150,
      sortable: true,
      Cell: (cell) => <div> {`${cell.value ? cell.value : "N.A."}`}</div>,
    },

    {
      accessor: "part_description",
      Header: "Part Description",
      sortable: false,
      Cell: (cell) => <div> {`${cell.value ? cell.value : "N.A."}`}</div>,
    },
    {
      accessor: "warehouse",
      Header: "Warehouse",
      width: 100,
      sortable: true,
      Cell: (cell) => <div> {`${cell.value ? cell.value : "Shipped"}`}</div>,
    },
    {
      accessor: "opportunity_name",
      Header: "Project Name",
      sortable: true,
      Cell: (cell) => <div> {`${cell.value ? cell.value : "N.A."}`}</div>,
    },
    {
      accessor: "customer_po",
      Header: "Customer PO",
      sortable: true,
      width: 120,
      Cell: (cell) => <div> {`${cell.value ? cell.value : "N.A."}`}</div>,
    },
  ];

  return (
    <>
      {customerCRMID && (
        <div className="vr-warehouse-container">
          <Spinner
            show={fetchStatus === FetchingStatus.FETCHING_DEVICE_DETAIL}
            className="warehouse-listing-loader"
          />
          <Table
            rows={rows}
            columns={columns}
            manualProps={manualProps}
            rowSelection={rowSelectionProps}
            customTopBar={renderTopBar()}
            className={`vr-warehouse-listing__table`}
            onRowClick={onRowClick}
            loading={fetchStatus === FetchingStatus.FETCHING_LIST}
          />
          <Filters
            show={showModal === ModalType.FILTER}
            onSubmit={applyFilters}
            filters={pagination.filters}
            onClose={() => setShowModal(null)}
          />
          {showModal === ModalType.DEVICE_DETAIL && (
            <DeviceDetails
              settings={settings}
              onClose={onCloseDeviceDetail}
              deviceDetails={deviceDetail}
            />
          )}
        </div>
      )}
      {showAllCustomers && <div id="hideCustomerSelection" />}
    </>
  );
};

export const isGreenStatus = (
  type: "Transit" | "In Stock" | "Config" | "Shipped",
  device: IVRWHDeviceDetail | IVRWarehouseRow,
  settings: IVRWarehouseSettings
): boolean => {
  if (settings) {
    switch (type) {
      case "Transit":
        return settings.in_transit_status.status === device.status;
      case "In Stock":
        return settings.in_stock_status.status === device.status;
      case "Config":
        return settings.in_configuration_status.status === device.status;
      case "Shipped":
        return settings.shipped_status.status
          ? settings.shipped_status.status === device.status
          : true;
      default:
        return false;
    }
  } else return false;
};

const mapStateToProps = (state: IReduxStore) => ({
  customerId: state.customer.customerId,
  customers: state.customer.customersShort,
  customerProfile: state.customerUser.customerProfile,
});

const mapDispatchToProps = (dispatch: any) => ({
  getStatusOptions: () => dispatch(getQuickbaseStatuses()),
  getWarehouseOptions: () => dispatch(getQuickbaseWarehouses()),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addWarningMessage: (msg: string) => dispatch(addWarningMessage(msg)),
  fetchSettings: () => dispatch(fetchOrUpdateVRWHSettings("get")),
  fetchDeviceDetail: (id: number) => dispatch(fetchVRWarehouseDeviceDetail(id)),
  fetchListing: (params: IServerPaginationParams & IVRWarehouseFilters) =>
    dispatch(fetchVRWarehouseDataListing(params)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VirtualWarehouseListing);
