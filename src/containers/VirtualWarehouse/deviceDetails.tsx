import React from "react";
import ModalBase from "../../components/ModalBase/modalBase";
import SquareButton from "../../components/Button/button";
import {
  fromISOStringToDateTimeString,
  fromISOStringToFormattedDate,
} from "../../utils/CalendarUtil";
import { isGreenStatus } from ".";

interface IDeviceDetailProps {
  onClose: () => void;
  settings: IVRWarehouseSettings;
  deviceDetails: IVRWHDeviceDetail;
}

const DeviceDetails: React.FC<IDeviceDetailProps> = ({
  onClose,
  settings,
  deviceDetails,
}) => {
  const getTitle = () => {
    let rightData: JSX.Element;
    if (isGreenStatus("Shipped", deviceDetails, settings)) {
      rightData = (
        <>
          <img
            src={`/assets/new-icons/shipped-truck.png`}
            className={`device-header-img`}
            alt="Shipped status"
          />
          <div className="success-label">
            {settings.shipped_status.label
              ? settings.shipped_status.label
              : "Shipped"}
          </div>
        </>
      );
    } else if (isGreenStatus("Transit", deviceDetails, settings)) {
      rightData = (
        <>
          <img
            src={`/assets/new-icons/delivery-truck.svg`}
            className={`device-header-img`}
            alt="Transit status"
          />
          <div className="success-label">
            {settings.in_transit_status.label}
          </div>
        </>
      );
    } else if (isGreenStatus("In Stock", deviceDetails, settings)) {
      rightData = (
        <>
          <img
            src={`/assets/new-icons/warehouse.svg`}
            className={`device-header-img`}
            alt="Warehouse status"
          />
          <div className="success-label">{settings.in_stock_status.label}</div>
        </>
      );
    } else if (isGreenStatus("Config", deviceDetails, settings)) {
      rightData = (
        <>
          <img
            src={`/assets/new-icons/config-screw.png`}
            className={`device-header-img`}
            alt="Configuration status"
          />
          <div className="success-label">
            {settings.in_configuration_status.label}
          </div>
        </>
      );
    } else {
      rightData = null;
    }

    return (
      <div className="device-details-modal-header">
        View Device Details
        {rightData && <div className="device-status-section">{rightData}</div>}
      </div>
    );
  };

  const getFooter = () => (
    <div className="device-details-modal-footer">
      <div className="updated-on">
        Last Synced on Acela:
        <p>
          {deviceDetails.updated_on
            ? fromISOStringToDateTimeString(deviceDetails.updated_on)
            : "-"}
        </p>
      </div>
      <SquareButton
        onClick={onClose}
        content="Close"
        bsStyle={"primary"}
      />
    </div>
  );

  const getBody = () => {
    const getString = (s: string | number, noData: string = "") =>
      s ? String(s) : noData;
    const isShipped = isGreenStatus("Shipped", deviceDetails, settings);

    return (
      <div className="device-details-modal-body">
        <div className="device-details-field">
          <label>Serial #</label>
          <label>{getString(deviceDetails.serial_number)}</label>
        </div>
        <div className="device-details-field">
          <label>Model</label>
          <label>{getString(deviceDetails.part_number)}</label>
        </div>
        <div className="device-details-field">
          <label>Manufacturer</label>
          <label>{getString(deviceDetails.manufacturer)}</label>
        </div>
        <div className="device-details-field">
          <label>{(!isShipped ? "Site / Location / " : "") + "Status"}</label>
          <label>
            {(!isShipped
              ? getString(deviceDetails.warehouse, "-") +
                " / " +
                getString(deviceDetails.location, "-") +
                " / "
              : "") + getString(deviceDetails.status, "-")}
          </label>
        </div>
        <div className="device-details-field">
          <label>Description</label>
          <label>{getString(deviceDetails.part_description)}</label>
        </div>
        <div className="device-details-field">
          <label>Received Date</label>
          <label>
            {getString(
              fromISOStringToFormattedDate(
                deviceDetails.consigned_date,
                "MM/DD/YYYY"
              )
            )}
          </label>
        </div>
        {isShipped && (
          <>
            <h4 className="device-detail-subheader">Shipping Detail</h4>
            <div className="device-details-field flex-basis-50">
              <label>Ship to Address</label>
              <label>{getString(deviceDetails.shipping_address, "-")}</label>
            </div>
            <div className="device-details-field flex-basis-50">
              <label>Tracking Number</label>
              {deviceDetails.tracking_number ||
              deviceDetails.carrier_tracking_url_mapping ? (
                <label
                  dangerouslySetInnerHTML={{
                    __html: deviceDetails.carrier_tracking_url_mapping
                      ? deviceDetails.carrier_tracking_url_mapping
                      : deviceDetails.tracking_number,
                  }}
                />
              ) : (
                <label>-</label>
              )}
            </div>
            <div className="device-details-field flex-basis-50">
              <label>Ship Date</label>
              <label>
                {getString(
                  fromISOStringToFormattedDate(
                    deviceDetails.shipping_date,
                    "MM/DD/YYYY"
                  )
                )}
              </label>
            </div>
            <div className="device-details-field flex-basis-50">
              <label>Carrier</label>
              <label>{getString(deviceDetails.carrier)}</label>
            </div>
          </>
        )}
        <h4 className="device-detail-subheader">Order Detail</h4>
        <div className="order-detail-left">
          <div className="device-details-field flex-basis-100">
            <label>Opportunity Name</label>
            <label>{getString(deviceDetails.opportunity_name)}</label>
          </div>
          <div className="device-details-field">
            <label>Customer PO</label>
            <label>{getString(deviceDetails.customer_po)}</label>
          </div>
          <div className="device-details-field">
            <label>Sales Order</label>
            <label>{getString(deviceDetails.sales_order_crm_id)}</label>
          </div>
          <div className="device-details-field">
            <label>LP PO</label>
            <label>{getString(deviceDetails.po_number)}</label>
          </div>
        </div>
        <div className="order-detail-right">
          <div className="device-details-field flex-basis-100">
            <label>Notes</label>
            <label>{getString(deviceDetails.notes)}</label>
          </div>
        </div>
      </div>
    );
  };

  return (
    <ModalBase
      show={true}
      onClose={onClose}
      titleElement={getTitle()}
      bodyElement={getBody()}
      footerElement={getFooter()}
      className="vrwh-device-details-modal"
    />
  );
};

export default DeviceDetails;
