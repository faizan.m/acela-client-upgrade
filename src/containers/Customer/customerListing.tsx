import React from "react";
import { connect } from "react-redux";
import { debounce } from "lodash";

import Input from "../../components/Input/input";
import Spinner from "../../components/Spinner";
import Table from "../../components/Table/table";
import {
  editCustomer,
  fetchCustomers,
  mergeCustomers,
  EDIT_CUSTOMER_FAILURE,
  EDIT_CUSTOMER_SUCCESS,
  MERGE_CUSTOMER_USER_SUCCESS,
  MERGE_CUSTOMER_USER_FAILURE,
} from "../../actions/customer";
import {
  getFeatureAccessUsers,
  ACCESS_FEATURE_USERS_SUCCESS,
} from "../../actions/providerAdminUser";
import { fetchCustomersShort } from "../../actions/customer";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import { allowPermission } from "../../utils/permissions";
import CustomerAccessForm from "./CustomerAccessForm";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import { phoneNumberInFormat } from "../../utils/CalendarUtil";
import Featurewise2FA from "../Featurewise2FA";
import { allowFeaturePermission } from "../../utils/permissions";
import "./style.scss";

interface IcustomerTable {
  id?: number;
  name: string;
  address: string;
  status: string;
  phone: string;
  primary_contact: string;
  collector_service_allowed: boolean;
}

interface ICustomerProps extends ICommonProps {
  user: ISuperUser;
  fetchCustomers: TFetchCustomers;
  fetchCustomersShort: () => Promise<any>;
  customers: ICustomerPaginated;
  customersShort: ICustomerShort[];
  editCustomer: TEditCustomer;
  isFetchingCustomers: boolean;
  prevLocation: string;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getFeatureAccessUsers: (feature: string) => Promise<any>;
  mergeCustomers: (
    sourceId: number,
    targetId: number,
    token: string
  ) => Promise<any>;
}
interface ICustomerState {
  rows: IcustomerTable[];
  selectedRows: number[];
  errorList?: any;
  customer?: Icustomer;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  customers: Icustomer[];
  showMergeModal: boolean;
  showTwoFAModal: boolean;
  isMergingCustomer: boolean;
  acquiredCustomer: number;
  resultingCustomer: number;
  displayRenew: boolean;
  renewCustomer: Icustomer;
  isPostingRenewal: boolean;
  enableForCustomer: string;
  featureAccessUsers: IFeatureAccess[];
}

class Customer extends React.Component<ICustomerProps, ICustomerState> {
  private debouncedFetch;
  constructor(props: ICustomerProps) {
    super(props);

    this.state = {
      rows: [],
      selectedRows: [],
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      customers: [],
      displayRenew: false,
      renewCustomer: null,
      isPostingRenewal: false,
      featureAccessUsers: [],
      enableForCustomer: "",
      showMergeModal: false,
      showTwoFAModal: false,
      isMergingCustomer: false,
      acquiredCustomer: null,
      resultingCustomer: null,
    };
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }
  componentDidMount() {
    this.fetchFeatureAccessList();
    if (!this.props.prevLocation.includes("/customer-configs/")) {
      this.props.fetchCustomers();
    }
  }

  componentDidUpdate(prevProps: ICustomerProps) {
    if (this.props.customers !== prevProps.customers) {
      this.setRows(this.props);
    }
  }

  setRows = (nextProps: ICustomerProps) => {
    const customersResponse = nextProps.customers;
    const customers: any[] = customersResponse.results;
    const rows: IcustomerTable[] = customers.map((customer, index) => ({
      name: customer.name ? customer.name : "N.A.",
      address: customer.address.address_1 ? customer.address.address_1 : "N.A.",
      phone:
        customer.profile && customer.profile.cell_phone_number
          ? customer.profile.cell_phone_number
          : "N.A.",
      primary_contact: customer.primary_contact
        ? customer.primary_contact
        : "N.A.",
      status: customer.is_active ? "Enabled" : "Disabled",
      id: customer.id,
      renew: customer.renewal_service_allowed,
      collector_service_allowed: customer.collector_service_allowed,
      config_compliance_service_allowed:
        customer.config_compliance_service_allowed,
      config_compliance_service_write_allowed:
        customer.config_compliance_service_write_allowed,
      index,
    }));

    this.setState((prevState) => ({
      rows,
      customers,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onRowClick = (rowInfo) => {
    this.props.history.push(`/customer-configs/${rowInfo.original.id}`);
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchCustomers(newParams);
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState((prevState) => ({
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };

  getCustomerOptions = (): IPickListOptions[] => {
    if (this.props.customersShort && this.props.customersShort.length > 0) {
      return this.props.customersShort.map((role) => ({
        value: role.id,
        label: role.name,
      }));
    } else {
      return [];
    }
  };

  fetchFeatureAccessList = () => {
    this.props.getFeatureAccessUsers("Merge Customer").then((action) => {
      if (action.type === ACCESS_FEATURE_USERS_SUCCESS) {
        this.setState({
          featureAccessUsers: action.response,
        });
      }
    });
  };

  renderTopBar = () => {
    return (
      <div className="customer-listing__actions header-panel">
        <Input
          field={{
            value: this.state.pagination.params.search,
            label: "",
            type: "SEARCH",
          }}
          width={4}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search by customer name"
          className="search"
        />
        {this.props.user &&
          allowFeaturePermission(
            this.props.user.id,
            "Merge Customer",
            this.state.featureAccessUsers
          ) && (
            <SquareButton
              content="Merge Customers"
              bsStyle={"primary"}
              className="merge-customer-btn"
              onClick={this.toggleMergeCustomerModal}
            />
          )}
      </div>
    );
  };

  closeRenew = () => {
    this.setState({
      displayRenew: false,
      renewCustomer: null,
    });
  };

  toggleMergeCustomerModal = () => {
    this.setState((prevState) => ({
      showMergeModal: !prevState.showMergeModal,
      acquiredCustomer: null,
      resultingCustomer: null,
    }));
  };

  handleChangeCustomer = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === "acquiredCustomer")
      this.setState({
        acquiredCustomer: Number(e.target.value),
      });
    else this.setState({ resultingCustomer: Number(e.target.value) });
  };

  toggleTwoFAModal = (token?: string) => {
    this.setState((prevState) => ({
      showTwoFAModal: !prevState.showTwoFAModal,
    }));
    if (token && typeof token === "string") this.handleMergeCustomer(token);
  };

  handleMergeCustomer = (token: string) => {
    this.setState(
      {
        isMergingCustomer: true,
      },
      () => {
        this.props
          .mergeCustomers(
            this.state.acquiredCustomer,
            this.state.resultingCustomer,
            token
          )
          .then((action) => {
            if (action.type === MERGE_CUSTOMER_USER_SUCCESS) {
              this.fetchData({ page: 1 });
              this.props.addSuccessMessage("Customers merged successfully!");
              this.props.fetchCustomersShort();
            } else if (action.type === MERGE_CUSTOMER_USER_FAILURE) {
              this.props.addErrorMessage("Error merging customers!");
            }
          })
          .finally(() => {
            this.toggleMergeCustomerModal();
            this.setState({ isMergingCustomer: false });
          });
      }
    );
  };

  openCompliance = (id, e: any) => {
    e.stopPropagation();
    const customer = this.state.customers.find(
      (cust) => cust.id === id.row._original.id
    );
    this.setState({
      displayRenew: true,
      renewCustomer: customer,
      enableForCustomer: "Compliance",
    });
  };

  onSubmitCustomerRenew = (customer: Icustomer) => {
    this.setState({ isPostingRenewal: true });
    this.props.editCustomer(customer.id.toString(), customer).then((action) => {
      if (action.type === EDIT_CUSTOMER_SUCCESS) {
        this.setState({
          displayRenew: false,
          renewCustomer: null,
          isPostingRenewal: false,
          enableForCustomer: "",
        });
        this.debouncedFetch();
      }
      if (action.type === EDIT_CUSTOMER_FAILURE) {
        this.setState({ isPostingRenewal: false });
      }
    });
  };

  getEnableDisableIcon = (value: boolean, obj) => {
    return (
      <div
        className="listing-icon"
        onClick={(e) => this.openCompliance(obj, e)}
      >
        {value && (
          <img
            src={`/assets/icons/active-right.svg`}
            alt=""
            title={`Enabled`}
            className="info-svg-images"
          />
        )}
        {!value && (
          <img
            src={`/assets/icons/HIGH.svg`}
            alt=""
            title={`Disabled`}
            className="info-svg-images"
          />
        )}
      </div>
    );
  };

  getReadWriteIcon = (colObj) => {
    const obj = colObj.original;
    return (
      <div
        className="listing-icon"
        onClick={(e) => this.openCompliance(colObj, e)}
      >
        {obj.config_compliance_service_write_allowed === true &&
          obj.config_compliance_service_allowed === true && (
            <img
              src={`/assets/icons/active-right.svg`}
              alt=""
              title={`Read/Write Full access`}
              className="info-svg-images"
            />
          )}
        {obj.config_compliance_service_write_allowed === false &&
          obj.config_compliance_service_allowed === true && (
            <img
              src={`/assets/icons/right-gray.svg`}
              alt=""
              title={`Read only access`}
              className="info-svg-images"
            />
          )}
        {obj.config_compliance_service_write_allowed === false &&
          obj.config_compliance_service_allowed === false && (
            <img
              src={`/assets/icons/HIGH.svg`}
              alt=""
              title={`Disabled`}
              className="info-svg-images"
            />
          )}
      </div>
    );
  };

  renderMergeCustomerModal = () => {
    return (
      <ModalBase
        show={this.state.showMergeModal}
        onClose={this.toggleMergeCustomerModal}
        titleElement={"Select the two customer entities to be merged"}
        bodyElement={
          <div className="customer-merge-modal-body">
            <div className="customer-merge-note">
              NOTE: All associated data of the acquired customer will be moved
              to the resulting customer in Acela
            </div>
            <div className="merge-cust-selection">
              <Input
                field={{
                  label: "Acquired Customer",
                  type: "PICKLIST",
                  value: this.state.acquiredCustomer,
                  options: this.getCustomerOptions(),
                  isRequired: true,
                }}
                width={6}
                multi={false}
                labelIcon="info"
                labelTitle={
                  "This customer will be deleted after merging in Acela"
                }
                name="acquiredCustomer"
                onChange={(e) => this.handleChangeCustomer(e)}
                placeholder={`Select Customer`}
              />
              <Input
                field={{
                  label: "Resulting Customer",
                  type: "PICKLIST",
                  value: this.state.resultingCustomer,
                  options: this.getCustomerOptions(),
                  isRequired: true,
                }}
                width={6}
                multi={false}
                name="resultingCustomer"
                labelIcon="info"
                labelTitle={
                  "This customer will be retained after merging in Acela"
                }
                onChange={(e) => this.handleChangeCustomer(e)}
                placeholder={`Select Customer`}
              />
            </div>
          </div>
        }
        footerElement={
          <>
            <SquareButton
              onClick={this.toggleMergeCustomerModal}
              content={"Cancel"}
              bsStyle={"default"}
            />
            <SquareButton
              onClick={() => this.toggleTwoFAModal()}
              content={"Merge"}
              bsStyle={"primary"}
              disabled={
                this.state.isMergingCustomer ||
                !this.state.acquiredCustomer ||
                !this.state.resultingCustomer ||
                this.state.acquiredCustomer === this.state.resultingCustomer
              }
            />
          </>
        }
        className={`merge-customer-modal`}
      />
    );
  };

  render() {
    const columns: any = [
      {
        accessor: "name",
        Header: "Customer Name",
        id: "name",
        Cell: (name) => <div className="pl-15">{name.value}</div>,
      },
      { accessor: "address", Header: "Address", id: "address__address_1" },
      {
        accessor: "phone",
        Header: "Phone",
        width: 120,
        id: "phone",
        Cell: (phone) => <div>{phoneNumberInFormat("", phone.value)}</div>,
      },
      {
        accessor: "primary_contact",
        Header: "Primary Contact",
        width: 180,
        id: "primary_contact",
      },
      {
        accessor: "renew",
        Header: "Renewal",
        sortable: false,
        show: allowPermission("renewal_enable_disable"),
        Cell: (c) => this.getEnableDisableIcon(c.value, c),
        width: 90,
      },
      {
        accessor: "config_compliance_service_allowed",
        Header: "Compliance",
        sortable: false,
        show: allowPermission("renewal_enable_disable"),
        Cell: (c) => this.getReadWriteIcon(c),
        width: 90,
      },
      {
        accessor: "collector_service_allowed",
        Header: "Collector",
        sortable: false,
        show: allowPermission("collector_enable_disable"),
        Cell: (c) => this.getEnableDisableIcon(c.value, c),
        width: 90,
      },
      {
        accessor: "status",
        id: "is_active",
        Header: "Status",
        Cell: (status) => (
          <div className={`status status--${status.value}`}>{status.value}</div>
        ),
        width: 130,
      },
    ];
    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: "id",
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };

    return (
      <div className="customer-listing-container">
        {this.state.showMergeModal && this.renderMergeCustomerModal()}
        {this.state.showTwoFAModal && (
          <Featurewise2FA onClose={this.toggleTwoFAModal} />
        )}
        <h3>Customer Configuration</h3>
        <div className="loader">
          <Spinner show={this.props.isFetchingCustomers} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetchingCustomers ? `loading` : ``
          }`}
          onRowClick={this.onRowClick}
          loading={this.props.isFetchingCustomers}
          manualProps={manualProps}
        />
        {this.state.displayRenew && 
          <CustomerAccessForm
            user={this.state.renewCustomer}
            onClose={this.closeRenew}
            onSubmit={this.onSubmitCustomerRenew}
            isLoading={this.state.isPostingRenewal}
          />
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customers: state.customer.customers,
  customersShort: state.customer.customersShort,
  isFetchingCustomers: state.customer.isFetchingCustomers,
  prevLocation: state.appState.prevLocation,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchCustomersShort: () => dispatch(fetchCustomersShort()),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  getFeatureAccessUsers: (feature: string) =>
    dispatch(getFeatureAccessUsers(feature)),
  fetchCustomers: (params?: IServerPaginationParams) =>
    dispatch(fetchCustomers(params)),
  editCustomer: (id: string, customer: Icustomer) =>
    dispatch(editCustomer(id, customer)),
  mergeCustomers: (sourceId: number, targetId: number, token: string) =>
    dispatch(mergeCustomers(sourceId, targetId, token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Customer);
