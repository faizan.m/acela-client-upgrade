import { debounce, isEqual } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import {
  activateCustomerUser,
  CREATE_CUSTOMERS_USER_FAILURE,
  CREATE_CUSTOMERS_USER_SUCCESS,
  EDIT_CUSTOMER_FAILURE,
  EDIT_CUSTOMER_SUCCESS,
  EDIT_CUSTOMER_USER_FAILURE,
  EDIT_CUSTOMER_USER_SUCCESS,
  editCustomer,
  editCustomerUser,
  fetchCustomerUsers,
  fetchSingleCustomer,
} from '../../actions/customer';
import { fetchUserTypes } from '../../actions/userType';
import BackButton from '../../components/BackLink';
import SquareButton from '../../components/Button/button';
import EditButton from '../../components/Button/editButton';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';
import CustomerAccessForm from './CustomerAccessForm';
import CustomerForm from './customerForm';
import UserDetails from '../User/userDetails';
import UserActivationForm from './UserActivation';
import moment from 'moment';
import { allowPermission } from '../../utils/permissions';
import { isValue } from '../../utils/CommonUtils';
import { phoneNumberInFormat } from '../../utils/CalendarUtil';
import './style.scss';
import LogoUpload from './LogoUpload';

interface ICustomerDetailsProps extends ICommonProps {
  fetchSingleCustomer: TFetchSingleCustomer;
  fetchCustomerUsers: TFetchSingleCustomer;
  editCustomerUser: TEditCustomerUser;
  activateCustomerUser: TactivateCustomerUser;
  editCustomer: TEditCustomer;
  customer: Icustomer;
  users: ISuperUserPaginated;
  fetchUserTypes: TFetchUserTypes;
  userTypes: IUserTypeRole[];
  isFetchingCustomer: boolean;
  isFetchingUsers: boolean;
}

interface ICustomerDetailsState {
  user: any;
  customerEdit: any;
  showImageModal: boolean;
  isCustomerUserFormOpen: boolean;
  isCustomerAccessFormOpen: boolean;
  isUserActivationFormOpen: boolean;
  isCustomerDetailsModalOpen: boolean;
  customer: Icustomer;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  users: ISuperUser[];
  rows: any[];
  isActivatingUser: boolean;
  isEditingUser: boolean;
  reset: boolean;
  isEditingCustomer: boolean;
  emailActivatingIndex: number;
  isPasswordSet: boolean,
}

class CustomerDetails extends React.Component<
  ICustomerDetailsProps,
  ICustomerDetailsState
  > {
  private debouncedFetch;
  constructor(props: ICustomerDetailsProps) {
    super(props);

    this.state = {
      user: null,
      users: [],
      customer: null,
      customerEdit: null,
      showImageModal: false,
      isCustomerUserFormOpen: false,
      isCustomerAccessFormOpen: false,
      isUserActivationFormOpen: false,
      isCustomerDetailsModalOpen: false,
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      rows: [],
      isActivatingUser: false,
      isEditingUser: false,
      isEditingCustomer: false,
      reset: false,
      emailActivatingIndex: null,
      isPasswordSet: false,
    };
    this.debouncedFetch = debounce(this.fetchData, 500);
  }

  componentDidMount() {
    const newCustomerId = this.props.match.params.customerId;

    if (newCustomerId) {
      this.props.fetchCustomerUsers(newCustomerId);
      this.props.fetchSingleCustomer(newCustomerId);
      this.props.fetchUserTypes();
    }
  }

  static getDerivedStateFromProps(nextProps: ICustomerDetailsProps, prevState: ICustomerDetailsState) {
    if (!isEqual(nextProps.customer, prevState.customer)) {
      return {
        customer: nextProps.customer,
      };
    }
    return null;
  }

  componentDidUpdate(prevProps: ICustomerDetailsProps) {
    if (this.props.users !== prevProps.users) {
      this.setRows(this.props);
    }
  }

  setRows = (nextProps: ICustomerDetailsProps) => {
    const customersResponse = nextProps.users;
    const users: ISuperUser[] = customersResponse.results;
    const rows: any[] = users.map((user, index) => ({
      first_name: `${user.first_name} ${user.last_name}`,
      email: user.email,
      phone_number: user.profile.cell_phone_number,
      country_code: user.profile.country_code,
      role: user.role_display_name,
      can_edit: user.can_edit,
      is_password_set: user.is_password_set,
      status: user.is_active ? 'Enabled' : 'Disabled',
      action: this.getActivationButton(user),
      index,
    }));

    this.setState(prevState => ({
      reset: false,
      rows,
      users,
      pagination: {
        ...prevState.pagination,
        totalRows: customersResponse.count,
        currentPage: customersResponse.links.page_number - 1,
        totalPages: Math.ceil(
          customersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  toggleCustomerAccessForm = () => {
    this.setState(prevState => ({
      isCustomerAccessFormOpen: !prevState.isCustomerAccessFormOpen,
    }));
  };

  toggleUserActivationForm = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState(prevState => ({
      isUserActivationFormOpen: !prevState.isUserActivationFormOpen,
      user: prevState.users[userIndex],
    }));
  };

  toggleUserActivation = () => {
    this.setState(prevState => ({
      isUserActivationFormOpen: !prevState.isUserActivationFormOpen,
    }));
  };

  toggleCustomerUserForm = (userIndex: number, event: any, isPasswordSet) => {
    event.stopPropagation();
    this.setState(prevState => ({
      isCustomerUserFormOpen: !prevState.isCustomerUserFormOpen,
      user: prevState.users[userIndex],
      isPasswordSet: !isPasswordSet
    }));
  };

  toggleCustomerUser = () => {
    this.setState(prevState => ({
      isCustomerUserFormOpen: !prevState.isCustomerUserFormOpen,
    }));
  };

  toggleCustomerDetailsModal = () => {
    this.setState(prevState => ({
      isCustomerDetailsModalOpen: !prevState.isCustomerDetailsModalOpen,
    }));
  };

  getActivationButton = user => {
    let actionType = 'showActive';
    if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count === 0
    ) {
      actionType = 'showActivate';
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 0 &&
      user.activation_mails_sent_count < 6
    ) {
      actionType = 'showResend';
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 5
    ) {
      actionType = 'showResendStop';
    }

    return actionType;
  };

  onclickSendMail = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState(
      {
        user: this.state.users[userIndex],
        emailActivatingIndex: userIndex,
      },
      () => {
        const newCustomerId = this.props.match.params.customerId;

        this.props
          .activateCustomerUser(newCustomerId, this.state.user)
          .then(action => {
            this.setState({ emailActivatingIndex: null });
            this.debouncedFetch();;
          })
          .catch(() => {
            this.setState({ emailActivatingIndex: null });
          });
      }
    );
  };

  onRowClick = rowInfo => {
    event.stopPropagation();
    const providerUserIndex = rowInfo.original.index;

    this.setState({
      isCustomerDetailsModalOpen: true,
      user: this.state.users[providerUserIndex],
    });
  };

  renderTopBar = () => {
    return (
      <div className="customer-user__listing-actions header-panel">
        <Input
          field={{
            value: this.state.pagination.params.search,
            label: '',
            type: "SEARCH",
          }}
          width={4}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search Users"
          className="search"
        />
      </div>
    );
  };

  onEditCustomerUser = customerUser => {
    const newCustomerId = this.props.match.params.customerId;
    this.setState({ isEditingUser: true });

    this.props.editCustomerUser(newCustomerId, customerUser).then(action => {
      if (action.type === EDIT_CUSTOMER_USER_SUCCESS) {
        this.debouncedFetch();
        this.setState(prevState => ({
          isCustomerUserFormOpen: false,
          isEditingUser: false,
        }));
      }
      if (action.type === EDIT_CUSTOMER_USER_FAILURE) {
        this.setState(prevState => ({
          isCustomerUserFormOpen: true,
          isEditingUser: false,
        }));
      }
    });
  };

  onEditCustomeAccess = customer => {
    const newCustomerId = this.props.match.params.customerId;
    this.setState({ isEditingCustomer: true });

    this.props.editCustomer(newCustomerId, customer).then(action => {
      if (action.type === EDIT_CUSTOMER_SUCCESS) {
        this.setState(prevState => ({
          isCustomerAccessFormOpen: false,
          isEditingCustomer: false,
        }));
      }
      if (action.type === EDIT_CUSTOMER_FAILURE) {
        this.debouncedFetch();;
        this.setState(prevState => ({
          isCustomerAccessFormOpen: true,
          isEditingCustomer: false,
        }));
      }
    });
  };

  onSubmitCustomerUSerActivate = user => {
    const newCustomerId = this.props.match.params.customerId;
    this.setState({ isActivatingUser: true });

    this.props.activateCustomerUser(newCustomerId, user).then(action => {
      if (action.type === CREATE_CUSTOMERS_USER_SUCCESS) {
        this.setState(prevState => ({
          isUserActivationFormOpen: false,
          isActivatingUser: false,
        }));
      }
      if (action.type === CREATE_CUSTOMERS_USER_FAILURE) {
        this.setState(prevState => ({
          isUserActivationFormOpen: true,
          isActivatingUser: false,
        }));
      }
      this.debouncedFetch();;
    });
  };

  getCustomerUserRoles = () => {
    const userTypes = this.props.userTypes;
    if (userTypes) {
      const superUserType = userTypes.find(
        userType => userType.user_type === 'customer'
      );

      return superUserType.roles.map(role => ({
        value: role.id,
        label: role.display_name,
      }));
    }

    return [];
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    const customerId = this.props.match.params.customerId;
    this.props.fetchCustomerUsers(customerId, newParams);
  };

  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }
    // set page to 1 for
    // filtering
    this.debouncedFetch({ page: 1 });
  };

  getEnableDisableIcon = (value: boolean) => {
    return (
      <div className="listing-icon" onClick={e => this.toggleCustomerAccessForm()}>
        {value && (
          <img
            src={`/assets/icons/active-right.svg`}
            alt=""
            title={`Enabled`}
            className="info-svg-images"
          />
        )}
        {!value && (
          <img
            src={`/assets/icons/HIGH.svg`}
            alt=""
            title={`Disabled`}
            className="info-svg-images"
          />
        )}
      </div>
    )
  }

  toggleLogoModal = () => {
    this.setState((prev) => ({ showImageModal: !prev.showImageModal }));
  };


  getReadWriteIcon = (obj) => {
    return (
      <div className="listing-icon" onClick={e => this.toggleCustomerAccessForm()}>
        {
          obj && obj.config_compliance_service_write_allowed === true && obj.config_compliance_service_allowed === true
          &&
          <img
            src={`/assets/icons/active-right.svg`}
            alt=""
            title={`Read/Write Full access`}
            className="info-svg-images"
          />
        }
        {
          obj && obj.config_compliance_service_write_allowed === false && obj.config_compliance_service_allowed === true
          && (
            <img
              src={`/assets/icons/right-gray.svg`}
              alt=""
              title={`Read only access`}
              className="info-svg-images"
            />
          )}
        {obj && obj.config_compliance_service_write_allowed === false && obj.config_compliance_service_allowed === false
          && (
            <img
              src={`/assets/icons/HIGH.svg`}
              alt=""
              title={`Disabled`}
              className="info-svg-images"
            />
          )}
      </div>
    )
  }

  renderCustomerDetails = () => {
    const customer = this.state.customer;

    return (
      <div className="customer-user__details">
        <div className="customer-user__details-header">
          <div className="login">
            <h5>Customer Details</h5>
            {customer &&
              customer.last_logged_in_user &&
              customer.last_logged_in_user.name && (
                <p>
                  (Last Login by {customer.last_logged_in_user.name} :
                  {moment(customer.last_logged_in_user.last_login).format(
                    'MM/DD/YYYY h:mm a'
                  )}
                  )
                </p>
              )}
          </div>

          {allowPermission('edit_customer_details') && (
            <div className='details-right'>
              <SquareButton
                content="Edit Logo"
                onClick={this.toggleLogoModal}
                bsStyle={"primary"}
                className='edit-img-btn'
              />
              <SquareButton
                content="Edit Access"
                onClick={this.toggleCustomerAccessForm}
                bsStyle={"primary"}
                disabled={this.props.isFetchingCustomer}
                className='edit-img-btn'
              />
            </div>
          )}
        </div>
        <Spinner show={this.props.isFetchingCustomer} />
        {customer && (
          <div className="customer-user__details-body">
            <div className="customer-user__details-field">
              <label>CUSTOMER NAME</label>
              <label>{`${customer.name}`}</label>
            </div>
            <div className="customer-user__details-field">
              <label>ADDRESS</label>
              <label>
                {customer.address &&
                  `${isValue(customer.address.address_1)}
                   ${isValue(customer.address.address_2)}
                   ${isValue(customer.address.city)}
                   ${isValue(customer.address.state)}
                   ${isValue(customer.address.zip_code)}`}
              </label>
            </div>
            <div className="customer-user__details-field">
              <label>PHONE</label>
              <label>{phoneNumberInFormat(customer.country_code, customer.phone)}</label>
            </div>
            <div className="customer-user__details-field">
              <label>PRIMARY CONTACT</label>
              <label>{customer.primary_contact}</label>
            </div>
            <div className="customer-user__details-field">
              <label># USERS</label>
              <label>{customer.users_count}</label>
            </div>
            <div className="customer-user__details-field">
              <label># DEVICES</label>
              <label>{customer.devices_count}</label>
            </div>
            <div className="customer-user__details-field">
              <label>STATUS</label>
              <label>{this.getEnableDisableIcon(customer.is_active)}</label>
            </div>
            <div className="customer-user__details-field">
              <label>IS MANAGED SERVICE CUSTOMER</label>
              <label>{customer.is_ms_customer ? 'Yes' : 'No'}</label>
            </div>
            <div className="customer-user__details-field">
              <label>COMPLIANCE</label>
              <label>{this.props.customer !== null && this.getReadWriteIcon(this.props.customer)}</label>
            </div>
            <div className="customer-user__details-field">
              <label>RENEWAL</label>
              <label>{this.getEnableDisableIcon(customer.renewal_service_allowed)}</label>
            </div>
            <div className="customer-user__details-field">
              <label>COLLECTOR</label>
              <label>{this.getEnableDisableIcon(customer.collector_service_allowed)}</label>
            </div>
            <div className="customer-user__details-field">
              <label>CUSTOMER ORDER TRACKING</label>
              <label>{this.getEnableDisableIcon(customer.customer_order_visibility_allowed)}</label>
            </div>
            <div className="customer-user__details-field">
              <label>PURCHASE HISTORY</label>
              <label>{this.getEnableDisableIcon(customer.purchase_order_history_visibility_allowed)}</label>
            </div>
          </div>
        )}
      </div>
    );
  };

  render() {
    const columns: ITableColumn[] = [
      {
        accessor: 'first_name',
        Header: 'Name',
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      { accessor: 'email', Header: 'Email Address' },
      {
        accessor: 'phone_number', Header: 'Phone',
        Cell: n => (
          <div> {phoneNumberInFormat(n.original.country_code, n.original.phone_number)}
          </div>
        )
      },
      { accessor: 'role', Header: 'Privilege Level', id: 'role' },
      {
        accessor: 'action',
        Header: 'User Activation',
        sortable: false,
        Cell: action => (
          <div>
            {action.value === 'showActivate' &&
              allowPermission('customer_config_user_activate') && (
                <SquareButton
                  content="Activate"
                  className="activate-btn"
                  onClick={e => this.toggleUserActivationForm(action.index, e)}
                  bsStyle={"primary"}
                />
              )}
            {action.value === 'showResend' &&
              action.index !== this.state.emailActivatingIndex && (
                <SquareButton
                  content="Resend e-Mail"
                  onClick={e => this.onclickSendMail(action.index, e)}
                  bsStyle={"primary"}
                  className="activate-btn"
                />
              )}
            {action.value === 'showResend' &&
              action.index === this.state.emailActivatingIndex && (
                <Spinner show={true} className="email__spinner" />
              )}
            {action.value === 'showResendStop' && (
              <div>No more Emails can sent</div>
            )}
            {action.value === 'showActive' && <div>Activated</div>}
          </div>
        ),
      },
      {
        accessor: 'status',
        Header: 'Status',
        id: 'is_active',
        Cell: status => (
          <div className={`status status--${status.value}`}>{status.value}</div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Edit',
        sortable: false,
        show: allowPermission('edit_customer_user'),
        Cell: cell =>
          cell.original.can_edit && (
            <div
              title={`${cell.original.is_password_set ? `` : 'Please activate User to edit'
                }`}
              className={`${cell.original.is_password_set ? `` : 'disable-div'}`}
            >
              <EditButton
                onClick={e => this.toggleCustomerUserForm(cell.value, e, cell.original.is_password_set)}
              />
            </div>
          ),
      },
    ];
    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: 'id',
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      reset: this.state.reset,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };

    return (
      <div className="customer-user">
        <BackButton
          path="/customer-configs"
          name="Back to Customer configuration"
        />
        {this.renderCustomerDetails()}
        <div className="customer-user__listing">
          <div className="customer-user__listing-header">
            <h5>Customer Users</h5>
          </div>
          <div className="customer-user__listing-loader">
            <Spinner show={this.props.isFetchingUsers} />
          </div>
          <Table
            columns={columns}
            rows={this.state.rows}
            rowSelection={rowSelectionProps}
            customTopBar={this.renderTopBar()}
            className={`customer-user__listing-table ${
              this.props.isFetchingUsers ? `loading` : ``
            }`}
            onRowClick={this.onRowClick}
            manualProps={manualProps}
            loading={this.props.isFetchingUsers}
          />
        </div>
        <CustomerForm
          user={this.state.user}
          onClose={this.toggleCustomerUser}
          show={this.state.isCustomerUserFormOpen}
          onSubmit={this.onEditCustomerUser}
          userRoles={this.getCustomerUserRoles()}
          isStatusEditable={this.state.isPasswordSet}
          userType="Acelevar"
          isLoading={this.state.isEditingUser}
        />
        {this.state.isCustomerAccessFormOpen && (
          <CustomerAccessForm
            user={this.state.customer}
            onClose={this.toggleCustomerAccessForm}
            onSubmit={this.onEditCustomeAccess}
            isLoading={this.state.isEditingCustomer}
          />
        )}
        <UserActivationForm
          user={this.state.user}
          onClose={this.toggleUserActivation}
          show={this.state.isUserActivationFormOpen}
          onSubmit={this.onSubmitCustomerUSerActivate}
          customerUserRoles={this.getCustomerUserRoles()}
          isLoading={this.state.isActivatingUser}
        />
        <UserDetails
          show={this.state.isCustomerDetailsModalOpen}
          user={this.state.user}
          onClose={this.toggleCustomerDetailsModal}
        />
        {this.state.showImageModal && (
          <LogoUpload
            show={true}
            onClose={this.toggleLogoModal}
            customerId={Number(this.props.match.params.customerId)}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customer: state.customer.customer,
  users: state.customer.users,
  userTypes: state.userType.userTypes,
  isFetchingCustomer: state.customer.isFetchingCustomer,
  isFetchingUsers: state.customer.isFetchingUsers,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSingleCustomer: (id: string) => dispatch(fetchSingleCustomer(id)),
  fetchCustomerUsers: (id: number, params?: IServerPaginationParams) =>
    dispatch(fetchCustomerUsers(id, params)),
  editCustomerUser: (id: string, user: ISuperUser) =>
    dispatch(editCustomerUser(id, user)),
  editCustomer: (id: string, customer: Icustomer) =>
    dispatch(editCustomer(id, customer)),
  activateCustomerUser: (id: number, user: ISuperUser) =>
    dispatch(activateCustomerUser(id, user)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerDetails);
