import React from 'react';
import _ from 'lodash';
import { cloneDeep } from 'lodash';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import AppValidators from '../../utils/validator';
import { getCountryCodes } from '../../utils/CalendarUtil';
import './style.scss';

interface ICustomerFormState {
  user: any;
  error: {
    cco_id: IFieldValidation;
  }
}

interface ICustomerFormProps {
  show: boolean;
  onClose: (e: any) => void;
  user?: ISuperUser;
  onSubmit: (user: ISuperUser) => void;
  userType: string;
  userRoles: Array<{ value: number; label: string }>;
  isLoading: boolean;
  isStatusEditable: boolean;
}

export default class CustomerForm extends React.Component<
  ICustomerFormProps,
  ICustomerFormState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  static emptyState: ICustomerFormState = {
    user: null,
    error: {
      cco_id: {
        ...CustomerForm.emptyErrorState,
      }
    },
  };

  constructor(props: ICustomerFormProps) {
    super(props);

    this.state = {
      ...CustomerForm.emptyState,
    };
  }

  componentDidUpdate(prevProps: ICustomerFormProps) {
    if (prevProps.user !== this.props.user) {
      this.setState({
        user: this.props.user,
      });
    }
  }

  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.user[event.target.name] = event.target.value;

    if (event.target.name === "cco_id") {
      newState.user.profile[event.target.name] = event.target.value;
      newState.error.cco_id.errorState = "success";
      newState.error.cco_id.errorMessage = '';
    }

    this.setState(newState);
  };

  handleChangeProfile = (event: any) => {
    const newState = cloneDeep(this.state);
    newState.user.profile[event.target.name] = event.target.value;
    this.setState(newState);
  };

  handleIsActiveChange = (event: any) => {
    const targetValue = event.target.value === 'Enable' ? true : false;
    const newState = cloneDeep(this.state);
    newState.user.is_active = targetValue;

    this.setState(newState);
  };

  onClose = e => {
    this.setState({
      ...CustomerForm.emptyState,
    });

    this.props.onClose(e);
  };

  onSubmit = () => {
    if (!this.validateForm()) return;

    const user = this.state.user;

    if (user.profile.cco_id && user.profile.cco_id !== "") {
      user.profile.cco_id_acknowledged = true;
    }

    this.props.onSubmit(user);

    this.setState({
      error: { ...CustomerForm.emptyState.error }
    });
  };

  validateForm = () => {

    const newState: ICustomerFormState = cloneDeep(this.state);

    let isValid = true;

    if (_.get(this.state.user, 'profile.cco_id') && !AppValidators.noSpaceAllowed(_.get(this.state.user, 'profile.cco_id'))) {
      newState.error.cco_id.errorState = "error";
      newState.error.cco_id.errorMessage = 'No Spaces allowed';

      isValid = false;
    }

    this.setState(newState);

    return isValid;
  };

  getBody = () => {
    return this.state.user ? (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isLoading} />
        </div>
        <div
          className={`user-modal__body ${this.props.isLoading ? `loading` : ''
            }`}
        >
          <Input
            field={{
              label: 'Name',
              type: "TEXT",
              isRequired: false,
              value: `${this.state.user.first_name} ${this.state.user.last_name}`,
            }}
            disabled={true}
            width={6}
            name="name"
            onChange={null}
          />
          <Input
            field={{
              label: 'Email Address',
              type: "TEXT",
              isRequired: false,
              value: this.state.user.email,
            }}
            disabled={true}
            width={6}
            placeholder=""
            name="email"
            onChange={null}
          />

          <Input
            field={{
              value: this.state.user.user_role,
              label: 'Role',
              type: "PICKLIST",
              isRequired: true,
              options: this.props.userRoles,
            }}
            width={6}
            name="user_role"
            onChange={this.handleChange}
            placeholder="Select Role"
          />
          <Input
            field={{
              label: 'CCO ID',
              type: "TEXT",
              isRequired: false,
              value: _.get(this.state.user, 'profile.cco_id'),
            }}
            error={this.state.error.cco_id}
            width={6}
            placeholder="Enter CCO ID"
            name="cco_id"
            onChange={this.handleChange}
          />
          <Input
            field={{
              label: 'Country code',
              isRequired: false,
              type: "PICKLIST",
              options: getCountryCodes(),
              value: this.state.user.profile.country_code,
            }}
            width={6}
            placeholder="code"
            name="country_code"
            onChange={this.handleChangeProfile}
            className="country-code"
          />
          <Input
            field={{
              label: 'Cell Phone Number',
              type: "TEXT",
              isRequired: false,
              value: _.get(this.state.user.profile, 'cell_phone_number'),
            }}
            width={6}
            placeholder="Enter cell phone number"
            name="cell_phone_number"
            onChange={this.handleChangeProfile}
          />
          <Input
            field={{
              label: 'Office Country code',
              isRequired: false,
              type: "PICKLIST",
              options: getCountryCodes(),
              value: this.state.user.profile.office_phone_country_code,
            }}
            width={6}
            placeholder="code"
            name="office_phone_country_code"
            onChange={this.handleChangeProfile}
            className="country-code"
          />
          <Input
            field={{
              label: 'Office Phone Number',
              type: "TEXT",
              isRequired: false,
              value: _.get(this.state.user.profile, 'office_phone'),
            }}
            width={6}
            placeholder="Enter office phone number"
            name="office_phone"
            onChange={this.handleChangeProfile}

          />

          <Input
            field={{
              value: this.state.user.is_active ? 'Enable' : 'Disable',
              label: 'Status',
              type: "RADIO",
              isRequired: false,
              options: [
                { value: 'Enable', label: 'Enable' },
                { value: 'Disable', label: 'Disable' },
              ],
            }}
            width={6}
            name="is_active"
            className={this.props.isStatusEditable ? 'disabled-status-edit' : ''}
            onChange={this.handleIsActiveChange}
            disabled={this.props.isStatusEditable}
          />

        </div>
      </div>
    ) : null;
  };

  getFooter = () => {
    return (
      <div
        className={`user-modal__footer
      ${this.props.isLoading ? `loading` : ''}`}
      >
        <SquareButton
          onClick={this.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Save Changes"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    const title = 'Edit user details';

    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={title}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="user-modal"
      />
    );
  }
}
