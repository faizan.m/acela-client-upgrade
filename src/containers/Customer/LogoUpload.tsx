import React, { DependencyList, useEffect, useRef, useState } from "react";
import axios from "axios";
import { connect } from "react-redux";
import ReactCrop, {
  Crop,
  PixelCrop,
  centerCrop,
  makeAspectCrop,
} from "react-image-crop";
import {
  customerLogoCRUD,
  CUSTOMER_LOGO_FAILURE,
  CUSTOMER_LOGO_SUCCESS,
} from "../../actions/customer";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import "react-image-crop/dist/ReactCrop.css";

const TO_RADIANS = Math.PI / 180;

async function canvasPreview(
  image: HTMLImageElement,
  canvas: HTMLCanvasElement,
  crop: PixelCrop,
  scale = 1,
  rotate = 0
) {
  const ctx = canvas.getContext("2d");

  if (!ctx) {
    throw new Error("No 2d context");
  }

  const scaleX = image.naturalWidth / image.width;
  const scaleY = image.naturalHeight / image.height;
  // devicePixelRatio slightly increases sharpness on retina devices
  // at the expense of slightly slower render times and needing to
  // size the image back down if you want to download/upload and be
  // true to the images natural size.
  const pixelRatio = window.devicePixelRatio;
  // const pixelRatio = 1

  canvas.width = Math.floor(crop.width * scaleX * pixelRatio);
  canvas.height = Math.floor(crop.height * scaleY * pixelRatio);

  ctx.scale(pixelRatio, pixelRatio);

  const cropX = crop.x * scaleX;
  const cropY = crop.y * scaleY;

  const rotateRads = rotate * TO_RADIANS;
  const centerX = image.naturalWidth / 2;
  const centerY = image.naturalHeight / 2;

  ctx.save();

  // 5) Move the crop origin to the canvas origin (0,0)
  ctx.translate(-cropX, -cropY);
  // 4) Move the origin to the center of the original position
  ctx.translate(centerX, centerY);
  // 3) Rotate around the origin
  ctx.rotate(rotateRads);
  // 2) Scale the image
  ctx.scale(scale, scale);
  // 1) Move the center of the image to the origin (0,0)
  ctx.translate(-centerX, -centerY);
  ctx.drawImage(
    image,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight,
    0,
    0,
    image.naturalWidth,
    image.naturalHeight
  );

  ctx.restore();
}

function useDebounceEffect(
  fn: () => void,
  waitTime: number,
  deps?: DependencyList
) {
  useEffect(() => {
    const t = setTimeout(() => {
      fn.apply(undefined, deps);
    }, waitTime);

    return () => {
      clearTimeout(t);
    };
  }, deps);
}

// This is to demonstate how to make and center a % aspect crop
// which is a bit trickier so we use some helper functions.
function centerAspectCrop(
  mediaWidth: number,
  mediaHeight: number,
  aspect: number
) {
  return centerCrop(
    makeAspectCrop(
      {
        unit: "px",
        width: mediaWidth,
      },
      aspect,
      mediaWidth,
      mediaHeight
    ),
    mediaWidth,
    mediaHeight
  );
}

interface ImageUploadProps {
  show: boolean;
  customerId: number;
  onClose: (save?: boolean) => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  customerLogoCRUD: (
    customerId: number,
    method: HTTPMethods,
    data?: FormData
  ) => Promise<any>;
}

const LogoUpload: React.FC<ImageUploadProps> = (props) => {
  const blobUrlRef = useRef("");
  const [crop, setCrop] = useState<Crop>();
  const [image, setImage] = useState<File>();
  const imgRef = useRef<HTMLImageElement>(null);
  const [imgSrc, setImgSrc] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const hiddenAnchorRef = useRef<HTMLAnchorElement>(null);
  const previewCanvasRef = useRef<HTMLCanvasElement>(null);
  const [isExisting, setIsExisting] = useState<boolean>(false);
  const [completedCrop, setCompletedCrop] = useState<PixelCrop>();

  useEffect(() => {
    fetchCustomerLogo();
  }, []);

  const fetchCustomerLogo = () => {
    props.customerLogoCRUD(props.customerId, "get").then((action) => {
      if (action.type === CUSTOMER_LOGO_SUCCESS) {
        if (action.response.file_path) {
          setIsExisting(true);
          const imageData: IFileResponse = action.response;
          setLoading(true);
          axios
            .get(imageData.file_path, { responseType: "blob" })
            .then((response) => {
              const imageBlob = response.data;

              // Create a File object from the image blob
              const file = new File([imageBlob], imageData.file_name, {
                type: imageBlob.type,
              });
              setImage(file);
              const reader = new FileReader();
              reader.addEventListener("load", () => {
                setImgSrc(reader.result ? reader.result.toString() : "");
              });
              reader.readAsDataURL(file);
            })
            .catch((err) => {
              console.log("Error fetching image from URL: \n", err);
            })
            .finally(() => setLoading(false));
        } else {
          setIsExisting(false);
          setImage(null);
          setCrop(null);
          setImgSrc(null);
        }
      } else if (action.type === CUSTOMER_LOGO_FAILURE) {
        props.addErrorMessage("Error from server");
      }
    });
  };

  function onSelectFile(e: React.ChangeEvent<HTMLInputElement>) {
    if (e.target.files && e.target.files.length > 0) {
      setCrop(undefined); // Makes crop preview update between images.
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setImgSrc(reader.result ? reader.result.toString() : "");
      });
      reader.readAsDataURL(e.target.files[0]);
      setImage(e.target.files[0]);
    }
  }

  function onImageLoad(e: React.SyntheticEvent<HTMLImageElement>) {
    const { width, height } = e.currentTarget;
    setCrop(centerAspectCrop(width, height, width / height));
  }

  const getImageBlob = async (): Promise<Blob> => {
    const image = imgRef.current;
    const previewCanvas = previewCanvasRef.current;
    if (!image || !previewCanvas || !completedCrop) {
      throw new Error("Crop canvas does not exist");
    }

    // This will size relative to the uploaded image
    // size. If you want to size according to what they
    // are looking at on screen, remove scaleX + scaleY
    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;

    const offscreen = document.createElement("canvas");
    offscreen.width = completedCrop.width * scaleX;
    offscreen.height = completedCrop.height * scaleY;

    const ctx = offscreen.getContext("2d");
    if (!ctx) {
      throw new Error("No 2d context");
    }

    ctx.drawImage(
      previewCanvas,
      0,
      0,
      previewCanvas.width,
      previewCanvas.height,
      0,
      0,
      offscreen.width,
      offscreen.height
    );

    let blob: Blob | null = (await new Promise((resolve) => {
      offscreen.toBlob(resolve, "image/png");
    })) as Blob;

    let quality = 1; // Initial quality
    let size = 0;
    const FiveMB = 5 * 1024 * 1024;

    while (size > FiveMB) {
      // Convert the canvas content to a Blob with current quality
      blob = (await new Promise((resolve) => {
        offscreen.toBlob(resolve, "image/jpeg", quality);
      })) as Blob;

      // Calculate the size of the blob
      size = blob.size;

      // Reduce the quality for the next iteration
      quality -= 0.1;
      if (quality <= 0) {
        throw new Error(
          "Image quality reduced to 0, but still exceeds 5MB limit"
        );
      }
    }

    // Return the final blob
    return blob as Blob;
  };

  const onDownload = async () => {
    const blob = await getImageBlob();
    if (blobUrlRef.current) {
      URL.revokeObjectURL(blobUrlRef.current);
    }
    blobUrlRef.current = URL.createObjectURL(blob);

    // For downloading image
    if (hiddenAnchorRef.current) {
      hiddenAnchorRef.current.href = blobUrlRef.current;
      hiddenAnchorRef.current.click();
    }
  };

  const onSubmit = async () => {
    const blob = await getImageBlob();
    const croppedImage = new File([blob], image.name, { type: blob.type });

    const formData = new FormData();
    formData.append("file_path", croppedImage);
    setLoading(true);
    props
      .customerLogoCRUD(props.customerId, "put", formData)
      .then((action) => {
        if (action.type === CUSTOMER_LOGO_SUCCESS) {
          props.addSuccessMessage(
            `Customer logo ${isExisting ? "updated" : "saved"} successfully!`
          );
          props.onClose(true);
        } else if (action.type === CUSTOMER_LOGO_FAILURE) {
          props.addErrorMessage(
            `Error ${isExisting ? "updating" : "saving"} customer logo!`
          );
        }
      })
      .finally(() => setLoading(false));
  };

  const onDelete = () => {
    setLoading(true);
    props
      .customerLogoCRUD(props.customerId, "delete")
      .then((action) => {
        if (action.type === CUSTOMER_LOGO_SUCCESS) {
          props.addSuccessMessage(`Customer logo deleted successfully!`);
          props.onClose(true);
        } else if (action.type === CUSTOMER_LOGO_FAILURE) {
          props.addErrorMessage(`Error deleting customer logo!`);
        }
      })
      .finally(() => setLoading(false));
  };

  useDebounceEffect(
    async () => {
      if (
        completedCrop &&
        completedCrop.width &&
        completedCrop.height &&
        imgRef.current &&
        previewCanvasRef.current
      ) {
        canvasPreview(imgRef.current, previewCanvasRef.current, completedCrop);
      }
    },
    100,
    [completedCrop]
  );

  const getBody = () => (
    <div
      style={{
        width: "100%",
        display: "flex",
        flexDirection: "column",
        gap: "20px",
      }}
    >
      <Spinner show={loading} className="loader modal-loader" />
      <Input
        field={{
          label: "Logo",
          type: "CUSTOM",
          value: null,
        }}
        width={12}
        name="file"
        customInput={
          <div style={{ display: "flex", gap: "5px", alignItems: "center" }}>
            <label
              className="file-button btn square-btn btn-primary"
              style={{ minWidth: 0, padding: "5px", borderRadius: "5px" }}
            >
              <span>Choose File</span>
              <input
                id="logo"
                name="file"
                type={"file"}
                onChange={onSelectFile}
                accept="image/png, image/jpg, image/jpeg"
                disabled={loading}
              />
            </label>
            <div>{image ? image.name : "No file selected"}</div>
          </div>
        }
        onChange={() => null}
        className="no-padding"
        disabled={loading}
      />
      {!!imgSrc && (
        <div style={{ display: "flex", flexDirection: "column" }}>
          <h3>Crop Image</h3>
          <ReactCrop
            crop={crop}
            onChange={(_, percentCrop) => setCrop(percentCrop)}
            onComplete={(c) => setCompletedCrop(c)}
            // aspect={16 / 9}
            // minWidth={400}
            minHeight={100}
            style={{ border: "1px solid black" }}
          >
            <img ref={imgRef} alt="Logo" src={imgSrc} onLoad={onImageLoad} />
          </ReactCrop>
        </div>
      )}
      {!!completedCrop && (
        <>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <h3>
              <span>Logo Preview</span>
              <img
                onClick={onDownload}
                style={{ float: "right", cursor: "pointer" }}
                src="/assets/icons/download.png"
                alt="Cropped Logo"
                title="Download Cropped Image"
              />
            </h3>
            <canvas
              ref={previewCanvasRef}
              style={{
                border: "1px solid black",
                objectFit: "contain",
                width: completedCrop.width,
                height: completedCrop.height,
              }}
            />
          </div>
          <div>
            <a
              href="#hidden"
              ref={hiddenAnchorRef}
              download
              style={{
                position: "absolute",
                top: "-200vh",
                visibility: "hidden",
              }}
            >
              Hidden download
            </a>
          </div>
        </>
      )}
    </div>
  );

  const getFooter = () => {
    return (
      <>
        <SquareButton
          content="Cancel"
          onClick={props.onClose}
          bsStyle={"default"}
          disabled={loading}
        />
        {isExisting && (
          <SquareButton
            content="Delete"
            onClick={onDelete}
            bsStyle={"danger"}
            disabled={loading}
          />
        )}
        <SquareButton
          content="Save"
          onClick={onSubmit}
          bsStyle={"primary"}
          disabled={loading || !completedCrop}
        />
      </>
    );
  };

  return (
    <ModalBase
      show={props.show}
      closeOnEscape={false}
      onClose={props.onClose}
      hideCloseButton={loading}
      titleElement={"Edit Customer Logo"}
      bodyElement={getBody()}
      footerElement={getFooter()}
      className={`image-upload-modal`}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  customerLogoCRUD: (
    customerId: number,
    method: HTTPMethods,
    data?: FormData
  ) => dispatch(customerLogoCRUD(customerId, method, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(LogoUpload);
