import React, { Component } from 'react';
import { connect } from 'react-redux';
import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import './style.scss';
import { fetchManufacturers } from '../../actions/inventory';
import { fetchCollectorServiceTypes, fetchCollectorMapping, updateCollectorMapping, UPDATE_CL_MAPPING_SUCCESS } from '../../actions/collector';
import { addSuccessMessage, addErrorMessage } from '../../actions/appState';

enum PageType {
  Mapping,
}

interface IISettingCollectorState {
  CollectorMapping: {
    solarwindsVendor: any;
    manufacturer: any;
  },
  settings: any;
  currentPage: {
    pageType: PageType;
  };
  error: {
    manufacturer: IFieldValidation;
    solarwindsVendor: IFieldValidation;
  };
  submitted: boolean;
}

interface IISettingCollectorProps extends ICommonProps {
  fetchCollectorServiceTypes: any;
  fetchCollectorMapping: any;
  fetchManufacturers: TFetchManufacturers;
  updateCollectorMapping: any;
  isFetching: boolean;
  isFetchingServiceTypes: boolean;
  serviceTypes: any;
  manufacturers: any;
  CollectorMapping: any;
  settings: any;
  addSuccessMessage: any;
  addErrorMessage: any;
}

class SettingCollector extends Component<
  IISettingCollectorProps,
  IISettingCollectorState
  > {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: '',
  };

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    CollectorMapping: {
      manufacturer: '',
      solarwindsVendor: '',
    },
    currentPage: {
      pageType: PageType.Mapping,
    },
    settings: null,
    error: {
      manufacturer: { ...SettingCollector.emptyErrorState },
      solarwindsVendor: { ...SettingCollector.emptyErrorState },
    },
    submitted: false,
  });
  componentDidMount() {
    if (!this.props.manufacturers) {
      this.props.fetchManufacturers();
    }
    if (this.props.serviceTypes.length === 0) {
      this.props.fetchCollectorServiceTypes();
    }
    this.props.fetchCollectorMapping();
  }

  componentDidUpdate(prevProps: IISettingCollectorProps) {
    if (
      this.props.settings &&
      prevProps.settings !== this.props.settings
    ) {
      this.setState({
        settings: this.props.settings,
      });
    }
  }

  changeMappingObject = (mappingObject, integration, newMF, vendor) => {
    const manufacturer = isNaN(newMF) ? null : parseInt(newMF);
    const serviceTypeData = {
      "solarwinds_vendor": vendor,
      "cw_mnf_id": manufacturer,
      "cw_mnf_name": this.getManufaturerName(manufacturer),
    }
    mappingObject[integration].device_mappings[manufacturer] = serviceTypeData;

    this.setState({ settings: mappingObject });
  }

  getManufaturerName = id => {
    let cw_mnf_name = '';
    cw_mnf_name = this.props.manufacturers &&
      (this.props.manufacturers.filter(m => m.id === id).length > 0) ?
      this.props.manufacturers.filter(m => m.id === id)[0].label : null
    return cw_mnf_name;
  }

  changeMappingObjectManufacturer = (mappingObject, integration, mf, vendor, oldMf) => {
    const manufacturer = isNaN(mf) ? null : parseInt(mf);
    const serviceTypeData = {
      "solarwinds_vendor": vendor,
      "cw_mnf_id": manufacturer,
      "cw_mnf_name": this.getManufaturerName(manufacturer),
    }
    delete mappingObject[integration].device_mappings[oldMf]
    mappingObject[integration].device_mappings[manufacturer] = serviceTypeData;

    this.setState({ settings: mappingObject });
  }

  OnSaveCollectorMapping = () => {
    if (this.isValid()) {
      const data = this.state.settings;
      this.props.updateCollectorMapping(data).then(action => {
        if (action.type === UPDATE_CL_MAPPING_SUCCESS) {
          this.setState({ submitted: false });
          this.props.addSuccessMessage('Mappings Updated')
        } else {
          this.props.addErrorMessage('Error while Mappings Update')
        }
      });
    }
  };

  isValid = () => {
    let isValid = true;
    const mappingObject = this.state.settings
    if (!mappingObject.solarwinds.device_mappings) {
      isValid = false
    }
    if (mappingObject.solarwinds.device_mappings) {

      Object.keys(mappingObject.solarwinds.device_mappings).map(m => {
        const mapping = mappingObject.solarwinds.device_mappings[m];
        if (mapping && (mapping.cw_mnf_id === null || mapping.solarwinds_vendor === '')) {
          isValid = false
        }
      });

    }
    this.setState({
      submitted: true
    });

    return isValid;
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  renderTopBar = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="voilence__header">
        <div
          className={`voilence__header-link ${
            currentPage.pageType === PageType.Mapping
              ? 'voilence__header-link--active'
              : ''
            }`}
          onClick={() => this.changePage(PageType.Mapping)}
        >
          solarwinds
        </div>
      </div>
    );
  };
  manufacturers = () => {
    const manufacturers = this.props.manufacturers
      ? this.props.manufacturers.map(c => ({
        value: c.id,
        label: c.label,
      }))
      : [];

    return manufacturers;
  };

  getSolarwindsVendors = () => {
    const meta = this.props.serviceTypes.length > 0 && this.props.serviceTypes.filter(s => s.name === 'solarwinds')[0].meta_config;
    const vendors = [];
    meta && Object.keys(meta).map(m =>
      vendors.push({ label: m, value: m })
    );
    return vendors
  }
  render() {
    const currentPage = this.state.currentPage;

    return (
      <div className="collector-mapping">
        {(this.props.isFetching ||
          this.props.isFetchingServiceTypes) && (
            <div className="loader">
              <Spinner
                show={
                  this.props.isFetching ||
                  this.props.isFetchingServiceTypes
                }
              />
            </div>
          )}
        <h3>Collector Settings</h3>
        <div className="collector-setting-block">
          {this.renderTopBar()}
          {currentPage.pageType === PageType.Mapping && (
            <div className="doc-setting-container">
              {this.state.settings && Object.keys(this.state.settings).map((integration, index) => (
                (
                  <div className={`mapping-collector`} key={index}>
                    <div className="heading">
                      Default Vendor Mapping
                    </div>
                    {
                      Object.keys(this.state.settings[integration].device_mappings).map(m => {
                        const mapping = this.state.settings[integration].device_mappings[m];

                        return (
                          <div className="matching-fields">
                            <Input
                              field={{
                                label: 'Vendor',
                                type: "PICKLIST",
                                value: mapping.solarwinds_vendor,
                                options: this.getSolarwindsVendors(),
                                isRequired: true,
                              }}
                              width={5}
                              name="serviceType"
                              onChange={e => this.changeMappingObject(this.state.settings, integration, m, e.target.value)}
                              error={!mapping.solarwinds_vendor && this.state.submitted ? {
                                errorState: "error",
                                errorMessage: 'Please select vendor'
                              } :
                                {
                                  errorState: "success",
                                  errorMessage: '',
                                }}
                              placeholder={`Select vendor`}
                            />
                            <Input
                              field={{
                                label: 'Manufacturers',
                                type: "PICKLIST",
                                value: m,
                                options: this.manufacturers(),
                                isRequired: true,
                              }}
                              width={5}
                              name="manufacturer"
                              onChange={e => this.changeMappingObjectManufacturer(this.state.settings, integration, e.target.value, mapping.solarwinds_vendor, m)}
                              error={m === 'null' && this.state.submitted ? {
                                errorState: "error",
                                errorMessage: 'Please select manufacturer'
                              } :
                                {
                                  errorState: "success",
                                  errorMessage: '',
                                }} placeholder={`Select manufacturer`}
                            />
                          </div>
                        )
                      }
                      )
                    }
                  </div>)
              ))}
                    <SquareButton
                      onClick={() => this.OnSaveCollectorMapping()}
                      content="Save"
                      bsStyle={"primary"}
                      className="save-button-collector-setting"
                      disabled={
                        this.props.isFetching}
                    />
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  isFetching: state.collector.isFetching,
  isFetchingServiceTypes: state.collector.isFetchingServiceTypes,
  serviceTypes: state.collector.serviceTypes,
  manufacturers: state.inventory.manufacturers,
  settings: state.collector.settings,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchManufacturers: () => dispatch(fetchManufacturers()),
  fetchCollectorServiceTypes: () => dispatch(fetchCollectorServiceTypes()),
  fetchCollectorMapping: () => dispatch(fetchCollectorMapping()),
  updateCollectorMapping: (data: any) => dispatch(updateCollectorMapping(data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingCollector);
