import React, { Component } from 'react';

import SquareButton from '../../components/Button/button';
import ModalBase from '../../components/ModalBase/modalBase';
import { phoneNumberInFormat } from '../../utils/CalendarUtil';

export default class CustomerUserDetail extends Component<IUserDetailProps> {
  constructor(props: IUserDetailProps) {
    super(props);
  }

  renderBody = () => {
    const {
      first_name,
      last_name,
      email,
      profile,
      is_active,
      role_display_name,
    } = this.props.user;

    return (
      <div className="detail__body">
        <div className="body">
          <div className="field">
            <p className="heading">NAME</p>
            <p>{`${first_name} ${last_name}`}</p>
          </div>
          <div className="field">
            <p className="heading">EMAIL</p>
            <p>{email ? `${email}` : `N/A`}</p>
          </div>
          <div className="field">
            <p className="heading">DEPARTMENT</p>
            <p>
              {profile && profile.department ? `${profile.department}` : `N/A`}
            </p>
          </div>
          <div className="field">
            <p className="heading">TITLE</p>
            <p>{profile && profile.title ? `${profile.title}` : `N/A`}</p>
          </div>
          <div className="field">
            <p className="heading">OFFICE PHONE NUMBER</p>
            {phoneNumberInFormat(profile.office_phone_country_code, profile.office_phone)}
          </div>
          <div className="field">
            <p className="heading">CELL PHONE NUMBER</p>
            {phoneNumberInFormat(profile.country_code, profile.cell_phone_number)}
          </div>
          <div className="field">
            <p className="heading">ROLE</p>
            <p>{role_display_name ? `${role_display_name}` : `N/A`}</p>
          </div>
          <div className="field">
            <p className="heading">TWITTER PROFILE URL</p>
            <p className="social_link">
              {profile && profile.twitter_profile_url ? (
                <a href={profile.twitter_profile_url} target="_blank">
                  {profile.twitter_profile_url}
                </a>
              ) : (
                  <span>N/A</span>
                )}
            </p>
          </div>
          <div className="field">
            <p className="heading">LINKEDIN PROFILE URL</p>
            <p className="social_link">
              {profile && profile.linkedin_profile_url ? (
                <a href={profile.linkedin_profile_url} target="_blank">
                  {profile.linkedin_profile_url}
                </a>
              ) : (
                  <span>N/A</span>
                )}
            </p>
          </div>
          <div className="field">
            <p className="heading">STATUS</p>
            <p>
              {is_active
                ? `Enabled`
                : `Disabled
`}
            </p>
          </div>
        </div>
        <div />
      </div>
    );
  };

  renderFooter = () => {
    return (
      <SquareButton
        content="Close"
        bsStyle={"primary"}
        onClick={this.props.close}
      />
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="User Details"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-detail-container"
      />
    );
  }
}
