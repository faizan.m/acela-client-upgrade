import { DebouncedFunc, debounce } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import {
  CREATE_CUSTOMERS_USER_FAILURE,
  CREATE_CUSTOMERS_USER_SUCCESS,
  fetchCustomerUsers,
  sendMailactivateCustomerUser,
} from '../../actions/customerUser';
import NewUser from './create';
import DetailsUser from './details';
import EditUser from './edit';

import SquareButton from '../../components/Button/button';
import EditButton from '../../components/Button/editButton';
import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';

import { fetchUserTypes } from '../../actions/userType';
import { allowPermission } from '../../utils/permissions';
import UserActivationForm from '../Customer/UserActivation';

import './style.scss';

interface ICustomerUserProps extends ICommonProps{
  fetchCustomerUsers: TFetchCustomerUsers;
  fetchUserTypes: TFetchUserTypes;
  sendMailactivateCustomerUser: TPostSendMail;
  users: any;
  userTypes: IUserTypeRole[];
  isFetchingUsers: boolean;
}

class CustomerUser extends React.Component<ICustomerUserProps, IUserState> {
  private debouncedFetch: DebouncedFunc<(params?: IServerPaginationParams) => void>;

  constructor(props: ICustomerUserProps) {
    super(props);

    this.state = {
      rows: [],
      selectedRows: [],
      pagination: {
        totalRows: 0,
        currentPage: 0,
        totalPages: 0,
        params: {},
      },
      users: [],
      isNewVisible: false,
      isEditVisible: false,
      editRow: null,
      isDetailVisible: false,
      detailRow: null,
      isUserActivationFormOpen: false,
      isActivating: false,
      emailActivatingIndex: null,
      reset: false,
    };
    this.debouncedFetch = debounce(this.fetchData, 1000);
  }
  componentDidMount() {
    this.props.fetchCustomerUsers();
    this.props.fetchUserTypes();
  }

  componentDidUpdate(prevProps: ICustomerUserProps) {
    if (this.props.users !== prevProps.users) {
      this.setRows(this.props);
    }
  }

  getActivationButton = user => {
    let actionType = 'showActive';
    if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count === 0
    ) {
      actionType = 'showActivate';
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 0 &&
      user.activation_mails_sent_count < 6
    ) {
      actionType = 'showResend';
    } else if (
      user.is_active === false &&
      user.is_password_set === false &&
      user.activation_mails_sent_count > 5
    ) {
      actionType = 'showResendStop';
    }

    return actionType;
  };

  setRows = (nextProps: ICustomerUserProps) => {
    const usersResponse = nextProps.users;
    const users: ICustomerUser[] = usersResponse.results;
    const rows: ICustomerUserTable[] = users.map((user, index) => ({
      name: `${user.first_name}  ${user.last_name}`,
      email: user.email,
      role: user.role_display_name,
      can_edit: user.can_edit,
      status: user.is_active ? 'Enabled' : 'Disabled',
      id: user.id,
      index,
      is_active: user.is_password_set,
      action: this.getActivationButton(user),
    }));

    this.setState(prevState => ({
      reset: false,
      rows,
      users,
      pagination: {
        ...prevState.pagination,
        totalRows: usersResponse.count,
        currentPage: usersResponse.links.page_number - 1,
        totalPages: Math.ceil(
          usersResponse.count / this.state.pagination.params.page_size
        ),
      },
    }));
  };

  onRowClick = rowInfo => {
    this.openDetailModal(rowInfo.original.index);
  };

  stopEvent = (event: any) => {
    event.stopPropagation();
  };

  onclickSendMail = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState({ emailActivatingIndex: userIndex });
    this.props
      .sendMailactivateCustomerUser(this.state.users[userIndex])
      .then(action => {
        this.setState({ emailActivatingIndex: null });
        this.debouncedFetch();
      })
      .catch(() => {
        this.setState({ emailActivatingIndex: null });
      });
  };

  // Server side searching, sorting, ordering
  fetchData = (params: IServerPaginationParams) => {
    const prevParams = this.state.pagination.params;
    // This is done because filtering is done through
    // searchbox which is not connected to the Table
    // component. Hence the parameters need to be saved
    // to handle filtering.
    const newParams = {
      ...prevParams,
      ...params,
    };
    this.setState(prevState => ({
      pagination: {
        ...prevState.pagination,
        params: newParams,
      },
    }));

    this.props.fetchCustomerUsers(newParams);
  };

  onSearchStringChange = e => {
    const search = e.target.value;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;

      this.setState(prevState => ({
        reset: true,
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch({
      page: 1,
    });
  };
  fetchUsers = () => {
    const search = this.state.pagination.params.search;

    if (search && search.length > 0) {
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: {
            ...prevState.pagination.params,
            search,
          },
        },
      }));
    } else {
      const newParams = { ...this.state.pagination.params };
      delete newParams.search;
      this.setState(prevState => ({
        pagination: {
          ...prevState.pagination,
          params: newParams,
        },
      }));
    }

    // set page to 1 for
    // filtering
    this.debouncedFetch();
  };
  toggleNewModal = () => {
    this.setState({ isNewVisible: !this.state.isNewVisible });
  };

  openEditModal = (index: number, event: any) => {
    event.stopPropagation();
    this.setState({ isEditVisible: true, editRow: index });
  };

  closeEditModal = (reset) => {
    this.setState({ isEditVisible: false, editRow: null });
    if (reset === true) {
      this.debouncedFetch()
    }
  };

  openDetailModal = (index: number) => {
    this.setState({ isDetailVisible: true, detailRow: index });
  };

  closeDetailModal = () => {
    this.setState({ isDetailVisible: false, detailRow: null });
  };

  toggleUserActivationForm = (userIndex: number, event: any) => {
    event.stopPropagation();
    this.setState(prevState => ({
      isUserActivationFormOpen: !prevState.isUserActivationFormOpen,
      user: prevState.users[userIndex],
    }));
  };

  toggleUserActivation = () => {
    this.setState(prevState => ({
      isUserActivationFormOpen: !prevState.isUserActivationFormOpen,
    }));
  };

  onSubmitCustomerUSerActivate = user => {
    this.setState({ isActivating: true });
    //const newCustomerId = this.props.match.params.customerId;

    this.props.sendMailactivateCustomerUser(user).then(action => {
      if (action.type === CREATE_CUSTOMERS_USER_SUCCESS) {
        this.setState(prevState => ({
          isUserActivationFormOpen: false,
          isActivating: false,
        }));
      }
      if (action.type === CREATE_CUSTOMERS_USER_FAILURE) {
        this.setState(prevState => ({
          isUserActivationFormOpen: true,
          isActivating: false,
        }));
      }
      this.fetchUsers();
    });
  };

  getCustomerUserRoles = () => {
    const userTypes = this.props.userTypes;
    if (userTypes) {
      const superUserType = userTypes.find(
        userType => userType.user_type === 'customer'
      );

      return superUserType.roles.map(role => ({
        value: role.id,
        label: role.display_name,
      }));
    }

    return [];
  };

  renderTopBar = () => {
    return (
      <div className="customer-listing__actions header-panel">
        <Input
          field={{
            value: this.state.pagination.params.search,
            label: '',
            type: "SEARCH",
          }}
          width={5}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search Users"
          className="search"
        />
        {allowPermission('add_customer_user') && (
          <SquareButton
            content="+ Add user"
            bsStyle={"primary"}
            className="btn__add"
            onClick={this.toggleNewModal}
          />
        )}
      </div>
    );
  };

  render() {
    const columns: any = [
      {
        accessor: 'name',
        Header: 'Name',
        id: 'name',
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      { accessor: 'email', Header: 'Email', id: 'email' },
      { accessor: 'role', Header: 'Role', id: 'role' },
      {
        accessor: 'status',
        id: 'is_active',
        Header: 'Status',
        Cell: status => (
          <div className={`status status--${status.value}`}>{status.value}</div>
        ),
        width: 130,
      },
      {
        accessor: 'action',
        sortable: false,
        Header: 'Action',
        Cell: action => (
          <div>
            {action.value === 'showActivate' &&
              allowPermission('customer_user_activate') && (
                <SquareButton
                  content="Activate"
                  className="activate-btn"
                  onClick={e => this.toggleUserActivationForm(action.index, e)}
                  bsStyle={"primary"}
                />
              )}
            {action.value === 'showResend' &&
              action.index !== this.state.emailActivatingIndex && (
                <SquareButton
                  content="Resend e-Mail"
                  onClick={e => this.onclickSendMail(action.index, e)}
                  bsStyle={"primary"}
                  className="activate-btn"
                />
              )}
            {action.value === 'showResend' &&
              action.index === this.state.emailActivatingIndex && (
                <Spinner show={true} className="email__spinner" />
              )}
            {action.value === 'showResendStop' && (
              <div>No more Emails can sent</div>
            )}
            {action.value === 'showActive' && <div>Activated</div>}
          </div>
        ),
      },
      {
        accessor: 'index',
        Header: 'Edit',
        width: 60,
        sortable: false,
        Cell: cell =>
          cell.original.can_edit && (
            <div
              title={`${
                cell.original.is_active ? `` : 'Please activate User to edit'
                }`}
              className={`${cell.original.is_active ? `` : 'disable-div'}`}
            >
              <EditButton
                onClick={e => this.openEditModal(cell.value, e)}
              />
            </div>
          ),
      },
    ];
    // const rows: any = this.getRows();
    const rowSelectionProps = {
      showCheckbox: false,
      selectIndex: 'id',
      onRowsToggle: null,
    };
    const manualProps = {
      manual: true,
      reset: this.state.reset,
      pages: this.state.pagination.totalPages,
      onFetchData: this.fetchData,
    };

    const {
      detailRow,
      editRow,
      isDetailVisible,
      isEditVisible,
      isNewVisible,
      users,
    } = this.state;

    return (
      <div className="customer-user-listing-container">
        <h3>Users and Access</h3>
        <div className="loader">
          <Spinner show={this.props.isFetchingUsers} />
        </div>
        <Table
          columns={columns}
          rows={this.state.rows}
          rowSelection={rowSelectionProps}
          customTopBar={this.renderTopBar()}
          className={`customer-listing__table ${
            this.props.isFetchingUsers ? `loading` : ``
            }`}
          onRowClick={this.onRowClick}
          manualProps={manualProps}
          loading={this.props.isFetchingUsers}
        />
        {isNewVisible && (
          <NewUser isVisible={isNewVisible} close={this.toggleNewModal} />
        )}
        {isEditVisible && (
          <EditUser
            isVisible={isEditVisible}
            close={this.closeEditModal}
            user={users[editRow]}
          />
        )}
        {isDetailVisible && (
          <DetailsUser
            isVisible={isDetailVisible}
            close={this.closeDetailModal}
            user={users[detailRow]}
          />
        )}
        <UserActivationForm
          user={this.state.user}
          onClose={this.toggleUserActivation}
          show={this.state.isUserActivationFormOpen}
          onSubmit={this.onSubmitCustomerUSerActivate}
          customerUserRoles={this.getCustomerUserRoles()}
          isLoading={this.state.isActivating}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  users: state.customerUser.users,
  userTypes: state.userType.userTypes,
  isFetchingUsers: state.customerUser.isFetchingUsers,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchCustomerUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchCustomerUsers(params)),
  sendMailactivateCustomerUser: (user: ICustomerUser) =>
    dispatch(sendMailactivateCustomerUser(user)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerUser);
