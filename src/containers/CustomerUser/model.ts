interface ICustomerUserTable {
  id?: string;
  name: string;
  email: string;
  role: string;
  status: string;
  index: number;
  can_edit: boolean;
}

interface IUserState {
  rows: ICustomerUserTable[];
  selectedRows: number[];
  errorList?: any;
  customer?: Icustomer;
  pagination: {
    totalRows: number;
    currentPage: number;
    totalPages?: number;
    params?: IServerPaginationParams;
  };
  users: ICustomerUser[];
  isNewVisible: boolean;
  isEditVisible: boolean;
  editRow: number;
  isDetailVisible: boolean;
  detailRow: number;
  isUserActivationFormOpen: boolean;
  user?: any;
  isActivating: boolean;
  emailActivatingIndex: number;
  reset: boolean;
}

interface IUserNewProps {
  close?: (e: any) => void;
  isVisible?: boolean;
  createUserCustomer?: any;
  userTypes?: IUserTypeRole[];
  fetchUserTypes?: () => void;
  isPostingUser?: boolean;
  fetchUsers?: () => void;
  customerId?: any;
  fetchCustomerUsers?: any;
}

interface IUserNewState {
  firstName: string;
  lastName: string;
  email: string;
  department: string;
  title: string;
  office_phone: string;
  office_phone_country_code?: string;
  cellPhone: string;
  country_code?: string;
  twitter: string;
  linkedin: string;
  isEnabled: string;
  role: string;
  isFormValid: boolean;
  error: {
    firstName: IFieldValidation;
    lastName: IFieldValidation;
    email: IFieldValidation;
    role: IFieldValidation;
  };
}

interface IUserEditProps {
  close: (refresh : boolean) => void;
  user: ICustomerUser;
  isVisible: boolean;
  userTypes?: IUserTypeRole[];
  editUserCustomer?: (id: string, user: ICustomerUser) => any;
  fetchUserTypes?: () => void;
  isPostingUser?: boolean;
}

interface IUserEditState {
  firstName: string;
  lastName: string;
  email: string;
  department: string;
  title: string;
  office_phone?: string;
  office_phone_country_code?: string;
  cellPhone: string;
  country_code: string;
  twitter: string;
  linkedin: string;
  is_password_set: boolean;
  isEnabled: string;
  role?: string;
  user_role: number;
  isFormValid: boolean;
  error: {
    firstName: IFieldValidation;
    lastName: IFieldValidation;
    email: IFieldValidation;
  };
}

interface IUserDetailProps {
  close: (e: any) => void;
  user: ICustomerUser;
  isVisible: boolean;
}
