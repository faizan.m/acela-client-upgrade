import { cloneDeep } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
  createUserCustomer,
  CUSTOMER_USERS_CREATE_FAILURE,
  CUSTOMER_USERS_CREATE_SUCCESS,
  fetchCustomerUsers,
} from '../../actions/customerUser';
//TODO Use Get Provider types instaed of user types
import { fetchUserTypes } from '../../actions/userType';
import AppValidators from '../../utils/validator';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import RadioButtonGroup from '../../components/Input/RadioButtonGroup/radioButtonGroup';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { commonFunctions } from '../../utils/commonFunctions';
import { getCountryCodes } from '../../utils/CalendarUtil';

class CustomerUserNew extends Component<IUserNewProps, IUserNewState> {
  roles = [{ value: '', label: '' }];
  statuses = [
    { value: 'true', label: 'Enabled' },
    { value: 'false', label: 'Disabled' },
  ];
  constructor(props: IUserNewProps) {
    super(props);

    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    firstName: '',
    lastName: '',
    email: '',
    isFormValid: false,
    department: '',
    title: '',
    office_phone: '',
    cellPhone: '',
    twitter: '',
    linkedin: '',
    isEnabled: 'false',
    role: this.roles[0].value,
    country_code: '1',
    error: {
      firstName: {
        errorState: "success",
        errorMessage: '',
      },
      lastName: {
        errorState: "success",
        errorMessage: '',
      },
      email: {
        errorState: "success",
        errorMessage: '',
      },
      role: {
        errorState: "success",
        errorMessage: '',
      },
      country_code: {
        errorState: "success",
        errorMessage: '',
      },
    },
  });
  componentDidMount() {
    this.props.fetchUserTypes();
  }

  handleChange = (e: any): void => {
    const newState = { ...this.state };
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  submitRequest = () => {
    const {
      cellPhone,
      department,
      country_code,
      office_phone,
      email,
      firstName,
      lastName,
      linkedin,
      title,
      twitter,
      role,
      isEnabled,
    } = this.state;

    if (this.validateForm()) {
      const newUser = {
        email,
        first_name: firstName,
        title,
        last_name: lastName,
        role,
        is_active: isEnabled,
        user_role: parseInt(role, 10),
        profile: {
          department,
          title,
          office_phone: office_phone,
          country_code,
          cell_phone_number: cellPhone,
          twitter_profile_url: twitter,
          linkedin_profile_url: linkedin,
        },
      };
      this.props.createUserCustomer(newUser).then(action => {
        if (action.type === CUSTOMER_USERS_CREATE_SUCCESS) {
          this.props.fetchCustomerUsers();
          this.props.close(null);
        }
        if (action.type === CUSTOMER_USERS_CREATE_FAILURE) {
          this.setValidationErrors(action.errorList.data);
        }
      });
    }
  };

  validateForm = () => {
    const error = this.getEmptyState().error;

    let isValid = true;
    if (!this.state.firstName) {
      error.firstName.errorState = "error";
      error.firstName.errorMessage = 'First Name cannot be empty';
      isValid = false;
    } else if (this.state.firstName && this.state.firstName.length > 300) {
      error.firstName.errorState = "error";
      error.firstName.errorMessage =
        'First Name should be less than 300 chars.';

      isValid = false;
    }
    if (!this.state.lastName) {
      error.lastName.errorState = "error";
      error.lastName.errorMessage = 'Last Name cannot be empty';
      isValid = false;
    } else if (this.state.lastName && this.state.lastName.length > 300) {
      error.lastName.errorState = "error";
      error.lastName.errorMessage = 'Last Name should be less than 300 chars.';

      isValid = false;
    }
    if (!this.state.email) {
      error.email.errorState = "error";
      error.email.errorMessage = 'Email cannot be empty';
      isValid = false;
    }
    if (this.state.email && !AppValidators.isValidEmail(this.state.email)) {
      error.email.errorState = "error";
      error.email.errorMessage = 'Enter a valid email';

      isValid = false;
    }
    if (!this.state.role) {
      error.role.errorState = "error";
      error.role.errorMessage = 'Role cannot be empty.';
      isValid = false;
    }
    this.setState({
      error,
    });

    return isValid;
  };

  transformUserTypes = () => {
    if (this.props.userTypes) {
      const roles = this.props.userTypes[2].roles.map(role => {
        return {
          value: role.id.toString(),
          label:
            role.display_name.charAt(0).toUpperCase() + role.display_name.slice(1),
        };
      });

      this.roles = roles;
    }

    return this.roles;
  };

  setValidationErrors = errorList => {
    let newState: IUserNewState = cloneDeep(this.state);
    newState = commonFunctions.errorStateHandle(errorList,newState)
    newState.isFormValid = false;
    this.setState(newState);
  };




  renderBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isPostingUser} />
        </div>
        <div
          className={`new__body ${this.props.isPostingUser ? `loading` : ''}`}
        >
          <div className="row">
            <Input
              field={{
                label: 'First Name ',
                type: "TEXT",
                isRequired: true,
                value: `${this.state.firstName}`,
              }}
              error={this.state.error.firstName}
              width={6}
              name="firstName"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Last Name ',
                type: "TEXT",
                isRequired: true,
                value: `${this.state.lastName}`,
              }}
              error={this.state.error.lastName}
              width={6}
              name="lastName"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Email Address ',
                type: "TEXT",
                isRequired: true,
                value: `${this.state.email}`,
              }}
              error={this.state.error.email}
              width={6}
              name="email"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Department',
                type: "TEXT",
                value: `${this.state.department}`,
              }}
              width={6}
              name="department"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Title',
                type: "TEXT",
                value: `${this.state.title}`,
              }}
              width={6}
              name="title"
              onChange={this.handleChange}
              className="new__body__input"
            />
                        <Input
              field={{
                label: 'Office Country code',
                isRequired: false,
                type: "PICKLIST",
                options: getCountryCodes(),
                value: this.state.country_code,
              }}
              width={6}
              placeholder="code"
              name="country_code"
              onChange={this.handleChange}
              className="country-code"
            />
            <Input
              field={{
                label: 'Office Phone Number',
                type: "TEXT",
                value: `${this.state.office_phone}`,
              }}
              width={6}
              placeholder="Enter office phone Number"
              name="office_phone"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Country code',
                isRequired: false,
                type: "PICKLIST",
                options: getCountryCodes(),
                value: this.state.country_code,
              }}
              width={6}
              placeholder="code"
              name="country_code"
              onChange={this.handleChange}
              className="country-code"
            />
            <Input
              field={{
                label: 'Cell Phone Number ',
                type: "TEXT",
                value: `${this.state.cellPhone}`,
              }}
              width={6}
              name="cellPhone"
              placeholder="Enter cell phone Number"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'Twitter Porfile URL ',
                type: "TEXT",
                value: `${this.state.twitter}`,
              }}
              width={6}
              name="twitter"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <Input
              field={{
                label: 'LinkedIn Porfile URL ',
                type: "TEXT",
                value: `${this.state.linkedin}`,
              }}
              width={6}
              name="linkedin"
              onChange={this.handleChange}
              className="new__body__input"
            />
            <div className="custom__input  new__body__input">
              <p>Status</p>
              <RadioButtonGroup
                name="isEnabled"
                value={this.state.isEnabled}
                options={this.statuses}
                disabled={true}
                onChange={this.handleChange}
              />
            </div>
            <Input
              field={{
                label: 'Role ',
                type: "PICKLIST",
                value: `${this.state.role}`,
                isRequired: true,
                options: this.transformUserTypes(),
              }}
              error={this.state.error.role}
              width={6}
              name="role"
              onChange={this.handleChange}
            />
          </div>
          <div />
        </div>
      </div>
    );
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isPostingUser ? `loading` : ''}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.props.close}
        />
        <SquareButton
          content="Add"
          bsStyle={"primary"}
          onClick={this.submitRequest}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Add New User"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-new-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  createUserCustomer: (user: ICustomerUser) =>
    dispatch(createUserCustomer(user)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
  fetchCustomerUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchCustomerUsers(params)),
});

const mapStateToProps = (state: IReduxStore) => ({
  userTypes: state.userType.userTypes,
  isPostingUser: state.customerUser.isPostingUser,
});
//TODO: Remove any
export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(CustomerUserNew);
