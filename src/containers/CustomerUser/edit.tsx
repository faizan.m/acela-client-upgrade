import { cloneDeep } from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';

import { editUserCustomer, EDIT_CUSTOMER_USERS_SUCCESS } from '../../actions/customerUser';
import { fetchUserTypes } from '../../actions/userType';

import SquareButton from '../../components/Button/button';
import Input from '../../components/Input/input';
import RadioButtonGroup from '../../components/Input/RadioButtonGroup/radioButtonGroup';
import SelectInput from '../../components/Input/Select/select';
import ModalBase from '../../components/ModalBase/modalBase';
import Spinner from '../../components/Spinner';
import { getUserProfile } from '../../utils/AuthUtil';
import { getCountryCodes } from '../../utils/CalendarUtil';
import { countryCodes } from '../../utils/countryCodes';

class CustomerUserEdit extends Component<IUserEditProps, IUserEditState> {
  roles = [{ value: null, label: '' }];
  statuses = [
    { value: 'true', label: 'Enabled' },
    { value: 'false', label: 'Disabled' },
  ];
  constructor(props: IUserEditProps) {
    super(props);
    const user = props.user;
    this.state = {
      firstName: user.first_name,
      lastName: user.last_name,
      email: user.email,
      department: user.profile.department || '',
      country_code: user.profile.country_code,
      title: user.profile.title,
      office_phone_country_code: user.profile.office_phone_country_code,
      office_phone: user.profile.office_phone ? user.profile.office_phone : '',
      cellPhone: user.profile.cell_phone_number || '',
      twitter: user.profile.twitter_profile_url
        ? user.profile.twitter_profile_url
        : '',
      linkedin: user.profile.linkedin_profile_url
        ? user.profile.linkedin_profile_url
        : '',
      isEnabled: user.is_active.toString(),
      user_role: user.user_role,
      is_password_set: user.is_password_set,
      isFormValid: false,
      error: {
        firstName: {
          errorState: "success",
          errorMessage: '',
        },
        lastName: {
          errorState: "success",
          errorMessage: '',
        },
        email: {
          errorState: "success",
          errorMessage: '',
        },
      },
    };
  }

  componentDidMount() {
    this.props.fetchUserTypes();
  }

  handleChange = (e: any): void => {
    const newState: IUserEditState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };

  //dont allow user to self disable
  handleIsActiveChange = (e: any) => {
    const { user_id } = getUserProfile();
    if (user_id !== this.props.user.id) {
      const newState: IUserEditState = cloneDeep(this.state);
      newState[e.target.name] = e.target.value;
      this.setState(newState);
    }
  };

  submitRequest = () => {
    const {
      cellPhone,
      department,
      email,
      firstName,
      lastName,
      linkedin,
      title,
      twitter,
      user_role,
      isEnabled,
      role,
      country_code,
      office_phone_country_code,
      office_phone,
    } = this.state;

    if (this.validateForm()) {
      const editedUser = {
        email,
        first_name: firstName,
        last_name: lastName,
        is_active: isEnabled,
        user_role,
        role,
        profile: {
          department,
          title,
          office_phone,
          country_code,
          cell_phone_number: cellPhone,
          twitter_profile_url: twitter,
          linkedin_profile_url: linkedin,
          office_phone_country_code,
        },
      };
      this.props.editUserCustomer(this.props.user.id, editedUser).then(action => {
        if (action.type === EDIT_CUSTOMER_USERS_SUCCESS) {
          this.props.close(true);
        }
      });
    }
  };

  validateForm = () => {
    const newState: IUserEditState = cloneDeep(this.state);
    let isValid = true;
    if (!this.state.firstName) {
      newState.error.firstName.errorState = "error";
      newState.error.firstName.errorMessage = 'First Name cannot be empty';
      isValid = false;
    } else if (this.state.firstName && this.state.firstName.length > 300) {
      newState.error.firstName.errorState = "error";
      newState.error.firstName.errorMessage =
        'First Name should be less than 300 chars.';

      isValid = false;
    }
    if (!this.state.lastName) {
      newState.error.lastName.errorState = "error";
      newState.error.lastName.errorMessage = 'Last Name cannot be empty';
      isValid = false;
    } else if (this.state.lastName && this.state.lastName.length > 300) {
      newState.error.lastName.errorState = "error";
      newState.error.lastName.errorMessage =
        'Last Name should be less than 300 chars.';

      isValid = false;
    }
    if (!this.state.email) {
      newState.error.email.errorState = "error";
      newState.error.email.errorMessage = 'Email cannot be empty';
      isValid = false;
    }
    newState.isFormValid = isValid;
    this.setState(newState);

    return isValid;
  };

  transformUserTypes = () => {
    if (this.props.userTypes) {
      const roles = this.props.userTypes[2].roles.map(role => {
        return {
          value: role.id,
          label:
            role.display_name.charAt(0).toUpperCase() + role.display_name.slice(1),
        };
      });

      this.roles = roles;
    }

    return this.roles;
  };

  renderBody = () => {
    return (
      <div>
        <div className="loader modal-loader">
          <Spinner show={this.props.isPostingUser} />
        </div>
        <div
          className={`edit__body ${this.props.isPostingUser ? `loading` : ''}`}
        >
          <div className="row">
            <Input
              field={{
                label: 'First Name ',
                type: "TEXT",
                isRequired: true,
                value: `${this.state.firstName}`,
              }}
              error={this.state.error.firstName}
              width={6}
              name="firstName"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <Input
              field={{
                label: 'Last Name ',
                type: "TEXT",
                isRequired: true,
                value: `${this.state.lastName}`,
              }}
              width={6}
              name="lastName"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <Input
              field={{
                label: 'Email Address ',
                type: "TEXT",
                isRequired: true,
                value: `${this.state.email}`,
              }}
              disabled={true}
              width={6}
              name="email"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <Input
              field={{
                label: 'Department',
                type: "TEXT",
                value: `${this.state.department}`,
              }}
              width={6}
              name="department"
              onChange={this.handleChange}
              className="edit__body__input"
            />

            <Input
              field={{
                label: 'Office Country code',
                isRequired: false,
                type: "PICKLIST",
                options: getCountryCodes(),
                value: this.state.office_phone_country_code,
              }}
              width={6}
              placeholder="code"
              name="office_phone_country_code"
              onChange={this.handleChange}
              className="country-code"
            />
            <Input
              field={{
                label: 'Office Phone Number',
                type: "TEXT",
                value: `${this.state.office_phone}`,
              }}
              width={6}
              name="office_phone"
              onChange={this.handleChange}
              className="edit__body__input"
            />

            <Input
              field={{
                label: 'Country code',
                isRequired: false,
                type: "PICKLIST",
                options: this.getCountryCodes(),
                value: this.state.country_code,
              }}
              width={6}
              placeholder="code"
              name="country_code"
              onChange={this.handleChange}
              className="edit__body__input country-code"
            />
            <Input
              field={{
                label: 'Cell Phone Number ',
                type: "TEXT",
                value: `${this.state.cellPhone}`,
              }}
              width={6}
              name="cellPhone"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <Input
              field={{
                label: 'Title',
                type: "TEXT",
                value: `${this.state.title}`,
              }}
              width={6}
              name="title"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <Input
              field={{
                label: 'Twitter Porfile URL ',
                type: "TEXT",
                value: `${this.state.twitter}`,
              }}
              width={6}
              name="twitter"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <Input
              field={{
                label: 'LinkedIn Porfile URL ',
                type: "TEXT",
                value: `${this.state.linkedin}`,
              }}
              width={6}
              name="linkedin"
              onChange={this.handleChange}
              className="edit__body__input"
            />
            <div className="custom__input edit__body__input">
              <p>Role</p>
              <SelectInput
                name="user_role"
                value={this.state.user_role}
                options={this.transformUserTypes()}
                onChange={this.handleChange}
              />
            </div>
            <div className={`custom__input  edit__body__input ${!this.state.is_password_set ? 'disabled-status-edit' : ''}`}>
              <p>Status</p>
              <RadioButtonGroup
                name="isEnabled"
                value={this.state.isEnabled}
                options={this.statuses}
                onChange={this.handleIsActiveChange}
                disabled={!this.state.is_password_set}
              />
            </div>

          </div>
          <div />
        </div>
      </div>
    );
  };

  getCountryCodes = () => {
    if (countryCodes && countryCodes.length > 0) {
      return countryCodes.map(country => ({
        value: country.code,
        label: `${country.code} (${country.name})`,
      }));
    } else {
      return [];
    }
  };

  renderFooter = () => {
    return (
      <div className={`${this.props.isPostingUser ? `loading` : ''}`}>
        <SquareButton
          content="Cancel"
          bsStyle={"default"}
          onClick={this.props.close}
        />
        <SquareButton
          content="Save Changes"
          bsStyle={"primary"}
          onClick={this.submitRequest}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.isVisible}
        onClose={this.props.close}
        titleElement="Edit User"
        bodyElement={this.renderBody()}
        footerElement={this.renderFooter()}
        className="customer-user-edit-container"
      />
    );
  }
}

const mapDispatchToProps = (dispatch: any) => ({
  editUserCustomer: (id: string, user: ICustomerUser) =>
    dispatch(editUserCustomer(id, user)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
});

const mapStateToProps = (state: IReduxStore) => ({
  userTypes: state.userType.userTypes,
  isPostingUser: state.customerUser.isPostingUser,
});

export default connect<any, any>(
  mapStateToProps,
  mapDispatchToProps
)(CustomerUserEdit);
