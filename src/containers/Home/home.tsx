import React, { lazy } from "react";
import { connect } from "react-redux";
import { debounce, DebouncedFunc, get } from "lodash";
import { Redirect, Route, Switch, withRouter } from "react-router";

import {
  getRefreshToken,
  loginSuccess,
  logOutRequest,
  REFRESH_TOKEN_FAILURE,
  REFRESH_TOKEN_SUCCESS,
} from "../../actions/auth";
import {
  FETCH_CUSTOMER_PROFILE_SUCCESS,
  fetchCustomerProfile,
} from "../../actions/customerUser";
import { fetchUserProfile } from "../../actions/profile";
import { fetchCustomersShort } from "../../actions/customer";
import { fetchProviderProfile } from "../../actions/provider";
import { fetchProviderConfigStatus } from "../../actions/provider/integration";
import {
  getUserProfile,
  isLoggedIn,
  isTokeExpired,
  isTokeExpiring,
  storeUserProfile,
  getLocalRefreshToken,
} from "../../utils/AuthUtil";
import {
  getConvertedColorWithOpacity,
  isValidVal,
} from "../../utils/CommonUtils";
// COMPONENTS
import Alert from "../Alert/alert";
import Header from "../Header/header";
import Spinner from "../../components/Spinner";
import SideMenu from "../../components/SideMenu/sideMenu";
import Disclaimer from "../../components/Disclaimer/disclaimer";
import PrivateRoute from "../../components/PrivateRoute/privateRoute";
import ErrorBoundary from "../../components/ErrorBoundary/errorBoundary";

const NotFound = lazy(() => import("../NotFound/notfound"));

// Menu dropdown options
const Profile = lazy(() => import("../Profile/profile"));
const ResetPassword = lazy(() => import("../ResetPassword/resetPassword"));

// Dashboard Module
const DashboardContainer = lazy(() =>
  import("../Dashboard/dashboardContainer")
);

// Asset Management Module
const FieldMapping = lazy(() => import("../InventoryManagement/FieldMapping"));
const smartNetMapping = lazy(() =>
  import("../InventoryManagement/smartNetMapping")
);
const InventoryManagement = lazy(() =>
  import("../InventoryManagement/inventoryManagement")
);

// Subscription Management
const ImportSubscription = lazy(() =>
  import("../SubscriptionManagement/importSubscription")
);
const SubscriptionManagement = lazy(() =>
  import("../SubscriptionManagement/subscriptionManagement")
);

// Project Management Module
const ProjectDetailsV2 = lazy(() =>
  import("../ProjectManagement/ProjectDetails/projectContext")
);
const ProjectDetails = lazy(() =>
  import("../ProjectManagement/ProjectDetails")
);
const ProjectManagement = lazy(() =>
  import("../ProjectManagement/List/projectList")
);
const PMOChangeRequests = lazy(() =>
  import("../ProjectManagement/ChangeRequest/list")
);
const ChangeRequestsList = lazy(() =>
  import("../ProjectManagement/ChangeRequest/listAll")
);
const ProjectDashboard = lazy(() =>
  import("../ProjectManagement/Dashboard/dashboard")
);
const EngineerTimesheets = lazy(() =>
  import("../ProjectManagement/Timesheet/timesheet")
);
const HistoryPMO = lazy(() =>
  import("../ProjectManagement/ProjectDetails/HistoryProject")
);
const ChangeRequestDetails = lazy(() =>
  import("../ProjectManagement/ChangeRequest/create")
);
const ProjectMeeting = lazy(() =>
  import("../ProjectManagement/ProjectDetails/meetingDetails")
);

// Compliance Module
const RulesContainer = lazy(() =>
  import("../ConfigurationMangment/RulesContainer/rules")
);
const VariablesContainer = lazy(() =>
  import("../ConfigurationMangment/VariablesContainer/variables")
);
const ConfigurationManagement = lazy(() =>
  import("../ConfigurationMangment/Compliance/configurationListing")
);
const ConfigurationManagementDashboard = lazy(() =>
  import("../ConfigurationMangment/Dashboard/dashboardContainer")
);

// Customer Configurations Module
const CustomerDetails = lazy(() => import("../Customer/customerDetails"));
const CustomerListing = lazy(() => import("../Customer/customerListing"));

// Users and Access Module
const CustomerUser = lazy(() => import("../CustomerUser"));
const SuperUsersListing = lazy(() => import("../SuperUser/superUserListing"));
const ProviderUserListing = lazy(() =>
  import("../ProviderUser/providerUserListing")
);

// Documentation Module
const CircuitTypes = lazy(() => import("./../Settings/circuitTypes"));
const Contacts = lazy(() => import("../Documentation/Provider/contacts"));
const CircuitsSites = lazy(() =>
  import("../Documentation/Provider/circuitsSites")
);
const CircuitMapping = lazy(() =>
  import("../Documentation/Provider/circuitImport")
);
const storageService = lazy(() =>
  import("../Documentation/Provider/storageService")
);
const storageServiceCU = lazy(() =>
  import("../Documentation/customer/storageService")
);
const CircuitsSitesCustomer = lazy(() =>
  import("../Documentation/customer/circuitsSites")
);
const CustomerInformation = lazy(() =>
  import("../Documentation/Provider/customerInformation")
);

// Collectors Module
const CollectorList = lazy(() => import("../Collectors/collectorListing"));
const QueryList = lazy(() => import("../Collectors/QueryBuilder/queryListing"));

// Renewal Module
const Renewal = lazy(() => import("../Renewal/Renewal"));
const ProviderRenewal = lazy(() => import("../ProviderRenewal"));

// Report Module
const Report = lazy(() => import("../Report/CustomerReport"));
const StatisticsList = lazy(() => import("../Report/statistics"));
const circuitReport = lazy(() => import("../Report/circuitReport"));
const customerAnalysisReport = lazy(() =>
  import("../Report/customerAnalysisReport")
);
const ProviderReport = lazy(() => import("../Report/ProviderReport"));

// Service Request Module
const ServiceRequest = lazy(() => import("../ServiceRequest"));
const ServiceDetails = lazy(() => import("../ServiceRequest/details"));

// Sales Module
const AddSOW = lazy(() => import("../SOW/Sow/addSow"));
const SOWListing = lazy(() => import("../SOW/Sow/sowList"));
const NotesList = lazy(() => import("../SOW/Notes/NotesList"));
const AddTemplate = lazy(() => import("../SOW/Template/addTemplate"));
const ChangeRequest = lazy(() => import("../SOW/ChangeRequest/index"));
const SOWDashBoard = lazy(() => import("../SOW/Dashboard/SOWdashboard"));
const SalesActivity = lazy(() => import("../SalesActivity/activityList"));
const TemplateListing = lazy(() => import("../SOW/Template/templateList"));
const AgreementListing = lazy(() => import("../SOW/Agreement/agreementList"));
const SowMetrics = lazy(() => import("../SOW/SOWMetrics/Dashboard/sowMetrics"));
const SalesActivityDetails = lazy(() =>
  import("../SalesActivity/activityDetail")
);
const PurchaseOrderList = lazy(() =>
  import("../PurchaseOrder/purchaseOrderList")
);
const PurchaseOrderDetail = lazy(() =>
  import("../PurchaseOrder/purchaseOrderDetail")
);
const CreateAgreement = lazy(() =>
  import("../SOW/Agreement/Create/CreateAgreement")
);
const serviceCatalogList = lazy(() =>
  import("../SOW/ServiceCatalog/serviceCatalogList")
);

// Operations Module
const EmailGenerationIngram = lazy(() =>
  import("../SOW/OrderTracking/emailGenerationIngram")
);
const OrderTrackingV2 = lazy(() =>
  import("../SOW/OrderTracking/orderTrackingV2")
);
const orderTrackingDashboard = lazy(() =>
  import("../SOW/OrderTracking/Dashboard/orderTrackingDashboard")
);
const EmailGenerationCW = lazy(() =>
  import("../SOW/OrderTracking/emailGenerationCW")
);
const OrdersLinking = lazy(() => import("../SOW/OrderTracking/OrdersLinking"));

// Email Logs Module
const EmailLog = lazy(() => import("../EmailLogs/emailsList"));

// ERP Settings
const ModelMapping = lazy(() => import("../Settings/modelMapping"));
const Communication = lazy(() => import("../Settings/Communication"));
const TerritoryMapping = lazy(() => import("./../Settings/territoryMapping"));

// Settings Module
const Setting = lazy(() => import("../Settings/settings")); // Also used in ERP Settings
const SettingAlias = lazy(() => import("../SettingAlias"));
const SettingCustomer = lazy(() => import("../SettingCustomer"));
const SettingSmartNet = lazy(() => import("../SettingSmartNet"));
const SalesSettings = lazy(() => import("../SettingSOW/index"));
const VRWarehouseSettings = lazy(() => import("../SettingVRWarehouse"));
const SiteSettings = lazy(() => import("../SiteSettings"));
const SettingCollector = lazy(() => import("../SettingCollector"));
const ProjectSettings = lazy(() => import("../ProjectSetting/projectSetting"));
const SettingStorageServices = lazy(() => import("../SettingStorageServices"));
const LogicMonitor = lazy(() => import("../SettingLogicMonitor/logicMonitor"));
const APICredentials = lazy(() => import("../Settings/APICredentials"));
const AgreementTemplateListing = lazy(() =>
  import("../SOW/AgreementTemplate/agreementTemplateList")
);
const CreateAgreementTemplate = lazy(() =>
  import("../SOW/AgreementTemplate/Create/CreateAgreementTemplate")
);

// Providers Module
const ProviderDetails = lazy(() => import("../Provider/providerDetails"));
const ProviderListing = lazy(() => import("../Provider/providerListing"));

// Client 360 Module
const Client360 = lazy(() => import("../Client360"));

// Virtual Warehouse Module
const VirtualWarehouseListing = lazy(() => import("../VirtualWarehouse"));

// Product Catalog Module
const ProductCatalogListing = lazy(() => import("../ProductCatalog"));
const ImportProducts = lazy(() => import("../ProductCatalog/ImportProduct"));

import { sideMenuOptionsList } from "../../utils/sidemenuOptions";
import "easymde/dist/easymde.min.css";
// import "react-tagsinput/react-tagsinput.css"; (New Component)
import "react-quill/dist/quill.snow.css";
// import "rc-color-picker/assets/index.css"; (New Component)
// import "react-daterange-picker/dist/css/react-calendar.css"; (New Component)
import "./style.scss";

interface IHomeProps extends ICommonProps {
  loadingComponents: number;
  logOutRequest: TLogout;
  loginSuccess: TLoginSuccess;
  getRefreshToken: any;
  fetchProviderConfigStatus: TFetchConfigStatus;
  userProfile: IUserProfile;
  configStatus: IConfigStatus;
  errors: IHash;
  fetchCustomerProfile: TFetchCustomerProfile;
  fetchProviderProfile: TFetchProviderProfile;
  themeColor: string;
  logo_url: string;
  updated_on: string;
  company_url: string;
  enable_url: boolean;
  fetchingToken: boolean;
  fetchUserProfile: TFetchUserProfile;
  fetchCustomersShort: TFetchCustomers;
}

interface IHomeState {
  isSideMenuCollapsed: boolean;
  sideMenuOptions: ISideMenuOption[];
  showSettingsDisclaimer: boolean;
  showCustomerSelection: boolean;
  disableCustomerSelection: boolean;
  showRenewal: boolean;
  showCollector: boolean;
  showCompliance: boolean;
  logo_url: string;
  showContacts: boolean;
  showServiceRequest: boolean;
  showPurchaseHistory: boolean;
}

class Home extends React.Component<IHomeProps, IHomeState> {
  private debouncedFetch: DebouncedFunc<() => void>;

  constructor(props: IHomeProps) {
    super(props);

    this.state = {
      isSideMenuCollapsed: false,
      sideMenuOptions: [],
      showSettingsDisclaimer: false,
      showCustomerSelection: false,
      disableCustomerSelection: false,
      showRenewal: false,
      showContacts: false,
      showCollector: false,
      showCompliance: false,
      showServiceRequest: false,
      showPurchaseHistory: false,
      logo_url: "",
    };
    this.debouncedFetch = debounce(this.tokenRefresh, 1000);
  }

  componentDidMount() {
    const UserProfile: IUserProfile = getUserProfile();
    if (UserProfile.scopes.type === "CUSTOMER") {
      this.props.fetchCustomerProfile().then((action) => {
        if (action.type === FETCH_CUSTOMER_PROFILE_SUCCESS) {
          const res: Icustomer = action.response;
          const showRenewal = res.renewal_service_allowed;
          const showContacts = res.is_ms_customer;
          const showServiceRequest = res.is_ms_customer;
          const showCollector = res.collector_service_allowed;
          const showCompliance = res.config_compliance_service_allowed;
          const showPurchaseHistory =
            res.purchase_order_history_visibility_allowed;
          this.setState(
            {
              showContacts,
              showServiceRequest,
              showRenewal,
              showCollector,
              showCompliance,
              showPurchaseHistory,
            },
            () => this.setNavLinks(UserProfile)
          );
        }
      });
    }
    const isCollpased = localStorage.getItem("isCollapsed");
    if (isCollpased === "true") {
      this.setState({ isSideMenuCollapsed: true });
    }
    this.debouncedFetch();
    if (isLoggedIn()) {
      const UserProfile: IUserProfile = getUserProfile();
      this.props.loginSuccess(UserProfile);
      const userType = UserProfile.scopes.type;

      if (userType === "CUSTOMER" || userType === "PROVIDER") {
        this.props.fetchProviderProfile();
      }

      if (this.props.userProfile) {
        this.setNavLinks(this.props.userProfile);
      }
    }
  }

  static getDerivedStateFromProps(
    nextProps: IHomeProps,
    prevState: IHomeState
  ) {
    if (
      nextProps.location.pathname === "/service-requests" ||
      nextProps.location.pathname === "/inventory-management" ||
      nextProps.location.pathname === "/subscription-management" ||
      nextProps.location.pathname === "/inventory-management/field-mapping" ||
      nextProps.location.pathname ===
        "/inventory-management/field-mapping-smartnet" ||
      nextProps.location.pathname === "/collectors" ||
      nextProps.location.pathname === "/virtual-warehouse" ||
      nextProps.location.pathname.includes("configuration") ||
      nextProps.location.pathname.includes("documentation") ||
      nextProps.location.pathname.includes("/purchase-history") ||
      nextProps.location.pathname.includes("/client-360")
    ) {
      if (!prevState.showCustomerSelection)
        return { showCustomerSelection: true };
      else return null;
    } else if (prevState.showCustomerSelection) {
      return { showCustomerSelection: false };
    }
    if (
      nextProps.location.pathname.includes(
        "/inventory-management/field-mapping"
      )
    ) {
      if (!prevState.disableCustomerSelection)
        return { disableCustomerSelection: true };
      else return null;
    } else if (prevState.showCustomerSelection) {
      return { disableCustomerSelection: false };
    }
    return null;
  }

  componentDidUpdate(prevProps: IHomeProps) {
    this.debouncedFetch();
    if (isLoggedIn()) {
      if (prevProps.userProfile !== this.props.userProfile) {
        this.setNavLinks(this.props.userProfile);
        const userType = this.props.userProfile.scopes.type;
        if (
          userType === "CUSTOMER" ||
          userType === "PROVIDER"
        ) {
          this.props.fetchProviderConfigStatus();
        }
      }
      if (prevProps.themeColor !== this.props.themeColor) {
        if (this.props.themeColor && isValidVal(this.props.themeColor)) {
          this.setState({ logo_url: this.props.logo_url });
          document.documentElement.style.setProperty(
            "--primary",
            this.props.themeColor
          );
          document.documentElement.style.setProperty(
            "--primary-light",
            getConvertedColorWithOpacity(this.props.themeColor)
          );
        }
      }
      if (prevProps.updated_on !== this.props.updated_on) {
        if (this.props.logo_url && isValidVal(this.props.logo_url)) {
          this.setState({ logo_url: this.props.logo_url });
        }
      }
    }

    if (this.props.configStatus !== prevProps.configStatus) {
      const configStatus = this.props.configStatus;
      const isTotalConfigured =
        configStatus.crm_authentication_configured &&
        configStatus.crm_board_mapping_configured &&
        configStatus.crm_device_categories_configured &&
        configStatus.device_manufacturer_api_configured;

      const showSettingsDisclaimer = !isTotalConfigured;

      if (showSettingsDisclaimer !== this.state.showSettingsDisclaimer) {
        this.setState({
          showSettingsDisclaimer,
        });
      }
    }
  }

  tokenRefresh = () => {
    if (
      isTokeExpiring() &&
      this.props.fetchingToken === false &&
      getLocalRefreshToken()
    ) {
      this.props.getRefreshToken().then((action) => {
        if (isTokeExpired()) {
          if (action.type === REFRESH_TOKEN_SUCCESS) {
            storeUserProfile(action.response);
            const UserProfile: IUserProfile = getUserProfile();
            this.props.loginSuccess(UserProfile);
            const userType = UserProfile.scopes.type;

            if (userType === "PROVIDER") {
              this.props.fetchProviderProfile();
              this.props.fetchCustomersShort();
            }
            if (userType === "CUSTOMER") {
              this.props.fetchCustomerProfile();
            }
            this.props.fetchUserProfile();
          }
          if (action.type === REFRESH_TOKEN_FAILURE) {
            this.logout();
          }
        } else {
          if (action.type === REFRESH_TOKEN_SUCCESS) {
            storeUserProfile(action.response);
            const userProfile: IUserProfile = getUserProfile();
            this.props.loginSuccess(userProfile);
          }
        }
      });
    }
  };

  setNavLinks = (userProfile: IUserProfile) => {
    let sideMenuOptions: ISideMenuOption[] = [];
    sideMenuOptions = sideMenuOptionsList.filter((f) => {
      const accessLevel = f.accessLevel[userProfile.scopes.type];
      const isRole = accessLevel.includes(userProfile.scopes.role);
      const isAll = accessLevel[0] === "ALL";
      const isAvailable = get(this.state, f.accessLevel["isAvailable"]);
      //check menu has access from API value

      f.childs.map((f) => {
        if (f.defaultHide) {
          f.defaultHide = !get(this.state, f["isAvailable"]);
          //check default hide is true and check child menu has access from API value
        }
        return f;
      });
      return isRole || isAll || isAvailable ? f : null;
    });
    this.setState({ sideMenuOptions: [...sideMenuOptions] });
  };

  logout = () => {
    this.props.logOutRequest();
    this.props.history.push("/login");
  };

  toggleSideMenuCollapse = () => {
    this.setState((prevState) => ({
      isSideMenuCollapsed: !prevState.isSideMenuCollapsed,
    }));
    localStorage.setItem("isCollapsed", `${!this.state.isSideMenuCollapsed}`);
  };

  render() {
    // const userType = this.props.userProfile
    //   ? this.props.userProfile.scopes.type
    //   : null;

    // const userRole = this.props.userProfile
    //   ? this.props.userProfile.scopes.role
    //   : null;

    return (
      <div
        className={
          "app-grid" +
          (this.state.isSideMenuCollapsed
            ? " app-grid--side-menu-collapsed"
            : "")
        }
      >
        <Header
          {...this.props}
          disableCustomerSelection={this.state.disableCustomerSelection}
          showCustomerSelection={this.state.showCustomerSelection}
          logoUrl={this.state.logo_url}
          // showGeneralSetting={
          //   userType === "PROVIDER" && userRole === "ADMIN"
          // }
          company_url={this.props.company_url}
          enable_url={this.props.enable_url}
        />
        <SideMenu
          logout={this.logout}
          isCollapsed={this.state.isSideMenuCollapsed}
          toggleCollapseState={this.toggleSideMenuCollapse}
          sideMenuOptions={this.state.sideMenuOptions}
          location={this.props.location}
          // showSetting={
          //   userType === "PROVIDER" && userRole === "OWNER"
          // }
          logoUrl={this.state.logo_url}
          // showGeneralSetting={
          //   userType === "PROVIDER" && userRole === "ADMIN"
          // }
          company_url={this.props.company_url}
          enable_url={this.props.enable_url}
        />
        <div className="app-holder">
          <ErrorBoundary customClass="app-body">
            <React.Suspense
              fallback={<div className="main-loading-fallback">Loading...</div>}
            >
              <div className="app-body">
                <div className="loader">
                  <Spinner show={this.props.fetchingToken} />
                </div>
                {!this.state.showSettingsDisclaimer ? (
                  <Switch>
                    <PrivateRoute
                      exact
                      path="/dashboard"
                      component={DashboardContainer}
                    />
                    <Route
                      exact
                      path="/"
                      render={() => <Redirect to="/dashboard" />}
                    />

                    <PrivateRoute
                      exact
                      path="/providers"
                      component={ProviderListing}
                    />
                    <PrivateRoute
                      exact
                      path="/providers/details/:providerId"
                      component={ProviderDetails}
                    />
                    <PrivateRoute
                      exact
                      path="/accelavar-users"
                      component={SuperUsersListing}
                    />
                    <PrivateRoute
                      exact
                      path="/customer-configs"
                      component={CustomerListing}
                    />
                    <PrivateRoute
                      exact
                      path="/customer-users"
                      component={CustomerUser}
                    />
                    <PrivateRoute
                      exact
                      path="/inventory-management"
                      component={InventoryManagement}
                    />
                    <PrivateRoute
                      exact
                      path="/subscription-management"
                      component={SubscriptionManagement}
                    />
                    <PrivateRoute
                      exact
                      path="/subscription-management/import-subscription"
                      component={ImportSubscription}
                    />
                    <PrivateRoute
                      exact
                      path="/configuration/configuration-management"
                      component={ConfigurationManagement}
                    />
                    <PrivateRoute
                      exact
                      path="/configuration/summary"
                      component={ConfigurationManagementDashboard}
                    />
                    <PrivateRoute
                      exact
                      path="/configuration/variables"
                      component={VariablesContainer}
                    />
                    <PrivateRoute
                      exact
                      path="/configuration/rules"
                      component={RulesContainer}
                    />
                    <PrivateRoute
                      exact
                      path="/inventory-management/field-mapping"
                      component={FieldMapping}
                    />
                    <PrivateRoute
                      exact
                      path="/inventory-management/field-mapping-smartnet"
                      component={smartNetMapping}
                    />
                    <PrivateRoute
                      exact
                      path="/customer-configs/:customerId"
                      component={CustomerDetails}
                    />
                    <PrivateRoute
                      exact
                      path="/provider-users"
                      component={ProviderUserListing}
                    />
                    <PrivateRoute
                      exact
                      path="/customer-renewals"
                      component={ProviderRenewal}
                    />
                    <PrivateRoute
                      exact
                      path="/service-requests"
                      component={ServiceRequest}
                    />
                    <PrivateRoute
                      exact
                      path="/service-requests/:requestId"
                      component={ServiceDetails}
                    />
                    <PrivateRoute
                      exact
                      path="/service-requests-provider/:customerId/:requestId"
                      component={ServiceDetails}
                    />
                    <PrivateRoute exact path="/reports" component={Report} />
                    <PrivateRoute
                      exact
                      path="/provider-reports"
                      component={ProviderReport}
                    />
                    <PrivateRoute
                      exact
                      path="/circuit-reports"
                      component={circuitReport}
                    />
                    <PrivateRoute
                      exact
                      path="/customer-analysis-reports"
                      component={customerAnalysisReport}
                    />
                    <PrivateRoute exact path="/profile" component={Profile} />
                    <PrivateRoute
                      path="/erp-setting/primary"
                      component={Setting}
                    />
                    <PrivateRoute
                      path="/erp-setting/model-type"
                      component={ModelMapping}
                    />
                    <PrivateRoute
                      path="/erp-setting/territory"
                      component={TerritoryMapping}
                    />
                    <PrivateRoute
                      path="/erp-setting/communication"
                      component={Communication}
                    />
                    <PrivateRoute
                      path="/setting/alias"
                      component={SettingAlias}
                    />
                    <PrivateRoute
                      path="/setting/api-credentials"
                      component={APICredentials}
                    />
                    <PrivateRoute
                      path="/setting/logic-monitor-v2"
                      component={LogicMonitor}
                    />
                    <PrivateRoute
                      path="/setting/sow"
                      component={SalesSettings}
                    />
                    <PrivateRoute
                      path="/setting/virtual-warehouse-settings"
                      component={VRWarehouseSettings}
                    />
                    <PrivateRoute
                      path="/setting/sites"
                      component={SiteSettings}
                    />
                    <PrivateRoute
                      path="/setting/collector"
                      component={SettingCollector}
                    />
                    <PrivateRoute
                      path="/setting/customer"
                      component={SettingCustomer}
                    />
                    <PrivateRoute
                      path="/setting/storage-service"
                      component={SettingStorageServices}
                    />
                    <PrivateRoute
                      path="/setting/smartnet_receiving"
                      component={SettingSmartNet}
                    />
                    <PrivateRoute
                      path="/setting/project-setting"
                      component={ProjectSettings}
                    />
                    <PrivateRoute
                      path="/reset-password"
                      component={ResetPassword}
                    />
                    <PrivateRoute path="/renewal" component={Renewal} />
                    <PrivateRoute path="/renewal/:id" component={Renewal} />
                    <PrivateRoute
                      path="/documentation/customer-info"
                      component={CustomerInformation}
                    />
                    <PrivateRoute
                      path="/documentation/contacts"
                      component={Contacts}
                    />
                    <PrivateRoute
                      path="/documentation/circuits"
                      component={CircuitsSites}
                    />
                    <PrivateRoute
                      path="/documentation/circuit-types"
                      component={CircuitTypes}
                    />
                    <PrivateRoute
                      exact
                      path="/documentation/import-circuit"
                      component={CircuitMapping}
                    />
                    <PrivateRoute
                      path="/documentation/customer-circuits"
                      component={CircuitsSitesCustomer}
                    />
                    <PrivateRoute
                      path="/documentation/Documents"
                      component={storageService}
                    />
                    <PrivateRoute
                      path="/documentation/customer-documents"
                      component={storageServiceCU}
                    />
                    <PrivateRoute
                      path="/sow/templates"
                      component={TemplateListing}
                    />
                    <PrivateRoute
                      path="/sow/service-analytics"
                      component={SOWDashBoard}
                    />
                    <PrivateRoute
                      path="/sow/change-request"
                      component={ChangeRequest}
                    />
                    <PrivateRoute
                      path="/sow/template/:id"
                      component={AddTemplate}
                    />
                    <PrivateRoute path="/sow/sows" component={SOWListing} />
                    <PrivateRoute
                      path="/collectors"
                      component={CollectorList}
                    />
                    <PrivateRoute
                      path="/collector/:collectorId/queries"
                      component={QueryList}
                    />
                    <PrivateRoute path="/sow/sow/:id" component={AddSOW} />
                    <PrivateRoute
                      path="/sow/service-catalog-list"
                      component={serviceCatalogList}
                    />
                    <PrivateRoute path="/sow/notes" component={NotesList} />
                    <PrivateRoute
                      path="/sow/order-tracking-v1"
                      component={EmailGenerationIngram}
                    />
                    <PrivateRoute
                      exact
                      path="/ProjectManagement"
                      component={ProjectManagement}
                    />
                    <PrivateRoute
                      exact
                      path="/ProjectDashboard"
                      component={ProjectDashboard}
                    />
                    <PrivateRoute
                      exact
                      path="/Projects/:id"
                      component={ProjectDetailsV2}
                    />
                    <PrivateRoute
                      exact
                      path="/Projects/:id/change-requests"
                      component={PMOChangeRequests}
                    />
                    <PrivateRoute
                      exact
                      path="/change-requests"
                      component={ChangeRequestsList}
                    />
                    <PrivateRoute
                      exact
                      path="/Projects/:id/change-requests/:ChangeRequestID"
                      component={ChangeRequestDetails}
                    />
                    <PrivateRoute
                      path="/ProjectManagement/:id"
                      component={ProjectDetails}
                    />
                    <PrivateRoute
                      path="/Project/:id/meeting/:meetingID"
                      component={ProjectMeeting}
                    />
                    <PrivateRoute path="/email-logs" component={EmailLog} />
                    <PrivateRoute
                      path="/Project/:id/history"
                      component={HistoryPMO}
                    />
                    <PrivateRoute
                      path="/EngineerTimesheets"
                      component={EngineerTimesheets}
                    />
                    <PrivateRoute
                      exact
                      path="/reports/statistics"
                      component={StatisticsList}
                    />
                    <PrivateRoute
                      exact
                      path="/sow/dashboard"
                      component={SowMetrics}
                    />
                    <PrivateRoute
                      exact
                      path="/sales-activity"
                      component={SalesActivity}
                    />
                    <PrivateRoute
                      exact
                      path="/sales-activity/create"
                      component={SalesActivityDetails}
                    />
                    <PrivateRoute
                      exact
                      path="/sales-activity/:id/edit"
                      component={SalesActivityDetails}
                    />
                    <PrivateRoute
                      exact
                      path="/sow/order-tracking-v2"
                      component={OrderTrackingV2}
                    />
                    <PrivateRoute
                      exact
                      path="/operations/dash-board"
                      component={orderTrackingDashboard}
                    />
                    <PrivateRoute
                      exact
                      path="/product-catalog"
                      component={ProductCatalogListing}
                    />
                    <PrivateRoute
                      exact
                      path="/product-catalog/import"
                      component={ImportProducts}
                    />
                    <PrivateRoute
                      exact
                      path="/operations/email-generation-cw"
                      component={EmailGenerationCW}
                    />
                    <PrivateRoute
                      exact
                      path="/operations/linking"
                      component={OrdersLinking}
                    />
                    <PrivateRoute
                      exact
                      path="/purchase-history"
                      component={PurchaseOrderList}
                    />
                    <PrivateRoute
                      exact
                      path="/purchase-history/:id/"
                      component={PurchaseOrderDetail}
                    />
                    <PrivateRoute
                      path="/operations/email-generation-ingram"
                      component={EmailGenerationIngram}
                    />
                    <PrivateRoute
                      path="/setting/agreement-templates"
                      component={AgreementTemplateListing}
                    />
                    <PrivateRoute
                      path="/setting/agreement-template/:id"
                      component={CreateAgreementTemplate}
                    />
                    <PrivateRoute
                      path="/sow/agreements"
                      component={AgreementListing}
                    />
                    <PrivateRoute
                      path="/sow/agreement/:id"
                      component={CreateAgreement}
                    />
                    <PrivateRoute
                      path="/operations/order-tracking-v2"
                      component={OrderTrackingV2}
                    />
                    <PrivateRoute
                      exact
                      path="/client-360"
                      component={Client360}
                    />
                    <PrivateRoute
                      exact
                      path="/virtual-warehouse"
                      component={VirtualWarehouseListing}
                    />
                    <PrivateRoute exact path="*" component={NotFound} />
                  </Switch>
                ) : (
                  <Switch>
                    <PrivateRoute exact path="/profile" component={Profile} />
                    <PrivateRoute path="/setting/erp" component={Setting} />
                    <PrivateRoute
                      path="/setting/alias"
                      component={SettingAlias}
                    />
                    <PrivateRoute
                      path="/reset-password"
                      component={ResetPassword}
                    />
                    <PrivateRoute
                      path="/setting/api-credentials"
                      component={APICredentials}
                    />
                    <PrivateRoute default path="/" component={Disclaimer} />
                  </Switch>
                )}
              </div>
            </React.Suspense>
          </ErrorBoundary>
        </div>
        <Alert />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  loadingComponents: state.appState.loadingComponents,
  userProfile: state.auth.userProfile,
  themeColor: state.provider.themeColor,
  company_url: state.provider.company_url,
  enable_url: state.provider.enable_url,
  logo_url: state.provider.logo_url,
  updated_on: state.provider.updated_on,
  configStatus: state.providerIntegration.configStatus,
  fetchingToken: state.auth.fetchingToken,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProviderConfigStatus: () => dispatch(fetchProviderConfigStatus()),
  logOutRequest: () => dispatch(logOutRequest()),
  loginSuccess: (userProfile: IUserProfile) =>
    dispatch(loginSuccess(userProfile)),
  fetchCustomerProfile: () => dispatch(fetchCustomerProfile()),
  fetchProviderProfile: () => dispatch(fetchProviderProfile()),
  getRefreshToken: () => dispatch(getRefreshToken()),
  fetchUserProfile: () => dispatch(fetchUserProfile()),
  fetchCustomersShort: () => dispatch(fetchCustomersShort()),
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
