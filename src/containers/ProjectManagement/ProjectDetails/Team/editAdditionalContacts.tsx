import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  updateAdditionalContact,
  saveProjectAdditionalContactsByID,
  PROJECT_ADDITIONAL_CONTACTS_FAILURE,
  PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Spinner from "../../../../components/Spinner";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import ModalBase from "../../../../components/ModalBase/modalBase";
import Validator from "../../../../utils/validator";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { useProject } from "../projectContext";

const isValidPhoneNumber = (phone: string) => /^\d{10}$/.test(phone);

interface EditCustomerContactProps {
  data?: IProjectAdditionalContact;
  onClose: (saved?: boolean) => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  updateContact: (
    projectId: number,
    contactId: number,
    data: IProjectAdditionalContact
  ) => Promise<any>;
  saveContact: (
    projectId: number,
    data: IProjectAdditionalContact
  ) => Promise<any>;
}

const EditAdditionalContact: React.FC<EditCustomerContactProps> = (props) => {
  const { projectID } = useProject();
  const [saving, setSaving] = useState<boolean>(false);
  const [editInfo, setEditInfo] = useState<IProjectAdditionalContact>({
    name: "",
    email: "",
    role: "",
    phone: "",
  });
  const [error, setError] = useState<IErrorValidation>({});
  const isEditMode = Boolean(props.data);

  useEffect(() => {
    if (props.data) setEditInfo(props.data);
  }, [props.data]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEditInfo((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const isValid = () => {
    let isValid = true;
    const err = cloneDeep(error);
    const textFields = ["name", "email", "phone", "role"];
    textFields.forEach((field) => {
      if (!editInfo[field]) {
        isValid = false;
        err[field] = commonFunctions.getErrorState("This field is required");
      } else err[field] = commonFunctions.getErrorState();
    });
    if (editInfo.phone && !isValidPhoneNumber(editInfo.phone)) {
      isValid = false;
      err.phone = commonFunctions.getErrorState(
        "Please enter a valid phone number"
      );
    }
    if (editInfo.email && !Validator.isValidEmail(editInfo.email)) {
      isValid = false;
      err.email = commonFunctions.getErrorState("Please enter a valid email");
    }

    setError(err);
    return isValid;
  };

  const onSave = () => {
    if (isValid()) {
      setSaving(true);
      if (isEditMode)
        props
          .updateContact(projectID, props.data.id, editInfo)
          .then((action) => {
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
              props.addSuccessMessage("Resource updated successfully!");
              props.onClose(true);
            } else if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
              props.addErrorMessage("Error updating additional contact!");
              const err = action.errorList.data;
              if (err && Object.keys(err).length) {
                const serverErrors = cloneDeep(error);
                Object.keys(err).forEach((key) => {
                  serverErrors[key] = commonFunctions.getErrorState(err[key]);
                });
                setError(serverErrors);
              }
            }
          })
          .finally(() => setSaving(false));
      else
        props
          .saveContact(projectID, editInfo)
          .then((action) => {
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
              props.addSuccessMessage("Resource saved successfully!");
              props.onClose(true);
            } else if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
              props.addErrorMessage("Error saving additional contact!");
            }
          })
          .finally(() => setSaving(false));
    }
  };

  const getBody = () => {
    return (
      <div className="member-modal-body">
        <Spinner className="loader" show={saving} />
        <Input
          field={{
            label: "Name",
            type: "TEXT",
            value: editInfo.name,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="name"
          onChange={handleChange}
          placeholder={`Enter name`}
          error={error.name}
        />
        <Input
          field={{
            label: "Email",
            type: "TEXT",
            value: editInfo.email,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="email"
          onChange={handleChange}
          placeholder={`Enter email`}
          error={error.email}
        />
        <Input
          field={{
            label: "Role",
            type: "TEXT",
            value: editInfo.role,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="role"
          onChange={handleChange}
          placeholder={`Enter role`}
          error={error.role}
        />
        <Input
          field={{
            label: "Phone",
            type: "TEXT",
            value: editInfo.phone,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="phone"
          onChange={handleChange}
          placeholder={`Enter phone`}
          error={error.phone}
        />
      </div>
    );
  };

  return (
    <ModalBase
      show={true}
      onClose={props.onClose}
      titleElement={`${isEditMode ? "Edit" : "Create"} Vendor Resource`}
      bodyElement={getBody()}
      footerElement={
        <>
          <SquareButton
            content={"Close"}
            bsStyle={"default"}
            onClick={props.onClose}
            disabled={saving}
          />
          <SquareButton
            content={isEditMode ? "Update" : "Create"}
            bsStyle={"primary"}
            onClick={onSave}
            disabled={saving}
          />
        </>
      }
      className="team-tab-edit-modal"
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  updateContact: (
    projectId: number,
    contactId: number,
    data: IProjectAdditionalContact
  ) => dispatch(updateAdditionalContact(projectId, contactId, data)),
  saveContact: (projectId: number, data: IProjectAdditionalContact) =>
    dispatch(saveProjectAdditionalContactsByID(projectId, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditAdditionalContact);
