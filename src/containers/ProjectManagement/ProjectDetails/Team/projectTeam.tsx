import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  getProjectTeamMembers,
  FETCH_PROJECT_TEAM_SUCCESS,
} from "../../../../actions/pmo";
import SimpleList from "../../../../components/List";
import EditButton from "../../../../components/Button/editButton";
import { useProject } from "../projectContext";
import EditProjectTeam from "./editProjectTeam";

interface ProjectTeamProps {
  search: string;
  fetchData: (
    projectId: number,
    params: IServerPaginationParams
  ) => Promise<any>;
}

const searchFields = ["name", "project_role", "email", "phone_number"];

const ProjectTeam: React.FC<ProjectTeamProps> = (props) => {
  const { projectID } = useProject();
  const [ordering, setOrdering] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [rows, setRows] = useState<IProjectTeamMember[]>([]);
  const [editRow, setEditRow] = useState<IProjectTeamMember>();

  const onCloseEditModal = (saved?: boolean) => {
    if (saved === true) {
      fetchData();
    }
    setEditRow(undefined);
  };

  const columns: IListColumn<IProjectTeamMember>[] = [
    {
      name: "Name",
      id: "full_name",
    },
    {
      name: "Role",
      id: "project_role",
      ordering: "project_role",
    },
    {
      name: "Email Address",
      id: "email",
      ordering: "email",
    },
    {
      name: "Cell Phone",
      id: "full_cell_phone_number",
    },
    {
      name: "Office Phone",
      id: "full_office_phone_number",
    },
    {
      name: "",
      className: "team-row-actions",
      Cell: (row) => (
        <EditButton title="Edit User" onClick={() => setEditRow(row)} />
      ),
    },
  ];

  useEffect(() => {
    fetchData(ordering);
  }, [ordering]);

  const fetchData = (orderField?: string) => {
    setLoading(true);
    props
      .fetchData(projectID, { ordering: orderField, pagination: false })
      .then((action) => {
        if (action.type === FETCH_PROJECT_TEAM_SUCCESS) {
          setRows(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const searchedRows = useMemo(() => {
    return !props.search
      ? rows
      : rows.filter((row) =>
          searchFields.some(
            (field) =>
              row[field] && row[field].toLowerCase().includes(props.search)
          )
        );
  }, [rows, props.search]);

  const onOrderChange = (currentOrder: string) => {
    let orderBy = "";
    const prevOrder = ordering;
    switch (prevOrder) {
      case `-${currentOrder}`:
        orderBy = currentOrder;
        break;
      case currentOrder:
        orderBy = `-${currentOrder}`;
        break;
      default:
        orderBy = currentOrder;
        break;
    }
    setOrdering(orderBy);
  };

  return (
    <div className="project-team-container">
      <h3 className="listing-heading">LookingPoint Contacts</h3>
      <SimpleList
        uniqueKey={"id"}
        loading={loading}
        columns={columns}
        rows={searchedRows}
        currentOrder={ordering}
        className="project-team-list"
        onOrderChange={onOrderChange}
      />
      {Boolean(editRow) && (
        <EditProjectTeam data={editRow} onClose={onCloseEditModal} />
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchData: (projectId: number, params: IServerPaginationParams) =>
    dispatch(getProjectTeamMembers(projectId, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectTeam);
