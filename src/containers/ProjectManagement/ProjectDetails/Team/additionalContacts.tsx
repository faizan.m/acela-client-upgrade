import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  deleteAdditionalContact,
  getProjectAdditionalContactsByID,
  PROJECT_ADDITIONAL_CONTACTS_FAILURE,
  PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
} from "../../../../actions/pmo";
import SimpleList from "../../../../components/List";
import EditButton from "../../../../components/Button/editButton";
// import DeleteButton from "../../../../components/Button/deleteButton";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import EditAdditionalContacts from "./editAdditionalContacts";
import { useProject } from "../projectContext";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";

interface AdditionalContactsProps {
  search: string;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  deleteContact: (projectId: number, contactId: number) => Promise<any>;
  fetchData: (
    projectId: number,
    params: IServerPaginationParams
  ) => Promise<any>;
}

const searchFields = ["name", "role", "email", "phone"];

const AdditionalContacts: React.FC<AdditionalContactsProps> = (props) => {
  const { projectID } = useProject();
  const [ordering, setOrdering] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [rows, setRows] = useState<IProjectAdditionalContact[]>([]);
  const [editRow, setEditRow] = useState<IProjectAdditionalContact>();

  const onCloseEditModal = (saved?: boolean) => {
    if (saved === true) {
      fetchData();
    }
    setEditRow(undefined);
  };

  const onDeleteRowClick = (row: IProjectAdditionalContact) => {
    props.deleteContact(projectID, row.id).then((action) => {
      if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
        fetchData();
        props.addSuccessMessage("Resource deleted successfully!");
      } else if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
        props.addErrorMessage("Error deleting the resource!");
      }
    });
  };

  const columns: IListColumn<IProjectAdditionalContact>[] = [
    {
      name: "Name",
      id: "name",
      ordering: "name",
    },
    {
      name: "Role",
      id: "role",
      ordering: "role",
    },
    {
      name: "Email Address",
      id: "email",
      ordering: "email",
    },
    {
      name: "Phone Number",
      id: "phone",
    },
    {
      name: "",
      className: "team-row-actions",
      Cell: (row) => (
        <>
          <EditButton title="Edit User" onClick={() => setEditRow(row)} />
          <SmallConfirmationBox
            className="delete-meeting"
            text="resource"
            onClickOk={() => onDeleteRowClick(row)}
            showButton={true}
          />
        </>
      ),
    },
  ];

  useEffect(() => {
    fetchData(ordering);
  }, [ordering]);

  const fetchData = (orderField?: string) => {
    setLoading(true);
    props
      .fetchData(projectID, { ordering: orderField, pagination: false })
      .then((action) => {
        if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
          setRows(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const searchedRows = useMemo(() => {
    return !props.search
      ? rows
      : rows.filter((row) =>
          searchFields.some(
            (field) =>
              row[field] && row[field].toLowerCase().includes(props.search)
          )
        );
  }, [rows, props.search]);

  const onOrderChange = (currentOrder: string) => {
    let orderBy = "";
    const prevOrder = ordering;
    switch (prevOrder) {
      case `-${currentOrder}`:
        orderBy = currentOrder;
        break;
      case currentOrder:
        orderBy = `-${currentOrder}`;
        break;
      default:
        orderBy = currentOrder;
        break;
    }
    setOrdering(orderBy);
  };

  return (
    <div className="project-team-container">
      <h3 className="listing-heading">Resources</h3>
      <SimpleList
        uniqueKey={"id"}
        loading={loading}
        columns={columns}
        rows={searchedRows}
        currentOrder={ordering}
        className="project-team-list"
        onOrderChange={onOrderChange}
      />
      {Boolean(editRow) && (
        <EditAdditionalContacts data={editRow} onClose={onCloseEditModal} />
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  deleteContact: (projectId: number, contactId: number) =>
    dispatch(deleteAdditionalContact(projectId, contactId)),
  fetchData: (projectId: number, params: IServerPaginationParams) =>
    dispatch(getProjectAdditionalContactsByID(projectId, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdditionalContacts);
