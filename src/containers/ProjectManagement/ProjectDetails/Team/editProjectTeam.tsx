import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  updateProjectTeamMember,
  FETCH_PROJECT_TEAM_SUCCESS,
  FETCH_PROJECT_TEAM_FAILURE,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import ModalBase from "../../../../components/ModalBase/modalBase";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { useProject } from "../projectContext";
import Spinner from "../../../../components/Spinner";

const isValidPhoneNumber = (phone: string) => /^\d{10}$/.test(phone);

interface EditProjectTeamProps {
  data: IProjectTeamMember;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  updateProjectTeamMember: (
    projectId: number,
    memberId: string,
    data: IProjectTeamMember
  ) => Promise<any>;
  onClose: (saved?: boolean) => void;
}

const getLabelValueBox = (labelStr: string, value: string, width: number) => {
  return (
    <div className={`label-value-box field-section col-md-${width}`}>
      <div className="field__label row">
        <label className="field__label-label">{labelStr}</label>
      </div>
      <div className="box-field-value">{value}</div>
    </div>
  );
};

const EditProjectTeam: React.FC<EditProjectTeamProps> = (props) => {
  const { projectID } = useProject();
  const [saving, setSaving] = useState<boolean>(false);
  const [editInfo, setEditInfo] = useState<Partial<IProjectTeamMember>>({
    cell_phone_number: "",
    office_phone_number: "",
  });
  const [error, setError] = useState<IErrorValidation>({});

  useEffect(() => {
    if (props.data) {
      const obj: Partial<IProjectTeamMember> = {
        cell_phone_number: "",
        office_phone_number: "",
      };
      if (props.data.cell_phone_number && props.data.cell_phone_number.trim())
        obj.cell_phone_number = props.data.cell_phone_number;
      if (
        props.data.office_phone_number &&
        props.data.office_phone_number.trim()
      )
        obj.office_phone_number = props.data.office_phone_number;
      setEditInfo(obj);
    }
  }, [props.data]);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEditInfo((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const isValid = () => {
    let isValid = true;
    const err = cloneDeep(error);
    const phoneNumberFields = ["cell_phone_number", "office_phone_number"];
    phoneNumberFields.forEach((field) => {
      if (editInfo[field] && !isValidPhoneNumber(editInfo[field])) {
        isValid = false;
        err[field] = commonFunctions.getErrorState(
          "Please enter a valid phone number"
        );
      } else err[field] = commonFunctions.getErrorState();
    });
    setError(err);
    return isValid;
  };

  const onSave = () => {
    if (isValid()) {
      setSaving(true);
      const payload = {
        cell_phone_number: editInfo.cell_phone_number
          ? editInfo.cell_phone_number
          : null,
        office_phone_number: editInfo.office_phone_number
          ? editInfo.office_phone_number
          : null,
      } as IProjectTeamMember;
      props
        .updateProjectTeamMember(projectID, props.data.id, payload)
        .then((action) => {
          if (action.type === FETCH_PROJECT_TEAM_SUCCESS) {
            props.addSuccessMessage("Contact updated successfully!");
            props.onClose(true);
          } else if (action.type === FETCH_PROJECT_TEAM_FAILURE) {
            props.addErrorMessage("Error updating contact!");
          }
        })
        .finally(() => setSaving(false));
    }
  };

  const getBody = () => {
    return (
      <div className="member-modal-body">
        <Spinner className="loader" show={saving} />
        {getLabelValueBox("Name", props.data.full_name, 6)}
        {getLabelValueBox("Email", props.data.email, 6)}
        {getLabelValueBox("Project Role", props.data.project_role, 6)}
        <Input
          field={{
            label: "Cell Phone No.",
            type: "TEXT",
            value: editInfo.cell_phone_number,
            isRequired: false,
          }}
          width={6}
          multi={false}
          name="cell_phone_number"
          onChange={handleChange}
          placeholder={`Enter phone number`}
          error={error.cell_phone_number}
        />
        <Input
          field={{
            label: "Office Phone No.",
            type: "TEXT",
            value: editInfo.office_phone_number,
            isRequired: false,
          }}
          width={6}
          multi={false}
          name="office_phone_number"
          onChange={handleChange}
          placeholder={`Enter phone number`}
          error={error.office_phone_number}
        />
      </div>
    );
  };

  return (
    <ModalBase
      show={true}
      onClose={props.onClose}
      titleElement={`Edit Team Member`}
      bodyElement={getBody()}
      footerElement={
        <>
          <SquareButton
            content={"Close"}
            bsStyle={"default"}
            onClick={props.onClose}
          />
          <SquareButton
            content={"Update"}
            bsStyle={"primary"}
            onClick={onSave}
            disabled={saving}
          />
        </>
      }
      className="team-tab-edit-modal"
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  updateProjectTeamMember: (
    projectId: number,
    memberId: string,
    data: IProjectTeamMember
  ) => dispatch(updateProjectTeamMember(projectId, memberId, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProjectTeam);
