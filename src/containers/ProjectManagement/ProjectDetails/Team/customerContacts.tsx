import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  customerContactUD,
  fetchProjectCustomerContactsV2,
  PROJECT_CUSTOMER_CONTACTS_FAILURE,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
} from "../../../../actions/pmo";
import SimpleList from "../../../../components/List";
import EditButton from "../../../../components/Button/editButton";
import EditCustomerContacts from "./editCustomerContacts";
import { useProject } from "../projectContext";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";

interface CustomerContactsProps {
  search: string;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  deleteContact: (
    projectId: number,
    userId: string,
    data: ICustomerContactItem
  ) => Promise<any>;
  fetchData: (
    projectId: number,
    params: IServerPaginationParams
  ) => Promise<any>;
}

const searchFields = ["name", "project_role", "email"];

const CustomerContacts: React.FC<CustomerContactsProps> = (props) => {
  const { projectID } = useProject();
  const [ordering, setOrdering] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [rows, setRows] = useState<ICustomerContactItem[]>([]);
  const [editRow, setEditRow] = useState<ICustomerContactItem>();

  const onCloseEditModal = (saved?: boolean) => {
    if (saved === true) {
      fetchData();
    }
    setEditRow(undefined);
  };

  const onDeleteRowClick = (row: ICustomerContactItem) => {
    props.deleteContact(projectID, row.id, row).then((action) => {
      if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
        fetchData();
        props.addSuccessMessage("Contact deleted successfully!");
      } else if (action.type === PROJECT_CUSTOMER_CONTACTS_FAILURE) {
        props.addErrorMessage("Error deleting contact!");
      }
    });
  };

  const columns: IListColumn<ICustomerContactItem>[] = [
    {
      name: "Name",
      id: "full_name",
    },
    {
      name: "Role",
      id: "role",
      ordering: "role",
    },
    {
      name: "Email Address",
      id: "email",
      ordering: "email",
    },
    {
      name: "Cell Phone",
      id: "full_cell_phone_number",
    },
    {
      name: "Office Phone",
      id: "full_office_phone_number",
    },
    {
      name: "",
      className: "team-row-actions",
      Cell: (row) => (
        <>
          <EditButton title="Edit User" onClick={() => setEditRow(row)} />
          <SmallConfirmationBox
            className="delete-meeting"
            text="contact"
            onClickOk={() => onDeleteRowClick(row)}
            showButton={true}
          />
        </>
      ),
    },
  ];

  useEffect(() => {
    fetchData(ordering);
  }, [ordering]);

  const fetchData = (orderField?: string) => {
    setLoading(true);
    props
      .fetchData(projectID, { ordering: orderField, pagination: false })
      .then((action) => {
        if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
          setRows(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const searchedRows = useMemo(() => {
    return !props.search
      ? rows
      : rows.filter((row) =>
          searchFields.some(
            (field) =>
              row[field] && row[field].toLowerCase().includes(props.search)
          )
        );
  }, [rows, props.search]);

  const onOrderChange = (currentOrder: string) => {
    let orderBy = "";
    const prevOrder = ordering;
    switch (prevOrder) {
      case `-${currentOrder}`:
        orderBy = currentOrder;
        break;
      case currentOrder:
        orderBy = `-${currentOrder}`;
        break;
      default:
        orderBy = currentOrder;
        break;
    }
    setOrdering(orderBy);
  };

  return (
    <div className="project-team-container">
      <h3 className="listing-heading">Customer Contacts</h3>
      <SimpleList
        uniqueKey={"id"}
        loading={loading}
        columns={columns}
        rows={searchedRows}
        currentOrder={ordering}
        className="project-team-list"
        onOrderChange={onOrderChange}
      />
      {Boolean(editRow) && (
        <EditCustomerContacts data={editRow} onClose={onCloseEditModal} />
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  deleteContact: (
    projectId: number,
    userId: string,
    data: ICustomerContactItem
  ) => dispatch(customerContactUD("delete", projectId, userId, data)),
  fetchData: (projectId: number, params: IServerPaginationParams) =>
    dispatch(fetchProjectCustomerContactsV2(projectId, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CustomerContacts);
