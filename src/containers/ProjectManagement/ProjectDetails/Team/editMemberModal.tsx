// import React, { useEffect, useState } from "react";
// import { connect } from "react-redux";
// import Input from "../../../../components/Input/input";
// import { getContactRoles } from "../../../../actions/pmo";


// interface EditTeamMemberProps {
//   type?: MemberType; // To indicate type of screen, if not passed creation screen would be shown
//   getContactRoles: () => Promise<any>;
//   onClose: (saved?: boolean) => void;
//   data?:
//     | IProjectCustomerContact
//     | IProjectAdditionalContact
//     | IProjectTeamMember;
// }

// enum MemberType {
//   ADDITIONAL_CONTACT = "Resource",
//   CUSTOMER_CONTACT = "Customer Contact",
//   PROJECT_TEAM_MEMBER = "Project Team Member",
// }

// const getInitialData = (
//   type: MemberType
// ): IProjectCustomerContact | IProjectAdditionalContact => {
//   if (type === MemberType.CUSTOMER_CONTACT) {
//     return {
//       user_id: "",
//       email: "",
//       contact_crm_id: null,
//       name: "",
//       profile_url: "",
//       contact_record_id: null,
//       project_role: "",
//     };
//   } else if (type === MemberType.ADDITIONAL_CONTACT) {
//     return {
//       name: "",
//       email: "",
//       phone: "",
//       role: "",
//     };
//   } else {
//     return null; // Add support if necessary
//   }
// };

// // const memberTypeOptions = [
// //   MemberType.PROJECT_TEAM_MEMBER,
// //   MemberType.CUSTOMER_CONTACT,
// //   MemberType.ADDITIONAL_CONTACT,
// // ];


// const CustomerContactsModal : React.FC<EditTeamMemberProps> = (props) => {
//   const [roleOptions, setRoleOptions] = useState<IPickListOptions[]>([]);

//   useEffect(() => {
//     fetchCustomerRoleOptions();
//   }, [])

//   const fetchCustomerRoleOptions = async () => {
//     const action = await props.getContactRoles();
//     setRoleOptions(
//       action.response.map((t) => ({
//         value: t.id,
//         label: `${t.role}`,
//       }))
//     );
//   };
//   return ();
// }


// const EditTeamMember: React.FC<EditTeamMemberProps> = (props) => {
//   const [screenType, setScreenType] = useState<MemberType>(
//     MemberType.CUSTOMER_CONTACT
//   );
//   const [data, setData] = useState<
//     IProjectCustomerContact | IProjectAdditionalContact
//   >({} as IProjectCustomerContact);

//   const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
//     setData((prev) => ({
//       ...prev,
//       [e.target.name]: e.target.value,
//     }));
//   };

//   const CustomerContactBody = () => {
//     // const data;
//     return (
//       <>
//         <Input
//           field={{
//             label: "First Name",
//             type: "TEXT",
//             value: data.name, // change this
//             isRequired: false,
//           }}
//           width={6}
//           multi={false}
//           name="first_name"
//           onChange={handleChange}
//           placeholder={`Enter first name`}
//           // error={error.first_name}
//         />
//         <Input
//           field={{
//             label: "Last Name",
//             type: "TEXT",
//             value: data.name, // change this
//             isRequired: false,
//           }}
//           width={6}
//           multi={false}
//           name="first_name"
//           onChange={handleChange}
//           placeholder={`Enter first name`}
//           // error={error.first_name}
//         />
//         <Input
//           field={{
//             label: "Email",
//             type: "TEXT",
//             value: data.email, // change this
//             isRequired: false,
//           }}
//           width={6}
//           multi={false}
//           name="email"
//           onChange={handleChange}
//           placeholder={`Enter email`}
//           // error={error.email}
//         />
//         <Input
//           field={{
//             label: "Role",
//             type: "PICKLIST",
//             value: null, // change this
//             isRequired: false,
//           }}
//           width={6}
//           multi={false}
//           name="email"
//           onChange={handleChange}
//           placeholder={`Enter email`}
//           // error={error.email}
//         />
//       </>
//     );
//   };

//   const AdditionalContactBody = () => {
//     return <div />;
//   };

//   const ProjectTeamBody = () => {
//     return <div />;
//   };

//   const handleChangeSwitch = (e: React.ChangeEvent<HTMLInputElement>) => {
//     if (e.target.checked) {
//       setData(getInitialData(MemberType.ADDITIONAL_CONTACT));
//       setScreenType(MemberType.ADDITIONAL_CONTACT);
//     } else {
//       setData(getInitialData(MemberType.CUSTOMER_CONTACT));
//       setScreenType(MemberType.CUSTOMER_CONTACT);
//     }
//   };

//   const getBody = () => {
//     switch (screenType) {
//       case MemberType.CUSTOMER_CONTACT:
//         <CustomerContactBody />;
//       case MemberType.ADDITIONAL_CONTACT:
//         <AdditionalContactBody />;
//       case MemberType.PROJECT_TEAM_MEMBER:
//         <ProjectTeamBody />;
//       default:
//         return null;
//     }
//   };

//   return (
//     <div className="project-details-modal">
//       {!props.type && (
//         <div className="open-won-switch customer-resource">
//           <span>Customer</span>
//           <input
//             className="switch"
//             type="checkbox"
//             onChange={handleChangeSwitch}
//           />
//           <span>Resource</span>
//         </div>
//       )}
//       {getBody()}
//     </div>
//   );
// };

// const mapStateToProps = (state: IReduxStore) => ({});

// const mapDispatchToProps = (dispatch: any) => ({
//   getContactRoles: () => dispatch(getContactRoles()),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(EditTeamMember);
