import React, { useEffect, useMemo, useState } from "react";
import { debounce } from "lodash";
import ProjectTeam from "./projectTeam";
import CustomerContacts from "./customerContacts";
import AdditionalContacts from "./additionalContacts";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import ModalBase from "../../../../components/ModalBase/modalBase";
import EditCustomerContacts from "./editCustomerContacts";
import EditAdditionalContacts from "./editAdditionalContacts";
import { useProject } from "../projectContext";

const TeamTab = () => {
  const { history, location } = useProject();
  const [searchInput, setSearchInput] = useState("");
  const [showChoices, setShowChoices] = useState<boolean>(false);
  const [keys, setKeys] = useState({
    customer_contact: Math.random(),
    vendor_resource: Math.random(),
  });
  const [debouncedSearchInput, setDebouncedSearchInput] = useState("");

  useEffect(() => {
    // Function to update query parameter
    const updateTabQueryParam = () => {
      const searchParams = new URLSearchParams(location.search);
      searchParams.set("tab", "Team");
      history.replace({
        pathname: location.pathname,
        search: `?${searchParams.toString()}`,
      });
    };
    updateTabQueryParam();
  }, [history, location.pathname, location.search]);

  const debouncedSearch = useMemo(
    () =>
      debounce((value: string) => {
        setDebouncedSearchInput(value);
      }, 500),
    []
  );

  const onSaveContact = (
    saved?: boolean,
    type?: "customer_contact" | "vendor_resource"
  ) => {
    if (saved) {
      setKeys((prev) => ({
        ...prev,
        [type]: Math.random(),
      }));
    }
    closeCreationModal();
  };

  const closeCreationModal = () => {
    setShowChoices(false);
  };

  const handleSearchChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const searchValue = e.target.value.toLowerCase();
    setSearchInput(searchValue);
    debouncedSearch(searchValue);
  };

  return (
    <>
      <div className="project-team-tab">
        <div className="team-tab-action">
          <Input
            field={{
              value: searchInput,
              label: "",
              type: "SEARCH",
            }}
            width={10}
            name="searchString"
            onChange={handleSearchChange}
            placeholder="Search"
            className={`team-tab-search`}
          />
          <div className="search-btn-separator" />
          <SquareButton
            content="Add"
            bsStyle={"primary"}
            className="add-team-contact"
            onClick={() => setShowChoices(true)}
          />
        </div>
        <ProjectTeam search={debouncedSearchInput} />
        <CustomerContacts
          key={keys.customer_contact}
          search={debouncedSearchInput}
        />
        <AdditionalContacts
          key={keys.vendor_resource}
          search={debouncedSearchInput}
        />
      </div>
      {showChoices && (
        <CreateContactModal
          onSave={onSaveContact}
          onClose={closeCreationModal}
        />
      )}
    </>
  );
};

const CreateContactModal = ({
  onSave,
  onClose,
}: {
  onClose: () => void;
  onSave: (
    saved?: boolean,
    type?: "customer_contact" | "vendor_resource"
  ) => void;
}) => {
  const [screen, setScreen] = useState<string>("customer_contact");
  const [showCreationModal, setShowCreationModal] = useState<boolean>(false);

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setScreen(e.target.value);
  };

  const onConfirm = () => {
    setShowCreationModal(true);
  };

  const onCloseResourceModal = (saved?: boolean) => {
    if (saved === true) {
      onSave(true, "vendor_resource");
      setShowCreationModal(false);
      setScreen("customer_contact");
    } else onClose();
  };

  const onCloseCustomerModal = (saved?: boolean) => {
    if (saved === true) {
      onSave(true, "customer_contact");
      setShowCreationModal(false);
    } else onClose();
  };

  return (
    <>
      {showCreationModal && screen === "customer_contact" && (
        <EditCustomerContacts onClose={onCloseCustomerModal} />
      )}
      {showCreationModal && screen === "vendor_resource" && (
        <EditAdditionalContacts onClose={onCloseResourceModal} />
      )}
      <ModalBase
        show={!showCreationModal}
        onClose={onClose}
        titleElement={`Create Contact`}
        bodyElement={
          <Input
            field={{
              value: screen,
              label: "Which type of contact you want to create?",
              type: "RADIO",
              isRequired: false,
              options: [
                { value: "customer_contact", label: "Customer Contact" },
                { value: "vendor_resource", label: "Vendor Resource" },
              ],
            }}
            width={6}
            name="screen"
            onChange={onChange}
          />
        }
        footerElement={
          <>
            <SquareButton
              content={"Close"}
              bsStyle={"default"}
              onClick={onClose}
            />
            <SquareButton
              content={"Confirm"}
              bsStyle={"primary"}
              onClick={onConfirm}
            />
          </>
        }
        className="team-tab-edit-modal"
      />
    </>
  );
};

export default TeamTab;
