import React, { useEffect, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  getContactRoles,
  customerContactUD,
  createCustomerContactV2,
  NEW_PROJECT_BOARD_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_FAILURE,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../../../actions/documentation";
import Spinner from "../../../../components/Spinner";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import ModalBase from "../../../../components/ModalBase/modalBase";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { useProject } from "../projectContext";
import AddUser from "../../../SOW/Sow/addUser";

const isValidPhoneNumber = (phone: string) => /^\d{10}$/.test(phone);

interface EditCustomerContactProps {
  data?: ICustomerContactItem;
  addErrorMessage: TShowErrorMessage;
  getContactRoles: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  fetchCustomerUsers: (id: number) => Promise<any>;
  createCustomerContact: (projectId: number, data: any) => Promise<any>;
  editContact: (
    projectId: number,
    userId: string,
    data: ICustomerContactItem
  ) => Promise<any>;
  onClose: (saved?: boolean) => void;
}

const getLabelValueBox = (labelStr: string, value: string, width: number) => {
  return (
    <div className={`label-value-box field-section col-md-${width}`}>
      <div className="field__label row">
        <label className="field__label-label">{labelStr}</label>
      </div>
      <div className="box-field-value">{value}</div>
    </div>
  );
};

interface CustomerContactEdit {
  cell_phone_number?: string;
  office_phone_number?: string;
  role: number;
  user?: string;
  contact_crm_id?: number;
}

const EditCustomerContact: React.FC<EditCustomerContactProps> = (props) => {
  const { projectID, project } = useProject();
  const [saving, setSaving] = useState<boolean>(false);
  const [showModal, setShowModal] = useState<boolean>(false);
  const [editInfo, setEditInfo] = useState<CustomerContactEdit>({
    cell_phone_number: "",
    office_phone_number: "",
    role: null,
    user: "",
    contact_crm_id: null,
  });
  const [error, setError] = useState<IErrorValidation>({});
  const [fetchingRoles, setFetchingRoles] = useState<boolean>(false);
  const [fetchingUsers, setFetchingUsers] = useState<boolean>(false);
  const [roleOptions, setRoleOptions] = useState<IPickListOptions[]>([]);
  const [userOptions, setUserOptions] = useState<IPickListOptions[]>([]);
  const isCreateMode = !Boolean(props.data);

  useEffect(() => {
    fetchRoles();
    if (props.data) {
      const obj: CustomerContactEdit = { role: null };
      if (props.data.cell_phone_number && props.data.cell_phone_number.trim())
        obj.cell_phone_number = props.data.cell_phone_number;
      if (
        props.data.office_phone_number &&
        props.data.office_phone_number.trim()
      )
        obj.office_phone_number = props.data.office_phone_number;
      obj.role = props.data.role_id;
      setEditInfo(obj);
    } else fetchCustomerUsers();
  }, [props.data]);

  const fetchCustomerUsers = () => {
    setFetchingUsers(true);
    props
      .fetchCustomerUsers(project.customer_id)
      .then((action) => {
        if (action.type === FETCH_ALL_CUST_USERS_SUCCESS) {
          const customerUsers = action.response.map((data) => ({
            value: data.id,
            label: `${data.first_name} ${data.last_name}`,
            data: data.crm_id,
          }));
          setUserOptions(customerUsers);
        }
      })
      .finally(() => setFetchingUsers(false));
  };

  const fetchRoles = () => {
    setFetchingRoles(true);
    props
      .getContactRoles()
      .then((action) => {
        if (action.type === NEW_PROJECT_BOARD_SUCCESS)
          setRoleOptions(
            action.response.map((t) => ({
              value: t.id,
              label: `${t.role}`,
            }))
          );
      })
      .finally(() => setFetchingRoles(false));
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEditInfo((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const isValid = () => {
    let isValid = true;
    const err = cloneDeep(error);
    if (!isCreateMode) {
      const phoneNumberFields = ["cell_phone_number", "office_phone_number"];
      phoneNumberFields.forEach((field) => {
        if (editInfo[field] && !isValidPhoneNumber(editInfo[field])) {
          isValid = false;
          err[field] = commonFunctions.getErrorState(
            "Please enter a valid phone number"
          );
        } else err[field] = commonFunctions.getErrorState();
      });
    } else {
      if (!editInfo.user) {
        isValid = false;
        err.user = commonFunctions.getErrorState("This field is required");
      } else err.user = commonFunctions.getErrorState();
    }
    if (!editInfo.role) {
      isValid = false;
      err.role = commonFunctions.getErrorState("This field is required");
    } else err.role = commonFunctions.getErrorState();
    setError(err);
    return isValid;
  };

  const closeUserModal = () => {
    setShowModal(false);
    fetchCustomerUsers();
  };

  const onSave = () => {
    if (isValid()) {
      setSaving(true);
      if (isCreateMode) {
        const payload = {
          role: editInfo.role,
          user: editInfo.user,
          contact_crm_id: userOptions.find((el) => editInfo.user === el.value)
            .data,
        };
        props
          .createCustomerContact(projectID, payload)
          .then((action) => {
            if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
              props.addSuccessMessage("Customer contact created successfully!");
              props.onClose(true);
            } else if (action.type === PROJECT_CUSTOMER_CONTACTS_FAILURE) {
              props.addErrorMessage("Error creating customer contact!");
            }
          })
          .finally(() => setSaving(false));
      } else {
        const payload = {
          cell_phone_number: editInfo.cell_phone_number
            ? editInfo.cell_phone_number
            : null,
          office_phone_number: editInfo.office_phone_number
            ? editInfo.office_phone_number
            : null,
          role: editInfo.role as any,
        } as ICustomerContactItem;
        props
          .editContact(projectID, props.data.id, payload)
          .then((action) => {
            if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
              props.addSuccessMessage("Customer Contact updated successfully!");
              props.onClose(true);
            } else if (action.type === PROJECT_CUSTOMER_CONTACTS_FAILURE) {
              props.addErrorMessage("Error updating customer contact!");
            }
          })
          .finally(() => setSaving(false));
      }
    }
  };

  const getBody = () => {
    return (
      <div className="member-modal-body">
        <Spinner className="loader" show={saving} />
        {isCreateMode ? (
          <>
            <SquareButton
              content="+"
              onClick={() => setShowModal(true)}
              className="add-new-user-pmo"
              bsStyle={"primary"}
              title="Add New User"
              disabled={!project.customer_id}
            />
            <Input
              field={{
                label: "User",
                type: "PICKLIST",
                value: editInfo.user,
                options: userOptions,
                isRequired: true,
              }}
              width={6}
              multi={false}
              name="user"
              onChange={handleChange}
              loading={fetchingUsers}
              placeholder={`Select User`}
              error={error.user}
            />
            <Input
              field={{
                label: "Project Role",
                type: "PICKLIST",
                value: editInfo.role,
                options: roleOptions,
                isRequired: true,
              }}
              width={6}
              multi={false}
              name="role"
              onChange={handleChange}
              loading={fetchingRoles}
              placeholder={`Select Role`}
              error={error.role}
            />
          </>
        ) : (
          <>
            {getLabelValueBox("Name", props.data.full_name, 6)}
            {getLabelValueBox("Email", props.data.email, 6)}
            <Input
              field={{
                label: "Project Role",
                type: "PICKLIST",
                value: editInfo.role,
                options: roleOptions,
                isRequired: false,
              }}
              width={6}
              multi={false}
              name="role"
              onChange={handleChange}
              loading={fetchingRoles}
              placeholder={`Select Role`}
              error={error.role}
            />
            <Input
              field={{
                label: "Cell Phone No.",
                type: "TEXT",
                value: editInfo.cell_phone_number,
                isRequired: false,
              }}
              width={6}
              multi={false}
              name="cell_phone_number"
              onChange={handleChange}
              placeholder={`Enter phone number`}
              error={error.cell_phone_number}
            />

            <Input
              field={{
                label: "Office Phone No.",
                type: "TEXT",
                value: editInfo.office_phone_number,
                isRequired: false,
              }}
              width={6}
              multi={false}
              name="office_phone_number"
              onChange={handleChange}
              placeholder={`Enter phone number`}
              error={error.office_phone_number}
            />
          </>
        )}
      </div>
    );
  };

  return (
    <>
      <ModalBase
        show={true}
        onClose={props.onClose}
        titleElement={`${isCreateMode ? "Create" : "Edit"} Customer Contact`}
        bodyElement={getBody()}
        footerElement={
          <>
            <SquareButton
              content={"Close"}
              bsStyle={"default"}
              onClick={props.onClose}
              disabled={saving}
            />
            <SquareButton
              content={isCreateMode ? "Create" : "Update"}
              bsStyle={"primary"}
              onClick={onSave}
              disabled={saving}
            />
          </>
        }
        className="team-tab-edit-modal"
      />
      {showModal && (
        <AddUser
          isVisible={true}
          close={closeUserModal}
          customerId={project.customer_id}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getContactRoles: () => dispatch(getContactRoles()),
  fetchCustomerUsers: (id: number) => dispatch(fetchAllCustomerUsers(id)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  createCustomerContact: (projectId: number, data: any) =>
    dispatch(createCustomerContactV2(projectId, data)),
  editContact: (
    projectId: number,
    userId: string,
    data: ICustomerContactItem
  ) => dispatch(customerContactUD("put", projectId, userId, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditCustomerContact);
