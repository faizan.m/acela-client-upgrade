import React, { useState } from "react";
import { connect } from "react-redux";
import {
  deleteActionItem,
  uploadProcessCR,
  createChangeRequests,
  UPDATE_CHANGE_REQ_SUCCESS,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../../components/Button/deleteButton";
import InfiniteListing from "../../../../components/InfiniteList/infiniteList";
import { useProject } from "../projectContext";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import {
  DOWNLOAD_SOW_SUCCESS,
  downloadSOWDocumnet,
} from "../../../../actions/sow";
import ModalBase from "../../../../components/ModalBase/modalBase";
import Spinner from "../../../../components/Spinner";
import IconButton from "../../../../components/Button/iconButton";

interface ChangeRequestListProps {
  addSuccessMessage?: TShowErrorMessage;
  addErrorMessage?: TShowErrorMessage;
  deleteMeeting?: (
    projectId: number,
    meetingId: number,
    api: string
  ) => Promise<any>;
  downloadSOWDocumnet?: (
    id: number,
    type: string,
    name?: string
  ) => Promise<any>;
  uploadProcessCR?: (id: number, CRID: number, data: any) => Promise<any>;
  createChangeRequests?: (projectID: number, data: any) => Promise<any>;
}

const ChangeRequestList: React.FC<ChangeRequestListProps> = (props) => {
  const {
    projectID,
    projectTeam,
    customerContacts,
    location,
    history,
  } = useProject();
  const [refreshKey, setRefreshKey] = useState<number>(Math.random());
  const [previewPDFIds, setPreviewPDFIds] = useState<number[]>([]);
  const [downloadingIds, setDownloadingIds] = useState<number[]>([]);

  const [openPreview, setOpenPreview] = useState<boolean>(false);
  const [updatingIds, setUpdadingIds] = useState<number[]>([]);
  const [previewHTML, setPreviewHTML] = useState<IFileResponse>(null);
  const [show, setShow] = useState<boolean>(false);

  const [uploading, setUploading] = useState<boolean>(false);

  const [attachment, setAttachment] = useState<File>(null);
  const [changeRequestId, setChangeRequestId] = useState<number>(0);

  const goToDetails = (changeRequest: ICRListItem) => {
    const query = new URLSearchParams(location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    history.push(
      `/Projects/${projectID}/change-requests/${changeRequest.id}?&AllProject=${AllProject}`
    );
  };

  const onSaveClick = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number,
    row: ICRListItem
  ) => {
    setUpdadingIds([...updatingIds, row.id]);
    row.status = e.target.value;
    props.createChangeRequests(projectID, row).then((action) => {
      if (action.type === UPDATE_CHANGE_REQ_SUCCESS) {
        setUpdadingIds([...updatingIds.filter((id) => row.id !== id)]);
        props.addSuccessMessage(
          `Status update for ${row.name} to ${row.status}!`
        );
      }
    });
  };

  const columns: IColumnInfinite<ICRListItem>[] = [
    {
      name: "Type & Name",
      id: "name",
      ordering: "name",
      Cell: (row) => (
        <div
          className="first-column"
          onClick={() => goToDetails(row)}
          title={row.name}
        >
          {row.change_request_type === "Fixed Fee" && (
            <img
              className="icon-list"
              title="Fixed Fee"
              alt=""
              src="/assets/icons/coin.svg"
            />
          )}
          {row.change_request_type === "T & M" && (
            <img
              className="icon-list"
              title={"T & M"}
              alt=""
              src="/assets/icons/timer.svg"
            />
          )}
          <div>{"  " + row.name}</div>
        </div>
      ),
    },

    {
      name: "Requested By",
      id: "requested",
      className: "width-15",
      Cell: (row) => {
        const requested =
          customerContacts &&
          customerContacts.find((u) => u.user_id === row.requested_by);
        return (
          requested && (
            <>
              <div
                style={{
                  backgroundImage: `url(${
                    requested && requested.profile_url
                      ? `${requested.profile_url}`
                      : "/assets/icons/user-filled-shape.svg"
                  })`,
                }}
                className="pm-image-circle"
              />
              <div className="type" title={`${requested.name}`}>
                {`${requested.name}`}
              </div>
            </>
          )
        );
      },
    },

    {
      name: "Presales Engineer",
      id: "requested",
      className: "width-15",
      Cell: (row) => {
        const assigned_to =
          projectTeam && projectTeam.find((u) => u.id === row.assigned_to);
        return (
          assigned_to && (
            <>
              <div
                style={{
                  backgroundImage: `url(${
                    assigned_to && assigned_to.profile_url
                      ? `${assigned_to.profile_url}`
                      : "/assets/icons/user-filled-shape.svg"
                  })`,
                }}
                className="pm-image-circle"
              />
              <div className="type" title={assigned_to.full_name}>
                {assigned_to.full_name}
              </div>
            </>
          )
        );
      },
    },
    {
      name: "Created",
      id: "created_on",
      className: "width-8",
      Cell: (row) =>
        fromISOStringToFormattedDate(row.created_on, "MMM DD, YYYY"),
    },
    {
      name: "Updated",
      id: "updated_on",
      className: "width-8",
      Cell: (row) =>
        fromISOStringToFormattedDate(row.updated_on, "MMM DD, YYYY"),
    },
    {
      name: "Amount",
      id: "name",
      className: "width-7",
      Cell: (row) => {
        const formatter = new Intl.NumberFormat("en-US", {
          style: "currency",
          currency: "USD",
        });
        return formatter.format(
          (row &&
            row.json_config &&
            row.json_config.service_cost &&
            row.json_config.service_cost.customer_cost) ||
            0
        );
      },
    },
    {
      name: "Number",
      id: "change_number",
      className: "width-5",
      Cell: (row) => row.change_number || 0,
    },
    {
      name: "Status",
      id: "updated_on",
      className: "width-15 align-center",
      Cell: (row, index) => (
        <Input
          field={{
            label: "",
            type: "PICKLIST",
            value: row.status,
            isRequired: false,
            options: ["In Progress", "Cancelled/Rejected", "Approved"].map(
              (x) => ({
                value: x,
                label: <div className={`label-user-dropdown ${x}`}>{x}</div>,
                disabled: x === "Approved",
              })
            ),
          }}
          width={12}
          name="status"
          onChange={(e) => onSaveClick(e, index, row)}
          placeholder={""}
          disabled={row.status === "Approved"}
          className={`list-selection`}
          loading={updatingIds.includes(row.id)}
        />
      ),
    },
    {
      name: "",
      id: "",
      className: "cr-icons-col",
      Cell: (row) => {
        return (
          <>
            {row.status === "In Progress" ? (
              <img
                onClick={() => goToDetails(row)}
                className={"d-pointer edit-png"}
                alt=""
                src={"/assets/icons/edit.png"}
                title={"Edit in details"}
              />
            ) : (
              <div className="blank-img">&nbsp;&nbsp;</div>
            )}
            {getPDFPrevieMarkUp(row)}
            {getDOCXDownloadMarkUp(row.sow_document)}
            {row.status === "In Progress" && (
              <IconButton
                className="action-button-clt"
                icon="process.png"
                onClick={() => {
                  setShow(true);
                  setChangeRequestId(row.id);
                }}
                title={"Proccess CR"}
              />
            )}
          </>
        );
      },
    },
  ];
  const getPDFPrevieMarkUp = (row: ICRListItem) => {
    if (previewPDFIds.includes(row.id)) {
      return (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Downloading File"
        />
      );
    } else {
      return (
        <DeleteButton
          type="pdf_preview"
          title="Preview"
          onClick={() => onPDFPreviewClick(row)}
        />
      );
    }
  };

  const getDOCXDownloadMarkUp = (cell: number) => {
    if (downloadingIds.includes(cell)) {
      return (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Downloading File"
        />
      );
    } else {
      return (
        <DeleteButton
          type="pdf_download"
          title="PDF Download"
          onClick={(e) => getStream(cell)}
        />
      );
    }
  };

  const onPDFPreviewClick = (changeRequest: ICRListItem) => {
    setPreviewPDFIds([...previewPDFIds, changeRequest.id]);
    props
      .downloadSOWDocumnet(changeRequest.sow_document, "pdf", "")
      .then((a) => {
        if (a.type === DOWNLOAD_SOW_SUCCESS) {
          setOpenPreview(true);
          setPreviewHTML(a.response);
          setPreviewPDFIds(
            previewPDFIds.filter((id) => changeRequest.id !== id)
          );
        }
      });
  };

  const getStream = (doc: number) => {
    setDownloadingIds([...downloadingIds, doc]);
    props.downloadSOWDocumnet(doc, "pdf").then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        const url = action.response.file_path;
        const link = document.createElement("a");
        link.href = url;
        link.target = "_blank";
        link.setAttribute("download", action.response.file_name);
        document.body.appendChild(link);
        link.click();
        setDownloadingIds(downloadingIds.filter((id) => doc !== id));
      }
    });
  };

  const toggleOpenPreview = () => {
    setOpenPreview(!openPreview);
    setPreviewHTML(null);
  };

  const clearPopUp = (e: boolean = false) => {
    setShow(e);
  };

  const onUploadClick = () => {
    if (attachment) {
      setUploading(true);
      const data = new FormData();
      data.append("files", attachment);
      props.uploadProcessCR(projectID, changeRequestId, data).then((action) => {
        if (action.type === "PROCES_CR_UPLOAD_SUCCESS") {
          setRefreshKey(Math.random());
          props.addSuccessMessage("Upload Successful!");
        }
        if (action.type === "PROCES_CR_UPLOAD_FAILURE") {
          if (action.errorList.data && action.errorList.data.detail) {
            props.addErrorMessage(action.errorList.data.detail);
          } else props.addErrorMessage("Upload Failed!");
        }
        setUploading(false);
        setChangeRequestId(0);
        setShow(false);
      });
    } else {
      props.addErrorMessage("Invalid format.");
    }
  };

  const handleFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const attachment: File = files[0];
    setAttachment(attachment);
  };

  return (
    <div className="change-request-list-container">
      <h3>Change Request(s)</h3>
      <div className="add-section">
        <div className="search-add-separator" />
        <SquareButton
          content="+"
          bsStyle={"primary"}
          onClick={() => {
            const query = new URLSearchParams(location.search);
            const AllProject =
              query.get("AllProject") === "true" ? true : false;
            history.push(
              `/Projects/${projectID}/change-requests/0?AllProject=${AllProject}`
            );
          }}
          title="Create Change Request"
          className="cr-list-add"
        />
      </div>
      <PDFViewer
        show={openPreview}
        onClose={toggleOpenPreview}
        titleElement={`View Change Request Preview`}
        previewHTML={previewHTML}
        footerElement={
          <SquareButton
            content="Close"
            bsStyle={"default"}
            onClick={toggleOpenPreview}
          />
        }
        className=""
      />
      <ModalBase
        show={show}
        hideCloseButton={uploading}
        onClose={() => clearPopUp()}
        titleElement={"Change Request Processing"}
        bodyElement={
          <>
            <div className="loader">
              <Spinner show={uploading} />
            </div>
            <Input
              field={{
                label: "",
                type: "FILE",
                value: `${attachment ? attachment : ""}`,
                id: "attachment",
              }}
              width={9}
              name=""
              accept="*"
              onChange={handleFile}
              className="custom-file-input upload-btn"
            />
          </>
        }
        footerElement={
          <div className="col-md-12">
            {
              <SquareButton
                content={"Save"}
                bsStyle={"primary"}
                onClick={onUploadClick}
                className=""
                disabled={!attachment || uploading}
              />
            }
            <SquareButton
              content={"Close"}
              bsStyle={"default"}
              onClick={() => clearPopUp(!show)}
              disabled={uploading}
            />
          </div>
        }
        className={`confirm`}
      />

      <InfiniteListing
        showSearch={true}
        refreshKey={refreshKey}
        id="projectChangeRequestList"
        className="change-request-infinite-list"
        url={`providers/pmo/projects/${projectID}/change-requests`}
        checkBoxfilters={[]}
        columns={columns}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  deleteMeeting: (projectId: number, meetingId: number, api: string) =>
    dispatch(deleteActionItem(projectId, meetingId, api)),
  downloadSOWDocumnet: (id: number, type: string, name?: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
  uploadProcessCR: (id: number, CRID: number, data: any) =>
    dispatch(uploadProcessCR(id, CRID, data)),
  createChangeRequests: (projectID: number, data: any) =>
    dispatch(createChangeRequests(projectID, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChangeRequestList);
