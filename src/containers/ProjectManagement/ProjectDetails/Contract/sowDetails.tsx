import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  fetchSowDocs,
  downloadSOWDocumnet,
  DOWNLOAD_SOW_SUCCESS,
  FETCH_SOW_DOC_LIST_SUCCESS,
  FETCH_SOW_DOC_LIST_FAILURE,
} from "../../../../actions/sow";
import { addErrorMessage } from "../../../../actions/appState";
import SquareButton from "../../../../components/Button/button";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../../components/Button/deleteButton";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import { useProject } from "../projectContext";

interface SowDetailProps {
  addErrorMessage: TShowErrorMessage;
  getSOW: (sowId: number) => Promise<any>;
  previewSow: (id: number) => Promise<any>;
}

const SowDetails: React.FC<SowDetailProps> = (props) => {
  const { project } = useProject();
  const [sow, setSow] = useState<ISowDocListRow>();
  const [loading, setLoading] = useState<boolean>();
  const [pdfPreviewData, setPDFPreviewData] = useState<{
    loading: boolean;
    openPreview: boolean;
    previewHTML: IFileResponse;
  }>({
    loading: false,
    openPreview: false,
    previewHTML: null,
  });

  useEffect(() => {
    if (project.sow_document) fetchSowDocument(project.sow_document);
  }, [project.sow_document]);

  const fetchSowDocument = (id: number) => {
    setLoading(true);
    props
      .getSOW(id)
      .then((action) => {
        if (action.type === FETCH_SOW_DOC_LIST_SUCCESS)
          setSow(action.response[0]);
        else if (action.type === FETCH_SOW_DOC_LIST_FAILURE)
          props.addErrorMessage("Error fetching linked sow!");
      })
      .finally(() => setLoading(false));
  };

  const onPDFPreviewClick = () => {
    setPDFPreviewData((prev) => ({ ...prev, loading: true }));
    props.previewSow(sow.id).then((a) => {
      if (a.type === DOWNLOAD_SOW_SUCCESS) {
        setPDFPreviewData({
          openPreview: true,
          loading: false,
          previewHTML: a.response,
        });
      }
    });
  };

  const closePreview = () => {
    setPDFPreviewData((prev) => ({
      ...prev,
      openPreview: false,
      previewHTML: null,
    }));
  };

  const getString = (s: string | number, noData: string = "") =>
    s ? String(s) : noData;

  return (
    <div className="sow-details details-box">
      <h3 className="details-heading">SoW</h3>
      {loading && <div className="details-loader">Loading...</div>}
      {!loading && sow && (
        <div className="body">
          <div className="sow-details-actions">
            {pdfPreviewData.loading ? (
              <img
                className="icon__loading"
                src="/assets/icons/loading.gif"
                alt="Loading Preview"
              />
            ) : (
              <DeleteButton
                type="pdf_preview"
                title="Preview"
                onClick={onPDFPreviewClick}
              />
            )}
          </div>
          <div className="field">
            <label>Name</label>
            <label>{sow.name}</label>
          </div>
          <div className="field">
            <label>Opportunity</label>
            <label>{sow.quote.name}</label>
          </div>
          <div className="field">
            <label>Presales Engineer</label>
            <label>{sow.author_name}</label>
          </div>
          <div className="field">
            <label>Created</label>
            <label>
              {getString(
                fromISOStringToFormattedDate(sow.created_on, "MM/DD/YYYY")
              )}
            </label>
          </div>
          <div className="field">
            <label>Updated</label>
            <label>
              {getString(
                fromISOStringToFormattedDate(sow.updated_on, "MM/DD/YYYY")
              )}
            </label>
          </div>
          <div className="field">
            <label>Customer</label>
            <label>{sow.customer_name}</label>
          </div>
          <div className="field">
            <label>Type</label>
            <label>{sow.doc_type}</label>
          </div>
        </div>
      )}
      {!project.sow_document && (
        <div>No linked SoW found for this project!</div>
      )}
      <PDFViewer
        show={pdfPreviewData.openPreview}
        onClose={closePreview}
        titleElement={`Meeting PDF Preview`}
        previewHTML={pdfPreviewData.previewHTML}
        footerElement={
          <SquareButton
            content="Close"
            bsStyle={"default"}
            onClick={closePreview}
          />
        }
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToPropsSowDetails = (dispatch: any) => ({
  getSOW: (sowId: number) =>
    dispatch(
      fetchSowDocs({
        pagination: false,
        id: sowId,
      })
    ),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  previewSow: (id: number) => dispatch(downloadSOWDocumnet(id, "pdf")),
});

export default connect(
  mapStateToProps,
  mapDispatchToPropsSowDetails
)(SowDetails);
