import React, { useEffect } from "react";
import SowDetails from "./sowDetails";
import ChangeRequestList from "./changeRequestList";
import { useProject } from "../projectContext";

const Contracts: React.FC = () => {
  const { history, location } = useProject();

  useEffect(() => {
    // Function to update query parameter
    const updateTabQueryParam = () => {
      const searchParams = new URLSearchParams(location.search);
      searchParams.set("tab", "Contracts");
      history.replace({
        pathname: location.pathname,
        search: `?${searchParams.toString()}`,
      });
    };
    updateTabQueryParam();
  }, [history, location.pathname, location.search]);

  return (
    <div className="project-contracts-tab">
      <SowDetails />
      <ChangeRequestList />
    </div>
  );
};

export default Contracts;
