import React from "react";
import { connect } from "react-redux";
import { getActionItemList } from "../../../actions/pmo";
import Spinner from "../../../components/Spinner";
import UsershortInfo from '../../../components/UserImage';
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";
import { cloneDeep } from "lodash";

interface IArchivedListProps extends ICommonProps {
  getActionItemList: any;
  project: any;
  api?: any;
}

interface IArchivedListState {
  itemList: any[];
  loading: boolean;
  sorting: string;
}

class ArchivedLists extends React.Component<
  IArchivedListProps,
  IArchivedListState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IArchivedListProps) {
    super(props);

    this.state = {
      itemList: [],
      loading: false,
      sorting: "",
    };
  }
  componentDidMount() {
    this.setState({ loading: true })
    this.getActionItemList();
  }

  getActionItemList = () => {
    this.props.getActionItemList(this.props.project.id, this.props.api).then(
      action => {
        this.setState({
          loading: false, itemList: action.response
        })
      }
    )
  }
  setSort = (name)=>{
    this.setState({ sorting: this.state.sorting.includes('-') ?  name : `-${name}`  });
  }
   getSortClass =(name)=>{
    return this.state.sorting &&  this.state.sorting.includes(name) &&(this.state.sorting.includes('-')? 'desc-order' : 'asc-order') || '';
   }

  convertHtmlToText = (str: string) => {
    var tempDivElement = document.createElement("div");
    tempDivElement.innerHTML = str;
    return tempDivElement.textContent || tempDivElement.innerText || "";
  };
   
  render() {
    const itemList = this.state.itemList;
    return (
      <div className="project-details-action-items archived-list">
        {
          itemList && itemList.length > 0 && !this.state.loading && (
            <div className="col-md-12 status-row" >

              <label className={`col-md-4 field__label-label`}>
                Item Name
              </label>
              <label className={`col-md-3 field__label-label ${this.getSortClass('item_type')}`}  onClick={(e) =>this.setSort('item_type')}>
                Type
              </label>
              <label className={`col-md-3 field__label-label`}>
                Owner
              </label>
              <label className={`col-md-2 field__label-label`}>
                Completed On
              </label>

            </div>)}
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {
          itemList && !this.state.loading &&
          itemList
          .sort((a, b) =>
          commonFunctions.sortList(a, b, "id", this.state.sorting))
          .map((row, index) => {

            return (
              <div className={`col-md-12 status-row`} key={index}>
                {
                  <div
                    className="pd-action-item col-md-4"
                    title={this.convertHtmlToText(
                      row.blob.description ||
                        row.blob.name ||
                        row.blob.meeting.name
                    )}
                  >
                    {this.convertHtmlToText(
                      row.blob.description ||
                        row.blob.name ||
                        row.blob.meeting.name
                    )}
                  </div>
                }

                {
                  <div className="description col-md-3 user">{row.item_type}</div>
                }

                <>
                  {
                    row.item_type !== "Meeting" &&
                    <UsershortInfo
                      name={row.blob.owner_name || this.props.project.project_manager ||'N.A.'}
                      url={row.blob.owner_name? row.blob.owner_profile_pic : this.props.project.project_manager }
                      className="inside-box col-md-3"
                    />
                  }
                  {
                    row.item_type === "Meeting" &&
                    <UsershortInfo
                      name={this.props.project.project_manager || 'N.A.'}
                      url={this.props.project.project_manager_profile_pic}
                      className="inside-box col-md-3"
                    />
                  }
                </>

                {
                  row.item_type !== "Meeting" &&
                  <div className="row-date col-md-2">
                    {row.blob.updated_on ? fromISOStringToFormattedDate(row.blob.updated_on, 'MMM DD, YYYY') : 'N.A.'}
                  </div>
                }
                {
                  row.item_type === "Meeting" &&
                  <div className="row-date col-md-2">
                    {row.blob.meeting && row.blob.meeting.conducted_on ? fromISOStringToFormattedDate(row.blob.meeting.conducted_on, 'MMM DD, YYYY') : 'N.A.'}
                  </div>
                }
                {
                  row.item_type === "Meeting" &&

                  <div className="archived">
                    <img
                      className={'d-pointer icon-remove info-png'}
                      alt=""
                      title={'Archived Meeting'}
                      src={'/assets/icons/archive-filled-box.svg'}
                      onClick={() => {
                        const query = new URLSearchParams(this.props.location.search);
                        const AllProject = query.get('AllProject') === 'true' ? true : false;
                        const meetingObj = cloneDeep(row.blob);
                        localStorage.setItem('meeting', JSON.stringify(meetingObj));
                        this.props.history.push(
                          `/Project/${this.props.project.id}/meeting/${meetingObj.meeting.meeting_id || meetingObj.meeting.id}?type=${meetingObj.meeting.meeting_type}&view=true&AllProject=${AllProject}&completed=true`
                        );
                      }}
                    />
                  </div>
                }
              </div>
            )
          }

          )
        }
        {
          itemList && itemList.length === 0 && !this.state.loading &&
          <div className="col-md-12 status-row " >

            <div
              className="no-data-action-item col-md-11"
            > No items available</div>
          </div>
        }


      </div >
    );
  }
}


const mapDispatchToProps = (dispatch: any) => ({
  getActionItemList: (projectId: any, api: any) => dispatch(getActionItemList(projectId, api)),
});

export default connect(null, mapDispatchToProps)(ArchivedLists);

