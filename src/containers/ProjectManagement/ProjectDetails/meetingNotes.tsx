import React, { useEffect, useState } from "react";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import { commonFunctions } from "../../../utils/commonFunctions";
import { QuillEditorAcela } from "../../../components/QuillEditor/QuillEditor";
import "./style.scss";

interface MeetingNotesProps {
  note: string;
  saved: boolean;
  callbackMeetingNote: (html: string, autoSave: boolean) => void;
}

const MeetingNotes: React.FC<MeetingNotesProps> = (props) => {
  const [note, setNote] = useState<string>("");
  const [show, setShow] = useState<boolean>(false);

  useEffect(() => {
    setNote(props.note);
  }, [props.note]);

  const handleChangeNote = (html: string) => {
    setNote(html);
  };

  const closeModal = () => {
    setShow(false);
    setNote(props.note); // reset the notes if closed
  };

  const saveNote = () => {
    props.callbackMeetingNote(note, true);
    closeModal();
  };

  return (
    <>
      <div className="meeting-notes">
        <div
          className={`icon-note-show`}
          onClick={() => {
            setShow(true);
          }}
        >
          Show Note
        </div>
      </div>
      {props.saved && <div className="saved-note-show">Note Saved Successfully</div>}
      <ModalBase
        show={show}
        onClose={closeModal}
        titleElement={"Meeting Notes"}
        bodyElement={
          <div className="meeting-note-input">
            <QuillEditorAcela
              onChange={handleChangeNote}
              label={"Note"}
              value={note}
              scrollingContainer=".modal"
              wrapperClass={"meeting-notes-quill"}
              isRequired={true}
              customToolbar={[
                ["bold", "italic", "underline", "strike"],
                [{ color: [] }, { background: [] }],
                [{ list: "ordered" }, { list: "bullet" }],
              ]}
              hideFullscreen={true}
              hideTable={true}
            />
          </div>
        }
        footerElement={
          <div>
            <SquareButton
              content={`Cancel`}
              bsStyle={"default"}
              onClick={closeModal}
              className="meeting-modal-btn"
            />
            <SquareButton
              content={"Save Changes"}
              bsStyle={"primary"}
              onClick={saveNote}
              className="meeting-modal-btn"
              disabled={commonFunctions.isEditorEmpty(note)}
            />
          </div>
        }
        className={"meeting-notes-modal"}
      />
      ;
    </>
  );
};

export default MeetingNotes;
