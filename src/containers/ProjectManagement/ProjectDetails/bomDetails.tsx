import { cloneDeep, flatMap, isEmpty } from "lodash";
import React from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)
import {
  getDataCommon,
  GET_DATA_SUCCESS,
} from "../../../actions/orderTracking";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import Input from "../../../components/Input/input";
import PMOCollapsible from "../../../components/PMOCollapsible";
import RightMenu from "../../../components/RighMenuBase/rightMenuBase";
import Spinner from "../../../components/Spinner";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import { exportCSVFile } from "../../../utils/download";
import { searchInFields } from "../../../utils/searchListUtils";
import "./bomDetails.scss";

interface BOMDetailsProps {
  getDataCommon: any;
  id: any;
  show: boolean;
  onClose?: (e: any) => void;
}

interface IBOMDetailstate {
  searchKey: string;
  salesOrderData: any[];
  selectedRows: any[];
  selectAllRows: boolean;
  lineItems: any[];
  hide_received: boolean;
  hide_zero_cost: boolean;
  ticket_id: number;
  ticket_note: string;
  loadingNote: boolean;
  loading: boolean;
  viewAllNote: boolean;
  groupBy: string;
  showGroupBy: boolean;
}

class BOMDetails extends React.Component<BOMDetailsProps, IBOMDetailstate> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: BOMDetailsProps) {
    super(props);

    this.state = {
      searchKey: "",
      salesOrderData: [],
      selectAllRows: false,
      hide_received: false,
      selectedRows: [],
      lineItems: [],
      hide_zero_cost: false,
      ticket_id: null,
      ticket_note: "",
      loadingNote: false,
      loading: false,
      viewAllNote: false,
      showGroupBy: false,
      groupBy: "service_ticket_id",
    };
  }
  componentDidMount(): void {
    const id = this.props.id;
    this.getProjectOrderTrackingData(id);
  }
  handleChange = (event: any) => {
    const newState = cloneDeep(this.state);
    newState[event.target.name] = event.target.value;

    this.setState(newState);
  };
  onClose = (event: any) => {
    this.setState({ searchKey: "" });
    this.props.onClose(event);
  };

  getProjectOrderTrackingData = (id) => {
    this.setState({ loading: true });
    this.props
      .getDataCommon(`providers/pmo/projects/${id}/order-tracking`)
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          const salesOrderData = action.response.flatMap(
            (x) => x.sales_order_data
          );
          this.setState({ salesOrderData, loading: true }, () => {
            this.getAllData();
          });
        } else {
          this.setState({ loading: false });
        }
      });
  };

  getAllData = async () => {
    this.setState({ loading: true });
    const salesOrderData = this.state.salesOrderData;
    const newState = cloneDeep(this.state);
    const data = salesOrderData.map((board) =>
      this.props.getDataCommon(
        `providers/purchase-order-tickets/${board.service_ticket_id}/notes?all_notes=${this.state.viewAllNote}`
      )
    );
    const infos: any = await Promise.all(data);
    salesOrderData.forEach((board, index) => {
      const notes = infos[index].response;
      if (Array.isArray(notes)) {
        board.line_items_data.map((x) => {
          x.note = notes;
          x.service_ticket_id = board.service_ticket_id;
          return x;
        });
        board.noteData = notes;
      } else {
        board.line_items_data.map((x) => {
          x.note = [notes];
          x.service_ticket_id = board.service_ticket_id;
          return x;
        });
        board.noteData = [notes];
      }
    });
    (newState.salesOrderData as any[]) = salesOrderData;
    const lineItems = flatMap(salesOrderData, (x) => {
      return x.line_items_data;
    });
    (newState.lineItems as any[]) = lineItems;
    (newState.loading as boolean) = false;
    this.setState(newState);
  };

  handleChangeTableSN = (event: any, data?: any, index?: number) => {
    const salesOrderData = this.state.salesOrderData;
    salesOrderData[index].serial_numbers = event;
    this.setState({
      salesOrderData,
    });
  };

  handleChangeTableTrackingNo = (event: any, data?: any, index?: number) => {
    const salesOrderData = this.state.salesOrderData;
    salesOrderData[index].tracking_number = event.join(",");
    this.setState({
      salesOrderData,
    });
  };
  getTitle = () => {
    return <div className="rule-details">BOM Details</div>;
  };
  renderPurchaseOrders = (salesOrderData, id) => {
    const searchString = this.state.searchKey;
    let filteredData = salesOrderData;
    if (searchString.length > 0) {
      filteredData = filteredData.filter((row) =>
        searchInFields(row, searchString, [
          "description",
          "po_number",
          "product_id",
          "note.text",
        ])
      );
    }

    if (this.state.hide_received) {
      filteredData = filteredData.filter(
        (item) => item.received_status !== "FullyReceived"
      );
    }
    if (this.state.hide_zero_cost) {
      filteredData = filteredData.filter((item) => item.unit_cost !== 0);
    }
    return (
      filteredData &&
      filteredData.map((data, index) => (
        <div
          key={data.id}
          className={`product-details-container ${data.selected && "selected"}`}
        >
          <>
            <div className="product-detail top-secton">
              <div className="product-id">
                <div
                  className="detail-title"
                  title={`Cost : ${data.unit_cost}, ${data.received_status}`}
                >
                  Product ID
                </div>
                <div className="product-data">{data.product_id || "-"}</div>
              </div>
              <div>
                <div className="detail-title">Description</div>
                <div className="product-data po-description">
                  {data.description}
                </div>
              </div>
              <div>
                <div className="detail-title">Purchased Qty</div>
                <div className="quantity-pending product-data purchased-qty">
                  {data.ordered_quantity ? data.ordered_quantity : "-"}
                </div>
              </div>
              <div className="recieved-qty-actions">
                <div className="detail-title">Received Qty</div>
                <div className="quantity-p quantity-shipped product-data shipped-qty">
                  {data.received_quantity ? data.received_quantity : "-"}
                </div>
              </div>
              <div className="recieved-qty-actions">
                <div className="detail-title">Estimated Delivery Date</div>
                <div className="quantity-p quantity-shipped product-data shipped-qty">
                  {data.ship_date
                    ? fromISOStringToFormattedDate(data.ship_date)
                    : "-"}
                </div>
              </div>
            </div>
            <div className="product-detail middle-section">
              <div className="tag-input">
                <div className="detail-title">Tracking Number</div>
                {/* <TagsInput
                  value={this.getTrackingNumbersFormatted(
                    data.tracking_numbers
                  )}
                  onChange={(e) =>
                    this.handleChangeTableTrackingNo(e, data, index)
                  }
                  inputProps={{
                    className: "react-tagsinput-input",
                    placeholder: "",
                  }}
                  addOnBlur={true}
                  disabled={true}
                /> */}
              </div>
              <div className="tag-input">
                <div className="detail-title">Serial Number</div>
                {/* <TagsInput
                  value={data.serial_numbers || []}
                  onChange={(e) => this.handleChangeTableSN(e, data, index)}
                  inputProps={{
                    className: "react-tagsinput-input",
                    placeholder: "",
                  }}
                  addOnBlur={true}
                  disabled={true}
                /> */}
              </div>
            </div>
          </>
        </div>
      ))
    );
  };
  getTrackingNumbersFormatted = (data) => {
    let result = [];
    if (Array.isArray(data)) {
      result = data;
    } else {
      result = data && data.length > 0 && data !== "" ? data.split(",") : [];
    }

    return result;
  };
  renderNotes = (salesOrderData, ticketId) => {
    return (
      salesOrderData &&
      salesOrderData.map(
        (data, index) =>
          data.noteData &&
          data.noteData
            .filter(
              (x) => x && x.ticketId && x.ticketId.toString() === ticketId
            )
            .map((note, index) => (
              <div key={data.id} className={`product-details-container`}>
                <div className="note-bom" title={note.ticketId}>
                  Note : {note && note.text}
                </div>
              </div>
            ))
      )
    );
  };
  handleCheckBoxBOM = (e: any) => {
    const newstate = this.state;
    newstate[e.target.name] = e.target.checked;
    this.setState(newstate);
  };

  handleChangeViewAllNote = (e: any) => {
    const newstate = this.state;
    (newstate.viewAllNote as boolean) = !this.state.viewAllNote;
    this.setState(newstate, () => {
      this.getAllData();
    });
  };

  renderHeader = () => {
    return (
      <div className="bom-details-body">
        <div className="left-header col-md-7">
          <Input
            field={{
              label: "",
              type: "SEARCH",
              value: this.state.searchKey,
              isRequired: false,
            }}
            width={7}
            placeholder="Search"
            name="searchKey"
            className="purchaseOrder-select"
            onChange={(e) => this.handleChange(e)}
            multi={false}
          />
          <div className="bom-checkboxes col-md-4">
            <Checkbox
              isChecked={this.state.hide_zero_cost}
              name="hide_zero_cost"
              onChange={(e) => this.handleCheckBoxBOM(e)}
              className="exclude-services-checkbox"
            >
              Hide zero cost items
            </Checkbox>
            <Checkbox
              isChecked={this.state.hide_received}
              name="hide_received"
              onChange={(e) => this.handleCheckBoxBOM(e)}
              className="exclude-services-checkbox"
            >
              Hide received
            </Checkbox>
          </div>
        </div>
        <div className="action-buttons-bom col-md-5">
          <SquareButton
            content={`${
              this.state.viewAllNote ? "Hide" : "View"
            }  Shipping Note`}
            bsStyle={"primary"}
            onClick={(e) => this.handleChangeViewAllNote(e)}
            disabled={this.state.lineItems.length === 0}
            className="bom-btn"
          />
          <SquareButton
            content={`Export Tracking Data`}
            bsStyle={"primary"}
            onClick={(e) => this.download()}
            disabled={this.state.lineItems.length === 0}
            className="bom-btn"
          />
        </div>
      </div>
    );
  };
  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };
  getBody = () => {
    const groupedByInvalid = this.groupBy(
      this.state.lineItems,
      this.state.groupBy
    );

    return (
      <div className="edit-rule">
        <div className="loader modal-loader">
          <Spinner show={this.state.loading || this.state.loadingNote} />
        </div>
        <div className={`${this.state.loading ? `loading` : ""}`}>
          {this.renderHeader()}
          <div className="bom-data-section">
            <div className="purchase-order-list-bom">
              {this.state.groupBy &&
                groupedByInvalid &&
                !isEmpty(groupedByInvalid) &&
                Object.keys(groupedByInvalid).map((data, i) => (
                  <div key={i} className="voilation-statistics col-md-12">
                    <PMOCollapsible
                      background={""}
                      label={`PO Number - ${groupedByInvalid[data][0].po_number}`}
                      isOpen={true}
                    >
                      {this.renderNotes(this.state.salesOrderData, data)}
                      {this.renderPurchaseOrders(groupedByInvalid[data], 0)}
                    </PMOCollapsible>
                  </div>
                ))}
            </div>
            {this.state.lineItems.length === 0 && !this.state.loading && (
              <div className="no-data">Not Available</div>
            )}
          </div>
        </div>
      </div>
    );
  };

  download = () => {
    let headers = {
      product_id: "Product Id",
      description: "Description",
      ordered_quantity: "Purchased Qty",
      received_quantity: "Received Qty",
      tracking_numbers: "Tracking Numbers",
      serial_numbers: "Serial Numbers",
    };
    let itemsFormatted = [];
    let itemsNotFormatted = this.state.lineItems;
    itemsNotFormatted.forEach((item) => {
      itemsFormatted.push({
        product_id: item.product_id ? `"${item.product_id}"` : "N.A.",
        description: item.description ? `"${item.description}"` : "N.A.",
        ordered_quantity: item.product_id
          ? `"${item.ordered_quantity}"`
          : "N.A.",
        received_quantity: item.product_id
          ? `"${item.received_quantity}"`
          : "N.A.",
        tracking_numbers: item.product_id
          ? `"${item.tracking_numbers.join(", ")}"`
          : "N.A.",
        serial_numbers: item.product_id
          ? `"${item.serial_numbers.join(", ")}"`
          : "N.A.",
      });
    });

    var fileTitle = "BOM Details";
    exportCSVFile(headers, itemsFormatted, fileTitle);
  };

  getFooter = () => {
    return "";
  };

  render() {
    return (
      <RightMenu
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="bom-details"
      />
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getDataCommon: (url: any) => dispatch(getDataCommon(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BOMDetails);
