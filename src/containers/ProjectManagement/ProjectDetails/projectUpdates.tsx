import React, { useState } from "react";
import ReactQuill from "react-quill";
import SquareButton from "../../../components/Button/button";
import Input from "../../../components/Input/input";
import { commonFunctions } from "../../../utils/commonFunctions";

interface IProjectUpdatesProps {
  projectId: number;
  projectUpdates: IProjectUpdates;
  onSave: (projectUpdates: IProjectUpdates) => void;
}

const ProjectUpdates: React.FC<IProjectUpdatesProps> = (props) => {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [updates, setUpdates] = useState<string>("");
  const [note, setNote] = useState<string>("");
  const [etc, setEtc] = useState<number>(null);

  const validHours = (hours: number) => {
    return (
      hours === null || (Number.isInteger(hours) && hours >= 0 && hours <= 999)
    );
  };

  const handleChange = (e) => {
    setEtc(Number(e.target.value));
  };

  const handleChangeEditor = (key: "updates" | "note", value: string) => {
    key === "updates" ? setUpdates(value) : setNote(value);
  };

  const toggleEdit = (edit: boolean) => {
    setUpdates(props.projectUpdates.updates);
    setNote(props.projectUpdates.note);
    setEtc(props.projectUpdates.estimated_hours_to_complete);
    setEditMode(edit);
  };

  return (
    <div className="project-updates-container">
      <div className="project-updates-header">
        <label className="col-xs-4">Updates</label>
        <label className="col-xs-4">Note</label>
        <label className="col-xs-3">Estimated Hours to Complete</label>
      </div>
      {editMode ? (
        <>
          <div className="pr-up-edit-mode">
            <ReactQuill
              value={updates}
              onChange={(html: string) => handleChangeEditor("updates", html)}
              modules={{
                toolbar: [[{ list: "ordered" }, { list: "bullet" }]],
              }}
              theme="snow"
              className={`project-updates-quill col-md-4`}
            />
            <ReactQuill
              value={note}
              onChange={(html: string) => handleChangeEditor("note", html)}
              modules={{
                toolbar: [[{ list: "ordered" }, { list: "bullet" }]],
              }}
              theme="snow"
              className={`project-updates-quill col-md-4`}
            />
            <Input
              field={{
                value: etc,
                label: "",
                type: "NUMBER",
                isRequired: false,
              }}
              width={3}
              name={"etc"}
              placeholder="Enter Estimated Hours"
              onChange={(e) => handleChange(e)}
              className="project-updates-input"
              minimumValue="0"
              maximumValue="999"
              error={
                !validHours(etc)
                  ? {
                      errorState: "error",
                      errorMessage:
                        "Please enter a valid integer between 0 to 999",
                    }
                  : undefined
              }
            />
          </div>
          <div className="pr-up-actions">
            <SquareButton
              onClick={() => toggleEdit(false)}
              content={"Cancel"}
              bsStyle={"default"}
            />
            <SquareButton
              onClick={() => {
                props.onSave({
                  updates,
                  note,
                  estimated_hours_to_complete: etc,
                });
                setEditMode(false);
              }}
              content={"Apply"}
              bsStyle={"primary"}
              disabled={!validHours(etc)}
            />
          </div>
        </>
      ) : (
        <div className="pr-up-display-mode">
          <img
            className={"pr-up-edit"}
            alt="Edit Project Updates"
            src={"/assets/icons/edit.png"}
            title={"Edit Project Updates"}
            onClick={() => toggleEdit(true)}
          />
          {commonFunctions.isEditorEmpty(props.projectUpdates.updates) ? (
            <div className="pr-up-no-data col-md-4">No data</div>
          ) : (
            <div
              className="project-updates-html ql-editor col-md-4"
              dangerouslySetInnerHTML={{
                __html: props.projectUpdates.updates,
              }}
            />
          )}
          {commonFunctions.isEditorEmpty(props.projectUpdates.note) ? (
            <div className="pr-up-no-data col-md-4">No data</div>
          ) : (
            <div
              className="project-updates-html ql-editor col-md-4"
              dangerouslySetInnerHTML={{
                __html: props.projectUpdates.note,
              }}
            />
          )}
          {props.projectUpdates.estimated_hours_to_complete === null ? (
            <div className="pr-up-no-data col-md-3" style={{ width: "190px" }}>
              No data
            </div>
          ) : (
            <div className="project-updates-etc col-md-3">
              {props.projectUpdates.estimated_hours_to_complete}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default ProjectUpdates;
