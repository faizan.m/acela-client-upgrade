import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  fetchProjectSetupInfo,
  PROJECT_SETUP_SUCCESS,
} from "../../../actions/pmo";
import HorizontalTabSlider, {
  Tab,
} from "../../../components/HorizontalTabSlider/HorizontalTabSlider";
import TeamTab from "./Team";
import SetupTab from "./Setup";
import DetailsTab from "./Details";
import MeetingList from "./Meeting/meetingList";
import { useProject } from "./projectContext";
import Dashboard from "./Dashboard";
import PMOTooltip from "../../../components/PMOTooltip/pmoTooltip";
import { getBackgroundOverdue } from "../../../utils/CommonUtils";
import Contracts from "./Contract";
import ProjectHistory from "./History";
import BOMDetails from "./BOM/bomDetails";

enum ProjectPageType {
  DASHBOARD = "Dashboard",
  PLAN = "Plan",
  DETAILS = "Details",
  MEETINGS = "Meetings",
  TEAM = "Team",
  CONTRACTS = "Contracts",
  HARDWARE = "Hardware",
  HISTORY = "History",
  SETUP = "Setup",
}

interface ProjectDetailsProps {
  fetchSetupInfo: (projectID: number) => Promise<any>;
}

const ProjectDetailIcons = () => {
  const { project, projectID, history, location } = useProject();

  const redirectToOldPage = () => {
    const query = new URLSearchParams(location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    history.push(`/ProjectManagement/${projectID}?AllProject=${AllProject}`);
  };

  return (
    <div className="project-details-icons">
      <div
        style={{ backgroundColor: project.time_status_color }}
        className={`rectangle-icon-outer`}
      >
        <PMOTooltip
          iconClass="status-svg-images-black"
          icon={`/assets/icons/clock-circular-gross-outline-of-time.svg`}
        >
          <div
            style={{ borderColor: project.time_status_color }}
            className="data-box"
          >
            <div className="text">Time Remaining</div>
            <div
              style={{
                color: project.time_status_color,
                fontWeight: 600,
              }}
              className="text"
            >
              {project.time_status_value} %
            </div>
          </div>
        </PMOTooltip>
      </div>

      <div
        style={{ backgroundColor: project.date_status_color }}
        className={`rectangle-icon-outer`}
      >
        <PMOTooltip
          iconClass="status-svg-images-black"
          icon={`/assets/icons/calendar-silhouette.svg`}
        >
          <div
            style={{ borderColor: project.date_status_color }}
            className="data-box"
          >
            <div className="text">Close Date</div>
            <div
              style={{
                color: project.date_status_color,
                fontWeight: 600,
              }}
              className="text"
            >
              {project.date_status_value > 0
                ? `Due date in ${project.date_status_value} days`
                : "Due date passed"}
            </div>
          </div>
        </PMOTooltip>
      </div>
      <div
        style={{ backgroundColor: getBackgroundOverdue(project) }}
        className={`rectangle-icon-outer`}
      >
        <PMOTooltip
          iconClass="status-svg-images-black"
          icon={`/assets/icons/pulse.svg`}
          placement="bottom"
        >
          {(project.overdue_critical_path_items ||
            0 + project.overdue_action_items ||
            0) !== 0 && (
            <div
              style={{
                borderColor:
                  (project.overdue_critical_path_items ||
                    0 + project.overdue_action_items ||
                    0) > 0
                    ? "#db1643"
                    : "#176f07",
              }}
              className="data-box"
            >
              <div className="text">Overdue Actions</div>
              <div className="text">
                <span
                  style={{
                    color:
                      project.overdue_critical_path_items || 0 > 0
                        ? "#db1643"
                        : "#176f07",
                    fontWeight: 600,
                  }}
                >
                  {project.overdue_critical_path_items || 0}
                </span>
                {" Critical Items | "}
                <span
                  style={{
                    color:
                      project.overdue_action_items || 0 > 0
                        ? "#db1643"
                        : "#176f07",
                    fontWeight: 600,
                  }}
                >
                  {project.overdue_action_items || 0}
                </span>
                {" Action"}
              </div>
            </div>
          )}
          {(project.expiring_critical_path_items ||
            0 + project.expiring_action_items ||
            0) !== 0 && (
            <div
              style={{
                borderColor:
                  (project.expiring_critical_path_items ||
                    0 + project.expiring_action_items ||
                    0) > 0
                    ? "#dba629"
                    : "#176f07",
              }}
              className="data-box"
            >
              <div className="text">Pending Actions</div>
              <div className="text">
                <span
                  style={{
                    color:
                      project.expiring_critical_path_items || 0 > 0
                        ? "#dba629"
                        : "#176f07",
                    fontWeight: 600,
                  }}
                >
                  {project.expiring_critical_path_items || 0}
                </span>
                {" Critical Items | "}
                <span
                  style={{
                    color:
                      project.expiring_action_items || 0 > 0
                        ? "#dba629"
                        : "#176f07",
                    fontWeight: 600,
                  }}
                >
                  {project.expiring_action_items || 0}
                </span>
                {" Action"}
              </div>
            </div>
          )}
          {(project.expiring_critical_path_items ||
            0 + project.expiring_action_items ||
            0) === 0 &&
            (project.overdue_critical_path_items ||
              0 + project.overdue_action_items ||
              0) === 0 && (
              <div
                style={{
                  borderColor:
                    (project.expiring_critical_path_items ||
                      0 + project.expiring_action_items ||
                      0) > 0
                      ? "#dba629"
                      : "#176f07",
                }}
                className="data-box"
              >
                <div className="text">No Overdue & Pending Actions</div>
              </div>
            )}
        </PMOTooltip>
      </div>
      <img
        src="/assets/icons/redirect.png"
        alt="Redirect"
        title="Go to Old Project Details"
        className="project-redirect"
        onClick={redirectToOldPage}
      />
      <img
        src="/assets/icons/arrow-point-to-left.svg"
        alt="Left Arrow"
        title="Go Back"
        className="go-back"
        onClick={history.goBack}
      />
    </div>
  );
};

const ProjectDetailsMain: React.FC<ProjectDetailsProps> = (props) => {
  const { project, projectID, location, loadingProject } = useProject();
  const [isSetupLeft, setIsSetupLeft] = useState<boolean>(false);
  const [defaultTab, setDefaultTab] = useState<ProjectPageType>(
    ProjectPageType.DASHBOARD
  );

  useEffect(() => {
    checkIfProjectIsSetup();
  }, []);

  const checkIfProjectIsSetup = (checkingAfterSetup: boolean = false) => {
    props.fetchSetupInfo(projectID).then((action) => {
      if (action.type === PROJECT_SETUP_SUCCESS) {
        const res = action.response;
        const setupLeft: boolean =
          !res.project_type_configured || !res.time_entry_tickets_configured;
        if (setupLeft) {
          setIsSetupLeft(true);
          setDefaultTab(ProjectPageType.SETUP);
        } else {
          setIsSetupLeft(false);
          if (checkingAfterSetup) setDefaultTab(ProjectPageType.DASHBOARD);
        }
      }
    });
  };

  useEffect(() => {
    const query = new URLSearchParams(location.search);
    const tab = query.get("tab") as ProjectPageType;
    if (tab) setDefaultTab(tab);
  }, [location.search]);

  return (
    <div className="project-details-main">
      {loadingProject && <div className="project-loading">Loading...</div>}
      {project && (
        <>
          <h2 className="project-details-title">
            <span>
              Project Details /{" "}
              {`${project.customer} - ${project.title} - ${project.crm_id}`}
            </span>
            <ProjectDetailIcons />
          </h2>
          <HorizontalTabSlider
            defaultTab={defaultTab}
            className="project-details-nav"
          >
            <Tab title={ProjectPageType.DASHBOARD} hide={isSetupLeft}>
              <Dashboard />
            </Tab>
            <Tab title={ProjectPageType.DETAILS} hide={isSetupLeft}>
              <DetailsTab />
            </Tab>
            <Tab title={ProjectPageType.MEETINGS} hide={isSetupLeft}>
              <MeetingList />
            </Tab>
            <Tab title={ProjectPageType.TEAM} hide={isSetupLeft}>
              <TeamTab />
            </Tab>
            <Tab title={ProjectPageType.CONTRACTS} hide={isSetupLeft}>
              <Contracts />
            </Tab>
            <Tab title={ProjectPageType.HARDWARE} hide={isSetupLeft}>
              <BOMDetails />
            </Tab>
            <Tab title={ProjectPageType.HISTORY} hide={isSetupLeft}>
              <ProjectHistory />
            </Tab>
            <Tab title={ProjectPageType.SETUP}>
              <SetupTab
                onConfiguringSetup={() => checkIfProjectIsSetup(true)}
              />
            </Tab>
          </HorizontalTabSlider>
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchSetupInfo: (projectID: number) =>
    dispatch(fetchProjectSetupInfo(projectID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailsMain);
