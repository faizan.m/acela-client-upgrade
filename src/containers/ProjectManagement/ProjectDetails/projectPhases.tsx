import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import { getActivityStatusList, getProjectPhasesByID, getProjectStatusByProjectID, PROJECT_PHASES_SUCCESS, updateProjectDetails, updateProjectPhasesStatusByID } from '../../../actions/pmo';
import Input from '../../../components/Input/input';
import SMPopUp from '../../../components/SMPopup/smPopUp';
import { fromISOStringToFormattedDateCW } from '../../../utils/CalendarUtil';
import './style.scss';

interface IProjectPhasesProps {
    getProjectPhasesByID: any;
    updateProjectPhasesStatusByID: any;
    project?: any;
    projectTypeID?: number;
    getProjectStatusByProjectID: any;
    getActivityStatusList: any;
    updateProjectDetails: any;
}

interface IProjectPhasesState {
    list: any;
    phases: any;
    statusList: any[];
    loading: boolean;
    status: string;
    estimated_completion_date: string;
}
class ProjectPhases extends React.Component<
    IProjectPhasesProps,
    IProjectPhasesState
> {
    constructor(props: IProjectPhasesProps) {
        super(props);
        this.state = this.getEmptyState();
    }

    getEmptyState = () => ({
        list: [],
        phases: [],
        loading: false,
        statusList: [],
        status: '',
        estimated_completion_date: '',
    });


    componentDidUpdate(prevProps) {
        if (
            (this.props.project.id && this.props.project.id !== prevProps.project.id)
            ||
            (this.props.projectTypeID && this.props.projectTypeID !== prevProps.projectTypeID)
        ) {
            this.setState({ loading: true })
            this.getProjectPhasesByID(this.props.project.id, true);
            this.getProjectStatusByProjectID();
        }

    }

    getProjectPhasesByID = (id, pushDate) => {
        this.setState({ loading: true })
        this.props.getProjectPhasesByID(id).then(
            action => {
                if (action.type === PROJECT_PHASES_SUCCESS) {
                    const phasesList = action.response;
                    if (phasesList && phasesList.length > 1 && pushDate) {
                        phasesList[phasesList.length - 1].estimated_completion_date = this.props.project.estimated_end_date;
                    }

                    this.setState({ phases: phasesList });
                }
                this.setState({ loading: false })
            }
        );
    }
    getProjectStatusByProjectID = () => {
        this.props.getActivityStatusList('pmo/settings/milestone-status').then(
            action => {
                this.setState({ loading: false, statusList: action.response })
            }
        )
    }

    handleChange = (event: any) => {
        const newState: IProjectPhasesState = cloneDeep(this.state);

        if (event.target.name === "estimated_completion_date") {
            (newState[event.target.name] as string) = event.target.value;
        } else {
            (newState[event.target.name] as string) = event.target.value;
        }

        this.setState(newState);
    };
    onSaveStatus = (event: any, phase, index) => {
        this.setState({ loading: true })
        const newState = cloneDeep(this.state);
        this.props.updateProjectPhasesStatusByID(
            { status: this.state.status, estimated_completion_date: this.state.estimated_completion_date },
            this.props.project.id,
            phase.id,
        ).then(
            action => {
                if (action.type === PROJECT_PHASES_SUCCESS) {
                    this.getProjectPhasesByID(this.props.project.id, false);
                }
                this.setState({ loading: false })
                const totalPhases = this.state.phases.length;
                if (totalPhases === (index + 1)) {
                    this.updateProject()
                }
            }
        );

        this.setState(newState);
    };

    onPhaseClick = (phase) => {
        this.setState({
            status: phase.status && phase.status.id,
            estimated_completion_date: phase.estimated_completion_date
        });
    }
    updateProject = () => {
        this.setState({ loading: true });
        this.props.updateProjectDetails(
            {
                estimated_end_date: this.state.estimated_completion_date,
            },
            this.props.project.id
        ).then(
            action => {
                this.getProjectPhasesByID(this.props.project.id, false);
                this.setState({ loading: false })
            }
        );
    }
    render() {

        return (
            <div className="status-actions-row">

                {
                    this.state.phases && this.state.phases.map((phase, index) => (
                        <SMPopUp
                            key={index}
                            onClickOk={e => this.onSaveStatus(e, phase, index)}
                            isValidForm={this.state.status && this.state.estimated_completion_date}
                            clickableSection={
                                <div className="action"
                                    onClick={() => this.onPhaseClick(phase)}>
                                    <div className="status-name">{phase.phase.title}</div>
                                    <div className="status-box" style={{ backgroundColor: phase.status && phase.status.color }}>
                                        <div className="name">{phase.status && phase.status.title}</div>
                                        <div className="date">
                                            {phase.estimated_completion_date ? fromISOStringToFormattedDateCW(phase.estimated_completion_date, 'MMM DD, YYYY') : 'TBD'}
                                        </div>
                                    </div>
                                </div>
                            }>
                            <div className="box">
                                <Input
                                    field={{
                                        label: 'Status',
                                        type: "PICKLIST",
                                        value: this.state.status,
                                        options: this.state.statusList && this.state.statusList.map(s => ({
                                            value: s.id,
                                            label: `${s.title}`,
                                        })),
                                        isRequired: false,
                                    }}
                                    width={12}
                                    multi={false}
                                    name="status"
                                    onChange={e => this.handleChange(e)}
                                    placeholder={`Select`}
                                />
                                <Input
                                    field={{
                                        value: this.state.estimated_completion_date && new Date(this.state.estimated_completion_date).toISOString(),
                                        label: 'Estimated date of completion',
                                        type: "DATE",
                                        isRequired: false,
                                    }}
                                    width={12}
                                    name="estimated_completion_date"
                                    onChange={e => this.handleChange(e)}
                                    placeholder="Select Date"
                                    showTime={false}
                                    showDate={true}
                                    disablePrevioueDates={false}
                                    disableTimezone={true}
                                />
                            </div>
                        </SMPopUp>

                    ))
                }
                {
                    !this.state.loading && this.state.phases.length === 0 && (
                        <div className="no-data col-md-12">
                            No Phases available
                        </div>
                    )
                }
            </div>
        );
    }
}

const mapStateToProps = (state: IReduxStore) => ({
});

const mapDispatchToProps = (dispatch: any) => ({
    getActivityStatusList: (url: string) => dispatch(getActivityStatusList(url)),
    getProjectPhasesByID: (projectId: number) => dispatch(getProjectPhasesByID(projectId)),
    updateProjectPhasesStatusByID: (data: any, projectId: number, phaseId: number) => dispatch(updateProjectPhasesStatusByID(data, projectId, phaseId)),
    getProjectStatusByProjectID: (projectId: number) => dispatch(getProjectStatusByProjectID(projectId)),
    updateProjectDetails: (data: any, projectID: number) =>
        dispatch(updateProjectDetails(data, projectID)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectPhases);