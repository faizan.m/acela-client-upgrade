import React from "react";
import { connect } from "react-redux";
import {
  deleteProjectDocumentsByID, getProjectDocumentsByID, PROJECT_DOCUMENTS_SUCCESS, saveProjectDocumentsByID
} from "../../../actions/pmo";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import Spinner from '../../../components/Spinner';
import "./style.scss";

interface IProjectDocumentsProps {
  projectID?: number;
  getProjectDocumentsByID: any;
  saveProjectDocumentsByID: any;
  deleteProjectDocumentsByID: any;
}

interface IProjectDocumentsState {
  currentUploadedDocument: any;
  projectDocuments: any;
  loading: boolean;
}

const fileIcons = {
  pdf: "/assets/icons/008-file-7.svg",
  doc: "/assets/icons/005-file-4.svg",
  docx: "/assets/icons/006-file-5.svg",
  zip: "/assets/icons/009-file-8.svg",
  xls: "/assets/icons/047-file-46.svg",
  csv: "/assets/icons/048-file-47.svg",
  svg: "/assets/icons/001-file.svg",
  png: "/assets/icons/003-file-2.svg",
  jpg: "/assets/icons/004-file-3.svg",
  jpeg: "/assets/icons/004-file-3.svg",
  txt: "/assets/icons/034-file-33.svg",
};

class ProjectDocuments extends React.Component<
  IProjectDocumentsProps,
  IProjectDocumentsState
> {
  uploadInput = React.createRef<HTMLInputElement>();

  constructor(props: IProjectDocumentsProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentUploadedDocument: undefined,
    projectDocuments: [],
    loading: false,
  });

  componentDidMount() {
    this.getProjectDocumentsByID(this.props.projectID);
  }

  getProjectDocumentsByID = (id) => {
    this.setState({ loading: true });
    this.props.getProjectDocumentsByID(id).then((action) => {
      if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
        this.setState({
          projectDocuments: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  deleteDocument = (document) => {
    this.setState({ loading: true });
    this.props
      .deleteProjectDocumentsByID(this.props.projectID, document.id)
      .then((action) => {
        if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
          this.getProjectDocumentsByID(this.props.projectID);
        }
      });
  };

  handleFileUpload = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile = files[0];
    this.setState({
      currentUploadedDocument: dataFile,
    });
  };

  uploadDocument = () => {
    this.setState({ loading: true });
    const data = new FormData();
    data.append("file_path", this.state.currentUploadedDocument);
    data.append("file_name", this.state.currentUploadedDocument.name);
    this.props
      .saveProjectDocumentsByID(this.props.projectID, data)
      .then((action) => {
        if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
          this.setState({
            currentUploadedDocument: undefined,
          });
          this.getProjectDocumentsByID(this.props.projectID);
        }
      });
  };

  render() {
    return (
      <div className="document-details-box details-box">
          {this.state.loading && (
            <div className="loader">
                <Spinner show={true} />
            </div>
        )}
        <div className="doucments-list">
          {this.state.projectDocuments.map((document) => (
            <div className="document">
              <div className="doc-right">
                <img
                  className="file-icons"
                  alt=""
                  src={
                    fileIcons[document.file_name.split(".").pop()] ||
                    "/assets/icons/046-file-45.svg"
                  }
                />
                <div className="label-content">{document.file_name}</div>
              </div>
              <SmallConfirmationBox
                className="remove-file"
                onClickOk={() => this.deleteDocument(document)}
                text={"this file"}
              />
            </div>
          ))}
        </div>
        <input
          type="file"
          style={{ display: "none" }}
          ref={this.uploadInput}
          onChange={this.handleFileUpload}
        />
        {this.state.currentUploadedDocument ? (
          <>
            <div className="upload-box">
              <span className="upload-filename">
                {this.state.currentUploadedDocument.name}
              </span>
            </div>
            <div className="file-upload-actions">
              <img
                className="file-action-icons file-upload-save"
                alt=""
                src={"/assets/icons/validated.svg"}
                onClick={() => this.uploadDocument()}
              />
              <img
                className="file-action-icons file-upload-cancel"
                alt=""
                src={"/assets/icons/close.png"}
                onClick={() =>
                  this.setState({ currentUploadedDocument: undefined })
                }
              />
            </div>
          </>
        ) : (
          <div
            className="document-label-details action-item"
            onClick={() => (this.uploadInput as any).current.click()}
          >
            Upload File
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectDocumentsByID: (projectId: number) =>
    dispatch(getProjectDocumentsByID(projectId)),
  saveProjectDocumentsByID: (projectId: number, data: any) =>
    dispatch(saveProjectDocumentsByID(projectId, data)),
  deleteProjectDocumentsByID: (projectId: number, documentId: number) =>
    dispatch(deleteProjectDocumentsByID(projectId, documentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDocuments);
