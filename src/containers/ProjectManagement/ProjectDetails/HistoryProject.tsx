import React, { ReactNode } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import { pmoCommonAPI } from "../../../actions/pmo";
import SquareButton from "../../../components/Button/button";
import Spinner from "../../../components/Spinner";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import { showValue } from "../../ProjectSetting/HistorySetting";
import "./historyStyle.scss";
import "./style.scss";

interface IProjectProps extends ICommonProps {
  pmoCommonAPI: (
    api: string,
    type: string,
    data: any,
    params?: IScrollPaginationFilters
  ) => Promise<any>;
}

interface IProjectState {
  noData: boolean;
  loading: boolean;
  historyList: IProjectHistory[];
  pagination: IScrollPaginationFilters;
}

class HistoryPMO extends React.Component<any, IProjectState> {
  constructor(props: IProjectProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    noData: false,
    loading: true,
    historyList: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
    },
  });

  componentDidMount() {
    this.fetchMoreData(true);
  }

  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
      };
    }
    if (prevParams.nextPage !== null) {
      const id = this.props.match.params.id;

      this.props
        .pmoCommonAPI(`projects/${id}/history`, "get", "", prevParams)
        .then((action) => {
          if (action.response) {
            let historyList = [];
            if (clearData) {
              historyList = [...action.response.results];
            } else {
              historyList = [
                ...this.state.historyList,
                ...action.response.results,
              ];
            }
            const newPagination = {
              currentPage: action.response.links.page_number,
              nextPage: action.response.links.next_page_number,
              page_size: this.state.pagination.page_size,
            };

            this.setState({
              pagination: newPagination,
              historyList,
              noData: historyList.length === 0 ? true : false,
              loading: false,
            });
          } else {
            this.setState({
              noData: true,
              loading: false,
            });
          }
        });
    }
  };

  convertHtmlToText = (str: ReactNode) => {
    var tempDivElement = document.createElement("div");
    tempDivElement.innerHTML = str as string;
    return tempDivElement.textContent || tempDivElement.innerText || "";
  };

  listingRows = (history: IProjectHistory, index: number) => {
    let additionalHeader: string = "";
    let additionalData: IProjectActionDetails[] = [];
    if (
      history.additional_data &&
      history.additional_data.meeting_close_details
    ) {
      additionalHeader = "Meeting Close Details:";
      additionalData = history.additional_data.meeting_close_details;
    } else if (
      history.additional_data &&
      history.additional_data.cr_processing_details
    ) {
      additionalHeader = "CR Processing Details:";
      additionalData = history.additional_data.cr_processing_details;
    }
    return (
      <div className={`history-card`} key={index}>
        <div className="field updated-fields">
          <div className="tags">
            <div className={`action-tag ${history.action}`}>
              {history.action}
            </div>
            <div className={`name-tag ${history.model_display_name}`}>
              {history.model_display_name}
            </div>
            <div className="actor"> {history.actor} </div>
            <div className="date">
              {fromISOStringToFormattedDate(
                history.timestamp,
                "MMM DD, YYYY hh:mm a"
              )}
            </div>
          </div>

          {history &&
            history.changes_display_dict &&
            Object.entries(history.changes_display_dict).map(([key, value]) => {
              if (key.includes("markdown")) {
                return "";
              }
              return (
                <div className="data-row" key={key}>
                  <div className="key"> {key} </div>
                  <div className="old">
                    {this.convertHtmlToText(showValue(value[0], key))}
                  </div>
                  <div className="new">
                    {this.convertHtmlToText(showValue(value[1], key))}
                  </div>
                </div>
              );
            })}
          {additionalHeader && (
            <div className="history-addtnl-data">
              <h4>{additionalHeader}</h4>
              <ol>
                {additionalData.map((el, idx) => (
                  <li
                    key={idx}
                    className={el.errors.length ? "history-err-msg" : undefined}
                  >
                    {el.message}
                  </li>
                ))}
              </ol>
            </div>
          )}
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className="project-history">
        <div className="project-details meeting-details create-change-request">
          {this.state.loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          )}
          <div className="header">
            <div className="left">
              <div className="meeting-name">Project History</div>
            </div>
            <div className="right">
              <SquareButton
                content="Close"
                bsStyle={"default"}
                onClick={() => {
                  const query = new URLSearchParams(this.props.location.search);
                  const AllProject =
                    query.get("AllProject") === "true" ? true : false;
                  this.props.history.push(
                    `/ProjectManagement/${this.props.match.params.id}?AllProject=${AllProject}`
                  );
                }}
              />
            </div>
          </div>
        </div>
        <div className="list">
          <div className={`header columns`}>
            <div className={`field data-row  updated-fields`}>
              <div className="keys">Field Name</div>
              <div className="keys">Old Value</div>
              <div className="keys">New Value</div>
            </div>
          </div>
          <div id="scrollableDiv" style={{ height: "72vh", overflow: "auto" }}>
            <InfiniteScroll
              dataLength={this.state.historyList.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
            >
              {this.state.historyList.map((history, index) =>
                this.listingRows(history, index)
              )}

              {!this.state.loading &&
                this.state.pagination.nextPage &&
                this.state.historyList.length > 0 && (
                  <div
                    className="load-more"
                    onClick={() => {
                      this.fetchMoreData(false);
                    }}
                  >
                    load more data...
                  </div>
                )}
            </InfiniteScroll>
            {this.state.noData && this.state.historyList.length === 0 && (
              <div className="no-data">No History Available</div>
            )}
            {this.state.loading && (
              <div className="loader">
                <Spinner show={true} />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  pmoCommonAPI: (
    api: string,
    type: string,
    data: any,
    params?: IScrollPaginationFilters
  ) => dispatch(pmoCommonAPI(api, type, data, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryPMO);
