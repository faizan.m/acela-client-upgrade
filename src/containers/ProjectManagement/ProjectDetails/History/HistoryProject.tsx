import React, { ReactNode, useEffect, useRef, useState } from "react";
import { debounce } from "lodash";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import { pmoCommonAPI } from "../../../../actions/pmo";
import Spinner from "../../../../components/Spinner";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import { showValue } from "../../../ProjectSetting/HistorySetting";
import { useProject } from "../projectContext";
import Input from "../../../../components/Input/input";
import "./historyStyle.scss";

interface IProjectProps {
  pmoCommonAPI: (
    api: string,
    type: string,
    data: any,
    params?: IScrollPaginationFilters
  ) => Promise<any>;
}

const HistoryPMO: React.FC<IProjectProps> = (props) => {
  const { projectID } = useProject();

  const emptyPagination = {
    currentPage: 0,
    nextPage: 1,
    page_size: 25,
  };
  const [loading, setLoading] = useState<boolean>(true);
  const [noData, setNoData] = useState<boolean>(false);
  const [searchInput, setSearchInput] = useState<string>("");
  const [historyList, setHistoryList] = useState<IProjectHistory[]>([]);
  const fetchMoreDataDebounced = useRef(
    debounce((searchVal) => {
      fetchMoreData(true, searchVal);
    }, 500)
  );
  const [pagination, setPagination] = useState<IScrollPaginationFilters>(
    emptyPagination
  );

  useEffect(() => {
    if (projectID) fetchMoreData(true);
  }, [projectID]);

  const onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const searchValue = e.target.value.toLowerCase();
    setSearchInput(searchValue);
    fetchMoreDataDebounced.current(searchValue);
  };

  const fetchMoreData = (clearData: boolean = false, searchParam?: string) => {
    const search = searchParam ? searchParam : searchInput;
    let prevParams = pagination;
    setLoading(true);
    if (clearData) {
      prevParams = emptyPagination;
    }
    if (prevParams.nextPage !== null) {
      props
        .pmoCommonAPI(
          `projects/${projectID}/history?search=${search}`,
          "get",
          "",
          prevParams
        )
        .then((action) => {
          if (action.response) {
            let historyList = [];
            if (clearData) {
              setHistoryList([...action.response.results]);
            } else {
              setHistoryList([...historyList, ...action.response.results]);
            }
            const newPagination = {
              currentPage: action.response.links.page_number,
              nextPage: action.response.links.next_page_number,
              page_size: pagination.page_size,
            };

            setPagination(newPagination);
            setNoData(historyList.length === 0 ? true : false);
            setLoading(false);
          } else {
            setNoData(true);
            setLoading(false);
          }
        });
    }
  };

  const convertHtmlToText = (str: ReactNode) => {
    var tempDivElement = document.createElement("div");
    tempDivElement.innerHTML = str as string;
    return tempDivElement.textContent || tempDivElement.innerText || "";
  };

  const listingRows = (history: IProjectHistory, index: number) => {
    let additionalHeader: string = "";
    let additionalData: IProjectActionDetails[] = [];
    if (
      history.additional_data &&
      history.additional_data.meeting_close_details
    ) {
      additionalHeader = "Meeting Close Details:";
      additionalData = history.additional_data.meeting_close_details;
    } else if (
      history.additional_data &&
      history.additional_data.cr_processing_details
    ) {
      additionalHeader = "CR Processing Details:";
      additionalData = history.additional_data.cr_processing_details;
    }
    return (
      <div className={`history-card`} key={index}>
        <div className="field updated-fields">
          <div className="tags">
            <div className={`action-tag ${history.action}`}>
              {history.action}
            </div>
            <div className={`name-tag ${history.model_display_name}`}>
              {history.model_display_name}
            </div>
            <div className="actor"> {history.actor} </div>
            <div className="date">
              {fromISOStringToFormattedDate(
                history.timestamp,
                "MMM DD, YYYY hh:mm a"
              )}
            </div>
          </div>

          {history &&
            history.changes_display_dict &&
            Object.entries(history.changes_display_dict).map(([key, value]) => {
              if (key.includes("markdown")) {
                return "";
              }
              return (
                <div className="data-row" key={key}>
                  <div className="key"> {key} </div>
                  <div className="old">
                    {convertHtmlToText(showValue(value[0], key))}
                  </div>
                  <div className="new">
                    {convertHtmlToText(showValue(value[1], key))}
                  </div>
                </div>
              );
            })}
          {additionalHeader && (
            <div className="history-addtnl-data">
              <h4>{additionalHeader}</h4>
              <ol>
                {additionalData.map((el, idx) => (
                  <li
                    key={idx}
                    className={el.errors.length ? "history-err-msg" : undefined}
                  >
                    {el.message}
                  </li>
                ))}
              </ol>
            </div>
          )}
        </div>
      </div>
    );
  };

  return (
    <div className="project-history">
      <Input
        field={{
          value: searchInput,
          label: "",
          type: "SEARCH",
        }}
        width={10}
        name="searchString"
        onChange={onSearchStringChange}
        placeholder="Search"
        className={`logs-tab-search`}
      />
      {loading && (
        <div className="loader">
          <Spinner show={true} />
        </div>
      )}
      <div className="list">
        <div className={`header columns`}>
          <div className={`field data-row  updated-fields`}>
            <div className="keys">Field Name</div>
            <div className="keys">Old Value</div>
            <div className="keys">New Value</div>
          </div>
        </div>
        <div id="scrollableDiv" style={{ height: "72vh", overflow: "auto" }}>
          <InfiniteScroll
            dataLength={historyList.length}
            next={fetchMoreData}
            hasMore={pagination.nextPage ? true : false}
            loader={loading && <h4 className="no-data"> Loading...</h4>}
            scrollableTarget="scrollableDiv"
          >
            {historyList.map((history, index) => listingRows(history, index))}

            {!loading && pagination.nextPage && historyList.length > 0 && (
              <div
                className="load-more"
                onClick={() => {
                  fetchMoreData(false);
                }}
              >
                load more data...
              </div>
            )}
          </InfiniteScroll>
          {noData && historyList.length === 0 && (
            <div className="no-data">No History Available</div>
          )}
          {loading && (
            <div className="loader">
              <Spinner show={true} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  pmoCommonAPI: (
    api: string,
    type: string,
    data: any,
    params?: IScrollPaginationFilters
  ) => dispatch(pmoCommonAPI(api, type, data, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryPMO);
