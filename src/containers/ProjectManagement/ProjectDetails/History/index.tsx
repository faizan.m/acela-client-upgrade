import React, { useEffect, useState } from "react";
import HistoryProject from "./HistoryProject";
import CompletedItems from "./completedItems";
import { useProject } from "../projectContext";

enum PageType {
  LOGS,
  COMPLETED,
}

const ProjectHistory = () => {
  const [page, setPage] = useState<PageType>(PageType.COMPLETED);
  const { history, location } = useProject();

  useEffect(() => {
    // Function to update query parameter
    const updateTabQueryParam = () => {
      const searchParams = new URLSearchParams(location.search);
      searchParams.set("tab", "History");
      history.replace({
        pathname: location.pathname,
        search: `?${searchParams.toString()}`,
      });
    };
    updateTabQueryParam();
  }, [history, location.pathname, location.search]);

  return (
    <div className="project-logs-tab">
      <div className="log-header-links">
        <div
          className={
            "header-link" +
            (page === PageType.LOGS ? " header-link__active" : "")
          }
          onClick={() => setPage(PageType.LOGS)}
        >
          Logs
        </div>
        <div className="link-separator" />
        <div
          className={
            "header-link" +
            (page === PageType.COMPLETED ? " header-link__active" : "")
          }
          onClick={() => setPage(PageType.COMPLETED)}
        >
          Completed Items
        </div>
      </div>
      {page === PageType.LOGS && <HistoryProject />}
      {page === PageType.COMPLETED && <CompletedItems />}
    </div>
  );
};

export default ProjectHistory;
