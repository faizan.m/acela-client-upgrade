import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import {
  getActionItemList,
  ACTION_ITEM_SUCCESS,
} from "../../../../actions/pmo";
import Spinner from "../../../../components/Spinner";
import UsershortInfo from "../../../../components/UserImage";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import { useProject } from "../projectContext";
import Input from "../../../../components/Input/input";

interface CompletedItemProps {
  getActionItemList: (projectId: number) => Promise<any>;
}

interface ICompletedItem {
  id: number;
  item_type: "Meeting" | "Risk Item" | "Action Item" | "Critical Path Item";
  blob: { meeting: IProjectMeeting } | IProjectDetailItem;
  created_on: string;
  updated_on: string;
  project: number;
}

const CompletedItems: React.FC<CompletedItemProps> = (props) => {
  const { projectID, project, history } = useProject();
  const [search, setSearch] = useState<string>("");
  const [itemList, setItemList] = useState<ICompletedItem[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [sorting, setSorting] = useState<string>("");

  useEffect(() => {
    fetchCompletedItems();
  }, []);

  const fetchCompletedItems = () => {
    setLoading(true);
    props
      .getActionItemList(projectID)
      .then((action) => {
        if (action.type === ACTION_ITEM_SUCCESS)
          setItemList(action.response || []);
      })
      .finally(() => setLoading(false));
  };

  const setSort = (name: string) => {
    setSorting((prevSorting) =>
      prevSorting.includes("-") ? name : `-${name}`
    );
  };

  const getSortClass = (name: string) => {
    return (
      (sorting &&
        sorting.includes(name) &&
        (sorting.includes("-") ? "asc-order" : "desc-order")) ||
      ""
    );
  };

  const convertHtmlToText = (str: string) => {
    const tempDivElement = document.createElement("div");
    tempDivElement.innerHTML = str;
    return tempDivElement.textContent || tempDivElement.innerText || "";
  };

  const filterFunction = (row: ICompletedItem): boolean => {
    if (search && search.trim()) {
      let itemName = convertHtmlToText(
        (row.blob as IProjectDetailItem).description ||
          (row.blob as { meeting: IProjectMeeting }).meeting.name
      ).toLowerCase();
      let itemType = row.item_type ? row.item_type.toLowerCase() : "";
      let userName = project.project_manager;
      if (
        row.item_type !== "Meeting" &&
        (row.blob as IProjectDetailItem).owner_name
      )
        userName = (row.blob as IProjectDetailItem).owner_name;
      userName = userName ? userName.toLowerCase() : "";
      return (
        itemName.includes(search) ||
        itemType.includes(search) ||
        userName.includes(search)
      );
    }
    return true;
  };

  const getMeetingRow = (row: ICompletedItem) => {
    const blob = row.blob as { meeting: IProjectMeeting };
    return (
      <>
        <div
          className="pd-action-item col-md-4"
          title={convertHtmlToText(blob.meeting.name)}
        >
          {convertHtmlToText(blob.meeting.name)}
        </div>
        <div className="description col-md-3 user">{row.item_type}</div>
        <UsershortInfo
          name={project.project_manager || "N.A."}
          url={project.project_manager_profile_pic}
          className="inside-box col-md-3"
        />
        <div className="row-date col-md-2">
          {blob.meeting && blob.meeting.conducted_on
            ? fromISOStringToFormattedDate(
                blob.meeting.conducted_on,
                "MMM DD, YYYY"
              )
            : "N.A."}
        </div>
        <div className="remove col-md-1">
          <img
            className={"d-pointer info-png"}
            alt=""
            title={"Archived Meeting"}
            src={"/assets/icons/archive-filled-box.svg"}
            onClick={() => {
              const query = new URLSearchParams(location.search);
              const AllProject =
                query.get("AllProject") === "true" ? true : false;
              const meetingObj = cloneDeep(blob);
              localStorage.setItem("meeting", JSON.stringify(meetingObj));
              history.push(
                `/Project/${project.id}/meeting/${meetingObj.meeting
                  .meeting_id || meetingObj.meeting.id}?type=${
                  meetingObj.meeting.meeting_type
                }&view=true&AllProject=${AllProject}&completed=true`
              );
            }}
          />
        </div>
      </>
    );
  };

  const getProjectItemRow = (row: ICompletedItem) => {
    const blob = row.blob as IProjectDetailItem;
    return (
      <>
        <div
          className="pd-action-item col-md-4"
          title={convertHtmlToText(blob.description)}
        >
          {convertHtmlToText(blob.description)}
        </div>
        <div className="description col-md-3 user">{row.item_type}</div>
        <UsershortInfo
          name={blob.owner_name || project.project_manager || "N.A."}
          url={
            blob.owner_name && blob.owner_profile_pic
              ? blob.owner_profile_pic
              : project.project_manager_profile_pic
          }
          className="inside-box col-md-3"
        />
        <div className="row-date col-md-2">
          {blob.completed_on
            ? fromISOStringToFormattedDate(blob.completed_on, "MMM DD, YYYY")
            : "N.A."}
        </div>
        <div className="remove col-md-1" />
      </>
    );
  };

  return (
    <div className="project-details-action-items completed-items-container">
      <Input
        field={{
          value: search,
          label: "",
          type: "SEARCH",
        }}
        width={10}
        name="searchString"
        onChange={(e) => setSearch(e.target.value)}
        placeholder="Search"
        className={`logs-tab-search`}
      />
      {itemList && itemList.length > 0 && !loading && (
        <div className="col-md-12 status-row heading">
          <label className={`col-md-4 field__label-label`}>Item Name</label>
          <label
            className={`col-md-3 field__label-label ${getSortClass(
              "item_type"
            )}`}
            onClick={(e) => setSort("item_type")}
          >
            Type
          </label>
          <label className={`col-md-3 field__label-label`}>Owner</label>
          <label className={`col-md-2 field__label-label`}>Completed On</label>
          <label className="col-md-1" />
        </div>
      )}
      <Spinner show={loading} />
      {!loading &&
        itemList
          .sort((a, b) => commonFunctions.sortList(a, b, "id", sorting))
          .filter(filterFunction)
          .map((row) => {
            return (
              <div className={`col-md-12 status-row`} key={row.id}>
                {row.item_type === "Meeting"
                  ? getMeetingRow(row)
                  : getProjectItemRow(row)}
              </div>
            );
          })}
      {itemList && itemList.length === 0 && !loading && (
        <div className="col-md-12 status-row ">
          <div className="no-data-action-item col-md-11">
            No items available
          </div>
        </div>
      )}
    </div>
  );
};

const mapDispatchToProps = (dispatch: any) => ({
  getActionItemList: (projectId: number) =>
    dispatch(getActionItemList(projectId, "archived-items")),
});

export default connect(null, mapDispatchToProps)(CompletedItems);
