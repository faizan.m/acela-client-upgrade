import React, { createContext, useState, useContext, useEffect } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import {
  getActionItemList,
  getProjectDetails,
  getProjectTeamMembers,
  getProjectStatusByProjectID,
  getProjectCustomerContactsByID,
  getProjectAdditionalContactsByID,
  ACTION_ITEM_SUCCESS,
  PROJECT_DETAILS_FAILURE,
  PROJECT_DETAILS_SUCCESS,
  FETCH_PROJECT_TEAM_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
  PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
} from "../../../actions/pmo";
import { addErrorMessage } from "../../../actions/appState";
import ProjectDetailsMain from "./indexv2";
import "./style.scss";

// Create the project context
const ProjectContext = createContext<ProjectContextValue>(null);

interface ProjectDetailsPageProps extends ICommonProps {
  addErrorMessage: TShowErrorMessage;
  fetchMeetings: (projectID: number) => Promise<any>;
  getProjectDetails: (projectID: number) => Promise<any>;
  getProjectStatusByProjectID: (projectId: number) => Promise<any>;
  fetchProjectTeam: (
    projectId: number,
    params: IServerPaginationParams
  ) => Promise<any>;
  fetchCustomerContacts: (
    projectId: number,
    params: IServerPaginationParams
  ) => Promise<any>;
  fetchAdditionalContacts: (
    projectId: number,
    params: IServerPaginationParams
  ) => Promise<any>;
}

// TODO: Fetch all the project contacts here, make project item (action, critical, risk) dependent only on status
// TODO: Rename indexV2 to ProjectDetailNav and this file to index.tsx
// Provider component to manage project state
const ProjectProvider = (props: ProjectDetailsPageProps) => {
  const projectID: number = Number(props.match.params.id);
  const [project, setProject] = useState<IProjectDetails>();
  const [loadingProject, setLoadingProject] = useState<boolean>(false);
  const [meetingList, setMeetingList] = useState<IProjectMeeting[]>([]);
  const [projectStatusList, setStatusList] = useState<IStatusImpactItem[]>([]);
  const [projectTeam, setProjectTeam] = useState<IProjectTeamMember[]>();
  const [customerContacts, setCustomerContacts] = useState<
    IProjectCustomerContact[]
  >();
  const [additionalContacts, setAdditionalContacts] = useState<
    IProjectAdditionalContact[]
  >();

  useEffect(() => {
    fetchProjectDetails();
    fetchProjectMeetings();
    fetchProjectStatusList();
    fetchProjectTeam();
    fetchCustomerContacts();
    fetchAdditionalContacts();
  }, []);

  const fetchProjectTeam = () => {
    props.fetchProjectTeam(projectID, { pagination: false }).then((action) => {
      if (action.type === FETCH_PROJECT_TEAM_SUCCESS) {
        setProjectTeam(action.response);
      }
    });
  };

  const fetchCustomerContacts = () => {
    props
      .fetchCustomerContacts(projectID, { pagination: false })
      .then((action) => {
        if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
          setCustomerContacts(action.response);
        }
      });
  };

  const fetchAdditionalContacts = () => {
    props
      .fetchAdditionalContacts(projectID, { pagination: false })
      .then((action) => {
        if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
          setAdditionalContacts(action.response);
        }
      });
  };

  const updateProject = (newVals: Partial<IProjectDetails>) => {
    const updatedProject = cloneDeep({ ...project, ...newVals });
    setProject(updatedProject);
  };

  const fetchProjectDetails = () => {
    setLoadingProject(true);
    props
      .getProjectDetails(projectID)
      .then((action) => {
        if (action.type === PROJECT_DETAILS_SUCCESS) {
          setProject(action.response);
        } else if (action.type === PROJECT_DETAILS_FAILURE) {
          props.addErrorMessage("Error fetching project!");
        }
      })
      .finally(() => setLoadingProject(false));
  };

  const fetchProjectMeetings = () => {
    props.fetchMeetings(projectID).then((action) => {
      if (action.type === ACTION_ITEM_SUCCESS) {
        setMeetingList(action.response);
      }
    });
  };

  const fetchProjectStatusList = () => {
    props.getProjectStatusByProjectID(projectID).then((action) => {
      setStatusList(action.response);
    });
  };

  return (
    <ProjectContext.Provider
      value={{
        project,
        projectID,
        meetingList,
        updateProject,
        loadingProject,
        projectTeam,
        customerContacts,
        additionalContacts,
        projectStatusList,
        match: props.match,
        location: props.location,
        history: props.history,
      }}
    >
      <ProjectDetailsMain />
    </ProjectContext.Provider>
  );
};

// Custom hook to consume the Project context
export const useProject = (): ProjectContextValue => {
  const context = useContext(ProjectContext);
  if (!context) {
    throw new Error("useProject must be used within ProjectProvider");
  }
  return context;
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  fetchMeetings: (projectID: number) =>
    dispatch(getActionItemList(projectID, "meetings")),
  getProjectStatusByProjectID: (projectId: number) =>
    dispatch(getProjectStatusByProjectID(projectId)),
  getProjectDetails: (projectID: number) =>
    dispatch(getProjectDetails(projectID)),
  fetchProjectTeam: (projectId: number, params: IServerPaginationParams) =>
    dispatch(getProjectTeamMembers(projectId, params)),
  fetchCustomerContacts: (projectId: number, params: IServerPaginationParams) =>
    dispatch(getProjectCustomerContactsByID(projectId, params)),
  fetchAdditionalContacts: (
    projectId: number,
    params: IServerPaginationParams
  ) => dispatch(getProjectAdditionalContactsByID(projectId, params)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectProvider);
