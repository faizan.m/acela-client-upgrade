import React from "react";
import { connect } from "react-redux";
import {
  deleteMeetingDocumentsByProjectID, downloadMeetingDocument,
  DOWNLOAD_MEETING_DOCUMENT_SUCCESS, getMeetingDocumentsByProjectID, PROJECT_MEETING_DOCUMENTS_SUCCESS, saveMeetingDocumentsByProjectID
} from "../../../actions/pmo";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../components/Spinner";
import { saveBlob } from '../../../utils/download';

import "./style.scss";

interface IMeetingDocumentsProps {
  projectID?: number;
  meetingID?: string;
  getMeetingDocumentsByProjectID: any;
  saveMeetingDocumentsByProjectID: any;
  deleteMeetingDocumentsByProjectID: any;
  downloadMeetingDocument: any;
  viewOnly?: boolean;
}

interface IMeetingDocumentState {
  currentUploadedDocument: any;
  projectDocuments: any;
  loading: boolean;
  showFileSizeError: boolean;
}

const fileIcons = {
  pdf: "/assets/icons/008-file-7.svg",
  doc: "/assets/icons/005-file-4.svg",
  docx: "/assets/icons/006-file-5.svg",
  zip: "/assets/icons/009-file-8.svg",
  xls: "/assets/icons/047-file-46.svg",
  csv: "/assets/icons/048-file-47.svg",
  svg: "/assets/icons/001-file.svg",
  png: "/assets/icons/003-file-2.svg",
  jpg: "/assets/icons/004-file-3.svg",
  jpeg: "/assets/icons/004-file-3.svg",
  txt: "/assets/icons/034-file-33.svg",
};

class MeetingDocuments extends React.Component<
  IMeetingDocumentsProps,
  IMeetingDocumentState
> {
  uploadInput = React.createRef<HTMLInputElement>();

  constructor(props: IMeetingDocumentsProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    currentUploadedDocument: undefined,
    projectDocuments: [],
    loading: false,
    showFileSizeError: false
  });

  componentDidMount() {
    const {projectID, meetingID} = this.props;
    if (meetingID && meetingID !== "0") {
      this.getMeetingDocumentsByProjectID(projectID, meetingID);
    }
  }

  getMeetingDocumentsByProjectID = (projectId, meetingId) => {
    this.setState({ loading: true });
    this.props.getMeetingDocumentsByProjectID(projectId, meetingId).then((action) => {
      if (action.type === PROJECT_MEETING_DOCUMENTS_SUCCESS) {
        this.setState({
          projectDocuments: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  deleteDocument = (document) => {
    this.setState({ loading: true });
    this.props
      .deleteMeetingDocumentsByProjectID(this.props.projectID, this.props.meetingID, document.id)
      .then((action) => {
        if (action.type === PROJECT_MEETING_DOCUMENTS_SUCCESS) {
          this.getMeetingDocumentsByProjectID(this.props.projectID, this.props.meetingID);
        }
      });
  };

  handleFileUpload = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile = files[0];
    const fileSize = Math.round(((dataFile as any).size / 1024));

    this.setState({
      currentUploadedDocument: fileSize < 10240 && dataFile,
      showFileSizeError: fileSize > 10240
    });
  };

  uploadDocument = () => {
    this.setState({ loading: true });
    const data = new FormData();
    data.append("file_path", this.state.currentUploadedDocument);
    data.append("file_name", this.state.currentUploadedDocument.name);
    this.props
      .saveMeetingDocumentsByProjectID(this.props.projectID, this.props.meetingID, data)
      .then((action) => {
        if (action.type === PROJECT_MEETING_DOCUMENTS_SUCCESS) {
          this.setState({
            currentUploadedDocument: undefined,
          });
          this.getMeetingDocumentsByProjectID(this.props.projectID, this.props.meetingID);
        }
      });
  };

  downloadDocument = (document: any) => {
    this.setState({ loading: true })
    const {projectID, meetingID} = this.props;
    this.props.downloadMeetingDocument(projectID, meetingID, document.id).then(action => {
        if (action.type === DOWNLOAD_MEETING_DOCUMENT_SUCCESS) {
          saveBlob(document.file_name, action.response);
          this.setState({ loading: false })
        }
    });
  }

  render() {
    return (
      <div className="document-details-box details-box">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div className="doucments-list">
          {this.state.projectDocuments.map((document) => (
            <div className="document">
              <div className="doc-right">
                <img
                  className="file-icons"
                  alt=""
                  src={
                    fileIcons[document.file_name.split(".").pop()] ||
                    "/assets/icons/046-file-45.svg"
                  }
                />
                <div className="label-content">{document.file_name}</div>
              </div>
              <img
                  className="meeting-download-icons"
                  alt=""
                  src={"/assets/icons/download.png"}
                  onClick={() => this.downloadDocument(document)}
              />
              {!this.props.viewOnly && (
                <SmallConfirmationBox
                  className="remove-file"
                  onClickOk={() => this.deleteDocument(document)}
                  text={"this file"}
                />
              )}
            </div>
          ))}
        </div>
        <input
          type="file"
          style={{ display: "none" }}
          ref={this.uploadInput}
          onChange={this.handleFileUpload}
        />
        {this.props.meetingID !== '0' && this.state.currentUploadedDocument ? (
          <>
            <div className="meeting-upload-box">
              <span className="upload-filename">
                {this.state.currentUploadedDocument.name}
              </span>
            </div>
            <div className="meeting-file-upload-actions">
              <img
                className="file-action-icons file-upload-save"
                alt=""
                src={"/assets/icons/validated.svg"}
                onClick={() => this.uploadDocument()}
              />
              <img
                className="file-action-icons file-upload-cancel"
                alt=""
                src={"/assets/icons/close.png"}
                onClick={() =>
                  this.setState({ currentUploadedDocument: undefined })
                }
              />
            </div>
          </>
        ) : 
            !this.props.viewOnly && (
              <div
                className="document-label-details action-item"
                onClick={() => (this.uploadInput as any).current.click()}
              >
                Upload File
              </div>
            )
        }
        {
          this.state.showFileSizeError && (
            <div
              className="document-label-details action-item document-error"
              onClick={() => (this.uploadInput as any).current.click()}
            >
              File too big, please select a file less than 10MB
            </div>
          )
        }
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getMeetingDocumentsByProjectID: (projectId: number, meetingId: number) =>
    dispatch(getMeetingDocumentsByProjectID(projectId, meetingId)),
  saveMeetingDocumentsByProjectID: (projectId: number, meetingId: number, data: any) =>
    dispatch(saveMeetingDocumentsByProjectID(projectId, meetingId, data)),
  deleteMeetingDocumentsByProjectID: (projectId: number, meetingId: number, documentId: number) =>
    dispatch(deleteMeetingDocumentsByProjectID(projectId, meetingId, documentId)),
  downloadMeetingDocument: (projectId: number, meetingId: number, documentId: number) =>
    dispatch(downloadMeetingDocument(projectId, meetingId, documentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MeetingDocuments);
