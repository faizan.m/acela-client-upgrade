import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import PMOCollapsible from "../../../../components/PMOCollapsible";
import ProjectItem from "../projectItem";
import ProjectNotes from "./projectNotes";
import {
  getImpactList,
  getActionItemList,
  ACTION_ITEM_SUCCESS,
} from "../../../../actions/pmo";
import UsershortInfo from "../../../../components/UserImage";
import ProjectPhases from "./projectPhases";
import { useProject } from "../projectContext";

interface ProjectDetailsProps {
  getImpactList: () => Promise<any>;
  fetchMeetings: (projectID: number) => Promise<any>;
}

const ProjectDetails: React.FC<ProjectDetailsProps> = (props) => {
  const {
    project,
    history,
    location,
    projectStatusList,
    projectTeam,
    customerContacts,
    additionalContacts,
  } = useProject();
  const [meetingList, setMeetingList] = useState<IProjectMeeting[]>([]);
  const [impactList, setImpactList] = useState<IStatusImpactItem[]>();

  useEffect(() => {
    // Function to update query parameter
    const updateTabQueryParam = () => {
      const searchParams = new URLSearchParams(location.search);
      searchParams.set("tab", "Details");
      history.replace({
        pathname: location.pathname,
        search: `?${searchParams.toString()}`,
      });
    };
    updateTabQueryParam();
  }, [history, location.pathname, location.search]);

  useEffect(() => {
    fetchImpactList();
    fetchProjectMeetings();
  }, []);

  const fetchProjectMeetings = () => {
    props.fetchMeetings(project.id).then((action) => {
      if (action.type === ACTION_ITEM_SUCCESS) {
        setMeetingList(action.response);
      }
    });
  };

  const ownerOptions = useMemo(() => {
    if (projectTeam && customerContacts && additionalContacts) {
      const usersList: {
        user_id: string | number;
        name: string;
        profile_url: string;
      }[] = [
        ...projectTeam.map((el) => ({
          name: el.full_name,
          profile_url: el.profile_url,
          user_id: el.id,
        })),
        ...customerContacts.map((el) => ({
          name: el.name,
          profile_url: el.profile_url,
          user_id: el.user_id,
        })),
        ...additionalContacts.map((el) => {
          return {
            name: el.name,
            user_id: el.id,
            profile_url: "",
          };
        }),
      ];
      return usersList.map((el) => ({
        value: el.user_id,
        label: (
          <UsershortInfo
            name={el.name}
            url={el.profile_url}
            className="inside-box"
          />
        ),
      }));
    }
    return [];
  }, [projectTeam, customerContacts, additionalContacts]);

  const fetchImpactList = () => {
    props.getImpactList().then((action) => {
      setImpactList(action.response);
    });
  };

  // Remove support for meetingList props in ProjectNotes after complete integration
  return (
    <div className="project-details-tab">
      <ProjectPhases />
      <div className="project-details-collapsables">
        <PMOCollapsible label={"Critical Path Items"} isOpen={true}>
          <ProjectItem
            project={project}
            statusList={projectStatusList}
            impactList={impactList}
            ownerOptions={ownerOptions}
            itemType={"critical-path-items"}
          />
        </PMOCollapsible>
        <PMOCollapsible label={"Action Items"} isOpen={true}>
          <ProjectItem
            statusList={projectStatusList}
            impactList={impactList}
            project={project}
            itemType={"action-items"}
            ownerOptions={ownerOptions}
          />
        </PMOCollapsible>
        <PMOCollapsible label={"Risks"} isOpen={true}>
          <ProjectItem
            statusList={projectStatusList}
            impactList={impactList}
            itemType={"risk-items"}
            project={project}
            ownerOptions={ownerOptions}
          />
        </PMOCollapsible>
        <PMOCollapsible label={"Project Notes"} isOpen={true}>
          <ProjectNotes projectID={project.id} meetingList={meetingList} />
        </PMOCollapsible>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchMeetings: (projectID: number) =>
    dispatch(getActionItemList(projectID, "meetings")),
  getImpactList: () => dispatch(getImpactList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetails);
