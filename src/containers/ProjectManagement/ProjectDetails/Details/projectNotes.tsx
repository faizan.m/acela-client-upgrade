import { cloneDeep } from "lodash";
import React from "react";
import ReactQuill from "react-quill";
import { connect } from "react-redux";
import {
  getProjectCWNotesByID,
  getProjectNotesByID,
  PROJECT_NOTES_SUCCESS,
  saveCWProjectNotesByID,
  deleteCWProjectNotesByID,
  editCWProjectNotesByID,
} from "../../../../actions/pmo";
import SquareButton from "../../../../components/Button/button";
import Input from "../../../../components/Input/input";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import UsershortInfo from "../../../../components/UserImage";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import { commonFunctions } from "../../../../utils/commonFunctions";

interface IProjectNotesProps {
  user: ISuperUser;
  projectID?: number;
  meetingList: IProjectMeeting[];
  getAcelaNotesByID: (projectId: number) => Promise<any>;
  getProjectNotesByID: (projectId: number) => Promise<any>;
  saveProjectNotesByID: (projectId: number, note: string) => Promise<any>;
  deleteProjectNotesByID: (projectId: number, crm_id: number) => Promise<any>;
  editProjectNotesByID: (
    projectId: number,
    note: string,
    crm_id: number
  ) => Promise<any>;
}

interface IProjectNote {
  note_id: number;
  project_id: number;
  text: string;
  note_type_id: number;
  note_type_name: string;
  flagged: boolean;
  last_updated: string;
  updated_by: string;
  updated_on: string;
  author_id?: string;
  author?: string;
  author_email?: string;
  author_profile_pic?: string;
  meeting_id?: number;
}

interface IProjectNotesState {
  notes: IProjectNote[];
  loading: boolean;
  newNote: boolean;
  note: string;
  editableNoteID: number | undefined;
}
class ProjectNotes extends React.Component<
  IProjectNotesProps,
  IProjectNotesState
> {
  constructor(props: IProjectNotesProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    notes: [],
    loading: true,
    newNote: false,
    note: "",
    editableNoteID: undefined,
  });

  componentDidMount() {
    if (this.props.projectID && this.state.notes.length === 0) {
      this.getProjectNotesByID(this.props.projectID);
    }
  }

  getProjectNotesByID = (id: number) => {
    this.props.getProjectNotesByID(id).then((action) => {
      if (action.type === PROJECT_NOTES_SUCCESS) {
        const cwNotes: IProjectNote[] = action.response;
        // Get the meeting notes from the normal Project Notes API
        this.props
          .getAcelaNotesByID(id)
          .then((a) => {
            if (a.type === PROJECT_NOTES_SUCCESS) {
              const meetingNotes: IProjectNote[] = a.response
                .filter((el) => Boolean(el.meeting_id))
                .map((el) => {
                  delete el.id;
                  el.note_id = el.project_notes_crm_id;
                  el.text = el.note;
                  delete el.project_notes_crm_id;
                  return el;
                });
              this.setState({
                notes: [...meetingNotes, ...cwNotes],
              });
            }
          })
          .catch(() => {
            this.setState({ notes: cwNotes });
          })
          .finally(() => this.setState({ loading: false }));
      }
    });
  };

  getAcelaNotes = (id: number) => {};

  handleChange = (content: string) => {
    const newState: IProjectNotesState = cloneDeep(this.state);
    (newState.note as string) = content;
    this.setState(newState);
  };

  onSaveClick = () => {
    this.setState({ loading: true });
    this.props
      .saveProjectNotesByID(this.props.projectID, this.state.note)
      .then((action) => {
        if (action.type === PROJECT_NOTES_SUCCESS) {
          this.getProjectNotesByID(this.props.projectID);
        }
        this.setState({ note: "", newNote: false });
      });
  };

  onDeleteRowClick = (note: IProjectNote) => {
    this.setState({ loading: true });
    this.props
      .deleteProjectNotesByID(this.props.projectID, note.note_id)
      .then((action) => {
        if (action.type === PROJECT_NOTES_SUCCESS) {
          this.getProjectNotesByID(this.props.projectID);
          this.setState({
            editableNoteID: undefined,
            note: "",
            newNote: false,
          });
        }
      });
  };

  onEditSaveClick = (note: IProjectNote) => {
    this.setState({ loading: true });
    this.props
      .editProjectNotesByID(this.props.projectID, this.state.note, note.note_id)
      .then((action) => {
        if (action.type === PROJECT_NOTES_SUCCESS) {
          this.getProjectNotesByID(this.props.projectID);
        }
        this.setState({ editableNoteID: undefined, note: "", newNote: false });
      });
  };

  render() {
    return (
      <div className="project-notes">
        {this.state.newNote && (
          <div className="new-note">
            <Input
              field={{
                label: "Note",
                type: "CUSTOM",
                value: "",
                isRequired: true,
              }}
              width={12}
              onChange={(e) => null}
              name=""
              customInput={
                <ReactQuill
                  value={this.state.note}
                  onChange={(htmlString: string) =>
                    this.handleChange(htmlString)
                  }
                  modules={{
                    toolbar: [[{ list: "ordered" }, { list: "bullet" }]],
                  }}
                  theme="snow"
                  className={`action-items-quill project-notes-quill`}
                />
              }
            />
            <div className="action-btn">
              <SquareButton
                content={"Save"}
                bsStyle={"primary"}
                onClick={() => this.onSaveClick()}
                className=""
                disabled={commonFunctions.isEditorEmpty(this.state.note)}
              />
              <SquareButton
                content={"Cancel"}
                bsStyle={"default"}
                onClick={(e) => this.setState({ note: "", newNote: false })}
                className=""
              />
            </div>
          </div>
        )}
        {!this.state.newNote && (
          <div
            onClick={() => this.setState({ newNote: true })}
            className="add-new-note"
          >
            Add Note
          </div>
        )}
        {!this.state.loading &&
          this.state.notes &&
          this.state.notes.map((note, index) => {
            const meeting =
              this.props.meetingList &&
              this.props.meetingList.find((m) => m.id === note.meeting_id);
            return (
              <div className="notes-row" key={index}>
                <div className="top-header">
                  <div className="user">
                    <UsershortInfo
                      name={note.author ? note.author : note.updated_by}
                      url={
                        note.author_profile_pic ? note.author_profile_pic : ""
                      }
                    />
                    {meeting && (
                      <div
                        className="note-from"
                        title={`Meeting ID : ${note.meeting_id}`}
                        onClick={() => {}}
                      >
                        Meeting : {meeting ? meeting.name : ""}
                      </div>
                    )}
                  </div>
                  {this.props.user &&
                    !note.meeting_id &&
                    this.props.user.email === note.author_email && (
                      <div className="actions-notes">
                        <img
                          className={"d-pointer icon-remove edit-png"}
                          alt=""
                          src={"/assets/icons/edit.png"}
                          onClick={(e) =>
                            this.setState({
                              note: note.text,
                              editableNoteID: note.note_id,
                            })
                          }
                        />
                        <SmallConfirmationBox
                          className="remove"
                          onClickOk={() => this.onDeleteRowClick(note)}
                          text={"Note"}
                        />
                      </div>
                    )}
                </div>
                {this.state.editableNoteID === note.note_id ? (
                  <div className="new-note">
                    <Input
                      field={{
                        label: "Note",
                        type: "CUSTOM",
                        value: "",
                        isRequired: true,
                      }}
                      width={12}
                      onChange={(e) => null}
                      name=""
                      customInput={
                        <ReactQuill
                          value={this.state.note}
                          onChange={(htmlString: string) =>
                            this.handleChange(htmlString)
                          }
                          modules={{
                            toolbar: [
                              [{ list: "ordered" }, { list: "bullet" }],
                            ],
                          }}
                          theme="snow"
                          className={`action-items-quill project-notes-quill`}
                        />
                      }
                    />
                    <div className="action-btn">
                      <SquareButton
                        content={"Save"}
                        bsStyle={"primary"}
                        onClick={() => this.onEditSaveClick(note)}
                        className=""
                        disabled={commonFunctions.isEditorEmpty(
                          this.state.note
                        )}
                      />
                      <SquareButton
                        content={"Cancel"}
                        bsStyle={"default"}
                        onClick={(e) =>
                          this.setState({
                            editableNoteID: undefined,
                            note: "",
                            newNote: false,
                          })
                        }
                        className=""
                      />
                    </div>
                  </div>
                ) : (
                  <>
                    <div
                      className="note ql-editor"
                      dangerouslySetInnerHTML={{
                        __html: note.text,
                      }}
                    />
                    <div className="date">
                      {note.updated_on
                        ? fromISOStringToFormattedDate(
                            note.updated_on,
                            "MM/DD/YYYY hh:mm A"
                          )
                        : "N.A."}
                    </div>
                  </>
                )}
              </div>
            );
          })}
        {!this.state.loading && this.state.notes.length === 0 && (
          <div className="no-notes-row">No notes added</div>
        )}
        {this.state.loading && <div className="no-notes-row">Loading...</div>}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectNotesByID: (projectId: number) =>
    dispatch(getProjectCWNotesByID(projectId)),
  getAcelaNotesByID: (projectId: number) =>
    dispatch(getProjectNotesByID(projectId)),
  saveProjectNotesByID: (projectId: number, note: string) =>
    dispatch(saveCWProjectNotesByID(projectId, note)),
  deleteProjectNotesByID: (projectId: number, crm_id: number) =>
    dispatch(deleteCWProjectNotesByID(projectId, crm_id)),
  editProjectNotesByID: (projectId: number, note: string, crm_id: number) =>
    dispatch(editCWProjectNotesByID(projectId, note, crm_id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectNotes);
