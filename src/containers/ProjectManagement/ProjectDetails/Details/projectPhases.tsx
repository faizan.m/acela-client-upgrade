import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  getProjectPhasesByID,
  updateProjectDetails,
  getActivityStatusList,
  getProjectStatusByProjectID,
  updateProjectPhasesStatusByID,
  PROJECT_PHASES_SUCCESS,
} from "../../../../actions/pmo";
import Input from "../../../../components/Input/input";
import SMPopUp from "../../../../components/SMPopup/smPopUp";
import { fromISOStringToFormattedDateCW } from "../../../../utils/CalendarUtil";
import { useProject } from "../projectContext";

interface IProjectPhasesProps {
  getActivityStatusList: (url: string) => Promise<any>;
  getProjectPhasesByID: (projectId: number) => Promise<any>;
  getProjectStatusByProjectID: (projectId: number) => Promise<any>;
  updateProjectDetails: (data: any, projectID: number) => Promise<any>;
  updateProjectPhasesStatusByID: (
    data: any,
    projectId: number,
    phaseId: number
  ) => Promise<any>;
}

const ProjectPhases: React.FC<IProjectPhasesProps> = (props) => {
  const { project } = useProject();
  const [status, setStatus] = useState<number>();
  const [completionDate, setDate] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [phases, setPhases] = useState<ProjectPhaseParent[]>([]);
  const [statusOptions, setStatusOptions] = useState<IPickListOptions[]>([]);

  useEffect(() => {
    fetchStatuses();
    fetchPhases(true);
  }, []);

  const fetchPhases = (pushDate: boolean) => {
    setLoading(true);
    props
      .getProjectPhasesByID(project.id)
      .then((action) => {
        if (action.type === PROJECT_PHASES_SUCCESS) {
          const phasesList = action.response;
          if (phasesList && phasesList.length > 1 && pushDate) {
            phasesList[phasesList.length - 1].estimated_completion_date =
              project.estimated_end_date;
          }
          setPhases(phasesList);
        }
      })
      .finally(() => setLoading(false));
  };

  const fetchStatuses = () => {
    props
      .getActivityStatusList("pmo/settings/milestone-status")
      .then((action) => {
        const options: IPickListOptions[] = action.response.map((s) => ({
          value: s.id,
          label: `${s.title}`,
        }));
        setStatusOptions(options);
      });
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (e.target.name === "status") setStatus(Number(e.target.value));
    else setDate(e.target.value);
  };

  const onSaveStatus = (phase: ProjectPhaseParent, index: number) => {
    props
      .updateProjectPhasesStatusByID(
        {
          status: status,
          estimated_completion_date: completionDate,
        },
        project.id,
        phase.id
      )
      .then((action) => {
        if (action.type === PROJECT_PHASES_SUCCESS) {
          fetchPhases(false);
        }
        const totalPhases = phases.length;
        if (totalPhases === index + 1) {
          updateProject();
        }
      });
  };

  const onPhaseClick = (phase: ProjectPhaseParent) => {
    setStatus(phase.status && phase.status.id);
    setDate(phase.estimated_completion_date);
  };

  const updateProject = () => {
    props
      .updateProjectDetails(
        {
          estimated_end_date: completionDate,
        },
        project.id
      )
      .then((action) => {
        fetchPhases(false);
      });
  };

  return (
    <div className="project-phases-container">
      {phases &&
        phases.map((phase, index) => (
          <SMPopUp
            key={index}
            className="project-phase-popup"
            onClickOk={() => onSaveStatus(phase, index)}
            isValidForm={status && completionDate}
            clickableSection={
              <div className="action" onClick={() => onPhaseClick(phase)}>
                <div className="status-name">{phase.phase.title}</div>
                <div
                  className="status-box"
                  style={{
                    backgroundColor: phase.status && phase.status.color,
                  }}
                >
                  <div className="name">
                    {phase.status && phase.status.title}
                  </div>
                  <div className="date">
                    {phase.estimated_completion_date
                      ? fromISOStringToFormattedDateCW(
                          phase.estimated_completion_date,
                          "MMM DD, YYYY"
                        )
                      : "TBD"}
                  </div>
                </div>
              </div>
            }
          >
            <div className="box">
              <Input
                field={{
                  label: "Status",
                  type: "PICKLIST",
                  value: status,
                  options: statusOptions,
                  isRequired: false,
                }}
                width={12}
                multi={false}
                name="status"
                onChange={(e) => handleChange(e)}
                placeholder={`Select`}
              />
              <Input
                field={{
                  value:
                    completionDate && new Date(completionDate).toISOString(),
                  label: "Estimated date of completion",
                  type: "DATE",
                  isRequired: false,
                }}
                width={12}
                name="completionDate"
                onChange={(e) => handleChange(e)}
                placeholder="Select Date"
                showTime={false}
                showDate={true}
                disablePrevioueDates={false}
                disableTimezone={true}
              />
            </div>
          </SMPopUp>
        ))}
      {!loading && phases.length === 0 && (
        <div className="no-data col-md-12">No Phases available</div>
      )}
      {loading && <div className="no-data col-md-12">Loading...</div>}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getActivityStatusList: (url: string) => dispatch(getActivityStatusList(url)),
  getProjectPhasesByID: (projectId: number) =>
    dispatch(getProjectPhasesByID(projectId)),
  updateProjectPhasesStatusByID: (
    data: any,
    projectId: number,
    phaseId: number
  ) => dispatch(updateProjectPhasesStatusByID(data, projectId, phaseId)),
  getProjectStatusByProjectID: (projectId: number) =>
    dispatch(getProjectStatusByProjectID(projectId)),
  updateProjectDetails: (data: any, projectID: number) =>
    dispatch(updateProjectDetails(data, projectID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectPhases);
