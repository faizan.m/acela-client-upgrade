import React from "react";
import moment from "moment";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import ReactQuill from "react-quill";
import {
  postActionItem,
  editActionItem,
  deleteActionItem,
  getActionItemList,
  ACTION_ITEM_SUCCESS,
  ACTION_ITEM_FAILURE,
} from "../../../actions/pmo";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import {
  dateSetTimeZero,
  fromISOStringToFormattedDate,
} from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";
import {
  getConvertedColorWithOpacity,
  isEmptyObj,
} from "../../../utils/CommonUtils";

interface IProjectItemProps {
  user: ISuperUser;
  viewOnly?: boolean;
  project?: IProjectDetails;
  ownerOptions: IPickListOptions[];
  statusList: IStatusImpactItem[];
  impactList: IStatusImpactItem[];
  initialData?: IProjectDetailItem[];
  itemType: "action-items" | "critical-path-items" | "risk-items";
  getActionItemList: (projectId: number, api: string) => Promise<any>;
  editActionItem: (projectId: number, data: any, api: string) => Promise<any>;
  postActionItem: (
    projectId: number,
    data: IProjectDetailItem,
    api: string
  ) => Promise<any>;
  deleteActionItem: (
    projectId: number,
    ItemId: number,
    api: string
  ) => Promise<any>;
  callbackSorting?: any;
  sorting?: any;
  sortingKey?: any;
}

interface IProjectItemState {
  sorting: string;
  loading: boolean;
  projectItemList: IProjectItem[];
}

interface IProjectItem extends IProjectDetailItem {
  edited?: boolean;
  descriptionError?: string;
  showCompleted?: boolean;
  completed_on?: any;
}

class ProjectItems extends React.Component<
  IProjectItemProps,
  IProjectItemState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  constructor(props: IProjectItemProps) {
    super(props);

    this.state = {
      sorting: "",
      loading: false,
      projectItemList: [],
    };
  }

  componentDidMount() {
    if (this.props.viewOnly) {
      this.setState({ projectItemList: this.props.initialData || [] });
    } else {
      this.getActionItemList();
    }
    if (this.props.sortingKey && !isEmptyObj(this.props.sorting)) {
      this.setState({
        sorting: this.props.sorting[this.props.sortingKey].ordering_field,
      });
    }
  }

  getActionItemList = () => {
    this.setState({ loading: true });
    this.props
      .getActionItemList(this.props.project.id, this.props.itemType)
      .then((action) => {
        this.setState({ loading: false, projectItemList: action.response });
      });
  };

  addNew = () => {
    const newState = cloneDeep(this.state);
    let status: IStatusImpactItem;
    if (this.props.itemType === "risk-items") {
      status = this.props.impactList && this.props.impactList[1];
      newState.projectItemList.push({
        description: "",
        owner: this.props.user && this.props.user.id,
        impact: status && status.id,
        created_on: moment(),
        edited: true,
      });
    } else {
      status =
        this.props.statusList &&
        this.props.statusList.find((f) => f.is_default);
      newState.projectItemList.push({
        description: "",
        owner: this.props.user && this.props.user.id,
        due_date: moment(new Date()).add(7, "days"),
        status: status && status.id,
        edited: true,
      });
    }
    this.setState(newState);
  };

  onDeleteRowClick = (index: number, rowData: IProjectItem) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    this.props
      .deleteActionItem(this.props.project.id, rowData.id, this.props.itemType)
      .then((action) => {
        if (action.type === ACTION_ITEM_SUCCESS) {
          this.getActionItemList();
        }
        if (action.type === ACTION_ITEM_FAILURE) {
          newState.projectItemList[index].descriptionError = `Couldn't delete`;
        }
        this.setState(newState);
      });
  };

  onSaveRowClick = (index: number, rowData: IProjectItem) => {
    const newState = cloneDeep(this.state);
    if (rowData.id) {
      this.props
        .editActionItem(this.props.project.id, rowData, this.props.itemType)
        .then((action) => {
          if (action.type === ACTION_ITEM_SUCCESS) {
            newState.projectItemList[index].edited = false;
            newState.projectItemList[index].descriptionError = "";
            newState.projectItemList[index] = action.response;
          }
          if (action.type === ACTION_ITEM_FAILURE) {
            newState.projectItemList[index].edited = true;
            newState.projectItemList[
              index
            ].descriptionError = action.errorList.data.description.join(" ");
          }
          this.setState(newState);
        });
    } else {
      this.props
        .postActionItem(this.props.project.id, rowData, this.props.itemType)
        .then((action) => {
          if (action.type === ACTION_ITEM_SUCCESS) {
            newState.projectItemList[index].edited = false;
            newState.projectItemList[index].descriptionError = "";
            newState.projectItemList[index] = action.response;
          }
          if (action.type === ACTION_ITEM_FAILURE) {
            newState.projectItemList[index].edited = true;
            newState.projectItemList[
              index
            ].descriptionError = action.errorList.data.description.join(" ");
          }
          this.setState(newState);
        });
    }
  };

  handleChangeList = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.projectItemList[index].edited = true;
    const value = e.target.value;
    newState.projectItemList[index][e.target.name] = value;
    this.setState(newState);
  };

  handleChangeMarkdown = (content: string, index: number) => {
    const newState = cloneDeep(this.state);
    newState.projectItemList[index].edited = true;
    newState.projectItemList[index]["description"] = content;
    this.setState(newState);
  };

  onClickCompleted = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const inputValue = e.target.value || 0;
    const newState = cloneDeep(this.state);
    newState.projectItemList[index].edited = true;
    newState.projectItemList[index][e.target.name] = inputValue;

    let completedCondition: boolean;
    if (this.props.itemType === "risk-items") {
      const status = this.props.impactList.find((x) => x.id === inputValue);
      completedCondition = status && status.name === "Mitigated";
    } else {
      const status = this.props.statusList.find((x) => x.id === inputValue);
      completedCondition = status && status.title === "Completed";
    }

    if (completedCondition) {
      newState.projectItemList[index].showCompleted = true;
      newState.projectItemList[index].completed_on = dateSetTimeZero(
        new Date()
      );
    }

    this.setState(newState);
  };

  enableEdit = (index: number, row: IProjectItem) => {
    if (!this.props.viewOnly && row.status_title !== "Completed") {
      const newState = cloneDeep(this.state);
      newState.projectItemList[index].edited = true;
      this.setState(newState);
    }
  };

  getActionItemData = () => {
    if (!this.state.projectItemList) {
      return null;
    }
  };

  setSort = (name: string) => {
    this.setState(
      (prevState) => {
        const sort = prevState.sorting.includes("-") ? name : `-${name}`;
        return {
          sorting: sort,
        };
      },
      () => {
        if (this.props.callbackSorting) {
          this.props.callbackSorting(this.state.sorting, this.props.sortingKey);
        }
      }
    );
  };

  getSortClass = (name: string) => {
    return (
      (this.state.sorting &&
        this.state.sorting.includes(name) &&
        (this.state.sorting.includes("-") ? "desc-order" : "asc-order")) ||
      ""
    );
  };

  render() {
    const projectItemList = this.state.projectItemList;
    const isRiskItem = this.props.itemType === "risk-items";
    const impactOptions = this.props.impactList
      ? this.props.impactList.map((s) => ({
          value: s.id,
          label: (
            <div className="field-section color-preview-option" title={""}>
              <div
                style={{
                  backgroundColor: s.color,
                  borderColor: s.color,
                }}
                className="left-column"
              />
              <div
                style={{
                  background: `${getConvertedColorWithOpacity(s.color)}`,
                }}
                className="text"
              >
                {s.name}
              </div>
            </div>
          ),
        }))
      : [];

    const getStatusOptions = (completedOn: string) =>
      this.props.statusList
        ? this.props.statusList.map((s) => ({
            value: s.id,
            label: (
              <div
                className="field-section color-preview-option"
                title={
                  completedOn &&
                  `Completed On : ${fromISOStringToFormattedDate(completedOn)}`
                }
              >
                <div
                  style={{
                    backgroundColor: s.color,
                    borderColor: s.color,
                  }}
                  className="left-column"
                />
                <div
                  style={{
                    background: `${getConvertedColorWithOpacity(s.color)}`,
                  }}
                  className="text"
                >
                  {s.title}
                </div>
              </div>
            ),
          }))
        : [];

    return (
      <div className="project-details-action-items">
        {projectItemList && projectItemList.length > 0 && !this.state.loading && (
          <div className="col-md-12 status-row heading">
            <label
              className={`col-md-4 field__label-label ${this.getSortClass(
                "description"
              )}`}
              onClick={(e) => this.setSort("description")}
            >
              Item Name
            </label>
            <label
              className={`col-md-3 field__label-label ${this.getSortClass(
                "owner_name"
              )}`}
              onClick={(e) => this.setSort("owner_name")}
            >
              Owner
            </label>
            <label
              className={`col-md-2 field__label-label ${this.getSortClass(
                isRiskItem ? "created_on" : "due_date"
              )}`}
              onClick={(e) =>
                this.setSort(isRiskItem ? "created_on" : "due_date")
              }
            >
              {isRiskItem ? "Entered On" : "Due Date"}
            </label>
            <label
              className={`col-md-2 field__label-label ${this.getSortClass(
                isRiskItem ? "impact" : "status_title"
              )}`}
              onClick={(e) =>
                this.setSort(isRiskItem ? "impact" : "status_title")
              }
            >
              {isRiskItem ? "Impact" : "Status"}
            </label>
            <label className="col-md-1" />
          </div>
        )}
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {projectItemList &&
          !this.state.loading &&
          projectItemList
            .sort((a, b) =>
              commonFunctions.sortList(a, b, "id", this.state.sorting)
            )
            .map((row, index) => {
              row.owner = row.owner || row.resource;
              return (
                <div
                  className={`col-md-12 status-row${
                    row.edited ? " edited" : ""
                  }${
                    row.showCompleted && !row.edited
                      ? " pointer-events-none"
                      : ""
                  }`}
                  key={index}
                >
                  {!isRiskItem &&
                    row.status_title !== "Completed" &&
                    moment(row.due_date) < moment() && (
                      <img
                        className="overdue-item"
                        alt=""
                        title="Overdue Item"
                        src={
                          "/assets/icons/clock-circular-gross-outline-of-time.svg"
                        }
                      />
                    )}
                  {!row.edited && (
                    <div
                      className="pd-action-item col-md-4 ql-editor"
                      onClick={() => {
                        this.enableEdit(index, row);
                      }}
                      dangerouslySetInnerHTML={{ __html: row.description }}
                    />
                  )}
                  {row.edited && (
                    <Input
                      field={{
                        label: "",
                        type: "CUSTOM",
                        value: "",
                        isRequired: false,
                      }}
                      width={4}
                      labelIcon={""}
                      name=""
                      customInput={
                        <ReactQuill
                          value={row.description || ""}
                          onChange={(e) => this.handleChangeMarkdown(e, index)}
                          modules={{
                            toolbar: [
                              [{ list: "ordered" }, { list: "bullet" }],
                            ],
                          }}
                          theme="snow"
                          className={`action-items-quill ${
                            row.descriptionError ? "ql-error" : ""
                          }`}
                        />
                      }
                      onChange={(e) => null}
                      className="action-item-editor"
                      placeholder={""}
                      error={
                        row.descriptionError && {
                          errorState: "error",
                          errorMessage: row.descriptionError,
                        }
                      }
                      disabled={this.props.viewOnly}
                    />
                  )}
                  <Input
                    field={{
                      label: "",
                      type: "PICKLIST",
                      value: row.owner ? row.owner : row.resource,
                      isRequired: false,
                      options: this.props.ownerOptions,
                    }}
                    width={3}
                    labelIcon={"info"}
                    name="owner"
                    onChange={(e) => this.handleChangeList(e, index)}
                    placeholder={row.edited ? "Select User" : ""}
                    className={`project-item-dropdown ${
                      row.status_title === "Completed"
                        ? "pointer-events-none"
                        : ""
                    } `}
                    disabled={
                      this.props.viewOnly ||
                      (isRiskItem && row.impact_name === "Mitigated")
                    }
                  />
                  {row.edited && (
                    <Input
                      field={{
                        label: "",
                        type: "DATE",
                        isRequired: false,
                        value: isRiskItem ? row.created_on : row.due_date,
                      }}
                      width={2}
                      name={isRiskItem ? "created_on" : "due_date"}
                      onChange={(e) => this.handleChangeList(e, index)}
                      placeholder="Date"
                      showTime={false}
                      showDate={true}
                      disabled={this.props.viewOnly}
                      disablePrevioueDates={!isRiskItem}
                    />
                  )}
                  {!row.edited && (
                    <div
                      className="row-date col-md-2"
                      onClick={() => {
                        this.enableEdit(index, row);
                      }}
                    >
                      {(!isRiskItem && row.due_date) ||
                      (isRiskItem && row.created_on)
                        ? fromISOStringToFormattedDate(
                            isRiskItem ? row.created_on : row.due_date,
                            "MMM DD, YYYY"
                          )
                        : "N.A."}
                    </div>
                  )}
                  <Input
                    field={{
                      label: "",
                      type: "PICKLIST",
                      value: isRiskItem ? row.impact : row.status,
                      isRequired: false,
                      options: isRiskItem
                        ? impactOptions
                        : getStatusOptions(row.completed_on),
                    }}
                    width={2}
                    labelIcon={"info"}
                    name={isRiskItem ? "impact" : "status"}
                    onChange={(e) => this.onClickCompleted(e, index)}
                    placeholder={`Select ${isRiskItem ? "Impact" : "Status"}`}
                    className="project-item-dropdown"
                    disabled={
                      this.props.viewOnly ||
                      (isRiskItem && row.impact_name === "Mitigated") ||
                      (!isRiskItem && row.status_title === "Completed")
                    }
                  />
                  {row.showCompleted && (
                    <Input
                      field={{
                        label: "Completed On",
                        type: "DATE",
                        isRequired: false,
                        value: row.completed_on,
                      }}
                      width={2}
                      name="completed_on"
                      onChange={(e) => this.handleChangeList(e, index)}
                      placeholder="Completed On"
                      className="completed-on-date"
                      showTime={false}
                      showDate={true}
                      disabled={this.props.viewOnly}
                      disablePrevioueDates={false}
                    />
                  )}
                  {!this.props.viewOnly && row.id && (
                    <SmallConfirmationBox
                      className="remove col-md-1"
                      onClickOk={() => this.onDeleteRowClick(index, row)}
                      text={"Item"}
                    />
                  )}
                  {(row.edited || !row.id) && (
                    <div className="row-action-btn">
                      <SquareButton
                        onClick={() => {
                          const newState = cloneDeep(this.state);
                          if (!row.id) {
                            newState.projectItemList.splice(index, 1);
                          } else {
                            this.getActionItemList();
                          }
                          this.setState(newState);
                        }}
                        content={"Cancel"}
                        bsStyle={"default"}
                      />
                      <SquareButton
                        onClick={() => this.onSaveRowClick(index, row)}
                        content={"Apply"}
                        bsStyle={"primary"}
                        disabled={
                          (!isRiskItem && (!row.status || !row.due_date)) ||
                          !row.owner ||
                          row.description.length < 12
                        }
                      />
                    </div>
                  )}
                </div>
              );
            })}
        {projectItemList &&
          projectItemList.length === 0 &&
          !this.state.loading && (
            <div className="col-md-12 status-row risk-edited">
              <div className="no-data-action-item col-md-11">
                No items available
              </div>
            </div>
          )}
        {projectItemList &&
          this.state.projectItemList.filter((x) => !x.id).length === 0 &&
          !this.props.viewOnly && (
            <div className="col-md-12 status-row risk-edited">
              <div
                onClick={this.addNew}
                className="add-new-action-item col-md-11"
              >
                {isRiskItem
                  ? "Add Risk Item"
                  : this.props.itemType === "action-items"
                  ? "Add Action Item"
                  : "Add Critical Path Item"}
              </div>
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  editActionItem: (projectId: number, data: any, api: string) =>
    dispatch(editActionItem(projectId, data, api)),
  postActionItem: (projectId: number, data: IProjectDetailItem, api: string) =>
    dispatch(postActionItem(projectId, data, api)),
  deleteActionItem: (projectId: number, ItemId: number, api: string) =>
    dispatch(deleteActionItem(projectId, ItemId, api)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectItems);
