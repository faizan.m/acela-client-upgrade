import React from "react";

import { cloneDeep, debounce } from "lodash";
import moment from "moment";
import { connect } from "react-redux";
import { ACTION_ITEM_FAILURE, ACTION_ITEM_SUCCESS, deleteActionItem, editActionItem, getActionItemList, GetAdditionalSetting, getCustomerTouchMeetingTemplate, getMeetingTemplates, getMeetingTemplateSetting, getProjectStatusByProjectID, MEETING_SUCCESS, postActionItem, saveMeetingDetails } from "../../../actions/pmo";
import { fetchProviderUsers } from "../../../actions/provider/user";
import SquareButton from "../../../components/Button/button";
import Input from "../../../components/Input/input";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import SMPopUp from "../../../components/SMPopup/smPopUp";
import Spinner from "../../../components/Spinner";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";
import { getConvertedColorWithOpacity } from "../../../utils/CommonUtils";
import { getURLBYMeetingType } from "./meetingDetails";

interface IMeetingListProps extends ICommonProps {
  getActionItemList: any;
  project?: any;
  postActionItem: any;
  deleteActionItem: any;
  fetchProviderUsers: any;
  providerUsers: any;
  editActionItem: any;
  api: any;
  getMeetingTemplates: any;
  user: any;
  saveMeetingDetails: any;
  GetAdditionalSetting: any;
  getCustomerTouchMeetingTemplate: any;
  getMeetingTemplateSetting: any;
  getProjectStatusByProjectID: any;
  parentCallback: any;
}

interface IMeetingListState {
  meetingList: any[];
  open: boolean;
  loading: boolean;
  impactList: any[];
  type: string;
  template: any;
  typeList: any[];
  templateList: any[];
  projectMeeting: IProjectMeeting;
  default_project_acceptance_markdown: string;
  default_project_acceptance_text: string;
  statusList: any;
  sorting: string;
}

class MeetingLists extends React.Component<
  IMeetingListProps,
  IMeetingListState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  private debouncedFetch;

  constructor(props: IMeetingListProps) {
    super(props);

    this.state = {
      meetingList: [],
      impactList: [
        { color: "#dba629", title: 'Pending' },
        { color: "#176f07", title: 'Completed' }],
      open: true,
      loading: false,
      type: '',
      template: '',
      typeList: [{ type: 'Status', isTemplate: true },
      { type: 'Customer Touch', isTemplate: false },
      { type: 'Internal Kickoff', isTemplate: true },
      { type: 'Kickoff', isTemplate: true },
      { type: 'Close Out', isTemplate: false },],
      templateList: [],
      projectMeeting: {
        name: '',
        schedule_start_datetime: moment(),
        schedule_end_datetime: moment().add(1, 'hours'),
        status: 'Pending',
        meeting_type: '',
        email_body: null,
        email_subject: null,
        email_body_text: "",
        project_acceptance: '',
        project_acceptance_email_subject: '',
        project_acceptance_markdown: '',
        project_acceptance_text: ''
      },
      default_project_acceptance_markdown: '',
      default_project_acceptance_text: '',
      statusList: [],
      sorting: "",
    };
    this.debouncedFetch = debounce(this.passObjectToParent, 1000);
  }
  componentDidMount() {
    this.setState({ loading: true })
    this.getActionItemList();
    this.getMeetingTemplateSetting()
    this.getProjectStatusByProjectID();
  }

  getMeetingTemplateSetting = async () => {
    const meetingMailTemplateResponse: any = await this.props.getMeetingTemplateSetting();
    const { results } = meetingMailTemplateResponse.response;

    const newState = cloneDeep(this.state);
    results.map((data) => {
      if (data.meeting_type === "Customer Touch") {
        newState.projectMeeting.email_body = data.email_body_markdown;
        newState.projectMeeting.email_subject = data.email_subject;
        newState.projectMeeting.email_body_text = data.email_body_text
      }
      if (data.meeting_type === "Close Out") {
        newState.projectMeeting.project_acceptance_email_subject = data.email_subject;
        newState.projectMeeting.project_acceptance_markdown = data.email_body_markdown;
        newState.projectMeeting.project_acceptance_text = data.email_body_text;
      }

    });

    this.setState(newState);

  }

  getCustomerTouchMeetingTemplate = async () => {
    const meetingTemplate: any = await this.props.getCustomerTouchMeetingTemplate();
    const { email_subject, message_html, message_markdown } = meetingTemplate.response;
    const newState = cloneDeep(this.state);
    newState.projectMeeting.email_body = message_markdown;
    newState.projectMeeting.email_subject = email_subject;
    newState.projectMeeting.email_body_text = message_html
    this.setState(newState);
  }
  getActionItemList = () => {
    this.props.getActionItemList(this.props.project.id, this.props.api).then(
      action => {
        this.setState({ loading: false, meetingList: action.response })
        this.props.parentCallback(action.response);
      }
    )
  }

  onDeleteRowClick = (index, rowData) => {
    const newState = cloneDeep(this.state);
    this.setState(newState);
    let url = `${getURLBYMeetingType(rowData.meeting_type)}`;
    this.props.deleteActionItem(this.props.project.id, rowData.id, url).then(
      action => {
        if (action.type === ACTION_ITEM_SUCCESS) {
          this.getActionItemList();
        }
        if (action.type === ACTION_ITEM_FAILURE) {
          newState.meetingList[index].descriptionError = `Couldn't delete`;
        }
        this.setState(newState);
      }
    )
  };

  onSaveRowClick = (e, index, rowData) => {
    const newState = cloneDeep(this.state);
    if (rowData.id) {
      this.props.editActionItem(this.props.project.id, rowData, this.props.api).then(
        action => {
          if (action.type === ACTION_ITEM_SUCCESS) {
            newState.meetingList[index].edited = false;
            newState.meetingList[index].descriptionError = '';
          }
          if (action.type === ACTION_ITEM_FAILURE) {
            newState.meetingList[index].edited = true;
            newState.meetingList[index].descriptionError = action.errorList.data.description.join(' ');
          }
          this.setState(newState);
        }
      )
    } else {
      this.props.postActionItem(this.props.project.id, rowData, this.props.api).then(
        action => {
          if (action.type === ACTION_ITEM_SUCCESS) {
            newState.meetingList[index].edited = false;
            newState.meetingList[index].descriptionError = '';
            newState.meetingList[index] = action.response;
          }
          if (action.type === ACTION_ITEM_FAILURE) {
            newState.meetingList[index].edited = true;
            newState.meetingList[index].descriptionError = action.errorList.data.description.join(' ');
          }
          this.setState(newState);
        }
      )
    }
  };
  
  handleChangeList = (e, index) => {
    const newState = cloneDeep(this.state);
    newState.meetingList[index].edited = true;
    newState.meetingList[index][e.target.name] = e.target.value;
    this.setState(newState);
    this.debouncedFetch(newState.meetingList);
  };
  enableEdit = (e, index, row) => {
    if (!row.is_archived) {
      const newState = cloneDeep(this.state);
      newState.meetingList[index].edited = true;
      this.setState(newState);
    }
  };

  passObjectToParent = (list) => {
    const mappedObject = {};
    list.map((data, i) => {
      mappedObject[data.name] = data;
    });
  };

  isValid = () => {
    const error = {
      subscription_category_id: { ...MeetingLists.emptyErrorState },
      device_category_row: { ...MeetingLists.emptyErrorState },
    };
    let isValid = true;

    if (this.state.meetingList && this.state.meetingList.length > 0) {
      this.state.meetingList.map((e) => {
        if (!e.connectwise_shipper || !e.name) {
          error.device_category_row.errorState = "error";
          // tslint:disable-next-line:max-line-length
          error.device_category_row.errorMessage = `Please enter required fields.`;
          isValid = false;
        }
      });
    }

    return isValid;
  };

  getProjectStatusByProjectID = () => {
    this.props.getProjectStatusByProjectID(this.props.project.id).then(
      action => {
        this.setState({ loading: false, statusList: action.response })
      }
    )
  }
  handleChange = (event: any) => {
    const newState: IMeetingListState = cloneDeep(this.state);
    const status = this.state.statusList && this.state.statusList.find(f => f.is_default);
    (newState[event.target.name] as string) = event.target.value;
    const selectedTemplate = this.state.templateList.find(t => t.id === event.target.value);
    newState.projectMeeting.agenda_topics = selectedTemplate.agenda_topics;
    let action_items = selectedTemplate.action_items;
    action_items = action_items.map(x => {
      return {
        "description": x,
        owner: this.props.project.project_manager_id,
        status: status && status.id,
        due_date: moment(new Date()).add(7, 'days')
      }
    })
    newState.projectMeeting.action_items = action_items;
    newState.projectMeeting.name = selectedTemplate.template_name;
    this.setState(newState);
  };
  handleChangeType = (event: any) => {
    let url = '';
    const newState: IMeetingListState = cloneDeep(this.state);
    (newState[event.target.name] as string) = event.target.value;
    if (['Kickoff', 'Internal Kickoff'].includes(event.target.value)) {
      url = 'settings/meeting-templates?meeting_type=Kickoff';
      this.getMeetingTemplates(url)
    } else if (['Status'].includes(event.target.value)) {
      url = 'settings/meeting-templates?meeting_type=Status';
      this.getMeetingTemplates(url)
    }
    this.setState(newState);
  };
  getMeetingTemplates = (url) => {
    this.setState({ loading: true })
    this.props.getMeetingTemplates(url).then(
      action => {
        this.setState({
          loading: false, templateList: action.response
        })
      }
    )
  }
  onSaveClick = (event: any) => {
    const projectID = this.props.match.params.id;
    let projectMeeting = this.state.projectMeeting;
    projectMeeting.owner =  this.props.project.project_manager_id
    projectMeeting.name = projectMeeting.name ? projectMeeting.name : this.state.type;
    if (!projectMeeting.agenda_topics || projectMeeting.agenda_topics && projectMeeting.agenda_topics.length === 0) {
      projectMeeting.agenda_topics = null;
    } else {
        projectMeeting.agenda_topics = projectMeeting.agenda_topics.map((x,index)=>{ return{...x, id:index}})
    }
    this.setState({ loading: true })
    projectMeeting.meeting_type = this.state.type;
    if (this.state.type === 'Internal Kickoff') {
      projectMeeting.is_internal = true;
    }
    if (projectMeeting.action_items) {
      projectMeeting.action_items.map(item => {
        this.props.postActionItem(this.props.project.id, item, 'action-items')
      })
    }
    if (this.state.type != 'Close Out') {
      delete projectMeeting.project_acceptance;
      delete projectMeeting.project_acceptance_email_subject;
    }
    if (this.state.type != 'Customer Touch') {
      projectMeeting.email_subject = projectMeeting.project_acceptance_email_subject;
      delete projectMeeting.email_body;
    }
    let url = `${getURLBYMeetingType(this.state.type)}`;
    const meetingID = this.props.match.params.meetingID;

    if (meetingID && meetingID !== "0") {
      url = `${getURLBYMeetingType(this.state.type)}/${this.props.match.params.meetingID}`;
    }
    this.props.saveMeetingDetails(projectID, projectMeeting, url).then(
      action => {
        if (action.type === MEETING_SUCCESS) {
          const query = new URLSearchParams(this.props.location.search);
          const AllProject = query.get('AllProject') === 'true' ? true : false;
          this.props.history.push(`/Project/${this.props.project.id}/meeting/${action.response.id}?type=${this.state.type}&template=${this.state.template}&AllProject=${AllProject}`)
        }
        this.setState({ loading: false })
      }
    );
  };

  hideMeetingType = (meeting_type) => {
    let show = true;

    if (['Customer Touch', 'Kickoff', 'Internal Kickoff'].includes(meeting_type)) {
      show = this.state.meetingList.filter(m => m.meeting_type === meeting_type).length > 0 ? false : true;
    }
    return show;
  }
  setSort = (name)=>{
    this.setState({ sorting: this.state.sorting.includes('-') ?  name : `-${name}`  });
  }
   getSortClass =(name)=>{
    return this.state.sorting &&  this.state.sorting.includes(name) &&(this.state.sorting.includes('-')? 'desc-order' : 'asc-order') || '';
   }
  render() {
    const meetingList = this.state.meetingList;
    return (
      <div className="project-details-action-items meeting-list">
        {
          meetingList && meetingList.length > 0 && !this.state.loading && (
            <div className="col-md-12 status-row" >

              <label className={`col-md-4 field__label-label ${this.getSortClass('name')}`}  onClick={(e) =>this.setSort('name')}>
                Item Name
              </label>
              <label className={`col-md-3 field__label-label ${this.getSortClass('meeting_type')}`}  onClick={(e) =>this.setSort('meeting_type')}>
                Type
              </label>
              <label className={`col-md-2 field__label-label ${this.getSortClass('status')}`}  onClick={(e) =>this.setSort('status')}>
                Project Status
              </label>
              <label className={`col-md-3 field__label-label ${this.getSortClass('schedule_end_datetime')}`}  onClick={(e) =>this.setSort('schedule_end_datetime')}>
                Date Scheduled
              </label>

            </div>)}
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {
          meetingList && !this.state.loading &&
          meetingList
          .sort((a, b) =>
          commonFunctions.sortList(a, b, "id", this.state.sorting)
          ).map((row, index) => {
            const color = this.state.impactList.find(i => i.title === row.status).color;

            return (
              <div className={`col-md-12 status-row ${row.edited ? 'edited' : ''}`} key={index}>
                {
                  !row.edited &&
                  <div className="description  col-md-4 user"
                    onClick={e => {
                      this.enableEdit(e, index, row)
                    }}>{row.name}</div>
                }
                {
                  row.edited &&
                  <Input
                    field={{
                      label: "",
                      type: "TEXT",
                      value: row.name,
                      isRequired: false,
                    }}
                    width={4}
                    labelIcon={'info'}
                    name="name"
                    onChange={(e) => this.handleChangeList(e, index)}
                    placeholder={`Enter name`}
                  />
                }
                {
                  <div className="description not-editable-status col-md-3 user"
                    onClick={e => {
                      this.enableEdit(e, index, row)
                    }}
                  >{row.meeting_type}</div>
                }
                {
                  (
                    <div className="field-section not-editable-status color-preview col-md-2"
                      title={row.status}
                    >
                      <div style={{ backgroundColor: color, borderColor: color }} className="left-column" />
                      <div style={{ background: `${getConvertedColorWithOpacity(color)}` }} className="text">
                        {row.status}
                      </div>
                    </div>
                  )
                }
                {
                  row.edited &&
                  (
                    <Input
                      field={{
                        label: '',
                        type: "DATE",
                        isRequired: false,
                        value: row.schedule_end_datetime
                      }}
                      width={2}
                      name="schedule_end_datetime"
                      onChange={(e) => this.handleChangeList(e, index)}
                      placeholder="Date"
                      showTime={false}
                      showDate={true}
                      disablePrevioueDates={true}
                    />
                  )
                }
                {
                  !row.edited &&
                  (
                    <div className="row-date col-md-2"
                      onClick={e => {
                        this.enableEdit(e, index, row)
                      }}>
                      {row.schedule_end_datetime ? fromISOStringToFormattedDate(row.schedule_end_datetime, 'MMM DD, YYYY') : 'N.A.'}
                    </div>)
                }
                {
                  !row.is_archived && !row.edited && row.id && (
                    <div className="">
                      <img
                        className={'d-pointer icon-remove edit-png'}
                        alt=""
                        src={'/assets/icons/edit.png'}
                        title={'Edit in details'}
                        onClick={() => {
                          const query = new URLSearchParams(this.props.location.search);
                          const AllProject = query.get('AllProject') === 'true' ? true : false;
                          this.props.history.push(`/Project/${this.props.project.id}/meeting/${row.id}?type=${row.meeting_type}&AllProject=${AllProject}`)
                        }}
                      />
                    </div>)
                }
                {
                  !row.is_archived && row.id && !row.edited && (
                    <SmallConfirmationBox
                      className='remove-meeting'
                      onClickOk={() => this.onDeleteRowClick(index, row)}
                      text={'Meeting'}
                    />)
                }
                {
                  row.is_archived && row.id && (
                    <div className="archived">
                      <img
                        className={'d-pointer icon-remove info-png'}
                        alt=""
                        title={'Archived Meeting'}
                        src={'/assets/icons/archive-filled-box.svg'}
                        onClick={() => {
                          const query = new URLSearchParams(this.props.location.search);
                          const AllProject = query.get('AllProject') === 'true' ? true : false;
                          this.props.history.push(`/Project/${this.props.project.id}/meeting/${row.id}?type=${row.meeting_type}&view=true&AllProject=${AllProject}`)
                        }}
                      />
                    </div>)
                }
                {
                  row.edited && (
                    <div className="row-action-btn-meeting-list">
                      <SquareButton
                        onClick={() => {
                          const newState = cloneDeep(this.state);
                          if (!row.id) {
                            newState.meetingList.splice(index, 1);
                          } else {
                            this.getActionItemList();
                          }
                          this.setState(newState);
                        }}
                        content={'Cancel'}
                        bsStyle={"default"}
                      />
                      <SquareButton
                        onClick={(e) => this.onSaveRowClick(e, index, row)}
                        content={'Apply'}
                        bsStyle={"primary"}
                      />
                    </div>)
                }

              </div>
            )
          }

          )
        }
        {
          meetingList && meetingList.length === 0 && !this.state.loading &&
          <div className="col-md-12 status-row risk-edited" >

            <div
              className="no-data-action-item col-md-11"
            > No meetings available</div>
          </div>
        }
        {
          meetingList && meetingList.filter(x => !x.id).length === 0 &&
          <div className="col-md-12 status-row risk-edited" >
            <SMPopUp
              onClickOk={e => this.onSaveClick(e)}
              okText="Save"
              isValidForm={this.state.type !== ''}
              clickableSection={
                <div
                  className="add-new-action-item col-md-11"
                >Add Meetings </div>
              }
              className="meeting-save-popup"
            >
              <div className="box">
                <Input
                  field={{
                    label: 'Type',
                    type: "PICKLIST",
                    value: this.state.type,
                    options: this.state.typeList
                      .filter(m => this.hideMeetingType(m.type))
                      .map(s => ({
                        value: s.type,
                        label: s.type,
                      })),
                    isRequired: false,
                  }}
                  width={12}
                  multi={false}
                  name="type"
                  onChange={e => this.handleChangeType(e)}
                  placeholder={`Select`}
                />
                {
                  this.state.type && this.state.typeList.find(t => t.type === this.state.type).isTemplate &&

                  <Input
                    field={{
                      value: this.state.template,
                      label: 'Template',
                      type: "PICKLIST",
                      options: this.state.templateList.map(c => ({
                        value: c.id,
                        label: c.template_name,
                      })),
                      isRequired: false,
                    }}
                    width={12}
                    name="template"
                    onChange={e => this.handleChange(e)}
                    placeholder="Select Template"
                    showTime={false}
                    showDate={true}
                  />}
              </div>
            </SMPopUp>
          </div>
        }

      </div >
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  providerUsers: state.providerUser.providerUsers,
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  getActionItemList: (projectId: number, api: string) => dispatch(getActionItemList(projectId, api)),
  editActionItem: (projectId: number, data: any, api: string) => dispatch(editActionItem(projectId, data, api)),
  postActionItem: (projectId: number, data: any, api: string) => dispatch(postActionItem(projectId, data, api)),
  deleteActionItem: (projectId: number, ItemId: any, api: string) => dispatch(deleteActionItem(projectId, ItemId, api)),
  fetchProviderUsers: (params?: IServerPaginationParams) =>
    dispatch(fetchProviderUsers(params)),
  getMeetingTemplates: (url: string) =>
    dispatch(getMeetingTemplates(url)),
  saveMeetingDetails: (projectID: number, data: any, api: string) => dispatch(saveMeetingDetails(projectID, data, api)),
  GetAdditionalSetting: () => dispatch(GetAdditionalSetting()),
  getCustomerTouchMeetingTemplate: () => dispatch(getCustomerTouchMeetingTemplate()),
  getMeetingTemplateSetting: () => dispatch(getMeetingTemplateSetting()),
  getProjectStatusByProjectID: (projectId: number) => dispatch(getProjectStatusByProjectID(projectId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(MeetingLists);

