import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  getSOW,
  FETCH_SOW_SUCCESS,
  FETCH_SOWS_FAILURE,
} from "../../../../actions/sow";
import { addErrorMessage } from "../../../../actions/appState";
import { useProject } from "../projectContext";

interface SowProjectDetailsProps {
  addErrorMessage: TShowErrorMessage;
  getSOW: (sowId: number) => Promise<any>;
}

const SowProjectDetails: React.FC<SowProjectDetailsProps> = (props) => {
  const { project } = useProject();
  const [sow, setSow] = useState<ISoW>();
  const [loading, setLoading] = useState<boolean>(false);

  useEffect(() => {
    if (project.sow_document) fetchSowDocument(project.sow_document);
  }, [project.sow_document]);

  const fetchSowDocument = (id: number) => {
    setLoading(true);
    props
      .getSOW(id)
      .then((action) => {
        if (action.type === FETCH_SOW_SUCCESS) {
          setSow(action.response);
        } else if (action.type === FETCH_SOWS_FAILURE) {
          props.addErrorMessage("Error fetching linked sow!");
        }
      })
      .finally(() => setLoading(false));
  };

  const projectDetails = sow
    ? sow.json_config && sow.json_config.project_details
    : null;

  // add styling and collapsable section here
  return projectDetails ? (
    <div className="sow-project-details-section">
      <div className="ql-container ql-snow">
        <h4>{projectDetails.task_deliverables.label}</h4>
        <div
          className="ql-editor"
          dangerouslySetInnerHTML={{
            __html: projectDetails.task_deliverables.value,
          }}
        />
      </div>
      <div className="ql-container ql-snow">
        <h4>{projectDetails.project_assumptions.label}</h4>
        <div
          className="ql-editor"
          dangerouslySetInnerHTML={{
            __html: projectDetails.project_assumptions.value,
          }}
        />
      </div>
      <div className="ql-container ql-snow">
        <h4>{projectDetails.out_of_scope.label}</h4>
        <div
          className="ql-editor"
          dangerouslySetInnerHTML={{
            __html: projectDetails.out_of_scope.value,
          }}
        />
      </div>
    </div>
  ) : (
    <div className="no-project-details">
      {loading ? "Loading..." : "No SoW Document linked to this project"}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getSOW: (sowId: number) => dispatch(getSOW(sowId)),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SowProjectDetails);
