import React, { useEffect, useMemo, useState } from "react";
import moment from "moment";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import ReactQuill from "react-quill";
import {
  editActionItem,
  deleteActionItem,
  getActionItemList,
  ACTION_ITEM_SUCCESS,
  ACTION_ITEM_FAILURE,
} from "../../../../actions/pmo";
import Spinner from "../../../../components/Spinner";
import Input from "../../../../components/Input/input";
import UsershortInfo from "../../../../components/UserImage";
import SquareButton from "../../../../components/Button/button";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import {
  dateSetTimeZero,
  fromISOStringToFormattedDate,
} from "../../../../utils/CalendarUtil";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { getConvertedColorWithOpacity } from "../../../../utils/CommonUtils";
import { useProject } from "../projectContext";

interface IProjectItemProps {
  getActionItemList: (projectId: number, api: string) => Promise<any>;
  editActionItem: (projectId: number, data: any, api: string) => Promise<any>;
  deleteActionItem: (
    projectId: number,
    ItemId: number,
    api: string
  ) => Promise<any>;
}

interface IProjectItem extends IProjectDetailItem {
  edited?: boolean;
  descriptionError?: string;
  showCompleted?: boolean;
  completed_on?: any;
  type?: string;
}

const OverdueItems: React.FC<IProjectItemProps> = (props) => {
  const [sorting, setSorting] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);
  const [overdueItems, setOverdueItems] = useState<IProjectItem[]>([]);
  const {
    project,
    projectTeam,
    projectStatusList,
    customerContacts,
    additionalContacts,
  } = useProject();

  const ownerOptions = useMemo(() => {
    if (projectTeam && customerContacts && additionalContacts) {
      const usersList: {
        user_id: string | number;
        name: string;
        profile_url: string;
      }[] = [
        ...projectTeam.map((el) => ({
          name: el.full_name,
          profile_url: el.profile_url,
          user_id: el.id,
        })),
        ...customerContacts.map((el) => ({
          name: el.name,
          profile_url: el.profile_url,
          user_id: el.user_id,
        })),
        ...additionalContacts.map((el) => {
          return {
            name: el.name,
            user_id: el.id,
            profile_url: "",
          };
        }),
      ];
      return usersList.map((el) => ({
        value: el.user_id,
        label: (
          <UsershortInfo
            name={el.name}
            url={el.profile_url}
            className="inside-box"
          />
        ),
      }));
    }
    return [];
  }, [projectTeam, customerContacts, additionalContacts]);

  useEffect(() => {
    fetchActionAndCriticalItems();
  }, []);

  const fetchActionAndCriticalItems = () => {
    setLoading(true);
    Promise.all([
      props.getActionItemList(project.id, "action-items"),
      props.getActionItemList(project.id, "critical-path-items"),
    ])
      .then(([actionRes, criticalRes]) => {
        if (
          actionRes.type === ACTION_ITEM_SUCCESS &&
          criticalRes.type === ACTION_ITEM_SUCCESS
        ) {
          const actionItems: IProjectDetailItem[] = actionRes.response;
          const criticalPathItems: IProjectDetailItem[] = criticalRes.response;
          const overdueItems: IProjectItem[] = [
            ...actionItems.map((el) => ({
              ...el,
              type: "action-items",
            })),
            ...criticalPathItems.map((el) => ({
              ...el,
              type: "critical-path-items",
            })),
          ].filter(
            (el) =>
              el.status_title !== "Completed" && moment(el.due_date) < moment()
          );
          setOverdueItems(overdueItems);
        }
      })
      .finally(() => setLoading(false));
  };

  const onDeleteRowClick = (index: number, rowData: IProjectItem) => {
    const newState = cloneDeep(overdueItems);
    setOverdueItems(newState);
    props
      .deleteActionItem(project.id, rowData.id, rowData.type)
      .then((action) => {
        if (action.type === ACTION_ITEM_SUCCESS) {
          fetchActionAndCriticalItems();
        }
        if (action.type === ACTION_ITEM_FAILURE) {
          newState[index].descriptionError = `Couldn't delete`;
          setOverdueItems(newState);
        }
      });
  };

  const onSaveRowClick = (index: number, rowData: IProjectItem) => {
    const items = cloneDeep(overdueItems);
    props.editActionItem(project.id, rowData, rowData.type).then((action) => {
      if (action.type === ACTION_ITEM_SUCCESS) {
        if (
          !items[index].showCompleted &&
          moment(items[index].due_date) < moment()
        ) {
          items[index].edited = false;
          items[index].descriptionError = "";
          items[index] = { ...action.response, type: rowData.type };
        } else items.splice(index, 1);

        setOverdueItems(items);
      }
      if (action.type === ACTION_ITEM_FAILURE) {
        items[index].edited = true;
        items[index].descriptionError = action.errorList.data.description.join(
          " "
        );
        setOverdueItems(items);
      }
    });
  };

  const handleChangeList = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const list = cloneDeep(overdueItems);
    list[index].edited = true;
    list[index][e.target.name] = e.target.value;
    setOverdueItems(list);
  };

  const handleChangeMarkdown = (content: string, index: number) => {
    const list = cloneDeep(overdueItems);
    list[index].edited = true;
    list[index].description = content;
    setOverdueItems(list);
  };

  const onClickCompleted = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const inputValue = e.target.value || 0;
    const list = cloneDeep(overdueItems);
    list[index].edited = true;
    list[index][e.target.name] = inputValue;

    const status = projectStatusList.find((x) => x.id === inputValue);
    if (status && status.title === "Completed") {
      list[index].showCompleted = true;
      list[index].completed_on = dateSetTimeZero(new Date());
    }
    setOverdueItems(list);
  };

  const enableEdit = (index: number, row: IProjectItem) => {
    if (row.status_title !== "Completed") {
      const list = cloneDeep(overdueItems);
      list[index].edited = true;
      setOverdueItems(list);
    }
  };

  const setSort = (name: string) => {
    setSorting((prev) => (prev.includes("-") ? name : `-${name}`));
  };

  const getSortClass = (name: string) => {
    return (
      (sorting &&
        sorting.includes(name) &&
        (sorting.includes("-") ? "desc-order" : "asc-order")) ||
      ""
    );
  };

  const getStatusOptions = (completedOn: string) =>
    projectStatusList
      ? projectStatusList.map((s) => ({
          value: s.id,
          label: (
            <div
              className="field-section color-preview-option"
              title={
                completedOn &&
                `Completed On : ${fromISOStringToFormattedDate(completedOn)}`
              }
            >
              <div
                style={{
                  backgroundColor: s.color,
                  borderColor: s.color,
                }}
                className="left-column"
              />
              <div
                style={{
                  background: `${getConvertedColorWithOpacity(s.color)}`,
                }}
                className="text"
              >
                {s.title}
              </div>
            </div>
          ),
        }))
      : [];

  return (
    <div className="project-details-action-items past-due-items">
      {overdueItems.length > 0 && !loading && (
        <div className="col-md-12 status-row heading">
          <label
            className={getSortClass("type")}
            onClick={() => setSort("type")}
          />
          <label
            className={`col-md-4 field__label-label ${getSortClass(
              "description"
            )}`}
            onClick={(e) => setSort("description")}
          >
            Item Name
          </label>
          <label
            className={`col-md-3 field__label-label ${getSortClass(
              "owner_name"
            )}`}
            onClick={(e) => setSort("owner_name")}
          >
            Owner
          </label>
          <label
            className={`col-md-2 field__label-label ${getSortClass(
              "due_date"
            )}`}
            onClick={(e) => setSort("due_date")}
          >
            Due Date
          </label>
          <label
            className={`col-md-2 field__label-label ${getSortClass(
              "status_title"
            )}`}
            onClick={(e) => setSort("status_title")}
          >
            Status
          </label>
          <label className="col-md-1" />
        </div>
      )}
      {loading && (
        <div className="loader">
          <Spinner show={true} />
        </div>
      )}
      {!loading &&
        overdueItems
          .sort((a, b) => commonFunctions.sortList(a, b, "id", sorting))
          .map((row, index) => {
            row.owner = row.owner || row.resource;
            return (
              <div
                className={`col-md-12 status-row${row.edited ? " edited" : ""}${
                  row.showCompleted && !row.edited ? " pointer-events-none" : ""
                }`}
                key={index}
              >
                {row.type === "action-items" && (
                  <img
                    className="overdue-item"
                    alt=""
                    title="Action Item"
                    src={"/assets/icons/tick.svg"}
                  />
                )}
                {row.type === "critical-path-items" && (
                  <img
                    className="overdue-item"
                    alt=""
                    title="Critical Path Item"
                    src={"/assets/new-icons/warning_notification.svg"}
                  />
                )}

                {!row.edited && (
                  <div
                    className="pd-action-item col-md-4 ql-editor"
                    onClick={() => {
                      enableEdit(index, row);
                    }}
                    dangerouslySetInnerHTML={{ __html: row.description }}
                  />
                )}
                {row.edited && (
                  <Input
                    field={{
                      label: "",
                      type: "CUSTOM",
                      value: "",
                      isRequired: false,
                    }}
                    width={4}
                    labelIcon={""}
                    name=""
                    customInput={
                      <ReactQuill
                        value={row.description || ""}
                        onChange={(e) => handleChangeMarkdown(e, index)}
                        modules={{
                          toolbar: [[{ list: "ordered" }, { list: "bullet" }]],
                        }}
                        theme="snow"
                        className={`action-items-quill ${
                          row.descriptionError ? "ql-error" : ""
                        }`}
                      />
                    }
                    onChange={(e) => null}
                    placeholder={""}
                    className="action-item-editor"
                    error={
                      row.descriptionError && {
                        errorState: "error",
                        errorMessage: row.descriptionError,
                      }
                    }
                  />
                )}
                <Input
                  field={{
                    label: "",
                    type: "PICKLIST",
                    value: row.owner ? row.owner : row.resource,
                    isRequired: false,
                    options: ownerOptions,
                  }}
                  width={3}
                  labelIcon={"info"}
                  name="owner"
                  onChange={(e) => handleChangeList(e, index)}
                  placeholder={row.edited ? "Select User" : ""}
                  className={`project-item-dropdown ${
                    row.status_title === "Completed"
                      ? "pointer-events-none"
                      : ""
                  } `}
                />
                {row.edited && (
                  <Input
                    field={{
                      label: "",
                      type: "DATE",
                      isRequired: false,
                      value: row.due_date,
                    }}
                    width={2}
                    name={"due_date"}
                    onChange={(e) => handleChangeList(e, index)}
                    placeholder="Date"
                    showTime={false}
                    showDate={true}
                    disablePrevioueDates={true}
                  />
                )}
                {!row.edited && (
                  <div
                    className="row-date col-md-2"
                    onClick={() => {
                      enableEdit(index, row);
                    }}
                  >
                    {row.due_date
                      ? fromISOStringToFormattedDate(
                          row.due_date,
                          "MMM DD, YYYY"
                        )
                      : "N.A."}
                  </div>
                )}
                <Input
                  field={{
                    label: "",
                    type: "PICKLIST",
                    value: row.status,
                    isRequired: false,
                    options: getStatusOptions(row.completed_on),
                  }}
                  width={2}
                  labelIcon={"info"}
                  name={"status"}
                  onChange={(e) => onClickCompleted(e, index)}
                  placeholder={`Select ${"Status"}`}
                  className="project-item-dropdown"
                />
                {row.showCompleted && (
                  <Input
                    field={{
                      label: "Completed On",
                      type: "DATE",
                      isRequired: false,
                      value: row.completed_on,
                    }}
                    width={2}
                    name="completed_on"
                    className="completed-on-date"
                    onChange={(e) => handleChangeList(e, index)}
                    placeholder="Completed On"
                    showTime={false}
                    showDate={true}
                    disablePrevioueDates={false}
                  />
                )}
                {row.id && (
                  <SmallConfirmationBox
                    className="remove col-md-1"
                    onClickOk={() => onDeleteRowClick(index, row)}
                    text={"Item"}
                  />
                )}
                {(row.edited || !row.id) && (
                  <div className="row-action-btn">
                    <SquareButton
                      onClick={fetchActionAndCriticalItems}
                      content={"Cancel"}
                      bsStyle={"default"}
                    />
                    <SquareButton
                      onClick={() => onSaveRowClick(index, row)}
                      content={"Apply"}
                      bsStyle={"primary"}
                      disabled={
                        !row.status ||
                        !row.due_date ||
                        !row.owner ||
                        row.description.length < 12
                      }
                    />
                  </div>
                )}
              </div>
            );
          })}
      {overdueItems && overdueItems.length === 0 && !loading && (
        <div className="col-md-12 status-row risk-edited">
          <div className="no-data-action-item col-md-11">
            No items available
          </div>
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  editActionItem: (projectId: number, data: any, api: string) =>
    dispatch(editActionItem(projectId, data, api)),
  deleteActionItem: (projectId: number, ItemId: number, api: string) =>
    dispatch(deleteActionItem(projectId, ItemId, api)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OverdueItems);
