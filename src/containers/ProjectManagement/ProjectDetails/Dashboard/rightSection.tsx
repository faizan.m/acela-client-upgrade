import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import moment, { Moment } from "moment";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  getProjectBoardList,
  getProjectPhasesByID,
  updateProjectDetails,
  PROJECT_DETAILS_UPDATE_FAILURE,
  PROJECT_DETAILS_UPDATE_SUCCESS,
  FETCH_PROJECT_BOARD_LIST_SUCCESS,
  PROJECT_PHASES_SUCCESS,
} from "../../../../actions/pmo";
import {
  downloadSOWDocumnet,
  downloadServiceDetail,
  DOWNLOAD_SOW_FAILURE,
  DOWNLOAD_SOW_SUCCESS,
  PREVIEW_SERVICE_DETAIL_FAILURE,
  PREVIEW_SERVICE_DETAIL_SUCCESS,
} from "../../../../actions/sow";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../../components/Button/deleteButton";
import {
  fromISOStringToFormattedDate,
  fromISOStringToFormattedDateCW,
} from "../../../../utils/CalendarUtil";
import { getConvertedColorWithOpacity } from "../../../../utils/CommonUtils";
import ProjectDocs from "./projectDocs";
import { useProject } from "../projectContext";

interface OverallStatusProps {
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getProjectBoardList: () => Promise<any>;
  updateProjectDetails: (
    data: Partial<IProjectDetails>,
    projectID: number
  ) => Promise<any>;
}

interface PhaseMilestoneProps {
  fetchPhases: (projectId: number) => Promise<any>;
}

interface RightSectionProps extends OverallStatusProps, PhaseMilestoneProps {
  previewSowDocument: (sowId: number) => Promise<any>;
  previewSowServiceDetail: (sowId: number) => Promise<any>;
}

const PhaseMilestone: React.FC<PhaseMilestoneProps> = (props) => {
  const [openPhases, setPhases] = useState<ProjectPhaseParent[]>([]);
  const { project } = useProject();
  useEffect(() => {
    fetchPendingPhases();
  }, []);

  const fetchPendingPhases = () => {
    props.fetchPhases(project.id).then((action) => {
      if (action.type === PROJECT_PHASES_SUCCESS) {
        setPhases(action.response);
      }
    });
  };

  const upcomingPhase: ProjectPhaseParent = useMemo(() => {
    const currentTime = moment();
    let upcomingPhase: ProjectPhaseParent = null;
    let phaseCompletionDate: Moment = null;

    openPhases
      .filter((el) => el.estimated_completion_date)
      .forEach((phase) => {
        const phaseStartDate = moment(phase.estimated_completion_date);
        if (
          phaseStartDate.isAfter(currentTime) &&
          (!upcomingPhase || phaseStartDate.isBefore(phaseCompletionDate))
        ) {
          upcomingPhase = phase;
          phaseCompletionDate = phaseStartDate;
        }
      });

    return upcomingPhase;
  }, [openPhases]);

  return (
    <div className="upcoming-phase-milestone">
      <div className="status-name">
        <label>Current Milestone:</label>
        <span>{upcomingPhase ? upcomingPhase.phase.title : "N/A"}</span>
      </div>
      {upcomingPhase && (
        <div
          className="status-box"
          style={{
            backgroundColor: upcomingPhase.status && upcomingPhase.status.color,
          }}
        >
          <div className="name">
            {upcomingPhase.status && upcomingPhase.status.title}
          </div>
          <div className="date">
            {upcomingPhase.estimated_completion_date
              ? fromISOStringToFormattedDateCW(
                  upcomingPhase.estimated_completion_date,
                  "MMM DD, YYYY"
                )
              : "TBD"}
          </div>
        </div>
      )}
    </div>
  );
};

const OverallStatus: React.FC<OverallStatusProps> = (props) => {
  const { project, updateProject } = useProject();
  const [loading, setLoading] = useState<boolean>(false);
  const [overallStatus, setOverallStatus] = useState<number>(
    project.overall_status_id
  );
  const isOverallStatusChanged = overallStatus !== project.overall_status_id;
  const [statusList, setStatusList] = useState<IPickListOptions[]>([]);

  useEffect(() => {
    fetchStatusList();
  }, []);

  const fetchStatusList = () => {
    setLoading(true);
    props
      .getProjectBoardList()
      .then((action) => {
        if (action.type === FETCH_PROJECT_BOARD_LIST_SUCCESS) {
          const statuses: IProjectStatus[] = action.response
            ? action.response.map((el) => el.statuses).flat()
            : [];
          const options: IPickListOptions[] = statuses.map((s, i) => ({
            value: s.id,
            label: (
              <div
                className="field-section color-preview-option"
                title={s.title}
                key={i}
              >
                <div
                  style={{
                    backgroundColor: s.color,
                    borderColor: s.color,
                  }}
                  className="left-column"
                />
                <div
                  style={{
                    background: `${getConvertedColorWithOpacity(s.color)}`,
                  }}
                  className="text"
                >
                  {s.title}
                </div>
              </div>
            ),
          }));
          setStatusList(options);
        }
      })
      .finally(() => setLoading(false));
  };

  const onOverallStatusApplyClick = () => {
    setLoading(true);
    props
      .updateProjectDetails(
        {
          overall_status: String(overallStatus),
        },
        project.id
      )
      .then((action) => {
        if (action.type === PROJECT_DETAILS_UPDATE_SUCCESS) {
          updateProject({ overall_status_id: overallStatus });
        } else if (action.type === PROJECT_DETAILS_UPDATE_FAILURE) {
          props.addErrorMessage("Error updating overall status!");
        }
      })
      .finally(() => setLoading(false));
  };

  const onOverallStatusCancelClick = () => {
    setOverallStatus(project.overall_status_id);
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setOverallStatus(Number(e.target.value));
  };

  return (
    <div className="status-overall">
      <div className="action">
        <Input
          field={{
            label: "Overall Status",
            type: "PICKLIST",
            value: overallStatus,
            isRequired: false,
            options: statusList,
          }}
          width={12}
          labelIcon={"info"}
          name="overall_status"
          loading={loading}
          onChange={handleChange}
          placeholder={`Select Status`}
          className="overall-status-edit"
        />
        {!loading && isOverallStatusChanged && (
          <div className="overall-status-action-btn">
            <SquareButton
              onClick={onOverallStatusCancelClick}
              content={"Cancel"}
              bsStyle={"default"}
            />
            <SquareButton
              onClick={onOverallStatusApplyClick}
              content={"Apply"}
              bsStyle={"primary"}
            />
          </div>
        )}
      </div>
    </div>
  );
};

const RightSection: React.FC<RightSectionProps> = (props) => {
  const { project, meetingList, history } = useProject();
  const [previewObj, setPreviewObj] = useState<{
    show: boolean;
    loading: boolean;
    type: "service_detail" | "document";
    previewHTML: IFileResponse;
  }>({
    show: false,
    loading: false,
    type: null,
    previewHTML: null,
  });

  const upcomingMeeting: IProjectMeeting = useMemo(() => {
    const currentTime = moment();
    let upcomingMeeting: IProjectMeeting = null;
    let upcomingMeetingTime: Moment = null;

    meetingList
      .filter((el) => el.schedule_start_datetime)
      .forEach((meeting) => {
        const meetingStartTime = moment(meeting.schedule_start_datetime);
        // Check if meeting is in the future and update the upcomingMeeting variable
        if (
          meetingStartTime.isAfter(currentTime) &&
          (!upcomingMeeting || meetingStartTime.isBefore(upcomingMeetingTime))
        ) {
          upcomingMeeting = meeting;
          upcomingMeetingTime = meetingStartTime;
        }
      });

    return upcomingMeeting;
  }, [meetingList]);

  const redirectToMeeting = () => {
    const meetingId = upcomingMeeting.id;
    const meetingType = upcomingMeeting.meeting_type;
    const query = new URLSearchParams(location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    history.push(
      `/Project/${project.id}/meeting/${meetingId}?type=${meetingType}&AllProject=${AllProject}`
    );
  };

  const previewSowDocument = () => {
    setPreviewObj((prev) => ({
      ...prev,
      type: "document",
      loading: true,
    }));
    props.previewSowDocument(project.sow_document).then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        setPreviewObj((prev) => ({
          ...prev,
          show: true,
          loading: false,
          previewHTML: action.response,
        }));
      } else if (action.type === DOWNLOAD_SOW_FAILURE) {
        props.addErrorMessage("Error previewing sow document!");
      }
    });
  };

  const previewSowServiceDetail = () => {
    setPreviewObj((prev) => ({
      ...prev,
      type: "service_detail",
      loading: true,
    }));
    props.previewSowServiceDetail(project.sow_document).then((action) => {
      if (action.type === PREVIEW_SERVICE_DETAIL_SUCCESS) {
        setPreviewObj((prev) => ({
          ...prev,
          show: true,
          loading: false,
          previewHTML: action.response,
        }));
      } else if (action.type === PREVIEW_SERVICE_DETAIL_FAILURE) {
        props.addErrorMessage("Error previewing sow service detail!");
      }
    });
  };

  const closeDocumentPreview = () => {
    setPreviewObj({
      type: null,
      show: false,
      loading: false,
      previewHTML: null,
    });
  };

  return (
    <>
      <div className="details-dashboard-right">
        <OverallStatus
          addErrorMessage={props.addErrorMessage}
          addSuccessMessage={props.addSuccessMessage}
          getProjectBoardList={props.getProjectBoardList}
          updateProjectDetails={props.updateProjectDetails}
        />
        <div className="estimated-end-date">
          <label>Estimated End Date:</label>
          <span>
            {fromISOStringToFormattedDate(
              project.estimated_end_date,
              "MMM DD, YYYY"
            ) || "-"}
          </span>
        </div>
        <PhaseMilestone fetchPhases={props.fetchPhases} />
        <div className="upcoming-meeting-section">
          <h5>Upcoming Meeting</h5>
          {upcomingMeeting ? (
            <div className="upcoming-meeting-link" onClick={redirectToMeeting}>
              <span>
                {fromISOStringToFormattedDate(
                  upcomingMeeting.schedule_start_datetime,
                  "MMM DD, YYYY"
                )}
              </span>
              <span>{upcomingMeeting.name}</span>
            </div>
          ) : (
            <div className="no-upcoming-meeting">No meetings found</div>
          )}
        </div>
        {project.sow_document && (
          <>
            <div className="sow-preview-row">
              {previewObj.type === "document" && previewObj.loading ? (
                <img
                  className="icon__loading"
                  src="/assets/icons/loading.gif"
                  alt="Loading Preview"
                />
              ) : (
                <DeleteButton
                  type="pdf_preview"
                  title="SoW Document Preview"
                  onClick={previewSowDocument}
                />
              )}
              <span>Statement of Work</span>
            </div>
            <div className="sow-preview-row">
              {previewObj.type === "service_detail" && previewObj.loading ? (
                <img
                  className="icon__loading"
                  src="/assets/icons/loading.gif"
                  alt="Loading Preview"
                />
              ) : (
                <DeleteButton
                  type="pdf_preview"
                  title="SoW Service Detail Preview"
                  onClick={previewSowServiceDetail}
                />
              )}
              <span>Service Detail</span>
            </div>
          </>
        )}
        <ProjectDocs />
      </div>
      <PDFViewer
        show={previewObj.show}
        onClose={closeDocumentPreview}
        titleElement={
          previewObj.type === "service_detail"
            ? "SoW Service Detail Preview"
            : "View SoW Preview"
        }
        footerElement={
          <SquareButton
            content="Close"
            bsStyle={"default"}
            onClick={closeDocumentPreview}
          />
        }
        previewHTML={previewObj.previewHTML}
        className=""
      />
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectBoardList: () => dispatch(getProjectBoardList()),
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  fetchPhases: (projectId: number) =>
    dispatch(getProjectPhasesByID(projectId, true)),
  previewSowDocument: (sowId: number) =>
    dispatch(downloadSOWDocumnet(sowId, "pdf")),
  previewSowServiceDetail: (sowId: number) =>
    dispatch(downloadServiceDetail(sowId)),
  updateProjectDetails: (data: Partial<IProjectDetails>, projectID: number) =>
    dispatch(updateProjectDetails(data, projectID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(RightSection);
