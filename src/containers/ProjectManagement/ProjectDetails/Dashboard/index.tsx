import React from "react";
import { connect } from "react-redux";
import HoursTable from "./hoursTable";
import RightSection from "./rightSection";
import ProjectUpdates from "./projectUpdates";
import PastDueItems from "./pastDueItems";
import PMOCollapsible from "../../../../components/PMOCollapsible";
import SowProjectDetails from "./sowProjectDetails";

interface DashboardProps {}

const Dashboard: React.FC<DashboardProps> = (props) => {
  return (
    <div className="project-details-dashboard-tab">
      <div className="details-dashboard-left">
        <HoursTable />
        <PMOCollapsible label={"Past Due Items"} isOpen={true}>
          <PastDueItems />
        </PMOCollapsible>
        <PMOCollapsible label={"Project Updates"} isOpen={true}>
          <ProjectUpdates />
        </PMOCollapsible>
        <PMOCollapsible label={"Project Details"} isOpen={true}>
          <SowProjectDetails />
        </PMOCollapsible>
      </div>
      <RightSection />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
