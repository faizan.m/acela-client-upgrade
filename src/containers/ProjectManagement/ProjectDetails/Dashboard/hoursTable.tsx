import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  getProjectResourceSummary,
  PROJECT_RESOURCE_SUMMARY_SUCCESS,
} from "../../../../actions/pmo";
import "../../../../commonStyles/serviceCostCalculations.scss";
import { useProject } from "../projectContext";

interface HoursTableProps {
  getProjectResourceSummary: (projectID: number) => Promise<any>;
}

interface IProjectResource {
  role: string;
  total_actual_hours: number;
  total_budget_hours: number;
  total_remaining_hours: number;
  total_percentage_of_budget: number;
}

const HoursTable: React.FC<HoursTableProps> = (props) => {
  const [rows, setRows] = useState<IProjectResource[]>([]);
  const { project } = useProject();

  const totalBudgetHours = rows.reduce(
    (prev, row) => prev + Number(row.total_budget_hours),
    0
  );
  const totalActualHours = rows.reduce(
    (prev, row) => prev + Number(row.total_actual_hours),
    0
  );
  const totalRemainingHours = totalBudgetHours - totalActualHours;
  const totalBudgetPercent =
    totalBudgetHours !== 0
      ? Math.round((totalActualHours / totalBudgetHours) * 100)
      : 0;

  useEffect(() => {
    fetchTableData();
  }, []);

  const fetchTableData = () => {
    props.getProjectResourceSummary(project.id).then((action) => {
      if (action.type === PROJECT_RESOURCE_SUMMARY_SUCCESS) {
        const projectResources: IProjectResource[] = action.response;
        projectResources.sort((a, b) =>
          a.role === "Unmapped" ? 1 : b.role === "Unmapped" ? -1 : 0
        );
        const lastItem = projectResources[projectResources.length - 1];
        if (
          lastItem.role === "Unmapped" &&
          !lastItem.total_actual_hours &&
          !lastItem.total_budget_hours
        )
          projectResources.pop();
        setRows(projectResources);
      }
    });
  };

  return (
    <div className="calculations">
      <div className="calculations-table">
        <div className="calculations-table-header">
          <div className="calculations-header-title">Resource</div>
          <div className="calculations-header-title">Budget Hours</div>
          <div className="calculations-header-title">Actual Hours</div>
          <div className="calculations-header-title">Remaining Hours</div>
          <div className="calculations-header-title">% of Budget</div>
        </div>
        <div className="calculations-table-body">
          {rows.map((el, idx) => (
            <div className="calculations-table-row" key={idx}>
              <div className="calculations-table-col">{el.role}</div>
              <div className="calculations-table-col">
                {Number(el.total_budget_hours)}
              </div>
              <div className="calculations-table-col">
                {Number(el.total_actual_hours)}
              </div>
              <div className="calculations-table-col">
                {Number(el.total_remaining_hours)}
              </div>
              <div className="calculations-table-col">
                {Number(el.total_percentage_of_budget)}%
              </div>
            </div>
          ))}
          <div className="calculations-table-row">
            <div className="calculations-table-col">Subtotal</div>
            <div className="calculations-table-col">{totalBudgetHours}</div>
            <div className="calculations-table-col">{totalActualHours}</div>
            <div className="calculations-table-col">{totalRemainingHours}</div>
            <div className="calculations-table-col">{totalBudgetPercent}%</div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectResourceSummary: (projectID: number) =>
    dispatch(getProjectResourceSummary(projectID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HoursTable);
