import React, { useEffect, useState } from "react";
import { isEmpty } from "lodash";
import ReactQuill from "react-quill";
import SquareButton from "../../../../components/Button/button";
import Input from "../../../../components/Input/input";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { useProject } from "../projectContext";
import { connect } from "react-redux";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  updateProjectDetails,
  PROJECT_DETAILS_UPDATE_FAILURE,
  PROJECT_DETAILS_UPDATE_SUCCESS,
} from "../../../../actions/pmo";

interface IProjectUpdatesProps {
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  updateProjectDetails: (
    data: Partial<IProjectDetails>,
    projectID: number
  ) => Promise<any>;
}

const ProjectUpdates: React.FC<IProjectUpdatesProps> = (props) => {
  const [editMode, setEditMode] = useState<boolean>(false);
  const [updates, setUpdates] = useState<string>("");
  const [note, setNote] = useState<string>("");
  const [etc, setEtc] = useState<number>(null);

  const { project, updateProject } = useProject();

  useEffect(() => {
    if (!isEmpty(project.project_updates)) {
      setUpdates(project.project_updates.updates);
      setNote(project.project_updates.note);
      setEtc(project.project_updates.estimated_hours_to_complete);
    }
  }, [project]);

  const handleCancel = () => {
    setEditMode(false);
    setUpdates(project.project_updates.updates);
    setNote(project.project_updates.note);
    setEtc(project.project_updates.estimated_hours_to_complete);
  };

  const validHours = (hours: number) =>
    hours === null || (Number.isInteger(hours) && hours >= 0 && hours <= 999);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEtc(Number(e.target.value));
  };

  const handleChangeEditor = (key: "updates" | "note", value: string) => {
    key === "updates" ? setUpdates(value) : setNote(value);
  };

  const onSave = () => {
    setEditMode(false);
    const projectUpdates = {
      project_updates: {
        updates,
        note,
        estimated_hours_to_complete: etc,
      },
    };
    props.updateProjectDetails(projectUpdates, project.id).then((action) => {
      if (action.type === PROJECT_DETAILS_UPDATE_SUCCESS) {
        updateProject(projectUpdates);
      } else if (action.type === PROJECT_DETAILS_UPDATE_FAILURE) {
        props.addErrorMessage("Error updating overall status!");
      }
    });
  };

  return (
    <div className="project-updates-wrapper">
      <div className="project-updates-header">
        <label className="col-xs-4">Updates</label>
        <label className="col-xs-4">Note</label>
        <label className="col-xs-3">Estimated Hours to Complete</label>
      </div>
      {editMode ? (
        <>
          <div className="pr-up-edit-mode">
            <ReactQuill
              value={updates}
              onChange={(html: string) => handleChangeEditor("updates", html)}
              modules={{
                toolbar: [[{ list: "ordered" }, { list: "bullet" }]],
              }}
              theme="snow"
              className={`project-updates-quill col-md-4`}
            />
            <ReactQuill
              value={note}
              onChange={(html: string) => handleChangeEditor("note", html)}
              modules={{
                toolbar: [[{ list: "ordered" }, { list: "bullet" }]],
              }}
              theme="snow"
              className={`project-updates-quill col-md-4`}
            />
            <Input
              field={{
                value: etc,
                label: "",
                type: "NUMBER",
                isRequired: false,
              }}
              width={3}
              name={"etc"}
              placeholder="Enter Hours"
              onChange={(e) => handleChange(e)}
              className="project-updates-input"
              minimumValue="0"
              maximumValue="999"
              error={
                !validHours(etc)
                  ? {
                      errorState: "error",
                      errorMessage:
                        "Please enter a valid integer between 0 to 999",
                    }
                  : undefined
              }
            />
          </div>
          <div className="pr-up-actions">
            <SquareButton
              onClick={handleCancel}
              content={"Cancel"}
              bsStyle={"default"}
            />
            <SquareButton
              onClick={onSave}
              content={"Apply"}
              bsStyle={"primary"}
              disabled={!validHours(etc)}
            />
          </div>
        </>
      ) : (
        <div className="pr-up-display-mode">
          <img
            className={"pr-up-edit"}
            alt="Edit Project Updates"
            src={"/assets/icons/edit.png"}
            title={"Edit Project Updates"}
            onClick={() => setEditMode(true)}
          />
          {commonFunctions.isEditorEmpty(updates) ? (
            <div className="pr-up-no-data col-md-4">No data</div>
          ) : (
            <div
              className="project-updates-html ql-editor col-md-4"
              dangerouslySetInnerHTML={{
                __html: updates,
              }}
            />
          )}
          {commonFunctions.isEditorEmpty(note) ? (
            <div className="pr-up-no-data col-md-4">No data</div>
          ) : (
            <div
              className="project-updates-html ql-editor col-md-4"
              dangerouslySetInnerHTML={{
                __html: note,
              }}
            />
          )}
          {etc === null ? (
            <div className="pr-up-no-data col-md-3" style={{ width: "190px" }}>
              No data
            </div>
          ) : (
            <div className="project-updates-etc col-md-3">{etc}</div>
          )}
        </div>
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  updateProjectDetails: (data: Partial<IProjectDetails>, projectID: number) =>
    dispatch(updateProjectDetails(data, projectID)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectUpdates);
