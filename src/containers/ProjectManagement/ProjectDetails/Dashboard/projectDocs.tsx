import React, { useEffect, useRef, useState } from "react";
import { connect } from "react-redux";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  downloadAttachment,
  DOWNLOAD_DOCUMENT_SUCCESS,
} from "../../../../actions/dashboard";
import {
  PROJECT_DOCUMENTS_FAILURE,
  PROJECT_DOCUMENTS_SUCCESS,
  deleteProjectDocumentsByID,
  getProjectDocumentsByID,
  saveProjectDocumentsByID,
} from "../../../../actions/pmo";
import Spinner from "../../../../components/Spinner";
import Accordian from "../../../../components/Accordian";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import { saveBlob } from "../../../../utils/download";
import { useProject } from "../projectContext";

interface ProjectDocumentProps {
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  downloadAttachment: (documentId: number) => Promise<any>;
  getProjectDocumentsByID: (projectID: number) => Promise<any>;
  saveProjectDocumentsByID: (projectID: number, data: FormData) => Promise<any>;
  deleteProjectDocumentsByID: (
    projectID: number,
    documentID: number
  ) => Promise<any>;
}

interface IProjectDoc {
  id: number;
  title: string;
  owner: string;
  file_name: string;
  last_updated_on: string;
  server_file_name: string;
}

const fileIcons = {
  pdf: "/assets/icons/008-file-7.svg",
  doc: "/assets/icons/005-file-4.svg",
  docx: "/assets/icons/006-file-5.svg",
  zip: "/assets/icons/009-file-8.svg",
  xls: "/assets/icons/047-file-46.svg",
  xlsx: "/assets/icons/047-file-46.svg",
  csv: "/assets/icons/048-file-47.svg",
  svg: "/assets/icons/001-file.svg",
  png: "/assets/icons/003-file-2.svg",
  jpg: "/assets/icons/004-file-3.svg",
  jpeg: "/assets/icons/004-file-3.svg",
  txt: "/assets/icons/034-file-33.svg",
};

const ProjectDocs: React.FC<ProjectDocumentProps> = (props) => {
  const uploadInput = useRef(null);
  const deleteConfirmBox = useRef(null);
  const [editDocumentId, setEditDocumentId] = useState<number>(null);
  const [currentDownloadingFile, setCurrentDownloadingFile] = useState(null);
  const [projectDocuments, setProjectDocuments] = useState<IProjectDoc[]>([]);
  const [showDocumentLoader, setShowDocumentLoader] = useState<boolean>(false);
  const [currentUploadedDocument, setCurrentUploadedDocument] = useState<File>(
    null
  );
  const [showDocumentActions, setShowDocumentActions] = useState<boolean>(
    false
  );
  const { project } = useProject();

  useEffect(() => {
    fetchProjecDocuments();
  }, []);

  // Function to upload a document
  const uploadDocument = () => {
    setShowDocumentLoader(true);
    const data = new FormData();
    data.append("file_path", currentUploadedDocument);
    data.append("file_name", currentUploadedDocument.name);

    props.saveProjectDocumentsByID(project.id, data).then((action) => {
      if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
        setCurrentUploadedDocument(null);
        fetchProjecDocuments();
      }
    });
  };

  // Function to get project documents by ID
  const fetchProjecDocuments = () => {
    setShowDocumentLoader(true);
    props
      .getProjectDocumentsByID(project.id)
      .then((action) => {
        if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
          setProjectDocuments(action.response);
        }
      })
      .finally(() => setShowDocumentLoader(false));
  };

  // Function to download a document
  const downloadDocument = (document: IProjectDoc) => {
    setCurrentDownloadingFile(document.id);
    props.downloadAttachment(document.id).then((action) => {
      if (action.type === DOWNLOAD_DOCUMENT_SUCCESS) {
        saveBlob(document.file_name, action.response);
        setCurrentDownloadingFile(null);
      }
    });
  };

  const handleFileUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const dataFile = files[0];
    setCurrentUploadedDocument(dataFile);
  };

  const deleteDocument = (document: IProjectDoc) => {
    props
      .deleteProjectDocumentsByID(project.id, document.id)
      .then((action) => {
        if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
          fetchProjecDocuments();
          props.addSuccessMessage("Document deleted successfully");
        }
        if (action.type === PROJECT_DOCUMENTS_FAILURE) {
          props.addErrorMessage("Error deleting document!");
        }
      })
      .catch(() => {
        props.addErrorMessage("Error deleting document, server error!");
      });
  };

  return (
    <Accordian label={"Documents"} isOpen={false}>
      <div className="details-box">
        <div className="doucments-list">
          {showDocumentLoader && (
            <div className="document-loader">
              <Spinner show={true} />
            </div>
          )}
          {projectDocuments.map((document, i) => (
            <div
              key={i}
              className="document"
              onMouseEnter={() => {
                setShowDocumentActions(true);
                setEditDocumentId(document.id);
              }}
              onMouseLeave={() => {
                if (!deleteConfirmBox.current) {
                  setShowDocumentActions(false);
                  setEditDocumentId(null);
                }
              }}
            >
              <div className="doc-right">
                <img
                  className="file-icons"
                  alt=""
                  src={
                    fileIcons[document.file_name.split(".").pop()] ||
                    "/assets/icons/046-file-45.svg"
                  }
                />
                <div className="document-label-content">{document.title}</div>
              </div>
              {showDocumentActions && editDocumentId === document.id && (
                <div className="download-icons">
                  <SmallConfirmationBox
                    setRef={deleteConfirmBox}
                    className="remove"
                    onClickOk={() => deleteDocument(document)}
                    text={"document"}
                  />
                </div>
              )}
              <img
                className="download-icons"
                alt=""
                src={
                  document.id === currentDownloadingFile
                    ? "/assets/icons/loading.gif"
                    : "/assets/icons/download.png"
                }
                onClick={
                  document.id === currentDownloadingFile
                    ? null
                    : () => downloadDocument(document)
                }
              />
            </div>
          ))}
        </div>
        <input
          type="file"
          style={{ display: "none" }}
          ref={uploadInput}
          onChange={handleFileUpload}
        />
        {currentUploadedDocument ? (
          <>
            <div className="upload-box">
              <span className="upload-filename">
                {currentUploadedDocument.name}
              </span>
            </div>
            <div className="file-upload-actions">
              <img
                className="file-action-icons file-upload-save"
                alt=""
                src={"/assets/icons/validated.svg"}
                onClick={() => uploadDocument()}
              />
              <img
                className="file-action-icons file-upload-cancel"
                alt=""
                src={"/assets/icons/close.png"}
                onClick={() => setCurrentUploadedDocument(null)}
              />
            </div>
          </>
        ) : (
          !showDocumentLoader && (
            <div
              className="document-label-details action-item"
              onClick={() => (uploadInput as any).current.click()}
            >
              Upload File
            </div>
          )
        )}
      </div>
    </Accordian>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  getProjectDocumentsByID: (projectID: number) =>
    dispatch(getProjectDocumentsByID(projectID)),
  saveProjectDocumentsByID: (projectID: number, data: FormData) =>
    dispatch(saveProjectDocumentsByID(projectID, data)),
  deleteProjectDocumentsByID: (projectID: number, documentID: number) =>
    dispatch(deleteProjectDocumentsByID(projectID, documentID)),
  downloadAttachment: (documentId: number) =>
    dispatch(downloadAttachment(documentId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDocs);
