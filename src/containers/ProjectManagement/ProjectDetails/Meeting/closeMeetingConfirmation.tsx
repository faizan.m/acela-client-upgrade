import React, { useState } from "react";
import { connect } from "react-redux";
import {
  meetingComplete,
  makeMeetingComplete,
  COMPLETE_MEETING_FAILURE,
  COMPLETE_MEETING_SUCCESS,
  MEETING_COMPLETE_SUCCESS,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import { getURLBYMeetingType } from "../meetingDetails";
import ConfirmBox from "../../../../components/ConfirmBox/ConfirmBox";

interface CloseMeetingConfirmProps extends MeetingCloseProps {
  emailData: CloseMeetingEmail;
  onCloseConfirmation: (save?: boolean) => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  archiveMeeting: (projectID: number, meetingID: number) => Promise<any>;
  makeMeetingComplete: (
    projectID: number,
    meetingType: string,
    meetingID: number,
    data: FormData
  ) => Promise<any>;
  user: any;
}

const CloseMeetingConfirm: React.FC<CloseMeetingConfirmProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);

  const markMeetingCompleteArchive = () => {
    setLoading(true);
    const projectID = props.projectDetails.id;
    const meetingID = props.meetingId;
    const formData = new FormData();
    formData.append("email_body", `${props.emailData.email_body} ${props.user.profile.email_signature.email_signature}`);
    formData.append("email_subject", props.emailData.email_subject);
    formData.append("project_update", props.emailData.project_update);
    if (props.emailData.attachment)
      formData.append("attachment", props.emailData.attachment);
    props.emailData.recipients.forEach((el, idx) => {
      formData.append(`recipients[${idx}]`, el);
    });
    props
      .makeMeetingComplete(
        projectID,
        getURLBYMeetingType(props.meetingDetails.meeting_type),
        meetingID,
        formData
      )
      .then((action) => {
        if (action.type === COMPLETE_MEETING_SUCCESS) {
          props.archiveMeeting(projectID, meetingID).then((action) => {
            if (action.type === MEETING_COMPLETE_SUCCESS) {
              props.addSuccessMessage("Email triggered successfully!");
              props.addSuccessMessage(
                "Meeting marked as completed and archived!"
              );
              setLoading(false);
              props.onCloseConfirmation(true);
            }
          });
        } else if (action.type === COMPLETE_MEETING_FAILURE) {
          props.addErrorMessage("Unable to make the meeting as complete");
          setLoading(false);
          if (
            action.errorList &&
            action.errorList.data &&
            typeof action.errorList.data === "object"
          ) {
            Object.keys(action.errorList.data).forEach((key) => {
              const errorMessage = action.errorList.data[key];
              props.addErrorMessage(errorMessage);
            });
          }
        }
      });
  };

  return (
    <ConfirmBox
      show={true}
      title="Confirm Meeting Completion"
      message={
        "Are you sure you want to close this meeting and send email to the recipients?"
      }
      onClose={() => props.onCloseConfirmation(false)}
      onSubmit={markMeetingCompleteArchive}
      isLoading={loading}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (str: string) => dispatch(addErrorMessage(str)),
  addSuccessMessage: (str: string) => dispatch(addSuccessMessage(str)),
  archiveMeeting: (projectID: number, meetingID: number) =>
    dispatch(
      meetingComplete(
        projectID,
        "post",
        `meetings/${meetingID}/archive`
      )
    ),
  makeMeetingComplete: (
    projectID: number,
    meetingType: string,
    meetingID: number,
    data: FormData
  ) => dispatch(makeMeetingComplete(projectID, meetingType, meetingID, data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CloseMeetingConfirm);
