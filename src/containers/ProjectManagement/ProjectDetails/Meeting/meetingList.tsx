import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import {
  postActionItem,
  deleteActionItem,
  saveMeetingDetails,
  meetingTemplateCRUD,
  getMeetingPDFPreview,
  getMeetingTemplateSetting,
  MEETING_SUCCESS,
  ACTION_ITEM_SUCCESS,
  ACTION_ITEM_FAILURE,
  MEETING_TEMPLATE_SUCCESS,
  FETCH_MEETING_PDF_PREVIEW_SUCCESS,
  FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS,
} from "../../../../actions/pmo";
import { addErrorMessage } from "../../../../actions/appState";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import EditButton from "../../../../components/Button/editButton";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import ModalBase from "../../../../components/ModalBase/modalBase";
import DeleteButton from "../../../../components/Button/deleteButton";
import InfiniteListing from "../../../../components/InfiniteList/infiniteList";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import { useProject } from "../projectContext";
import { getURLBYMeetingType } from "../meetingDetails";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import moment from "moment";

interface MeetingListProps {
  addErrorMessage: TShowErrorMessage;
  fetchMeetingTemplates: () => Promise<any>;
  fetchMeetingEmailTemplates: () => Promise<any>;
  postActionItem: (projectId: number, data: IProjectDetailItem) => Promise<any>;
  saveMeetingDetails: (
    projectID: number,
    data: IProjectMeeting,
    api: string
  ) => Promise<any>;
  getMeetingPDFPreview: (
    projectId: number,
    meetingId: number,
    completed: boolean
  ) => Promise<any>;
  deleteMeeting: (
    projectId: number,
    meetingId: number,
    api: string
  ) => Promise<any>;
}

const regularMeetings = [
  "Status",
  "Kickoff",
  "Internal Kickoff",
];

// TODO: Check created meeting status for customer touch and close out, what email fields are to be set

const CreateMeetingModal = ({
  onClose,
  postActionItem,
  meetingTemplates,
  saveMeetingDetails,
  meetingMailTemplates,
}: {
  onClose: (save?: boolean) => void;
  meetingTemplates: IMeetingTemplate[];
  meetingMailTemplates: IMeetingEmailTemplate[];
  postActionItem: (projectID: number, data: IProjectDetailItem) => Promise<any>;
  saveMeetingDetails: (
    projectID: number,
    data: IProjectMeeting,
    api: string
  ) => Promise<any>;
}) => {
  const {
    project,
    location,
    history,
    meetingList,
    projectStatusList,
  } = useProject();
  const [type, setType] = useState<MeetingType>();
  const [template, setTemplate] = useState<number>();
  const [error, setError] = useState({
    type: commonFunctions.getErrorState(),
    template: commonFunctions.getErrorState(),
  });

  const isValid = () => {
    let isValid = true;
    const err = cloneDeep(error);
    if (!type) {
      isValid = false;
      err.type = commonFunctions.getErrorState("This field is required");
    } else err.type = commonFunctions.getErrorState();
    if (hasMeetingTemplate && !template) {
      isValid = false;
      err.template = commonFunctions.getErrorState("This field is required");
    } else err.template = commonFunctions.getErrorState();
    setError(err);
    return isValid;
  };

  const hasMeetingTemplate =
    type &&
    (type === "Status" ||
      type === "Kickoff" ||
      type === "Internal Kickoff");

  const meetingOptions: string[] = useMemo(() => {
    const meetings: string[] = [
      "Kickoff",
      "Internal Kickoff",
      "Status",
      "Close Out",
      "Customer Touch",
    ].filter((el) => {
      let show = true;
      if (
        el === "Customer Touch" ||
        el === "Internal Kickoff" ||
        el === "Kickoff"
      ) {
        show = !Boolean(meetingList.find((m) => m.meeting_type === el));
      }
      return show;
    });

    return meetings;
  }, [meetingList]);

  const templateOptions: IPickListOptions[] = useMemo(() => {
    if (hasMeetingTemplate) {
      return meetingTemplates
        .filter((el) => {
          const derivedType =
            type === "Internal Kickoff" ? "Kickoff" : type;
          return el.meeting_type === derivedType;
        })
        .map((el) => ({
          value: el.id,
          label: el.template_name,
        }));
    }
    return [];
  }, [type]);

  const handleChangeType = (e: React.ChangeEvent<HTMLInputElement>) => {
    setType(e.target.value as MeetingType);
    setTemplate(null);
  };

  const handleChangeTemplate = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTemplate(Number(e.target.value));
  };

  const onSaveClick = () => {
    if (isValid()) {
      const selectedTemplate = meetingTemplates.find(
        (el) => el.id === template
      );
      let actionItems: IProjectDetailItem[];
      if (hasMeetingTemplate) {
        const status =
          projectStatusList && projectStatusList.find((f) => f.is_default);
        actionItems = selectedTemplate.action_items.map((html) => ({
          description: html,
          owner: project.project_manager_id,
          status: status && status.id,
          due_date: moment(new Date()).add(7, "days"),
        }));
        actionItems.forEach((item) => {
          postActionItem(project.id, item);
        });
      }

      const projectMeeting: IProjectMeeting = {
        name: hasMeetingTemplate ? selectedTemplate.template_name : type,
        owner: project.project_manager_id,
        agenda_topics:
          hasMeetingTemplate && selectedTemplate.agenda_topics
            ? selectedTemplate.agenda_topics.map((el, idx) => ({
                ...el,
                id: idx,
              }))
            : null,
        meeting_type: type,
        is_internal: type === "Internal Kickoff",
        action_items: actionItems,
        schedule_start_datetime: moment(),
        schedule_end_datetime: moment(),
        email_body: "",
        email_subject: "",
      };

      if (type === "Customer Touch") {
        const mailTemplate = meetingMailTemplates.find(
          (el) => el.meeting_type === type
        );
        if (mailTemplate) {
          projectMeeting.email_body = mailTemplate.email_body_markdown;
          projectMeeting.email_subject = mailTemplate.email_subject;
          projectMeeting.email_body_text = mailTemplate.email_body_text;
        }
      }
      if (type === "Close Out") {
        const mailTemplate = meetingMailTemplates.find(
          (el) => el.meeting_type === type
        );
        if (mailTemplate) {
          projectMeeting.project_acceptance_markdown =
            mailTemplate.email_body_markdown;
          projectMeeting.email_subject = mailTemplate.email_subject;
          projectMeeting.project_acceptance_email_subject =
            mailTemplate.email_subject;
          projectMeeting.project_acceptance_text = mailTemplate.email_body_text;
        }
      }

      let url = `${getURLBYMeetingType(type)}`;
      saveMeetingDetails(project.id, projectMeeting, url).then((action) => {
        if (action.type === MEETING_SUCCESS) {
          if (action.type === MEETING_SUCCESS) {
            const query = new URLSearchParams(location.search);
            const AllProject =
              query.get("AllProject") === "true" ? true : false;
            history.push(
              `/Project/${project.id}/meeting/${action.response.id}?type=${type}&template=${template}&AllProject=${AllProject}`
            );
          }
        }
      });
    }
  };

  return (
    <ModalBase
      show={true}
      onClose={onClose}
      titleElement={"Create Meeting"}
      bodyElement={
        <div className="project-details-modal">
          <Input
            field={{
              label: "Type",
              type: "PICKLIST",
              value: type,
              options: meetingOptions,
              isRequired: false,
            }}
            width={4}
            multi={false}
            name="type"
            onChange={handleChangeType}
            placeholder={`Select Meeting Type`}
            error={error.type}
          />
          {hasMeetingTemplate && (
            <Input
              field={{
                value: template,
                label: "Meeting Template",
                type: "PICKLIST",
                options: templateOptions,
                isRequired: false,
              }}
              width={8}
              name="template"
              onChange={handleChangeTemplate}
              placeholder="Select Template"
              error={error.template}
            />
          )}
        </div>
      }
      footerElement={
        <>
          <SquareButton
            content="Close"
            title="Close Modal"
            bsStyle={"default"}
            onClick={onClose}
          />
          <SquareButton
            content="Create Meeting"
            title="Create Meeting"
            bsStyle={"primary"}
            onClick={onSaveClick}
          />
        </>
      }
      className="create-meeting"
    />
  );
};

const MeetingList: React.FC<MeetingListProps> = (props) => {
  const { project, location, history } = useProject();
  const [showModal, setShowModal] = useState<boolean>(false);
  const [refreshKey, setRefreshKey] = useState<number>(Math.random());
  const [meetingTemplates, setMeetingTemplates] = useState<IMeetingTemplate[]>(
    []
  );
  const [meetingEmailTemplates, setMeetingEmailTemplates] = useState<
    IMeetingEmailTemplate[]
  >([]);

  const [pdfPreviewData, setPDFPreviewData] = useState<{
    pdfIds: number[];
    openPreview: boolean;
    previewHTML: { url: string };
  }>({
    pdfIds: [],
    openPreview: false,
    previewHTML: null,
  });

  useEffect(() => {
    // Function to update query parameter
    const updateTabQueryParam = () => {
      const searchParams = new URLSearchParams(location.search);
      searchParams.set("tab", "Meetings");
      history.replace({
        pathname: location.pathname,
        search: `?${searchParams.toString()}`,
      });
    };
    updateTabQueryParam();
  }, [history, location.pathname, location.search]);

  useEffect(() => {
    fetchMeetingTemplates();
    fetchMeetingEmailTemplates();
  }, []);

  const fetchMeetingTemplates = () => {
    props.fetchMeetingTemplates().then((action) => {
      if (action.type === MEETING_TEMPLATE_SUCCESS) {
        setMeetingTemplates(action.response);
      }
    });
  };

  const fetchMeetingEmailTemplates = async () => {
    props.fetchMeetingEmailTemplates().then((action) => {
      if (action.type === FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS) {
        const { results } = action.response;
        setMeetingEmailTemplates(results);
      }
    });
  };

  const onPDFPreviewClick = (row: IProjectMeeting) => {
    setPDFPreviewData((prev) => ({
      ...prev,
      pdfIds: [...prev.pdfIds, row.id],
    }));

    props.getMeetingPDFPreview(project.id, row.id, false).then((a) => {
      if (a.type === FETCH_MEETING_PDF_PREVIEW_SUCCESS) {
        setPDFPreviewData((prev) => ({
          openPreview: true,
          previewHTML: a.response,
          pdfIds: prev.pdfIds.filter((el) => el !== row.id),
        }));
      } else {
        setPDFPreviewData((prev) => ({
          ...prev,
          pdfIds: prev.pdfIds.filter((el) => el !== row.id),
        }));
      }
    });
  };

  const getPDFPreviewMarkup = (row: IProjectMeeting) => {
    return regularMeetings.includes(row.meeting_type as MeetingType) ? (
      pdfPreviewData.pdfIds.includes(row.id) ? (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Loading Preview"
        />
      ) : (
        <DeleteButton
          type="pdf_preview"
          title="Preview"
          onClick={() => onPDFPreviewClick(row)}
        />
      )
    ) : null;
  };

  const closePreview = () => {
    setPDFPreviewData((prev) => ({
      ...prev,
      openPreview: false,
      previewHTML: null,
    }));
  };

  const closeCreateModal = (save?: boolean) => {
    if (save === true) setRefreshKey(Math.random());
    setShowModal(false);
  };

  const onEditRowClick = (row: IProjectMeeting) => {
    const query = new URLSearchParams(location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    history.push(
      `/Project/${project.id}/meeting/${row.id}?type=${row.meeting_type}&AllProject=${AllProject}`
    );
  };

  const onDeleteRowClick = (row: IProjectMeeting) => {
    let url = `${getURLBYMeetingType(row.meeting_type)}`;
    props.deleteMeeting(project.id, row.id, url).then((action) => {
      if (action.type === ACTION_ITEM_SUCCESS) {
        setRefreshKey(Math.random());
      }
      if (action.type === ACTION_ITEM_FAILURE) {
        props.addErrorMessage("Error deleting meeting!");
      }
    });
  };

  const onOpenArhivedMeeting = (row: IProjectMeeting) => {
    const query = new URLSearchParams(location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    history.push(
      `/Project/${project.id}/meeting/${row.id}?type=${row.meeting_type}&view=true&AllProject=${AllProject}`
    );
  };

  const columns: IColumnInfinite<IProjectMeeting>[] = [
    {
      name: "Scheduled Date",
      id: "schedule_start_datetime",
      ordering: "schedule_start_datetime",
      className: "width-10",
      Cell: (row) =>
        fromISOStringToFormattedDate(
          row.schedule_start_datetime,
          "MMM DD, YYYY"
        ),
    },
    {
      name: "Description",
      id: "name",
      ordering: "name",
      className: "meeting-description",
    },
    {
      name: "Type",
      id: "meeting_type",
      ordering: "meeting_type",
      className: "width-15",
    },
    {
      name: "Meeting Status",
      id: "status",
      ordering: "status",
      className: "width-15",
      Cell: (row) => (row.status === "Pending" ? "Draft" : row.status),
    },
    {
      name: "Created Date",
      id: "created_on",
      ordering: "created_on",
      className: "width-10",
      Cell: (row) =>
        fromISOStringToFormattedDate(row.created_on, "MMM DD, YYYY"),
    },
    {
      name: "Updated Date",
      id: "updated_on",
      ordering: "updated_on",
      className: "width-10",
      Cell: (row) =>
        fromISOStringToFormattedDate(row.updated_on, "MMM DD, YYYY"),
    },
    {
      name: "",
      id: "",
      className: "meeting-row-actions width-15",
      Cell: (row) => {
        return (
          <>
            {row.is_archived ? (
              <>
                <DeleteButton
                  type="archived"
                  title="Open Meeting"
                  onClick={() => onOpenArhivedMeeting(row)}
                />
                {getPDFPreviewMarkup(row)}
              </>
            ) : (
              <>
                <EditButton
                  title="Open Meeting"
                  onClick={() => onEditRowClick(row)}
                />
                {getPDFPreviewMarkup(row)}
                <SmallConfirmationBox
                  className="delete-meeting"
                  text="meeting"
                  onClickOk={() => onDeleteRowClick(row)}
                  elementToCLick={<DeleteButton onClick={() => null} />}
                />
              </>
            )}
          </>
        );
      },
    },
  ];

  return (
    <div className="meeting-list-container">
      <SquareButton
        content="Create"
        bsStyle={"primary"}
        className="create-meeting"
        onClick={() => setShowModal(true)}
      />
      <PDFViewer
        show={pdfPreviewData.openPreview}
        onClose={closePreview}
        titleElement={`Meeting PDF Preview`}
        previewHTML={{
          file_path:
            pdfPreviewData.previewHTML && pdfPreviewData.previewHTML.url,
        }}
        footerElement={
          <SquareButton
            content="Close"
            bsStyle={"default"}
            onClick={closePreview}
          />
        }
      />
      <InfiniteListing
        showSearch={true}
        refreshKey={refreshKey}
        id="projectMeetingList"
        className="meeting-infinite-list"
        url={`providers/pmo/projects/${project.id}/meetings`}
        checkBoxfilters={[
          {
            key: "status",
            value: false,
            name: "Show Completed Meetings",
            trueValue: "Completed",
            falseValue: "",
          },
        ]}
        columns={columns}
      />
      {showModal && (
        <CreateMeetingModal
          onClose={closeCreateModal}
          postActionItem={props.postActionItem}
          saveMeetingDetails={props.saveMeetingDetails}
          meetingTemplates={meetingTemplates}
          meetingMailTemplates={meetingEmailTemplates}
        />
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  fetchMeetingEmailTemplates: () => dispatch(getMeetingTemplateSetting()),
  getMeetingPDFPreview: (
    projectId: number,
    meetingId: number,
    completed: boolean
  ) => dispatch(getMeetingPDFPreview(projectId, meetingId, completed, false)),
  saveMeetingDetails: (projectID: number, data: IProjectMeeting, api: string) =>
    dispatch(saveMeetingDetails(projectID, data, api)),
  postActionItem: (projectId: number, data: IProjectDetailItem) =>
    dispatch(postActionItem(projectId, data, "action-items")),
  deleteMeeting: (projectId: number, meetingId: number, api: string) =>
    dispatch(deleteActionItem(projectId, meetingId, api)),
  fetchMeetingTemplates: () => dispatch(meetingTemplateCRUD("get")),
});

export default connect(mapStateToProps, mapDispatchToProps)(MeetingList);
