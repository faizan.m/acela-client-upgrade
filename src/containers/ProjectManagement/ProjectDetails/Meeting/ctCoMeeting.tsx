import React, { Component } from "react";
import _ from "lodash";
import "quill-mention";
import { cloneDeep } from "lodash";
import ReactQuill from "react-quill"; // ES6
import { Mention, MentionsInput } from "react-mentions";
import { commonFunctions } from "../../../../utils/commonFunctions";

interface ICTCOMeetingState {}

interface ICTCOMeetingProps {
  viewOnly: boolean;
  data: IMeetingEmailTemplate;
  setData: (data: IMeetingEmailTemplate) => void;
}

class CTCOMeeting extends Component<ICTCOMeetingProps, ICTCOMeetingState> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  customerMailBodyRef: React.LegacyRef<ReactQuill>;

  constructor(props: ICTCOMeetingProps) {
    super(props);
    this.state = this.getEmptyState();
    this.customerMailBodyRef = React.createRef();
  }

  getEmptyState = () => ({});

  mentionModule = {
    allowedChars: /^[A-Za-z\s]*$/,
    mentionDenotationChars: ["@", "#", "$", "%"],
    defaultMenuOrientation: "bottom",
    source: (searchTerm, renderList, mentionChar) => {
      let values = [];

      if (mentionChar === "#") {
        values = commonFunctions.emailTemplateVariables;
      }

      renderList(values, searchTerm);
    },
  };

  handleChangeMailSubject = (e: React.ChangeEvent<HTMLInputElement>) => {
    const meetingTemplate = cloneDeep(this.props.data);
    const regex = /#RULE#/gi;
    const mailSubject = e.target.value;
    (meetingTemplate.email_subject as string) = mailSubject.replace(regex, "");
    this.setState({ meetingTemplate, showActionButtons: true });
    this.props.setData(meetingTemplate);
  };

  getContentFromObject = (object) => {
    let text = null;
    text =
      object &&
      object.ops.map((t) =>
        t.insert.hasOwnProperty("mention")
          ? _.get(t, "insert.mention.value", "")
          : t.insert
      );
    return text && text.join(" ");
  };

  handleChangeQuill = (content: string, editor) => {
    const object = editor.getContents(content);
    const meetingTemplate = cloneDeep(this.props.data);
    const emailBodyText = this.getContentFromObject(object);
    const mailBodyTxtArr = emailBodyText.split(" ");

    const mailBodyTxtFinalArr = [];
    mailBodyTxtArr.map((word) => {
      if (word.startsWith("{")) {
        word = "#" + word;
      }
      mailBodyTxtFinalArr.push(word);
    });

    (meetingTemplate.email_body_text as string) = mailBodyTxtFinalArr.join(" ");
    (meetingTemplate.email_body_markdown as string) = content;
    this.props.setData(meetingTemplate);
  };

  render() {
    return (
      <div className="ct-co-email-container col-md-12">
        <div className="col-md-12 row customer-email-field">
          <label className="col-md-3 field__label-label email-subject">
            <b>Email Subject</b>
          </label>
          <div className="mail-subject-mention-input">
            <MentionsInput
              markup="[__display__]"
              disabled={this.props.viewOnly}
              value={this.props.data.email_subject || ""}
              onChange={(e) => this.handleChangeMailSubject(e)}
              className={"outer"}
            >
              <Mention
                trigger="#"
                data={commonFunctions.rangeVariables()}
                className={"inner-drop"}
                markup="#RULE#__display__"
                disabled={true}
              />
            </MentionsInput>
          </div>
        </div>
        <div className="col-md-12 row customer-email-field">
          <div className="email-editor-wrapper">
            <ReactQuill
              readOnly={this.props.viewOnly}
              className="email-body-editor"
              ref={this.customerMailBodyRef}
              value={this.props.data.email_body_markdown || ""}
              onChange={(content, delta, source, editor) =>
                this.handleChangeQuill(content, editor)
              }
              modules={{ mention: this.mentionModule }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default CTCOMeeting;
