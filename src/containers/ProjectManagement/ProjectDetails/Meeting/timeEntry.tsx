import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep, isNil } from "lodash";
import { connect } from "react-redux";
import moment, { Moment } from "moment";
import {
  fetchTimeEntries,
  timeEntrySetting,
  createTimeEntries,
  TIME_ENTRY_SUCCESS,
  TIME_ENTRY_FAILURE,
  TIME_ENTRY_SETTING_SUCCESS,
  projectDefaultTETicketCRU,
  DEFAULT_TIME_ENTRY_TICKET_SUCCESS,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Spinner from "../../../../components/Spinner";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import ModalBase from "../../../../components/ModalBase/modalBase";
import EditButton from "../../../../components/Button/editButton";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";
import { commonFunctions } from "../../../../utils/commonFunctions";
import {
  toFormattedDate,
  getAMPMFormattedTime,
} from "../../../../utils/CalendarUtil";

interface TimeEntryModalProps extends MeetingCloseProps {
  isMeetingComplete: boolean;
  ticketList: IPickListOptions[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchTimeEntryDefaultNotes: () => Promise<any>;
  fetchTimeEntries: (meetingID: number) => Promise<any>;
  fetchTimeEntryDefaultTicket: (projectId: number) => Promise<any>;
  createTimeEntries: (
    projectID: number,
    meetingID: number,
    data: MeetingTimeEntry
  ) => Promise<any>;
}

interface EditTimeEntry extends TimeEntry {
  isPM: boolean;
  index?: number;
  projectManagerName: string;
}

interface CreatedTEDetails {
  id: number;
  meeting: number;
  user: string;
  entry_type: string;
  crm_id: number;
  ticket_crm_id: number;
  success: boolean;
  entry_details: {
    notes: string;
    errors: string[];
    end_time: string;
    start_time: string;
  };
}

interface TimeEntryErrors {
  [userId: string]: {
    success: boolean;
    errors: string[];
  };
}

const getEmptyTimeEntry = () => ({
  start_time: moment(),
  end_time: moment().add(1, "hours"),
  notes: "",
  ticket_id: null,
  user_id: "",
});

const EditTimeEntryModal = (props: {
  data: EditTimeEntry;
  ticketOptions: IPickListOptions[];
  engineerOptions: IPickListOptions[];
  onCloseEditModal: (save: boolean, timeEntry?: EditTimeEntry) => void;
}) => {
  const [timeEntry, setTimeEntry] = useState<EditTimeEntry>({
    ...getEmptyTimeEntry(),
    isPM: false,
    index: null,
    projectManagerName: "",
  });
  const [errors, setErrors] = useState<IErrorValidation>({});

  useEffect(() => {
    setTimeEntry(cloneDeep(props.data));
  }, [props.data]);

  const handleChangeFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTimeEntry((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const handleChangeStartClose = (e: {
    target: { name: string; value: Moment };
  }) => {
    const entry = cloneDeep(timeEntry);
    const targetValue = e.target.value;
    if (e.target.name === "start_time") {
      entry.start_time = targetValue;
      const end = moment(targetValue).add(1, "hours");
      entry.end_time = end;
    } else {
      entry.end_time = targetValue;
    }
    setTimeEntry(entry);
  };

  const isValid = (): boolean => {
    let isValid: boolean = true;
    const err = cloneDeep(errors);

    const requiredFields = ["start_time", "end_time", "ticket_id", "user_id"];

    requiredFields.forEach((field) => {
      if (!timeEntry[field]) {
        isValid = false;
        err[field] = commonFunctions.getErrorState("This field is required");
      } else err[field] = commonFunctions.getErrorState();
    });

    setErrors(err);
    return isValid;
  };

  const onSaveClick = () => {
    if (isValid()) {
      props.onCloseEditModal(true, timeEntry);
    }
  };

  const renderBody = () => {
    return (
      <div className="time-entry-edit-container">
        <Input
          field={{
            label: timeEntry.isPM ? "Project Manager" : "Engineer",
            type: timeEntry.isPM
              ? "TEXT"
              : "PICKLIST",
            value: timeEntry.isPM
              ? timeEntry.projectManagerName
              : timeEntry.user_id,
            options: props.engineerOptions,
            isRequired: true,
          }}
          width={6}
          multi={false}
          name="user_id"
          onChange={handleChangeFields}
          placeholder={`Select Engineer`}
          clearable={true}
          disabled={timeEntry.isPM}
          error={errors.user_id}
        />
        <Input
          field={{
            label: "Ticket",
            type: "PICKLIST",
            value: timeEntry.ticket_id,
            options: props.ticketOptions,
            isRequired: true,
          }}
          width={12}
          multi={false}
          name="ticket_id"
          onChange={handleChangeFields}
          placeholder={`Select Ticket`}
          className="te-ticket-selection"
          error={errors.ticket_id}
        />
        <Input
          field={{
            label: "Date",
            type: "DATE",
            value: timeEntry.start_time,
            isRequired: true,
          }}
          width={4}
          labelIcon={"info"}
          name="start_time"
          onChange={handleChangeStartClose}
          placeholder={`Select Date`}
          showTime={false}
          disablePrevioueDates={true}
        />
        <Input
          field={{
            label: "Start Time",
            type: "DATE",
            value: timeEntry.start_time,
            isRequired: true,
          }}
          width={4}
          labelIcon={"info"}
          name="start_time"
          onChange={handleChangeStartClose}
          showTime={true}
          showDate={false}
          hideIcon={true}
          disablePrevioueDates={false}
        />
        <Input
          field={{
            label: "End Time",
            type: "DATE",
            value: timeEntry.end_time,
            isRequired: true,
          }}
          width={4}
          labelIcon={"info"}
          name="end_time"
          onChange={handleChangeStartClose}
          showTime={true}
          showDate={false}
          hideIcon={true}
          className="to-time"
          disablePrevioueDates={false}
        />
        <Input
          field={{
            value: timeEntry.notes,
            label: "Notes",
            type: "TEXTAREA",
            isRequired: false,
          }}
          width={12}
          name="notes"
          onChange={handleChangeFields}
          placeholder="Enter notes"
          className="notes-height-sm"
        />
      </div>
    );
  };

  return (
    <ModalBase
      show={true}
      onClose={() => props.onCloseEditModal(false)}
      backdropClassName="edit-time-entry-modal-backdrop"
      titleElement={`Edit Time Entry`}
      bodyElement={renderBody()}
      footerElement={
        <>
          <SquareButton
            content={"Close"}
            bsStyle={"default"}
            onClick={() => props.onCloseEditModal(false)}
          />
          <SquareButton
            content={"Save Entry"}
            bsStyle={"primary"}
            onClick={onSaveClick}
          />
        </>
      }
      className="edit-time-entry-modal"
    />
  );
};

const TimeEntryErrorLogsModal = ({
  onClose,
  timeEntries,
}: {
  onClose: () => void;
  timeEntries: MeetingTimeEntry;
}) => {
  const renderBody = () => {
    return (
      <div className="te-error-list">
        <div className="te-error-row te-error-row-heading">
          <div>Name</div>
          <div>Ticket Number</div>
          <div>Error</div>
        </div>
        {[timeEntries.pm_time_entry, ...timeEntries.engineer_time_entries]
          .filter((el) => !el.additionalInfo.success)
          .map((entry, idx) => (
            <div className="te-error-row" key={idx}>
              <div>{entry.additionalInfo.user_name}</div>
              <div>{entry.additionalInfo.ticket_name}</div>
              <div>{entry.additionalInfo.errors}</div>
            </div>
          ))}
      </div>
    );
  };

  return (
    <ModalBase
      show={true}
      onClose={onClose}
      backdropClassName="edit-time-entry-modal-backdrop"
      titleElement={`Time Entry Errors`}
      bodyElement={renderBody()}
      footerElement={
        <>
          <SquareButton
            content={"Close"}
            bsStyle={"default"}
            onClick={onClose}
          />
        </>
      }
      className="time-entry-errors-modal"
    />
  );
};

const TimeEntryModal: React.FC<TimeEntryModalProps> = (props) => {
  const [status, setStatus] = useState<"create" | "review" | "recreate">(
    "create"
  );
  const [loading, setLoading] = useState<boolean>(false);
  const [showValidation, setShowValidation] = useState<boolean>(false);
  const [editTimeEntry, setEditTimeEntry] = useState<EditTimeEntry>();
  const [defaultNotes, setDefaultNotes] = useState<TimeEntrySetting>({
    default_pm_note: "",
    default_engineer_note: "",
  });
  const [defaultTickets, setDefaultTickets] = useState<TimeEntryTickets>({
    default_pm_ticket_id: null,
    default_engineer_ticket_id: null,
  });
  const [timeEntries, setTimeEntries] = useState<MeetingTimeEntry>({
    pm_time_entry: null,
    engineer_time_entries: [],
  });
  const [timeEntryError, setTimeEntryError] = useState({
    hasErrors: false,
    showModal: false,
  });

  const engineerOptions: IPickListOptions[] = useMemo(() => {
    return props.engineers
      ? props.engineers.map((el) => ({
          value: el.id,
          label: `${el.first_name} - ${el.last_name}`,
        }))
      : [];
  }, [props.engineers]);

  useEffect(() => {
    fetchTimeEntryLogs();
  }, []);

  const setDefaultTimeEntries = () => {
    const attendeesSet: Set<string> = new Set(
      props.attendees ? props.attendees.map((el) => el.attendee) : []
    );
    const timeEntry: MeetingTimeEntry = {
      pm_time_entry: {
        start_time: moment(props.meetingDetails.schedule_start_datetime),
        end_time: moment(props.meetingDetails.schedule_end_datetime),
        user_id: props.projectDetails.project_manager_id,
        notes: "",
        ticket_id: null,
      },
      engineer_time_entries: props.engineers
        .filter((el) => attendeesSet.has(el.id))
        .map((el) => ({
          start_time: moment(props.meetingDetails.schedule_start_datetime),
          end_time: moment(props.meetingDetails.schedule_end_datetime),
          user_id: el.id,
          notes: "",
          ticket_id: null,
        })),
    };
    setTimeEntries(timeEntry);
    fetchDefaultNotes(timeEntry);
  };

  const setTimeEntriesFromLogs = (createdTEs: CreatedTEDetails[]) => {
    let hasErrors = false;
    const cloneTE: MeetingTimeEntry = {
      pm_time_entry: null,
      engineer_time_entries: [],
    };
    createdTEs.forEach((createdEntry) => {
      const entry: TimeEntry = {
        start_time: moment(createdEntry.entry_details.start_time),
        end_time: moment(createdEntry.entry_details.end_time),
        notes: createdEntry.entry_details.notes,
        ticket_id: createdEntry.ticket_crm_id,
        user_id: createdEntry.user,
        additionalInfo: {
          success: createdEntry.success,
          errors: createdEntry.entry_details.errors,
          user_name:
            createdEntry.entry_type === "Project Manager"
              ? props.projectDetails.project_manager
              : getString(createdEntry.user, "engineer"),
          ticket_name: getString(createdEntry.ticket_crm_id, "ticket"),
        },
      };
      hasErrors = hasErrors || !createdEntry.success;
      if (createdEntry.entry_type === "Project Manager")
        cloneTE.pm_time_entry = entry;
      else cloneTE.engineer_time_entries.push(entry);
    });
    setTimeEntryError({
      hasErrors,
      showModal: false,
    });
    setTimeEntries(cloneTE);
  };

  const fetchTimeEntryLogs = () => {
    setLoading(true);
    props
      .fetchTimeEntries(props.meetingId)
      .then((action) => {
        if (action.type === TIME_ENTRY_SUCCESS) {
          const createdTEs: CreatedTEDetails[] = action.response;
          // If no logs are there, it means that no time entry was created
          if (createdTEs.length === 0) setDefaultTimeEntries();
          else {
            setStatus("review");
            setTimeEntriesFromLogs(createdTEs);
          }
        }
      })
      .finally(() => setLoading(false));
  };

  // TODO: Store default_notes in a context
  const fetchDefaultNotes = (entryParam: MeetingTimeEntry) => {
    Promise.all([
      props.fetchTimeEntryDefaultNotes(),
      props.fetchTimeEntryDefaultTicket(props.projectDetails.id),
    ]).then(([teNotes, teTickets]) => {
      const teClone = cloneDeep(entryParam);
      if (teNotes.type === TIME_ENTRY_SETTING_SUCCESS) {
        const response: TimeEntrySetting = teNotes.response;
        teClone.pm_time_entry.notes = response.default_pm_note;
        teClone.engineer_time_entries = teClone.engineer_time_entries.map(
          (entry) => ({
            ...entry,
            notes: response.default_engineer_note,
          })
        );
        setDefaultNotes(response);
      }
      if (teTickets.type === DEFAULT_TIME_ENTRY_TICKET_SUCCESS) {
        const response: TimeEntryTickets = teTickets.response;
        teClone.pm_time_entry.ticket_id = response.default_pm_ticket_id;
        teClone.engineer_time_entries = teClone.engineer_time_entries.map(
          (entry) => ({
            ...entry,
            ticket_id: response.default_engineer_ticket_id,
          })
        );
        setDefaultTickets(response);
      }
      setTimeEntries(teClone);
    });
  };

  const onEditClick = (timeEntry: TimeEntry, index: number) => {
    setEditTimeEntry({
      index,
      isPM: index === -1,
      projectManagerName:
        index === -1 ? props.projectDetails.project_manager : "",
      ...timeEntry,
    });
  };

  const handleCloseEditModal = (
    save: boolean,
    edittedTimeEntry: EditTimeEntry
  ) => {
    if (save) {
      const cloneTimeEntry = cloneDeep(timeEntries);
      const timeEntry: TimeEntry = {
        start_time: edittedTimeEntry.start_time,
        end_time: edittedTimeEntry.end_time,
        notes: edittedTimeEntry.notes,
        ticket_id: edittedTimeEntry.ticket_id,
        user_id: edittedTimeEntry.user_id,
        additionalInfo: edittedTimeEntry.additionalInfo,
      };
      if (edittedTimeEntry.isPM) cloneTimeEntry.pm_time_entry = timeEntry;
      else
        cloneTimeEntry.engineer_time_entries[
          edittedTimeEntry.index
        ] = timeEntry;
      setTimeEntries(cloneTimeEntry);
    }
    setEditTimeEntry(null);
  };

  const addNewEngineerEntry = () => {
    const timeEntryClone = cloneDeep(timeEntries);
    timeEntryClone.engineer_time_entries.push({
      start_time: moment(props.meetingDetails.schedule_start_datetime),
      end_time: moment(props.meetingDetails.schedule_end_datetime),
      notes: defaultNotes.default_engineer_note,
      ticket_id: defaultTickets.default_engineer_ticket_id,
      user_id: "",
    });
    setTimeEntries(timeEntryClone);
  };

  const deleteEngineerEntry = (index: number) => {
    const timeEntryClone = cloneDeep(timeEntries);
    timeEntryClone.engineer_time_entries.splice(index, 1);
    setTimeEntries(timeEntryClone);
  };

  const getValidationError = (): string => {
    let isValid = true;
    const requiredFields = ["start_time", "end_time", "ticket_id", "user_id"];
    const checkTimeEntry = (entry: TimeEntry) => {
      let valid = true;
      requiredFields.forEach((field) => {
        if (!entry[field]) valid = valid && false;
      });
      return valid;
    };

    if (timeEntries.pm_time_entry)
      isValid = isValid && checkTimeEntry(timeEntries.pm_time_entry);
    timeEntries.engineer_time_entries.forEach((entry) => {
      isValid = isValid && checkTimeEntry(entry);
    });

    if (!isValid) {
      return "Please select all the required fields";
    }

    const uniqueIds: Set<string> = new Set(
      timeEntries.engineer_time_entries.map((el) => el.user_id)
    );
    if (uniqueIds.size !== timeEntries.engineer_time_entries.length) {
      return "All engineers must be unique";
    }

    return "";
  };

  const getEntryWithAdditionalInfo = (
    timeEntry: TimeEntry,
    errors: TimeEntryErrors,
    isPM: boolean = false
  ): TimeEntry => {
    const newEntry = cloneDeep(timeEntry);
    newEntry.additionalInfo = {
      success:
        timeEntry.user_id in errors ? errors[timeEntry.user_id].success : true,
      errors:
        timeEntry.user_id in errors ? errors[timeEntry.user_id].errors : [],
      user_name: isPM
        ? props.projectDetails.project_manager
        : getString(newEntry.user_id, "engineer"),
      ticket_name: getString(newEntry.ticket_id, "ticket"),
    };
    return newEntry;
  };

  const onRecreateTimeEntries = () => {
    setShowValidation(true);
    if (!Boolean(getValidationError())) {
      setShowValidation(false);
      const timeEntryPayload = cloneDeep(timeEntries);
      timeEntryPayload.pm_time_entry = !timeEntryPayload.pm_time_entry
        .additionalInfo.success
        ? timeEntryPayload.pm_time_entry
        : undefined;
      timeEntryPayload.engineer_time_entries = timeEntryPayload.engineer_time_entries.filter(
        (el) => !el.additionalInfo.success
      );
      if (timeEntryPayload.pm_time_entry)
        timeEntryPayload.pm_time_entry.additionalInfo = undefined;
      if (timeEntryPayload.engineer_time_entries.length) {
        timeEntryPayload.engineer_time_entries = timeEntryPayload.engineer_time_entries.map(
          (el) => {
            delete el.additionalInfo;
            return el;
          }
        );
      } else {
        timeEntryPayload.engineer_time_entries = undefined;
      }
      setLoading(true);
      props
        .createTimeEntries(
          props.projectDetails.id,
          props.meetingId,
          timeEntryPayload
        )
        .then((action) => {
          if (action.type === TIME_ENTRY_SUCCESS) {
            const response: TimeEntryErrors = action.response;
            const newTimeEntry: MeetingTimeEntry = {
              pm_time_entry: getEntryWithAdditionalInfo(
                timeEntries.pm_time_entry,
                response,
                true
              ),
              engineer_time_entries: timeEntries.engineer_time_entries.map(
                (el) => getEntryWithAdditionalInfo(el, response)
              ),
            };
            setTimeEntries(newTimeEntry);
            const hasErrors = Object.values(response).reduce((prev, curr) => {
              return prev || !curr.success;
            }, false);
            setTimeEntryError({
              hasErrors,
              showModal: false,
            });
            fetchTimeEntryLogs();
          } else if (action.type === TIME_ENTRY_FAILURE) {
            props.addErrorMessage("Error editing time entries!");
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const onCreateTimeEntries = () => {
    setShowValidation(true);
    if (!Boolean(getValidationError())) {
      setLoading(true);
      setShowValidation(false);
      props
        .createTimeEntries(
          props.projectDetails.id,
          props.meetingId,
          timeEntries
        )
        .then((action) => {
          if (action.type === TIME_ENTRY_SUCCESS) {
            const response: TimeEntryErrors = action.response;
            const newTimeEntry: MeetingTimeEntry = {
              pm_time_entry: getEntryWithAdditionalInfo(
                timeEntries.pm_time_entry,
                response,
                true
              ),
              engineer_time_entries: timeEntries.engineer_time_entries.map(
                (el) => getEntryWithAdditionalInfo(el, response)
              ),
            };
            setTimeEntries(newTimeEntry);
            const hasErrors = Object.values(response).reduce((prev, curr) => {
              return prev || !curr.success;
            }, false);
            setTimeEntryError({
              hasErrors,
              showModal: false,
            });
            setStatus("review");
          } else if (action.type === TIME_ENTRY_FAILURE) {
            props.addErrorMessage("Error creating time entries!");
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const getString = (
    str: string | number,
    type?: "engineer" | "ticket"
  ): string => {
    if (type && str) {
      const options = type === "engineer" ? engineerOptions : props.ticketList;
      const res = options.find((el) => el.value === str);
      return res ? (res.label as string) : "-";
    }
    return str ? String(str) : "-";
  };

  const toggleErrorLogModal = () => {
    setTimeEntryError((prev) => ({
      ...prev,
      showModal: !prev.showModal,
    }));
  };

  const getBody = () => {
    const editMode: boolean = status === "create" || status === "recreate";
    const shouldShowEdit = (timeEntry: TimeEntry) =>
      timeEntry.additionalInfo ? !timeEntry.additionalInfo.success : true;
    const getStatusMarkup = (success: boolean) =>
      success ? (
        <div className="te-status-success">Success</div>
      ) : (
        <div className="te-status-error">Failed</div>
      );

    return (
      <div className="cm-time-entry-container">
        <Spinner show={loading} className="close-meeting-te-spinner" />
        {status === "create" && <h3>Time Entry Creation</h3>}
        {status === "review" && <h3>Time Entry Status</h3>}
        {status === "recreate" && <h3>Edit Failed Time Entries</h3>}
        {status !== "review" && showValidation && (
          <div className="time-entry-show-error">{getValidationError()}</div>
        )}
        {noData ? (
          <div className="no-data">Logged Time Entry Data Not Found</div>
        ) : (
          <div className="time-entries-list">
            <div className="time-entry-row time-entry-row-heading">
              <div>Name</div>
              <div>Date</div>
              <div>Start Time</div>
              <div>End Time</div>
              <div>Ticket</div>
              <div>Notes</div>
              <div>Work Role</div>
              {status === "review" && <div>Status</div>}
            </div>
            {timeEntries.pm_time_entry && (
              <div className="time-entry-row">
                <div title={getString(props.projectDetails.project_manager)}>
                  {getString(props.projectDetails.project_manager)}
                </div>
                <div
                  title={getString(
                    toFormattedDate(timeEntries.pm_time_entry.start_time)
                  )}
                >
                  {getString(
                    toFormattedDate(timeEntries.pm_time_entry.start_time)
                  )}
                </div>
                <div
                  title={getString(
                    getAMPMFormattedTime(timeEntries.pm_time_entry.start_time)
                  )}
                >
                  {getString(
                    getAMPMFormattedTime(timeEntries.pm_time_entry.start_time)
                  )}
                </div>
                <div
                  title={getString(
                    getAMPMFormattedTime(timeEntries.pm_time_entry.end_time)
                  )}
                >
                  {getString(
                    getAMPMFormattedTime(timeEntries.pm_time_entry.end_time)
                  )}
                </div>
                <div
                  title={getString(
                    timeEntries.pm_time_entry.ticket_id,
                    "ticket"
                  )}
                >
                  {getString(timeEntries.pm_time_entry.ticket_id, "ticket")}
                </div>
                <div title={getString(timeEntries.pm_time_entry.notes)}>
                  {getString(timeEntries.pm_time_entry.notes)}
                </div>
                <div title="Project Manager">PM</div>
                {editMode ? (
                  <div className="time-entry-actions">
                    {shouldShowEdit(timeEntries.pm_time_entry) && (
                      <EditButton
                        title="Edit Time Entry"
                        onClick={() =>
                          onEditClick(timeEntries.pm_time_entry, -1)
                        }
                      />
                    )}
                  </div>
                ) : (
                  getStatusMarkup(
                    timeEntries.pm_time_entry.additionalInfo &&
                      timeEntries.pm_time_entry.additionalInfo.success
                  )
                )}
              </div>
            )}
            {Boolean(
              timeEntries.engineer_time_entries &&
                timeEntries.engineer_time_entries.length
            ) &&
              timeEntries.engineer_time_entries.map((entry, idx) => (
                <div className="time-entry-row" key={idx}>
                  <div title={getString(entry.user_id, "engineer")}>
                    {getString(entry.user_id, "engineer")}
                  </div>
                  <div title={getString(toFormattedDate(entry.start_time))}>
                    {getString(toFormattedDate(entry.start_time))}
                  </div>
                  <div
                    title={getString(getAMPMFormattedTime(entry.start_time))}
                  >
                    {getString(getAMPMFormattedTime(entry.start_time))}
                  </div>
                  <div title={getString(getAMPMFormattedTime(entry.end_time))}>
                    {getString(getAMPMFormattedTime(entry.end_time))}
                  </div>
                  <div title={getString(entry.ticket_id, "ticket")}>
                    {getString(entry.ticket_id, "ticket")}
                  </div>
                  <div title={getString(entry.notes)}>
                    {getString(entry.notes)}
                  </div>
                  <div title="Engineer">Engineer</div>
                  {editMode ? (
                    <div className="time-entry-actions">
                      {shouldShowEdit(entry) && (
                        <>
                          <EditButton
                            title="Edit Time Entry"
                            onClick={() => onEditClick(entry, idx)}
                          />
                          <SmallConfirmationBox
                            showButton={true}
                            onClickOk={() => deleteEngineerEntry(idx)}
                            text="time entry"
                          />
                        </>
                      )}
                    </div>
                  ) : (
                    getStatusMarkup(
                      entry.additionalInfo && entry.additionalInfo.success
                    )
                  )}
                </div>
              ))}

            {status === "create" && (
              <div
                className="add-new-engineer-entry"
                onClick={addNewEngineerEntry}
              >
                <span className="add-new-row-plus">+</span>
                <span className="add-new-row-text">
                  Add Engineer Time Entry
                </span>
              </div>
            )}
          </div>
        )}
      </div>
    );
  };

  const getFooter = () => {
    return (
      <>
        {noData ? (
          <SquareButton
            content={"Close"}
            bsStyle={"default"}
            onClick={() => props.onClose(true)}
          />
        ) : (
          status === "review" && (
            <>
              <SquareButton
                content={"Edit Failed Time Entries"}
                bsStyle={"link"}
                onClick={() => setStatus("recreate")}
              />
              <SquareButton
                content={"Close"}
                bsStyle={"default"}
                onClick={() => props.onClose(true)}
              />
              {timeEntryError.hasErrors && (
                <SquareButton
                  content={"Show Error Logs"}
                  bsStyle={"primary"}
                  onClick={toggleErrorLogModal}
                />
              )}
            </>
          )
        )}
        {status === "create" && (
          <>
            <SquareButton
              content={"Cancel"}
              bsStyle={"default"}
              onClick={() => props.onClose(true)}
            />
            <SquareButton
              content={"Create Time Entries"}
              bsStyle={"primary"}
              onClick={onCreateTimeEntries}
            />
          </>
        )}
        {status === "recreate" && (
          <SquareButton
            content={"Create Failed Entries"}
            bsStyle={"primary"}
            onClick={onRecreateTimeEntries}
          />
        )}
      </>
    );
  };

  const noData: boolean =
    !loading &&
    !timeEntries.pm_time_entry &&
    (!timeEntries.engineer_time_entries ||
      timeEntries.engineer_time_entries.length === 0);

  return (
    <>
      <ModalBase
        show={true}
        onClose={() => props.onClose(true)}
        closeOnEscape={false}
        titleElement={"Edit Time Entries"}
        bodyElement={getBody()}
        footerElement={getFooter()}
        className="close-meeting-modal"
      />
      {!isNil(editTimeEntry) && (
        <EditTimeEntryModal
          data={editTimeEntry}
          ticketOptions={props.ticketList}
          onCloseEditModal={handleCloseEditModal}
          engineerOptions={engineerOptions}
        />
      )}
      {timeEntryError.showModal && (
        <TimeEntryErrorLogsModal
          timeEntries={timeEntries}
          onClose={toggleErrorLogModal}
        />
      )}
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  ticketList: state.pmo.projectTickets,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (str: string) => dispatch(addErrorMessage(str)),
  addSuccessMessage: (str: string) => dispatch(addSuccessMessage(str)),
  fetchTimeEntries: (meetingID: number) =>
    dispatch(fetchTimeEntries(meetingID)),
  fetchTimeEntryDefaultNotes: () => dispatch(timeEntrySetting("get")),
  fetchTimeEntryDefaultTicket: (projectId: number) =>
    dispatch(projectDefaultTETicketCRU("get", projectId)),
  createTimeEntries: (
    projectID: number,
    meetingID: number,
    data: MeetingTimeEntry
  ) => dispatch(createTimeEntries(projectID, meetingID, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TimeEntryModal);
