import React, { useEffect, useMemo, useState } from "react";
import { cloneDeep, difference, union } from "lodash";
import { connect } from "react-redux";
import {
  uploadImage,
  UPLOAD_IMAGE_FAILURE,
  UPLOAD_IMAGE_SUCCESS,
} from "../../../../actions/sow";
import {
  saveMeetingDetails,
  getMeetingPDFPreviewV2,
  getMeetingEmailPreview,
  getMeetingEmailRecipients,
  MEETING_SUCCESS,
  MEETING_FAILURE,
  MEETING_EMAIL_RECEPIENTS_SUCCESS,
  FETCH_MEETING_PDF_PREVIEW_SUCCESS,
  FETCH_MEETING_EMAIL_PREVIEW_SUCCESS,
} from "../../../../actions/pmo";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import Spinner from "../../../../components/Spinner";
import Input from "../../../../components/Input/input";
import SquareButton from "../../../../components/Button/button";
import Checkbox from "../../../../components/Checkbox/checkbox";
import PDFViewer from "../../../../components/PDFViewer/PDFViewer";
import ModalBase from "../../../../components/ModalBase/modalBase";
import HTMLViewer from "../../../../components/HTMLViewer/HTMLViewer";
import { QuillEditorAcela } from "../../../../components/QuillEditor/QuillEditor";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { getURLBYMeetingType } from "../meetingDetails";
import SmallConfirmationBox from "../../../../components/SmallConfirmationBox/confirmation";

interface CMEmailPageProps extends MeetingCloseProps {
  meetingTemplates: IMeetingEmailTemplate[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  uploadImage: (file: FormData, name: string) => Promise<any>;
  getEmailRecipients: (projectId: number, meetingId: number) => Promise<any>;
  getPDFPreview: (
    projectId: number,
    meetingId: number,
    project_update: string
  ) => Promise<any>;
  getEmailPreview: (
    projectId: number,
    meetingId: number,
    email_data: CloseMeetingEmail
  ) => Promise<any>;
  saveMeetingDetails: (
    projectID: number,
    data: IProjectMeeting,
    api: string
  ) => Promise<any>;
  onSaveAndClose: (emailData: CloseMeetingEmail) => void;
}

interface EmailRecipients {
  team_member_emails: string[];
  internal_team_member_emails: string[];
  customer_contact_emails: string[];
  additional_attendee_emails: string[];
  primary_contact_email: string | null;
  account_manager_email: string | null;
}

const allowedFileTypes = new Set([
  "image/png",
  "image/jpeg",
  "image/jpeg",
  "application/pdf",
  "text/plain",
  "text/plain",
  "application/vnd.ms-project",
  "application/msword",
  "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  "application/vnd.ms-word.document.macroEnabled.12",
  "application/vnd.ms-excel",
  "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
  "application/vnd.ms-powerpoint",
  "application/zip",
  "text/csv",
  "application/dxf",
  "image/vnd.adobe.photoshop",
  "application/vnd.visio2010",
  "application/vnd.visio",
  "application/vnd.visio2013",
  "application/vnd.openxmlformats-officedocument.presentationml.presentation",
  "application/vnd.ms-powerpoint.presentation.macroEnabled.12",
]);

const replaceMentionHtml = (html: string): string => {
  const regex = /<span\s+?class="mention"\s+data-index="\d+?"\s+?data-denotation-char="#"\s+?data-id=".+?"\s+data-value="\{.+?\}"\s*?>.?<span\s+?contenteditable="false">.?<span class="ql-mention-denotation-char">#<\/span>{(.+?)}<\/span>.?<\/span>/g;
  return html ? html.replace(regex, "#{$1}") : "";
};

interface PreviewObj {
  data: string;
  view: "email" | "meeting_pdf";
}

// Function to check if meeting is Customer Touch or Close Out
const isCTOrCL = (type: MeetingType) =>
  type === "Customer Touch" || type === "Close Out";

const CloseMeetingEmailPage: React.FC<CMEmailPageProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(false);
  const [loadingRecipients, setLoadingRecipients] = useState<boolean>(false);
  const [errors, setErrors] = useState<IErrorValidation>({});
  const [preview, setPreview] = useState<PreviewObj>({
    data: "",
    view: null,
  });
  const [meetingEmails, setMeetingEmails] = useState<EmailRecipients>();
  const [emailControls, setEmailControls] = useState({
    add_internal_contacts: false,
    add_customer_contacts: false,
    add_additional_contacts: false,
  });
  const [emailSettings, setEmailSettings] = useState<CloseMeetingEmail>({
    email_subject: "",
    email_body: "",
    project_update: "",
    recipients: [],
    attachment: null,
  });

  const placeholderValues = useMemo(
    () => ({
      "#{Project Name}": props.projectDetails.title,
      "#{Customer Name}": props.projectDetails.customer,
      "#{Customer Primary Contact First Name}": props.projectDetails
        .primary_contact
        ? props.projectDetails.primary_contact.split(" ")[0]
        : "",
      "#{Customer Primary Contact}": props.projectDetails.primary_contact,
      "#{Project Owner}": props.projectDetails.project_manager,
    }),
    [props.projectDetails, props.meetingDetails]
  );

  useEffect(() => {
    fetchMeetingEmailRecipients();
  }, []);

  useEffect(() => {
    if (props.meetingTemplates && props.meetingTemplates.length) {
      const currentTemplate = props.meetingTemplates.find(
        (el) => el.meeting_type === props.meetingDetails.meeting_type
      );
      const replacePlaceholders = (text: string) => {
        let newText = text;
        Object.keys(placeholderValues).forEach((key) => {
          newText = newText.replace(key, placeholderValues[key]);
        });
        return newText;
      };
      const replaceMentionValue = (text: string) => {
        return replacePlaceholders(replaceMentionHtml(text));
      };

      if (currentTemplate) {
        if (props.meetingDetails.meeting_type === "Close Out") {
          setEmailSettings((prev) => ({
            ...prev,
            project_update: replaceMentionValue(currentTemplate.project_update),
            email_body: replaceMentionValue(
              props.meetingDetails.project_acceptance_markdown
            ),
            email_subject: replacePlaceholders(
              props.meetingDetails.email_subject
            ),
          }));
        } else if (
          props.meetingDetails.meeting_type === "Customer Touch"
        ) {
          setEmailSettings((prev) => ({
            ...prev,
            project_update: replaceMentionValue(currentTemplate.project_update),
            email_body: replaceMentionValue(props.meetingDetails.email_body),
            email_subject: replacePlaceholders(
              props.meetingDetails.email_subject
            ),
          }));
        } else {
          setEmailSettings((prev) => ({
            ...prev,
            project_update: replaceMentionValue(currentTemplate.project_update),
            email_body: replaceMentionValue(
              currentTemplate.email_body_markdown
            ),
            email_subject: replacePlaceholders(currentTemplate.email_subject),
          }));
        }
      }
    }
  }, [props.meetingTemplates, props.projectDetails, props.meetingDetails]);

  const fetchMeetingEmailRecipients = () => {
    setLoadingRecipients(true);
    const currentTemplate = props.meetingTemplates.find(
      (el) => el.meeting_type === props.meetingDetails.meeting_type
    );
    props
      .getEmailRecipients(props.projectDetails.id, props.meetingId)
      .then((action) => {
        if (action.type === MEETING_EMAIL_RECEPIENTS_SUCCESS) {
          const response: EmailRecipients = action.response;
          setMeetingEmails(response);
          let recipients: string[] = [];
          if (currentTemplate.add_additional_contacts)
            recipients = union(recipients, response.additional_attendee_emails);
          if (currentTemplate.add_customer_contacts)
            recipients = union(recipients, response.customer_contact_emails);
          if (currentTemplate.add_internal_contacts)
            recipients = union(
              recipients,
              response.internal_team_member_emails
            );
          if (isCTOrCL(props.meetingDetails.meeting_type as MeetingType)) {
            if (response.primary_contact_email)
              recipients = union(recipients, response.primary_contact_email);
            if (response.account_manager_email)
              recipients = union(recipients, response.account_manager_email);
          }
          setEmailSettings((prev) => ({
            ...prev,
            recipients,
          }));
          setEmailControls({
            add_additional_contacts: currentTemplate.add_additional_contacts,
            add_customer_contacts: currentTemplate.add_customer_contacts,
            add_internal_contacts: currentTemplate.add_internal_contacts,
          });
        }
      })
      .finally(() => setLoadingRecipients(false));
  };

  const getRecipientOptions = (): IPickListOptions[] => {
    const getOption = (
      s: string,
      disabled: boolean = false
    ): IPickListOptions => ({
      value: s,
      label: (
        <div
          title={
            disabled
              ? "Primary Contact and Account Manager are default selected in Customer Touch and Close Out meeting"
              : undefined
          }
        >
          {s}
        </div>
      ),
      disabled: disabled,
    });

    if (meetingEmails) {
      const isInternalKickoff =
        props.meetingDetails.meeting_type === "Internal Kickoff";
      let emails: string[] = meetingEmails.team_member_emails;
      if (emailControls.add_internal_contacts)
        emails = union(emails, meetingEmails.internal_team_member_emails);
      if (emailControls.add_customer_contacts && !isInternalKickoff)
        emails = union(emails, meetingEmails.customer_contact_emails);
      if (emailControls.add_additional_contacts && !isInternalKickoff)
        emails = union(emails, meetingEmails.additional_attendee_emails);

      if (isInternalKickoff && meetingEmails.primary_contact_email)
        emails = difference(emails, [meetingEmails.primary_contact_email]);

      if (isCTOrCL(props.meetingDetails.meeting_type as MeetingType)) {
        if (meetingEmails.primary_contact_email)
          emails = union(emails, [meetingEmails.primary_contact_email]);
        if (meetingEmails.account_manager_email)
          emails = union(emails, [meetingEmails.account_manager_email]);
      }

      return emails.map((el) =>
        getOption(
          el,
          isCTOrCL(props.meetingDetails.meeting_type as MeetingType) &&
            (el === meetingEmails.primary_contact_email ||
              el === meetingEmails.account_manager_email)
        )
      );
    }
    return [];
  };

  const onEmailControlChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const controlName = e.target.name;
    const checked = e.target.checked;
    let newRecipients: string[];

    if (controlName === "add_internal_contacts") {
      // This ensure that primary contact and account manager remain selected
      const internalContacts = isCTOrCL(
        props.meetingDetails.meeting_type as MeetingType
      )
        ? difference(meetingEmails.internal_team_member_emails, [
            meetingEmails.primary_contact_email,
            meetingEmails.account_manager_email,
          ])
        : meetingEmails.internal_team_member_emails;
      newRecipients = checked
        ? union(
            emailSettings.recipients,
            meetingEmails.internal_team_member_emails
          )
        : difference(emailSettings.recipients, internalContacts);
    } else if (controlName === "add_customer_contacts") {
      // This ensure that primary contact and account manager remain selected
      const customerContacts = isCTOrCL(
        props.meetingDetails.meeting_type as MeetingType
      )
        ? difference(meetingEmails.customer_contact_emails, [
            meetingEmails.primary_contact_email,
            meetingEmails.account_manager_email,
          ])
        : meetingEmails.customer_contact_emails;
      newRecipients = checked
        ? union(emailSettings.recipients, meetingEmails.customer_contact_emails)
        : difference(emailSettings.recipients, customerContacts);
    } else if (controlName === "add_additional_contacts") {
      // This ensure that primary contact and account manager remain selected
      const additionalAttendees = isCTOrCL(
        props.meetingDetails.meeting_type as MeetingType
      )
        ? difference(meetingEmails.additional_attendee_emails, [
            meetingEmails.primary_contact_email,
            meetingEmails.account_manager_email,
          ])
        : meetingEmails.additional_attendee_emails;

      newRecipients = checked
        ? union(
            emailSettings.recipients,
            meetingEmails.additional_attendee_emails
          )
        : difference(emailSettings.recipients, additionalAttendees);
    }
    setEmailControls((prev) => ({
      ...prev,
      [controlName]: checked,
    }));
    setEmailSettings((prev) => ({
      ...prev,
      recipients: newRecipients,
    }));
  };

  const closePreview = () => {
    setPreview({
      view: null,
      data: "",
    });
  };

  const onMeetingPDFPreview = () => {
    if (isValid()) {
      setLoading(true);
      props
        .getPDFPreview(
          props.projectDetails.id,
          props.meetingId,
          emailSettings.project_update
        )
        .then((a) => {
          if (a.type === FETCH_MEETING_PDF_PREVIEW_SUCCESS) {
            setPreview({
              view: "meeting_pdf",
              data: a.response.url,
            });
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const onEmailPreview = () => {
    if (isValid()) {
      setLoading(true);
      props
        .getEmailPreview(props.projectDetails.id, props.meetingId, {
          email_body: emailSettings.email_body,
          email_subject: emailSettings.email_subject,
          recipients: emailSettings.recipients,
          user_attachments: emailSettings.attachment
            ? [emailSettings.attachment.name]
            : [],
        })
        .then((a) => {
          if (a.type === FETCH_MEETING_EMAIL_PREVIEW_SUCCESS) {
            setPreview({
              view: "email",
              data: a.response.html,
            });
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const onImageUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const attachment: any = files[0];
    setLoading(true);
    if (
      attachment &&
      attachment.type &&
      ["image/png", "image/jpg", "image/jpeg"].includes(attachment.type)
    ) {
      const data = new FormData();
      if (attachment) {
        data.append("filename", attachment);
        props
          .uploadImage(data, attachment)
          .then((action) => {
            if (action.type === UPLOAD_IMAGE_SUCCESS) {
              props.addSuccessMessage("Image Uploaded Successfully!");
              let imageString = `<p><img src="${action.response.file_path}" style="max-width: 100%"></p>`;
              setEmailSettings((prevState) => ({
                ...prevState,
                email_body: prevState.email_body + imageString,
              }));
            }
            if (action.type === UPLOAD_IMAGE_FAILURE) {
              props.addErrorMessage("Image Upload Failed!");
            }
          })
          .finally(() => setLoading(false));
      }
    } else {
      props.addErrorMessage("Invalid image format.");
    }
  };

  const isValid = () => {
    let isValid = true;
    const err = cloneDeep(errors);
    if (!emailSettings.email_subject || !emailSettings.email_subject.trim()) {
      err.email_subject = commonFunctions.getErrorState(
        "This field is required"
      );
      isValid = false;
    } else err.email_subject = commonFunctions.getErrorState();
    if (commonFunctions.isEditorEmpty(emailSettings.email_body)) {
      err.email_body = commonFunctions.getErrorState("This field is required");
      isValid = false;
    } else err.email_body = commonFunctions.getErrorState();

    if (commonFunctions.isEditorEmpty(emailSettings.project_update)) {
      err.project_update = commonFunctions.getErrorState(
        "This field is required"
      );
      isValid = false;
    } else err.project_update = commonFunctions.getErrorState();

    if (!emailSettings.recipients.length) {
      err.recipients = commonFunctions.getErrorState("This field is required");
      isValid = false;
    } else err.recipients = commonFunctions.getErrorState();

    setErrors(err);
    return isValid;
  };

  const onSaveAndClose = () => {
    if (isValid()) {
      const projectMeeting = cloneDeep(props.meetingDetails);

      if (
        !projectMeeting.agenda_topics ||
        (projectMeeting.agenda_topics &&
          projectMeeting.agenda_topics.length === 0)
      ) {
        projectMeeting.agenda_topics = null;
      }

      projectMeeting.project_update = emailSettings.project_update;
      if (projectMeeting.meeting_type === "Internal Kickoff")
        projectMeeting.is_internal = true;
      else if (projectMeeting.meeting_type === "Close Out") {
        projectMeeting.email_subject = emailSettings.email_subject;
        projectMeeting.project_acceptance_markdown = emailSettings.email_body;
      } else if (projectMeeting.meeting_type === "Customer Touch") {
        projectMeeting.email_subject = emailSettings.email_subject;
        projectMeeting.email_body = emailSettings.email_body;
      }

      let url = `${getURLBYMeetingType(projectMeeting.meeting_type)}/${
        props.meetingId
      }`;
      setLoading(true);
      props
        .saveMeetingDetails(props.projectDetails.id, projectMeeting, url)
        .then((action) => {
          if (action.type === MEETING_SUCCESS) {
            props.onSaveAndClose(emailSettings);
          } else if (action.type === MEETING_FAILURE) {
            props.addErrorMessage("Error saving meeting!");
          }
        })
        .finally(() => setLoading(false));
    }
  };

  const handleChangeEmailFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEmailSettings((prev) => ({
      ...prev,
      [e.target.name]: e.target.value,
    }));
  };

  const handleFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const attachment: File = e.target.files[0];
    const maxSize = 10 * 1024 * 1024; // 10MB in bytes
    if (!allowedFileTypes.has(attachment.type)) {
      props.addErrorMessage("Please upload a file with valid extension!");
      return;
    }
    if (attachment.size > maxSize) {
      props.addErrorMessage("File size exceed maximum limit of 10MB!");
      return;
    }
    setEmailSettings((prev) => ({
      ...prev,
      attachment,
    }));
  };

  const removeAttachment = () => {
    setEmailSettings((prev) => ({ ...prev, attachment: null }));
  };

  const handleChangeEditor = (
    html: string,
    key: "project_update" | "email_body"
  ) => {
    setEmailSettings((prev) => ({
      ...prev,
      [key]: html,
    }));
  };

  const getBody = () => {
    const isInternalKickoff =
      props.meetingDetails.meeting_type === "Internal Kickoff";

    return (
      <>
        <div className="close-meeting-email-container">
          <Spinner
            show={loading || loadingRecipients}
            className="close-meeting-spinner"
          />
          <section className="email-container-left">
            <h3>Email Details</h3>
            <SquareButton
              content={
                <div>
                  <img
                    className="attachments__icon__upload"
                    src="/assets/icons/cloud.png"
                  />
                  Upload Attachment
                  <input type="file" onChange={handleFile} />
                </div>
              }
              bsStyle={"primary"}
              onClick={() => null}
              className="attachment-btn"
            />
            <div className="email-detail-field">
              <label className="email-detail-label" htmlFor="Send to email">
                To
              </label>
              <Input
                field={{
                  label: null,
                  type: "PICKLIST",
                  value: emailSettings.recipients,
                  isRequired: true,
                  options: getRecipientOptions(),
                }}
                multi={true}
                width={12}
                name="recipients"
                onChange={handleChangeEmailFields}
                placeholder={`Select Recipients`}
                loading={loadingRecipients}
                error={errors.recipients}
              />
            </div>
            <div className="email-detail-field">
              <label className="email-detail-label" htmlFor="Email Subject">
                Subject
              </label>
              <Input
                field={{
                  label: null,
                  type: "TEXT",
                  value: emailSettings.email_subject,
                  isRequired: false,
                }}
                width={12}
                name="email_subject"
                onChange={handleChangeEmailFields}
                placeholder={`Enter Subject`}
                error={errors.email_subject}
              />
            </div>
            {Boolean(emailSettings.attachment) && (
              <div className="email-detail-field attachment-section">
                <label
                  className="email-detail-label"
                  htmlFor="Attachments"
                  title="Attachment"
                >
                  Attachment
                </label>
                <div className="view-attachments">
                  <div className="attachment-box">
                    <span className="attachment-link">
                      {emailSettings.attachment.name}
                    </span>
                    <SmallConfirmationBox
                      text="attachment"
                      onClickOk={removeAttachment}
                    />
                  </div>
                </div>
              </div>
            )}
            <div className="email-detail-field">
              <label className="btn square-btn btn-default upload-btn-container">
                <img
                  className="icon-upload"
                  src="/assets/icons/photo-camera.svg"
                  title="Upload image"
                />
                <input
                  type="file"
                  name=""
                  accept="image/jpg, image/jpeg, image/png"
                  className="custom-file-input"
                  onChange={(e) => onImageUpload(e)}
                  onClick={(event: any) => {
                    event.target.value = null;
                  }}
                />
              </label>
              <QuillEditorAcela
                scrollingContainer=".email-container-left"
                onChange={(html: string) =>
                  handleChangeEditor(html, "email_body")
                }
                value={emailSettings.email_body}
                hideTable={true}
                customToolbar={[
                  ["bold", "italic", "underline", "strike", "custom-image"],
                  [{ color: [] }, { background: [] }],
                  [{ list: "ordered" }, { list: "bullet" }],
                  [{ header: [1, 2, 3, 4, 5, 6, false] }],
                ]}
                wrapperClass="cm-email-body"
                error={errors.email_body}
              />
            </div>
            <div className="email-detail-field">
              <QuillEditorAcela
                hideFullscreen={true}
                label="Project Updates"
                scrollingContainer=".email-container-left"
                onChange={(html: string) =>
                  handleChangeEditor(html, "project_update")
                }
                value={emailSettings.project_update}
                wrapperClass="cm-project-updates"
                error={errors.project_update}
              />
            </div>
          </section>
          <section className="email-container-right">
            <div className="cm-btn-container">
              <SquareButton
                content="Preview PDF"
                onClick={onMeetingPDFPreview}
                className=""
                bsStyle={"primary"}
                title={
                  isCTOrCL(props.meetingDetails.meeting_type as MeetingType)
                    ? "Preview not available for this meeting type"
                    : undefined
                }
                disabled={
                  loading ||
                  isCTOrCL(props.meetingDetails.meeting_type as MeetingType)
                }
              />
              <SquareButton
                content="View Email"
                onClick={onEmailPreview}
                bsStyle={"primary"}
                disabled={loading || loadingRecipients}
              />
              <SquareButton
                content="Save & Send"
                onClick={onSaveAndClose}
                bsStyle={"primary"}
                disabled={loading || loadingRecipients}
              />
              <SquareButton
                content="Cancel"
                onClick={() => props.onClose(false)}
                bsStyle={"default"}
                disabled={loading}
              />
            </div>
            <div className="cm-checkbox-container">
              <h4>Meeting Controls</h4>
              <Checkbox
                isChecked={emailControls.add_internal_contacts}
                name="add_internal_contacts"
                onChange={onEmailControlChange}
                title={
                  "Email will be sent to the selected members from LookingPoint"
                }
                disabled={isInternalKickoff}
              >
                Internal Meeting
              </Checkbox>
            </div>
            <div className="cm-checkbox-container">
              <h4>Email Controls</h4>
              <Checkbox
                isChecked={emailControls.add_customer_contacts}
                name="add_customer_contacts"
                onChange={onEmailControlChange}
                title={"Email will be sent to the selected Customer Contacts"}
                disabled={isInternalKickoff}
              >
                Include the Customer
              </Checkbox>
              <Checkbox
                isChecked={emailControls.add_additional_contacts}
                name="add_additional_contacts"
                onChange={onEmailControlChange}
                title={"Email will be sent to the selected Vendor Resources"}
                disabled={isInternalKickoff}
              >
                Include Vendor Resources
              </Checkbox>
            </div>
          </section>
        </div>
        <PDFViewer
          show={preview.view === "meeting_pdf"}
          onClose={closePreview}
          titleElement={`Meeting PDF Preview`}
          previewHTML={{
            file_path: preview.data,
          }}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={closePreview}
            />
          }
          className="meeting-pdf-preview"
        />
        <HTMLViewer
          show={preview.view === "email"}
          onClose={closePreview}
          titleElement={`Meeting Email Preview`}
          previewHTML={preview.data}
        />
      </>
    );
  };

  return (
    <ModalBase
      show={props.show}
      onClose={() => props.onClose(false)}
      hideCloseButton={Boolean(loading || preview.view)}
      titleElement={`Close Meeting`}
      bodyElement={getBody()}
      className="close-meeting-modal"
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  meetingTemplates: state.pmo.meetingTemplates,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (str: string) => dispatch(addErrorMessage(str)),
  addSuccessMessage: (str: string) => dispatch(addSuccessMessage(str)),
  getEmailRecipients: (projectId: number, meetingId: number) =>
    dispatch(getMeetingEmailRecipients(projectId, meetingId)),
  saveMeetingDetails: (projectID: number, data: IProjectMeeting, api: string) =>
    dispatch(saveMeetingDetails(projectID, data, api)),
  uploadImage: (file: FormData, name: string) =>
    dispatch(uploadImage(file, name)),
  getEmailPreview: (
    projectId: number,
    meetingId: number,
    email_data: CloseMeetingEmail
  ) => dispatch(getMeetingEmailPreview(projectId, meetingId, email_data)),
  getPDFPreview: (
    projectId: number,
    meetingId: number,
    project_update: string
  ) => dispatch(getMeetingPDFPreviewV2(projectId, meetingId, project_update)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CloseMeetingEmailPage);
