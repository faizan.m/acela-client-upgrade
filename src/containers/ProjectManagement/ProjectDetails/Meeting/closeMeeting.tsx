import React, { useState } from "react";
import MeetingTimeEntryPage from "./timeEntry";
import CloseMeetingEmailModal from "./closeMeetingEmail";
import CloseMeetingConfirmation from "./closeMeetingConfirmation";

enum PageType {
  MeetingEmail,
  TimeEntryEdit,
  CloseMeetingConfirmation,
}

const MeetingClose: React.FC<MeetingCloseProps> = (props) => {
  const [currentPage, setCurrentPage] = useState<PageType>(
    PageType.MeetingEmail
  );
  const [emailContext, setEmailContext] = useState<CloseMeetingEmail>();

  const onSaveAndCloseEmailPage = (data: CloseMeetingEmail) => {
    setEmailContext(data);
    setCurrentPage(PageType.CloseMeetingConfirmation);
  };

  const onCloseConfirmation = (meetingCompleted?: boolean) => {
    if (meetingCompleted === true) setCurrentPage(PageType.TimeEntryEdit);
    else setCurrentPage(PageType.MeetingEmail);
  };

  return (
    <>
      {currentPage === PageType.MeetingEmail && (
        <CloseMeetingEmailModal
          {...props}
          onSaveAndClose={onSaveAndCloseEmailPage}
        />
      )}
      {currentPage === PageType.CloseMeetingConfirmation && (
        <CloseMeetingConfirmation
          {...props}
          emailData={emailContext}
          onCloseConfirmation={onCloseConfirmation}
        />
      )}
      {currentPage === PageType.TimeEntryEdit && (
        <MeetingTimeEntryPage {...props} isMeetingComplete={false} />
      )}
    </>
  );
};

export default MeetingClose;
