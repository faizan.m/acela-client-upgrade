import { cloneDeep, isEmpty } from "lodash";
import React from "react";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  editCustomerUser,
  EDIT_CUSTOMER_USER_FAILURE,
  EDIT_CUSTOMER_USER_SUCCESS,
  getCustomerUser,
} from "../../../actions/customer";
import {
  downloadAttachment,
  DOWNLOAD_DOCUMENT_SUCCESS,
} from "../../../actions/dashboard";
import {
  fetchAllCustomerUsers,
  FETCH_ALL_CUST_USERS_SUCCESS,
} from "../../../actions/documentation";
import {
  getImpactList,
  getTeamMembers,
  deleteAdditionalContact,
  deleteCustomerContact,
  deleteProjectDocumentsByID,
  getActionItemList,
  getContactRoles,
  getProjectAdditionalContactsByID,
  getProjectBoardList,
  getProjectCustomerContactsByID,
  getProjectDetails,
  getProjectDocumentsByID,
  getProjectEngineersByID,
  getProjectTypes,
  getProjectStatusByProjectID,
  saveProjectAdditionalContactsByID,
  saveProjectCustomerContactsByID,
  saveProjectDocumentsByID,
  setProjectDetails,
  updateAdditionalContact,
  updateCustomerContact,
  updateProjectDetails,
  SET_PROJECT_DETAILS_FAILURE,
  PROJECT_ADDITIONAL_CONTACTS_FAILURE,
  PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
  PROJECT_DETAILS_SUCCESS,
  PROJECT_DETAILS_UPDATE_SUCCESS,
  PROJECT_DOCUMENTS_FAILURE,
  PROJECT_DOCUMENTS_SUCCESS,
  PROJECT_ENGINEERS_SUCCESS,
  PROJECT_TYPES_SUCCESS,
} from "../../../actions/pmo";
import {
  getCustomerQuotes,
  getTabledataSOWMetrics,
  GET_QUOTES_SUCCESS,
  GET_TABLE_SUCCESS,
} from "../../../actions/sow";
import {
  fetchUserTypes,
  FETCH_USERTYPES_SUCCESS,
} from "../../../actions/userType";
import Accordian from "../../../components/Accordian";
import SquareButton from "../../../components/Button/button";
import Input from "../../../components/Input/input";
import SelectInput from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import PMOCollapsible from "../../../components/PMOCollapsible";
import PMOTooltip from "../../../components/PMOTooltip/pmoTooltip";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../components/Spinner";
import UsershortInfo from "../../../components/UserImage";
import { commonFunctions } from "../../../utils/commonFunctions";
import {
  getBackgroundOverdue,
  getConvertedColorWithOpacity,
  getProgressbarColor,
} from "../../../utils/CommonUtils";
import { saveBlob } from "../../../utils/download";
import AppValidators from "../../../utils/validator";
import CustomerForm from "../../Customer/customerForm";
import CustomerUserNew from "./addCustomerUser";
import ArchivedLists from "./archivedList";
import BOMDetails from "./bomDetails";
import MeetingLists from "./meetingList";
import ProjectNotes from "./Details/projectNotes";
import ProjectPhases from "./projectPhases";
import ProjectUpdates from "./projectUpdates";
import ProjectItem from "./projectItem";
import "./style.scss";

interface IProjectDetailsProps extends ICommonProps {
  showPM: boolean;
  lastVisitedSite: string;
  getImpactList: () => Promise<any>;
  getTeamMembers: (id: number) => Promise<any>;
  getProjectStatusByProjectID: (projectId: number) => Promise<any>;
  fetchRulesInfiniteScroll: any;
  fetching: boolean;
  rules: any;
  getProjectPhasesByID: any;
  getProjectEngineersByID: any;
  getProjectCustomerContactsByID: any;
  getContactRoles: any;
  getProjectDocumentsByID: any;
  saveProjectDocumentsByID: any;
  deleteProjectDocumentsByID: any;
  saveProjectCustomerContactsByID: any;
  updateCustomerContact: any;
  deleteCustomerContact: any;
  getProjectDetails: any;
  getProjectTypes: any;
  updateProjectDetails: any;
  fetchAllCustomerUsers: any;
  getProjectBoardList: any;
  downloadAttachment: any;
  getActionItemList: any;
  editCustomerUser: any;
  fetchUserTypes: any;
  getCustomerUser: any;
  getProjectAdditionalContactsByID: any;
  saveProjectAdditionalContactsByID: any;
  updateAdditionalContact: any;
  deleteAdditionalContact: any;
  getTabledataSOWMetrics: any;
  getCustomerQuotes: any;
  addSuccessMessage: any;
  addErrorMessage: any;
  setProjectDetails: (projectId: number, data: any) => Promise<any>;
}

interface IProjectDetailsState {
  list: any;
  project: IProjectDetails;
  loading: Boolean;
  isOpen: boolean;
  type: any;
  projectTypes: any;
  projectEngineers: any;
  preSalesEngineers: any;
  currentUploadedDocument: any;
  showUploadError: string;
  ownerOptions: IPickListOptions[];
  impactList: IStatusImpactItem[];
  projectStatusList: IStatusImpactItem[];
  showAddContactForm: boolean;
  projectCustomerContacts: IProjectCustomerContact[];
  customerUsers: any;
  customerContactRolesList: any;
  projectDocuments: any;
  selectedCustomerName: any;
  selectedCustomerRole: any;
  showCreateUserModal: boolean;
  fetchingCustomerUsers: boolean;
  showContactActions: boolean;
  editContactId: any;
  editableCustomerRoleId: any;
  openMeeting: boolean;
  editOverallStatus: boolean;
  showDocumentLoader: boolean;
  statusList: any;
  isFileAction: boolean;
  accountManager: any;
  showEditDropdown: boolean;
  showUpdateDescription: boolean;
  openListOptions: boolean;
  showContactAddErrorMsg: string;
  meetingList: any[];
  isOverallStatusChanged: boolean;
  overallStatus: any;
  isEditingUser: boolean;
  isCustomerUserFormOpen: boolean;
  userToEdit: any;
  userRoles: any;
  projectAdditionalContacts: any[];
  showAdditionalContacts: boolean;
  showAdditionalContactAddErrorMsg: string;
  showAdditionalContactActions: boolean;
  showDocumentActions: boolean;
  editDocumentId?: number;
  editAdditionalContactId: any;
  showAdditionalContactForm: boolean;
  name: any;
  email: any;
  role: any;
  phone: any;
  error: {
    name: IFieldValidation;
    email: IFieldValidation;
    role: IFieldValidation;
    phone: IFieldValidation;
  };
  showAdditionalContactId: boolean;
  sow_document: any;
  customerQuotesList: any;
  sowDocumentList: any;
  opportunity_crm_ids: any;
  linked_sow_doc_name: any;
  openBOM: boolean;
  currentDownloadingFile?: number;
}

const project: IProjectDetails = {
  project_updates: {
    updates: "",
    note: "",
    estimated_hours_to_complete: null,
  },
};

const fileIcons = {
  pdf: "/assets/icons/008-file-7.svg",
  doc: "/assets/icons/005-file-4.svg",
  docx: "/assets/icons/006-file-5.svg",
  zip: "/assets/icons/009-file-8.svg",
  xls: "/assets/icons/047-file-46.svg",
  xlsx: "/assets/icons/047-file-46.svg",
  csv: "/assets/icons/048-file-47.svg",
  svg: "/assets/icons/001-file.svg",
  png: "/assets/icons/003-file-2.svg",
  jpg: "/assets/icons/004-file-3.svg",
  jpeg: "/assets/icons/004-file-3.svg",
  txt: "/assets/icons/034-file-33.svg",
};

class ProjectDetails extends React.Component<
  IProjectDetailsProps,
  IProjectDetailsState
> {
  static validator = new AppValidators();
  uploadInput = React.createRef<HTMLInputElement>();
  deleteConfirmBox = React.createRef<HTMLElement>();
  customerMailBodyRef: any;

  constructor(props: IProjectDetailsProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };

  getEmptyState = () => ({
    list: [],
    project,
    loading: false,
    isOpen: true,
    type: undefined,
    projectTypes: [],
    projectEngineers: [],
    preSalesEngineers: [],
    ownerOptions: [],
    impactList: [],
    projectStatusList: [],
    currentUploadedDocument: undefined,
    showUploadError: "",
    showAddContactForm: false,
    projectCustomerContacts: [],
    customerUsers: [],
    customerContactRolesList: [],
    projectDocuments: [],
    selectedCustomerName: undefined,
    selectedCustomerRole: undefined,
    showCreateUserModal: false,
    fetchingCustomerUsers: false,
    showContactActions: false,
    editContactId: undefined,
    editableCustomerRoleId: undefined,
    openMeeting: false,
    statusList: [],
    editOverallStatus: false,
    isFileAction: false,
    showDocumentLoader: false,
    accountManager: {
      id: "",
      member_id: 0,
      first_name: "",
      last_name: "",
      profile_url: "",
    },
    showEditDropdown: false,
    showUpdateDescription: false,
    openListOptions: false,
    showContactAddErrorMsg: "",
    meetingList: [],
    isOverallStatusChanged: false,
    overallStatus: undefined,
    isEditingUser: false,
    isCustomerUserFormOpen: false,
    userToEdit: null,
    userRoles: [],
    projectAdditionalContacts: [],
    showAdditionalContacts: false,
    showAdditionalContactAddErrorMsg: "",
    showAdditionalContactActions: false,
    showDocumentActions: false,
    editAdditionalContactId: undefined,
    showAdditionalContactForm: false,
    name: undefined,
    email: undefined,
    role: undefined,
    phone: undefined,
    error: {
      name: { ...ProjectDetails.emptyErrorState },
      email: { ...ProjectDetails.emptyErrorState },
      role: { ...ProjectDetails.emptyErrorState },
      phone: { ...ProjectDetails.emptyErrorState },
    },
    showAdditionalContactId: undefined,
    editDocumentId: undefined,
    sow_document: undefined,
    customerQuotesList: [],
    sowDocumentList: [],
    opportunity_crm_ids: [],
    linked_sow_doc_name: undefined,
    openBOM: false,
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState((prevState) => ({
      project: {
        ...prevState.project,
        id,
      },
    }));
    this.getProjectTypes();
    this.getProjectDetails(id);
    this.getProjectEngineersByID(id);
    this.gePreSalesProjectEngineersByID(id);
    this.getProjectDocumentsByID(id);
    this.fetchImpactList();
    this.fetchProjectContacts(id);
    this.fetchProjectStatus(id);
    this.getContactRoles();
    this.getCustomerUserRoles();
  }

  setRef = (element) => {
    this.deleteConfirmBox = element;
  };

  getAllCustomerUsers = (customerID) => {
    this.setState({ loading: true, fetchingCustomerUsers: true });
    this.props.fetchAllCustomerUsers(customerID).then((action) => {
      if (action.type === FETCH_ALL_CUST_USERS_SUCCESS) {
        this.setState({
          customerUsers: action.response,
          fetchingCustomerUsers: false,
        });
      }
      this.setState({ loading: false });
    });
  };

  getProjectDocumentsByID = (id) => {
    this.setState({ showDocumentLoader: true, isFileAction: true });
    this.props.getProjectDocumentsByID(id).then((action) => {
      if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
        this.setState({
          projectDocuments: action.response,
        });
      }
      this.setState({ showDocumentLoader: false, isFileAction: false });
    });

    this.props.getProjectBoardList().then((action) => {
      true;
      const a = action.response;
      const statusList = a && a.map((a) => a.statuses).flat();
      this.setState({ loading: false, statusList });
    });
  };

  getProjectAdditionalContactsByID = (id) => {
    this.setState({ showAdditionalContacts: true });
    this.props.getProjectAdditionalContactsByID(id).then((action) => {
      if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
        this.setState({
          projectAdditionalContacts: action.response,
          showContactActions: false,
          editContactId: undefined,
          editableCustomerRoleId: undefined,
          showAdditionalContactForm: false,
          email: undefined,
          name: undefined,
          phone: undefined,
          role: undefined,
        });
      }
      this.setState({ showAdditionalContacts: false });
    });
  };

  getProjectDetails = (id) => {
    this.setState({ loading: true });
    this.props.getProjectDetails(id).then((action) => {
      if (action.type === PROJECT_DETAILS_SUCCESS) {
        const project: IProjectDetails = action.response;
        project.project_updates = isEmpty(project.project_updates)
          ? {
              updates: "",
              note: "",
              estimated_hours_to_complete: null,
            }
          : project.project_updates;
        const ownerOptions = cloneDeep(this.state.ownerOptions);
        ownerOptions.push({
          value: project.project_manager_id,
          label: (
            <UsershortInfo
              name={project.project_manager}
              url={project.project_manager_profile_pic}
              className="inside-box"
            />
          ),
        });
        this.setState({
          project,
          ownerOptions,
          type: project.type_id,
          showEditDropdown: !project.type_id,
          overallStatus: project.overall_status_id,
          opportunity_crm_ids: project.opportunity_crm_ids,
          sow_document: project.sow_document,
          linked_sow_doc_name: project.linked_sow_doc_name,
        });
        this.getCustomerQutoes(project.customer_id);
        this.getAllCustomerUsers(project.customer_id);
        this.getSOWDocuments(project.customer_id);
      }
      this.setState({ loading: false });
    });
  };

  fetchProjectContacts = (projectID: number) => {
    this.setState({ loading: true, showAdditionalContacts: true });
    Promise.all([
      this.props.getActionItemList(projectID, "account-manager"),
      this.props.getTeamMembers(projectID),
      this.props.getProjectCustomerContactsByID(projectID),
      this.props.getProjectAdditionalContactsByID(projectID),
    ]).then(
      ([accManager, teamMembers, customerContacts, additionalContacts]) => {
        const usersList: {
          user_id: string;
          name: string;
          profile_url: string;
        }[] = [
          ...teamMembers.response.map((el) => ({
            name: el.name,
            profile_url: el.profile_url,
            user_id: el.id,
          })),
          ...customerContacts.response.map((el) => ({
            name: el.name,
            profile_url: el.profile_url,
            user_id: el.user_id,
          })),
          ...additionalContacts.response.map((el) => {
            return {
              name: el.name,
              user_id: el.id,
              profile_url: el.profile_url,
            };
          }),
        ];

        usersList.push({
          user_id: accManager.response.id,
          name: accManager.response.name,
          profile_url: accManager.response.profile_url,
        });
        this.state.project.project_manager &&
          usersList.push({
            user_id: this.state.project.project_manager_id,
            name: this.state.project.project_manager,
            profile_url: this.state.project.project_manager_profile_pic,
          });
        const options: IPickListOptions[] = usersList.map((el) => ({
          value: el.user_id,
          label: (
            <UsershortInfo
              name={el.name}
              url={el.profile_url}
              className="inside-box"
            />
          ),
        }));
        this.setState({
          loading: false,
          name: undefined,
          role: undefined,
          email: undefined,
          phone: undefined,
          ownerOptions: options,
          editContactId: undefined,
          showContactActions: false,
          showAdditionalContactForm: false,
          editableCustomerRoleId: undefined,
          accountManager: accManager.response,
          projectCustomerContacts: customerContacts.response,
          projectAdditionalContacts: additionalContacts.response,
        });
      }
    );
  };

  fetchProjectStatus = (id: number) => {
    this.props.getProjectStatusByProjectID(id).then((action) => {
      this.setState({ projectStatusList: action.response });
    });
  };

  fetchImpactList = () => {
    this.props.getImpactList().then((action) => {
      this.setState({ impactList: action.response });
    });
  };

  getProjectTypes = () => {
    this.setState({ loading: true });
    this.props.getProjectTypes().then((action) => {
      if (action.type === PROJECT_TYPES_SUCCESS) {
        const projectTypes = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));
        this.setState({ projectTypes });
      }
      this.setState({ loading: false });
    });
  };

  getProjectEngineersByID = (id) => {
    this.setState({ loading: true });
    this.props.getProjectEngineersByID(id).then((action) => {
      if (action.type === PROJECT_ENGINEERS_SUCCESS) {
        this.setState({
          projectEngineers: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };
  gePreSalesProjectEngineersByID = (id) => {
    this.setState({ loading: true });
    this.props
      .getProjectEngineersByID(id, "pre-sales-engineers")
      .then((action) => {
        if (action.type === PROJECT_ENGINEERS_SUCCESS) {
          this.setState({
            preSalesEngineers: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  getProjectCustomerContactsByID = (id) => {
    this.setState({ loading: true });
    this.props.getProjectCustomerContactsByID(id).then((action) => {
      if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
        this.setState({
          projectCustomerContacts: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  getContactRoles = async () => {
    const mapping: any = await this.props.getContactRoles();
    this.setState({ customerContactRolesList: mapping.response });
  };

  getSOWDocuments = (customerId) => {
    this.setState({ loading: true });

    const params = {
      limited_fields: true,
      "doc-status": "won",
      customer_id: customerId,
    };

    this.props.getTabledataSOWMetrics(params).then((action) => {
      if (action.type === GET_TABLE_SUCCESS) {
        const sowDocumentList = action.response.map((data) => ({
          value: data.id,
          label: `${data.name}`,
        }));
        this.setState({ sowDocumentList });
      }
      this.setState({ loading: false });
    });
  };

  getCustomerQutoes = (customerId) => {
    this.setState({ loading: true });
    this.props
      .getCustomerQuotes(customerId, { "won-only": true })
      .then((action) => {
        if (action.type === GET_QUOTES_SUCCESS) {
          const customerQuotesList = action.response.map((data) => ({
            value: data.id,
            label: `${data.name}`,
          }));
          this.setState({ customerQuotesList });
        }
        this.setState({ loading: false });
      });
  };

  onOverallStatusApplyClick = (event: any) => {
    this.setState({ loading: true });
    this.props
      .updateProjectDetails(
        {
          title: this.state.project.title,
          overall_status: this.state.overallStatus,
        },
        this.state.project.id
      )
      .then((action) => {
        if (action.type === PROJECT_DETAILS_UPDATE_SUCCESS) {
          this.setState({
            project: {
              ...action.response,
              project_updates: isEmpty(action.response.project_updates)
                ? {
                    updates: "",
                    note: "",
                    estimated_hours_to_complete: null,
                  }
                : action.response.project_updates,
            },
            overallStatus: this.state.overallStatus,
            isOverallStatusChanged: false,
          });
        }
        this.setState({ loading: false, editOverallStatus: false });
        this.getProjectDetails(this.state.project.id);
      });
  };

  handleChange = (event: any) => {
    this.setState({
      isOverallStatusChanged: true,
      overallStatus: event.target.value,
    });
  };

  handleTypeChange = async (event: any) => {
    const newState = cloneDeep(this.state);
    (newState.project[event.target.name] as any) = event.target.value;
    (newState.project["type"] as any) = this.getProjectTypeLabel(
      event.target.value
    );
    this.setState(newState);
  };

  handleTypeSave = async (event: any) => {
    this.setState({ loading: true });
    this.props
      .updateProjectDetails(
        {
          title: this.state.project.title,
          type: this.state.project.type_id,
        },
        this.state.project.id
      )
      .then((action) => {
        this.setState({
          showEditDropdown: false,
          loading: false,
          editOverallStatus: false,
        });
        this.getProjectDetails(this.state.project.id);
      });
  };

  saveProjectDescription = async () => {
    this.props.updateProjectDetails(
      {
        description: this.state.project.description,
      },
      this.state.project.id
    );
    this.setState({ showUpdateDescription: false });
  };

  getProjectTypeLabel = (typeId) => {
    const { projectTypes } = this.state;
    if (projectTypes.length > 0 && typeId) {
      const type = projectTypes.find((obj) => obj.value == typeId);
      if (type) {
        return type.label;
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  handleFileUpload = (e: any) => {
    const files = Array.from(e.target.files);
    const dataFile = files[0];
    this.setState({
      currentUploadedDocument: dataFile,
      showUploadError: "",
    });
  };

  getCustomerUserOptions = () => {
    const users = this.state.customerUsers
      ? this.state.customerUsers.map((t) => ({
          value: t.id,
          label: `${t.first_name} ${t.last_name}`,
          disabled: false,
        }))
      : [];

    return users;
  };

  getCustomerRoleOptions = () => {
    const roleOptions = this.state.customerContactRolesList
      ? this.state.customerContactRolesList.map((t) => ({
          value: t.id,
          label: `${t.role}`,
          disabled: false,
        }))
      : [];

    return roleOptions;
  };

  uploadDocument = () => {
    this.setState({ showDocumentLoader: true });
    const data = new FormData();
    data.append("file_path", this.state.currentUploadedDocument);
    data.append("file_name", this.state.currentUploadedDocument.name);
    this.props
      .saveProjectDocumentsByID(this.state.project.id, data)
      .then((action) => {
        if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
          this.setState({
            currentUploadedDocument: undefined,
          });
          this.getProjectDocumentsByID(this.state.project.id);
        }
      });
  };

  deleteDocument = (document) => {
    this.setState({ loading: true });
    this.props
      .deleteProjectDocumentsByID(this.state.project.id, document.id)
      .then((action) => {
        if (action.type === PROJECT_DOCUMENTS_SUCCESS) {
          this.getProjectDocumentsByID(this.state.project.id);
          this.props.addSuccessMessage("Document deleted successfully");
        }
        if (action.type === PROJECT_DOCUMENTS_FAILURE) {
          this.props.addErrorMessage("Error deleting document!");
        }
        this.setState({ loading: false });
      })
      .catch(() => {
        this.setState({ loading: false });
        this.props.addErrorMessage("Error deleting document, server error!");
      });
  };

  handleContactChange = (e: any, type: string) => {
    if (type == "name") {
      this.setState({
        selectedCustomerName: e.target.value,
      });
    }

    if (type == "role") {
      this.setState({
        selectedCustomerRole: e.target.value,
      });
    }
  };

  handleAdditionalContactChange = (e: any) => {
    const newState = cloneDeep(this.state);
    (newState[e.target.name] as any) = e.target.value;
    this.setState(newState);
  };

  toggleCreateUserModal = () => {
    this.setState((prevState) => ({
      showCreateUserModal: !prevState.showCreateUserModal,
      userToEdit: null,
    }));
  };

  addCustomerContact = () => {
    this.setState({ loading: false });
    const customerContact = this.state.customerUsers.filter(
      (customer) => customer.id === this.state.selectedCustomerName
    );

    if (
      this.state.selectedCustomerName &&
      this.state.selectedCustomerName &&
      customerContact.length > 0
    ) {
      const data = {
        role: this.state.selectedCustomerRole,
        user: this.state.selectedCustomerName,
        contact_crm_id: customerContact[0].crm_id,
      };

      this.props
        .saveProjectCustomerContactsByID(this.state.project.id, data)
        .then((action) => {
          if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
            this.getProjectCustomerContactsByID(this.state.project.id);
            this.setState((prevState) => ({
              showAddContactForm: false,
              selectedCustomerRole: undefined,
              selectedCustomerName: undefined,
              showContactAddErrorMsg: "",
            }));
          } else {
            let errorMsg = "";

            if (Array.isArray(action.errorList.data)) {
              errorMsg = action.errorList.data[0]["contact/id"];
            } else {
              errorMsg = action.errorList.data.contact_crm_id;
            }
            this.setState((prevState) => ({
              showContactAddErrorMsg: errorMsg,
            }));
          }
        });
    }
  };

  closeUserModal = () => {
    this.setState((prevState) => ({
      showCreateUserModal: !prevState.showCreateUserModal,
    }));
    this.getAllCustomerUsers(this.state.project.customer_id);
  };

  onContactDeleteClick = (contact: any) => {
    this.setState({ loading: true });

    this.props
      .deleteCustomerContact(
        this.state.project.id,
        contact.contact_crm_id,
        contact.contact_record_id
      )
      .then((action) => {
        if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
          this.getProjectCustomerContactsByID(this.state.project.id);
          this.setState({
            showContactActions: false,
            editContactId: undefined,
            editableCustomerRoleId: undefined,
            showAdditionalContactForm: false,
          });
        }
      });
  };

  onAddtionalContactDeleteClick = (contact: any) => {
    this.setState({ loading: true });

    this.props
      .deleteAdditionalContact(this.state.project.id, contact.id)
      .then((action) => {
        if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
          this.getProjectAdditionalContactsByID(this.state.project.id);
          this.setState({
            showAdditionalContactActions: false,
            editAdditionalContactId: undefined,
            editableCustomerRoleId: undefined,
          });
          this.setState({ loading: false });
        }
      });
  };

  handleContactRoleUpdate = (e: any, contact: any) => {
    this.setState({ loading: true });
    this.setState({
      selectedCustomerRole: e.target.value,
    });
    const data = {
      role_id: e.target.value,
      contact_crm_id: contact.contact_crm_id,
    };

    this.props
      .updateCustomerContact(
        this.state.project.id,
        contact.contact_crm_id,
        data
      )
      .then((action) => {
        if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
          this.getProjectCustomerContactsByID(this.state.project.id);
          this.setState({
            showContactActions: false,
            editContactId: undefined,
            editableCustomerRoleId: undefined,
            selectedCustomerRole: undefined,
          });
        }
      });
  };

  downloadDocument = (document: any) => {
    this.setState({ currentDownloadingFile: document.id });

    this.props.downloadAttachment(document.id).then((action) => {
      if (action.type === DOWNLOAD_DOCUMENT_SUCCESS) {
        saveBlob(document.file_name, action.response);
        this.setState({ currentDownloadingFile: undefined });
      }
    });
  };
  handleChangeEditor = (event) => {
    const newState = cloneDeep(this.state);
    (newState.project[event.target.name] as any) = event.target.value;
    this.setState(newState);
  };

  handleCallback = (childData) => {
    this.setState({ meetingList: childData });
  };

  toggleCustomerUser = () => {
    this.setState((prevState) => ({
      isCustomerUserFormOpen: !prevState.isCustomerUserFormOpen,
    }));
  };
  onEditCustomerUser = (customerUser) => {
    const newCustomerId = this.state.project.customer_id;
    this.setState({ isEditingUser: true });

    this.props.editCustomerUser(newCustomerId, customerUser).then((action) => {
      if (action.type === EDIT_CUSTOMER_USER_SUCCESS) {
        this.setState((prevState) => ({
          isCustomerUserFormOpen: false,
          isEditingUser: false,
        }));
      }
      if (action.type === EDIT_CUSTOMER_USER_FAILURE) {
        this.setState((prevState) => ({
          isCustomerUserFormOpen: true,
          isEditingUser: false,
        }));
      }
    });
  };
  getCustomerUserRoles = () => {
    this.setState({ loading: true, fetchingCustomerUsers: true });
    this.props.fetchUserTypes().then((action) => {
      if (action.type === FETCH_USERTYPES_SUCCESS) {
        const superUserType = action.response.results.find(
          (userType) => userType.user_type === "provider"
        );

        if (superUserType) {
          this.setState({
            userRoles: superUserType.roles.map((role) => ({
              value: role.id,
              label: role.display_name,
            })),
          });
        }
      }
    });
  };

  saveLinkingDocuments = () => {
    this.setState({ loading: true });
    this.props
      .updateProjectDetails(
        {
          sow_document: this.state.sow_document,
          opportunity_crm_ids: this.state.opportunity_crm_ids,
          linked_sow_doc_name: this.state.linked_sow_doc_name,
        },
        this.state.project.id
      )
      .then((action) => {
        this.setState({ loading: false });
        this.getProjectDetails(this.state.project.id);
      });
  };

  getFilteredCustomerQuotesList = () => {
    return this.state.customerQuotesList.filter(
      (data) => this.state.opportunity_crm_ids.indexOf(data.value) === -1
    );
  };

  handleLinkingChange = (e, type) => {
    const newState = cloneDeep(this.state);
    if (type === "sow_document") {
      (newState[e.target.name] as any) = e.target.value;
      (newState.linked_sow_doc_name as any) = e.target.label;
    }

    if (type === "opportunity_crm_ids") {
      const ids = this.state.opportunity_crm_ids;
      ids.push(e.target.value);
      (newState.opportunity_crm_ids as any) = ids.filter((x) => x);
    }

    this.setState(newState);
  };

  removeOpportunity = (opportunityId) => {
    const newState = cloneDeep(this.state);
    const ids = this.state.opportunity_crm_ids.filter(
      (id) => id !== opportunityId
    );
    (newState.opportunity_crm_ids as any) = ids;
    this.setState(newState);
  };

  addAdditionalContact = () => {
    const isValid = this.validateForm();
    if (isValid) {
      const {
        editAdditionalContactId,
        email,
        name,
        role,
        phone,
        project,
      } = this.state;

      let data: any = {
        name: name,
        phone: phone ? phone : null,
        role: role,
      };
      if (editAdditionalContactId) {
        // put
        const contact: any = this.state.projectAdditionalContacts.filter(
          (val) => val.id === editAdditionalContactId
        );
        if (contact.length > 0 && email !== contact[0].email) {
          data = { ...data, email: email };
        }
        this.props
          .updateAdditionalContact(project.id, editAdditionalContactId, data)
          .then((action) => {
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
              this.getProjectAdditionalContactsByID(project.id);
              this.setState({
                showAdditionalContacts: false,
                showAddContactForm: false,
              });
            }
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
              this.setValidationErrors(action.errorList.data);
            }
          });
      } else {
        data = { ...data, email: email };
        this.props
          .saveProjectAdditionalContactsByID(project.id, data)
          .then((action) => {
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_SUCCESS) {
              this.getProjectAdditionalContactsByID(project.id);
              this.setState({
                showAdditionalContacts: false,
                showAddContactForm: false,
              });
            }
            if (action.type === PROJECT_ADDITIONAL_CONTACTS_FAILURE) {
              this.setValidationErrors(action.errorList.data);
            }
          });
      }
    }
  };
  setValidationErrors = (errorList) => {
    const newState: IProjectDetailsState = cloneDeep(this.state);
    const errorT = commonFunctions.errorStateHandle(errorList, newState);
    this.setState(errorT);
  };

  validateForm() {
    const error: any = this.getEmptyState().error;
    let isValid = true;

    if (!this.state.name || this.state.name.trim() === "") {
      error.name.errorState = "error";
      error.name.errorMessage = "Please enter name";
      isValid = false;
    }
    if (
      !this.state.email ||
      this.state.email.trim() === "" ||
      !AppValidators.isValidEmail(this.state.email)
    ) {
      error.email.errorState = "error";
      error.email.errorMessage = "Please enter valid email";
      isValid = false;
    }

    if (
      !this.state.phone ||
      (this.state.phone && this.state.phone.trim() === "") ||
      !ProjectDetails.validator.isValidPhoneNumber(this.state.phone)
    ) {
      error.phone.errorState = "error";
      error.phone.errorMessage = "Please enter valid phone number";
      isValid = false;
    }

    this.setState({ error });

    return isValid;
  }

  renderAdditionalContacts = () => {
    return (
      <div style={{ paddingTop: "15px" }}>
        <div className="label-details">
          {this.state.projectAdditionalContacts.length > 0 ? "Resources" : ""}
        </div>
        {this.state.projectAdditionalContacts.map((contact, i) => (
          <div
            className="contact-container"
            onMouseEnter={() =>
              this.setState({
                showAdditionalContactActions: true,
                showAdditionalContactId: contact.id,
              })
            }
            onMouseLeave={() => {
              if (!this.deleteConfirmBox) {
                this.setState({
                  showAdditionalContactActions: false,
                  showAdditionalContactId: undefined,
                });
              }
            }}
            key={contact.id}
          >
            <div className={`label-content ${contact.id}`}>
              <UsershortInfo name={contact.name} url={contact.profile_url} />
              {this.state.showAdditionalContactActions &&
                this.state.showAdditionalContactId === contact.id && (
                  <div className="contact-actions">
                    {contact.id && (
                      <img
                        className={"contact-edit-icon"}
                        alt=""
                        src={"/assets/icons/edit.png"}
                        onClick={(e) => {
                          this.setState({
                            showAdditionalContactForm: true,
                            name: contact.name,
                            email: contact.email,
                            role: contact.role,
                            phone: contact.phone,
                            editAdditionalContactId: contact.id,
                            showAdditionalContactId: undefined,
                          });
                        }}
                      />
                    )}
                    <SmallConfirmationBox
                      setRef={this.setRef}
                      className="remove"
                      onClickOk={() =>
                        this.onAddtionalContactDeleteClick(contact)
                      }
                      text={"Additional Contact"}
                    />
                  </div>
                )}
            </div>
            {<div className="label-sub-content">{contact.role}</div>}
          </div>
        ))}
        {(this.state.showAddContactForm ||
          this.state.showAdditionalContactForm) && (
          <div className="open-won-switch customer-resource">
            Customer{" "}
            <input
              className="switch"
              type="checkbox"
              onChange={(e) => {
                const value = e.target.checked === true ? true : false;
                this.setState({
                  showAdditionalContactForm: value,
                  selectedCustomerRole: undefined,
                  editAdditionalContactId: undefined,
                  showAdditionalContactId: undefined,
                  showAddContactForm: !value,
                });
              }}
            />{" "}
            Resource
          </div>
        )}
        {!this.state.showAdditionalContactForm ? (
          ""
        ) : (
          <div className="meeting-detail-contact">
            <div className="box">
              <Input
                field={{
                  label: "Name",
                  type: "TEXT",
                  value: this.state.name,
                  options: this.getCustomerRoleOptions(),
                  isRequired: true,
                }}
                width={12}
                multi={false}
                name="name"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter name`}
                error={this.state.error.name}
              />
              <Input
                field={{
                  label: "Email",
                  type: "TEXT",
                  value: this.state.email,
                  options: this.getCustomerRoleOptions(),
                  isRequired: true,
                }}
                width={12}
                multi={false}
                name="email"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter email`}
                error={this.state.error.email}
              />
              <Input
                field={{
                  label: "Role",
                  type: "TEXT",
                  value: this.state.role,
                  options: this.getCustomerRoleOptions(),
                  isRequired: false,
                }}
                width={12}
                multi={false}
                name="role"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter role`}
                error={this.state.error.role}
              />
              <Input
                field={{
                  label: "Phone",
                  type: "TEXT",
                  value: this.state.phone,
                  isRequired: true,
                }}
                width={12}
                multi={false}
                name="phone"
                onChange={(e) => this.handleAdditionalContactChange(e)}
                placeholder={`Enter phone`}
                error={this.state.error.phone}
              />
              <div className="right">
                <SquareButton
                  content="Cancel"
                  bsStyle={"default"}
                  className="contact-cancel"
                  onClick={() =>
                    this.setState({
                      showAdditionalContactAddErrorMsg: "",
                      showAdditionalContactForm: false,
                      name: undefined,
                      role: undefined,
                      email: undefined,
                      phone: undefined,
                      editAdditionalContactId: undefined,
                      showAdditionalContactId: undefined,
                    })
                  }
                />
                <SquareButton
                  content="Apply"
                  bsStyle={"primary"}
                  className="contact-save"
                  onClick={() => this.addAdditionalContact()}
                />
              </div>
            </div>
          </div>
        )}
        {this.state.showAdditionalContactAddErrorMsg != "" && (
          <div className="label-details-error-msg">
            {this.state.showAdditionalContactAddErrorMsg}
          </div>
        )}
      </div>
    );
  };

  saveProjectUpdates = (projectUpdates: IProjectUpdates) => {
    this.setState({
      project: {
        ...this.state.project,
        project_updates: projectUpdates,
      },
    });
    // Save to backend using PUT API
    const payload = { project_updates: projectUpdates };
    this.props
      .setProjectDetails(this.state.project.id, payload)
      .then((action) => {
        if (action.type === SET_PROJECT_DETAILS_FAILURE) {
          this.props.addErrorMessage("Unable to save Project Update!");
        }
      });
  };

  getDocumentName = (id) => {
    const document = this.state.customerQuotesList.filter(
      (obj) => obj.value === id
    );
    if (document.length > 0) return document[0].label;
    return "";
  };
  toggleBOMDetails = () => {
    this.setState(() => ({
      openBOM: false,
    }));
  };

  render() {
    const project = this.state.project;

    return (
      <div className="project-details">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div className="header">
          <div className="left">
            <div className="icons">
              <div
                style={{ backgroundColor: project.time_status_color }}
                className={`rectangle-icon-outer`}
              >
                <PMOTooltip
                  iconClass="status-svg-images-black"
                  icon={`/assets/icons/clock-circular-gross-outline-of-time.svg`}
                >
                  <div
                    style={{ borderColor: project.time_status_color }}
                    className="data-box"
                  >
                    <div className="text">Time Remaining</div>
                    <div
                      style={{
                        color: project.time_status_color,
                        fontWeight: 600,
                      }}
                      className="text"
                    >
                      {project.time_status_value} %
                    </div>
                  </div>
                </PMOTooltip>
              </div>

              <div
                style={{ backgroundColor: project.date_status_color }}
                className={`rectangle-icon-outer`}
              >
                <PMOTooltip
                  iconClass="status-svg-images-black"
                  icon={`/assets/icons/calendar-silhouette.svg`}
                >
                  <div
                    style={{ borderColor: project.date_status_color }}
                    className="data-box"
                  >
                    <div className="text">Close Date</div>
                    <div
                      style={{
                        color: project.date_status_color,
                        fontWeight: 600,
                      }}
                      className="text"
                    >
                      {" "}
                      {project.date_status_value > 0
                        ? `Due date in ${project.date_status_value} days`
                        : "Due date passed"}{" "}
                    </div>
                  </div>
                </PMOTooltip>
              </div>
              <div
                style={{ backgroundColor: getBackgroundOverdue(project) }}
                className={`rectangle-icon-outer`}
              >
                <PMOTooltip
                  iconClass="status-svg-images-black"
                  icon={`/assets/icons/pulse.svg`}
                  placement="bottom"
                >
                  {(project.overdue_critical_path_items ||
                    0 + project.overdue_action_items ||
                    0) !== 0 && (
                    <div
                      style={{
                        borderColor:
                          (project.overdue_critical_path_items ||
                            0 + project.overdue_action_items ||
                            0) > 0
                            ? "#db1643"
                            : "#176f07",
                      }}
                      className="data-box"
                    >
                      <div className="text">Overdue Actions</div>
                      <div className="text">
                        <span
                          style={{
                            color:
                              project.overdue_critical_path_items || 0 > 0
                                ? "#db1643"
                                : "#176f07",
                            fontWeight: 600,
                          }}
                        >
                          {" "}
                          {project.overdue_critical_path_items || 0}{" "}
                        </span>
                        Critical Items |
                        <span
                          style={{
                            color:
                              project.overdue_action_items || 0 > 0
                                ? "#db1643"
                                : "#176f07",
                            fontWeight: 600,
                          }}
                        >
                          {" "}
                          {project.overdue_action_items || 0}{" "}
                        </span>{" "}
                        Action
                      </div>
                    </div>
                  )}
                  {(project.expiring_critical_path_items ||
                    0 + project.expiring_action_items ||
                    0) !== 0 && (
                    <div
                      style={{
                        borderColor:
                          (project.expiring_critical_path_items ||
                            0 + project.expiring_action_items ||
                            0) > 0
                            ? "#dba629"
                            : "#176f07",
                      }}
                      className="data-box"
                    >
                      <div className="text">Pending Actions</div>
                      <div className="text">
                        <span
                          style={{
                            color:
                              project.expiring_critical_path_items || 0 > 0
                                ? "#dba629"
                                : "#176f07",
                            fontWeight: 600,
                          }}
                        >
                          {" "}
                          {project.expiring_critical_path_items || 0}{" "}
                        </span>
                        Critical Items |
                        <span
                          style={{
                            color:
                              project.expiring_action_items || 0 > 0
                                ? "#dba629"
                                : "#176f07",
                            fontWeight: 600,
                          }}
                        >
                          {" "}
                          {project.expiring_action_items || 0}{" "}
                        </span>{" "}
                        Action
                      </div>
                    </div>
                  )}
                  {(project.expiring_critical_path_items ||
                    0 + project.expiring_action_items ||
                    0) === 0 &&
                    (project.overdue_critical_path_items ||
                      0 + project.overdue_action_items ||
                      0) === 0 && (
                      <div
                        style={{
                          borderColor:
                            (project.expiring_critical_path_items ||
                              0 + project.expiring_action_items ||
                              0) > 0
                              ? "#dba629"
                              : "#176f07",
                        }}
                        className="data-box"
                      >
                        <div className="text">No Overdue & Pending Actions</div>
                      </div>
                    )}
                </PMOTooltip>
              </div>
            </div>
            <div className="name">
              {project.customer} - {project.title}{" "}
            </div>
          </div>
          <div className="right">
            <span className="project-id">
              (Project ID : <span>{project.crm_id}</span>)
            </span>
            <SquareButton
              content="Project Details V2"
              bsStyle={"primary"}
              onClick={() => {
                const projectID = this.props.match.params.id;
                const query = new URLSearchParams(this.props.location.search);
                const AllProject =
                  query.get("AllProject") === "true" ? true : false;
                this.props.history.push(
                  `/Projects/${projectID}?AllProject=${AllProject}`
                );
              }}
              className="bom-details"
            />
            <SquareButton
              content="BOM"
              bsStyle={"primary"}
              onClick={() => {
                this.setState({ openBOM: true });
              }}
              className="bom-details"
            />
            <SquareButton
              content="+ Change Request"
              bsStyle={"primary"}
              onClick={() => {
                const projectID = this.props.match.params.id;
                const query = new URLSearchParams(this.props.location.search);
                const AllProject =
                  query.get("AllProject") === "true" ? true : false;
                this.props.history.push(
                  `/Projects/${projectID}/change-requests?AllProject=${AllProject}`
                );
              }}
            />
            {this.state.openListOptions && (
              <div className="description-history-actions">
                <div
                  className="activity"
                  onClick={() => {
                    this.setState({
                      showUpdateDescription: true,
                      openListOptions: false,
                    });
                  }}
                >
                  Description
                </div>
                <div
                  className="activity"
                  onClick={() => {
                    const projectID = this.props.match.params.id;
                    this.props.history.push(`/Project/${projectID}/history`);
                  }}
                >
                  History
                </div>
                <div
                  className="activity"
                  onClick={() =>
                    this.props.history.push(
                      `/sow/agreement/0?customer=${this.state.project.customer_id}`
                    )
                  }
                >
                  Agreement
                </div>
              </div>
            )}

            <div
              className="update-project-description-btn"
              title={"Update Project Description"}
              onClick={() =>
                this.setState(
                  { openListOptions: !this.state.openListOptions },
                  () => {
                    setTimeout(() => {
                      this.setState({ openListOptions: false });
                    }, 7000);
                  }
                )
              }
            >
              {" "}
              &#8942;
            </div>
            <div className="separator" />

            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={() => {
                this.props.history.push(this.props.lastVisitedSite);
              }}
            />
          </div>
        </div>
        <div className="first-row">
          <ProjectPhases
            project={this.state.project}
            projectTypeID={this.state.type}
          />
          <div className="status-overall">
            <div className="action">
              <Input
                field={{
                  label: "Overall Status:",
                  type: "PICKLIST",
                  value: this.state.overallStatus,
                  isRequired: false,
                  options:
                    this.state.statusList &&
                    this.state.statusList.map((s, i) => ({
                      value: s.id,
                      label: (
                        <div
                          className="field-section color-preview-option"
                          title={s.title}
                          key={i}
                        >
                          <div
                            style={{
                              backgroundColor: s.color,
                              borderColor: s.color,
                            }}
                            className="left-column"
                          />
                          <div
                            style={{
                              background: `${getConvertedColorWithOpacity(
                                s.color
                              )}`,
                            }}
                            className="text"
                          >
                            {s.title}
                          </div>
                        </div>
                      ),
                    })),
                }}
                width={12}
                labelIcon={"info"}
                name="overall_status"
                onChange={(e) => this.handleChange(e)}
                placeholder={``}
                className="overall-status-edit"
              />
              {this.state.isOverallStatusChanged && (
                <div className="overall-status-action-btn">
                  <SquareButton
                    onClick={() => {
                      this.setState({
                        isOverallStatusChanged: false,
                        overallStatus: project.overall_status_id,
                      });
                    }}
                    content={"Cancel"}
                    bsStyle={"default"}
                  />
                  <SquareButton
                    onClick={(e) => this.onOverallStatusApplyClick(e)}
                    content={"Apply"}
                    bsStyle={"primary"}
                  />
                </div>
              )}
              <div className="progressbar-project-details">
                <div className="label-text">Project Progress :</div>
                <div className="percentage">{project.progress_percent}%</div>
                <div className="progress-bar-custom-outer">
                  <div
                    className="progress-bar-custom"
                    style={{
                      width: `${
                        project.progress_percent > 100
                          ? 100
                          : project.progress_percent
                      }%`,
                      backgroundColor: getProgressbarColor(
                        project.progress_percent
                      ),
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="first-row">
          <div className="action-second status-actions-row">
            <div className="project-action-component">
              <div className="list-actions">
                <PMOCollapsible
                  background="#164da5"
                  label={"Critical Path Items"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <ProjectItem
                      itemType={"critical-path-items"}
                      project={this.state.project}
                      ownerOptions={this.state.ownerOptions}
                      statusList={this.state.projectStatusList}
                      impactList={this.state.impactList}
                    />
                  )}
                </PMOCollapsible>
                <PMOCollapsible
                  background="#550aad"
                  label={"Action Items"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <ProjectItem
                      itemType={"action-items"}
                      project={this.state.project}
                      ownerOptions={this.state.ownerOptions}
                      statusList={this.state.projectStatusList}
                      impactList={this.state.impactList}
                    />
                  )}
                </PMOCollapsible>
                <PMOCollapsible
                  background="#c007d0"
                  label={"Risks"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <ProjectItem
                      itemType={"risk-items"}
                      project={this.state.project}
                      ownerOptions={this.state.ownerOptions}
                      statusList={this.state.projectStatusList}
                      impactList={this.state.impactList}
                    />
                  )}
                </PMOCollapsible>
                <PMOCollapsible
                  background="#0493ad"
                  label={"Meetings"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <MeetingLists
                      {...this.props}
                      api={"meetings"}
                      project={this.state.project}
                      parentCallback={this.handleCallback}
                    />
                  )}
                </PMOCollapsible>
                <PMOCollapsible
                  label={"Project Updates"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <ProjectUpdates
                      projectId={this.state.project.id}
                      projectUpdates={this.state.project.project_updates}
                      onSave={this.saveProjectUpdates}
                    />
                  )}
                </PMOCollapsible>
                <PMOCollapsible
                  background="#176f07"
                  label={"Project Notes"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <ProjectNotes
                      projectID={this.state.project.id}
                      meetingList={this.state.meetingList}
                      {...this.props}
                    />
                  )}
                </PMOCollapsible>
                {/* <PMOCollapsible background="#176f07" label={'Connectwise Project Notes'} isOpen={this.state.isOpen}>
                                    {this.state.project && this.state.project.id && <ProjectCWNotes projectID={this.state.project.id} meetingList={this.state.meetingList} {...this.props} />}
                                </PMOCollapsible> */}
                <PMOCollapsible
                  background="#c007d0"
                  label={"Completed Items"}
                  isOpen={this.state.isOpen}
                >
                  {this.state.project && this.state.project.id && (
                    <ArchivedLists
                      {...this.props}
                      api={"archived-items"}
                      project={this.state.project}
                    />
                  )}
                </PMOCollapsible>
              </div>
            </div>
          </div>
          <div className="status-overall-accordians">
            <Accordian label={"Project Details"} isOpen={true}>
              <div className="details-box">
                <div className="label-details">Customer</div>
                <div className="label-content">{project.customer}</div>
              </div>
              <div className="details-box">
                <div className="label-details">Customer Contact</div>
                {this.state.projectCustomerContacts.map((contact, i) => (
                  <div
                    className="contact-container"
                    key={i}
                    onMouseEnter={() =>
                      this.setState({
                        showContactActions: true,
                        editContactId: contact.contact_crm_id,
                      })
                    }
                    onMouseLeave={() => {
                      if (!this.deleteConfirmBox) {
                        this.setState({
                          showContactActions: false,
                          editContactId: undefined,
                          editableCustomerRoleId: undefined,
                        });
                      }
                    }}
                  >
                    <div className={`label-content ${contact.contact_crm_id}`}>
                      <UsershortInfo
                        name={contact.name}
                        url={contact.profile_url}
                      />
                      {this.state.showContactActions &&
                        this.state.editContactId === contact.contact_crm_id && (
                          <div className="contact-actions">
                            {contact.user_id && (
                              <img
                                className={"contact-edit-icon"}
                                alt=""
                                src={"/assets/icons/edit.png"}
                                onClick={(e) => {
                                  this.props
                                    .getCustomerUser(
                                      contact.user_id,
                                      this.state.project.customer_id
                                    )
                                    .then((action) => {
                                      if (
                                        action.type ===
                                        "GET_CUSTOMER_USER_SUCCESS"
                                      ) {
                                        this.setState({
                                          userToEdit: action.response,
                                          isCustomerUserFormOpen: true,
                                        });
                                      }
                                    });
                                }}
                              />
                            )}

                            <SmallConfirmationBox
                              setRef={this.setRef}
                              className="remove"
                              onClickOk={() =>
                                this.onContactDeleteClick(contact)
                              }
                              text={"Customer Contact"}
                            />
                          </div>
                        )}
                    </div>
                    {this.state.editableCustomerRoleId ===
                    contact.contact_crm_id ? (
                      <Input
                        field={{
                          label: "",
                          type: "PICKLIST",
                          value: this.state.selectedCustomerRole,
                          options: this.getCustomerRoleOptions(),
                          isRequired: false,
                        }}
                        width={12}
                        multi={false}
                        name="status"
                        onChange={(e) =>
                          this.handleContactRoleUpdate(e, contact)
                        }
                        placeholder={`Select`}
                      />
                    ) : (
                      <div className="label-sub-content">
                        {contact.project_role}
                        {contact.project_role ? "" : "Edit Role"}
                        {
                          <img
                            className={"contact-role-edit-icon"}
                            alt=""
                            src={"/assets/icons/edit.png"}
                            onClick={(e) =>
                              this.setState({
                                editableCustomerRoleId: contact.contact_crm_id,
                              })
                            }
                            onMouseEnter={() =>
                              this.setState({
                                showContactActions: true,
                              })
                            }
                            onMouseLeave={() => {
                              if (!this.deleteConfirmBox) {
                                this.setState({
                                  showContactActions: false,
                                });
                              }
                            }}
                          />
                        }
                      </div>
                    )}
                  </div>
                ))}
                <div className="contact-container">
                  {this.renderAdditionalContacts()}
                </div>
                {!this.state.showAddContactForm &&
                  !this.state.showAdditionalContactForm && (
                    <div className="meeting-detail-contact">
                      <div
                        className="label-details action-item"
                        onClick={() => {
                          this.setState({
                            showAddContactForm: true,
                            selectedCustomerRole: undefined,
                          });
                        }}
                      >
                        Add Contact
                      </div>
                    </div>
                  )}
                {this.state.showAddContactForm && (
                  <div className="meeting-detail-contact">
                    <div className="box">
                      <div className="select-type select-customer-contact add-new-option-section field-section col-xs-6 col-md-6">
                        <div className="add-new-option-box">
                          <div className="field__label row">
                            <label className="field__label-label" title="">
                              Select User
                            </label>
                            <span className="field__label-required" />
                          </div>
                          <div>
                            <SelectInput
                              name="user_id"
                              value={this.state.selectedCustomerName}
                              onChange={(e) =>
                                this.handleContactChange(e, "name")
                              }
                              options={this.getCustomerUserOptions()}
                              multi={false}
                              searchable={true}
                              placeholder="Select User"
                              disabled={false}
                              loading={this.state.fetchingCustomerUsers}
                            />
                          </div>
                        </div>

                        <SquareButton
                          content="+"
                          onClick={(e) => this.toggleCreateUserModal()}
                          className="add-new-option-customer"
                          bsStyle={"primary"}
                          title="Add New User"
                          disabled={false}
                        />
                      </div>
                      <Input
                        field={{
                          label: "Project Role",
                          type: "PICKLIST",
                          value: this.state.selectedCustomerRole,
                          options: this.getCustomerRoleOptions(),
                          isRequired: false,
                        }}
                        width={12}
                        multi={false}
                        name="status"
                        onChange={(e) => this.handleContactChange(e, "role")}
                        placeholder={`Select`}
                      />
                      <div className="right">
                        <SquareButton
                          content="Cancel"
                          bsStyle={"default"}
                          className="contact-cancel"
                          onClick={() =>
                            this.setState({
                              showContactAddErrorMsg: "",
                              showAddContactForm: false,
                              selectedCustomerName: undefined,
                              selectedCustomerRole: undefined,
                            })
                          }
                        />
                        <SquareButton
                          content="Apply"
                          bsStyle={"primary"}
                          className="contact-save"
                          onClick={() => this.addCustomerContact()}
                        />
                      </div>
                    </div>
                  </div>
                )}
                {this.state.showCreateUserModal && (
                  <CustomerUserNew
                    isVisible={this.state.showCreateUserModal}
                    close={this.closeUserModal}
                    customerId={this.state.project.customer_id}
                  />
                )}
                {this.state.showContactAddErrorMsg != "" && (
                  <div className="label-details-error-msg">
                    {this.state.showContactAddErrorMsg}
                  </div>
                )}
              </div>
              <div className="details-box">
                <div className="label-details">Project Manager</div>
                <div className="label-content">
                  <UsershortInfo
                    name={project.project_manager}
                    url={project.project_manager_profile_pic}
                  />
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Engineer Name</div>
                {this.state.projectEngineers &&
                  this.state.projectEngineers.length === 0 && (
                    <div className="label-content">-</div>
                  )}
                {this.state.projectEngineers &&
                  this.state.projectEngineers.map((engineer, i) => (
                    <div key={i} className="label-content">
                      <UsershortInfo
                        name={`${engineer.first_name} ${engineer.last_name}`}
                        url={engineer.profile_url}
                      />
                    </div>
                  ))}
              </div>
              <div className="details-box">
                <div className="label-details">Pre Sales Engineer</div>
                {this.state.preSalesEngineers &&
                  this.state.preSalesEngineers.length === 0 && (
                    <div className="label-content">-</div>
                  )}
                {this.state.preSalesEngineers &&
                  this.state.preSalesEngineers.map((engineer, i) => (
                    <div key={i} className="label-content">
                      <UsershortInfo
                        name={`${engineer.first_name} ${engineer.last_name}`}
                        url={engineer.profile_url}
                      />
                    </div>
                  ))}
              </div>
              <div className="details-box">
                <div className="label-details">Account Manager</div>
                <div className="label-content">
                  {(this.state.accountManager &&
                    this.state.accountManager.id && (
                      <UsershortInfo
                        name={`${this.state.accountManager.first_name} ${this.state.accountManager.last_name}`}
                        url={this.state.accountManager.profile_url}
                      />
                    )) ||
                    "-"}
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Project Type</div>
                {project.type && !this.state.showEditDropdown ? (
                  <div className="project-type">
                    <div className="label-content">{project.type}</div>
                    <img
                      className={"project-edit-icon"}
                      alt=""
                      src={"/assets/icons/edit.png"}
                      onClick={(e) =>
                        this.setState({
                          showEditDropdown: !this.state.showEditDropdown,
                        })
                      }
                    />
                  </div>
                ) : (
                  <div className="project-type">
                    <Input
                      field={{
                        label: "",
                        type: "PICKLIST",
                        value: project.type_id,
                        options: this.state.projectTypes,
                        isRequired: false,
                      }}
                      width={8}
                      multi={false}
                      name="type_id"
                      onChange={(e) => this.handleTypeChange(e)}
                      placeholder={`Select`}
                    />
                    <div className="action">
                      <img
                        className={"project-save-icon"}
                        alt=""
                        src={"/assets/icons/validated.svg"}
                        onClick={this.handleTypeSave}
                      />
                      <img
                        className={"project-delete-icon"}
                        alt=""
                        src={"/assets/icons/cross-sign.svg"}
                        onClick={(e) =>
                          this.setState({
                            showEditDropdown: !this.state.type,
                            project: {
                              ...this.state.project,
                              type_id: this.state.type,
                              type: this.getProjectTypeLabel(this.state.type),
                            },
                          })
                        }
                      />
                    </div>
                  </div>
                )}
              </div>
            </Accordian>
            <Accordian label={"Documents"} isOpen={false}>
              <div className="details-box">
                <div className="doucments-list">
                  {this.state.showDocumentLoader && (
                    <div className="document-loader">
                      <Spinner className={"document=loader"} show={true} />
                    </div>
                  )}
                  {this.state.projectDocuments.map((document, i) => (
                    <div
                      key={i}
                      className="document"
                      onMouseEnter={() =>
                        this.setState({
                          showDocumentActions: true,
                          editDocumentId: document.id,
                        })
                      }
                      onMouseLeave={() => {
                        if (!this.deleteConfirmBox) {
                          this.setState({
                            showDocumentActions: false,
                            editDocumentId: undefined,
                          });
                        }
                      }}
                    >
                      <div className="doc-right">
                        <img
                          className="file-icons"
                          alt=""
                          src={
                            fileIcons[document.file_name.split(".").pop()] ||
                            "/assets/icons/046-file-45.svg"
                          }
                        />
                        <div className="document-label-content">
                          {document.title}
                        </div>
                      </div>
                      {this.state.showDocumentActions &&
                        this.state.editDocumentId === document.id && (
                          <div className="download-icons">
                            <SmallConfirmationBox
                              setRef={this.setRef}
                              className="remove"
                              onClickOk={() => this.deleteDocument(document)}
                              text={"document"}
                            />
                          </div>
                        )}
                      <img
                        className="download-icons"
                        alt=""
                        src={
                          document.id === this.state.currentDownloadingFile
                            ? "/assets/icons/loading.gif"
                            : "/assets/icons/download.png"
                        }
                        onClick={
                          document.id === this.state.currentDownloadingFile
                            ? null
                            : () => this.downloadDocument(document)
                        }
                      />
                    </div>
                  ))}
                </div>
                <input
                  type="file"
                  style={{ display: "none" }}
                  ref={this.uploadInput}
                  onChange={this.handleFileUpload}
                />
                {this.state.currentUploadedDocument ? (
                  <>
                    <div className="upload-box">
                      <span className="upload-filename">
                        {this.state.currentUploadedDocument.name}
                      </span>
                    </div>
                    <div className="file-upload-actions">
                      <img
                        className="file-action-icons file-upload-save"
                        alt=""
                        src={"/assets/icons/validated.svg"}
                        onClick={() => this.uploadDocument()}
                      />
                      <img
                        className="file-action-icons file-upload-cancel"
                        alt=""
                        src={"/assets/icons/close.png"}
                        onClick={() =>
                          this.setState({ currentUploadedDocument: undefined })
                        }
                      />
                    </div>
                  </>
                ) : (
                  !this.state.showDocumentLoader && (
                    <div
                      className="document-label-details action-item"
                      onClick={() => (this.uploadInput as any).current.click()}
                    >
                      Upload File
                    </div>
                  )
                )}
              </div>
            </Accordian>
            <Accordian label={"Linking"} isOpen={false}>
              <div className="details-box">
                <div className="label-details">SOW Document</div>
                <div className="label-content">
                  <Input
                    field={{
                      label: "",
                      type: "PICKLIST",
                      value: this.state.sow_document,
                      options: this.state.sowDocumentList,
                      isRequired: false,
                    }}
                    width={12}
                    multi={false}
                    name="sow_document"
                    onChange={(e) =>
                      this.handleLinkingChange(e, "sow_document")
                    }
                    placeholder={`Select`}
                  />
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Hardware Opportunity</div>
                <div
                  style={{ flexDirection: "column" }}
                  className="label-content opp-container"
                >
                  <div className="doucments-list">
                    {this.state.opportunity_crm_ids.map((oppId) => (
                      <div key={oppId} className="document">
                        <div className="doc-right">
                          <img
                            className="file-icons"
                            alt=""
                            src={"/assets/icons/046-file-45.svg"}
                          />
                          <div className="document-label-content">
                            {this.getDocumentName(oppId)}
                          </div>
                        </div>
                        <img
                          style={{ width: "10px", height: "10px" }}
                          className="download-icons"
                          alt=""
                          src={"/assets/icons/close.png"}
                          onClick={() => this.removeOpportunity(oppId)}
                        />
                      </div>
                    ))}
                  </div>
                  <div>
                    <Input
                      field={{
                        label: "",
                        type: "PICKLIST",
                        value: this.state.opportunity_crm_ids,
                        options: this.getFilteredCustomerQuotesList(),
                        isRequired: false,
                      }}
                      width={12}
                      multi={false}
                      name="opportunity_crm_ids"
                      onChange={(e) =>
                        this.handleLinkingChange(e, "opportunity_crm_ids")
                      }
                      placeholder={`Select`}
                    />
                  </div>
                </div>
                <SquareButton
                  content="Save"
                  bsStyle={"primary"}
                  className="contact-save"
                  onClick={() => this.saveLinkingDocuments()}
                />
              </div>
            </Accordian>
          </div>
        </div>
        {
          <CustomerForm
            user={this.state.userToEdit}
            onClose={this.toggleCustomerUser}
            show={this.state.isCustomerUserFormOpen}
            onSubmit={this.onEditCustomerUser}
            userRoles={this.state.userRoles}
            isStatusEditable={false}
            userType="provider"
            isLoading={this.state.isEditingUser}
          />
        }

        {
          <ModalBase
            show={this.state.showUpdateDescription}
            onClose={() => this.setState({ showUpdateDescription: false })}
            titleElement={"Update Project Description"}
            bodyElement={
              <div className="col-md-12 update-project-description">
                <Input
                  field={{
                    label: "Description",
                    type: "TEXTAREA",
                    value: project.description,
                    isRequired: false,
                  }}
                  width={12}
                  name="description"
                  onChange={(e) => this.handleChangeEditor(e)}
                  placeholder={``}
                  className="overall-status-edit-input"
                />
                <SquareButton
                  content="Update"
                  bsStyle={"primary"}
                  onClick={() => this.saveProjectDescription()}
                />
              </div>
            }
            footerElement={""}
            className="collector-details"
          />
        }
        {this.state.openBOM && (
          <BOMDetails
            show={this.state.openBOM}
            onClose={this.toggleBOMDetails}
            id={this.props.match.params.id}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  lastVisitedSite: state.pmo.lastVisitedSite,
});

const mapDispatchToProps = (dispatch: any) => ({
  getImpactList: () => dispatch(getImpactList()),
  getTeamMembers: (id: number) => dispatch(getTeamMembers(id)),
  getProjectStatusByProjectID: (projectId: number) =>
    dispatch(getProjectStatusByProjectID(projectId)),
  getProjectDetails: (projectID: number) =>
    dispatch(getProjectDetails(projectID)),
  setProjectDetails: (projectID: number, data: any) =>
    dispatch(setProjectDetails(projectID, data)),
  updateProjectDetails: (data: any, projectID: number) =>
    dispatch(updateProjectDetails(data, projectID)),
  getProjectTypes: () => dispatch(getProjectTypes()),
  getProjectEngineersByID: (projectID: number, type: string) =>
    dispatch(getProjectEngineersByID(projectID, type)),
  getProjectCustomerContactsByID: (projectID: number) =>
    dispatch(getProjectCustomerContactsByID(projectID)),
  getProjectDocumentsByID: (projectID: number) =>
    dispatch(getProjectDocumentsByID(projectID)),
  saveProjectDocumentsByID: (projectID: number, data: any) =>
    dispatch(saveProjectDocumentsByID(projectID, data)),
  deleteProjectDocumentsByID: (projectID: number, documentID: number) =>
    dispatch(deleteProjectDocumentsByID(projectID, documentID)),
  fetchAllCustomerUsers: (id: number, params?: IServerPaginationParams) =>
    dispatch(fetchAllCustomerUsers(id)),
  getContactRoles: () => dispatch(getContactRoles()),
  saveProjectCustomerContactsByID: (projectID: number, data: any) =>
    dispatch(saveProjectCustomerContactsByID(projectID, data)),
  updateCustomerContact: (
    projectID: number,
    contact_crm_id: number,
    data: any
  ) => dispatch(updateCustomerContact(projectID, contact_crm_id, data)),
  deleteCustomerContact: (
    projectID: number,
    contact_crm_id: number,
    contact_record_id: number
  ) =>
    dispatch(
      deleteCustomerContact(projectID, contact_crm_id, contact_record_id)
    ),
  getProjectBoardList: () => dispatch(getProjectBoardList()),
  downloadAttachment: (documentId: number) =>
    dispatch(downloadAttachment(documentId)),
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  editCustomerUser: (id: string, user: ISuperUser) =>
    dispatch(editCustomerUser(id, user)),
  getCustomerUser: (id: string, custId: any) =>
    dispatch(getCustomerUser(id, custId)),
  fetchUserTypes: () => dispatch(fetchUserTypes()),
  getProjectAdditionalContactsByID: (projectId: number) =>
    dispatch(getProjectAdditionalContactsByID(projectId)),
  saveProjectAdditionalContactsByID: (projectId: number, data: any) =>
    dispatch(saveProjectAdditionalContactsByID(projectId, data)),
  updateAdditionalContact: (projectId: number, contactId: number, data: any) =>
    dispatch(updateAdditionalContact(projectId, contactId, data)),
  deleteAdditionalContact: (projectId: number, contactId: number) =>
    dispatch(deleteAdditionalContact(projectId, contactId)),
  getTabledataSOWMetrics: (params?: IServerPaginationParams) =>
    dispatch(getTabledataSOWMetrics(params)),
  getCustomerQuotes: (customerId: number, params: any) =>
    dispatch(getCustomerQuotes(customerId, params)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetails);
