import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { isEmpty } from "lodash";
import {
  fetchSowDocs,
  FETCH_SOW_DOC_LIST_SUCCESS,
} from "../../../../actions/sow";
import SimpleList from "../../../../components/List";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import { useProject } from "../projectContext";

interface LinkedSoWProps {
  sowId: number;
  quotes: IQuote[];
  isFetchingQuotes: boolean;
  setData: (sow: number) => void;
  getSowList: (quoteIds: number[]) => Promise<any>;
}

const LinkedSoW: React.FC<LinkedSoWProps> = (props) => {
  const [rows, setRows] = useState<ISowDocListRow[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const { project } = useProject();

  const psQuotes: number[] = useMemo(() => {
    let arr = [];
    if (!props.isFetchingQuotes) {
      // Fetch those sows whose opportunity names starts with PS
      const regex = new RegExp(`^ps`, "i");
      arr = props.quotes.filter((el) => regex.test(el.name)).map((el) => el.id);
    }
    return arr;
  }, [props.quotes, props.isFetchingQuotes]);

  useEffect(() => {
    if (!isEmpty(psQuotes)) fetchSowList(psQuotes);
  }, [psQuotes]);

  const onRadioChanged = (e: React.ChangeEvent<HTMLInputElement>) => {
    const linkedSow = Number(e.target.value);
    props.setData(linkedSow);
  };

  const columns: IListColumn<ISowDocListRow>[] = [
    {
      name: "",
      className: "sow-first-col radio-btn-group",
      Cell: (row) => (
        <div className="radio-btn">
          <input
            type="radio"
            name={`sow-link-radio`}
            //   onClick={(e) => onRadioChanged(e)}
            onChange={onRadioChanged}
            value={`${row.id}`}
            checked={props.sowId == row.id}
            title={"Select a single SoW to link to project"}
          />
        </div>
      ),
    },
    {
      name: "Opportunity Name",
      Cell: (row) => row.quote.name,
    },
    {
      id: "name",
      name: "SoW Name",
    },
    {
      name: "Won Date",
      Cell: (row) =>
        fromISOStringToFormattedDate(
          row.quote.closed_date,
          "MMM DD, YYYY"
        ) || "-",
    },
    {
      id: "author_name",
      name: "Author",
    },
  ];

  const fetchSowList = (quotes: number[]) => {
    setLoading(true);
    props
      .getSowList(quotes)
      .then((action) => {
        if (action.type === FETCH_SOW_DOC_LIST_SUCCESS) {
          const res: ISowDocListRow[] = action.response;
          // If already a mapped sow, map that
          const sowIds = res.map((el) => el.id);
          if (project.sow_document && sowIds.includes(project.sow_document))
            props.setData(project.sow_document);
          setRows(res);
        }
      })
      .finally(() => setLoading(false));
  };

  return (
    <div className="setup-linking-table">
      <h3 className="setup-sub-header">Statement of Work</h3>
      <SimpleList
        uniqueKey={"id"}
        noDataString="No linked SoWs found"
        loading={loading || props.isFetchingQuotes}
        columns={columns}
        rows={rows}
        className="setup-linking-list"
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getSowList: (quoteIds: number[]) =>
    dispatch(
      fetchSowDocs({
        pagination: false,
        quote_id: quoteIds.join(","),
        doc_type: "Fixed Fee,T & M"
      })
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(LinkedSoW);
