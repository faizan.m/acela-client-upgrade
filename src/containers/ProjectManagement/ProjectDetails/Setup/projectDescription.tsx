import React from "react";
import Input from "../../../../components/Input/input";

interface ProjectDescriptionProps {
  description: string;
  error?: IFieldValidation;
  setData: (description: string) => void;
}

const ProjectDescription: React.FC<ProjectDescriptionProps> = (props) => {
  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    props.setData(e.target.value);
  };

  return (
    <Input
      field={{
        label: "Project Description",
        value: props.description,
        type: "TEXTAREA",
        isRequired: false,
      }}
      width={8}
      name="description"
      className="project-type-input"
      placeholder={`Enter Description`}
      onChange={handleChange}
      error={props.error}
    />
  );
};

export default ProjectDescription;
