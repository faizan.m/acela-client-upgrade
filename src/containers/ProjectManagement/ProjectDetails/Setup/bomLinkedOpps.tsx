import React, {  useState } from "react";
import SimpleList from "../../../../components/List";
import Checkbox from "../../../../components/Checkbox/checkbox";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";

interface BomLinkedOppsProps {
  oppIds: number[];
  quotes: IQuote[];
  error?: IFieldValidation;
  isFetchingQuotes: boolean;
  setData: (oppIds: number[]) => void;
}

const BomLinkedOpps: React.FC<BomLinkedOppsProps> = (props) => {
  const oppIdSet: Set<number> = new Set(props.oppIds);
  const [noOpp, setNoOpp] = useState<boolean>(false);

  const onChangeNoOpp = (e: React.ChangeEvent<HTMLInputElement>) => {
    setNoOpp(e.target.checked);
    props.setData([]);
  };

  const onChangeOpp = (e: React.ChangeEvent<HTMLInputElement>) => {
    const oppId = Number(e.target.name);
    const prevOpps: Set<number> = new Set(props.oppIds);
    if (e.target.checked) prevOpps.add(oppId);
    else prevOpps.delete(oppId);
    props.setData(Array.from(prevOpps));
  };

  const columns: IListColumn<IQuote>[] = [
    {
      name: "",
      className: "bom-first-col",
      Cell: (row) => (
        <Checkbox
          name={String(row.id)}
          isChecked={oppIdSet.has(row.id)}
          title={
            noOpp
              ? "Unable to check as no hardware opportunity should be linked to this project"
              : "Check this to link the opportunity for BoM"
          }
          onChange={onChangeOpp}
          className="bom-opp-checkbox"
          disabled={noOpp}
        />
      ),
    },
    {
      id: "name",
      name: "Opportunity Name",
    },
    {
      id: "closed_date",
      name: "Won Date",
      Cell: (row) =>
        fromISOStringToFormattedDate(row.closed_date, "MMM DD, YYYY") || "-",
    },
    {
      id: "type_name",
      name: "Opportunity Type",
    },
  ];

  return (
    <div className="setup-linking-table">
      <div className="setup-sub-header">
        <h3>BoM</h3>
        <Checkbox
          name={"noOpp"}
          isChecked={noOpp}
          onChange={onChangeNoOpp}
          className="setup-subheader-checkbox"
        >
          No hardware opportunity related to this Project
        </Checkbox>
      </div>
      <SimpleList
        uniqueKey={"id"}
        noDataString="No hardware open quotes found for this customer"
        loading={props.isFetchingQuotes}
        columns={columns}
        rows={props.quotes}
        className="setup-linking-list"
      />
    </div>
  );
};

export default BomLinkedOpps;
