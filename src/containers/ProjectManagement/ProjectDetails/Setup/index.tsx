import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import { getCustomerQuotes, GET_QUOTES_SUCCESS } from "../../../../actions/sow";
import {
  addErrorMessage,
  addSuccessMessage,
} from "../../../../actions/appState";
import {
  updateProjectDetails,
  updateWorkplanPhases,
  projectDefaultTETicketCRU,
  PROJECT_WORK_PHASE_SUCCESS,
  PROJECT_WORK_PHASE_FAILURE,
  PROJECT_DETAILS_UPDATE_SUCCESS,
  PROJECT_DETAILS_UPDATE_FAILURE,
  DEFAULT_TIME_ENTRY_TICKET_SUCCESS,
  DEFAULT_TIME_ENTRY_TICKET_FAILURE,
} from "../../../../actions/pmo";
import Spinner from "../../../../components/Spinner";
import SquareButton from "../../../../components/Button/button";
import LinkedSow from "./linkedSow";
import ProjectType from "./projectType";
import BomLinkedOpps from "./bomLinkedOpps";
import WorkplanPhases from "./workplanPhases";
import TimeEntrySetup from "./teDefaultTickets";
import ProjectDescription from "./projectDescription";
import { commonFunctions } from "../../../../utils/commonFunctions";
import { useProject } from "../projectContext";

interface SetupTabProps {
  onConfiguringSetup: () => void;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getQuotesListing: (customerId: number) => Promise<any>;
  updatePhases: (projectID: number, data: IProjectWorkPhase[]) => Promise<any>;
  updateProjectDetails: (
    data: Partial<IProjectDetails>,
    projectID: number
  ) => Promise<any>;
  updateDefaultTickets: (
    method: HTTPMethods,
    projectID: number,
    data: TimeEntryTickets
  ) => Promise<any>;
}

const SetupTab: React.FC<SetupTabProps> = (props) => {
  const [quotes, setQuotes] = useState<IQuote[]>([]);
  const [bomOpps, setBOMOpps] = useState<number[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [projectType, setProjectType] = useState<number>();
  const [description, setDescription] = useState<string>("");
  const [linkedSow, setLinkedSow] = useState<number>(null);
  const [error, setError] = useState<IErrorValidation>({});
  const [phases, setPhases] = useState<IProjectWorkPhase[]>([]);
  const [fetchingQuotes, setFetchingQuotes] = useState<boolean>(false);
  const [defaultTickets, setDefaultTickets] = useState<TimeEntryTickets>({
    id: null,
    default_pm_ticket_id: null,
    default_engineer_ticket_id: null,
  });

  const { project, updateProject } = useProject();

  const hwQuotes: IQuote[] = useMemo(() => {
    let arr = [];
    if (quotes.length) {
      // Filter those quotes whose opportunity names starts with HW i.e Hardware Opportunities
      const regex = new RegExp(`^hw`, "i");
      arr = quotes.filter((el) => regex.test(el.name));
      const availableIds: Set<number> = new Set(arr.map((el) => el.id));
      const existingOppIds = project.opportunity_crm_ids
        ? project.opportunity_crm_ids
        : [];
      const existingIdsInQuotes = existingOppIds.filter((el) =>
        availableIds.has(el)
      );
      setBOMOpps(existingIdsInQuotes);
    }
    return arr;
  }, [quotes]);

  useEffect(() => {
    if (project) {
      fetchQuotes(project.customer_id);
      if (project.type_id) setProjectType(project.type_id);
      if (project.description) setDescription(project.description);
      if (project.sow_document) setLinkedSow(project.sow_document);
      if (project.opportunity_crm_ids) setBOMOpps(project.opportunity_crm_ids);
    }
  }, [project]);

  const fetchQuotes = (customerID: number) => {
    setFetchingQuotes(true);
    props
      .getQuotesListing(customerID)
      .then((action) => {
        if (action.type === GET_QUOTES_SUCCESS) {
          setQuotes(action.response);
        }
      })
      .finally(() => setFetchingQuotes(false));
  };

  const isValid = (): boolean => {
    let isValid = true;
    const err = cloneDeep(error);
    if (!projectType) {
      isValid = false;
      err.projectType = commonFunctions.getErrorState("This field is required");
    } else err.projectType = commonFunctions.getErrorState();

    if (
      !defaultTickets.default_engineer_ticket_id ||
      !defaultTickets.default_pm_ticket_id
    ) {
      isValid = false;
      err.defaultTickets = commonFunctions.getErrorState(
        "This field is required"
      );
    } else err.defaultTickets = commonFunctions.getErrorState();

    setError(err);
    return isValid;
  };

  const onSetupSave = () => {
    if (isValid()) {
      setLoading(true);
      const updates = {
        type: projectType,
        sow_document: linkedSow,
        description: description,
        opportunity_crm_ids: bomOpps,
      };
      const tePayload = {
        default_pm_ticket_id: defaultTickets.default_pm_ticket_id,
        default_engineer_ticket_id: defaultTickets.default_engineer_ticket_id,
      };
      const phasePayload = phases
        .filter((el) => el.role_type)
        .map((el) => ({
          phase: el.id,
          role_type: el.role_type,
        }));
      const projectId = project.id;
      setLoading(true);
      Promise.all([
        props.updateProjectDetails(updates, projectId),
        props.updatePhases(projectId, phasePayload),
        props.updateDefaultTickets(
          defaultTickets.id ? "put" : "post",
          projectId,
          tePayload
        ),
      ])
        .then(([updateDetailRes, phaseRes, teRes]) => {
          if (updateDetailRes.type === PROJECT_DETAILS_UPDATE_FAILURE) {
            props.addErrorMessage(
              "Error updating project type, project description sow and bom linked opportunities!"
            );
            const errors = updateDetailRes.errorList.data;
            if (errors && Array.isArray(errors) && errors.length) {
              errors.forEach((obj: object) => {
                props.addErrorMessage(Object.values(obj)[0]);
              });
            }
          }
          if (phaseRes.type === PROJECT_WORK_PHASE_FAILURE) {
            props.addErrorMessage("Error saving project workplan phases!");
          }
          if (teRes.type === DEFAULT_TIME_ENTRY_TICKET_FAILURE) {
            props.addErrorMessage("Error saving default time entry tickets!");
          }

          if (updateDetailRes.type === PROJECT_DETAILS_UPDATE_SUCCESS) {
            updateProject({
              ...updateDetailRes.response,
              opportunity_crm_ids: bomOpps,
              sow_document: linkedSow,
            });
          }
          if (
            updateDetailRes.type === PROJECT_DETAILS_UPDATE_SUCCESS &&
            phaseRes.type === PROJECT_WORK_PHASE_SUCCESS &&
            teRes.type === DEFAULT_TIME_ENTRY_TICKET_SUCCESS
          ) {
            props.addSuccessMessage("Project setup completed successfully!");
            props.onConfiguringSetup();
          }
        })
        .finally(() => setLoading(false));
    }
  };

  return (
    <div className="project-setup-tab">
      <Spinner show={loading} className="project-setup-spinner" />
      <LinkedSow
        sowId={linkedSow}
        quotes={quotes}
        isFetchingQuotes={fetchingQuotes}
        setData={(data) => setLinkedSow(data)}
      />
      <BomLinkedOpps
        oppIds={bomOpps}
        quotes={hwQuotes}
        isFetchingQuotes={fetchingQuotes}
        setData={(data) => setBOMOpps(data)}
      />
      <div className="project-setup-details">
        <ProjectType
          error={error.projectType}
          projectType={projectType}
          setData={(data) => setProjectType(data)}
        />
        <ProjectDescription
          description={description}
          setData={(data) => setDescription(data)}
        />
      </div>
      <WorkplanPhases phases={phases} setData={(data) => setPhases(data)} />
      <TimeEntrySetup
        tickets={defaultTickets}
        error={error.defaultTickets}
        setData={(data) => setDefaultTickets(data)}
      />
      <SquareButton
        content="Save"
        onClick={onSetupSave}
        className="setup-save-btn"
        bsStyle={"primary"}
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (msg: string) => dispatch(addErrorMessage(msg)),
  addSuccessMessage: (msg: string) => dispatch(addSuccessMessage(msg)),
  updateProjectDetails: (data: Partial<IProjectDetails>, projectID: number) =>
    dispatch(updateProjectDetails(data, projectID)),
  getQuotesListing: (customerId: number) =>
    dispatch(getCustomerQuotes(customerId, { "won-only": true })),
  updatePhases: (projectID: number, data: IProjectWorkPhase[]) =>
    dispatch(updateWorkplanPhases(projectID, data)),
  updateDefaultTickets: (
    method: HTTPMethods,
    projectID: number,
    data: TimeEntryTickets
  ) => dispatch(projectDefaultTETicketCRU(method, projectID, data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SetupTab);
