import React, { useEffect } from "react";
import { connect } from "react-redux";
import {
  getProjectTickets,
  projectDefaultTETicketCRU,
  DEFAULT_TIME_ENTRY_TICKET_SUCCESS,
} from "../../../../actions/pmo";
import Input from "../../../../components/Input/input";
import { useProject } from "../projectContext";

interface TimeEntrySetupProps {
  tickets: TimeEntryTickets;
  error?: IFieldValidation;
  loadingOptions: boolean;
  ticketOptions: IPickListOptions[];
  setData: (tickets: TimeEntryTickets) => void;
  getProjectTickets: (projectId: number) => Promise<any>;
  fetchDefaultTickets: (projectId: number) => Promise<any>;
}

const TimeEntrySetup: React.FC<TimeEntrySetupProps> = (props) => {
  const { project } = useProject();

  useEffect(() => {
    fetchDefaultTickets();
    props.getProjectTickets(project.crm_id);
  }, []);

  const fetchDefaultTickets = () => {
    props.fetchDefaultTickets(project.id).then((action) => {
      if (action.type === DEFAULT_TIME_ENTRY_TICKET_SUCCESS) {
        props.setData(action.response);
      }
    });
  };

  const handleChangeTicket = (e: React.ChangeEvent<HTMLInputElement>) => {
    const prevData = { ...props.tickets };
    prevData[e.target.name] = Number(e.target.value);
    props.setData(prevData);
  };

  return (
    <div className="time-entry-default-tickets">
      <h3 className="setup-sub-header">Time Entry Setup</h3>
      <div className="time-entry-setup-table">
        <div className="te-setup-row">
          <div className="role-col">Role</div>
          <div className="ticket-col">Meeting Ticket</div>
        </div>
        <div className="te-setup-row">
          <div className="role-col">Project Manager</div>
          <div className="ticket-col">
            <Input
              field={{
                label: null,
                value: props.tickets.default_pm_ticket_id,
                type: "PICKLIST",
                options: props.ticketOptions,
                isRequired: false,
              }}
              width={12}
              name="default_pm_ticket_id"
              loading={props.loadingOptions}
              className="default-ticket-input"
              placeholder={`Select Ticket`}
              onChange={handleChangeTicket}
              error={
                !props.tickets.default_pm_ticket_id ? props.error : undefined
              }
            />
          </div>
        </div>
        <div className="te-setup-row">
          <div className="role-col">Engineer</div>
          <div className="ticket-col">
            <Input
              field={{
                label: null,
                value: props.tickets.default_engineer_ticket_id,
                type: "PICKLIST",
                options: props.ticketOptions,
                isRequired: false,
              }}
              width={12}
              name="default_engineer_ticket_id"
              loading={props.loadingOptions}
              className="default-ticket-input"
              placeholder={`Select Ticket`}
              onChange={handleChangeTicket}
              error={
                !props.tickets.default_engineer_ticket_id
                  ? props.error
                  : undefined
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  loadingOptions: state.pmo.isFetching,
  ticketOptions: state.pmo.projectTickets,
});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectTickets: (projectID: number) =>
    dispatch(getProjectTickets(projectID)),
  fetchDefaultTickets: (projectId: number) =>
    dispatch(projectDefaultTETicketCRU("get", projectId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(TimeEntrySetup);
