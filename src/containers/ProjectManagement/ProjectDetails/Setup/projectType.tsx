import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  getProjectTypes,
  PROJECT_TYPES_SUCCESS,
} from "../../../../actions/pmo";
import Input from "../../../../components/Input/input";

interface ProjectTypeProps {
  projectType: number;
  error?: IFieldValidation;
  setData: (type: number) => void;
  getProjectTypes: () => Promise<any>;
}

const ProjectType: React.FC<ProjectTypeProps> = (props) => {
  const [loadingOptions, setLoadingOptions] = useState<boolean>(false);
  const [typeOptions, setTypeOptions] = useState<IPickListOptions[]>([]);

  useEffect(() => {
    fetchProjectTypeOptions();
  }, []);

  const fetchProjectTypeOptions = () => {
    setLoadingOptions(true);
    props
      .getProjectTypes()
      .then((action) => {
        if (action.type === PROJECT_TYPES_SUCCESS) {
          const typeOptions = action.response.map((data) => ({
            value: data.id,
            label: `${data.title}`,
          }));
          setTypeOptions(typeOptions);
        }
      })
      .finally(() => setLoadingOptions(false));
  };

  const handleChangeType = (e: React.ChangeEvent<HTMLInputElement>) => {
    const projectType = Number(e.target.value);
    props.setData(projectType);
  };

  return (
    <Input
      field={{
        label: "Project Type",
        value: props.projectType,
        type: "PICKLIST",
        options: typeOptions,
        isRequired: false,
      }}
      width={4}
      name="project-type"
      loading={loadingOptions}
      className="project-type-input"
      placeholder={`Select Project Type`}
      onChange={handleChangeType}
      error={props.error}
    />
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectTypes: () => dispatch(getProjectTypes()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectType);
