import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { cloneDeep } from "lodash";
import {
  getWorkplanPhases,
  PROJECT_WORK_PHASE_SUCCESS,
} from "../../../../actions/pmo";
import { getRoleTypes, ROLE_TYPE_SUCCESS } from "../../../../actions/setting";
import Input from "../../../../components/Input/input";
import SimpleList from "../../../../components/List";
import { useProject } from "../projectContext";

interface WorkplanPhasesProps {
  phases: IProjectWorkPhase[];
  getRoles: () => Promise<any>;
  setData: (phases: IProjectWorkPhase[]) => void;
  fetchPhases: (projectId: number) => Promise<any>;
}

const WorkplanPhases: React.FC<WorkplanPhasesProps> = (props) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [loadingOptions, setLoadingOptions] = useState<boolean>(false);
  const [roleOptions, setRoleOptions] = useState<IPickListOptions[]>([]);
  const { projectID } = useProject();

  useEffect(() => {
    fetchPhases();
    fetchRoleOptions();
  }, []);

  const handleChangeRole = (
    e: React.ChangeEvent<HTMLInputElement>,
    dataIdx: number
  ) => {
    const role_type = e.target.value;
    const role_name = roleOptions.find((el) => el.value == role_type)
      .label as string;
    const newPhases = cloneDeep(props.phases);
    newPhases[dataIdx].role_type = Number(role_type);
    newPhases[dataIdx].role_type_name = role_name;
    props.setData(newPhases);
  };

  const columns: IListColumn<IProjectWorkPhase>[] = [
    {
      id: "description",
      name: "CW Work Plan Phases",
      className: "phase-col-1",
      Cell: (row, idx) => (
        <>
          <span>{`Phase ${idx + 1}`}</span>
          <span>{row.description}</span>
        </>
      ),
    },
    {
      id: "budget_hours",
      name: "Phase Hours",
      className: "width-10 phase-hours-col",
      Cell: (row) =>
        row.budget_hours === null || row.budget_hours === undefined
          ? "N.A"
          : row.budget_hours,
    },
    {
      name: "Phase to Role Mapping",
      className: "phase-role-col",
      Cell: (row, idx) => (
        <Input
          field={{
            label: null,
            value: row.role_type,
            type: "PICKLIST",
            options: roleOptions,
            isRequired: false,
          }}
          width={12}
          name="role_type"
          loading={loadingOptions}
          className="phase-role-input"
          placeholder={`Select Role`}
          onChange={(e) => handleChangeRole(e, idx)}
        />
      ),
    },
  ];

  const fetchPhases = () => {
    props
      .fetchPhases(projectID)
      .then((action) => {
        if (action.type === PROJECT_WORK_PHASE_SUCCESS) {
          props.setData(action.response);
        }
      })
      .finally(() => setLoading(false));
  };

  const fetchRoleOptions = () => {
    setLoadingOptions(true);
    props
      .getRoles()
      .then((action) => {
        if (action.type === ROLE_TYPE_SUCCESS) {
          const res: IDRoleObject[] = action.response;
          setRoleOptions(
            res.map((el) => ({
              value: el.id,
              label: el.role,
            }))
          );
        }
      })
      .finally(() => setLoadingOptions(false));
  };

  return (
    <div className="workplan-phase-table">
      <h3 className="setup-sub-header">Map CW Work Phases to Roles</h3>
      <SimpleList
        uniqueKey={"id"}
        loading={loading}
        columns={columns}
        rows={props.phases}
        className="phase-roles-list"
        noDataString="CW workplan phases not found for this project"
      />
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getRoles: () => dispatch(getRoleTypes()),
  fetchPhases: (projectId: number) => dispatch(getWorkplanPhases(projectId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WorkplanPhases);
