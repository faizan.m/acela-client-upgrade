import { flatMap } from "lodash";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
// import TagsInput from "react-tagsinput"; (New Component)

import {
  getDataCommon,
  GET_DATA_SUCCESS,
} from "../../../../actions/orderTracking";
import SquareButton from "../../../../components/Button/button";
import Checkbox from "../../../../components/Checkbox/checkbox";
import Input from "../../../../components/Input/input";
import PMOCollapsible from "../../../../components/PMOCollapsible";
import { fromISOStringToFormattedDate } from "../../../../utils/CalendarUtil";
import { exportCSVFile } from "../../../../utils/download";
import { searchInFields } from "../../../../utils/searchListUtils";
import "./bomDetails.scss";
import { useProject } from "../projectContext";

interface BOMDetailsProps {
  getDataCommon?: any;
}

const BOMDetails: React.FC<BOMDetailsProps> = (props) => {
  const { project } = useProject();
  const [searchKey, setSearchKey] = useState<string>("");
  const [salesOrderData, setSalesOrderData] = useState<any[]>([]);
  const [lineItems, setLineItems] = useState<any[]>([]);

  const [hide_received, setHide_received] = useState<boolean>(false);
  const [hide_zero_cost, setHide_zero_cost] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [viewAllNote, setViewAllNote] = useState<boolean>(false);

  useEffect(() => {
    if (project.id) {
      getProjectOrderTrackingData(project.id);
    }
  }, [project.id]);

  useEffect(() => {
    getAllData();
  }, [salesOrderData, viewAllNote]);

  const getProjectOrderTrackingData = (id) => {
    setLoading(true);
    props
      .getDataCommon(`providers/pmo/projects/${id}/order-tracking`)
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          const salesOrderData = action.response.flatMap((x) =>
            x.sales_order_data.map((ele) => {
              return { ...ele, salesOrderID: x.sales_order_id };
            })
          );
          setSalesOrderData(salesOrderData);
          setLoading(false);
        } else {
          setLoading(false);
        }
      });
  };

  const getAllData = async () => {
    setLoading(true);
    const data = salesOrderData.map((board) =>
      props.getDataCommon(
        `providers/purchase-order-tickets/${board.service_ticket_id}/notes?all_notes=${viewAllNote}`
      )
    );
    const infos: any = await Promise.all(data);
    salesOrderData.forEach((board, index) => {
      const notes = infos[index].response;
      if (Array.isArray(notes)) {
        board.line_items_data.map((x) => {
          x.note = notes;
          x.service_ticket_id = board.service_ticket_id;
          x.salesOrderID = board.salesOrderID;
          return x;
        });
        board.noteData = notes;
      } else {
        board.line_items_data.map((x) => {
          x.note = [notes];
          x.service_ticket_id = board.service_ticket_id;
          x.salesOrderID = board.salesOrderID;
          return x;
        });
        board.noteData = [notes];
      }
    });
    setSalesOrderData(salesOrderData);
    const lineItems = flatMap(salesOrderData, (x) => {
      return x.line_items_data;
    });
    setLineItems(lineItems);
    setLoading(false);
  };

  // const handleChangeTableSN = (event: any, data?: any, index?: number) => {
  //   salesOrderData[index].serial_numbers = event;
  //   setSalesOrderData(salesOrderData);
  // };

  // const handleChangeTableTrackingNo = (
  //   event: any,
  //   data?: any,
  //   index?: number
  // ) => {
  //   salesOrderData[index].tracking_number = event.join(",");
  //   setSalesOrderData(salesOrderData);
  // };
  const renderPurchaseOrders = (salesOrderData, id) => {
    const searchString = searchKey;
    let filteredData = salesOrderData;
    if (searchString.length > 0) {
      filteredData = filteredData.filter((row) =>
        searchInFields(row, searchString, [
          "description",
          "po_number",
          "product_id",
          "note.text",
        ])
      );
    }

    if (hide_received) {
      filteredData = filteredData.filter(
        (item) => item.received_status !== "FullyReceived"
      );
    }
    if (hide_zero_cost) {
      filteredData = filteredData.filter((item) => item.unit_cost !== 0);
    }
    return (
      filteredData &&
      filteredData.map((data, index) => (
        <div
          key={data.id}
          className={`product-details-container ${data.selected && "selected"}`}
        >
          <>
            <div className="product-detail top-secton">
              <div className="product-id">
                <div
                  className="detail-title"
                  title={`Cost : ${data.unit_cost}, ${data.received_status}`}
                >
                  Product ID
                </div>
                <div className="product-data">{data.product_id || "-"}</div>
              </div>
              <div>
                <div className="detail-title">Description</div>
                <div className="product-data po-description">
                  {data.description}
                </div>
              </div>
              <div>
                <div className="detail-title">Purchased Qty</div>
                <div className="quantity-pending product-data purchased-qty">
                  {data.ordered_quantity ? data.ordered_quantity : "-"}
                </div>
              </div>
              <div className="recieved-qty-actions">
                <div className="detail-title">Received Qty</div>
                <div className="quantity-p quantity-shipped product-data shipped-qty">
                  {data.received_quantity ? data.received_quantity : "-"}
                </div>
              </div>
              <div className="recieved-qty-actions">
                <div className="detail-title">Estimated Delivery Date</div>
                <div className="quantity-p quantity-shipped product-data shipped-qty">
                  {data.ship_date
                    ? fromISOStringToFormattedDate(data.ship_date)
                    : "-"}
                </div>
              </div>
            </div>
            <div className="product-detail middle-section">
              <div className="tag-input">
                <div className="detail-title">Tracking Number</div>
                {/* <TagsInput
                  value={getTrackingNumbersFormatted(data.tracking_numbers)}
                  onChange={(e) => handleChangeTableTrackingNo(e, data, index)}
                  inputProps={{
                    className: "react-tagsinput-input",
                    placeholder: "",
                  }}
                  addOnBlur={true}
                  disabled={true}
                /> */}
              </div>
              <div className="tag-input">
                <div className="detail-title">Serial Number</div>
                {/* <TagsInput
                  value={data.serial_numbers || []}
                  onChange={(e) => handleChangeTableSN(e, data, index)}
                  inputProps={{
                    className: "react-tagsinput-input",
                    placeholder: "",
                  }}
                  addOnBlur={true}
                  disabled={true}
                /> */}
              </div>
            </div>
          </>
        </div>
      ))
    );
  };
  // const getTrackingNumbersFormatted = (data) => {
  //   let result = [];
  //   if (Array.isArray(data)) {
  //     result = data;
  //   } else {
  //     result = data && data.length > 0 && data !== "" ? data.split(",") : [];
  //   }

  //   return result;
  // };
  const renderNotes = (salesOrderData, ticketId) => {
    return (
      salesOrderData &&
      salesOrderData.map(
        (data, index) =>
          data.noteData &&
          data.noteData
            .filter(
              (x) => x && x.ticketId && x.ticketId.toString() === ticketId
            )
            .map((note, index) => (
              <div key={data.id} className={`product-details-container`}>
                <div className="note-bom" title={note.ticketId}>
                  Note : {note && note.text}
                </div>
              </div>
            ))
      )
    );
  };

  const handleChangeViewAllNote = (e: any) => {
    setViewAllNote(!viewAllNote);
  };

  const renderHeader = () => {
    return (
      <div className="bom-details-body-v2">
        <div className="left-header col-md-7">
          <Input
            field={{
              label: "",
              type: "SEARCH",
              value: searchKey,
              isRequired: false,
            }}
            width={7}
            placeholder="Search"
            name="searchKey"
            className="purchaseOrder-select"
            onChange={(e) => setSearchKey(e.target.value)}
            multi={false}
          />
          <div className="bom-checkboxes col-md-4">
            <Checkbox
              isChecked={hide_zero_cost}
              name="hide_zero_cost"
              onChange={(e) => setHide_zero_cost(e.target.checked)}
              className="exclude-services-checkbox"
            >
              Hide zero cost items
            </Checkbox>
            <Checkbox
              isChecked={hide_received}
              name="hide_received"
              onChange={(e) => setHide_received(e.target.checked)}
              className="exclude-services-checkbox"
            >
              Hide received
            </Checkbox>
          </div>
        </div>
        <div className="action-buttons-bom col-md-5">
          <SquareButton
            content={`${viewAllNote ? "Hide" : "View"}  Shipping Note`}
            bsStyle={"primary"}
            onClick={(e) => handleChangeViewAllNote(e)}
            disabled={lineItems.length === 0}
            className="bom-btn"
          />
          <SquareButton
            content={`Export Tracking Data`}
            bsStyle={"primary"}
            onClick={(e) => download()}
            disabled={lineItems.length === 0}
            className="bom-btn"
          />
        </div>
      </div>
    );
  };
  const groupByFn = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };
  const getBody = () => {
    const groupedByInvalid = groupByFn(lineItems, "service_ticket_id");

    return (
      <div className="edit-rule">
        <div className={`${loading ? `loading` : ""}`}>
          {renderHeader()}
          {loading && <div className="details-loader">Loading...</div>}
          <div className="bom-data-section-v2">
            <div className="purchase-order-list-bom">
              {groupedByInvalid &&
                Object.keys(groupedByInvalid).map((data, i) => (
                  <div key={i} className="outer-collaps col-md-12">
                    <PMOCollapsible
                      background={""}
                      label={`PO Number - ${groupedByInvalid[data][0].po_number} / SO - ${groupedByInvalid[data][0].salesOrderID}`}
                      isOpen={true}
                    >
                      {renderNotes(salesOrderData, data)}
                      {renderPurchaseOrders(groupedByInvalid[data], 0)}
                    </PMOCollapsible>
                  </div>
                ))}
            </div>
            {lineItems.length === 0 && !loading && (
              <div className="no-data">Not Available</div>
            )}
          </div>
        </div>
      </div>
    );
  };

  const download = () => {
    let headers = {
      product_id: "Product Id",
      description: "Description",
      ordered_quantity: "Purchased Qty",
      received_quantity: "Received Qty",
      tracking_numbers: "Tracking Numbers",
      serial_numbers: "Serial Numbers",
    };
    let itemsFormatted = [];
    let itemsNotFormatted = lineItems;
    itemsNotFormatted.forEach((item) => {
      itemsFormatted.push({
        product_id: item.product_id ? `"${item.product_id}"` : "N.A.",
        description: item.description ? `"${item.description}"` : "N.A.",
        ordered_quantity: item.product_id
          ? `"${item.ordered_quantity}"`
          : "N.A.",
        received_quantity: item.product_id
          ? `"${item.received_quantity}"`
          : "N.A.",
        tracking_numbers: item.product_id
          ? `"${item.tracking_numbers.join(", ")}"`
          : "N.A.",
        serial_numbers: item.product_id
          ? `"${item.serial_numbers.join(", ")}"`
          : "N.A.",
      });
    });

    var fileTitle = "BOM Details";
    exportCSVFile(headers, itemsFormatted, fileTitle);
  };

  return <div className="bom-details-v2">{getBody()}</div>;
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getDataCommon: (url: any) => dispatch(getDataCommon(url)),
});

export default connect(mapStateToProps, mapDispatchToProps)(BOMDetails);
