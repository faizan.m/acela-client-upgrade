import React from "react";
import { cloneDeep } from "lodash";
import { connect } from "react-redux";
import moment, { Moment } from "moment";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  getImpactList,
  getTeamMembers,
  getProjectTypes,
  meetingComplete,
  getActionItemList,
  getProjectDetails,
  getProjectTickets,
  saveMeetingDetails,
  getMeetingTemplates,
  getMeetingPDFPreview,
  getProjectEngineersByID,
  getMeetingTemplateSetting,
  getMeetingNotesByProjectID,
  getProjectStatusByProjectID,
  saveMeetingNotesByProjectID,
  updateMeetingNotesByProjectID,
  getMeetingAttendeesByProjectID,
  getProjectCustomerContactsByID,
  saveMeetingAttendeesByProjectID,
  getProjectAdditionalContactsByID,
  deleteMeetingAttendeesByProjectID,
  getExternalMeetingAttendeesByProjectID,
  saveExternalMeetingAttendeesByProjectID,
  deleteExternalMeetingAttendeesByProjectID,
  MEETING_SUCCESS,
  TEMPLATES_SUCCESS,
  PROJECT_TYPES_SUCCESS,
  PROJECT_DETAILS_SUCCESS,
  MEETING_COMPLETE_SUCCESS,
  MEETING_ATTENDEES_SUCCESS,
  PROJECT_ENGINEERS_SUCCESS,
  PROJECT_MEETING_NOTES_SUCCESS,
  FETCH_MEETING_PDF_PREVIEW_SUCCESS,
  EXTERNAL_MEETING_ATTENDEES_SUCCESS,
} from "../../../actions/pmo";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import Accordian from "../../../components/Accordian";
import UsershortInfo from "../../../components/UserImage";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import PMOCollapsible from "../../../components/PMOCollapsible";
import AppValidators from "../../../utils/validator";
import { commonFunctions } from "../../../utils/commonFunctions";
import {
  getConvertedColorWithOpacity,
  isEmptyObj,
} from "../../../utils/CommonUtils";
import AgendaTopic from "./agendaTopics";
import MeetingNotes from "./meetingNotes";
import TimeEntry from "./Meeting/timeEntry";
import CTCOMeeting from "./Meeting/ctCoMeeting";
import MeetingDocuments from "./meetingDocuments";
import CloseMeeting from "./Meeting/closeMeeting";
import ProjectItem from "./projectItem";
import "./style.scss";

interface IProjectMeetingProps extends ICommonProps {
  getImpactList: () => Promise<any>;
  getProjectTypes: () => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getTeamMembers: (id: number) => Promise<any>;
  getMeetingTemplateSetting: () => Promise<any>;
  getMeetingTemplates: (url: string) => Promise<any>;
  getProjectTickets: (projectId: number) => Promise<any>;
  getProjectDetails: (projectId: number) => Promise<any>;
  getProjectCustomerContactsByID: (id: number) => Promise<any>;
  getProjectStatusByProjectID: (projectId: number) => Promise<any>;
  getActionItemList: (projectId: number, api: string) => Promise<any>;
  getProjectAdditionalContactsByID: (projectId: number) => Promise<any>;
  getProjectEngineersByID: (projectID: number, type?: string) => Promise<any>;
  meetingComplete: (
    projectID: number,
    method: HTTPMethods,
    api: string
  ) => Promise<any>;
  saveMeetingDetails: (
    projectID: number,
    data: any, // change to ProjectMeeting after removing the old meeting close flow
    api: string
  ) => Promise<any>;
  getMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number
  ) => Promise<any>;
  saveMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    note: string
  ) => Promise<any>;
  updateMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    noteId: number,
    note: string
  ) => Promise<any>;
  getMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number
  ) => Promise<any>;
  deleteMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) => Promise<any>;
  saveMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) => Promise<any>;
  getMeetingPDFPreview: (
    projectId: number,
    meetingId: number,
    completed: boolean,
    sent_to_cc: boolean
  ) => Promise<any>;
  getExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number
  ) => Promise<any>;
  saveExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) => Promise<any>;
  deleteExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) => Promise<any>;
}

interface IProjectMeetingState {
  type: string;
  view: boolean;
  saved: boolean;
  isOpen: boolean;
  loading: boolean;
  template: string;
  noteData: string;
  pdfLoading: boolean;
  openPreview: boolean;
  saveClicked: boolean;
  project?: IProjectDetails;
  closeMeetingModal: boolean;
  previewHTML: { url: string };
  showTimeEntriesModal: boolean;
  accountManager: IProjectEngMan;
  projectMeeting: IProjectMeeting;
  impactList: IStatusImpactItem[];
  projectTypes: IPickListOptions[];
  ownerOptions: IPickListOptions[];
  projectAdditionalContacts: any[];
  isFetchingMeetingDetails: boolean;
  projectEngineers: IProjectEngMan[];
  isFetchingCustomerContacts: boolean;
  preSalesEngineers: IProjectEngMan[];
  meetingAttendees: IMeetingAttendee[];
  projectStatusList: IStatusImpactItem[];
  meetingNote: { id?: number; note: string };
  externalMeetingAttendees: IMeetingAttendee[];
  projectCustomerContacts: IProjectCustomerContact[];
}

const project = {} as IProjectDetails;
const regularMeetings = ["Status", "Kickoff", "Internal Kickoff"];

class ProjectMeeting extends React.Component<
  IProjectMeetingProps,
  IProjectMeetingState
> {
  constructor(props: IProjectMeetingProps) {
    super(props);
    this.state = this.getEmptyState();
  }

  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  static validator = new AppValidators();
  getEmptyState = () => ({
    closeMeetingModal: false,
    showTimeEntriesModal: false,
    project,
    loading: false,
    isOpen: true,
    ownerOptions: [],
    projectTypes: [],
    impactList: [],
    projectStatusList: [],
    type: "",
    template: "",
    projectMeeting: {
      name: "",
      schedule_start_datetime: moment(),
      schedule_end_datetime: moment().add(1, "hours"),
      status: "Pending",
      meeting_type: "",
      email_body: "",
      email_subject: "",
      email_body_text: "",
      email_body_replaced_text: "",
      project_acceptance_markdown: "",
      project_acceptance_text: "",
      project_acceptance_replaced_text: "",
      preview_ordering: {
        CriticalPathItem: {
          ordering_field: "",
        },
        ActionItem: {
          ordering_field: "",
        },
        RiskItem: {
          ordering_field: "",
        },
      },
    },
    closeMeeting: false,
    view: false,
    meetingNote: {
      note: "",
    },
    noteData: "",
    projectCustomerContacts: [],
    meetingAttendees: [],
    projectEngineers: [],
    isFetchingMeetingDetails: false,
    accountManager: {
      id: "",
      member_id: 0,
      first_name: "",
      last_name: "",
      name: "",
      profile_url: "",
    },
    isFetchingCustomerContacts: false,
    openPreview: false,
    previewHTML: null,
    preSalesEngineers: [],
    pdfLoading: false,
    externalMeetingAttendees: [],
    projectAdditionalContacts: [],
    saved: false,
    saveClicked: false,
    send_to_cc: false,
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState((prevState) => ({
      project: {
        ...prevState.project,
        id,
      },
    }));
    this.getProjectTypes();
    this.fetchImpactList();
    this.getProjectDetails(id);
    this.fetchProjectContacts(id);
    this.fetchProjectStatus(id);
    this.getProjectEngineersByID(id);
    this.getPreSalesProjectEngineersByID(id);
    this.props.getMeetingTemplateSetting();
    const query = new URLSearchParams(this.props.location.search);
    const type = query.get("type") as MeetingType;
    const view = query.get("view") === "true" ? true : false;
    const completed = query.get("completed") === "true" ? true : false;
    const template = query.get("template");
    const meetingID = this.props.match.params.meetingID;
    const newState = cloneDeep(this.state);
    newState.projectMeeting.name = type;
    (newState.type as string) = type;
    (newState.view as boolean) = view;
    (newState.template as string) = template;
    this.setState(newState);
    if (view) {
      if (completed) {
        let meeting = JSON.parse(localStorage.getItem("meeting")) as any;
        if (type === "Customer Touch" || type === "Close Out") {
          meeting = meeting.meeting;
          (newState.projectMeeting as IProjectMeeting) = meeting || {};
        } else {
          (newState.projectMeeting as IProjectMeeting) = meeting.meeting || {};
        }
        (newState.projectMeeting.critical_path_items as IProjectDetailItem[]) =
          (meeting && meeting.critical_path_items) || [];
        (newState.projectMeeting
          .risk_items as IProjectDetailItem[]) = meeting.risk_items;
        (newState.meetingNote as any) =
          meeting.meeting_notes && meeting.meeting_notes[0];
        (newState.projectMeeting
          .action_items as IProjectDetailItem[]) = meeting.action_items;
        newState.projectMeeting.email_body = meeting.email_body || "";
        newState.projectMeeting.email_subject = meeting.email_subject || "";
        (newState.meetingAttendees as any) = meeting.meeting_attendee;
        this.setState(newState);
      } else {
        this.getCompletedMeeting();
      }
    } else {
      if (meetingID && meetingID !== "0") {
        this.getMeeting(meetingID, type);
        this.getMeetingNotesByProjectID(id, meetingID);
        this.getMeetingAttendeesByProjectID(id, meetingID);
        this.getExternalMeetingAttendeesByProjectID(id, meetingID);
      } else if (regularMeetings.includes(type)) {
        this.getMeetingTemplate(template);
      }
    }
  }

  getProjectEngineersByID = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectEngineersByID(id).then((action) => {
      if (action.type === PROJECT_ENGINEERS_SUCCESS) {
        this.setState({
          projectEngineers: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  getPreSalesProjectEngineersByID = (id: number) => {
    this.setState({ loading: true });
    this.props
      .getProjectEngineersByID(id, "pre-sales-engineers")
      .then((action) => {
        if (action.type === PROJECT_ENGINEERS_SUCCESS) {
          this.setState({
            preSalesEngineers: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  fetchProjectContacts = (projectID: number) => {
    this.setState({ loading: true, isFetchingCustomerContacts: true });
    Promise.all([
      this.props.getActionItemList(projectID, "account-manager"),
      this.props.getTeamMembers(projectID),
      this.props.getProjectCustomerContactsByID(projectID),
      this.props.getProjectAdditionalContactsByID(projectID),
    ]).then(
      ([accManager, teamMembers, customerContacts, additionalContacts]) => {
        const usersList: {
          user_id: string;
          name: string;
          profile_url: string;
        }[] = [
          ...teamMembers.response.map((el) => ({
            name: el.name,
            profile_url: el.profile_url,
            user_id: el.id,
          })),
          ...customerContacts.response.map((el) => ({
            name: el.name,
            profile_url: el.profile_url,
            user_id: el.user_id,
          })),
          ...additionalContacts.response.map((el) => {
            return {
              name: el.name,
              user_id: el.id,
              profile_url: el.profile_url,
            };
          }),
        ];

        usersList.push({
          user_id: accManager.response.id,
          name: accManager.response.name,
          profile_url: accManager.response.profile_url,
        });
        this.state.project.project_manager &&
          usersList.push({
            user_id: this.state.project.project_manager_id,
            name: this.state.project.project_manager,
            profile_url: this.state.project.project_manager_profile_pic,
          });
        const options: IPickListOptions[] = usersList.map((el) => ({
          value: el.user_id,
          label: (
            <UsershortInfo
              name={el.name}
              url={el.profile_url}
              className="inside-box"
            />
          ),
        }));
        this.setState({
          loading: false,
          ownerOptions: options,
          isFetchingCustomerContacts: false,
          accountManager: accManager.response,
          projectCustomerContacts: customerContacts.response,
          projectAdditionalContacts: additionalContacts.response,
        });
      }
    );
  };

  fetchProjectStatus = (id: number) => {
    this.props.getProjectStatusByProjectID(id).then((action) => {
      this.setState({ projectStatusList: action.response });
    });
  };

  fetchImpactList = () => {
    this.props.getImpactList().then((action) => {
      this.setState({ impactList: action.response });
    });
  };

  getProjectDetails = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectDetails(id).then((action) => {
      if (action.type === PROJECT_DETAILS_SUCCESS) {
        const project: IProjectDetails = action.response;
        const ownerOptions = cloneDeep(this.state.ownerOptions);
        ownerOptions.push({
          value: project.project_manager_id,
          label: (
            <UsershortInfo
              name={project.project_manager}
              url={project.project_manager_profile_pic}
              className="inside-box"
            />
          ),
        });
        this.setState({
          project,
          ownerOptions,
        });
        this.props.getProjectTickets(project.crm_id);
      }
      this.setState({ loading: false });
    });
  };

  getProjectTypes = () => {
    this.setState({ loading: true });
    this.props.getProjectTypes().then((action) => {
      if (action.type === PROJECT_TYPES_SUCCESS) {
        const projectTypes = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));
        this.setState({ projectTypes });
      }
      this.setState({ loading: false });
    });
  };

  getMeetingNotesByProjectID = (projectId: number, meetingId: number) => {
    this.props
      .getMeetingNotesByProjectID(projectId, meetingId)
      .then((action) => {
        if (action.type === PROJECT_MEETING_NOTES_SUCCESS) {
          if (action.response.length > 0) {
            this.setState({
              meetingNote: action.response[0],
              noteData: action.response[0].note,
            });
          }
        }
        this.setState({ loading: false });
      });
  };

  getMeetingAttendeesByProjectID = (projectId: number, meetingId: number) => {
    this.props
      .getMeetingAttendeesByProjectID(projectId, meetingId)
      .then((action) => {
        if (action.type === MEETING_ATTENDEES_SUCCESS) {
          this.setState({
            meetingAttendees: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  getExternalMeetingAttendeesByProjectID = (
    projectId: number,
    meetingId: number
  ) => {
    this.props
      .getExternalMeetingAttendeesByProjectID(projectId, meetingId)
      .then((action) => {
        if (action.type === EXTERNAL_MEETING_ATTENDEES_SUCCESS) {
          this.setState({
            externalMeetingAttendees: action.response,
          });
        }
        this.setState({ loading: false });
      });
  };

  saveMeetingAttendeesByProjectID = (userId: number) => {
    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;

    const data = {
      attendee: userId,
    };

    this.props
      .saveMeetingAttendeesByProjectID(projectID, meetingID, data)
      .then((action) => {
        if (action.type === MEETING_ATTENDEES_SUCCESS) {
          this.getMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  saveExternalMeetingAttendeesByProjectID = (userId: number) => {
    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;

    const data = {
      external_contact: userId,
    };

    this.props
      .saveExternalMeetingAttendeesByProjectID(projectID, meetingID, data)
      .then((action) => {
        if (action.type === EXTERNAL_MEETING_ATTENDEES_SUCCESS) {
          this.getExternalMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  deleteMeetingAttendeesByProjectID = (meetingAttendee: any) => {
    this.setState({ loading: true });

    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;
    const meetingAttendeeId = meetingAttendee.id;

    this.props
      .deleteMeetingAttendeesByProjectID(
        projectID,
        meetingID,
        meetingAttendeeId
      )
      .then((action) => {
        if (action.type === MEETING_ATTENDEES_SUCCESS) {
          this.getMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  deleteExternalMeetingAttendeesByProjectID = (meetingAttendee: any) => {
    this.setState({ loading: true });

    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;
    const meetingAttendeeId = meetingAttendee.id;

    this.props
      .deleteExternalMeetingAttendeesByProjectID(
        projectID,
        meetingID,
        meetingAttendeeId
      )
      .then((action) => {
        if (action.type === EXTERNAL_MEETING_ATTENDEES_SUCCESS) {
          this.getExternalMeetingAttendeesByProjectID(projectID, meetingID);
        }
        this.setState({ loading: false });
      });
  };

  exitCloseMeetingModal = (save: boolean = false) => {
    if (save === true)
      this.setState({
        view: true,
        closeMeetingModal: false,
      });
    else this.setState({ closeMeetingModal: false });
  };

  exitCloseTimeEntryModal = () => {
    this.setState({ showTimeEntriesModal: false });
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.projectMeeting[e.target.name] = e.target.value;
    this.setState(newState);
  };

  handleChangeStart = (e: { target: { name: string; value: Moment } }) => {
    const newState = cloneDeep(this.state);
    const targetValue = e.target.value;
    newState.projectMeeting.schedule_start_datetime = targetValue;
    const end = moment(targetValue).add(1, "hours");
    (newState.projectMeeting.schedule_end_datetime as any) = end;
    this.setState(newState);
  };

  onSaveClick = (showAlert: boolean = true) => {
    const projectID = this.props.match.params.id;
    let projectMeeting = this.state.projectMeeting;
    if (
      regularMeetings.includes(this.state.type) &&
      projectMeeting.agenda_topics.length > 0 &&
      projectMeeting.agenda_topics.findIndex((agenda_topic) =>
        commonFunctions.isEditorEmpty(agenda_topic.topic)
      ) !== -1
    ) {
      this.setState({ saveClicked: true });
      return;
    }
    this.setState({ loading: true });
    if (
      !projectMeeting.agenda_topics ||
      (projectMeeting.agenda_topics &&
        projectMeeting.agenda_topics.length === 0)
    ) {
      projectMeeting.agenda_topics = null;
    }
    if (this.state.type === "Internal Kickoff") {
      projectMeeting.is_internal = true;
    }
    let url = `${getURLBYMeetingType(this.state.type)}`;
    const meetingID = this.props.match.params.meetingID;

    if (meetingID && meetingID !== "0") {
      url = `${getURLBYMeetingType(this.state.type)}/${
        this.props.match.params.meetingID
      }`;
    }
    this.props
      .saveMeetingDetails(projectID, projectMeeting, url)
      .then((action) => {
        if (action.type === MEETING_SUCCESS) {
          this.state.noteData && this.meetingNotesAction(action.response.id);
          if (showAlert)
            this.props.addSuccessMessage(`Meeting saved successfully!`);
        }
      })
      .finally(() => this.setState({ loading: false, saveClicked: false }));
  };

  meetingNotesAction = (
    meetingID: string = this.props.match.params.meetingID
  ) => {
    const projectID = this.props.match.params.id;

    if (this.state.meetingNote.id) {
      // edit note
      const noteId = this.state.meetingNote.id;
      this.props
        .updateMeetingNotesByProjectID(
          projectID,
          Number(meetingID),
          noteId,
          this.state.noteData
        )
        .then((action) => {
          if (action.type === PROJECT_MEETING_NOTES_SUCCESS) {
            this.setState({
              meetingNote: action.response,
              noteData: action.response.note,
              saved: true,
            });
            setTimeout(() => this.setState({ saved: false }), 5000);
          }
        });
    } else {
      // save note
      this.props
        .saveMeetingNotesByProjectID(
          projectID,
          Number(meetingID),
          this.state.noteData
        )
        .then((action) => {
          if (action.type === PROJECT_MEETING_NOTES_SUCCESS) {
            this.setState({
              meetingNote: action.response,
              noteData: action.response.note,
              saved: true,
            });
            setTimeout(() => this.setState({ saved: false }), 5000);
          }
          this.setState({ loading: false });
        });
    }
  };

  getCompletedMeeting = () => {
    const projectID = this.props.match.params.id;
    const meetingID = this.props.match.params.meetingID;
    const url = `meetings/${meetingID}/archive`;
    this.setState({ loading: true });
    this.props
      .meetingComplete(projectID, "get", url)
      .then((action) => {
        if (action.type === MEETING_COMPLETE_SUCCESS) {
          const newState = cloneDeep(this.state);
          (newState.projectMeeting as IProjectMeeting) = action.response.meeting;
          newState.projectMeeting.agenda_topics =
            action.response.meeting.agenda_topics;
          newState.projectMeeting.critical_path_items =
            action.response.critical_path_items;
          newState.projectMeeting.risk_items = action.response.risk_items;
          (newState.meetingNote as any) =
            action.response.meeting_notes && action.response.meeting_notes[0];
          newState.projectMeeting.action_items = action.response.action_items;
          newState.projectMeeting.email_body =
            action.response.meeting.email_body || "";
          newState.projectMeeting.email_subject =
            action.response.meeting.email_subject || "";
          (newState.meetingAttendees as any) = action.response.meeting_attendee;
          this.setState(newState);
        }
      });
  };

  callbackfn = (agenda_topics: IAgendaTopic[]) => {
    const newState = cloneDeep(this.state);
    newState.projectMeeting.agenda_topics = agenda_topics;
    this.setState(newState, () => {
      this.onSaveClick(false);
    });
  };

  callbackCustomerTouchMeeting = (data: IMeetingEmailTemplate) => {
    const newState = cloneDeep(this.state);
    newState.projectMeeting.email_subject = data.email_subject;
    newState.projectMeeting.email_body = data.email_body_markdown;
    newState.projectMeeting.email_body_replaced_text = data.email_body_text;
    this.setState(newState);
  };

  callbackCloseOutMeeting = (data: IMeetingEmailTemplate) => {
    const newState = cloneDeep(this.state);
    newState.projectMeeting.email_subject = data.email_subject;
    newState.projectMeeting.project_acceptance_replaced_text =
      data.email_body_text;
    newState.projectMeeting.project_acceptance_markdown =
      data.email_body_markdown;
    this.setState(newState);
  };

  callbackMeetingNote = (note: string, autoSave: boolean) => {
    const newState = cloneDeep(this.state);
    (newState.meetingNote.note as any) = note;
    (newState.noteData as any) = note;
    (newState.saved as boolean) = false;
    this.setState(newState, () => {
      if (autoSave) {
        const meetingID = this.props.match.params.meetingID;
        this.meetingNotesAction(meetingID);
      }
    });
  };
  callbackSorting = (field: string, type: string) => {
    const newState = cloneDeep(this.state);
    (newState.projectMeeting.preview_ordering[type]
      .ordering_field as any) = field;
    this.setState(newState);
  };

  getMeetingTemplate = (template: string) => {
    this.setState({ loading: true });
    const url = `settings/meeting-templates/${template}`;

    this.props.getMeetingTemplates(url).then((action) => {
      const newState = cloneDeep(this.state);
      newState.projectMeeting.agenda_topics = action.response.agenda_topics;
      newState.projectMeeting.action_items = action.response.action_items;
      this.setState(newState);
    });
  };

  getMeeting = (id: number, type: MeetingType) => {
    this.setState({ loading: true, isFetchingMeetingDetails: true });
    const url = `projects/${this.props.match.params.id}/${getURLBYMeetingType(
      type
    )}/${id}`;
    this.props.getMeetingTemplates(url).then((action) => {
      if (action.type === TEMPLATES_SUCCESS) {
        const newState = cloneDeep(this.state);
        (newState.projectMeeting as IProjectMeeting) = action.response;
        if (
          regularMeetings.includes(type) &&
          isEmptyObj(newState.projectMeeting.preview_ordering)
        ) {
          newState.projectMeeting.preview_ordering = this.getEmptyState().projectMeeting.preview_ordering;
        }
        (newState.isFetchingMeetingDetails as boolean) = false;
        (newState.view as boolean) = action.response.status === "Completed";
        this.setState(newState);
      }
    });
  };

  handleChangeAttendeeCheck = (e: any, userId: any) => {
    if (e.target.checked) {
      this.saveMeetingAttendeesByProjectID(userId);
    } else {
      const attendeeArr =
        this.state.meetingAttendees &&
        this.state.meetingAttendees.filter((e) => e.attendee === userId);
      if (attendeeArr.length > 0) {
        this.deleteMeetingAttendeesByProjectID(attendeeArr[0]);
      }
    }
  };

  handleChangeExternalAttendeeCheck = (e: any, userId: any) => {
    if (e.target.checked) {
      this.saveExternalMeetingAttendeesByProjectID(userId);
    } else {
      const attendeeArr =
        this.state.externalMeetingAttendees &&
        this.state.externalMeetingAttendees.filter(
          (e) => e.external_contact === userId
        );
      if (attendeeArr.length > 0) {
        this.deleteExternalMeetingAttendeesByProjectID(attendeeArr[0]);
      }
    }
  };

  onPDFPreviewClick = () => {
    this.setState({ pdfLoading: true });
    this.onSaveClick(false);

    const meetingID = this.props.match.params.meetingID;
    const query = new URLSearchParams(this.props.location.search);
    const completed = query.get("completed") === "true" ? true : false;

    this.props
      .getMeetingPDFPreview(this.state.project.id, meetingID, completed, true)
      .then((a) => {
        if (a.type === FETCH_MEETING_PDF_PREVIEW_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            pdfLoading: false,
          });
        } else {
          this.setState({
            pdfLoading: false,
          });
        }
      });
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  renderAdditionalContacts = () => {
    return (
      <div style={{ paddingTop: "15px" }}>
        <div className="label-details">
          {this.state.projectAdditionalContacts.length > 0 ? "Resources" : ""}
        </div>
        {this.state.projectAdditionalContacts.map((contact, idx) => (
          <div className="contact-container" key={contact.id}>
            <div className={`label-content ${contact.id}`}>
              <UsershortInfo name={contact.name} url={contact.profile_url} />
              {["Kickoff", "Status"].includes(this.state.type) && (
                <Checkbox
                  isChecked={
                    this.state.externalMeetingAttendees &&
                    this.state.externalMeetingAttendees.filter(
                      (e) => e.external_contact === contact.id
                    ).length > 0
                  }
                  name="externalMeetingAttendees"
                  onChange={(e) =>
                    this.handleChangeExternalAttendeeCheck(e, contact.id)
                  }
                  disabled={this.state.view}
                />
              )}
            </div>
            {<div className="label-sub-content">{contact.role}</div>}
          </div>
        ))}
      </div>
    );
  };

  render() {
    const project = this.state.project;
    const projectEngineers = this.state.projectEngineers || [];
    return (
      <div className="project-details meeting-details">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        {regularMeetings.includes(this.state.type) && (
          <PDFViewer
            show={this.state.openPreview}
            onClose={this.toggleOpenPreview}
            titleElement={`Meeting PDF Preview`}
            previewHTML={{
              file_path: this.state.previewHTML && this.state.previewHTML.url,
            }}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={"default"}
                onClick={this.toggleOpenPreview}
              />
            }
            className=""
          />
        )}
        <div className="header">
          <div className="left">
            <div className="project-name">
              {project.customer} - {project.title}
            </div>
          </div>
          <div className="right">
            {regularMeetings.includes(this.state.type) && (
              <SquareButton
                content={
                  this.state.pdfLoading ? (
                    <>
                      Loading PDF
                      <img
                        className="icon__loading-white"
                        src="/assets/icons/loading.gif"
                        alt="Downloading File"
                      />
                    </>
                  ) : (
                    "Preview Meeting PDF"
                  )
                }
                bsStyle={"primary"}
                onClick={this.onPDFPreviewClick}
                disabled={this.state.pdfLoading}
              />
            )}
            {this.state.view ? (
              <SquareButton
                content="Edit Time Entries"
                bsStyle={"primary"}
                onClick={() => this.setState({ showTimeEntriesModal: true })}
                disabled={this.state.loading}
              />
            ) : (
              <SquareButton
                content="Close Meeting"
                bsStyle={"primary"}
                onClick={() => this.setState({ closeMeetingModal: true })}
                disabled={this.state.loading}
              />
            )}
            {!this.state.view && (
              <SquareButton
                content="Save"
                bsStyle={"primary"}
                onClick={(e) => this.onSaveClick()}
                disabled={
                  this.state.isFetchingMeetingDetails || this.state.loading
                }
              />
            )}
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.props.history.goBack}
            />
          </div>
        </div>
        <div className="first-row">
          <div className="action-second status-actions-row">
            <div className="project-action-component collapsible-section-meeting">
              {regularMeetings.includes(this.state.type) && (
                <>
                  <div className="name-date">
                    <Input
                      field={{
                        label: "Name",
                        type: "TEXT",
                        value: this.state.projectMeeting.name,
                        isRequired: false,
                      }}
                      width={10}
                      labelIcon={"info"}
                      name="name"
                      onChange={(e) => this.handleChange(e)}
                      placeholder={`Enter name`}
                      disabled={this.state.view}
                      className="meeeting-name-field"
                    />
                    <div className="date-time">
                      <Input
                        field={{
                          label: "Date",
                          type: "DATE",
                          value: this.state.projectMeeting
                            .schedule_start_datetime,
                          isRequired: false,
                        }}
                        width={4}
                        labelIcon={"info"}
                        name="schedule_start_datetime"
                        onChange={(e) => this.handleChangeStart(e)}
                        placeholder={`Select Date`}
                        showTime={false}
                        disablePrevioueDates={true}
                        disabled={this.state.view}
                      />
                      <div className="time-to-from">
                        <Input
                          field={{
                            label: "Time",
                            type: "DATE",
                            value: this.state.projectMeeting
                              .schedule_start_datetime,
                            isRequired: false,
                          }}
                          width={4}
                          labelIcon={"info"}
                          name="schedule_start_datetime"
                          onChange={(e) => this.handleChangeStart(e)}
                          showTime={true}
                          showDate={false}
                          hideIcon={true}
                          disablePrevioueDates={true}
                          disabled={this.state.view}
                        />
                        <div className="dash-separator">-</div>
                        <Input
                          field={{
                            label: " ",
                            type: "DATE",
                            value: this.state.projectMeeting
                              .schedule_end_datetime,
                            isRequired: false,
                          }}
                          width={4}
                          labelIcon={"info"}
                          name="schedule_end_datetime"
                          onChange={(e) => this.handleChange(e)}
                          showTime={true}
                          showDate={false}
                          hideIcon={true}
                          className="to-time"
                          disablePrevioueDates={true}
                          disabled={this.state.view}
                        />
                      </div>
                    </div>
                  </div>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Agenda Topics"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <AgendaTopic
                        saveClicked={this.state.saveClicked}
                        viewOnly={this.state.view}
                        initialData={this.state.projectMeeting.agenda_topics}
                        setAgenda={this.callbackfn}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Critical Path Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ProjectItem
                        initialData={
                          this.state.projectMeeting.critical_path_items
                        }
                        viewOnly={this.state.view}
                        itemType={"critical-path-items"}
                        project={this.state.project}
                        ownerOptions={this.state.ownerOptions}
                        statusList={this.state.projectStatusList}
                        impactList={this.state.impactList}
                        sorting={this.state.projectMeeting.preview_ordering}
                        sortingKey={"CriticalPathItem"}
                        callbackSorting={(e, key) =>
                          this.callbackSorting(e, key)
                        }
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#550aad"
                    label={"Action Items"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ProjectItem
                        initialData={this.state.projectMeeting.action_items}
                        viewOnly={this.state.view}
                        itemType={"action-items"}
                        project={this.state.project}
                        ownerOptions={this.state.ownerOptions}
                        statusList={this.state.projectStatusList}
                        impactList={this.state.impactList}
                        sorting={this.state.projectMeeting.preview_ordering}
                        sortingKey={"ActionItem"}
                        callbackSorting={(e, key) =>
                          this.callbackSorting(e, key)
                        }
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#c007d0"
                    label={"Risks"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <ProjectItem
                        initialData={this.state.projectMeeting.risk_items}
                        viewOnly={this.state.view}
                        itemType={"risk-items"}
                        project={this.state.project}
                        ownerOptions={this.state.ownerOptions}
                        statusList={this.state.projectStatusList}
                        impactList={this.state.impactList}
                        sorting={this.state.projectMeeting.preview_ordering}
                        sortingKey={"RiskItem"}
                        callbackSorting={(e, key) =>
                          this.callbackSorting(e, key)
                        }
                      />
                    )}
                  </PMOCollapsible>
                  {!this.state.loading &&
                    hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <MeetingNotes
                        note={
                          this.state.meetingNote && this.state.meetingNote.note
                        }
                        callbackMeetingNote={this.callbackMeetingNote}
                        saved={this.state.saved}
                      />
                    )}
                </>
              )}
              {["Closeout", "Close Out"].includes(this.state.type) && (
                <>
                  <PMOCollapsible
                    background="#164da5"
                    label={"Project Acceptance"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <CTCOMeeting
                        viewOnly={this.state.view}
                        data={{
                          email_subject: this.state.projectMeeting
                            .email_subject,
                          email_body_markdown: this.state.projectMeeting
                            .project_acceptance_markdown,
                          email_body_text: this.state.projectMeeting
                            .project_acceptance_text,
                        }}
                        setData={this.callbackCloseOutMeeting}
                      />
                    )}
                  </PMOCollapsible>
                  <PMOCollapsible
                    background="#c007d0"
                    label={"Meeting Documents"}
                    isOpen={this.state.isOpen}
                  >
                    {hasProjectMeetingIDs(
                      this.state.project,
                      this.state.projectMeeting
                    ) && (
                      <MeetingDocuments
                        meetingID={this.props.match.params.meetingID}
                        projectID={this.state.project.id}
                        viewOnly={this.state.view}
                      />
                    )}
                  </PMOCollapsible>
                </>
              )}
              {["Customer Touch"].includes(this.state.type) && (
                <div className="list-actions">
                  <CTCOMeeting
                    viewOnly={this.state.view}
                    data={{
                      email_subject: this.state.projectMeeting.email_subject,
                      email_body_markdown: this.state.projectMeeting.email_body,
                      email_body_text: this.state.projectMeeting
                        .email_body_replaced_text,
                    }}
                    setData={this.callbackCustomerTouchMeeting}
                  />
                </div>
              )}
            </div>
          </div>
          <div className="status-overall-accordians">
            <Accordian label={"Project Details"} isOpen={true}>
              <div className="details-box">
                <div className="status-overall">
                  <div className="action">
                    <div className="status-text">Overall Status:</div>
                    <div className="field-section color-preview" title={""}>
                      <div
                        style={{
                          backgroundColor: project.overall_status_color,
                          borderColor: project.overall_status_color,
                        }}
                        className="left-column"
                      />
                      <div
                        style={{
                          background: `${getConvertedColorWithOpacity(
                            project.overall_status_color
                          )}`,
                        }}
                        className="text"
                      >
                        {project.overall_status}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Customer</div>
                <div className="label-content">{project.customer}</div>
              </div>
              <div className="details-box">
                <div className="label-details">Customer Contacts</div>
                {this.state.projectCustomerContacts.length > 0 &&
                  regularMeetings.includes(this.state.type) && (
                    <div className="label-sub-content-attended-text">
                      Attended?
                    </div>
                  )}
                {this.state.projectCustomerContacts.map((contact, idx) => (
                  <div className="contact-container" key={idx}>
                    <div className={`label-content ${contact.contact_crm_id}`}>
                      <UsershortInfo
                        name={contact.name}
                        url={contact.profile_url}
                      />
                      {["Kickoff", "Status"].includes(this.state.type) && (
                        <Checkbox
                          isChecked={
                            this.state.meetingAttendees &&
                            this.state.meetingAttendees.filter(
                              (e) => e.attendee === contact.user_id
                            ).length > 0
                          }
                          name="meetingAttendees"
                          onChange={(e) =>
                            this.handleChangeAttendeeCheck(e, contact.user_id)
                          }
                          disabled={
                            this.state.view ||
                            contact.user_id === undefined ||
                            contact.user_id === null
                          }
                        />
                      )}
                    </div>
                    {
                      <div className="label-sub-content">
                        {contact.project_role}
                      </div>
                    }
                  </div>
                ))}
                <div className="contact-container">
                  {this.renderAdditionalContacts()}
                </div>
              </div>

              <div className="details-box">
                <div className="label-details">Project Manager</div>
                <div className="label-content">
                  <UsershortInfo
                    name={project.project_manager}
                    url={project.project_manager_profile_pic}
                  />
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Engineer Name</div>
                {projectEngineers.length === 0 && (
                  <div className="label-content">-</div>
                )}
                {projectEngineers.map((engineer, idx) => (
                  <div className="label-content" key={idx}>
                    <UsershortInfo
                      name={`${engineer.first_name} ${engineer.last_name}`}
                      url={engineer.profile_url}
                    />
                    {regularMeetings.includes(this.state.type) && (
                      <Checkbox
                        isChecked={
                          this.state.meetingAttendees &&
                          this.state.meetingAttendees.filter(
                            (e) => e.attendee === engineer.id
                          ).length > 0
                        }
                        name="meetingAttendees"
                        onChange={(e) =>
                          this.handleChangeAttendeeCheck(e, engineer.id)
                        }
                        disabled={
                          this.state.view ||
                          engineer.id === undefined ||
                          engineer.id === null
                        }
                      />
                    )}
                  </div>
                ))}
              </div>
              <div className="details-box">
                <div className="label-details">Pre Sales Engineer</div>
                {this.state.preSalesEngineers &&
                  this.state.preSalesEngineers.length === 0 && (
                    <div className="label-content">-</div>
                  )}
                {this.state.preSalesEngineers &&
                  this.state.preSalesEngineers.map((engineer, idx) => (
                    <div className="label-content" key={idx}>
                      <UsershortInfo
                        name={`${engineer.first_name} ${engineer.last_name}`}
                        url={engineer.profile_url}
                      />
                      {regularMeetings.includes(this.state.type) && (
                        <Checkbox
                          isChecked={
                            this.state.meetingAttendees &&
                            this.state.meetingAttendees.filter(
                              (e) => e.attendee === engineer.id
                            ).length > 0
                          }
                          name="meetingAttendees"
                          onChange={(e) =>
                            this.handleChangeAttendeeCheck(e, engineer.id)
                          }
                          disabled={
                            this.state.view ||
                            engineer.id === undefined ||
                            engineer.id === null
                          }
                        />
                      )}
                    </div>
                  ))}
              </div>
              <div className="details-box">
                <div className="label-details">Account Manager</div>
                <div className="label-content">
                  {(this.state.accountManager &&
                    this.state.accountManager.id && (
                      <>
                        <UsershortInfo
                          name={`${this.state.accountManager.first_name} ${this.state.accountManager.last_name}`}
                          url={this.state.accountManager.profile_url}
                        />
                        {regularMeetings.includes(this.state.type) && (
                          <Checkbox
                            isChecked={
                              this.state.meetingAttendees &&
                              this.state.meetingAttendees.filter(
                                (e) =>
                                  e.attendee === this.state.accountManager.id
                              ).length > 0
                            }
                            name="meetingAttendees"
                            onChange={(e) =>
                              this.handleChangeAttendeeCheck(
                                e,
                                this.state.accountManager.id
                              )
                            }
                            disabled={
                              this.state.view ||
                              project.project_manager_id === undefined ||
                              project.project_manager_id === null
                            }
                          />
                        )}
                      </>
                    )) ||
                    "-"}
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Project Type</div>
                <div className="project-type">
                  <div className="label-content">{project.type}</div>
                </div>
              </div>
            </Accordian>
          </div>
        </div>
        {this.state.closeMeetingModal && (
          <CloseMeeting
            show={true}
            engineers={this.state.projectEngineers}
            attendees={this.state.meetingAttendees}
            meetingId={Number(this.props.match.params.meetingID)}
            onClose={this.exitCloseMeetingModal}
            projectDetails={this.state.project}
            meetingDetails={this.state.projectMeeting}
          />
        )}
        {this.state.showTimeEntriesModal && (
          <TimeEntry
            show={true}
            isMeetingComplete={true}
            engineers={this.state.projectEngineers}
            attendees={this.state.meetingAttendees}
            meetingId={Number(this.props.match.params.meetingID)}
            onClose={this.exitCloseTimeEntryModal}
            projectDetails={this.state.project}
            meetingDetails={this.state.projectMeeting}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getImpactList: () => dispatch(getImpactList()),
  getTeamMembers: (id: number) => dispatch(getTeamMembers(id)),
  getMeetingTemplateSetting: () => dispatch(getMeetingTemplateSetting()),
  getProjectStatusByProjectID: (projectId: number) =>
    dispatch(getProjectStatusByProjectID(projectId)),
  getProjectTickets: (projectID: number) =>
    dispatch(getProjectTickets(projectID)),
  getProjectDetails: (projectID: number) =>
    dispatch(getProjectDetails(projectID)),
  getProjectTypes: () => dispatch(getProjectTypes()),
  getMeetingTemplates: (url: string) => dispatch(getMeetingTemplates(url)),
  saveMeetingDetails: (projectID: number, data: any, api: string) =>
    dispatch(saveMeetingDetails(projectID, data, api)),
  meetingComplete: (projectID: number, method: HTTPMethods, api: string) =>
    dispatch(meetingComplete(projectID, method, api)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getMeetingNotesByProjectID: (projectId: number, meetingId: number) =>
    dispatch(getMeetingNotesByProjectID(projectId, meetingId)),
  saveMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    note: string
  ) => dispatch(saveMeetingNotesByProjectID(projectId, meetingId, note)),
  updateMeetingNotesByProjectID: (
    projectId: number,
    meetingId: number,
    noteId: number,
    note: string
  ) =>
    dispatch(updateMeetingNotesByProjectID(projectId, meetingId, noteId, note)),
  getProjectCustomerContactsByID: (projectID: number) =>
    dispatch(getProjectCustomerContactsByID(projectID)),
  saveMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) => dispatch(saveMeetingAttendeesByProjectID(projectId, meetingId, data)),
  getMeetingAttendeesByProjectID: (projectId: number, meetingId: number) =>
    dispatch(getMeetingAttendeesByProjectID(projectId, meetingId)),
  getMeetingPDFPreview: (
    projectId: number,
    meetingId: number,
    completed: boolean,
    sent_to_cc: boolean
  ) =>
    dispatch(getMeetingPDFPreview(projectId, meetingId, completed, sent_to_cc)),
  deleteMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) =>
    dispatch(
      deleteMeetingAttendeesByProjectID(projectId, meetingId, attendeeId)
    ),
  getProjectEngineersByID: (projectID: number, type?: string) =>
    dispatch(getProjectEngineersByID(projectID, type)),
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  getProjectAdditionalContactsByID: (projectId: number) =>
    dispatch(getProjectAdditionalContactsByID(projectId)),
  saveExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    data: any
  ) =>
    dispatch(
      saveExternalMeetingAttendeesByProjectID(projectId, meetingId, data)
    ),
  getExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number
  ) => dispatch(getExternalMeetingAttendeesByProjectID(projectId, meetingId)),
  deleteExternalMeetingAttendeesByProjectID: (
    projectId: number,
    meetingId: number,
    attendeeId: number
  ) =>
    dispatch(
      deleteExternalMeetingAttendeesByProjectID(
        projectId,
        meetingId,
        attendeeId
      )
    ),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProjectMeeting);

export const hasProjectMeetingIDs = (
  project: IProjectDetails,
  meeting: IProjectMeeting
): boolean => {
  return Boolean(project && project.id && meeting && meeting.id);
};

export const getURLBYMeetingType = (type: string) => {
  let url = "";
  switch (type) {
    case "Status":
      url = "status-meetings";
      break;
    case "Customer Touch":
      url = "customertouch-meetings";
      break;
    case "Internal Kickoff":
      url = "kickoff-meetings";
      break;
    case "Kickoff":
      url = "kickoff-meetings";
      break;
    case "Close Out":
      url = "closeout-meetings";
      break;
    case "Closeout":
      url = "closeout-meetings";
      break;
    default:
      break;
  }
  return url;
};
