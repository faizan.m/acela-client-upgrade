import { cloneDeep } from "lodash";
import _cloneDeep from "lodash/cloneDeep";
import React, { Component } from "react";
import { DraggableArea } from "react-draggable-tags";
import ReactQuill from "react-quill";
import SquareButton from "../../../components/Button/button";

import Input from "../../../components/Input/input";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import SMPopUp from "../../../components/SMPopup/smPopUp";
import { commonFunctions } from "../../../utils/commonFunctions";
import "./style.scss";

interface IIAgendaTopicState {
  agenda_topics: any[];
  editableTopicIdx: number;
  agendaTopic: string;
  notes: string;
  editable: boolean;
  updateKey: number;
}
interface IIProjectSettingProps {
  setAgenda: (agenda_topics: any) => void;
  saveClicked: boolean;
  initialData?: { topic: string; notes: string; id?: number }[];
  viewOnly?: boolean;
}

export default class AgendaTopic extends Component<
  IIProjectSettingProps,
  IIAgendaTopicState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  creatableEl: any;

  constructor(props: any) {
    super(props);
    this.state = this.getEmptyState();
  }

  getEmptyState = () => ({
    agenda_topics: [{ topic: "", notes: "", id: 1 }],
    editableTopicIdx: -1,
    agendaTopic: "",
    notes: "",
    editable: false,
    updateKey: 0,
  });

  componentDidMount() {
    if (this.props.initialData) {
      const newState = _cloneDeep(this.state);
      (newState.agenda_topics as any) = this.props.initialData;
      this.setState(newState);
    }
  }

  handleChange = (content: string) => {
    this.setState({
      agendaTopic: content,
    });
  };

  onEditSaveClick = () => {
    const newState = _cloneDeep(this.state);
    newState.agenda_topics[this.state.editableTopicIdx][
      "topic"
    ] = this.state.agendaTopic;
    newState.agenda_topics[this.state.editableTopicIdx]["id"] =
      this.state.editableTopicIdx + 1;
    (newState.editableTopicIdx as number) = -1;
    (newState.agendaTopic as string) = "";
    (newState.editable as boolean) = false;
    this.props.setAgenda(newState.agenda_topics);
    this.setState(newState);
  };

  deleteAgendaTopic = (index) => {
    const newState = cloneDeep(this.state);
    newState.agenda_topics.splice(index, 1);
    this.props.setAgenda(newState.agenda_topics);
    this.setState(newState);
  };
  handleNoteChange = (e, topicIndex) => {
    const newState = cloneDeep(this.state);
    newState[e.target.name] = e.target.value;
    this.setState(newState);
  };
  handleDraggableChange = (tags) => {
    const newState = cloneDeep(this.state);
    (newState.agenda_topics as any) = tags;
    this.setState(newState, () => {
      this.props.setAgenda(newState.agenda_topics);
    });
  };
  render() {
    const agenda_topics: any[] = this.state.agenda_topics;
    return (
      <div className="agenda-topic-component">
        {!this.state.editable ? (
          <DraggableArea
            isList
            key={this.state.updateKey}
            tags={agenda_topics}
            render={({ tag, index }) => {
              return (
                <div
                  className="col-md-12 agenda-draggable"
                  key={index}
                >
                  <div className="agenda-topic-content-container col-xs-8">
                    <div
                      className="agenda-topic-content ql-editor"
                      dangerouslySetInnerHTML={{
                        __html: tag.topic,
                      }}
                      onClick={() =>
                        this.setState({
                          editableTopicIdx: index,
                          agendaTopic: tag.topic,
                          editable: true,
                        })
                      }
                    />
                    {this.props.saveClicked &&
                      commonFunctions.isEditorEmpty(tag.topic) && (
                        <div className="agenda-error">
                          This field is required
                        </div>
                      )}
                  </div>
                  <SMPopUp
                    key={index}
                    className="item-notes"
                    onClickOk={() => {
                      const newState = cloneDeep(this.state);
                      newState.agenda_topics[index]["notes"] = this.state.notes;
                      this.props.setAgenda(newState.agenda_topics);
                      (newState.updateKey as number) = 0;
                      this.setState(newState);
                    }}
                    isValidForm={true}
                    clickableSection={
                      <img
                        width="40px"
                        height="40px"
                        className={"d-pointer icon-remove edit-png"}
                        alt=""
                        src={`/assets/icons/comment-bubble${
                          tag.notes ? "-highlight" : ""
                        }.svg`}
                        onClick={(e) =>
                          this.setState({ notes: tag.notes, updateKey: 1 })
                        }
                        title={tag.notes}
                      />
                    }
                  >
                    <div className="box">
                      <Input
                        field={{
                          label: "Note",
                          type: "TEXTAREA",
                          value: this.state.notes,
                          isRequired: false,
                        }}
                        width={12}
                        multi={false}
                        name="notes"
                        onChange={(e) => {
                          e.stopPropagation();
                          this.handleNoteChange(e, index);
                        }}
                        placeholder={`Enter a note`}
                      />
                    </div>
                  </SMPopUp>
                  {!this.props.viewOnly && (
                    <SmallConfirmationBox
                      className="remove-action-item"
                      onClickOk={() => this.deleteAgendaTopic(index)}
                      text={"Agenda topic"}
                    />
                  )}
                </div>
              );
            }}
            onChange={(tags) => this.handleDraggableChange(tags)}
          />
        ) : (
          <>
            {this.state.agenda_topics.map((topic, topicIndex) => {
              return (
                <div className="col-md-12 status-row" key={topicIndex}>
                  {this.props.viewOnly ||
                  topicIndex !== this.state.editableTopicIdx ? (
                    <div className="agenda-topic-content-container col-xs-9">
                      <div
                        className="agenda-topic-content ql-editor"
                        dangerouslySetInnerHTML={{
                          __html: topic.topic,
                        }}
                        onClick={() =>
                          this.setState({
                            editableTopicIdx: topicIndex,
                            agendaTopic: topic.topic,
                            editable: true,
                          })
                        }
                      />
                      {this.props.saveClicked &&
                        commonFunctions.isEditorEmpty(topic.topic) && (
                          <div className="agenda-error">
                            This field is required
                          </div>
                        )}
                    </div>
                  ) : (
                    <div className="edit-agenda-topic col-xs-9">
                      <Input
                        field={{
                          label: "",
                          type: "CUSTOM",
                          value: "",
                          isRequired: false,
                        }}
                        width={12}
                        onChange={(e) => null}
                        name=""
                        customInput={
                          <ReactQuill
                            value={this.state.agendaTopic}
                            onChange={(htmlString: string) =>
                              this.handleChange(htmlString)
                            }
                            modules={{
                              toolbar: [
                                [{ list: "ordered" }, { list: "bullet" }],
                              ],
                            }}
                            theme="snow"
                            className={`agenda-topics-quill`}
                          />
                        }
                      />
                      <div className="agenda-topic-action-btn">
                        <SquareButton
                          content={"Save"}
                          bsStyle={"primary"}
                          onClick={() => this.onEditSaveClick()}
                          className=""
                          disabled={commonFunctions.isEditorEmpty(
                            this.state.agendaTopic
                          )}
                        />
                        <SquareButton
                          content={"Cancel"}
                          bsStyle={"default"}
                          onClick={(e) =>
                            this.setState({
                              editableTopicIdx: -1,
                              agendaTopic: "",
                              editable: false,
                            })
                          }
                          className=""
                        />
                      </div>
                    </div>
                  )}

                  <SMPopUp
                    key={topicIndex}
                    className="item-notes"
                    onClickOk={() => {
                      const newState = cloneDeep(this.state);
                      newState.agenda_topics[topicIndex][
                        "notes"
                      ] = this.state.notes;
                      this.setState(newState);
                      this.props.setAgenda(newState.agenda_topics);
                    }}
                    isValidForm={true}
                    clickableSection={
                      <img
                        width="40px"
                        height="40px"
                        className={"d-pointer icon-remove edit-png"}
                        alt=""
                        src={`/assets/icons/comment-bubble${
                          topic.notes ? "-highlight" : ""
                        }.svg`}
                        onClick={(e) => this.setState({ notes: topic.notes })}
                        title={topic.notes}
                      />
                    }
                  >
                    <div className="box">
                      <Input
                        field={{
                          label: "Note",
                          type: "TEXTAREA",
                          value: this.state.notes,
                          isRequired: false,
                        }}
                        width={12}
                        multi={false}
                        name="notes"
                        onChange={(e) => {
                          e.stopPropagation();
                          this.handleNoteChange(e, topicIndex);
                        }}
                        placeholder={`Enter a note`}
                      />
                    </div>
                  </SMPopUp>
                  {!this.props.viewOnly && (
                    <SmallConfirmationBox
                      className="remove-action-item"
                      onClickOk={() => this.deleteAgendaTopic(topicIndex)}
                      text={"Agenda topic"}
                    />
                  )}
                </div>
              );
            })}
          </>
        )}

        {this.state.agenda_topics.length === 0 && (
          <div className="col-md-12 status-row risk-edited">
            <div className="no-data-action-item col-md-9">
              No items available
            </div>
          </div>
        )}
        {!this.props.viewOnly && (
          <div className="col-md-12 status-row risk-edited">
            <div
              onClick={(e) => {
                const newState = cloneDeep(this.state);
                newState.agenda_topics.push({
                  topic: "",
                  notes: "",
                  id: this.state.agenda_topics.length + 1,
                });
                (newState.editableTopicIdx as number) = this.state.agenda_topics.length;
                (newState.agendaTopic as string) = "";
                (newState.editable as boolean) = true;
                this.setState(newState);
              }}
              className="add-new-action-item col-md-9"
            >
              Add Agenda Items
            </div>
          </div>
        )}
      </div>
    );
  }
}
