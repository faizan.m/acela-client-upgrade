import { cloneDeep, debounce, isEmpty } from "lodash";
import moment from "moment";
import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import store from "../../../store";
import {
  fetchCustomersShort,
  FETCH_CUSTOMERS_SHORT_SUCCESS,
} from "../../../actions/customer";
import {
  checkSettings,
  fetchProjects,
  GetAdditionalSetting,
  getProjectData,
  getProjectEngineers,
  getProjectManagers,
  getProjectTypes,
  GET_DATA_SUCCESS,
  PROJECT_ENGINEERS_SUCCESS,
  PROJECT_MANAGER_SUCCESS,
  PROJECT_TYPES_SUCCESS,
  SAVE_PROJECT_DETAILS_FILTER,
  LAST_VISITED_PMO_SITE,
} from "../../../actions/pmo";
import "../../../commonStyles/projectManagement.scss";
import Input from "../../../components/Input/input";
import PMOCollapsible from "../../../components/PMOCollapsible";
import PMOTooltip from "../../../components/PMOTooltip/pmoTooltip";
import Spinner from "../../../components/Spinner";
import {
  fromISOStringToFormattedDate,
  fromISOStringToFormattedDateCW,
} from "../../../utils/CalendarUtil";
import {
  getBackgroundColor,
  getBackgroundOverdue,
  getConvertedColorWithOpacity,
} from "../../../utils/CommonUtils";
import { rawDoughnutChart } from "../../SOW/OrderTracking/Dashboard/DoughnutChart";
import Filters from "./filters";
import "./style.scss";

interface IProjectProps extends ICommonProps {
  user: ISuperUser;
  projectDetailsFilters: any;
  additionalSetting: IAdditionSettingPMO;
  checkSettings: () => Promise<any>;
  getProjectTypes: () => Promise<any>;
  getProjectEngineers: () => Promise<any>;
  getProjectManagers: () => Promise<any>;
  fetchCustomersShort: () => Promise<any>;
  GetAdditionalSetting: () => Promise<any>;
  getProjectData: (url: string, data: any) => Promise<any>;
  fetchProjects: (params?: IScrollPaginationFilters) => Promise<any>;
}

interface IProjectState {
  currentPage: {
    pageType: PageType;
  };
  projects: any[];
  pagination: IScrollPaginationFilters;
  inputValue: string;
  noData: boolean;
  showAllProject?: boolean;
  groupBy: string;
  showGroupBy: boolean;
  loading: boolean;
  showFiltersPop: boolean;
  unmappedSOW: boolean;
  unmappedOpportunity: boolean;
  activeProjectTypeFilter: any;
  activeCustomerFilter: any;
  activeProjectManagerFilter: any;
  activeProjectEngineerFilter: any;
  projectTypeOptions?: any;
  customerOptions?: any;
  projectManagerOptions?: any;
  projectEngineersOptions?: any;
  fetchingCustomers: boolean;
  fetchingProjectTypes: boolean;
  fetchingProjectManagers: boolean;
  fetchingProjectEngineers: boolean;
  showAllClosedProjects: boolean;
  selectedClosedDateOption: any;
  selectedActionDateOption: any;
  selectedActionDate: any;
  selectedClosedDate: any;
  totalProjectsCount: number;
  chart: any[];
  showChart: boolean;
}

enum PageType {
  Projects,
  AllProjects,
}
class MyProjects extends React.Component<IProjectProps, IProjectState> {
  unlisten: any;
  constructor(props: IProjectProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 1000);
  }

  priorityColorMap = [
    "#4ba2c1",
    "#e55b7a",
    "#fac64d",
    "#5b9950",
    "#ba89f2",
    "#cdd5e1",
  ];

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Projects,
    },
    projects: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: "customer__name",
      search: "",
      type: "",
      customer: "",
      manager: "",
      closed_end_date: "",
      closed_start_date: "",
      action_end_date: "",
      action_start_date: "",
      unmapped_sow: false,
      unmapped_opp: false,
    },
    inputValue: "",
    noData: false,
    showGroupBy: false,
    showAllProject: false,
    unmappedSOW: false,
    unmappedOpportunity: false,
    loading: true,
    groupBy: "",
    showFiltersPop: false,
    activeProjectTypeFilter: [],
    activeCustomerFilter: [],
    activeProjectManagerFilter: [],
    activeProjectEngineerFilter: [],
    fetchingCustomers: false,
    fetchingProjectTypes: false,
    fetchingProjectManagers: false,
    fetchingProjectEngineers: false,
    projectTypeOptions: [],
    customerOptions: [],
    projectManagerOptions: [],
    projectEngineersOptions: [],
    showAllClosedProjects: false,
    selectedClosedDateOption: undefined,
    selectedActionDateOption: undefined,
    selectedActionDate: undefined,
    selectedClosedDate: undefined,
    totalProjectsCount: 0,
    chart: [],
    showChart: false,
  });

  componentDidMount() {
    const query = new URLSearchParams(this.props.location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    this.setState({ showAllProject: AllProject });
    if (this.props.projectDetailsFilters) {
      this.setState((prevState) => ({
        ...prevState,
        ...this.props.projectDetailsFilters,
      }));
    }
    if (this.state.projects && this.state.projects.length === 0) {
      this.getProjectTypes();
      this.getAllCustomers();
      this.getProjectManagers();
      this.getProjectEngineers();
      this.props.GetAdditionalSetting().then((action) => {
        this.fetchMoreData(true);
        this.getProjectDataOverdue(AllProject);
      });
    }
    this.unlisten = this.props.history.listen(() => {
      const projectDetailsFilter = {
        pagination: this.state.pagination,
        inputValue: this.state.inputValue,
        showGroupBy: this.state.showGroupBy,
        unmappedSOW: this.state.unmappedSOW,
        unmappedOpportunity: this.state.unmappedOpportunity,
        groupBy: this.state.groupBy,
        activeProjectTypeFilter: this.state.activeProjectTypeFilter,
        activeCustomerFilter: this.state.activeCustomerFilter,
        activeProjectManagerFilter: this.state.activeProjectManagerFilter,
        activeProjectEngineerFilter: this.state.activeProjectEngineerFilter,
        showAllClosedProjects: this.state.showAllClosedProjects,
        selectedClosedDateOption: this.state.selectedClosedDateOption,
        selectedActionDateOption: this.state.selectedActionDateOption,
        selectedActionDate: this.state.selectedActionDate,
        selectedClosedDate: this.state.selectedClosedDate,
      };
      const lastVisitedSite: string =
        this.props.location.pathname + this.props.location.search;
      store.dispatch({
        type: SAVE_PROJECT_DETAILS_FILTER,
        response: projectDetailsFilter,
      });
      store.dispatch({
        type: LAST_VISITED_PMO_SITE,
        response: lastVisitedSite,
      });
    });
  }

  componentWillUnmount(): void {
    this.unlisten();
  }

  getProjectTypes = () => {
    this.setState({ fetchingProjectTypes: true });
    this.props.getProjectTypes().then((action) => {
      if (action.type === PROJECT_TYPES_SUCCESS) {
        const projectTypeOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.title}`,
        }));
        this.setState({ projectTypeOptions });
      }
      this.setState({ fetchingProjectTypes: false });
    });
  };

  getAllCustomers = () => {
    this.setState({ fetchingCustomers: true });
    this.props.fetchCustomersShort().then((action) => {
      if (action.type === FETCH_CUSTOMERS_SHORT_SUCCESS) {
        const customerOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.name}`,
        }));
        this.setState({ customerOptions });
      }
      this.setState({ fetchingCustomers: false });
    });
  };

  getProjectDataOverdue = (AllProject) => {
    this.setState({ fetchingCustomers: true });
    const params = {
      project_engineer_or_manager: AllProject
        ? ""
        : `${this.props.user && this.props.user.id}`,
      project_manager: this.state.activeProjectManagerFilter.join(),
      project_engineer: this.state.activeProjectEngineerFilter.join(),
    };
    this.props
      .getProjectData("pmo/projects/project-dashboard-graph", params)
      .then((action) => {
        if (action.type === GET_DATA_SUCCESS) {
          const data = action.response;
          const chartData = [];
          data &&
            Object.keys(data).map((x, index) => {
              chartData.push({
                label: x,
                value: data[x],
                filter: false,
                ...this.getIconColor(x),
              });
            });
          this.setState({ chart: chartData });
        }
        this.setState({ fetchingCustomers: false });
      });
  };

  getProjectManagers = () => {
    this.setState({ fetchingProjectManagers: true });
    this.props.getProjectManagers().then((action) => {
      if (action.type === PROJECT_MANAGER_SUCCESS) {
        const projectManagerOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.first_name} ${data.last_name}`,
        }));
        this.setState({ projectManagerOptions });
      }
      this.setState({ fetchingProjectManagers: false });
    });
  };
  getProjectEngineers = () => {
    this.setState({ loading: true });
    this.props.getProjectEngineers().then((action) => {
      if (action.type === PROJECT_ENGINEERS_SUCCESS) {
        const projectEngineersOptions = action.response.map((data) => ({
          value: data.id,
          label: `${data.name}`,
        }));
        this.setState({
          projectEngineersOptions,
        });
      }
      this.setState({ loading: false });
    });
  };
  onSearchStringChange = (e) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
    this.updateMessage(search);
  };

  updateMessage = (search) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };
  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
        type: this.state.pagination.type,
        customer: this.state.pagination.customer,
        manager: this.state.pagination.manager,
        project_engineer: this.state.pagination.project_engineer,
        closed_start_date: this.state.pagination.closed_start_date,
        closed_end_date: this.state.pagination.closed_end_date,
        action_start_date: this.state.pagination.action_start_date,
        action_end_date: this.state.pagination.action_end_date,
        status: this.state.showAllClosedProjects ? "closed" : null,
        unmapped_opp: this.state.pagination.unmapped_opp,
        unmapped_sow: this.state.pagination.unmapped_sow,
        ...this.getOverDueFilter(),
      };
    }
    if (prevParams.nextPage !== null) {
      prevParams = {
        ...prevParams,
        customer: this.state.activeCustomerFilter.join(),
        type: this.state.activeProjectTypeFilter.join(),
        project_engineer_or_manager: "",
        project_manager: this.state.activeProjectManagerFilter.join(),
        project_engineer: this.state.activeProjectEngineerFilter.join(),
        status: this.state.showAllClosedProjects ? "closed" : null,
        closed_start_date: this.state.pagination.closed_start_date,
        closed_end_date: this.state.pagination.closed_end_date,
        action_start_date: this.state.pagination.action_start_date,
        action_end_date: this.state.pagination.action_end_date,
        unmapped_opp: this.state.pagination.unmapped_opp,
        unmapped_sow: this.state.pagination.unmapped_sow,
        ...this.getOverDueFilter(),
      };

      if (!this.state.showAllProject) {
        prevParams = {
          ...prevParams,
          project_engineer_or_manager: `${this.props.user.id}`,
        };
      }

      this.props.fetchProjects(prevParams).then((action) => {
        if (action.response) {
          let projects = [];
          if (clearData) {
            projects = [...action.response.results];
          } else {
            projects = [...this.state.projects, ...action.response.results];
          }
          const newPagination = {
            currentPage: action.response.links.page_number,
            nextPage: action.response.links.next_page_number,
            page_size: this.state.pagination.page_size,
            ordering: this.state.pagination.ordering,
            search: this.state.pagination.search,
            type: this.state.pagination.type,
            customer: this.state.pagination.customer,
            manager: this.state.pagination.manager,
            project_engineer: this.state.pagination.project_engineer,
            closed_start_date: this.state.pagination.closed_start_date,
            closed_end_date: this.state.pagination.closed_end_date,
            action_start_date: this.state.pagination.action_start_date,
            action_end_date: this.state.pagination.action_end_date,
            unmapped_opp: this.state.pagination.unmapped_opp,
            unmapped_sow: this.state.pagination.unmapped_sow,
            status: this.state.showAllClosedProjects ? "closed" : null,
          };

          this.setState({
            totalProjectsCount: action.response.count,
            pagination: newPagination,
            projects,
            noData: projects.length === 0 ? true : false,
            loading: false,
          });
          if (projects.length === 0) {
            this.props.checkSettings();
          }
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  fetchByOrder = (field) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true);
      }
    );
  };
  getOrderClass = (field) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  changePage = (show: boolean) => {
    const newstate = this.getEmptyState();
    newstate.showAllProject = show;
    newstate.projectTypeOptions = this.state.projectTypeOptions;
    newstate.customerOptions = this.state.customerOptions;
    newstate.projectManagerOptions = this.state.projectManagerOptions;
    newstate.projectEngineersOptions = this.state.projectEngineersOptions;
    newstate.activeProjectManagerFilter = [];
    this.props.history.push(`?AllProject=${show}`);

    this.setState(newstate, () => {
      this.fetchMoreData(true);
      this.getProjectDataOverdue(show);
    });
  };
  goToDetails = (project) => {
    this.props.history.push(
      `Projects/${project.id}?AllProject=${this.state.showAllProject}`
    );
  };
  getIconColor = (key) => {
    switch (key) {
      case "projects_closing_this_week":
        return { color: "#dba629", icon: "calendar-silhouette" };
      case "projects_with_overdue_action_items_or_overdue_critical_path_items":
        return { color: "#db1643", icon: "pulse" };
      case "overbudget_projects":
        return { color: "#db1643", icon: "coin" };
      case "total_projects":
        return { color: "#ccc", icon: "calendar-silhouette", hide: true };

      default:
        return { color: "#ccc", icon: "" };
    }
  };

  getOverDueFilter = () => {
    const filters = {};
    this.state.chart
      .filter((x) => !x.hide)
      .map((x) => {
        filters[x.label] = x.filter ? "True" : "False";
      });
    return filters;
  };
  renderTopBar = () => {
    const showAllProject = this.state.showAllProject;

    return (
      <div className="projects-action-tabs">
        <div
          className={`projects-action-tabs-link ${
            !showAllProject ? "--active" : ""
          }`}
          onClick={() => this.changePage(false)}
        >
          My Projects
        </div>
        <div
          className={`projects-action-tabs-link ${
            showAllProject ? "--active" : ""
          }`}
          onClick={() => this.changePage(true)}
        >
          All Projects
        </div>
        <Input
          field={{
            value: this.state.inputValue,
            label: "",
            type: "SEARCH",
          }}
          width={10}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search"
          className="custom-search custom-search-outside"
        />
        <div className="filters-overdue">
          {this.state.chart.map((x, i) => {
            return (
              <div
                className="btn-overdue"
                key={i}
                style={{ backgroundColor: x.color }}
              >
                {!x.hide ? (
                  <img
                    className="eye-open svg-white"
                    alt=""
                    title={x.label}
                    src={`/assets/icons/${x.icon}.svg`}
                  />
                ) : (
                  "Total Projects"
                )}
                : {x.value}
                {!x.hide && (
                  <img
                    className="eye-open"
                    alt=""
                    src={`/assets/icons/${
                      !x.filter ? "eye-close" : "eye-open"
                    }.svg`}
                    onClick={(e) => {
                      const newstate = cloneDeep(this.state);
                      const chart = newstate.chart;
                      chart.map((x) => (x.filter = false));
                      chart[i].filter = !x.filter;
                      const pagination = this.getEmptyState().pagination;
                      (newstate.pagination as any) = pagination;
                      this.setState(newstate, () => {
                        this.fetchMoreData(true);
                      });
                    }}
                  />
                )}
              </div>
            );
          })}
        </div>
      </div>
    );
  };
  groupBy = (xs, key) => {
    return xs.reduce((rv, x) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);

      return rv;
      // tslint:disable-next-line: align
    }, {});
  };

  applyFilters = (filters) => {
    let closed_start_date, closed_end_date;

    if (filters.closedDateOption) {
      if (
        filters.closedDateOption === "thisWeek" ||
        filters.closedDateOption === "thisMonth"
      ) {
        const filterType =
          filters.closedDateOption === "thisWeek" ? "isoWeek" : "month";
        closed_end_date = moment()
          .clone()
          .endOf(filterType);
        closed_start_date = closed_end_date.clone().startOf(filterType);
      }

      if (filters.closedDateOption === "before") {
        closed_end_date = filters.selectedClosedDate;
        closed_start_date = moment("1970-01-01", "YYYY-MM-DD");
      }

      if (filters.closedDateOption === "after") {
        closed_start_date = filters.selectedClosedDate;
      }
    }

    let action_start_date, action_end_date;

    if (filters.selectedActionDateOption) {
      if (filters.selectedActionDateOption === "before") {
        action_end_date = filters.selectedActionDate.format("YYYY-MM-DD");
      }

      if (filters.selectedActionDateOption === "after") {
        action_start_date = filters.selectedActionDate.format("YYYY-MM-DD");
      }

      if (filters.selectedActionDateOption === "olderThan") {
        action_end_date = moment()
          .subtract(filters.selectedActionDate, "d")
          .format("YYYY-MM-DD");
      }
    }

    this.setState(
      {
        unmappedSOW: filters.unmappedSOW,
        unmappedOpportunity: filters.unmappedOpportunity,
        selectedActionDate: filters.selectedActionDate,
        selectedClosedDate: filters.selectedClosedDate,
        selectedActionDateOption: filters.selectedActionDateOption,
        selectedClosedDateOption: filters.closedDateOption,
        showAllClosedProjects: filters.showAllClosedProjects,
        activeProjectTypeFilter: filters.projectType,
        activeCustomerFilter: filters.customer,
        activeProjectManagerFilter: filters.projectManager,
        activeProjectEngineerFilter: filters.projectEngineer,
        pagination: {
          ...this.state.pagination,
          type: filters.typeStr,
          customer: filters.customerStr,
          manager: filters.projectManagerStr,
          project_engineer: filters.projectEngineerStr,
          closed_start_date:
            closed_start_date && closed_start_date.format("YYYY-MM-DD"),
          closed_end_date:
            closed_end_date && closed_end_date.format("YYYY-MM-DD"),
          action_start_date,
          action_end_date,
          unmapped_sow: filters.unmappedSOW,
          unmapped_opp: filters.unmappedOpportunity,
        },
      },
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  removeFilter = (filterType: string) => {
    if (filterType === "showAllClosedProjects") {
      this.setState(
        {
          showAllClosedProjects: false,
          pagination: {
            ...this.state.pagination,
            customer: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "unmappedSOW") {
      this.setState(
        {
          unmappedSOW: false,
          pagination: {
            ...this.state.pagination,
            unmapped_sow: false,
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "unmappedOpportunity") {
      this.setState(
        {
          unmappedOpportunity: false,
          pagination: {
            ...this.state.pagination,
            unmapped_opp: false,
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "customer") {
      this.setState(
        {
          activeCustomerFilter: [],
          pagination: {
            ...this.state.pagination,
            customer: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "type") {
      this.setState(
        {
          activeProjectTypeFilter: [],
          pagination: {
            ...this.state.pagination,
            type: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "projectManager") {
      this.setState(
        {
          activeProjectManagerFilter: [],
          pagination: {
            ...this.state.pagination,
            manager: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }
    if (filterType === "projectEngineer") {
      this.setState(
        {
          activeProjectEngineerFilter: [],
          pagination: {
            ...this.state.pagination,
            project_engineer: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "closeDateOption") {
      this.setState(
        {
          selectedClosedDate: undefined,
          selectedClosedDateOption: undefined,
          pagination: {
            ...this.state.pagination,
            closed_end_date: "",
            closed_start_date: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }

    if (filterType === "actionDateOption") {
      this.setState(
        {
          selectedActionDate: undefined,
          selectedActionDateOption: undefined,
          pagination: {
            ...this.state.pagination,
            action_end_date: "",
            action_start_date: "",
          },
        },
        () => {
          this.fetchMoreData(true);
        }
      );
    }
  };

  clearFilters = () => {
    this.setState(
      {
        unmappedSOW: false,
        unmappedOpportunity: false,
        showAllClosedProjects: false,
        selectedClosedDate: undefined,
        selectedActionDate: undefined,
        selectedActionDateOption: undefined,
        selectedClosedDateOption: undefined,
        activeProjectTypeFilter: [],
        activeCustomerFilter: [],
        activeProjectManagerFilter: [],
        activeProjectEngineerFilter: [],
        showFiltersPop: false,
        pagination: {
          ...this.state.pagination,
          type: "",
          customer: "",
          manager: "",
          project_engineer: "",
          closed_end_date: "",
          closed_start_date: "",
          action_end_date: "",
          action_start_date: "",
          unmapped_sow: false,
          unmapped_opp: false,
        },
      },
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  projectListingRows = (project, index) => {
    return (
      <div
        onClick={() => this.goToDetails(project)}
        className={`row-panel  ${
          !project.is_time_status_critical ? "" : "row-highlight"
        } ${this.state.showAllProject ? "column-8" : "column-7"} `}
        key={index}
      >
        <div className="icons">
          <div
            style={{ backgroundColor: project.time_status_color }}
            className={`rectangle-icon-outer`}
          >
            <PMOTooltip
              iconClass="status-svg-images-black"
              icon={`/assets/icons/clock-circular-gross-outline-of-time.svg`}
            >
              <div
                style={{ borderColor: project.time_status_color }}
                className="data-box"
              >
                <div className="text">Time Remaining</div>
                <div
                  style={{ color: project.time_status_color, fontWeight: 600 }}
                  className="text"
                >
                  {project.time_status_value} %
                </div>
              </div>
            </PMOTooltip>
          </div>

          <div
            style={{ backgroundColor: project.date_status_color }}
            className={`rectangle-icon-outer ${index % 3 ? "" : "error"}`}
          >
            <PMOTooltip
              iconClass="status-svg-images-black"
              icon={`/assets/icons/calendar-silhouette.svg`}
            >
              <div
                style={{ borderColor: project.date_status_color }}
                className="data-box"
              >
                <div className="text">Close Date</div>
                <div
                  style={{ color: project.date_status_color, fontWeight: 600 }}
                  className="text"
                >
                  {" "}
                  {project.date_status_value > 0
                    ? `Due date in ${project.date_status_value} days`
                    : "Due date passed"}{" "}
                </div>
              </div>
            </PMOTooltip>
          </div>

          <div
            style={{ backgroundColor: getBackgroundOverdue(project) }}
            className={`rectangle-icon-outer ${index % 2 ? "" : "success"}`}
          >
            <PMOTooltip
              iconClass="status-svg-images-black"
              icon={`/assets/icons/pulse.svg`}
            >
              {(project.overdue_critical_path_items ||
                0 + project.overdue_action_items ||
                0) !== 0 && (
                <div
                  style={{
                    borderColor:
                      (project.overdue_critical_path_items ||
                        0 + project.overdue_action_items ||
                        0) > 0
                        ? "#db1643"
                        : "#176f07",
                  }}
                  className="data-box"
                >
                  <div className="text">Overdue Actions</div>
                  <div className="text">
                    <span
                      style={{
                        color:
                          project.overdue_critical_path_items || 0 > 0
                            ? "#db1643"
                            : "#176f07",
                        fontWeight: 600,
                      }}
                    >
                      {" "}
                      {project.overdue_critical_path_items || 0}{" "}
                    </span>
                    Critical Items |
                    <span
                      style={{
                        color:
                          project.overdue_action_items || 0 > 0
                            ? "#db1643"
                            : "#176f07",
                        fontWeight: 600,
                      }}
                    >
                      {" "}
                      {project.overdue_action_items || 0}{" "}
                    </span>{" "}
                    Action
                  </div>
                </div>
              )}
              {(project.expiring_critical_path_items ||
                0 + project.expiring_action_items ||
                0) !== 0 && (
                <div
                  style={{
                    borderColor:
                      (project.expiring_critical_path_items ||
                        0 + project.expiring_action_items ||
                        0) > 0
                        ? "#dba629"
                        : "#176f07",
                  }}
                  className="data-box"
                >
                  <div className="text">Pending Actions</div>
                  <div className="text">
                    <span
                      style={{
                        color:
                          project.expiring_critical_path_items || 0 > 0
                            ? "#dba629"
                            : "#176f07",
                        fontWeight: 600,
                      }}
                    >
                      {" "}
                      {project.expiring_critical_path_items || 0}{" "}
                    </span>
                    Critical Items |
                    <span
                      style={{
                        color:
                          project.expiring_action_items || 0 > 0
                            ? "#dba629"
                            : "#176f07",
                        fontWeight: 600,
                      }}
                    >
                      {" "}
                      {project.expiring_action_items || 0}{" "}
                    </span>{" "}
                    Action
                  </div>
                </div>
              )}
              {(project.expiring_critical_path_items ||
                0 + project.expiring_action_items ||
                0) === 0 &&
                (project.overdue_critical_path_items ||
                  0 + project.overdue_action_items ||
                  0) === 0 && (
                  <div
                    style={{
                      borderColor:
                        (project.expiring_critical_path_items ||
                          0 + project.expiring_action_items ||
                          0) > 0
                          ? "#dba629"
                          : "#176f07",
                    }}
                    className="data-box"
                  >
                    <div className="text">No Overdue & Pending Actions</div>
                  </div>
                )}
            </PMOTooltip>
          </div>
        </div>
        <div className="project-name">
          <div className="type ellipsis-no-width" title={project.customer}>
            {project.customer}
          </div>
        </div>
        <div className="project-name">
          <div
            className="name ellipsis-no-width"
            title={`${project.crm_id} - ${project.title}`}
          >
            {project.title}
          </div>
        </div>
        {this.state.showAllProject && (
          <div className="manager  user-image-component">
            <div
              style={{
                backgroundImage: `url(${
                  project.project_manager_profile_pic
                    ? `${project.project_manager_profile_pic}`
                    : "/assets/icons/user-filled-shape.svg"
                })`,
              }}
              className="pm-image-circle"
            />
            <div className="user-name" title={project.project_manager}>
              {project.project_manager}
            </div>
          </div>
        )}

        <div className="time">
          <div className="sub-heading">
            <div className="actual">{project.actual_hours}</div>
            <div />
            <div
              className="budget"
              title={
                project.hours_budget - project.actual_hours > 0
                  ? ""
                  : "Extra Hours"
              }
            >
              {(project.hours_budget - project.actual_hours).toFixed(2)}
            </div>
          </div>
        </div>
        <div className="">
          {fromISOStringToFormattedDateCW(
            project.estimated_end_date,
            "MMM DD, YYYY"
          )}
        </div>
        <div className="">
          {fromISOStringToFormattedDate(
            project.last_action_date,
            "MMM DD, YYYY"
          ) || "-"}
        </div>
        <div className="status color-preview" title={project.overall_status}>
          <div
            style={{
              backgroundColor: project.overall_status_color,
              borderColor: project.overall_status_color,
            }}
            className="left-column"
          />
          <div
            style={{
              background: `${getConvertedColorWithOpacity(
                project.overall_status_color
              )}`,
            }}
            className="ellipsis-no-width"
          >
            {project.overall_status}
          </div>
        </div>
      </div>
    );
  };

  renderFilters = () => {
    return (
      <div className="top-bar-active-filter">
        {this.state.showAllClosedProjects && (
          <div className="applied-filter">
            Show All Closed Projects:
            <span className="filters">{`True`}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("showAllClosedProjects")}
            />
          </div>
        )}
        {this.state.unmappedOpportunity && (
          <div className="applied-filter">
            Show Unmapped Opportunity Projects:
            <span className="filters">{`True`}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("unmappedOpportunity")}
            />
          </div>
        )}
        {this.state.unmappedSOW && (
          <div className="applied-filter">
            Show Unmapped SOW Projects:
            <span className="filters">{`True`}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("unmappedSOW")}
            />
          </div>
        )}
        {this.state.pagination.customer !== "" && (
          <div className="applied-filter">
            Customer:
            <span className="filters">{this.state.pagination.customer}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("customer")}
            />
          </div>
        )}
        {this.state.pagination.type !== "" && (
          <div className="applied-filter">
            Project Type:
            <span className="filters">{this.state.pagination.type}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("type")}
            />
          </div>
        )}
        {this.state.activeProjectManagerFilter.length > 0 && (
          <div className="applied-filter">
            Project Manager:
            <span className="filters">{this.state.pagination.manager}</span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("projectManager")}
            />
          </div>
        )}
        {this.state.activeProjectEngineerFilter.length > 0 && (
          <div className="applied-filter">
            Project Engineer:
            <span className="filters">
              {this.state.pagination.project_engineer}
            </span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("projectEngineer")}
            />
          </div>
        )}
        {this.state.selectedClosedDateOption && (
          <div className="applied-filter">
            Est. Closed Date:
            <span className="filters">
              {["before", "olderThan"].includes(
                this.state.selectedClosedDateOption
              ) && `Before - ${this.state.pagination.closed_end_date}`}
              {["after"].includes(this.state.selectedClosedDateOption) &&
                `After - ${this.state.pagination.closed_start_date}`}
              {("thisMonth" === this.state.selectedClosedDateOption ||
                "thisWeek" === this.state.selectedClosedDateOption) &&
                `${this.state.pagination.closed_start_date} - ${this.state.pagination.closed_end_date}`}
            </span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("closeDateOption")}
            />
          </div>
        )}
        {this.state.selectedActionDateOption && (
          <div className="applied-filter">
            Action Date:
            <span className="filters">
              {["before", "olderThan"].includes(
                this.state.selectedActionDateOption
              )
                ? `Before - ${this.state.pagination.action_end_date}`
                : ["after"].includes(this.state.selectedActionDateOption) &&
                  `After - ${this.state.pagination.action_start_date}`}
            </span>
            <img
              className="top-filter-delete"
              alt=""
              src={"/assets/icons/close.png"}
              onClick={() => this.removeFilter("actionDateOption")}
            />
          </div>
        )}
        <div className="clear-action" onClick={() => this.clearFilters()}>
          Clear All
        </div>
      </div>
    );
  };

  render() {
    const groupedByInvalid = this.groupBy(
      this.state.projects,
      this.state.groupBy
    );

    return (
      <div className="project-management project-list-panel">
        <div className="header-actions">
          <div className="left">{this.renderTopBar()}</div>
          <div className="right">
            <div className="project-count">{`Projects Count: ${this.state.totalProjectsCount}`}</div>
            <div
              className="action-btn filter"
              onClick={() =>
                this.setState({ showFiltersPop: !this.state.showFiltersPop })
              }
            >
              <img
                alt=""
                className="img-icon"
                src="/assets/icons/filter-filled-tool-symbol.svg"
              />
              Filter
            </div>
            {this.state.showFiltersPop && (
              <Filters
                show={this.state.showFiltersPop}
                unmappedSOW={this.state.unmappedSOW}
                unmappedOpportunity={this.state.unmappedOpportunity}
                selectedActionDateOption={this.state.selectedActionDateOption}
                selectedActionDate={this.state.selectedActionDate}
                selectedClosedDate={this.state.selectedClosedDate}
                selectedClosedDateOption={this.state.selectedClosedDateOption}
                showAllClosedProjects={this.state.showAllClosedProjects}
                projectTypeOptions={this.state.projectTypeOptions}
                customerOptions={this.state.customerOptions}
                projectManagerOptions={this.state.projectManagerOptions}
                projectEngineersOptions={this.state.projectEngineersOptions}
                setPopUpState={(show) =>
                  this.setState({ showFiltersPop: show })
                }
                activeProjectTypeFilter={this.state.activeProjectTypeFilter}
                activeCustomerFilter={this.state.activeCustomerFilter}
                activeProjectManagerFilter={
                  this.state.activeProjectManagerFilter
                }
                activeProjectEngineerFilter={
                  this.state.activeProjectEngineerFilter
                }
                applyFilters={this.applyFilters}
              />
            )}
            |
            {!this.state.showGroupBy && (
              <div
                className="action-btn-group"
                onClick={(e) =>
                  this.setState({ showGroupBy: true, groupBy: "" })
                }
              >
                <img
                  alt=""
                  className="img-icon"
                  src="/assets/icons/folder.svg"
                />
                Group By
              </div>
            )}
            {this.state.showGroupBy && (
              <Input
                field={{
                  label: "",
                  type: "PICKLIST",
                  value: this.state.groupBy,
                  isRequired: false,
                  options: [
                    { label: "Project Type", value: "type" },
                    { label: "Project Manager", value: "project_manager" },
                    { label: "Project Status", value: "overall_status" },
                    { label: "Company", value: "customer" },
                  ],
                }}
                width={10}
                labelIcon={"info"}
                name="status"
                onChange={(e) =>
                  this.setState({ groupBy: e.target.value }, () => {
                    this.fetchByOrder(e.target.value);
                  })
                }
                placeholder={`Group By`}
                className="group-by"
                clearable={true}
                onBlur={() => {
                  if (!this.state.groupBy) {
                    this.setState({ showGroupBy: false });
                  }
                }}
              />
            )}
          </div>
        </div>
        {this.state.showChart && (
          <div className="col-md-2 graph-box">
            {rawDoughnutChart(
              this,
              this.state.chart,
              "orderEstimatedShipDate",
              false,
              "Projects",
              this.priorityColorMap
            )}
          </div>
        )}

        {(this.state.activeProjectTypeFilter.length > 0 ||
          this.state.activeCustomerFilter.length > 0 ||
          this.state.activeProjectManagerFilter.length > 0 ||
          this.state.activeProjectEngineerFilter.length > 0 ||
          this.state.selectedClosedDateOption ||
          this.state.selectedActionDateOption ||
          this.state.showAllClosedProjects ||
          this.state.unmappedOpportunity ||
          this.state.unmappedSOW) && (
          <div className="applied-filters-row">
            <div>
              Filters:
              {this.renderFilters()}
            </div>
          </div>
        )}
        <div className="project-list">
          <div
            className={`header ${
              this.state.showAllProject ? "column-8" : "column-7"
            }`}
          >
            <div className={`icons`}>
              <div
                style={{ backgroundColor: "#ccc" }}
                className={`rectangle-icon-outer`}
              >
                <img
                  className="status-svg-images"
                  alt=""
                  src={`/assets/icons/clock-circular-gross-outline-of-time.svg`}
                  onClick={() => this.fetchByOrder("time_status_value")}
                />
              </div>

              <div
                style={{ backgroundColor: "#ccc" }}
                className={`rectangle-icon-outer`}
              >
                <img
                  className="status-svg-images"
                  alt=""
                  src={`/assets/icons/calendar-silhouette.svg`}
                  onClick={() => this.fetchByOrder("date_status_value_diff")}
                />
              </div>

              <div
                style={{ backgroundColor: "#ccc" }}
                className={`rectangle-icon-outer`}
              >
                <img
                  className="status-svg-images"
                  alt=""
                  src={`/assets/icons/pulse.svg`}
                  onClick={() => this.fetchByOrder("action_status_color")}
                />
              </div>
            </div>

            <div
              className={`project-name title ${this.getOrderClass(
                "customer__name"
              )}`}
              onClick={() => this.fetchByOrder("customer__name")}
            >
              {" "}
              Customer
            </div>
            <div
              className={`project-name title ${this.getOrderClass("title")}`}
              onClick={() => this.fetchByOrder("title")}
            >
              {" "}
              Project
            </div>
            {this.state.showAllProject && (
              <div className="title manager" title={"Project Manager"}>
                {" "}
                Project Manager
              </div>
            )}

            <div className="time">
              Time(hrs)
              <div className="sub-heading">
                <div className="actual">Actual</div>
                <div>|</div>
                <div className="budget">Remaining</div>
              </div>
            </div>
            <div
              className={`title ${this.getOrderClass("estimated_end_date")}`}
              onClick={() => this.fetchByOrder("estimated_end_date")}
            >
              {" "}
              Est. Close Date
            </div>
            <div
              className={`title  ${this.getOrderClass("last_action_date")}`}
              onClick={() => this.fetchByOrder("last_action_date")}
            >
              Last Action Date
            </div>
            <div
              className={`title ${this.getOrderClass("overall_status__title")}`}
              onClick={() => this.fetchByOrder("overall_status__title")}
            >
              Overall Status
            </div>
          </div>
          <div id="scrollableDiv" style={{ height: "72vh", overflow: "auto" }}>
            <InfiniteScroll
              dataLength={this.state.projects.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
            >
              {!this.state.groupBy &&
                this.state.projects.map((project, index) =>
                  this.projectListingRows(project, index)
                )}
              {this.state.groupBy &&
                groupedByInvalid &&
                !isEmpty(groupedByInvalid) &&
                Object.keys(groupedByInvalid).map((data, i) => (
                  <div key={i} className="voilation-statistics col-md-12">
                    <PMOCollapsible
                      background={getBackgroundColor(
                        groupedByInvalid[data][0][this.state.groupBy]
                      )}
                      label={
                        groupedByInvalid[data][0][this.state.groupBy] ||
                        "New Projects"
                      }
                      isOpen={true}
                    >
                      {groupedByInvalid[data].map((project, index) =>
                        this.projectListingRows(project, index)
                      )}
                    </PMOCollapsible>
                  </div>
                ))}
              {!this.state.loading &&
                this.state.pagination.nextPage &&
                this.state.projects.length > 0 && (
                  <div
                    className="load-more"
                    onClick={() => {
                      this.fetchMoreData(false);
                    }}
                  >
                    load more data...
                  </div>
                )}
            </InfiniteScroll>
            {this.state.noData && this.state.projects.length === 0 && (
              <div className="no-data">No Projects Available</div>
            )}
            {this.state.loading && (
              <div className="loader">
                <Spinner show={true} />
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  additionalSetting: state.pmo.additionalSetting,
  projectDetailsFilters: state.pmo.projectDetailsFilters,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchProjects: (params?: IScrollPaginationFilters) =>
    dispatch(fetchProjects(params)),
  GetAdditionalSetting: () => dispatch(GetAdditionalSetting()),
  getProjectTypes: () => dispatch(getProjectTypes()),
  fetchCustomersShort: () => dispatch(fetchCustomersShort()),
  getProjectManagers: () => dispatch(getProjectManagers()),
  checkSettings: () => dispatch(checkSettings()),
  getProjectData: (url: string, data: any) =>
    dispatch(getProjectData(url, data)),
  getProjectEngineers: () => dispatch(getProjectEngineers()),
});

export default connect(mapStateToProps, mapDispatchToProps)(MyProjects);
