import { cloneDeep } from 'lodash';
import React from 'react';
import { connect } from 'react-redux';
import SquareButton from '../../../components/Button/button';
import Checkbox from '../../../components/Checkbox/checkbox';
import Input from '../../../components/Input/input';
import SelectInput from '../../../components/Input/Select/select';

import './style.scss';

interface IProjectFiltersProps {
    show: boolean;
    setPopUpState: any;
    projectTypeOptions?: any;
    customerOptions?: any;
    unmappedSOW: boolean;
    unmappedOpportunity: boolean;
    projectManagerOptions?: any;
    projectEngineersOptions: any;
    activeProjectTypeFilter?: any;
    activeCustomerFilter?: any;
    activeProjectManagerFilter?: any;
    activeProjectEngineerFilter?: any;
    applyFilters: any;
    showAllClosedProjects: boolean;
    selectedClosedDateOption: any;
    selectedClosedDate: any;
    selectedActionDate: any;
    selectedActionDateOption: any;
}

interface IProjectFiltersState {
    list: any;
    unmappedSOW: boolean;
    unmappedOpportunity: boolean;
    showAllClosedProjects: boolean;
    activeFilterOption: string;
    fetchingCustomers: boolean;
    fetchingProjectTypes: boolean;
    fetchingProjectManagers: boolean;
    fetchingProjectEngineers: boolean;
    selectedProjectType: any;
    selectedProjectCustomer: any;
    selectedProjectManager: any;
    selectedProjectEngineer: any;
    selectedProjectTypArr: any;
    selectedProjectCustomerArr: any;
    selectedProjectManagerArr: any;
    selectedProjectEngineerArr: any;
    selectedClosedDateOption: any;
    selectedActionDateOption: any;
    selectedClosedDate: any;
    selectedActionDate: any;
}

class ProjectFilters extends React.Component<
    IProjectFiltersProps,
    IProjectFiltersState
> {
    constructor(props: IProjectFiltersProps) {
        super(props);
        this.state = this.getEmptyState();
    }

    getEmptyState = () => ({
        list: [],
        unmappedSOW: false,
        unmappedOpportunity: false,
        showAllClosedProjects: false,
        activeFilterOption: "projectType",
        fetchingCustomers: false,
        fetchingProjectTypes: false,
        fetchingProjectManagers: false,
        fetchingProjectEngineers: false,
        customers: [],
        projectTypes: [],
        projectManagers: [],
        selectedProjectType: undefined,
        selectedProjectCustomer: undefined,
        selectedProjectManager: undefined,
        selectedProjectEngineer: undefined,
        selectedProjectTypArr: [],
        selectedProjectCustomerArr: [],
        selectedProjectManagerArr: [],
        selectedProjectEngineerArr: [],
        selectedClosedDateOption: undefined,
        selectedActionDateOption: undefined,
        selectedClosedDate: undefined,
        selectedActionDate: undefined
    });

    componentDidMount() {
        this.setState({
            unmappedSOW: this.props.unmappedSOW,
            unmappedOpportunity: this.props.unmappedOpportunity,
            selectedActionDate: this.props.selectedActionDate,
            selectedActionDateOption: this.props.selectedActionDateOption,
            selectedClosedDate: this.props.selectedClosedDate,
            selectedClosedDateOption: this.props.selectedClosedDateOption,
            showAllClosedProjects: this.props.showAllClosedProjects,
            selectedProjectCustomerArr: [...this.props.activeCustomerFilter],
            selectedProjectTypArr: [...this.props.activeProjectTypeFilter],
            selectedProjectManagerArr: [...this.props.activeProjectManagerFilter],
            selectedProjectEngineerArr: [...this.props.activeProjectEngineerFilter]
        });
    }

    handleChange = (event: any) => {
        const newState: IProjectFiltersState = cloneDeep(this.state);        
        (newState[event.target.name] as string) = event.target.value;
        this.setState(newState);
    };

    onApplyClick = (e: any) => {
        e.preventDefault();
        this.props.setPopUpState(false);
        
        const typeStr = [];
        this.state.selectedProjectTypArr.forEach((value) => {
            this.props.projectTypeOptions.forEach((obj) => {
                if (obj.value === value) {
                    typeStr.push(obj.label)
                }
            });
        });

        const customerStr = [];
        this.state.selectedProjectCustomerArr.forEach((value) => {
            this.props.customerOptions.forEach((obj) => {
                if (obj.value === value) {
                    customerStr.push(obj.label)
                }
            });
        });
        
        const projectManagerStr = [];
        this.state.selectedProjectManagerArr.forEach((value) => {
            this.props.projectManagerOptions.forEach((obj) => {
                if (obj.value === value) {
                    projectManagerStr.push(obj.label)
                }
            });
        });

        const projectEngineerStr = [];
        this.state.selectedProjectEngineerArr.forEach((value) => {
            this.props.projectEngineersOptions.forEach((obj) => {
                if (obj.value === value) {
                    projectEngineerStr.push(obj.label)
                }
            });
        });

        this.props.applyFilters({
            selectedActionDate: this.state.selectedActionDate,
            selectedActionDateOption: this.state.selectedActionDateOption,
            selectedClosedDate: this.state.selectedClosedDate,
            closedDateOption: this.state.selectedClosedDateOption,
            showAllClosedProjects: this.state.showAllClosedProjects,
            projectType: this.state.selectedProjectTypArr,
            customer: this.state.selectedProjectCustomerArr,
            projectEngineer: this.state.selectedProjectEngineerArr,
            projectManager: this.state.selectedProjectManagerArr,
            typeStr: typeStr.join(),
            customerStr: customerStr.join(),
            projectManagerStr: projectManagerStr.join(),
            projectEngineerStr: projectEngineerStr.join(),
            unmappedSOW: this.state.unmappedSOW,
            unmappedOpportunity: this.state.unmappedOpportunity,
        });
    };

    onCancelClick = (e: any) => {
        e.preventDefault();
        this.props.setPopUpState(false);
    }

    handleChangeCheckBox = (e: any) => {
        const newState = cloneDeep(this.state);

        (newState[e.target.name] as any) = e.target.checked;
        this.setState(newState);
    };

    handleProjectTypeChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectType: e.target.value,
            selectedProjectTypArr: [ ...prevState.selectedProjectTypArr, e.target.value ],
        }));
    }

    handleProjectCustomerChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectCustomer: e.target.value,
            selectedProjectCustomerArr: [ ...prevState.selectedProjectCustomerArr, e.target.value ],
        }));
    }

    handleProjectManagerChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectManager: e.target.value,
            selectedProjectManagerArr: [ ...prevState.selectedProjectManagerArr, e.target.value ],
        }));
    }

    handleProjectEngineerChange = (e: any, type: string) => {
        this.setState(prevState => ({
            selectedProjectEngineer: e.target.value,
            selectedProjectEngineerArr: [ ...prevState.selectedProjectEngineerArr, e.target.value ],
        }));
    }
    removeFilter = (filter: any, type: string) => {
        if (type === "projectType") {
            this.setState({
                selectedProjectTypArr: this.state.selectedProjectTypArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "customer") {
            this.setState({
                selectedProjectCustomerArr: this.state.selectedProjectCustomerArr.filter((val) => { 
                    return val !== filter
            })});
        }

        if (type === "projectManager") {
            this.setState({
                selectedProjectCustomerArr: this.state.selectedProjectCustomerArr.filter((val) => { 
                    return val !== filter
            })});
        }
    }

    getProjectTypeVal = (projectType) => {
        const type = this.props.projectTypeOptions.find(obj => obj.value == projectType)
        if (type) {
            return type.label
        }
        return ""
    };

    getCustomerVal = (customer) => {
        const type = this.props.customerOptions.find(obj => obj.value == customer)
        if (type) {
            return type.label
        }
        return ""
    }

    getProjectManagerVal = (manager) => {
        const type = this.props.projectManagerOptions.find(obj => obj.value == manager)
        if (type) {
            return type.label
        }
        return ""
    }

    getProjectEngineerVal = (manager) => {
        const type = this.props.projectEngineersOptions.find(obj => obj.value == manager)
        if (type) {
            return type.label
        }
        return ""
    }

    render() {

        return (
            <div className="project-filters-container">
                <div className="header">
                    <div className="filter-heading">Filter By</div>
                    <Checkbox
                        isChecked={this.state.showAllClosedProjects}
                        name="showAllClosedProjects"
                        onChange={e => this.handleChangeCheckBox(e)}
                    >
                        Show all Closed Projects
                    </Checkbox>
                    <Checkbox
                        isChecked={this.state.unmappedSOW}
                        name="unmappedSOW"
                        onChange={e => this.handleChangeCheckBox(e)}
                    >
                        Show Projects not mapped to any SOW
                    </Checkbox>
                    <Checkbox
                        isChecked={this.state.unmappedOpportunity}
                        name="unmappedOpportunity"
                        onChange={e => this.handleChangeCheckBox(e)}
                    >
                        Show Projects not mapped to any Opportunity
                    </Checkbox>
                </div>
                <div className="body-container">
                    <div className="filter-left">
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "projectType" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'projectType'})}
                        >
                            <span className="filter-title">Project Type</span>
                                {
                                    this.state.selectedProjectTypArr.length > 0 &&
                                        <div className="selection-count">
                                            {`${this.state.selectedProjectTypArr.length} Selected`}
                                        </div>
                                }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "customer" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'customer'})}
                        >
                            <span className="filter-title">Customer</span>
                            {
                                this.state.selectedProjectCustomerArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedProjectCustomerArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "projectManager" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'projectManager'})}
                        >
                            <span className="filter-title">Project Manager</span>
                            {
                                this.state.selectedProjectManagerArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedProjectManagerArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "projectEngineer" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'projectEngineer'})}
                        >
                            <span className="filter-title">Project Engineer</span>
                            {
                                this.state.selectedProjectEngineerArr.length > 0 && 
                                   <div className="selection-count">
                                        {`${this.state.selectedProjectEngineerArr.length} Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "closedDate" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'closedDate'})}
                        >
                            <span className="filter-title">Closed Date</span>
                            {
                                this.state.selectedClosedDateOption && 
                                   <div className="selection-count">
                                        {`Selected`}
                                    </div>
                            }
                        </div>
                        <div
                            className={`filter-option ${this.state.activeFilterOption === "actionDate" && "active-filter"}`}
                            onClick={() => this.setState({ activeFilterOption: 'actionDate'})}
                        >
                            <span className="filter-title">Action Date</span>
                            {
                                this.state.selectedActionDateOption && 
                                   <div className="selection-count">
                                        {`Selected`}
                                    </div>
                            }
                        </div>
                    </div>
                    <div className="filter-right">
                        {
                            this.state.activeFilterOption === "projectManager" ? 
                            this.renderProjectManagerFilter()
                            :   this.state.activeFilterOption === "projectEngineer" ? 
                                this.renderProjectEngineerFilter()
                            :
                            this.state.activeFilterOption === "customer" ? 
                                this.renderProjectCustomerFilter()
                            :
                            this.state.activeFilterOption === "closedDate" ? 
                                this.renderClosedDateFilter()
                            :
                            this.state.activeFilterOption === "actionDate" ? 
                                this.renderActionDateFilter()
                            :
                                this.renderProjectTypeFilter()
                        }
                    </div>
                </div>
                <div className="action-footer">
                    <SquareButton
                        content="Cancel"
                        bsStyle={"default"}
                        className="contact-cancel"
                        onClick={(e) => this.onCancelClick(e)}
                    />
                    <SquareButton
                        content="Apply"
                        bsStyle={"primary"}
                        className="contact-save"
                        onClick={(e) => this.onApplyClick(e)}
                    />
                </div>
            </div>
        );
    }

    renderProjectTypeFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectType"
                    value={this.state.selectedProjectType}
                    onChange={e => this.handleProjectTypeChange(e, "projectType")}
                    options={this.props.projectTypeOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Project Type"
                    disabled={false}
                    loading={this.state.fetchingProjectTypes}
                />
                {
                    this.state.selectedProjectTypArr.map((projectType) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getProjectTypeVal(projectType)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(projectType, "projectType")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderProjectCustomerFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectCustomer"
                    value={this.state.selectedProjectCustomer}
                    onChange={e => this.handleProjectCustomerChange(e, "customer")}
                    options={this.props.customerOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Customer"
                    disabled={false}
                    loading={this.state.fetchingCustomers}
                />
                {
                    this.state.selectedProjectCustomerArr.map((customer) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getCustomerVal(customer)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(customer, "customer")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderProjectManagerFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectType"
                    value={this.state.selectedProjectManager}
                    onChange={e => this.handleProjectManagerChange(e, "projectManager")}
                    options={this.props.projectManagerOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Project Manager"
                    disabled={false}
                    loading={this.state.fetchingProjectManagers}
                />
                {
                    this.state.selectedProjectManagerArr.map((projectManager) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getProjectManagerVal(projectManager)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(projectManager, "projectManager")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }


    renderProjectEngineerFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="projectType"
                    value={this.state.selectedProjectEngineer}
                    onChange={e => this.handleProjectEngineerChange(e, "projectEngineer")}
                    options={this.props.projectEngineersOptions}
                    multi={false}
                    searchable={true}
                    placeholder="Choose Project Engineer"
                    disabled={false}
                    loading={this.state.fetchingProjectEngineers}
                />
                {
                    this.state.selectedProjectEngineerArr.map((projectEngineer) =>
                        <div className="selected-filter-section">
                            <div className="selected-filter-option">{this.getProjectEngineerVal(projectEngineer)}</div>
                            <img
                                className="selected-delete"
                                alt=""
                                src={'/assets/icons/close.png'}
                                onClick={() => this.removeFilter(projectEngineer, "projectEngineer")}
                            />
                        </div>
                    )
                }
            </div>
        )
    }

    renderClosedDateFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="selectedClosedDateOption"
                    value={this.state.selectedClosedDateOption}
                    onChange={e => this.handleChange(e)}
                    options={[
                        {
                            value: "thisWeek",
                            label: `This Week`,
                            disabled: false,
                        },
                        {
                            value: "thisMonth",
                            label: `This Month`,
                            disabled: false,
                        },
                        {
                            value: "after",
                            label: "After",
                            disabled: false,
                        },
                        {
                            value: "before",
                            label: "Before",
                            disabled: false,
                        }
                    ]}
                    multi={false}
                    searchable={false}
                    placeholder="Choose Date Period"
                    disabled={false}
                />
                {
                    (this.state.selectedClosedDateOption === "after" ||
                    this.state.selectedClosedDateOption === "before") &&

                    <Input
                        field={{
                            value: this.state.selectedClosedDate,
                            label: '',
                            type: "DATE",
                            isRequired: false,
                        }}
                        width={15}
                        name="selectedClosedDate"
                        onChange={e => this.handleChange(e)}
                        placeholder="Choose Date"
                        showTime={false}
                        showDate={true}
                        disablePrevioueDates={false}
                    />
                }
            </div>
        )
    }

    renderActionDateFilter = () => {
        return (
            <div className="filter-selection">
                <SelectInput
                    name="selectedActionDateOption"
                    value={this.state.selectedActionDateOption}
                    onChange={e => this.handleChange(e)}
                    options={[
                        {
                            value: "after",
                            label: "After",
                            disabled: false,
                        },
                        {
                            value: "before",
                            label: "Before",
                            disabled: false,
                        },
                        {
                            value: "olderThan",
                            label: "Older Than (Days)",
                            disabled: false,
                        }
                    ]}
                    multi={false}
                    searchable={false}
                    placeholder="Choose Date Period"
                    disabled={false}
                />
                {
                    this.state.selectedActionDateOption === "olderThan" ?
                    <Input
                        field={{
                            value: this.state.selectedActionDate,
                            label: '',
                            type: "NUMBER",
                            isRequired: false,
                        }}
                        width={15}
                        minimumValue={"0"}
                        name="selectedActionDate"
                        onChange={e => this.handleChange(e)}
                        placeholder="Enter Days"
                    />
                    :
                    <Input
                        field={{
                            value: this.state.selectedActionDate,
                            label: '',
                            type: "DATE",
                            isRequired: false,
                        }}
                        width={15}
                        name="selectedActionDate"
                        onChange={e => this.handleChange(e)}
                        placeholder="Choose Date"
                        showTime={false}
                        showDate={true}
                        disablePrevioueDates={false}
                    />
                }
            </div>
        )
    }
}

const mapStateToProps = (state: IReduxStore) => ({
});

const mapDispatchToProps = (dispatch: any) => ({
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ProjectFilters);
