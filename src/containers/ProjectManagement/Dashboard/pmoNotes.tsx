import { debounce, DebouncedFunc } from "lodash";
import React, { Component } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  editPMONote,
  createPMONote,
  fetchPMODashboardNotes,
  EDIT_PMO_NOTES_SUCCESS,
  EDIT_PMO_NOTES_FAILURE,
  FETCH_PMO_NOTES_SUCCESS,
  FETCH_PMO_NOTES_FAILURE,
} from "../../../actions/pmo";
import SquareButton from "../../../components/Button/button";
import EditButton from "../../../components/Button/editButton";
import Checkbox from "../../../components/Checkbox/checkbox";
import Input from "../../../components/Input/input";
import ModalBase from "../../../components/ModalBase/modalBase";
import { QuillEditorAcela } from "../../../components/QuillEditor/QuillEditor";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import Spinner from "../../../components/Spinner";
import UsershortInfo from "../../../components/UserImage";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import { commonFunctions } from "../../../utils/commonFunctions";

interface IPMONotesProps {
  user: ISuperUser;
  archived: boolean;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  fetchNotes: (data: IPMONotesPagination, archived: boolean) => Promise<any>;
  createNote: (note: string) => Promise<any>;
  editNote: (
    noteId: number,
    method: "put" | "delete",
    note?: IPMONote
  ) => Promise<any>;
}

interface IPMONotesState {
  note: string;
  noData: boolean;
  pmoNotes: IPMONote[];
  totalNotes: number;
  loading: boolean;
  nextPage: number;
  notesByMe: boolean;
  showModal: boolean;
  currentNoteId: number;
  isPostingNote: boolean;
  inputValue: string;
  pagination: IPMONotesPagination;
}

class PMONotes extends Component<IPMONotesProps, IPMONotesState> {
  private updateMessageDebounce: DebouncedFunc<(search: string) => void>;

  constructor(props: IPMONotesProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessageDebounce = debounce(this.updateMessage, 1000);
  }

  getEmptyState = () => ({
    note: "",
    nextPage: 1,
    pmoNotes: [],
    loading: false,
    noData: false,
    showModal: false,
    notesByMe: false,
    currentNoteId: 0,
    isPostingNote: false,
    totalNotes: 0,
    inputValue: "",
    pagination: {
      page: 1,
      page_size: 10,
      search: "",
      author_id: "",
    },
  });

  componentDidMount() {
    this.fetchMoreData(true);
  }

  saveNote = () => {
    this.setState({ isPostingNote: true }, () => {
      if (this.state.currentNoteId === 0)
        this.props
          .createNote(this.state.note)
          .then((action) => {
            if (action.type === FETCH_PMO_NOTES_SUCCESS) {
              this.fetchMoreData(true);
              this.closeModal();
              this.props.addSuccessMessage("Note created successfully!");
            } else if (action.type === FETCH_PMO_NOTES_FAILURE) {
              this.props.addErrorMessage("Error creating Note");
            }
          })
          .finally(() => this.setState({ isPostingNote: false }));
      else {
        this.props
          .editNote(this.state.currentNoteId, "put", {
            note: this.state.note,
          } as IPMONote)
          .then((action) => {
            if (action.type === EDIT_PMO_NOTES_SUCCESS) {
              this.fetchMoreData(true);
              this.closeModal();
              this.props.addSuccessMessage("Note updated successfully!");
            } else if (action.type === EDIT_PMO_NOTES_FAILURE) {
              this.props.addErrorMessage("Error updating Note");
            }
          })
          .finally(() => this.setState({ isPostingNote: false }));
      }
    });
  };

  closeModal = () => {
    this.setState({
      note: "",
      currentNoteId: 0,
      showModal: false,
    });
  };

  onCheckboxChange = (e) => {
    this.setState(
      {
        notesByMe: e.target.checked,
      },
      () => this.fetchMoreData(true)
    );
  };

  handleChangeNote = (html: string) => {
    this.setState({
      note: html,
    });
  };

  onSearchStringChange = (e) => {
    const search = e.target.value;
    this.setState({ inputValue: search }, () =>
      this.updateMessageDebounce(search)
    );
  };

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };

  onDeleteRowClick = (noteId: number) => {
    this.setState({ loading: true }, () => {
      this.props
        .editNote(noteId, "delete")
        .then((action) => {
          if (action.type === EDIT_PMO_NOTES_SUCCESS) {
            this.fetchMoreData(true);
            this.props.addSuccessMessage("Note deleted successfully!");
          } else if (action.type === EDIT_PMO_NOTES_FAILURE) {
            this.props.addErrorMessage("Error deleting note!");
          }
        })
        .finally(() => this.setState({ loading: false }));
    });
  };

  archiveNote = (noteId: number) => {
    this.setState({ loading: true }, () => {
      this.props
        .editNote(noteId, "put", { is_archived: true } as IPMONote)
        .then((action) => {
          if (action.type === EDIT_PMO_NOTES_SUCCESS) {
            this.fetchMoreData(true);
            this.props.addSuccessMessage("Note archived successfully!");
          } else if (action.type === EDIT_PMO_NOTES_FAILURE) {
            this.props.addErrorMessage("Error archiving note!");
          }
        })
        .finally(() => this.setState({ loading: false }));
    });
  };

  fetchMoreData = (clearData: boolean = false) => {
    this.setState(
      {
        loading: true,
      },
      () => {
        if (clearData || this.state.nextPage) {
          let newParams: IPMONotesPagination = this.state.pagination;
          if (clearData) {
            newParams = {
              ...newParams,
              ...this.getEmptyState().pagination,
              search: this.state.pagination.search,
              author_id: this.state.notesByMe ? this.props.user.id : "",
            };
          } else if (this.state.nextPage) {
            newParams = {
              ...newParams,
              page: this.state.nextPage,
            };
          }
          this.props
            .fetchNotes(newParams, this.props.archived)
            .then((action) => {
              if (action.type === FETCH_PMO_NOTES_SUCCESS) {
                let pmoNotes = [];
                if (clearData) {
                  pmoNotes = [...action.response.results];
                } else {
                  pmoNotes = [
                    ...this.state.pmoNotes,
                    ...action.response.results,
                  ];
                }

                this.setState({
                  totalNotes: action.response.count,
                  pagination: newParams,
                  pmoNotes,
                  noData: pmoNotes.length === 0 ? true : false,
                  nextPage: action.response.links.next_page_number,
                  loading: false,
                });
              } else {
                this.setState({
                  noData: true,
                  loading: false,
                });
              }
            });
        }
      }
    );
  };

  renderNoteInputModal = () => {
    return (
      <ModalBase
        show={this.state.showModal}
        onClose={this.closeModal}
        titleElement={
          this.state.currentNoteId === 0 ? "Create New Note" : "Update Note"
        }
        bodyElement={
          <div className="pmo-note-input">
            <Spinner
              show={this.state.isPostingNote}
              className="pmo-note-modal-loader"
            />
            <QuillEditorAcela
              onChange={this.handleChangeNote}
              label={"Note"}
              value={this.state.note}
              scrollingContainer=".modal"
              wrapperClass={"pmo-notes-quill"}
              isRequired={true}
              customToolbar={[
                ["bold", "italic", "underline", "strike"],
                [{ color: [] }, { background: [] }],
                [{ list: "ordered" }, { list: "bullet" }],
              ]}
              hideFullscreen={true}
              hideTable={true}
            />
          </div>
        }
        footerElement={
          <div>
            <SquareButton
              content={`Cancel`}
              bsStyle={"default"}
              onClick={this.closeModal}
              className="pmo-modal-btn"
              disabled={this.state.isPostingNote}
            />
            <SquareButton
              content={this.state.currentNoteId === 0 ? `Save` : `Update`}
              bsStyle={"primary"}
              onClick={() => this.saveNote()}
              className="pmo-modal-btn"
              disabled={
                this.state.isPostingNote ||
                commonFunctions.isEditorEmpty(this.state.note)
              }
            />
          </div>
        }
        className={"pmo-notes-modal"}
      />
    );
  };

  renderTopBar = () => {
    return (
      <div className={"dashboard-pmo__table-top"}>
        <div style={{ display: "flex" }}>
          <Input
            field={{
              label: "",
              type: "SEARCH",
              value: this.state.inputValue,
              isRequired: false,
            }}
            width={12}
            placeholder="Search"
            name="searchString"
            onChange={this.onSearchStringChange}
            className="dashboard-pmo__search"
          />
          <div className="total-count">
            <span>Total Notes : </span>
            {this.state.totalNotes}
          </div>

          <Checkbox
            isChecked={this.state.notesByMe}
            name="notesByMe"
            className="pmo-notes-checkbox"
            onChange={(e) => this.onCheckboxChange(e)}
          >
            Show only my notes
          </Checkbox>
        </div>
        {!this.props.archived && (
          <div style={{ display: "flex" }}>
            <SquareButton
              content={
                <>
                  <span className="add-plus">+</span>
                  <span className="add-text">Add Note</span>
                </>
              }
              className="add-new-pmo-note add-btn"
              bsStyle={"link"}
              onClick={() =>
                this.setState({ showModal: true, currentNoteId: 0, note: "" })
              }
            />
          </div>
        )}
      </div>
    );
  };

  renderNotesList = () => {
    return (
      <div className="project-list">
        <Spinner show={this.state.loading} className="pmo-review-spinner" />
        {this.renderTopBar()}
        <div
          id="scrollableDiv"
          style={{ height: "auto", maxHeight: "75vh", overflow: "auto" }}
        >
          <InfiniteScroll
            dataLength={this.state.pmoNotes.length}
            next={this.fetchMoreData}
            hasMore={this.state.nextPage ? true : false}
            loader={<h4 className="no-data">Loading...</h4>}
            scrollableTarget="scrollableDiv"
          >
            {this.state.pmoNotes &&
              this.state.pmoNotes.map((note: IPMONote, index: number) => {
                return (
                  <div className="notes-row" key={index}>
                    <div className="top-header">
                      <div className="user">
                        <UsershortInfo
                          name={
                            (note.author_first_name || "") +
                            " " +
                            (note.author_last_name || "")
                          }
                          className="pmo-user-image"
                          url={note.author_pic ? note.author_pic : ""}
                        />
                      </div>
                      {!this.props.archived &&
                        this.props.user &&
                        this.props.user.id === note.author_id && (
                          <div className="actions-notes">
                            <EditButton
                              title="Update Note"
                              onClick={() =>
                                this.setState({
                                  showModal: true,
                                  note: note.note,
                                  currentNoteId: note.id,
                                })
                              }
                            />
                            <SmallConfirmationBox
                              title={"Archive Note"}
                              className="pmo-notes-archive-button"
                              showButton={true}
                              onClickOk={() => this.archiveNote(note.id)}
                              fullText={
                                "Are you sure you want to archive this Note?"
                              }
                              confirmText={"Archive"}
                              wrapperClass={"pmo-notes-confirm-box"}
                            />
                            <SmallConfirmationBox
                              title={"Delete Note"}
                              className="remove"
                              showButton={true}
                              onClickOk={() => this.onDeleteRowClick(note.id)}
                              text={"Note"}
                            />
                          </div>
                        )}
                    </div>
                    <div className="date">
                      {note.updated_on
                        ? fromISOStringToFormattedDate(
                            note.updated_on,
                            "MM/DD/YYYY hh:mm A"
                          )
                        : "N.A."}
                    </div>
                    <div
                      className="note ql-editor"
                      dangerouslySetInnerHTML={{
                        __html: note.note,
                      }}
                    />
                  </div>
                );
              })}
            {!this.state.loading &&
              this.state.nextPage &&
              this.state.pmoNotes.length > 0 && (
                <div
                  className="load-more"
                  onClick={() => {
                    this.fetchMoreData(false);
                  }}
                >
                  Load more notes...
                </div>
              )}
          </InfiniteScroll>
          {this.state.noData && this.state.pmoNotes.length === 0 && (
            <div className="no-data">No Notes Available</div>
          )}
        </div>
      </div>
    );
  };

  render() {
    return (
      <>
        {this.renderNoteInputModal()}
        <div className="pmo-notes-container">{this.renderNotesList()}</div>
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch) => ({
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  fetchNotes: (data: IPMONotesPagination, archived: boolean) =>
    dispatch(fetchPMODashboardNotes(data, archived)),
  createNote: (note: string) => dispatch(createPMONote(note)),
  editNote: (
    noteId: number,
    method: "put" | "delete",
    note: IPMONote
  ) => dispatch(editPMONote(noteId, method, note)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PMONotes);
