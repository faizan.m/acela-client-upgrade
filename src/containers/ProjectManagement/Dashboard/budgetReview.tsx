import React from "react";
import InfiniteListing from "../../../components/InfiniteList/infiniteList";

interface BudgetReviewProps {
  filters: IPMODashboardFilterParams;
}

const BudgetReview: React.FC<BudgetReviewProps> = (props) => {
  return (
    <div className="budget-review-container">
      <InfiniteListing
        showSearch={true}
        id="budget-review-list"
        className="budget-review-infinite-list"
        url={`providers/pmo/projects/metrics/budget-review`}
        filters={props.filters}
        checkBoxfilters={[
          {
            key: "sow_type",
            value: false,
            name: "Only Fixed Fee Projects",
            trueValue: "Fixed Fee",
            falseValue: "",
          },
        ]}
        columns={[
          {
            name: "Company Name",
            id: "customer_name",
            ordering: "customer_name"
          },
          {
            name: "Project Name",
            id: "title",
            ordering: "title",
          },
          {
            name: "Budget Hours",
            className: "width-10",
            id: "hours_budget",
            ordering: "hours_budget",
          },
          {
            name: "Actual Hours",
            className: "width-10",
            id: "actual_hours",
            ordering: "actual_hours",
          },
          {
            name: "Remaining Hours",
            className: "width-10",
            id: "remaining_hours",
            ordering: "remaining_hours",
          },
          {
            name: "% of Budget",
            className: "width-10",
            id: "percent_of_budget",
            ordering: "percent_of_budget",
            Cell: (project_row) =>
              project_row.percent_of_budget
                ? project_row.percent_of_budget.toFixed(2) + "%"
                : project_row.percent_of_budget ,
          },
          {
            name: "Project Status",
            className: "project-status",
            id: "overall_status_title",
            ordering: "status",
            Cell: (project_row) => (
              <div
                style={{
                  background: project_row.overall_status_color + "33", // for adding opacity
                  borderLeft: `5px solid ${project_row.overall_status_color}`,
                  fontWeight: 600,
                  padding: "0 6px",
                  width: "100%",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center"
                }}
              >
                {project_row.overall_status_title}
              </div>
            ),
          },
          {
            name: "Project Manager",
            id: "project_manager_name",
            ordering: "project_manager_name",
          },
        ]}
      />
    </div>
  );
};

export default BudgetReview;
