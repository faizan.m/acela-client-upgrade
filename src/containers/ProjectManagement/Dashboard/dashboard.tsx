import { cloneDeep, debounce, DebouncedFunc, isNil } from "lodash";
import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import store from "../../../store";
import {
  checkSettings,
  fetchPMODashboardReview,
  getProjectsByType,
  getProjectsByStatus,
  getProjectsByClosingDate,
  getProjectsByProjectManager,
  LAST_VISITED_PMO_SITE,
  SAVE_PMO_DASHBOARD_FILTERS,
  FETCH_PROJECT_TYPE_GRAPH_SUCCESS,
  FETCH_PROJECT_MANAGER_GRAPH_SUCCESS,
  FETCH_PROJECT_STATUS_GRAPH_SUCCESS,
  FETCH_PROJECTS_CLOSING_GRAPH_SUCCESS,
} from "../../../actions/pmo";
import Input from "../../../components/Input/input";
import { commonFunctions } from "../../../utils/commonFunctions";
import {
  getConvertedColorWithOpacity,
  getProgressbarColor,
} from "../../../utils/CommonUtils";
import BudgetReview from "./budgetReview";
import { rawDoughnutChart } from "./DoughnutChart";
import "../../../commonStyles/doughnut_chart.scss";
import Spinner from "../../../components/Spinner";
import moment from "moment";
import PmoNotes from "./pmoNotes";
import Checkbox from "../../../components/Checkbox/checkbox";
import "./style.scss";

interface IDashboardProps extends ICommonProps {
  pmoDashboardFilters: {
    inputValue: string;
    projectsByPM: IDoughnutChartObject[];
    projectsByStatus: IDoughnutChartObject[];
    projectsByClosingDate: IDoughnutChartObject[];
    projectsByType: IDoughnutChartObject[];
    pagination: IServerPaginationParams & IPMODashboardFilterParams;
  };
  checkSettings: () => Promise<any>;
  getProjectsByType: (params: IPMODashboardFilterParams) => Promise<any>;
  getProjectsByStatus: (params: IPMODashboardFilterParams) => Promise<any>;
  fetchPMODashboardReview: (
    params: IServerPaginationParams & IPMODashboardFilterParams
  ) => Promise<any>;
  getProjectsByClosingDate: (params: IPMODashboardFilterParams) => Promise<any>;
  getProjectsByProjectManager: (
    params: IPMODashboardFilterParams
  ) => Promise<any>;
}

interface IDashboardState {
  currentPage: {
    pageType: PageType;
  };
  projects: any[];
  pagination: IServerPaginationParams & IPMODashboardFilterParams;
  nextPage: number;
  inputValue: string;
  noData: boolean;
  loading: boolean;
  projectsByPM: IDoughnutChartObject[];
  projectsByStatus: IDoughnutChartObject[];
  projectsByClosingDate: IDoughnutChartObject[];
  projectsByType: IDoughnutChartObject[];
  loadingProjectManagerGraph: boolean;
  loadingProjectStatusGraph: boolean;
  loadingProjectClosingGraph: boolean;
  loadingProjectTypeGraph: boolean;
  totalProjectsCount: number;
}
enum PageType {
  Notes,
  Review,
  BudgetReview,
  ArchivedNotes,
}
class Dashboard extends React.Component<IDashboardProps, IDashboardState> {
  unlisten: any;
  private updateMessageDebounce: DebouncedFunc<(search: string) => void>;

  constructor(props: IDashboardProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessageDebounce = debounce(this.updateMessage, 1000);
  }

  priorityColorMap = [
    "#4ba2c1",
    "#e55b7a",
    "#fac64d",
    "#5b9950",
    "#ba89f2",
    "#dadada",
    "#f5c8af",
    "#93b1c6",
    "#ff7148",
    "#9ac483",
  ];

  getEmptyState = () => ({
    currentPage: {
      pageType: PageType.Review,
    },
    projects: [],
    pagination: {
      page: 1,
      page_size: 25,
      ordering: "",
      search: "",
      sow_type: "",
    },
    inputValue: "",
    noData: false,
    loading: false,
    nextPage: 1,
    totalProjectsCount: 0,
    projectsByPM: [],
    projectsByStatus: [],
    projectsByClosingDate: [],
    projectsByType: [],
    loadingProjectManagerGraph: false,
    loadingProjectStatusGraph: false,
    loadingProjectClosingGraph: false,
    loadingProjectTypeGraph: false,
  });

  componentDidMount() {
    if (this.props.pmoDashboardFilters)
      this.setState(
        (prevState) => ({ ...prevState, ...this.props.pmoDashboardFilters }),
        () => this.fetchMoreData(true, false)
      );
    else this.fetchMoreData(true, true);
    this.unlisten = this.props.history.listen(() => {
      const projectDetailsFilter = {
        pagination: this.state.pagination,
        inputValue: this.state.inputValue,
        projectsByPM: this.state.projectsByPM,
        projectsByStatus: this.state.projectsByStatus,
        projectsByClosingDate: this.state.projectsByClosingDate,
        projectsByType: this.state.projectsByType,
      };
      const lastVisitedSite: string = this.props.location.pathname;
      store.dispatch({
        type: SAVE_PMO_DASHBOARD_FILTERS,
        response: projectDetailsFilter,
      });
      store.dispatch({
        type: LAST_VISITED_PMO_SITE,
        response: lastVisitedSite,
      });
    });
  }

  componentWillUnmount(): void {
    this.unlisten();
  }

  onSearchStringChange = (e) => {
    const search = e.target.value;
    this.setState({ inputValue: search }, () =>
      this.updateMessageDebounce(search)
    );
  };

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true, false);
  };

  getOnlyFilters = (): IPMODashboardFilterParams => {
    let filters: IServerPaginationParams & IPMODashboardFilterParams = {
      ...this.state.pagination,
    };
    delete filters.page;
    delete filters.page_size;
    delete filters.ordering;
    delete filters.search;
    return filters;
  };

  getFilterValueByName = (
    list: IDoughnutChartObject[],
    value: string
  ): boolean => {
    const filterValue = list.find((x) => x.label === value);
    return filterValue ? filterValue.filter : false;
  };

  getPreviousColor = (
    list: IDoughnutChartObject[],
    value: string,
    idx: number
  ): string => {
    const prevData = list.find((x) => x.label === value);
    return prevData
      ? prevData.color
      : this.priorityColorMap[idx % this.priorityColorMap.length];
  };

  fetchProjectsByProjectManager = () => {
    this.setState({ loadingProjectManagerGraph: true });
    const params: IPMODashboardFilterParams = this.getOnlyFilters();
    this.props.getProjectsByProjectManager(params).then((action) => {
      if (action.type === FETCH_PROJECT_MANAGER_GRAPH_SUCCESS) {
        const projectsByPM: IDoughnutChartObject[] = action.response.map(
          (data, idx: number) => ({
            value: data.count,
            label: `${data.project_manager_name}`,
            id: data.project_manager_id,
            color: this.getPreviousColor(
              this.state.projectsByPM,
              data.project_manager_name,
              idx
            ),
            filter: this.getFilterValueByName(
              this.state.projectsByPM,
              data.project_manager_name
            ),
          })
        );
        this.setState({ projectsByPM });
      }
      this.setState({ loadingProjectManagerGraph: false });
    });
  };

  fetchProjectsByStatus = () => {
    this.setState({ loadingProjectStatusGraph: true });
    const params: IPMODashboardFilterParams = this.getOnlyFilters();
    this.props.getProjectsByStatus(params).then((action) => {
      if (action.type === FETCH_PROJECT_STATUS_GRAPH_SUCCESS) {
        const projectsByStatus: IDoughnutChartObject[] = action.response.map(
          (data, idx: number) => ({
            value: data.count,
            label: `${data.project_status}`,
            id: data.project_status_id,
            color: data.project_status_color,
            filter: this.getFilterValueByName(
              this.state.projectsByStatus,
              data.project_status
            ),
          })
        );
        this.setState({ projectsByStatus });
      }
      this.setState({ loadingProjectStatusGraph: false });
    });
  };

  fetchProjectsByType = () => {
    this.setState({ loadingProjectTypeGraph: true });
    const params: IPMODashboardFilterParams = this.getOnlyFilters();
    this.props.getProjectsByType(params).then((action) => {
      if (action.type === FETCH_PROJECT_TYPE_GRAPH_SUCCESS) {
        const projectsByType: IDoughnutChartObject[] = action.response.map(
          (data, idx: number) => ({
            value: data.count,
            label: `${
              data.project_type !== null ? data.project_type : "No Type"
            }`,
            id: data.project_type_id,
            color: this.getPreviousColor(
              this.state.projectsByType,
              data.project_type !== null ? data.project_type : "No Type",
              idx
            ),
            filter: this.getFilterValueByName(
              this.state.projectsByType,
              data.project_type !== null ? data.project_type : "No Type"
            ),
          })
        );
        this.setState({ projectsByType });
      }
      this.setState({ loadingProjectTypeGraph: false });
    });
  };

  fetchProjectsByClosingDate = () => {
    this.setState({ loadingProjectClosingGraph: true });
    const params: IPMODashboardFilterParams = this.getOnlyFilters();
    this.props.getProjectsByClosingDate(params).then((action) => {
      if (action.type === FETCH_PROJECTS_CLOSING_GRAPH_SUCCESS) {
        const projectsByClosingDate: IDoughnutChartObject[] = action.response.map(
          (data, idx: number) => ({
            value: data.count,
            label: `${data.days_range}`,
            color: this.getPreviousColor(
              this.state.projectsByClosingDate,
              data.days_range,
              idx
            ),
            filter: this.getFilterValueByName(
              this.state.projectsByClosingDate,
              data.days_range
            ),
          })
        );
        this.setState({ projectsByClosingDate });
      }
      this.setState({ loadingProjectClosingGraph: false });
    });
  };

  fetchMoreData = (clearData: boolean = false, loadGraphs: boolean = false) => {
    const filterParams: IPMODashboardFilterParams = this.createFilterParams();
    this.setState(
      (prevState) => ({
        loading: true,
        pagination: {
          ...prevState.pagination,
          ...filterParams,
        },
      }),
      () => {
        if (clearData || this.state.nextPage) {
          let newParams: IServerPaginationParams &
            IPMODashboardFilterParams = this.state.pagination;
          if (clearData) {
            newParams = {
              ...newParams,
              ...this.getEmptyState().pagination,
              ordering: this.state.pagination.ordering,
              search: this.state.pagination.search,
              sow_type: this.state.pagination.sow_type,
            };
          } else if (this.state.nextPage) {
            newParams = {
              ...newParams,
              page: this.state.nextPage,
            };
          }
          this.props.fetchPMODashboardReview(newParams).then((action) => {
            if (action.response) {
              let projects = [];
              if (clearData) {
                projects = [...action.response.results];
              } else {
                projects = [...this.state.projects, ...action.response.results];
              }

              this.setState({
                totalProjectsCount: action.response.count,
                pagination: newParams,
                projects,
                noData: projects.length === 0 ? true : false,
                nextPage: action.response.links.next_page_number,
                loading: false,
              });
              if (projects.length === 0) {
                this.props.checkSettings();
              }
            } else {
              this.setState({
                noData: true,
                loading: false,
              });
            }
          });
        }

        if (loadGraphs) {
          this.fetchProjectsByType();
          this.fetchProjectsByStatus();
          this.fetchProjectsByClosingDate();
          this.fetchProjectsByProjectManager();
        }
      }
    );
  };

  createFilterParams = (): IPMODashboardFilterParams => {
    const projectManagerObject = this.state.projectsByPM.find(
      (el) => el.filter
    );
    const projectTypeObject = this.state.projectsByType.find((el) => el.filter);
    const projectStatusObject = this.state.projectsByStatus.find(
      (el) => el.filter
    );
    const projectClosingDateObject = this.state.projectsByClosingDate.find(
      (el) => el.filter
    );
    let estimated_end_date_before: string;
    let estimated_end_date_after: string;
    if (projectClosingDateObject) {
      switch (projectClosingDateObject.label) {
        case "Projects closed last week":
          estimated_end_date_after = moment()
            .subtract(1, "weeks")
            .startOf("isoWeek")
            .format("YYYY-MM-DD");
          estimated_end_date_before = moment()
            .subtract(1, "weeks")
            .endOf("isoWeek")
            .format("YYYY-MM-DD");
          break;
        case "Projects closing next week":
          estimated_end_date_after = moment()
            .add(1, "weeks")
            .startOf("isoWeek")
            .format("YYYY-MM-DD");
          estimated_end_date_before = moment()
            .add(1, "weeks")
            .endOf("isoWeek")
            .format("YYYY-MM-DD");
          break;
        case "Projects closing this week":
          estimated_end_date_after = moment()
            .startOf("isoWeek")
            .format("YYYY-MM-DD");
          estimated_end_date_before = moment()
            .endOf("isoWeek")
            .format("YYYY-MM-DD");
          break;
        case "Projects closed in last 30 days before last week":
          estimated_end_date_before = moment()
            .subtract(1, "weeks")
            .startOf("isoWeek")
            .subtract(1, "day")
            .format("YYYY-MM-DD");
          estimated_end_date_after = moment()
            .subtract(1, "weeks")
            .startOf("isoWeek")
            .subtract(30, "days")
            .format("YYYY-MM-DD");
          break;
        case "Projects closing in 30 days after next week":
          estimated_end_date_after = moment()
            .add(1, "weeks")
            .endOf("isoWeek")
            .add(1, "day")
            .format("YYYY-MM-DD");
          estimated_end_date_before = moment()
            .add(1, "weeks")
            .endOf("isoWeek")
            .add(30, "days")
            .format("YYYY-MM-DD");
          break;
      }
    }
    let filters: IPMODashboardFilterParams = {
      project_manager: projectManagerObject
        ? projectManagerObject.id
        : undefined,
      type: projectTypeObject ? String(projectTypeObject.id) : undefined,
      overall_status: projectStatusObject ? projectStatusObject.id : undefined,
      estimated_end_date_before,
      estimated_end_date_after,
    };
    return filters;
  };

  fetchByOrder = (field: string) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true, false);
      }
    );
  };

  getOrderClass = (field: string) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  filtersTiles = (
    list: IDoughnutChartObject[],
    name:
      | "projectsByPM"
      | "projectsByStatus"
      | "projectsByClosingDate"
      | "projectsByType"
  ) => {
    return (
      <>
        {list.map((x, ind) => {
          if (!x.filter) {
            return false;
          }
          return (
            <div className="filter-tile" title={String(x.label)}>
              {x.label}
              <img
                className={"d-pointer icon-remove"}
                alt=""
                src={"/assets/icons/cross-sign.svg"}
                onClick={(e) => {
                  let newState = cloneDeep(this.state);
                  newState[name][ind].filter = false;
                  this.setState(newState, () => {
                    this.fetchMoreData(true, true);
                  });
                }}
              />
            </div>
          );
        })}
      </>
    );
  };

  handleChangeFFCheckBox = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          sow_type: e.target.checked ? "Fixed Fee" : "",
        },
      }),
      () => this.fetchMoreData(true, false)
    );
  };

  appliedFilterLength = () => {
    return (
      this.state.projectsByPM.filter((x) => x.filter).length +
      this.state.projectsByClosingDate.filter((x) => x.filter).length +
      this.state.projectsByStatus.filter((x) => x.filter).length +
      this.state.projectsByType.filter((x) => x.filter).length
    );
  };

  clearFilters = () => {
    const newState = cloneDeep(this.state);
    newState.projectsByPM.forEach((x) => (x.filter = false));
    newState.projectsByType.forEach((x) => (x.filter = false));
    newState.projectsByClosingDate.forEach((x) => (x.filter = false));
    newState.projectsByStatus.forEach((x) => (x.filter = false));
    this.setState(newState, () => {
      this.fetchMoreData(true, true);
    });
  };

  changePage = (pageType: PageType) => {
    this.setState({
      currentPage: {
        pageType,
      },
    });
  };

  tabs = () => {
    const currentPage = this.state.currentPage;

    return (
      <div className="pmo-tabs__header">
        {
          <div
            className={`pmo-tabs__header-link ${
              currentPage.pageType === PageType.Review ? "--active" : ""
            }`}
            onClick={() => this.changePage(PageType.Review)}
          >
            Review
          </div>
        }
        <div
          className={`pmo-tabs__header-link ${
            currentPage.pageType === PageType.BudgetReview ? "--active" : ""
          }`}
          onClick={() => this.changePage(PageType.BudgetReview)}
        >
          Budget Review
        </div>
        <div
          className={`pmo-tabs__header-link ${
            currentPage.pageType === PageType.Notes ? "--active" : ""
          }`}
          onClick={() => this.changePage(PageType.Notes)}
        >
          Notes
        </div>
        <div
          className={`pmo-tabs__header-link ${
            currentPage.pageType === PageType.ArchivedNotes ? "--active" : ""
          }`}
          onClick={() => this.changePage(PageType.ArchivedNotes)}
        >
          Archived Notes
        </div>
      </div>
    );
  };

  filtersSection = () => {
    return (
      <div className="filters">
        <div className="filters-h3">
          <img alt="" className="filter-img" src="/assets/icons/filter.png" />
          <span>Applied Filters:</span>
        </div>
        <div className="tiles">
          {this.filtersTiles(this.state.projectsByPM, "projectsByPM")}
          {this.filtersTiles(this.state.projectsByType, "projectsByType")}
          {this.filtersTiles(this.state.projectsByStatus, "projectsByStatus")}
          {this.filtersTiles(
            this.state.projectsByClosingDate,
            "projectsByClosingDate"
          )}
          {this.appliedFilterLength() === 0 && (
            <div className="no-filter-applied">No Filter Applied</div>
          )}
          {this.appliedFilterLength() > 1 && (
            <div
              className="filter-tile clear-filter-applied"
              onClick={() => this.clearFilters()}
            >
              Clear all
            </div>
          )}
        </div>
      </div>
    );
  };

  renderTopBar = () => {
    return (
      <div className={"dashboard-pmo__table-top"}>
        <Input
          field={{
            label: "",
            type: "SEARCH",
            value: this.state.inputValue,
            isRequired: false,
          }}
          width={12}
          placeholder="Search"
          name="searchString"
          onChange={this.onSearchStringChange}
          className="dashboard-pmo__search"
        />
        <div className="total-count">
          <span>Total Projects : </span>
          {this.state.totalProjectsCount}
          <span> | </span>
        </div>
        {this.filtersSection()}
      </div>
    );
  };

  renderCharts = () => (
    <div className="charts-container">
      <div className="col-md-3 graph-box">
        {rawDoughnutChart(
          this,
          this.state.projectsByPM,
          "projectsByPM",
          this.state.loadingProjectManagerGraph,
          "Projects by Project Manager"
        )}
      </div>
      <div className="col-md-3 graph-box">
        {rawDoughnutChart(
          this,
          this.state.projectsByType,
          "projectsByType",
          this.state.loadingProjectTypeGraph,
          "Projects by Type"
        )}
      </div>
      <div className="col-md-3 graph-box">
        {rawDoughnutChart(
          this,
          this.state.projectsByStatus,
          "projectsByStatus",
          this.state.loadingProjectStatusGraph,
          "Projects by Status"
        )}
      </div>
      <div className="col-md-3 graph-box">
        {rawDoughnutChart(
          this,
          this.state.projectsByClosingDate,
          "projectsByClosingDate",
          this.state.loadingProjectClosingGraph,
          "Projects Closing"
        )}
      </div>
    </div>
  );
  renderReviewSection = () => (
    <div className="project-list">
      <Spinner show={this.state.loading} className="pmo-review-spinner" />
      {this.renderTopBar()}
      <Checkbox
        isChecked={this.state.pagination.sow_type === "Fixed Fee"}
        name="sow_type"
        onChange={(e) => this.handleChangeFFCheckBox(e)}
        className="review-ff-checkbox"
      >
        Only Fixed Fee Projects
      </Checkbox>
      <div
        id="scrollableDiv"
        style={{ height: "auto", maxHeight: "75vh", overflow: "auto" }}
      >
        <InfiniteScroll
          dataLength={this.state.projects.length}
          next={this.fetchMoreData}
          hasMore={this.state.nextPage ? true : false}
          loader={<h4 className="no-data">Loading...</h4>}
          scrollableTarget="scrollableDiv"
        >
          <div className="project-list-table col-md-12">
            <div className={`header-dashboard`}>
              <div
                className={`project-name ellipsis-text ${this.getOrderClass(
                  "title"
                )}`}
                onClick={() => this.fetchByOrder("title")}
              >
                Project Name
              </div>
              <div
                className={`pm ${this.getOrderClass("project_manager_name")}`}
                onClick={() => this.fetchByOrder("project_manager_name")}
              >
                PM
              </div>
              <div
                className={`status ${this.getOrderClass("status")}`}
                onClick={() => this.fetchByOrder("status")}
              >
                Status
              </div>
              <div
                className={`progress-dashboard ${this.getOrderClass(
                  "progress_percent"
                )}`}
                onClick={() => this.fetchByOrder("progress_percent")}
              >
                Progress
              </div>
              <div className="pr-up-html-h1">Updates</div>
              <div className="pr-up-html-h1">Note</div>
              <div
                className={`pr-up-etc-h1 ${this.getOrderClass(
                  "estimated_hours_to_complete"
                )}`}
                onClick={() => this.fetchByOrder("estimated_hours_to_complete")}
              >
                Estimated Hours to Complete
              </div>
            </div>
            {this.state.projects.map((project, index) => (
              <div className={`row-panel-dashboard`} key={index}>
                <div
                  className="project-details"
                  onClick={() => {
                    this.props.history.push(`Projects/${project.id}`);
                  }}
                >
                  <div
                    className="project-name"
                    style={{ borderColor: "#60c2e1" }}
                  >
                    <div
                      className="name-bold ellipsis-text"
                      title={`Project Name : ${project.title}`}
                    >
                      {project.title}
                    </div>
                    <div
                      className="name ellipsis-text"
                      title={`Customer : ${project.customer_name}`}
                    >
                      {project.customer_name}
                    </div>
                  </div>
                  <div className="manager user-image-component">
                    <div
                      style={{
                        backgroundImage: `url(${
                          project.project_manager_pic
                            ? `${project.project_manager_pic}`
                            : "/assets/icons/user-filled-shape.svg"
                        })`,
                      }}
                      title={project.project_manager_name}
                      className="pm-image-circle"
                    />
                  </div>
                </div>
                <div
                  className="status color-preview-dashboard"
                  title={project.overall_status_title}
                >
                  <div
                    style={{
                      backgroundColor: project.overall_status_color,
                      borderColor: project.overall_status_color,
                    }}
                    className="left-column"
                  />
                  <div
                    style={{
                      background: `${getConvertedColorWithOpacity(
                        project.overall_status_color
                      )}`,
                    }}
                    className="text"
                  >
                    {project.overall_status_title}
                  </div>
                </div>
                <div className="progressbar-dashboard">
                  <div className="percentage">{project.progress_percent}%</div>
                  <div className="progress-bar-custom-outer">
                    <div
                      className="progress-bar-custom"
                      style={{
                        width: `${
                          project.progress_percent > 100
                            ? 100
                            : project.progress_percent
                        }%`,
                        backgroundColor: getProgressbarColor(
                          project.progress_percent
                        ),
                      }}
                    />
                  </div>
                </div>
                <div className="pr-up-html-section">
                  {project.project_updates.updates &&
                  !commonFunctions.isEditorEmpty(
                    project.project_updates.updates
                  ) ? (
                    <div
                      className="pr-up-html ql-editor"
                      dangerouslySetInnerHTML={{
                        __html: project.project_updates.updates,
                      }}
                    />
                  ) : (
                    <div className="pr-up-no-data">No data</div>
                  )}
                  {project.project_updates.note &&
                  !commonFunctions.isEditorEmpty(
                    project.project_updates.note
                  ) ? (
                    <div
                      className="pr-up-html ql-editor"
                      dangerouslySetInnerHTML={{
                        __html: project.project_updates.note,
                      }}
                    />
                  ) : (
                    <div className="pr-up-no-data">No data</div>
                  )}
                </div>
                {!isNil(project.project_updates.estimated_hours_to_complete) ? (
                  <div className="pr-up-etc">
                    {project.project_updates.estimated_hours_to_complete}
                  </div>
                ) : (
                  <div
                    className="pr-up-no-data"
                    style={{
                      width: "170px",
                      minWidth: "170px",
                      marginRight: "0px",
                    }}
                  >
                    No data
                  </div>
                )}
              </div>
            ))}
          </div>
          {!this.state.loading &&
            this.state.nextPage &&
            this.state.projects.length > 0 && (
              <div
                className="load-more"
                onClick={() => {
                  this.fetchMoreData(false);
                }}
              >
                Load more projects...
              </div>
            )}
        </InfiniteScroll>
        {this.state.noData && this.state.projects.length === 0 && (
          <div className="no-data">No Projects Available</div>
        )}
      </div>
    </div>
  );

  render() {
    return (
      <div className="project-dashboard">
        {this.renderCharts()}
        {this.tabs()}
        {this.state.currentPage.pageType === PageType.Review &&
          this.renderReviewSection()}
        {this.state.currentPage.pageType === PageType.BudgetReview && (
          <div className="budget-review-wrapper">
            <div className="dashboard-pmo__table-top">
              <span id="pmo-total-filter-sep">|</span>
              {this.filtersSection()}
            </div>
            <BudgetReview filters={this.createFilterParams()} />
          </div>
        )}
        {this.state.currentPage.pageType === PageType.Notes && (
          <PmoNotes archived={false} />
        )}
        {this.state.currentPage.pageType === PageType.ArchivedNotes && (
          <PmoNotes archived={true} />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  pmoDashboardFilters: state.pmo.pmoDashboardFilters,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPMODashboardReview: (
    params: IServerPaginationParams & IPMODashboardFilterParams
  ) => dispatch(fetchPMODashboardReview(params)),
  getProjectsByType: (params: IPMODashboardFilterParams) =>
    dispatch(getProjectsByType(params)),
  getProjectsByStatus: (params: IPMODashboardFilterParams) =>
    dispatch(getProjectsByStatus(params)),
  getProjectsByClosingDate: (params: IPMODashboardFilterParams) =>
    dispatch(getProjectsByClosingDate(params)),
  getProjectsByProjectManager: (params: IPMODashboardFilterParams) =>
    dispatch(getProjectsByProjectManager(params)),
  checkSettings: () => dispatch(checkSettings()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
