import { cloneDeep, debounce } from 'lodash';
import moment from 'moment';
import 'moment-timezone';
import React from 'react';
import { connect } from 'react-redux';
import TimePicker from 'react-time-picker';
import { addErrorMessage, addSuccessMessage } from '../../../actions/appState';
import { deleteTimeEntry, fetchTimesheetProjectsList, getChargeCodes, getDraft, getProjectTickets, getTimeEntries, getTimesheetPeriod, GetWorkTypes, saveAsDraft, submitTimesheet, timeSheetSettings, UPDATE_TIMEENTRY_SUCCESS, updateTimeEntry } from '../../../actions/pmo';
import { fetchTimezones } from '../../../actions/provider';
import SquareButton from '../../../components/Button/button';
import Input from '../../../components/Input/input';
import SmallConfirmationBox from '../../../components/SmallConfirmationBox/confirmation';
import Spinner from '../../../components/Spinner';
import { dateSetTimeZero, toFormattedDate, toTimezone } from '../../../utils/CalendarUtil';
import { commonFunctions } from '../../../utils/commonFunctions';
import './style.scss';


interface IEngineerTimesheetProps extends ICommonProps {
  addErrorMessage: any;
  addSuccessMessage: any;
  getProjectTickets: any;
  GetWorkTypes: any;
  getTimesheetPeriod: any;
  timeSheetSettings: any;
  saveAsDraft: any;
  fetchTimesheetProjectsList: any;
  user: any,
  submitTimesheet: any;
  fetchTimezones: any;
  getDraft: any;
  getTimeEntries: any;
  getChargeCodes: any;
  updateTimeEntry: any;
  deleteTimeEntry: any;
}

interface IEngineerTimesheetState {
  loading: boolean;
  projectList: any[];
  ticketList: any[];
  WorkTypeList: any[];
  error: any;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
  timesheet: any[];
  timesheetPeriod: any[];
  dateRange: any;
  default_work_type_id: number,
  default_time_period_status: string;
  default_hours: number;
  isValid: boolean;
  timezones: any[];
  default_timezone: string;
  default_start_time: number;
  weekly_hours_threshold: number;
  daily_hours_threshold: number;
  draftID: any;
  chargeCodeList: any[];
  loadingRange: boolean;
  invalidIds: string[];
}

class EngineerTimesheets extends React.Component<
  IEngineerTimesheetProps, IEngineerTimesheetState
> {
  private debouncedFetch;

  constructor(props: IEngineerTimesheetProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedFetch = debounce(this.saveAsDraft, 4000);
  }

  getEmptyState = () => ({
    loading: false,
    projectList: [],
    ticketList: [],
    WorkTypeList: [],
    timesheetPeriod: [],
    error: null,
    isDatePickerSelected: false,
    isDateValueSelected: false,
    timesheet: [],
    dateRange: '',
    default_work_type_id: 1,
    default_time_period_status: "",
    default_hours: 0,
    isValid: true,
    timezones: [],
    default_timezone: '',
    default_start_time: 0,
    daily_hours_threshold: 0,
    weekly_hours_threshold: 0,
    draftID: 0,
    chargeCodeList: [],
    loadingRange: false,
    invalidIds: [],
  });

  componentDidMount() {
    this.getChargeCodes();
    this.getTimesheetSetting();
    this.fetchTimesheetProjectsList();
    this.getTimesheetPeriod();
    this.props.GetWorkTypes().then(
      action => {
        this.setState({ WorkTypeList: action.response })
      }
    )
    this.props.fetchTimezones().then(
      action => {
        this.setState({ timezones: action.response })
      }
    )
  }
  getDrafts = (id) => {
    this.setState({ loading: true });
    this.props.getDraft('', 'get', id).then(
      action => {
        if (action.type === 'DRAFT_TIMESHEET_SUCCESS') {
          const response = action.response.results && action.response.results[0];
          if (response) {
            const timesheet = response.draft.timesheet;
            const draftID = response.id || 0;
            const newState = cloneDeep(this.state);
            timesheet.map((t, index) => {
              t.subRows.map((s, subIndex) => {
                if (s && s.project_id !== "chargeCode") {
                  if (newState.timesheet[index].subRows[subIndex]) { newState.timesheet[index].subRows[subIndex].ticketLoading = true; }
                  this.getTicketsList(s.project_id, index, subIndex);
                } else {
                  if (newState.timesheet[index].subRows[subIndex]) {
                    newState.timesheet[index].subRows[subIndex].ticketList = this.state.chargeCodeList;
                  }
                }
              })
            });
            (newState.timesheet as any) = timesheet;
            (newState.draftID as any) = draftID;
            (newState.loading as boolean) = false;
            this.setState(newState, () => {
              this.getTimeEntriesData(id)
            });
          } else {
            this.setState({ draftID: null })
          }
          if (action.response.results && action.response.results.length === 0) {
            this.setState({
              loading: false,
            }, () => {
              this.getTimeEntriesData(id)
            });
          }
        } else {
          this.setState({
            loading: false,
          })
        }
      }
    )
  }
  getTimeEntriesData = (id) => {
    this.setState({ loading: true });
    this.props.getTimeEntries('', 'get', id).then(
      action => {
        if (action.type === 'TIMESHEET_SUCCESS') {
          const timeEntries = action.response;
          const newState = cloneDeep(this.state);
          newState.timesheet.map((timesheet, index) => {
            const entries = timeEntries.filter(entry => toTimezone(entry.timeStart, this.state.default_timezone, 'MM/DD/YYYY') === toFormattedDate(timesheet.date, 'MM/DD/YYYY'));
            entries.map((s, subIndex) => {
              newState.timesheet[index].subRows.push({
                entry_date: toTimezone(s.timeStart, this.state.default_timezone, 'MM/DD/YYYY'),
                row_id: `${s.timeStart}-${newState.timesheet[index].subRows.length + 1}`,
                project: s.chargeToType === 'ChargeCode' ? 'chargeCode' : s.project && s.project.id,
                project_id: s.chargeToType === 'ChargeCode' ? 'chargeCode' : s.project && s.project.id,
                charge_to_id: s.chargeToType ? s.chargeToId : s.ticket && s.ticket.id,
                charge_to_type: s.chargeToType,
                start_time: toTimezone(s.timeStart, this.state.default_timezone, 'HH:mm'),
                hours: s.actualHours,
                work_type_id: s.workType.id,
                notes: s.notes,
                time_sheet_id: s.timeSheet.id,
                time_entry_id: s.id,
                disabled: true,
                isConnectwiseData: true,
                isDraft: false,
                billing_option: s.billableOption,
                work_role_id: s.workRole.id,
                charge_to_status_id: s.statusId
              });
              newState.timesheet.map((t, index) => {
                t.subRows.map((s, subIndex) => {
                  if (s && s.project_id !== "chargeCode") {
                    newState.timesheet[index].subRows[subIndex].ticketLoading = true;
                    this.getTicketsList(s.project_id, index, subIndex);
                  } else {
                    newState.timesheet[index].subRows[subIndex].ticketList = this.state.chargeCodeList;
                  }
                })
              });
            })
          });
          (newState.loading as boolean) = false;
          this.setState(newState);
        } else {
          this.setState({
            loading: false,
          })
        }
      }
    )
  }
  getTicketsList = (id, index, subIndex) => {
    this.props.getProjectTickets(id).then(res => {
      const ticketList = res.response
      const newState = cloneDeep(this.state);
      if (newState.timesheet[index] && newState.timesheet[index].subRows[subIndex]) {
        newState.timesheet[index].subRows[subIndex].ticketList = ticketList;
        newState.timesheet[index].subRows[subIndex].ticketLoading = false;
      }
      this.setState(newState);
    });
  };
  fetchTimesheetProjectsList = () => {
    this.props.fetchTimesheetProjectsList().then(res => {
      const projectList = res.response;
      projectList.push({
        id: 'chargeCode',
        name: 'Charge Code',
        company: {
          name: 'Charge Code'
        },
      })
      this.setState({ projectList })
    });
  };
  getTimesheetSetting = () => {
    this.setState({ loading: true })
    this.props.timeSheetSettings('', 'get').then(
      action => {
        if (action.type === 'TIMESHEET_SETTING_SUCCESS') {
          this.setState({
            ...action.response, loading: false
          })
        }
      }
    )
  }
  getChargeCodes = () => {
    this.setState({ loading: true })
    this.props.getChargeCodes().then(
      action => {
        if (action.type === 'FETCH_CHARGE_CODE_SUCCESS') {
          this.setState({
            chargeCodeList: action.response, loading: false
          })
        }
      }
    )
  }

  getTimesheetPeriod = () => {
    this.setState({loadingRange : true})
    this.props.getTimesheetPeriod().then(res => {
      const timesheetPeriod = res.response
      if (res.type === 'ERROR') {
        this.props.addErrorMessage(res.errorList && res.errorList.data.join(', '));
      } else {
        this.setState({ loadingRange: false,timesheetPeriod, dateRange: timesheetPeriod[0].id });
        this.onSelectRange(timesheetPeriod[0].id)
      }
    });
  };

  onSelectRange = (date) => {
    const dateRange = this.state.timesheetPeriod.find(d => d.id === date);
    var dates = this.getDaysBetweenDates(dateRange.dateStart, dateRange.dateEnd);
    const timesheet = [];
    dates.forEach(function (x, index) {
      timesheet.push({
        'date': x, subRows: []
      });
    });
    this.setState({
      timesheet,
      dateRange: date,
      isDatePickerSelected: false,
      isDateValueSelected: true,
    });
    this.getDrafts(date);
  };

  getDaysBetweenDates = (startDate, endDate) => {
    var now = moment.utc(startDate).clone(), dates = [];

    while (now.isSameOrBefore(moment.utc(endDate))) {
      dates.push(now.format('MM/DD/YYYY'));
      now.add(1, 'days');
    }
    return dates;
  };
  addSubRow = (index) => {
    const newState = cloneDeep(this.state);
    const dateRange = this.state.dateRange;
    const default_start_time = this.state.default_start_time;
    newState.timesheet[index].subRows.push({
      entry_date: dateSetTimeZero(newState.timesheet[index].date),
      row_id: `${newState.timesheet[index].date}-${newState.timesheet[index].subRows.length + 1}`,
      project_id: '',
      charge_to_id: '',
      start_time: default_start_time,
      hours: this.state.default_hours,
      work_type_id: this.state.default_work_type_id,
      notes: '',
      time_sheet_id: dateRange,
      disabled: false,
      isConnectwiseData: false,
      isDraft: true,
    });
    this.setState(newState);
  }

  cancelSubRow = (index, subIndex, action,time_entry_id,isConnectwiseData) => {
    const newState = cloneDeep(this.state);
    if (action === 'remove') {
      this.setState({
        loading: true
       })
      newState.timesheet[index].subRows.splice(subIndex, 1);
      if(time_entry_id && isConnectwiseData){
        this.props.deleteTimeEntry(time_entry_id).then(
          action => {
            if (action.type === UPDATE_TIMEENTRY_SUCCESS) {
              this.setState({
               loading: false
              })
              this.props.addSuccessMessage(`Timesheet entry deleted from CW`);
            }
          }
        )
      }

    } else {
      newState.timesheet[index].subRows[subIndex].edit = false;
      this.props.addSuccessMessage(`Timesheet entry deleted`);
    }
    this.setState(newState);
    
    this.debouncedFetch();
  }
  handleChangeSubRow = (index, subIndex, e) => {
    const newState = cloneDeep(this.state);
    newState.timesheet[index].subRows[subIndex][e.target.name] = e.target.value;
    newState.timesheet[index].subRows[subIndex][`${e.target.name}Name`] = e.target.label;
    newState.timesheet[index].subRows[subIndex][`charge_to_status_id`] = e.target && e.target.e && e.target.e.status;
    this.setState(newState);
    this.debouncedFetch();
  }
  handleChangeSubRowTime = (index, subIndex, e) => {
    const newState = cloneDeep(this.state);
    newState.timesheet[index].subRows[subIndex].start_time = e;
    this.setState(newState);
    this.debouncedFetch();
  }

  formatTimeShow = (h: number) => {
    var s = (h % 24 < 12) ? "am" : "pm", h = h % 12 || 12;
    return (h < 10 ? "0" + h : h) + ":00" + " " + s;
  }

  handleChangeSubRowHours = (index, subIndex, e) => {
    const newState = cloneDeep(this.state);
    newState.timesheet[index].subRows[subIndex][e.target.name] = e.target.value;
    newState.timesheet[index].subRows[subIndex][`${e.target.name}Name`] = e.target.label;
    if (e.target.value > 24 || e.target.value < 0 || e.target.value === "0" || !e.target.value) {
      newState.timesheet[index].subRows[subIndex].hoursError = 'Hours should be between 0.5 to 24 only.';
    } else {
      newState.timesheet[index].subRows[subIndex].hoursError = '';
    }
    this.setState(newState);
    this.debouncedFetch();
  }


  handleChangeSubRowProject = (index, subIndex, e) => {
    const newState = cloneDeep(this.state);
    newState.timesheet[index].subRows[subIndex][e.target.name] = e.target.value;
    newState.timesheet[index].subRows[subIndex][`${e.target.name}Name`] = e.target.label;
    newState.timesheet[index].subRows[subIndex].charge_to_id = '';
    newState.timesheet[index].subRows[subIndex].charge_to_type = e.target.value === "chargeCode" ? 'ChargeCode' : 'ProjectTicket';
    if (e.target.value !== "chargeCode") {
      newState.timesheet[index].subRows[subIndex].ticketLoading = true;
        this.getTicketsList(e.target.value, index, subIndex)
    } else {
      newState.timesheet[index].subRows[subIndex].ticketList = this.state.chargeCodeList;
    }
    this.setState(newState);
    this.debouncedFetch();
  }

  saveAsDraft = () => {
    this.isValidTimesheet(false)
    let timesheet = cloneDeep(this.state.timesheet);
    let timesheetPayload = [];
    timesheet.map((element, index) => {
      timesheetPayload[index] = { ...element };
      timesheetPayload[index].subRows = timesheetPayload[index].subRows.filter(x => x && !x.disabled && x.isDraft)
    });
    const payload = { 'draft': { 'timesheet': timesheetPayload, 'dateRange': this.state.dateRange }, 'time_period_id': this.state.dateRange };
    const method = this.state.draftID ? 'PUT' : 'POST';
    this.props.saveAsDraft(payload, method, this.state.draftID).then(
      action => {
        if (action.type === 'SAVE_DRAFT_TIMESHEET_SUCCESS') {
          this.setState({
            loading: false,
          })
          this.setState({ draftID: action.response.id })
        }
      }
    )
  }
  generateStartTime = (time,entry_date)=>{
    const start_time1 = moment.utc(entry_date).set({
      hour: time.substring(0, 2),
      minute: time.substring(3, 5),
      second: 0,
      millisecond: 0,
    });
    const formatDate = moment.tz(this.state.default_timezone).utcOffset();
    const offsetDate = moment(start_time1)
      .utcOffset(formatDate).format();
    const ofString = offsetDate.substring(offsetDate.length - 6, offsetDate.length);
    const SDate = moment(start_time1).format();
   return  SDate.substring(0, SDate.length - 1) + ofString;
  }
  submitForApproval = () => {

    if (this.isValidTimesheet(true)) {
      this.setState({ loading: true });

      let timesheet = cloneDeep(this.state.timesheet);
      timesheet.forEach(element => {
        element.subRows.filter(x => x.disabled)
        element.subRows.forEach(e => {
          delete e.ticketList;
          delete e.NotesError;
          delete e.hoursError;
          delete e.project;
          delete e.project_idError;
          delete e.project_idName;
          delete e.start_timeError;
          delete e.ticketLoading;
          delete e.charge_to_idError;
          delete e.charge_to_idName;
          delete e.typeError;
          e.charge_to_type = e.charge_to_type || 'ProjectTicket ';
          e.project_id = e.project_id === 'chargeCode' ? null : e.project_id
          // e.start_time1 = moment.utc(e.entry_date).set({
          //   hour: e.start_time.substring(0, 2),
          //   minute: e.start_time.substring(3, 5),
          //   second: 0,
          //   millisecond: 0,
          // });
          // const formatDate = moment.tz(this.state.default_timezone).utcOffset();
          // const offsetDate = moment(e.start_time1)
          //   .utcOffset(formatDate).format();
          // const ofString = offsetDate.substring(offsetDate.length - 6, offsetDate.length);
          // const SDate = moment(e.start_time1).format();
          //e.start_time = SDate.substring(0, SDate.length - 1) + ofString;

          e.start_time =  this.generateStartTime(e.start_time,e.entry_date)

          delete e.start_time1;
        });
      });

      let timesheetPayload = [];
      timesheet.map((element, index) => {
        timesheetPayload[index] = { ...element };
        timesheetPayload[index].subRows = timesheetPayload[index].subRows.filter(x => x && !x.time_entry_id).filter(x => x && !x.disabled)
      });
      let rows = [];
      timesheetPayload.forEach(function (obj) {
        rows = rows.concat(obj.subRows);
      });
      this.props.submitTimesheet(rows).then(
        action => {
          if (action.type === 'SUBMIT_TIMESHEET_SUCCESS') {
            this.setState({
              loading: false,
              invalidIds:[]
            });
            this.getTimesheetPeriod();
            this.props.addSuccessMessage(`Timesheet Submitted successfully...`);
            if (this.state.draftID) {
              this.props.saveAsDraft({}, 'delete', this.state.draftID)
            }
          } else {
            this.setState({
              loading: false,
              error: action.errorList && action.errorList.data && action.errorList.data.error,
              invalidIds: action.errorList && action.errorList.data.invalid_ticket_ids
            });
            this.props.addErrorMessage(`Error Occurred`);
          }
        }
      )
    } else {
      this.props.addErrorMessage(`Please resolve all errors to submit`);
    }
  }
  isValidTimesheet = (checkShow) => {
    let isValid = true;
    let timesheet = this.state.timesheet;
    timesheet.forEach((row, index) => {
      row.subRows.map((subRow, subIndex) => {
        if (subRow) {
          const isTandM = subRow.project_idName && subRow.project_idName.toLowerCase().includes('t&m');
          const requiredMessage = checkShow ? "required" : "";
          const readOnlyField = !subRow.disabled ? !subRow.project_id ? requiredMessage : '' : '';
          const notesAdded = subRow.notes;
          timesheet[index].subRows[subIndex].NotesError = isTandM && !notesAdded ? 'Notes required' : '';
          timesheet[index].subRows[subIndex].project_idError =  readOnlyField;
          timesheet[index].subRows[subIndex].charge_to_idError = !subRow.charge_to_id ? 'Please select ticket' : '';
          timesheet[index].subRows[subIndex].start_timeError = !subRow.start_time ? requiredMessage : '';
          timesheet[index].subRows[subIndex].typeError = !subRow.work_type_id ? requiredMessage : '';
          timesheet[index].subRows[subIndex].hoursError = !subRow.hours ? requiredMessage : '';

          if (subRow.hours > 24 || subRow.hours < 0 || subRow.hours === "0" || !subRow.hours) {
            timesheet[index].subRows[subIndex].hoursError = 'Hours should be between 0.5 to 24 only.';
          }
          if (checkShow && (readOnlyField ||
            !subRow.charge_to_id ||
            !subRow.start_time ||
            !subRow.work_type_id ||
            subRow.hours > 24 || subRow.hours < 0 || subRow.hours === "0" || !subRow.hours ||
            isTandM && !notesAdded)) {
            isValid = false;
          }
        }
      });
    });
    this.setState({ timesheet, isValid, error: '' });
    return isValid;
  }
  
  getTotalCount = () => {
    const count = commonFunctions.arrSum(this.state.timesheet.map(x => x.subRows && x.subRows.map(y => y && y.hours)))
    return count ? Number(count.toFixed(2)) : 0
  }

  enableEdit = (e, index, subIndex,isOpen) => {
      const newState = cloneDeep(this.state);
      newState.timesheet[index].subRows[subIndex].edited = isOpen;
      newState.timesheet[index].subRows[subIndex].disabled = !isOpen;
      newState.timesheet[index].subRows[subIndex].isConnectwiseData = false;
      this.setState(newState);
  };


  onSaveRowClick = (e, index,subIndex, rowData) => {
    this.setState({
      loading: true
     })
     const payload = cloneDeep(rowData.subRows[subIndex]);
     
     payload.start_time =  this.generateStartTime(payload.start_time,payload.entry_date)

    this.props.updateTimeEntry(payload).then(
      action => {
        if (action.type === UPDATE_TIMEENTRY_SUCCESS) {
          this.setState({
             loading: false
          })
          this.props.addSuccessMessage('Timesheet Updated')
          this.enableEdit(e, index, subIndex,false);
        }
      }
    )
  };

  render() {

    return (
      <div className="engineer-timesheet">
        <div className="loader modal-loader">
          <Spinner show={this.state.loading} />
        </div>
        <div className="header">
          <div className="right">

            <Input
              field={{
                label: "Date Range",
                type: "PICKLIST",
                value: this.state.dateRange,
                isRequired: false,
                options: commonFunctions.isNonEmptyArray(this.state.timesheetPeriod).map(t => ({
                  value: t.id,
                  label: `${toFormattedDate(t.dateStart, "MM/DD/YYYY")} - ${toFormattedDate(t.dateEnd, "MM/DD/YYYY")}`,
                })),
              }}
              width={6}
              labelIcon={''}
              name="dateRange"
              onChange={(e) => this.onSelectRange(e.target.value)}
              placeholder={`Date Range`}
              disabled={false}
              loading={this.state.loadingRange}
            />
            <Input
              field={{
                label: 'Timezone',
                type: "PICKLIST",
                isRequired: false,
                value: this.state.default_timezone,
                options: this.state.timezones && this.state.timezones.map(t => ({
                  value: t.timezone,
                  label: t.display_name,
                }))
              }}
              width={6}
              placeholder="Select timezone"
              name="timezone"
              onChange={e => this.setState({ default_timezone: e.target.value })}
              disabled={true}
            />
          </div>
          <div className="left">
            <div className={`total-count`}
            >

              Total Hours : {this.getTotalCount()}
              {this.getTotalCount() >= this.state.weekly_hours_threshold &&
                <img className="daily-check-passed" alt="" src="/assets/icons/check.png" />}
            </div>
            <SmallConfirmationBox
              className='remove-timesheet-row'
              onClickOk={() => this.submitForApproval()}
              fullText={'Want to Submit for Approval? Please confirm'}
              confirmText="Confirm"
              elementToCLick={<SquareButton
                onClick={() => null}
                content="Submit for Approval"
                bsStyle={"default"}
                disabled={false}
                className="submit-btn"
              />}
            />

            <SquareButton
              onClick={() => this.saveAsDraft()}
              content="Save as Draft"
              bsStyle={"primary"}
              disabled={false}
              className="save-btn"
            />
          </div>
        </div>
        <div className="timesheet-table">
          {
            this.state.error && <div className="no-data error-message">{this.state.error}</div>
          }
          <div className="table-header">
            <div className="column date">Date</div>
            <div className="column">Project Name</div>
            <div className="column">Ticket</div>
            <div className="column">Start Time</div>
            <div className="column">Hours</div>
            <div className="column">Work Type</div>
            <div className="column">Notes</div>
          </div>
          {
            this.state.timesheet.length == 0 && <div className="no-data">No Data (Please select a date range)</div>
          }
          {
            this.state.timesheet.map((row, index) => {
              return (
                <div className="time-row" key={index}>
                  <div className="content">
                    <div className="date-row">
                      <div className="date">{moment(row.date).format("MM/DD/YYYY")}</div>
                      <div className="date">{moment(row.date).format("dddd")}</div>
                      {commonFunctions.arrSum(row.subRows && row.subRows.map(y => y && y.hours)) >= this.state.daily_hours_threshold &&
                        <img className="daily-check-passed" alt="" src="/assets/icons/check.png" />}
                    </div>
                    <div className="sub-rows">
                      {
                        row.subRows && row.subRows.map((subRow, subIndex) => {
                          return (
                              subRow ? <div title={subRow.time_entry_id } className={`sub-row-edit ${subRow.disabled ? 'disabled-row ' : ''} ${ this.state.invalidIds.includes(`${subRow.charge_to_id}`) ? 'error-subrow ' : ''}`} key={subIndex}>
                                <div className="inputs">
                                  {subRow.time_entry_id && <div className='Connectwise'>Connectwise</div> }
                                  <Input
                                    field={{
                                      label: "",
                                      type: "PICKLIST",
                                      value: subRow.project_id,
                                      isRequired: false,
                                      options: this.state.projectList.map(project => ({
                                        value: project.id,
                                        label: ` ${project.company && project.company.name ? `${project.company.name}` : ''} - ${project.name}`,
                                      }))
                                    }}
                                    width={2}
                                    labelIcon={''}
                                    name="project_id"
                                    onChange={(e) => this.handleChangeSubRowProject(index, subIndex, e)}
                                    placeholder={`Select Project`}
                                    error={subRow.project_idError && {
                                      errorState: "error",
                                      errorMessage: subRow.project_idError,
                                    }}
                                    disabled={subRow.disabled}
                                  />
                                  <Input
                                    field={{
                                      label: "",
                                      type: "PICKLIST",
                                      value: subRow.charge_to_id,
                                      isRequired: false,
                                      options: subRow.ticketList && subRow.ticketList.map(ticket => ({
                                        value: ticket.id,
                                        label: `${ticket.summary || ticket.name}`,
                                        status: ticket.status && ticket.status.id
                                      })),
                                    }}
                                    width={2}
                                    labelIcon={''}
                                    name="charge_to_id"
                                    onChange={(e) => this.handleChangeSubRow(index, subIndex, e)}
                                    placeholder={`Select Ticket`}
                                    error={subRow.charge_to_idError && {
                                      errorState: "error",
                                      errorMessage: subRow.charge_to_idError,
                                    }}
                                    loading={subRow.ticketLoading}
                                    disabled={subRow.disabled}
                                  />
                                  <Input
                                    field={{
                                      label: "",
                                      type: "CUSTOM",
                                      value: subRow.start_time,
                                      isRequired: false,
                                    }}
                                    width={2}
                                    labelIcon={''}
                                    name="start_time"
                                    disabled={subRow.disabled}
                                    customInput={
                                      <TimePicker
                                        value={subRow.start_time}
                                        onChange={(e) => this.handleChangeSubRowTime(index, subIndex, e)}
                                        clockIcon={null}
                                        clearIcon={null}
                                        format={"hh:mm a"}
                                        disabled={subRow.disabled}
                                      />
                                    }
                                    onChange={(e) => null}
                                    placeholder={'Start Time'}
                                    error={subRow.start_timeError && {
                                      errorState: "error",
                                      errorMessage: subRow.start_timeError,
                                    }}
                                  />
                                  <Input
                                    field={{
                                      label: "",
                                      type: "NUMBER",
                                      value: subRow.hours,
                                      isRequired: false,
                                    }}
                                    width={1}
                                    labelIcon={''}
                                    name="hours"
                                    onChange={(e) => this.handleChangeSubRowHours(index, subIndex, e)}
                                    placeholder={`Hours`}
                                    error={subRow.hoursError && {
                                      errorState: "error",
                                      errorMessage: subRow.hoursError,
                                    }}
                                    disabled={subRow.disabled}
                                    className={''}
                                  />
                                  <Input
                                    field={{
                                      label: "",
                                      type: "PICKLIST",
                                      value: subRow.work_type_id,
                                      isRequired: false,
                                      options: this.state.WorkTypeList && this.state.WorkTypeList.map(type => ({
                                        value: type.id,
                                        label: `${type.name}`,
                                      })),
                                    }}
                                    width={2}
                                    labelIcon={''}
                                    name="work_type_id"
                                    onChange={(e) => this.handleChangeSubRow(index, subIndex, e)}
                                    placeholder={`Type`}
                                    error={subRow.typeError && {
                                      errorState: "error",
                                      errorMessage: subRow.typeError,
                                    }}
                                    disabled={subRow.disabled}
                                  />
                                  <Input
                                    field={{
                                      label: "",
                                      type: "TEXT",
                                      value: subRow.notes,
                                      isRequired: false,
                                    }}
                                    width={2}
                                    labelIcon={''}
                                    name="notes"
                                    onChange={(e) => this.handleChangeSubRow(index, subIndex, e)}
                                    placeholder={`Note`}
                                    error={subRow.NotesError && {
                                      errorState: "error",
                                      errorMessage: subRow.NotesError,
                                    }}
                                    disabled={subRow.disabled}
                                    title={subRow.notes && subRow.notes.length > 15 ? subRow.notes : '' }
                                  />
                            
                                <div className="action-btn">
                                  <SmallConfirmationBox
                                    className='remove-timesheet-row'
                                    onClickOk={() => this.cancelSubRow(index, subIndex, 'remove',subRow.time_entry_id,subRow.isConnectwiseData)}
                                    text={'timesheet row'}
                                  />
                                      {!subRow.edited && subRow.time_entry_id && (

                                  <img
                                    className={"d-pointer icon-remove edit-png"}
                                    alt=""
                                    src={"/assets/icons/edit.png"}
                                    title={"Edit"}
                                    onClick={(e) => {
                                      this.enableEdit(e, index, subIndex,true);
                                    }}
                                  />
                              )}

                                </div>
                              {subRow.edited && (
                                <div className="row-action-btn">
                                  <SquareButton
                             onClick={(e) => {
                                      this.enableEdit(e, index, subIndex,false);
                                    }}
                                    content={"Cancel"}
                                    bsStyle={"default"}
                                  />
                                  <SquareButton
                                    onClick={(e) =>
                                      this.onSaveRowClick(e, index,subIndex, row)
                                    }
                                    content={"Apply"}
                                    bsStyle={"primary"}
                                    disabled={
                                      !subRow.start_time ||
                                      !subRow.hours ||
                                      !subRow.work_type_id ||
                                      subRow.notes &&subRow.notes.length < 3
                                    }
                                  />
                                </div>
                              )}
                                </div>
                              </div> : null
                          )
                        })
                      }
                    </div>
                  </div>
                  <div className="add-another" onClick={e => this.addSubRow(index)}>Add New</div>
                </div>
              )
            })
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
});

const mapDispatchToProps = (dispatch: any) => ({
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  getProjectTickets: (id?: number) => dispatch(getProjectTickets(id)),
  updateTimeEntry: (data:any) => dispatch(updateTimeEntry(data)),
  deleteTimeEntry: (id: number) => dispatch(deleteTimeEntry(id)),
  fetchTimesheetProjectsList: () => dispatch(fetchTimesheetProjectsList()),
  GetWorkTypes: () => dispatch(GetWorkTypes()),
  getTimesheetPeriod: () => dispatch(getTimesheetPeriod()),
  saveAsDraft: (data: any, method: any, rangeId: any) => dispatch(saveAsDraft(data, method, rangeId)),
  getDraft: (data: any, method: any, rangeId: any) => dispatch(getDraft(data, method, rangeId)),
  getTimeEntries: (data: any, method: any, rangeId: any) => dispatch(getTimeEntries(data, method, rangeId)),
  submitTimesheet: (data: any) => dispatch(submitTimesheet(data)),
  timeSheetSettings: (data: any, method: any) => dispatch(timeSheetSettings(data, method)),
  fetchTimezones: () => dispatch(fetchTimezones()),
  getChargeCodes: () => dispatch(getChargeCodes()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EngineerTimesheets);
