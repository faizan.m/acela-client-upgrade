import React from "react";
import moment from "moment";
import * as XLSX from "xlsx";
import { connect } from "react-redux";
import { cloneDeep, debounce } from "lodash";
import InfiniteScroll from "react-infinite-scroll-component";
import {
  uploadProcessCR,
  createChangeRequests,
  fetchChangeRequestsAll,
  FETCH_CHANGE_REQ_SUCCESS,
  UPDATE_CHANGE_REQ_SUCCESS,
} from "../../../actions/pmo";
import {
  downloadSOWDocumnet,
  DOWNLOAD_SOW_SUCCESS,
} from "../../../actions/sow";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import Select from "../../../components/Input/Select/select";
import ModalBase from "../../../components/ModalBase/modalBase";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../components/Button/deleteButton";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import Filters from "./filters";
import "../../../commonStyles/projectManagement.scss";
import "./style.scss";

interface ICRListAllProps extends ICommonProps {
  customers: ICustomerShort[];
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  createChangeRequests: (projectID: number, data: any) => Promise<any>;
  uploadProcessCR: (id: number, CRID: number, data: any) => Promise<any>;
  fetchChangeRequests: (
    params: IServerPaginationParams & ICRFilters
  ) => Promise<any>;
  downloadSOWDocumnet: (
    id: number,
    type: string,
    name?: string
  ) => Promise<any>;
}

interface ICRListAllState {
  show: boolean;
  noData: boolean;
  nextPage: number;
  loading: boolean;
  attachment: File;
  totalRows: number;
  inputValue: string;
  uploading: boolean;
  list: ICRListItem[];
  filters: ICRFilters;
  showSearch: boolean;
  openPreview: boolean;
  showFiltersPop: boolean;
  previewPDFIds: number[];
  changeRequestId: number;
  downloadingIds: number[];
  loadingExcelData: boolean;
  previewHTML: IFileResponse;
  showAllChangeRequest?: boolean;
  customerOptions: { value: string; label: string }[];
  pagination: IServerPaginationParams;
}

class CRListAll extends React.Component<ICRListAllProps, ICRListAllState> {
  constructor(props: ICRListAllProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 2000);
  }

  getEmptyState = () => ({
    list: [],
    nextPage: 1,
    pagination: {
      page: 1,
      page_size: 25,
      ordering: "",
      search: "",
    },
    filters: {},
    totalRows: 0,
    inputValue: "",
    noData: false,
    showSearch: false,
    loadingExcelData: false,
    showAllChangeRequest: false,
    loading: true,
    showFiltersPop: false,
    customerOptions: [],
    openPreview: false,
    previewPDFIds: [],
    previewHTML: null,
    attachment: null,
    uploading: false,
    show: false,
    changeRequestId: 0,
    downloadingIds: [],
  });

  componentDidMount() {
    this.fetchMoreData(true);
  }

  static getDerivedStateFromProps(
    nextProps: ICRListAllProps,
    prevState: ICRListAllState
  ) {
    if (nextProps.customers && !prevState.customerOptions.length) {
      const customerOptions: IPickListOptions[] = nextProps.customers.map(
        (el) => ({
          value: el.id,
          label: el.name,
        })
      );
      return {
        customerOptions,
      };
    }
    return null;
  }

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
    this.updateMessage(search);
  };

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };

  handleExportClick = () => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
      minimumFractionDigits: 0,
    });

    this.setState({ loadingExcelData: true });
    const filters = this.state.filters;
    let prevParams: IServerPaginationParams & ICRFilters = this.state
      .pagination;
    prevParams = {
      ...prevParams,
      ...filters,
      customer_id:
        filters.customer_id && filters.customer_id.length
          ? (filters.customer_id as number[]).join(",")
          : undefined,
      page: 1,
      page_size: this.state.totalRows,
    };

    const getString = (s: string | number) => (s ? String(s) : "");

    this.props
      .fetchChangeRequests(prevParams)
      .then((action) => {
        if (action.type === FETCH_CHANGE_REQ_SUCCESS) {
          const raw_data: ICRListItem[] = action.response.results;
          const rows = raw_data.map((row: ICRListItem) => ({
            name: getString(row.name),
            type: getString(row.change_request_type),
            project: getString(row.project_title),
            customer: getString(row.customer_name),
            requested_by: getString(row.requested_by_name),
            project_manager: getString(row.project_manager),
            assigned_to: getString(row.assigned_to_name),
            change_number: getString(row.change_number),
            customer_cost: formatter.format(Number(row.customer_cost || 0)),
            status: getString(row.status),
            created:
              fromISOStringToFormattedDate(row.created_on, "MMM DD, YYYY") ||
              "-",
            updated:
              fromISOStringToFormattedDate(row.updated_on, "MMM DD, YYYY") ||
              "-",
          }));
          const worksheet = XLSX.utils.json_to_sheet(rows);
          const workbook = XLSX.utils.book_new();
          XLSX.utils.book_append_sheet(workbook, worksheet, "Change Requests");

          /* fix headers */
          XLSX.utils.sheet_add_aoa(
            worksheet,
            [
              [
                "Name",
                "Type",
                "Project",
                "Customer",
                "Requested By",
                "Project Manager",
                "Assigned To",
                "Change Number",
                "Amount",
                "Status",
                "Created On",
                "Updated On",
              ],
            ],
            { origin: "A1" }
          );

          /* calculate each column width */
          const max_width_arr = rows.reduce(
            (w, r) => [
              Math.max(w[0], r.name.length),
              Math.max(w[1], r.type.length),
              Math.max(w[2], r.project.length),
              Math.max(w[3], r.customer.length),
              Math.max(w[4], r.requested_by.length),
              Math.max(w[5], r.project_manager.length),
              Math.max(w[6], r.assigned_to.length),
              15,
              15,
              Math.max(w[9], r.status.length),
              Math.max(w[10], r.created.length),
              Math.max(w[11], r.updated.length),
            ],
            [10, 10, 10, 10, 10, 10, 10, 15, 15, 10, 10, 10]
          );

          worksheet["!cols"] = max_width_arr.map((el) => ({
            wch: el,
          }));
          XLSX.writeFile(
            workbook,
            `change_requests_${new Date().toISOString()}.xlsx`
          );
          this.setState({ loadingExcelData: false });
        } else this.setState({ loadingExcelData: false });
      })
      .catch(() => this.setState({ loadingExcelData: false }));
  };

  fetchMoreData = (clearData: boolean = false) => {
    const filters = this.state.filters;
    let prevParams: IServerPaginationParams & ICRFilters = this.state
      .pagination;
    let nextPage = this.state.nextPage;
    this.setState({
      loading: true,
    });
    if (clearData) {
      nextPage = 1;
      prevParams = {
        ...this.state.pagination,
        page: 1,
        page_size: 25,
      };
    }
    if (nextPage !== null) {
      prevParams = {
        ...prevParams,
        ...filters,
        customer_id:
          filters.customer_id && filters.customer_id.length
            ? (filters.customer_id as number[]).join(",")
            : undefined,
      };

      this.props.fetchChangeRequests(prevParams).then((action) => {
        if (action.response) {
          let list = [];
          if (clearData) {
            list = [...action.response.results];
          } else {
            list = [...this.state.list, ...action.response.results];
          }
          const newPagination: IServerPaginationParams = {
            ...this.state.pagination,
            page: action.response.links.next_page_number,
          };

          this.setState({
            pagination: newPagination,
            list,
            totalRows: action.response.count,
            nextPage: action.response.links.next_page_number,
            noData: list.length === 0 ? true : false,
            loading: false,
          });
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  fetchByOrder = (field: string) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  getOrderClass = (field: string) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  goToDetails = (changeRequest: ICRListItem) => {
    this.props.history.push(
      `/Projects/${changeRequest.project_id}/change-requests/${changeRequest.id}`
    );
  };

  renderFilters = () => {
    const filters = this.state.filters;
    const shouldRenderFilters =
      filters.status ||
      (filters.customer_id && filters.customer_id.length) ||
      (filters.updated_before && filters.updated_after);

    return shouldRenderFilters ? (
      <div className="custom-filters-listing">
        <label>Applied Filters: </label>
        {filters.status && (
          <div className="section-show-filters cr-applied-filters-row">
            <label>Status: </label>
            <div className="cr-filter-content">
              {filters.status + "  "}
              <span
                className="cr-filter-clear-icon"
                onClick={() => this.applyFilters({ status: undefined })}
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
        {filters.updated_after && (
          <div className="section-show-filters cr-applied-filters-row">
            <label>Updated between:</label>
            <div className="cr-filter-content">
              {moment(filters.updated_after).format("MM/DD/YYYY")}
              {" - "}
              {moment(filters.updated_before).format("MM/DD/YYYY")}
              <span
                className="date-clear cr-filter-clear-icon"
                onClick={() =>
                  this.applyFilters({
                    updated_after: undefined,
                    updated_before: undefined,
                  })
                }
              >
                &#x00d7;
              </span>
            </div>
          </div>
        )}
        {Boolean(filters.customer_id && filters.customer_id.length) && (
          <div className="section-show-filters cr-applied-filters-row">
            <label>Customer: </label>
            <Select
              name="customer"
              value={filters.customer_id}
              onChange={(e) =>
                this.applyFilters({ customer_id: e.target.value })
              }
              options={this.state.customerOptions}
              multi={true}
              placeholder=""
              searchable={false}
            />
          </div>
        )}
      </div>
    ) : null;
  };

  renderTableHeaderActions = () => {
    return (
      <div className="cr-listing-header">
        <div className="actions">
          <div className="left-section">
            <Input
              field={{
                value: this.state.inputValue,
                label: "",
                type: "SEARCH",
              }}
              width={5}
              name="searchString"
              onChange={this.onSearchStringChange}
              onBlur={() => {
                if (this.state.inputValue.trim() === "") {
                  this.setState({ showSearch: false });
                }
              }}
              placeholder="Search"
              className="search  search-change"
            />
            <div className="total-count">
              <span>Total : </span>
              {this.state.totalRows || 0}
            </div>
          </div>
          <div className="right-btn-actions">
            <SquareButton
              onClick={this.handleExportClick}
              className={"cr-filter-btn"}
              content={
                <span>
                  <img
                    src={
                      this.state.loadingExcelData
                        ? "/assets/icons/loading.gif"
                        : "/assets/icons/export.png"
                    }
                    alt="Downloading File"
                  />
                  Export
                </span>
              }
              bsStyle={"primary"}
              disabled={this.state.loading || this.state.loadingExcelData}
            />
            <SquareButton
              content={
                <span>
                  <img alt="" src="/assets/icons/filter.png" />
                  Filters
                </span>
              }
              bsStyle={"primary"}
              onClick={() => this.toggleFilterModal(true)}
              className="cr-filter-btn"
            />
          </div>
        </div>
        {this.renderFilters()}
      </div>
    );
  };

  toggleFilterModal = (show: boolean) => {
    this.setState({ showFiltersPop: show });
  };

  applyFilters = (filters: ICRFilters) => {
    this.setState(
      (prev) => ({ filters: { ...prev.filters, ...filters } }),
      () => {
        this.toggleFilterModal(false);
        this.fetchMoreData(true);
      }
    );
  };

  onSaveClick = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const newState = cloneDeep(this.state);
    (newState.list[index].status as string) = e.target.value;
    this.setState(newState, () => {
      this.props
        .createChangeRequests(
          this.props.match.params.id,
          this.state.list[index]
        )
        .then((action) => {
          if (action.type === UPDATE_CHANGE_REQ_SUCCESS) {
          }
        });
    });
  };

  getPDFPreviewMarkup = (cr: ICRListItem) => {
    if (this.state.previewPDFIds.includes(cr.id)) {
      return (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Downloading File"
        />
      );
    } else {
      return (
        <DeleteButton
          type="pdf_preview"
          title="Preview"
          onClick={() => this.onPDFPreviewClick(cr)}
        />
      );
    }
  };

  changeRequestListingRows = (changeRequest: ICRListItem, index: number) => {
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    return (
      <div className={`change-request-row`} key={index}>
        <div
          className="left-align first-column"
          onClick={() => this.goToDetails(changeRequest)}
          title={changeRequest.name}
        >
          {changeRequest.change_request_type === "Fixed Fee" && (
            <img
              className="icon-list-fixed-fee"
              title="Fixed Fee"
              alt=""
              src="/assets/icons/coin.svg"
            />
          )}{" "}
          {changeRequest.change_request_type === "T & M" && (
            <img
              className="icon-list-tm"
              title={"T & M"}
              alt=""
              src="/assets/icons/timer.svg"
            />
          )}
          {changeRequest.name}
        </div>
        <div
          className="left-align ellipsis-no-width"
          title={changeRequest.project_title}
        >
          {changeRequest.project_title}
        </div>
        <div
          className="left-align ellipsis-no-width"
          title={changeRequest.customer_name}
        >
          {changeRequest.customer_name}
        </div>
        <div className="user-dropdown-list ellipsis-no-width">
          {changeRequest.requested_by_name}
        </div>

        <div className="user-dropdown-list ellipsis-no-width">
          {changeRequest.project_manager}
        </div>
        <div className={`left-align`}>
          {fromISOStringToFormattedDate(
            changeRequest.updated_on,
            "MMM DD, YYYY"
          ) || "-"}
        </div>
        <div className={`center-align`}>
          {formatter.format(changeRequest.customer_cost || 0)}
        </div>
        <div className={`center-align`}>{changeRequest.change_number}</div>
        {/* <div className={`center-align`}>{changeRequest.change_number || 0}</div> */}
        {/* <div className={`left-align`}>
          {changeRequest.status === "In Progress" && (
            <SquareButton
              content={"Process CR"}
              bsStyle={"primary"}
              onClick={() => {
                this.setState({
                  show: true,
                  changeRequestId: changeRequest.id,
                });
              }}
              className="process-cr"
            />
          )}
        </div> */}
        <Input
          field={{
            label: "",
            type: "PICKLIST",
            value: changeRequest.status,
            isRequired: false,
            options: ["In Progress", "Cancelled/Rejected", "Approved"].map(
              (x) => ({
                value: x,
                label: <div className={`label-user-dropdown ${x}`}>{x}</div>,
                disabled: x === "Approved",
              })
            ),
          }}
          width={12}
          name="status"
          onChange={(e) => this.onSaveClick(e, index)}
          placeholder={""}
          disabled={changeRequest.status === "Approved"}
          className={`list-selection`}
        />
        {/* <div className="left-align">{changeRequest.status}</div> */}
        <div className="right-align">
          {changeRequest.status === "In Progress" ? (
            <img
              onClick={() => this.goToDetails(changeRequest)}
              className={"d-pointer edit-png"}
              alt=""
              src={"/assets/icons/edit.png"}
              title={"Edit in details"}
            />
          ) : (
            <div className="blank-img" />
          )}
          {this.getPDFPreviewMarkup(changeRequest)}
        </div>
      </div>
    );
  };

  onPDFPreviewClick(changeRequest: ICRListItem): any {
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, changeRequest.id],
    });
    this.props
      .downloadSOWDocumnet(changeRequest.sow_document_id, "pdf", "")
      .then((a) => {
        if (a.type === DOWNLOAD_SOW_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            previewPDFIds: this.state.previewPDFIds.filter(
              (id) => changeRequest.id !== id
            ),
          });
        }
      });
  }

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.show as boolean) = e;
    this.setState(newState);
  };

  handleFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const attachment: File = files[0];
    this.setState({ attachment });
  };

  onUploadClick = () => {
    const attachment = this.state.attachment;
    this.setState({ uploading: true });
    if (attachment) {
      const data = new FormData();
      data.append("files", attachment);
      this.props
        .uploadProcessCR(
          this.props.match.params.id,
          this.state.changeRequestId,
          data
        )
        .then((action) => {
          if (action.type === "PROCES_CR_UPLOAD_SUCCESS") {
            this.fetchMoreData(true);
            this.props.addSuccessMessage("Upload Successful!");
          }
          if (action.type === "PROCES_CR_UPLOAD_FAILURE") {
            if (action.errorList.data && action.errorList.data.detail) {
              this.props.addErrorMessage(action.errorList.data.detail);
            } else this.props.addErrorMessage("Upload Failed!");
          }
          this.setState({ uploading: false, changeRequestId: 0, show: false });
        });
    } else {
      this.props.addErrorMessage("Invalid format.");
    }
  };

  render() {
    return (
      <>
        <div className="change-request">
          <h3>Change Requests</h3>
          {this.renderTableHeaderActions()}
          <div className="change-request-list change-request-list-all">
            <div className={`header`}>
              <div
                className={`left-align first-column ${this.getOrderClass(
                  "name"
                )}`}
                onClick={() => this.fetchByOrder("name")}
              >
                Type &amp; Name
              </div>
              <div
                className={`left-align order-field ${this.getOrderClass(
                  "project_title"
                )}`}
                onClick={() => this.fetchByOrder("project_title")}
              >
                Project
              </div>
              <div
                className={`left-align order-field ${this.getOrderClass(
                  "customer_name"
                )}`}
                onClick={() => this.fetchByOrder("customer_name")}
              >
                Customer
              </div>
              <div className={`left-align ellipsis-no-width`}>Requested By</div>
              <div className={`left-align ellipsis-no-width`}>
                Project Manager
              </div>
              <div
                className={`left-align order-field ${this.getOrderClass(
                  "updated_on"
                )}`}
                onClick={() => this.fetchByOrder("updated_on")}
              >
                Updated
              </div>
              <div
                className={`center-align order-field ${this.getOrderClass(
                  "customer_cost"
                )}`}
                onClick={() => this.fetchByOrder("customer_cost")}
              >
                Amount
              </div>
              {/* <div className={`center-align`}>Number</div> */}
              {/* <div className={`left-align process-cr-heading`}>Process CR</div> */}
              <div className="center-align">Number</div>
              <div
                className={`left-align order-field ${this.getOrderClass(
                  "status"
                )}`}
                onClick={() => this.fetchByOrder("status")}
              >
                Status
              </div>
              <div />
            </div>
            <div id="scrollableDiv">
              <InfiniteScroll
                dataLength={this.state.list.length}
                next={this.fetchMoreData}
                hasMore={Boolean(this.state.nextPage)}
                loader={<h4 className="no-data"> Loading...</h4>}
                scrollableTarget="scrollableDiv"
                style={{
                  overflow: "unset",
                  minHeight: this.state.list.length > 0 ? "200px" : "unset",
                }}
              >
                {this.state.list.map((project, index) =>
                  this.changeRequestListingRows(project, index)
                )}
                {!this.state.loading &&
                  this.state.nextPage &&
                  this.state.list.length > 0 && (
                    <div
                      className="load-more"
                      onClick={() => {
                        this.fetchMoreData(false);
                      }}
                    >
                      load more data...
                    </div>
                  )}
              </InfiniteScroll>
              {this.state.noData && this.state.list.length === 0 && (
                <div className="no-data">No Change Requests Available</div>
              )}
              {this.state.loading && (
                <div className="loader">
                  <Spinner show={true} />
                </div>
              )}
            </div>
            <PDFViewer
              show={this.state.openPreview}
              onClose={this.toggleOpenPreview}
              titleElement={`View Change Request Preview`}
              previewHTML={this.state.previewHTML}
              footerElement={
                <SquareButton
                  content="Close"
                  bsStyle={"default"}
                  onClick={this.toggleOpenPreview}
                />
              }
              className=""
            />
            <ModalBase
              show={this.state.show}
              onClose={(e) => this.clearPopUp()}
              titleElement={"Change Request Processing"}
              bodyElement={
                <>
                  <div className="loader">
                    <Spinner show={this.state.uploading} />
                  </div>
                  <Input
                    field={{
                      label: "",
                      type: "FILE",
                      value: `${
                        this.state.attachment ? this.state.attachment : ""
                      }`,
                      id: "attachment",
                    }}
                    width={9}
                    name=""
                    accept="*"
                    onChange={this.handleFile}
                    className="custom-file-input upload-btn"
                  />
                </>
              }
              footerElement={
                <div className="col-md-12">
                  {
                    <SquareButton
                      content={"Save"}
                      bsStyle={"primary"}
                      onClick={this.onUploadClick}
                      className=""
                      disabled={!this.state.attachment}
                    />
                  }
                  <SquareButton
                    content={"Close"}
                    bsStyle={"default"}
                    onClick={(e) => this.clearPopUp(!this.state.show)}
                    className=""
                  />
                </div>
              }
              className={`confirm`}
            />
          </div>
        </div>
        <Filters
          show={this.state.showFiltersPop}
          customers={this.state.customerOptions}
          onSubmit={this.applyFilters}
          onClose={() => this.toggleFilterModal(false)}
          prevFilters={this.state.filters}
        />
      </>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchChangeRequests: (params: IServerPaginationParams & ICRFilters) =>
    dispatch(fetchChangeRequestsAll(params)),
  createChangeRequests: (projectID: number, data: any) =>
    dispatch(createChangeRequests(projectID, data)),
  downloadSOWDocumnet: (id: number, type: string, name?: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
  uploadProcessCR: (id: number, CRID: number, data: any) =>
    dispatch(uploadProcessCR(id, CRID, data)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CRListAll);
