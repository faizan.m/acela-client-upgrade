import { cloneDeep, debounce } from "lodash";
import React from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";
import {
  uploadProcessCR,
  getActionItemList,
  getProjectDetails,
  fetchChangeRequests,
  createChangeRequests,
  getProjectCustomerContactsByID,
  PROJECT_DETAILS_SUCCESS,
  UPDATE_CHANGE_REQ_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
} from "../../../actions/pmo";
import {
  downloadSOWDocumnet,
  DOWNLOAD_SOW_SUCCESS,
} from "../../../actions/sow";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import DeleteButton from "../../../components/Button/deleteButton";
import { fromISOStringToFormattedDate } from "../../../utils/CalendarUtil";
import "../../../commonStyles/projectManagement.scss";
import "./style.scss";

interface IChangeRequestProps extends ICommonProps {
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
  getProjectDetails: (projectID: number) => Promise<any>;
  getActionItemList: (projectId: number, api: string) => Promise<any>;
  getProjectCustomerContactsByID: (projectID: number) => Promise<any>;
  createChangeRequests: (projectID: number, data: any) => Promise<any>;
  uploadProcessCR: (id: number, CRID: number, data: any) => Promise<any>;
  downloadSOWDocumnet: (
    id: number,
    type: string,
    name?: string
  ) => Promise<any>;
  fetchChangeRequests: (
    id: number,
    params?: IScrollPaginationFilters
  ) => Promise<any>;
}

interface IChangeRequestState {
  project?: any;
  show: boolean;
  usersList: any;
  noData: boolean;
  loading: boolean;
  attachment: File;
  inputValue: string;
  uploading: boolean;
  list: ICRListItem[];
  showSearch: boolean;
  openPreview: boolean;
  previewPDFIds: number[];
  changeRequestId: number;
  downloadingIds: number[];
  previewHTML: IFileResponse;
  projectCustomerContacts: any;
  pagination: IScrollPaginationFilters;
}

const project = {
  name: "",
  pm: "",
  type: "",
  actual: 0,
  budget: 0,
  close_date: "",
  action_date: "",
  status: "",
};

class PMOChangeRequests extends React.Component<
  IChangeRequestProps,
  IChangeRequestState
> {
  constructor(props: IChangeRequestProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 2000);
  }

  getEmptyState = () => ({
    project,
    list: [],
    show: false,
    noData: false,
    loading: true,
    usersList: [],
    inputValue: "",
    attachment: null,
    uploading: false,
    showSearch: false,
    previewPDFIds: [],
    previewHTML: null,
    openPreview: false,
    changeRequestId: 0,
    downloadingIds: [],
    projectCustomerContacts: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: "",
      search: "",
    },
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState((prevState) => ({
      project: {
        ...prevState.project,
        id,
      },
    }));
    this.getProjectDetails(id);
    this.fetchMoreData(true);
    this.getTeamMembers();
    this.getProjectCustomerContactsByID(id);
  }

  getTeamMembers = () => {
    this.props
      .getActionItemList(this.props.match.params.id, "team-members")
      .then((teamMembers) => {
        const usersList = teamMembers.response;
        this.setState({ usersList });
      });
  };

  getProjectCustomerContactsByID = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectCustomerContactsByID(id).then((action) => {
      if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
        this.setState({
          projectCustomerContacts: action.response,
        });
      }
    });
  };

  getProjectDetails = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectDetails(id).then((action) => {
      if (action.type === PROJECT_DETAILS_SUCCESS) {
        this.setState({
          project: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
    this.updateMessage(search);
  };

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };

  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
      };
    }
    if (prevParams.nextPage !== null) {
      prevParams = {
        ...prevParams,
      };

      const projectID = this.props.match.params.id;
      this.props.fetchChangeRequests(projectID, prevParams).then((action) => {
        if (action.response) {
          let list = [];
          if (clearData) {
            list = [...action.response.results];
          } else {
            list = [...this.state.list, ...action.response.results];
          }
          const newPagination = {
            currentPage: action.response.links.page_number,
            nextPage: action.response.links.next_page_number,
            page_size: this.state.pagination.page_size,
            ordering: this.state.pagination.ordering,
            search: this.state.pagination.search,
          };

          this.setState({
            pagination: newPagination,
            list,
            noData: list.length === 0 ? true : false,
            loading: false,
          });
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  fetchByOrder = (field: string) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  getOrderClass = (field: string) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  goToDetails = (changeRequest: ICRListItem) => {
    const query = new URLSearchParams(this.props.location.search);
    const AllProject = query.get("AllProject") === "true" ? true : false;
    this.props.history.push(
      `/Projects/${this.props.match.params.id}/change-requests/${changeRequest.id}?&AllProject=${AllProject}`
    );
  };

  renderTableHeaderActions = () => {
    return (
      <div className="header-actions">
        <div className="left">Change Request</div>
        <div className="right">
          <Input
            field={{
              value: this.state.inputValue,
              label: "",
              type: "SEARCH",
            }}
            width={10}
            name="searchString"
            onChange={this.onSearchStringChange}
            onBlur={() => {
              if (this.state.inputValue.trim() === "") {
                this.setState({ showSearch: false });
              }
            }}
            placeholder="Search"
            className="search  search-change"
          />
          <span className="search-add-separator">|</span>
          <SquareButton
            content="Add"
            bsStyle={"primary"}
            onClick={() => {
              const projectID = this.props.match.params.id;
              const query = new URLSearchParams(this.props.location.search);
              const AllProject =
                query.get("AllProject") === "true" ? true : false;
              const ChangeRequestID = 0;
              this.props.history.push(
                `/Projects/${projectID}/change-requests/${ChangeRequestID}?AllProject=${AllProject}`
              );
            }}
            className="header-action-btn"
          />
        </div>
      </div>
    );
  };

  onSaveClick = (e: React.ChangeEvent<HTMLInputElement>, index: number) => {
    const newState = cloneDeep(this.state);
    (newState.list[index].status as any) = e.target.value;
    this.setState(newState, () => {
      this.props
        .createChangeRequests(
          this.props.match.params.id,
          this.state.list[index]
        )
        .then((action) => {
          if (action.type === UPDATE_CHANGE_REQ_SUCCESS) {
          }
        });
    });
  };

  getStream = (doc: number) => {
    this.setState({
      downloadingIds: [...this.state.downloadingIds, doc],
    });
    this.props.downloadSOWDocumnet(doc, "pdf").then((action) => {
      if (action.type === DOWNLOAD_SOW_SUCCESS) {
        const url = action.response.file_path;
        const link = document.createElement("a");
        link.href = url;
        link.target = "_blank";
        link.setAttribute("download", action.response.file_name);
        document.body.appendChild(link);
        link.click();
        this.setState({
          downloadingIds: this.state.downloadingIds.filter((id) => doc !== id),
        });
      }
    });
  };

  changeRequestListingRows = (changeRequest: ICRListItem, index: number) => {
    const getPDFPrevieMarkUp = (sow_document) => {
      if (this.state.previewPDFIds.includes(changeRequest.id)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_preview"
            title="Preview"
            onClick={() => this.onPDFPreviewClick(changeRequest)}
          />
        );
      }
    };

    const getDOCXDownloadMarkUp = (cell: number) => {
      if (this.state.downloadingIds.includes(cell)) {
        return (
          <img
            className="icon__loading"
            src="/assets/icons/loading.gif"
            alt="Downloading File"
          />
        );
      } else {
        return (
          <DeleteButton
            type="pdf_download"
            title="PDF Download"
            onClick={(e) => this.getStream(cell)}
          />
        );
      }
    };

    const requested =
      this.state.projectCustomerContacts &&
      this.state.projectCustomerContacts.find(
        (u) => u.user_id === changeRequest.requested_by
      );
    const assigned_to =
      this.state.usersList &&
      this.state.usersList.find((u) => u.id === changeRequest.assigned_to);
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });

    return (
      <div className={`change-request-row`} key={index}>
        <div
          className="left-align first-column ellipsis-no-width"
          onClick={() => this.goToDetails(changeRequest)}
          title={changeRequest.name}
        >
          {" "}
          {changeRequest.change_request_type === "Fixed Fee" && (
            <img
              className="icon-list-fixed-fee"
              title="Fixed Fee"
              alt=""
              src="/assets/icons/coin.svg"
            />
          )}{" "}
          {changeRequest.change_request_type === "T & M" && (
            <img
              className="icon-list-tm"
              title={"T & M"}
              alt=""
              src="/assets/icons/timer.svg"
            />
          )}
          {changeRequest.name}
        </div>

        <div className="user-dropdown-list">
          {requested && (
            <>
              <div
                style={{
                  backgroundImage: `url(${
                    requested && requested.profile_url
                      ? `${requested.profile_url}`
                      : "/assets/icons/user-filled-shape.svg"
                  })`,
                }}
                className="pm-image-circle"
              />
              <div className="type" title={`${requested.name}`}>
                {`${requested.name}`}
              </div>
            </>
          )}
        </div>

        <div className="user-dropdown-list">
          {assigned_to && (
            <>
              <div
                style={{
                  backgroundImage: `url(${
                    assigned_to && assigned_to.profile_url
                      ? `${assigned_to.profile_url}`
                      : "/assets/icons/user-filled-shape.svg"
                  })`,
                }}
                className="pm-image-circle"
              />
              <div
                className="type"
                title={`${assigned_to.first_name} ${assigned_to.last_name}`}
              >
                {`${assigned_to.first_name} ${assigned_to.last_name}`}
              </div>
            </>
          )}
        </div>

        <div className={`left-align`}>
          {fromISOStringToFormattedDate(
            changeRequest.created_on,
            "MMM DD, YYYY"
          ) || "-"}
        </div>
        <div className={`left-align`}>
          {fromISOStringToFormattedDate(
            changeRequest.updated_on,
            "MMM DD, YYYY"
          ) || "-"}
        </div>
        <div className={`center-align`}>
          {formatter.format(
            (changeRequest &&
              changeRequest.json_config &&
              changeRequest.json_config.service_cost &&
              changeRequest.json_config.service_cost.customer_cost) ||
              0
          )}
        </div>
        <div className={`center-align`}>{changeRequest.change_number || 0}</div>
        <div className={`left-align`}>
          {changeRequest.status === "In Progress" && (
            <SquareButton
              content={"Process CR"}
              bsStyle={"primary"}
              onClick={() => {
                this.setState({
                  show: true,
                  changeRequestId: changeRequest.id,
                });
              }}
              className="process-cr"
            />
          )}
        </div>
        <Input
          field={{
            label: "",
            type: "PICKLIST",
            value: changeRequest.status,
            isRequired: false,
            options: ["In Progress", "Cancelled/Rejected", "Approved"].map(
              (x) => ({
                value: x,
                label: <div className={`label-user-dropdown ${x}`}>{x}</div>,
                disabled: x === "Approved",
              })
            ),
          }}
          width={12}
          name="status"
          onChange={(e) => this.onSaveClick(e, index)}
          placeholder={""}
          disabled={changeRequest.status === "Approved"}
          className={`list-selection`}
        />
        <div className="right-align">
          {changeRequest.status === "In Progress" ? (
            <img
              onClick={() => this.goToDetails(changeRequest)}
              className={"d-pointer edit-png"}
              alt=""
              src={"/assets/icons/edit.png"}
              title={"Edit in details"}
            />
          ) : (
            <div className="blank-img" />
          )}
          {getPDFPrevieMarkUp(changeRequest)}
          {getDOCXDownloadMarkUp(changeRequest.sow_document)}
        </div>
      </div>
    );
  };

  onPDFPreviewClick(changeRequest: ICRListItem) {
    this.setState({
      previewPDFIds: [...this.state.previewPDFIds, changeRequest.id],
    });
    this.props
      .downloadSOWDocumnet(changeRequest.sow_document, "pdf", "")
      .then((a) => {
        if (a.type === DOWNLOAD_SOW_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            previewPDFIds: this.state.previewPDFIds.filter(
              (id) => changeRequest.id !== id
            ),
          });
        }
      });
  }

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  clearPopUp = (e: boolean = false) => {
    const newState = cloneDeep(this.state);
    (newState.show as boolean) = e;
    this.setState(newState);
  };

  handleFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const files = Array.from(e.target.files);
    const attachment: File = files[0];
    this.setState({ attachment });
  };

  onUploadClick = () => {
    const attachment: any = this.state.attachment;
    this.setState({ uploading: true });
    if (attachment) {
      const data = new FormData();
      data.append("files", attachment);
      this.props
        .uploadProcessCR(
          this.props.match.params.id,
          this.state.changeRequestId,
          data
        )
        .then((action) => {
          if (action.type === "PROCES_CR_UPLOAD_SUCCESS") {
            this.fetchMoreData(true);
            this.props.addSuccessMessage("Upload Successful!");
          }
          if (action.type === "PROCES_CR_UPLOAD_FAILURE") {
            if (action.errorList.data && action.errorList.data.detail) {
              this.props.addErrorMessage(action.errorList.data.detail);
            } else this.props.addErrorMessage("Upload Failed!");
          }
          this.setState({ uploading: false, changeRequestId: 0, show: false });
        });
    } else {
      this.props.addErrorMessage("Invalid format.");
    }
  };

  render() {
    const project = this.state.project;
    return (
      <div className="change-request">
        <div className="header-change-request">
          <div className="left">
            <div className="name">
              {project.title} - {project.customer}
            </div>
          </div>
          <div className="right">
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={() => {
                const query = new URLSearchParams(this.props.location.search);
                const AllProject =
                  query.get("AllProject") === "true" ? true : false;
                this.props.history.push(
                  `/ProjectManagement/${this.props.match.params.id}?meeting=true&AllProject=${AllProject}`
                );
              }}
            />
          </div>
        </div>
        {this.renderTableHeaderActions()}
        <div className="change-request-list">
          <div className={`header`}>
            <div
              className={`left-align first-column ${this.getOrderClass(
                "name"
              )}`}
              onClick={() => this.fetchByOrder("name")}
            >
              Type &amp; Name
            </div>
            <div className={`left-align`}> Requested By</div>
            <div className={`left-align`}> Assigned to</div>
            <div className={`left-align`}> Created</div>
            <div className={`left-align`}> Updated</div>
            <div className={`center-align`}>Amount</div>
            <div className={`center-align`}>Number</div>
            <div className={`left-align process-cr-heading`}>Process CR</div>
            <div className={`left-align`}>Status</div>
            <div />
          </div>
          <div
            id="scrollableDiv"
            style={{
              height: "72vh",
              overflow: "auto",
              minHeight: "200px",
            }}
          >
            <InfiniteScroll
              dataLength={this.state.list.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
              style={{
                minHeight: this.state.list.length > 0 ? "200px" : "unset",
              }}
            >
              {this.state.list.map((project, index) =>
                this.changeRequestListingRows(project, index)
              )}
              {!this.state.loading &&
                this.state.pagination.nextPage &&
                this.state.list.length > 0 && (
                  <div
                    className="load-more"
                    onClick={() => {
                      this.fetchMoreData(false);
                    }}
                  >
                    load more data...
                  </div>
                )}
            </InfiniteScroll>
            {this.state.noData && this.state.list.length === 0 && (
              <div className="no-data">No Change Requests Available</div>
            )}
            {this.state.loading && (
              <div className="loader">
                <Spinner show={true} />
              </div>
            )}
          </div>
          <PDFViewer
            show={this.state.openPreview}
            onClose={this.toggleOpenPreview}
            titleElement={`View Change Request Preview`}
            previewHTML={this.state.previewHTML}
            footerElement={
              <SquareButton
                content="Close"
                bsStyle={"default"}
                onClick={this.toggleOpenPreview}
              />
            }
            className=""
          />
          <ModalBase
            show={this.state.show}
            onClose={(e) => this.clearPopUp()}
            titleElement={"Change Request Processing"}
            bodyElement={
              <>
                <div className="loader">
                  <Spinner show={this.state.uploading} />
                </div>
                <Input
                  field={{
                    label: "",
                    type: "FILE",
                    value: `${
                      this.state.attachment ? this.state.attachment : ""
                    }`,
                    id: "attachment",
                  }}
                  width={9}
                  name=""
                  accept="*"
                  onChange={this.handleFile}
                  className="custom-file-input upload-btn"
                />
              </>
            }
            footerElement={
              <div className="col-md-12">
                {
                  <SquareButton
                    content={"Save"}
                    bsStyle={"primary"}
                    onClick={this.onUploadClick}
                    className=""
                    disabled={!this.state.attachment}
                  />
                }
                <SquareButton
                  content={"Close"}
                  bsStyle={"default"}
                  onClick={() => this.clearPopUp(!this.state.show)}
                  className=""
                />
              </div>
            }
            className={`confirm`}
          />
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchChangeRequests: (id: number, params?: IScrollPaginationFilters) =>
    dispatch(fetchChangeRequests(id, params)),
  getProjectDetails: (projectID: number) =>
    dispatch(getProjectDetails(projectID)),
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  createChangeRequests: (projectID: number, data: any) =>
    dispatch(createChangeRequests(projectID, data)),
  getProjectCustomerContactsByID: (projectID: number) =>
    dispatch(getProjectCustomerContactsByID(projectID)),
  downloadSOWDocumnet: (id: number, type: string, name?: string) =>
    dispatch(downloadSOWDocumnet(id, type, name)),
  uploadProcessCR: (id: number, CRID: number, data: any) =>
    dispatch(uploadProcessCR(id, CRID, data)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(PMOChangeRequests);
