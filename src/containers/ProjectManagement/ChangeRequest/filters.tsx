import moment from "moment";
import React from "react";
// import DateRangePicker from "react-daterange-picker"; (New Component)
import SquareButton from "../../../components/Button/button";
import ModalBase from "../../../components/ModalBase/modalBase";
import Input from "../../../components/Input/input";
import "../../../commonStyles/filtersListing.scss";
import "../../../commonStyles/filterModal.scss";
import "./style.scss";

interface ICRListingFilterProps {
  show: boolean;
  onClose: () => void;
  onSubmit: (filters: ICRFilters) => void;
  prevFilters?: ICRFilters;
  customers: IPickListOptions[];
}

interface ICRListingFilterState {
  filters: ICRFilters;
  isDatePickerSelected: boolean;
  isDateValueSelected: boolean;
}

interface SelectOptions {
  value: string;
  label: string;
}

const StatusOptions: SelectOptions[] = [
  {
    value: "Approved",
    label: "Approved",
  },
  {
    value: "In Progress",
    label: "In Progress",
  },
  {
    value: "Cancelled/Rejected",
    label: "Cancelled/Rejected",
  },
];

export default class AgreementFilter extends React.Component<
  ICRListingFilterProps,
  ICRListingFilterState
> {
  constructor(props: ICRListingFilterProps) {
    super(props);

    this.state = {
      filters: {
        customer_id: undefined,
        updated_before: undefined,
        updated_after: undefined,
        status: undefined,
      },
      isDatePickerSelected: false,
      isDateValueSelected: false,
    };
  }

  componentDidUpdate(prevProps: ICRListingFilterProps) {
    if (prevProps.show !== this.props.show && this.props.show) {
      this.setState({
        filters: this.props.prevFilters ? this.props.prevFilters : {},
        isDateValueSelected: Boolean(
          this.props.prevFilters && this.props.prevFilters.updated_before
        ),
      });
    }
  }

  onSubmit = () => {
    this.props.onSubmit(this.state.filters);
  };

  onFilterChange = (e) => {
    const targetName = e.target.name;
    const targetValue = e.target.value;
    this.setState((prevState) => ({
      filters: {
        ...prevState.filters,
        [targetName]: targetValue,
      },
    }));
  };

  // render methods
  getTitle = () => {
    return "Filters";
  };

  showDateRangePicker = () => {
    const prevFilter = { ...this.state.filters };
    this.setState({
      isDatePickerSelected: !this.state.isDatePickerSelected,
      isDateValueSelected: false,
      filters: {
        ...prevFilter,
        updated_after: "",
        updated_before: "",
      },
    });
  };

  onDateClear = () => {
    const prevFilters = this.state.filters;
    this.setState({
      filters: {
        ...prevFilters,
        updated_after: "",
        updated_before: "",
      },
      isDateValueSelected: false,
    });
  };

  onSelect = (value) => {
    this.setState({
      filters: {
        ...this.state.filters,
        updated_after: value.start.format("YYYY-MM-DD"),
        updated_before: value.end.format("YYYY-MM-DD"),
      },
      isDatePickerSelected: false,
      isDateValueSelected: true,
    });
  };

  getBody = () => {
    const filters = this.state.filters;

    const dateValue = filters.updated_after
      ? {
          start: moment(filters.updated_after),
          end: moment(filters.updated_before),
        }
      : null;

    return (
      <div className="filters-modal__body agr-filter col-md-12">
        <Input
          field={{
            label: "Customer",
            type: "PICKLIST",
            value: filters.customer_id,
            options: this.props.customers,
            isRequired: false,
          }}
          width={6}
          multi={true}
          name="customer_id"
          onChange={this.onFilterChange}
          placeholder={`Select Customer`}
        />
        <Input
          field={{
            label: "Status",
            type: "PICKLIST",
            value: filters.status,
            options: StatusOptions,
            isRequired: false,
          }}
          width={6}
          multi={false}
          name="status"
          onChange={this.onFilterChange}
          placeholder={`Select Status`}
        />

        <div className="field-section col-md-12">
          <div className="field__label">
            <label className="field__label-label">Updated between</label>
          </div>
          <div className="date-range-box field__input">
            <SquareButton
              onClick={this.showDateRangePicker}
              content=""
              bsStyle={"default"}
              className="date-button"
            />

            {/* {this.state.isDatePickerSelected &&
              !this.state.isDateValueSelected && (
                <DateRangePicker
                  value={dateValue}
                  onSelect={this.onSelect}
                  singleDateRange={true}
                  minimumDate={new Date("1990/01/01")}
                  maximumDate={new Date()}
                  numberOfCalendars={1}
                  showLegend={true}
                />
              )} */}
            {!this.state.isDatePickerSelected &&
              this.state.isDateValueSelected && (
                <>
                  <div className="date-value">
                    {dateValue.start.format("MM/DD/YYYY")}
                    {" - "}
                    {dateValue.end.format("MM/DD/YYYY")}
                  </div>
                  <div className="date-clear" onClick={this.onDateClear}>
                    &#x00d7;
                  </div>
                </>
              )}
          </div>
        </div>
      </div>
    );
  };

  getFooter = () => {
    return (
      <div className="filters-modal__footer">
        <SquareButton
          onClick={this.props.onClose}
          content="Cancel"
          bsStyle={"default"}
        />
        <SquareButton
          onClick={this.onSubmit}
          content="Apply"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <div>
        <ModalBase
          show={this.props.show}
          onClose={this.props.onClose}
          titleElement={this.getTitle()}
          bodyElement={this.getBody()}
          footerElement={this.getFooter()}
          className="filters-modal"
        />
      </div>
    );
  }
}
