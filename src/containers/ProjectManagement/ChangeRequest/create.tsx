import React from "react";
import moment from "moment";
import { connect } from "react-redux";
import {
  map,
  pick,
  cloneDeep,
  debounce,
  DebouncedFunc,
  isNil,
  round,
} from "lodash";
import {
  pmoCommonAPI,
  getProjectDetails,
  getChangeRequests,
  getActionItemList,
  createChangeRequests,
  getProjectEngineersByID,
  getProjectCustomerContactsByID,
  PROJECT_DETAILS_SUCCESS,
  PROJECT_ENGINEERS_SUCCESS,
  UPDATE_CHANGE_REQ_SUCCESS,
  PROJECT_CUSTOMER_CONTACTS_SUCCESS,
  SUCCESS,
} from "../../../actions/pmo";
import {
  previewSOW,
  getVendorsList,
  getCategoryList,
  getBaseTemplate,
  getVendorMappingList,
  CREATE_SOW_FAILURE,
  CREATE_SOW_SUCCESS,
} from "../../../actions/sow";
import { fetchSOWDOCSetting } from "../../../actions/setting";
import { addErrorMessage, addSuccessMessage } from "../../../actions/appState";

import AppValidators from "../../../utils/validator";
import { commonFunctions } from "../../../utils/commonFunctions";
import { getConvertedColorWithOpacity } from "../../../utils/CommonUtils";
import {
  getCustomerCost,
  getRecommendedHours,
  getSowCalculationFields,
  getCalculatedHourlyResource,
} from "../../../utils/sowCalculations";

import AddVendor from "../../SOW/Sow/addVendor";
import Spinner from "../../../components/Spinner";
import Input from "../../../components/Input/input";
import Accordian from "../../../components/Accordian";
import UsershortInfo from "../../../components/UserImage";
import SquareButton from "../../../components/Button/button";
import Checkbox from "../../../components/Checkbox/checkbox";
import PDFViewer from "../../../components/PDFViewer/PDFViewer";
import { QuillEditorAcela } from "../../../components/QuillEditor/QuillEditor";
import SmallConfirmationBox from "../../../components/SmallConfirmationBox/confirmation";
import PromptUnsaved from "../../../components/UnsavedWarning/PromptUnsaved";
import EditButton from "../../../components/Button/editButton";
import VendorMappingModal from "../../SOW/Sow/sowVendorMapping";
import "../../../commonStyles/serviceCostCalculations.scss";
import "./style.scss";
pmoCommonAPI;

interface CRStakeHolder {
  id: number;
  member_id: number;
  first_name?: string;
  last_name?: string;
  name?: string;
  profile_url: string;
}

interface IChangeRequestDetailsProps extends ICommonProps {
  baseTemplates: {
    base_config: IJSONConfig;
    change_request_config: IChangeRequestConfig;
  };
  docSetting: IDOCSetting;
  isFetchingVendors: boolean;
  vendorOptions: IPickListOptions[];
  vendorMapping: IVendorAliasMapping[];
  addErrorMessage: TShowErrorMessage;
  getVendorsList: () => Promise<any>;
  getBaseTemplate: () => Promise<any>;
  getCategoryList: () => Promise<any>;
  addSuccessMessage: TShowSuccessMessage;
  getVendorMapping: () => Promise<any>;
  fetchSOWDOCSetting: () => Promise<any>;
  previewSOW: (changeRequest: IChangeRequest) => Promise<any>;
  getProjectDetails: (projectID: number) => Promise<any>;
  pmoCommonAPI: (api: string, type: string) => Promise<any>;
  getProjectEngineersByID: (projectID: number) => Promise<any>;
  getActionItemList: (projectId: number, api: string) => Promise<any>;
  getProjectCustomerContactsByID: (projectID: number) => Promise<any>;
  createChangeRequests: (
    projectID: number,
    data: IChangeRequest
  ) => Promise<any>;
  getChangeRequests: (id: number, changeRequestID: number) => Promise<any>;
}

interface IChangeRequestDetailsState {
  project?: any;
  showVendorModal: boolean;
  showVendorMappingModal: boolean;
  changeRequest: IChangeRequest;
  sowCalculations: ISoWCalculationFields;
  error: {
    name: IFieldValidation;
    requested_by: IFieldValidation;
    assigned_to: IFieldValidation;
    service_technology_types: IFieldValidation;
    service_catalog_category: IFieldValidation;
    linked_service_catalog: IFieldValidation;
    engineering_hours: IFieldValidation;
    engineering_hourly_rate: IFieldValidation;
    after_hours_rate: IFieldValidation;
    project_management_hours: IFieldValidation;
    project_management_hourly_rate: IFieldValidation;
    after_hours: IFieldValidation;
    integration_technician_hours: IFieldValidation;
    integration_technician_hourly_rate: IFieldValidation;
    change_request_type: IFieldValidation;
    default_ps_risk: IFieldValidation;
  };
  loading: boolean;
  userLoading: boolean;
  contactLoading: boolean;
  isOpen: boolean;
  view: boolean;
  projectCustomerContacts: any;
  projectEngineers: any;
  accountManager: CRStakeHolder;
  sendEmail: boolean;
  usersList: CRStakeHolder[];
  isCollapsed: boolean[];
  errorsLabel: {
    [fieldName: string]: IFieldValidation;
  };
  errorsValue: {
    [fieldName: string]: IFieldValidation;
  };
  isValid: boolean;
  showError: boolean;
  openPreview: boolean;
  previewHTML: IFileResponse;
  currentVendorMapping: IVendorAliasMapping;
  vendorDescriptionMapping: Map<number, IVendorAliasMapping>;
  categories?: number;
  unsaved: boolean;
  riskVarOverride: boolean;
  riskVarRemove: boolean;
}

const project = {
  name: "",
  pm: "",
  type: "",
  actual: 0,
  budget: 0,
  close_date: "",
  action_date: "",
  status: "",
};

class ChangeRequestDetails extends React.Component<
  IChangeRequestDetailsProps,
  IChangeRequestDetailsState
> {
  static emptyErrorState: IFieldValidation = {
    errorState: "success",
    errorMessage: "",
  };
  static EmptyHourlyResource: IHourlyResource = {
    hours: 0,
    override: true,
    is_hidden: false,
    hourly_rate: 0,
    hourly_cost: 0,
    resource_id: null,
    resource_name: "",
    internal_cost: 0,
    customer_cost: 0,
    margin: 0,
    margin_percentage: 0,
  };
  static EmptyContractor: IContractor = {
    name: "",
    partner_cost: 0,
    customer_cost: 0,
    margin_percentage: 0,
    type: "Service",
    vendor_id: null,
    description: "",
  };
  private debouncedCalculations: DebouncedFunc<() => void>;

  constructor(props: IChangeRequestDetailsProps) {
    super(props);
    this.state = this.getEmptyState();
    this.debouncedCalculations = debounce(this.doCRCalculations, 1000);
  }

  getEmptyState = () => ({
    project,
    loading: false,
    userLoading: false,
    contactLoading: false,
    showVendorModal: false,
    showVendorMappingModal: false,
    isOpen: true,
    changeRequest: {
      name: "",
      requested_by: "",
      change_request_type: "" as SowDocType,
      assigned_to: "",
      doc_type: "Change Request",
      status: "In Progress",
      json_config: null,
      id: 0,
    },
    error: {
      name: { ...ChangeRequestDetails.emptyErrorState },
      requested_by: { ...ChangeRequestDetails.emptyErrorState },
      assigned_to: { ...ChangeRequestDetails.emptyErrorState },
      service_technology_types: { ...ChangeRequestDetails.emptyErrorState },
      service_catalog_category: { ...ChangeRequestDetails.emptyErrorState },
      linked_service_catalog: { ...ChangeRequestDetails.emptyErrorState },
      engineering_hours: { ...ChangeRequestDetails.emptyErrorState },
      engineering_hourly_rate: { ...ChangeRequestDetails.emptyErrorState },
      after_hours_rate: { ...ChangeRequestDetails.emptyErrorState },
      project_management_hours: { ...ChangeRequestDetails.emptyErrorState },
      project_management_hourly_rate: {
        ...ChangeRequestDetails.emptyErrorState,
      },
      integration_technician_hours: { ...ChangeRequestDetails.emptyErrorState },
      integration_technician_hourly_rate: {
        ...ChangeRequestDetails.emptyErrorState,
      },
      after_hours: { ...ChangeRequestDetails.emptyErrorState },
      change_request_type: { ...ChangeRequestDetails.emptyErrorState },
      default_ps_risk: { ...ChangeRequestDetails.emptyErrorState },
    },
    view: false,
    riskVarOverride: false,
    riskVarRemove: false,
    projectCustomerContacts: [],
    meetingAttendees: [],
    projectEngineers: [],
    vendorDescriptionMapping: new Map<number, IVendorAliasMapping>(),
    currentVendorMapping: {
      vendor_name: "",
      vendor_crm_id: null,
      resource_type: null,
      resource_description: "",
    },
    accountManager: {
      id: 0,
      member_id: 0,
      first_name: "",
      last_name: "",
      profile_url: "",
    },
    sendEmail: false,
    usersList: [],
    isCollapsed: [],
    errorsLabel: {},
    errorsValue: {},
    isValid: false,
    showError: false,
    openPreview: false,
    previewHTML: null,
    sowCalculations: {
      engineeringHoursInternalCost: 0,
      engineeringHoursCustomerCost: 0,
      engineeringHoursMargin: 0,
      engineeringHoursMarginPercent: 0,
      afterHoursInternalCost: 0,
      afterHoursCustomerCost: 0,
      afterHoursMargin: 0,
      afterHoursMarginPercent: 0,
      integrationTechnicianInternalCost: 0,
      integrationTechnicianCustomerCost: 0,
      integrationTechnicianMargin: 0,
      integrationTechnicianMarginPercent: 0,
      pmHoursInternalCost: 0,
      pmHoursCustomerCost: 0,
      pmHoursMargin: 0,
      pmHoursMarginPercent: 0,
      totalCustomerCost: 0,
      totalInternalCost: 0,
      totalMargin: 0,
      totalMarginPercent: 0,
      hourlyLaborCustomerCost: 0,
      hourlyLaborInternalCost: 0,
      hourlyLaborMargin: 0,
      hourlyLaborMarginPercent: 0,
      riskBudgetCustomerCost: 0,
      riskBudgetInternalCost: 0,
      riskBudgetMargin: 0,
      riskBudgetMarginPercent: 0,
      contractorCustomerCost: 0,
      contractorInternalCost: 0,
      contractorMargin: 0,
      contractorMarginPercent: 0,
      proSerCustomerCost: 0,
      proSerInternalCost: 0,
      proSerMargin: 0,
      proSerMarginPercent: 0,
      travelCustomerCost: 0,
      travelInternalCost: 0,
      travelMargin: 0,
      travelMarginPercent: 0,
    },
    unsaved: false,
  });

  componentDidMount() {
    const id = this.props.match.params.id;
    this.setState((prevState) => ({
      project: {
        ...prevState.project,
        id,
      },
    }));

    this.props.getVendorsList();
    this.props.getVendorMapping();
    this.props.fetchSOWDOCSetting();
    this.getProjectDetails(id);
    this.getProjectCustomerContactsByID(id);
    this.getProjectEngineersByID(id);
    this.getAccountManager();
    this.getTeamMembers();
    this.getCategoryList();
    const query = new URLSearchParams(this.props.location.search);
    const view = query.get("view") === "true" ? true : false;
    const newState = cloneDeep(this.state);
    (newState.view as boolean) = view;
    (newState.userLoading as boolean) = true;
    (newState.contactLoading as boolean) = true;
    this.setState(newState);
    if (
      this.props.match.params.ChangeRequestID &&
      this.props.match.params.ChangeRequestID !== "0"
    ) {
      this.getChangeRequestDetails(id, this.props.match.params.ChangeRequestID);
    } else {
      this.props.getBaseTemplate();
    }
  }

  componentDidUpdate(prevProps: IChangeRequestDetailsProps) {
    if (
      this.props.baseTemplates &&
      this.props.baseTemplates !== prevProps.baseTemplates
    ) {
      const newState = cloneDeep(this.state);
      newState.changeRequest.json_config = this.props.baseTemplates.change_request_config;
      newState.changeRequest.json_config.service_cost.default_ps_risk = this
        .props.docSetting
        ? this.props.docSetting.default_ps_risk * 100
        : 0;
      if (prevProps.docSetting && newState.changeRequest.json_config) {
        let keys = [
          "engineering_hourly_rate",
          "after_hours_rate",
          "integration_technician_hourly_rate",
          "project_management_hourly_rate",
        ];
        keys.forEach((key) => {
          if (newState.changeRequest.json_config.service_cost[key] === 0) {
            newState.changeRequest.json_config.service_cost[key] =
              prevProps.docSetting[key];
          }
        });
        (newState.isCollapsed as boolean[]) = Object.keys(
          newState.changeRequest.json_config
        ).map(() => false);
      }
      this.setState(newState);
    }
    if (
      this.props.docSetting &&
      this.props.docSetting !== prevProps.docSetting &&
      this.state.changeRequest.json_config &&
      this.state.changeRequest.json_config.service_cost
    ) {
      const newState = cloneDeep(this.state);
      let keys = [
        "engineering_hourly_rate",
        "after_hours_rate",
        "integration_technician_hourly_rate",
        "project_management_hourly_rate",
      ];
      keys.forEach((key) => {
        if (newState.changeRequest.json_config.service_cost[key] === 0) {
          newState.changeRequest.json_config.service_cost[
            key
          ] = this.props.docSetting[key];
        }
      });
      (newState.riskVarOverride as boolean) =
        this.props.docSetting.default_ps_risk * 100 !==
        newState.changeRequest.json_config.service_cost.default_ps_risk;
      (newState.riskVarRemove as boolean) =
        newState.changeRequest.json_config.service_cost.default_ps_risk === 0;
      this.setState(newState, this.doCRCalculations);
    }
    if (
      this.props.vendorMapping &&
      this.props.vendorMapping !== prevProps.vendorMapping
    ) {
      const vendorMapping = new Map<number, IVendorAliasMapping>();
      this.props.vendorMapping.forEach((el) => {
        vendorMapping.set(el.vendor_crm_id, el);
      });
      this.setState({ vendorDescriptionMapping: vendorMapping }, () => {
        if (this.state.changeRequest.json_config) {
          const changeRequest = cloneDeep(this.state.changeRequest);
          this.setVendorDescription(changeRequest.json_config);
          this.setState({ changeRequest });
        }
      });
    }
  }

  getTeamMembers = () => {
    this.props
      .getActionItemList(this.props.match.params.id, "team-members")
      .then((teamMembers) => {
        const usersList = teamMembers.response;
        this.setState({ usersList, userLoading: false });
      });
  };

  getCategoryList = () => {
    this.props.getCategoryList().then((s) => {
      const categories = s.response.find(
        (x: ICategoryList) => x.name === "Change Request"
      ).id;
      this.setState({ categories });
    });
  };

  getAccountManager = () => {
    this.props
      .getActionItemList(this.props.match.params.id, "account-manager")
      .then((action) => {
        this.setState({ loading: false, accountManager: action.response });
      });
  };

  getChangeRequestDetails = (id: number, changeRequestID: number) => {
    this.setState({ loading: true });
    this.props.getChangeRequests(id, changeRequestID).then((action) => {
      if (action.type === UPDATE_CHANGE_REQ_SUCCESS) {
        const changeRequest: IChangeRequest = action.response;
        this.setVendorDescription(changeRequest.json_config);
        const riskVarOverride: boolean = this.props.docSetting
          ? this.props.docSetting.default_ps_risk !==
            changeRequest.json_config.service_cost.default_ps_risk
          : false;
        const riskVarRemove: boolean =
          changeRequest.json_config.service_cost.default_ps_risk === 0;
        changeRequest.json_config.service_cost.default_ps_risk *= 100;
        this.setState({
          changeRequest,
          riskVarRemove,
          riskVarOverride,
          isCollapsed: Object.keys(action.response.json_config).map(
            () => false
          ),
        });
      }
      this.setState({ loading: false }, () => {
        this.doCRCalculations();
      });
    });
  };

  getProjectEngineersByID = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectEngineersByID(id).then((action) => {
      if (action.type === PROJECT_ENGINEERS_SUCCESS) {
        this.setState({
          projectEngineers: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  getProjectCustomerContactsByID = (id: number) => {
    this.props.getProjectCustomerContactsByID(id).then((action) => {
      if (action.type === PROJECT_CUSTOMER_CONTACTS_SUCCESS) {
        this.setState({
          projectCustomerContacts: action.response,
          contactLoading: false,
        });
      }
    });
  };

  getProjectDetails = (id: number) => {
    this.setState({ loading: true });
    this.props.getProjectDetails(id).then((action) => {
      if (action.type === PROJECT_DETAILS_SUCCESS) {
        this.setState({
          project: action.response,
        });
      }
      this.setState({ loading: false });
    });
  };

  calculateTotalHours = (serviceCost: IServiceCost): number => {
    let total_hours: number =
      serviceCost.after_hours +
      serviceCost.engineering_hours +
      serviceCost.project_management_hours +
      serviceCost.integration_technician_hours;
    if (serviceCost.hourly_resources)
      serviceCost.hourly_resources.forEach((el) => {
        total_hours += el.hours ? el.hours : 0;
      });
    return total_hours;
  };

  setVendorDescription = (jsonConfig: IChangeRequestConfig) => {
    /* Sets the description for those resources which does not have a set resource description*/

    if (!jsonConfig) return;

    if (jsonConfig.service_cost.hourly_resources) {
      jsonConfig.service_cost.hourly_resources.forEach((resource) => {
        if (
          this.state.vendorDescriptionMapping.has(resource.resource_id) &&
          !resource.resource_description
        )
          resource.resource_description = this.state.vendorDescriptionMapping.get(
            resource.resource_id
          ).resource_description;
        else
          resource.resource_description = resource.resource_description
            ? resource.resource_description
            : "";
      });
    }

    if (jsonConfig.service_cost.contractors) {
      jsonConfig.service_cost.contractors.forEach((contractor) => {
        if (
          this.state.vendorDescriptionMapping.has(contractor.vendor_id) &&
          !contractor.description
        ) {
          contractor.description = this.state.vendorDescriptionMapping.get(
            contractor.vendor_id
          ).resource_description;
        }
      });
    }
  };

  doCRCalculations = () => {
    if (this.state.changeRequest.json_config && this.props.docSetting)
      this.setState({
        sowCalculations: getSowCalculationFields(
          this.state.changeRequest.json_config.service_cost,
          this.props.docSetting
        ),
      });
  };

  handleChangeChangeRequest = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    // If we change type to Fixed Fee, unhide all the resources
    if (e.target.value === "Fixed Fee") {
      if (
        this.state.changeRequest.json_config &&
        this.state.changeRequest.json_config.service_cost
      ) {
        newState.changeRequest.json_config.service_cost.hourly_resources = this
          .state.changeRequest.json_config.service_cost.hourly_resources
          ? this.state.changeRequest.json_config.service_cost.hourly_resources.map(
              (el) => ({
                ...el,
                is_hidden: false,
              })
            )
          : [];
        newState.changeRequest.json_config.service_cost.hide_after_hours = false;
        newState.changeRequest.json_config.service_cost.hide_engineering_hours = false;
        newState.changeRequest.json_config.service_cost.hide_project_management_hours = false;
        newState.changeRequest.json_config.service_cost.hide_integration_technician_hours = false;
      }
    }
    newState.changeRequest[e.target.name] = e.target.value as SowDocType;
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doCRCalculations);
  };

  handleChange = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: string,
    field: string,
    type: string
  ) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config[key][field][type] = e.target.value;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeMarkdown = (
    html: string,
    key: string,
    field: string,
    type: string,
    source: string
  ) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config[key][field][type] = html;
    (newState.unsaved as boolean) = source === "api" ? false : true;
    this.setState(newState);
  };

  toggleVendorModal = (show: boolean) => {
    this.setState({
      showVendorModal: show,
    });
  };

  onSubmitVendorModal = () => {
    this.props.getVendorsList();
    this.toggleVendorModal(false);
  };

  onSaveClick = () => {
    if (this.checkValidaBoards()) {
      this.setState({ loading: true });
      const changeRequest: IChangeRequest = this.generatePayload();
      this.props
        .createChangeRequests(this.props.match.params.id, changeRequest)
        .then((action) => {
          if (action.type === UPDATE_CHANGE_REQ_SUCCESS) {
            const changeRequestId = action.response.id;
            const projectID = this.props.match.params.id;

            if (this.state.sendEmail) {
              this.props
                .pmoCommonAPI(
                  `projects/${projectID}/change-requests/${changeRequestId}/send-email`,
                  "post"
                )
                .then((a) => {
                  if (a.type === SUCCESS) {
                    this.props.addSuccessMessage(
                      `Change request ${
                        changeRequestId ? "updated" : "saved"
                      } and Email sent!`
                    );
                    this.setState(
                      { loading: false, unsaved: false },
                      this.props.history.goBack
                    );
                  } else {
                    this.props.addErrorMessage("Error while sending email !!!");
                    this.setState({ loading: false, unsaved: false });
                  }
                });
            } else {
              this.props.addSuccessMessage(
                `Change Request ${
                  changeRequestId ? "updated" : "saved"
                } successfully!`
              );
              this.setState(
                { loading: false, unsaved: false },
                this.props.history.goBack
              );
            }
          } else {
            this.setState({ loading: false });
            this.setValidationErrors(action.errorList.data);
          }
        });
    }
  };

  generatePayload = (): IChangeRequest => {
    const changeRequest = cloneDeep(this.state.changeRequest);
    const json_config = this.state.changeRequest.json_config;
    if (this.state.changeRequest && json_config) {
      Object.keys(json_config).map((key, index) => {
        const boardContainerData = json_config[key];
        {
          Object.keys(boardContainerData).map((k, i) => {
            if (k !== "section_label" && k !== "ordering") {
              if (
                boardContainerData[k] &&
                boardContainerData[k].type === "MARKDOWN"
              ) {
                json_config[key][
                  k
                ].value_markdown = commonFunctions.convertToMarkdown(
                  json_config[key][k].value
                );
              }
            }
          });
        }
      });
    }
    changeRequest.json_config.service_cost.notesMD = commonFunctions.convertToMarkdown(
      changeRequest.json_config.service_cost.notes
    );
    changeRequest.json_config.service_cost.customer_cost = this.state.sowCalculations.totalCustomerCost;
    changeRequest.json_config.service_cost.internal_cost = this.state.sowCalculations.totalInternalCost;
    if (
      changeRequest.json_config.change_affects &&
      changeRequest.json_config.change_affects[
        CRConfigSectionKeys.change_affects
      ]
    ) {
      changeRequest.json_config.change_affects[
        CRConfigSectionKeys.change_affects
      ].value = changeRequest.json_config.change_affects[
        CRConfigSectionKeys.change_affects
      ].options
        .filter((op) => op.active === true)
        .map((op) => op.label);
    }
    changeRequest.json_config.service_cost.contractors = map(
      json_config.service_cost.contractors,
      (object) => {
        return pick(object, [
          "customer_cost",
          "margin_percentage",
          "name",
          "partner_cost",
          "type",
          "vendor_id",
          "description",
        ]);
      }
    );
    changeRequest.json_config.service_cost.travels = map(
      json_config.service_cost.travels,
      (object) => {
        return pick(object, ["cost", "description"]);
      }
    );
    if (json_config.service_cost.hourly_resources)
      json_config.service_cost.hourly_resources = map(
        json_config.service_cost.hourly_resources,
        (object) => {
          return pick(object, [
            "hours",
            "override",
            "hourly_cost",
            "hourly_rate",
            "resource_id",
            "resource_name",
            "resource_description",
            "internal_cost",
            "customer_cost",
            "margin",
            "margin_percentage",
            "is_hidden",
          ]);
        }
      );
    changeRequest.json_config.service_cost.default_ps_risk =
      json_config.service_cost.default_ps_risk / 100;
    changeRequest.updated_on = moment().format("YYYY-MM-DDTHH:mm:ssZ");
    changeRequest.doc_type = "Change Request";
    return changeRequest;
  };

  // render methods
  renderTemplateByJson = (rawJson: IChangeRequestConfig) => {
    const readOnly = this.state.changeRequest.status === "Approved";
    let count = 0;
    const json: IChangeRequestConfig = Object.assign({}, rawJson);
    delete json.service_cost;

    return (
      <div className="template-fields col-md-12">
        {json &&
          Object.keys(json).length > 0 &&
          Object.keys(json)
            .sort((a, b) => json[a].ordering - json[b].ordering)
            .map((key, fieldIndex) => {
              const isCollapsed = !this.state.isCollapsed[fieldIndex];
              const toggleCollapsedState = () =>
                this.setState((prevState) => ({
                  isCollapsed: [
                    ...prevState.isCollapsed.slice(0, fieldIndex),
                    !prevState.isCollapsed[fieldIndex],
                    ...prevState.isCollapsed.slice(fieldIndex + 1),
                  ],
                }));

              return (
                <div
                  className={`field ${isCollapsed ? "field--collapsed" : ""}`}
                  key={fieldIndex}
                >
                  <div
                    className="section-heading"
                    onClick={toggleCollapsedState}
                  >
                    {json[key].section_label}{" "}
                    <div className="action-collapse">
                      {isCollapsed ? "+" : "-"}
                    </div>
                  </div>
                  <div className="body-section">
                    {" "}
                    {key &&
                      Object.keys(json[key])
                        .sort(
                          (a, b) =>
                            ((json[key][a] && json[key][a].ordering) || 0) -
                            ((json[key][b] && json[key][b].ordering) || 0)
                        )
                        .map((field, i) => {
                          if (
                            field === "section_label" ||
                            field === "visible_in" ||
                            field === "ordering" ||
                            field === "default_hidden"
                          ) {
                            return false;
                          }

                          count++;

                          return (
                            <div
                              className={`${
                                json[key][field].type === "MARKDOWN"
                                  ? "single-field col-lg-12 col-md-12 col-xs-12"
                                  : "single-field col-lg-12 col-md-12 col-xs-12"
                              }`}
                              key={i}
                            >
                              {json[key][field].type === "MARKDOWN" && (
                                <div
                                  className={`${
                                    this.state.errorsValue &&
                                    this.state.errorsValue[count] &&
                                    this.state.errorsValue[count].errorMessage
                                      ? "markdown markdown-error"
                                      : "markdown"
                                  }`}
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      disabled={readOnly}
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}

                                  <QuillEditorAcela
                                    onChange={(html, source) =>
                                      this.handleChangeMarkdown(
                                        html,
                                        key,
                                        field,
                                        "value",
                                        source
                                      )
                                    }
                                    label={`${
                                      json[key][field].is_label_editable ===
                                      "true"
                                        ? "Value"
                                        : json[key][field].label
                                    }`}
                                    value={json[key][field].value}
                                    wrapperClass={"change-request-config-quill"}
                                    isRequired={
                                      String(json[key][field].is_required) ===
                                      "true"
                                    }
                                    scrollingContainer=".app-body"
                                    isDisabled={readOnly}
                                    error={
                                      this.state.errorsValue &&
                                      this.state.errorsValue[count] &&
                                      this.state.errorsValue[count].errorMessage
                                        ? {
                                            errorState: "error",
                                            errorMessage: this.state
                                              .errorsValue[count].errorMessage,
                                          }
                                        : undefined
                                    }
                                  />
                                </div>
                              )}
                              {json[key][field].type === "CHECKBOX" && (
                                <div
                                  key={field}
                                  className="options input-label-box"
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      disabled={readOnly}
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  <div className="options-content checkbox-option">
                                    {json[key][field].options &&
                                      json[key][field].options.map(
                                        (option, optionIndex) => {
                                          return (
                                            <Checkbox
                                              isChecked={option.active}
                                              key={optionIndex}
                                              name="loa"
                                              onChange={(e) =>
                                                this.handleChangeCheckBox(
                                                  e,
                                                  key,
                                                  field,
                                                  "options",
                                                  optionIndex
                                                )
                                              }
                                              disabled={readOnly}
                                            >
                                              {option.label}
                                            </Checkbox>
                                          );
                                        }
                                      )}
                                  </div>
                                  {this.state.errorsValue &&
                                    this.state.errorsValue[count] &&
                                    this.state.errorsValue[count]
                                      .errorMessage && (
                                      <div className="select-markdown-error">
                                        {
                                          this.state.errorsValue[count]
                                            .errorMessage
                                        }
                                      </div>
                                    )}
                                </div>
                              )}
                              {json[key][field].type === "TEXTBOX" && (
                                <div className="input-label-box">
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      className="label-input"
                                      disabled={readOnly}
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  <Input
                                    field={{
                                      value: json[key][field].value,
                                      label: `${
                                        json[key][field].is_label_editable ===
                                        "true"
                                          ? "Value"
                                          : json[key][field].label
                                      }`,
                                      type: "TEXT",
                                      isRequired:
                                        json[key][field].is_required &&
                                        JSON.parse(
                                          json[key][field].is_required
                                        ),
                                    }}
                                    width={8}
                                    name={field}
                                    disabled={readOnly}
                                    onChange={(e) =>
                                      this.handleChange(e, key, field, "value")
                                    }
                                    error={this.state.errorsValue[count]}
                                  />
                                </div>
                              )}
                              {json[key][field].type === "RADIOBOX" && (
                                <div
                                  key={field}
                                  className="options input-label-box"
                                >
                                  {json[key][field].is_label_editable ===
                                    "true" && (
                                    <Input
                                      field={{
                                        value: json[key][field].label,
                                        label: json[key][field].section_label,
                                        type: "TEXT",
                                        isRequired: true,
                                      }}
                                      width={6}
                                      name={field}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "label"
                                        )
                                      }
                                      disabled={readOnly}
                                      className="label-input"
                                      error={this.state.errorsLabel[count]}
                                    />
                                  )}
                                  <div className="options-content">
                                    <Input
                                      field={{
                                        value: json[key][field].value,
                                        label:
                                          json[key][field].is_label_editable ===
                                          "false"
                                            ? json[key][field].label
                                            : "",
                                        type: "RADIO",
                                        isRequired: false,
                                        options: json[key][field].options.map(
                                          (role: string) => ({
                                            value: role,
                                            label: role,
                                          })
                                        ),
                                      }}
                                      width={8}
                                      name={field + count}
                                      onChange={(e) =>
                                        this.handleChange(
                                          e,
                                          key,
                                          field,
                                          "value"
                                        )
                                      }
                                      disabled={readOnly}
                                      error={this.state.errorsValue[count]}
                                    />
                                  </div>
                                </div>
                              )}
                            </div>
                          );
                        })}
                  </div>
                </div>
              );
            })}
      </div>
    );
  };

  getColWidth = (type: string) => {
    let width = "col-md-6";
    switch (type) {
      case "MARKDOWN":
        width = "col-md-6";
        break;
      case "TEXTBOX":
        width = "col-md-5";
        break;
      case "TEXTAREA":
        width = "col-md-6";
        break;
      case "RADIOBOX":
        width = "col-md-3";
        break;

      default:
        break;
    }

    return width;
  };

  getUpdateServiceCostCalc = (
    calcFields: ISoWCalculationFields,
    isZeroDollarCR: boolean
  ): ISoWCalculationFields => {
    if (isZeroDollarCR) {
      const newCalcFields: ISoWCalculationFields = cloneDeep(calcFields);
      newCalcFields.riskBudgetCustomerCost = 0;
      newCalcFields.riskBudgetInternalCost = 0;
      newCalcFields.riskBudgetMargin = 0;
      newCalcFields.riskBudgetMarginPercent = 0;
      newCalcFields.totalCustomerCost = 0;
      newCalcFields.totalInternalCost -= calcFields.riskBudgetInternalCost;
      newCalcFields.totalMargin = -newCalcFields.totalInternalCost;
      newCalcFields.totalMarginPercent = 0;
      return newCalcFields;
    }
    return calcFields;
  };

  renderStaticFields = () => {
    const serCostObj: IServiceCost = this.state.changeRequest.json_config
      .service_cost;
    if (!serCostObj) {
      return null;
    }

    const readOnly = this.state.changeRequest.status === "Approved";
    const formatter = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    });
    const serviceCost: ISoWCalculationFields = this.getUpdateServiceCostCalc(
      this.state.sowCalculations,
      this.state.changeRequest.json_config.service_cost
        .is_zero_dollar_change_request
    );
    const isTMDoc: boolean =
      this.state.changeRequest.change_request_type === "T & M";

    return (
      <div className="template-fields col-md-12">
        <div className="section-heading">
          SERVICE COST
          <div className="action-collapse">{""}</div>
        </div>
        <div className="service-cost-body">
          <div className="estimated-hours-rates-container">
            <div className="sub-heading col-md-12">
              <div className="text">
                <span>ESTIMATED HOURS TOTAL</span>
                {!readOnly && (
                  <div className="eht-actions-right">
                    <img
                      className="icon-reset-static"
                      src="/assets/icons/reset.svg"
                      title="Reset to default from setting"
                      onClick={this.setDefaultStatic}
                    />
                    <SquareButton
                      content={
                        <>
                          <span className="add-plus">+</span>
                          <span className="add-text">Create Vendor</span>
                        </>
                      }
                      className="add-vendor add-btn"
                      bsStyle={"link"}
                      onClick={() => this.toggleVendorModal(true)}
                      title={"Create new vendor"}
                    />
                  </div>
                )}
              </div>
            </div>
            <div className="estimated-hours-rates">
              <div className="estimated-hours-header">
                <div className="col-md-3">Resource</div>
                <div className="col-md-2">Total Hours</div>
                <div
                  className={`col-md-1 ${
                    isTMDoc ? "hide-header" : "override-header"
                  }`}
                >
                  Override
                </div>
                {isTMDoc && <div className="hide-header">Hide</div>}
                <div className="col-md-2">Hourly Rate (in $)</div>
                <div className="col-md-2">Hourly Cost (in $)</div>
                <div className="col-md-1">GP%</div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text">Engineer</div>
                <Input
                  field={{
                    value: serCostObj.engineering_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  placeholder="Enter Hours"
                  name={"engineering_hours"}
                  className="phase-name"
                  title={
                    serCostObj.hide_engineering_hours
                      ? "Resource is marked to hide in the CR, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={readOnly || serCostObj.hide_engineering_hours}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.engineering_hours}
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={false}
                    disabled={true}
                    name="override"
                    title="Override option is disabled because phases are not available in CR"
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_engineering_hours}
                      name="hide_engineering_hours"
                      disabled={serCostObj.engineering_hours !== 0}
                      title={
                        serCostObj.engineering_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in CR"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.engineering_hourly_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  name={"engineering_hourly_rate"}
                  placeholder="Enter Rate"
                  disabled={readOnly}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.engineering_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.engineeringHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.engineeringHoursMarginPercent)
                    ? serviceCost.engineeringHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text">
                  After Hours Engineer
                </div>
                <Input
                  field={{
                    value: serCostObj.after_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"after_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  title={
                    serCostObj.hide_after_hours
                      ? "Resource is marked to hide in the CR, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={readOnly || serCostObj.hide_after_hours}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  error={this.state.error.after_hours}
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={false}
                    disabled={true}
                    name="override"
                    title="Override option is disabled because phases are not available in CR"
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_after_hours}
                      name="hide_after_hours"
                      disabled={serCostObj.after_hours !== 0}
                      title={
                        serCostObj.after_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in CR"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.after_hours_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"after_hours_rate"}
                  placeholder="Enter Rate"
                  minimumValue={"0"}
                  disabled={readOnly}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.after_hours_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.afterHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.afterHoursMarginPercent)
                    ? serviceCost.afterHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text">
                  Integration Technician
                </div>
                <Input
                  field={{
                    value: serCostObj.integration_technician_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"integration_technician_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  title={
                    serCostObj.hide_integration_technician_hours
                      ? "Resource is marked to hide in the CR, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={
                    readOnly || serCostObj.hide_integration_technician_hours
                  }
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  error={this.state.error.integration_technician_hours}
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={false}
                    disabled={true}
                    name="override"
                    title="Override option is disabled because phases are not available in CR"
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_integration_technician_hours}
                      name="hide_integration_technician_hours"
                      disabled={serCostObj.integration_technician_hours !== 0}
                      title={
                        serCostObj.integration_technician_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in CR"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.integration_technician_hourly_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"integration_technician_hourly_rate"}
                  placeholder="Enter Rate"
                  disabled={readOnly}
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.integration_technician_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(
                    serviceCost.integrationTechnicianCustomerCost
                  )}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.integrationTechnicianMarginPercent)
                    ? serviceCost.integrationTechnicianMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              <div className="col-md-12 mapping-row">
                <div className="col-md-3 mapping-row-text">
                  Project Management
                </div>
                <Input
                  field={{
                    value: serCostObj.project_management_hours,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  name={"project_management_hours"}
                  placeholder="Enter Hours"
                  minimumValue={"0"}
                  onChange={(e) => this.handleChangeStatic(e)}
                  className="phase-name"
                  error={this.state.error.project_management_hours}
                  title={
                    serCostObj.hide_project_management_hours
                      ? "Resource is marked to hide in the CR, to modify Hours uncheck the Hide option"
                      : undefined
                  }
                  disabled={
                    readOnly ||
                    serCostObj.hide_project_management_hours ||
                    !serCostObj.project_management_hours_override
                  }
                />
                <div
                  className={`col-md-1 override-column ${
                    isTMDoc ? " hide-column" : ""
                  }`}
                >
                  <Checkbox
                    isChecked={serCostObj.project_management_hours_override}
                    name="project_management_hours_override"
                    disabled={
                      readOnly || serCostObj.hide_project_management_hours
                    }
                    title={
                      serCostObj.hide_project_management_hours
                        ? "Resource is marked to hide in the CR, to modify Hours uncheck the Hide option"
                        : "Project Management Hours will be calculated based on other Resource Hours, if Override is disabled"
                    }
                    onChange={(e) => this.handleChangeStaticOverride(e)}
                  />
                </div>
                {isTMDoc && (
                  <div className="col-md-1 override-column hide-column">
                    <Checkbox
                      isChecked={serCostObj.hide_project_management_hours}
                      name="hide_project_management_hours"
                      disabled={serCostObj.project_management_hours !== 0}
                      title={
                        serCostObj.project_management_hours !== 0
                          ? "Hide option is enabled when Total Hours has 0 value"
                          : "If this option is checked, the resource will be hidden in CR"
                      }
                      onChange={(e) => this.handleChangeStaticHide(e)}
                    />
                  </div>
                )}
                <Input
                  field={{
                    value: serCostObj.project_management_hourly_rate,
                    label: "",
                    type: "NUMBER",
                    isRequired: false,
                  }}
                  width={2}
                  minimumValue={"0"}
                  name={"project_management_hourly_rate"}
                  placeholder="Enter Rate"
                  disabled={readOnly}
                  onChange={(e) => this.handleChangeStatic(e)}
                  error={this.state.error.project_management_hourly_rate}
                />
                <div className="col-md-2 mapping-row-text">
                  {formatter.format(serviceCost.pmHoursCustomerCost)}
                </div>
                <div className="col-md-1 mapping-row-text">
                  {!isNil(serviceCost.pmHoursMarginPercent)
                    ? serviceCost.pmHoursMarginPercent + "%"
                    : "N/A"}
                </div>
              </div>
              {serCostObj.hourly_resources &&
                serCostObj.hourly_resources.map((el, idx) => (
                  <div className="col-md-12 mapping-row" key={idx}>
                    <EditButton
                      title="Edit Resource Description"
                      onClick={() =>
                        this.handleClickEditDescription(
                          el,
                          idx,
                          "Hourly Resource"
                        )
                      }
                    />
                    <Input
                      field={{
                        label: "",
                        type: "PICKLIST",
                        options: this.props.vendorOptions,
                        value: el.resource_id,
                        isRequired: false,
                      }}
                      width={3}
                      multi={false}
                      name="resource_id"
                      onChange={(e) => this.handleChangeStatic(e, idx)}
                      placeholder={`Select Resource`}
                      disabled={readOnly}
                      loading={this.props.isFetchingVendors}
                      error={
                        el.errorName &&
                        el.errorName.errorState === "error"
                          ? el.errorName
                          : {
                              errorState: "warning",
                              errorMessage: el.resource_description
                                ? el.resource_description
                                : "",
                            }
                      }
                    />
                    <Input
                      field={{
                        value: el.hours,
                        label: "",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={2}
                      name={"hours"}
                      minimumValue={"0"}
                      placeholder="Enter Hours"
                      onChange={(e) => this.handleChangeStatic(e, idx)}
                      className="phase-name"
                      title={
                        el.is_hidden
                          ? "Resource is marked to hide in the CR, to modify Hours uncheck the Hide option"
                          : undefined
                      }
                      disabled={readOnly || !el.override || el.is_hidden}
                    />

                    <div
                      className={`col-md-1 override-column ${
                        isTMDoc ? " hide-column" : ""
                      }`}
                    >
                      <Checkbox
                        isChecked={false}
                        disabled={true}
                        name="override"
                        title="Override option is disabled because phases are not available in CR"
                      />
                    </div>
                    {isTMDoc && (
                      <div className={`col-md-1 override-column hide-column`}>
                        <Checkbox
                          isChecked={el.is_hidden}
                          name="is_hidden"
                          disabled={el.hours !== 0}
                          title={
                            el.hours !== 0
                              ? "Hide option is enabled when Total Hours has 0 value"
                              : "If this option is checked, the resource will be hidden in CR"
                          }
                          onChange={(e) => this.handleChangeStaticHide(e, idx)}
                        />
                      </div>
                    )}
                    <Input
                      field={{
                        value: el.hourly_rate,
                        label: "",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={2}
                      name={"hourly_rate"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      disabled={readOnly}
                      onChange={(e) => this.handleChangeStatic(e, idx)}
                      error={el.errorRate}
                    />
                    <Input
                      field={{
                        value: el.hourly_cost,
                        label: "",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={2}
                      name={"hourly_cost"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      disabled={readOnly}
                      onChange={(e) => this.handleChangeStatic(e, idx)}
                      error={el.errorCost}
                    />
                    <div className="col-md-1 mapping-row-text">
                      {el.margin_percentage + "%"}
                    </div>
                    <SmallConfirmationBox
                      text="Hourly Resource"
                      showButton={true}
                      onClickOk={() => this.removeHourlyResource(idx)}
                      className={isTMDoc ? "remove-vendor-tm" : "remove-vendor"}
                    />
                  </div>
                ))}
              {!readOnly && (
                <div
                  className="add-new-row"
                  onClick={this.addNewHourlyResource}
                >
                  <span className="add-new-row-plus">+</span>
                  <span className="add-new-row-text">Add Hourly Resource</span>
                </div>
              )}
            </div>
          </div>
          {serCostObj && serCostObj.contractors && (
            <div className="contractors">
              <div className="sub-heading col-md-12">
                <div className="text">
                  {isTMDoc
                    ? "FEE BASED SERVICE OR PRODUCT"
                    : "FEE BASED CONTRACTORS"}
                </div>
              </div>
              {serCostObj.contractors.map((contractor, index) => {
                return (
                  <div className="contractor-row col-md-12" key={index}>
                    <EditButton
                      title="Edit Contractor Description"
                      onClick={() =>
                        this.handleClickEditDescription(
                          contractor,
                          index,
                          "Contractor"
                        )
                      }
                    />
                    <Input
                      field={{
                        value: contractor.vendor_id,
                        label: "Contractor Name",
                        type: "PICKLIST",
                        options: this.props.vendorOptions,
                        isRequired: false,
                      }}
                      width={3}
                      name={"vendor_id"}
                      placeholder="Select Contractor"
                      loading={this.props.isFetchingVendors}
                      disabled={readOnly}
                      onChange={(e) => this.handleChangeContractors(e, index)}
                      className="select-type"
                      error={
                        contractor.errorName &&
                        contractor.errorName.errorState ===
                          "error"
                          ? contractor.errorName
                          : {
                              errorState: "warning",
                              errorMessage: contractor.description
                                ? contractor.description
                                : "",
                            }
                      }
                    />
                    <Input
                      field={{
                        value: contractor.partner_cost,
                        label: "Partner Cost(In $)",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={2}
                      name={"partner_cost"}
                      minimumValue={"0"}
                      placeholder="Enter Rate"
                      disabled={readOnly}
                      onChange={(e) => this.handleChangeContractors(e, index)}
                      className="select-type"
                      error={contractor.errorRate}
                    />
                    <Input
                      field={{
                        value: contractor.margin_percentage,
                        label: "GP %",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={1}
                      minimumValue={"0"}
                      maximumValue={"100"}
                      name={"margin_percentage"}
                      placeholder="Enter %"
                      disabled={readOnly}
                      onChange={(e) => this.handleChangeContractors(e, index)}
                      className="select-type"
                    />
                    <Input
                      field={{
                        label: "Customer Cost(In $)",
                        value: formatter.format(
                          Number(getCustomerCost(contractor))
                        ),
                        type: "TEXT",
                        isRequired: false,
                      }}
                      width={2}
                      name={"margin_percentage"}
                      placeholder=" "
                      disabled={true}
                      className="disabled-calculations"
                      onChange={(e) => null}
                    />
                    <Input
                      field={{
                        label: "GP $",
                        value: formatter.format(
                          Number(getCustomerCost(contractor)) -
                            contractor.partner_cost
                        ),
                        type: "TEXT",
                        isRequired: false,
                      }}
                      width={2}
                      name="gross_profit"
                      disabled={true}
                      className="disabled-calculations"
                      onChange={(e) => null}
                    />
                    <Checkbox
                      isChecked={contractor.type === "Product"}
                      name="type"
                      disabled={readOnly}
                      onChange={(e) =>
                        this.handleChangeContractorsProduct(e, index)
                      }
                      className="product-type"
                    >
                      Is Product ?
                    </Checkbox>
                    {readOnly ||
                      (serCostObj.contractors.length > 1 && (
                        <SmallConfirmationBox
                          text="contractor"
                          onClickOk={() => this.removeContractor(index)}
                          showButton={true}
                        />
                      ))}
                  </div>
                );
              })}
              {!readOnly && (
                <div className="add-new-row" onClick={this.addNewContractor}>
                  <span className="add-new-row-plus">+</span>
                  <span className="add-new-row-text">Add Contractor</span>
                </div>
              )}
            </div>
          )}
          {serCostObj.travels && (
            <div className="travelscontractors">
              <div className="sub-heading col-md-12">
                <div className="text">
                  {" "}
                  {isTMDoc ? "TRAVEL ESTIMATE" : "TRAVEL"}
                </div>
              </div>
              {serCostObj.travels.map((travel, index) => {
                return (
                  <div className="contractor-row col-md-12" key={index}>
                    <Input
                      field={{
                        value: travel.description,
                        label: "Description",
                        type: "TEXT",
                        isRequired: false,
                      }}
                      width={4}
                      name={"description"}
                      placeholder="Enter description"
                      disabled={readOnly}
                      onChange={(e) => this.handleChangeTravels(e, index)}
                      error={travel.errorDescription}
                    />
                    <Input
                      field={{
                        value: travel.cost,
                        label: "Cost (in $)",
                        type: "NUMBER",
                        isRequired: false,
                      }}
                      width={3}
                      name={"cost"}
                      minimumValue={"0"}
                      disabled={readOnly}
                      placeholder="Enter Rate"
                      onChange={(e) => this.handleChangeTravels(e, index)}
                      error={travel.errorCost}
                    />
                    {readOnly ||
                      (serCostObj.travels.length > 1 && (
                        <SmallConfirmationBox
                          text="travel"
                          showButton={true}
                          onClickOk={() => this.removeTravel(index)}
                        />
                      ))}
                  </div>
                );
              })}
              {!readOnly && (
                <div className="add-new-row" onClick={this.addNewTravel}>
                  <span className="add-new-row-plus">+</span>
                  <span className="add-new-row-text">Add Travel Estimate</span>
                </div>
              )}
            </div>
          )}

          <div className="risk-control-section">
            <div className="sub-heading col-md-12">
              <div className="text">RISK CONTROL</div>
            </div>
            <div className="risk-variable-row">
              <Input
                field={{
                  value: serCostObj.default_ps_risk,
                  label: "Risk Variable (in %)",
                  type: "NUMBER",
                  isRequired: false,
                }}
                width={5}
                labelIcon="info"
                labelTitle={
                  "(i) - The risk variable controls the amount of risk added to the project. This variable controls the amount of risk budget added to the project. Based on the project variables add or reduce risk."
                }
                name={"default_ps_risk"}
                className="risk-variable-input"
                placeholder="Enter Risk Variable"
                onChange={this.handleChangeRiskVariable}
                disabled={
                  readOnly ||
                  !this.state.riskVarOverride ||
                  this.state.riskVarRemove
                }
                error={this.state.error.default_ps_risk}
              />
              <Checkbox
                isChecked={this.state.riskVarOverride}
                name="riskVarOverride"
                onChange={this.handleChangeRiskVarOverride}
                className="risk-variable-override"
                disabled={readOnly || this.state.riskVarRemove}
              >
                Override
              </Checkbox>
              <Checkbox
                isChecked={this.state.riskVarRemove}
                name="riskVarRemove"
                onChange={this.handleChangeRemoveRisk}
                disabled={readOnly}
              >
                Remove Risk Budget
              </Checkbox>
            </div>
          </div>

          <div className="calculations">
            <div className="sub-heading col-md-12">
              <div className="text">
                <span>SERVICE COST</span>
                <Checkbox
                  isChecked={serCostObj.is_zero_dollar_change_request}
                  name="is_zero_dollar_change_request"
                  disabled={readOnly}
                  onChange={(e) => this.handleChangeZeroDollar(e)}
                  className="zero-dollar"
                >
                  Zero Dollar Change Request
                </Checkbox>
              </div>
            </div>
            <div className="calculations-table risk-hour-table">
              <div className="calculations-table-header">
                <div className="calculations-header-title">Resource</div>
                <div className="calculations-header-title">Hours</div>
                <div className="calculations-header-title">Risk Hours</div>
                <div className="calculations-header-title">Total Hours</div>
                <div className="calculations-header-title">Customer Rate</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Engineer</div>
                  <div className="calculations-table-col">
                    {serCostObj.engineering_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.engRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.engineering_hours + serviceCost.engRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serCostObj.engineering_hourly_rate)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    After Hours Engineer
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.after_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.ahRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.after_hours + serviceCost.ahRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serCostObj.after_hours_rate)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Integration Technician
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.integration_technician_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.itRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.integration_technician_hours +
                      serviceCost.itRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serCostObj.integration_technician_hourly_rate
                    )}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Project Management
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.project_management_hours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.pmRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serCostObj.project_management_hours +
                      serviceCost.pmRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serCostObj.project_management_hourly_rate
                    )}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Hourly Contract Labor
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborTotalHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborTotalHours +
                      serviceCost.hourlyLaborRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborTotalRate)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Subtotal</div>
                  <div className="calculations-table-col">
                    {serviceCost.totalHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.totalRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.totalHours + serviceCost.totalRiskHours}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serCostObj.engineering_hourly_rate +
                        serCostObj.after_hours_rate +
                        serCostObj.integration_technician_hourly_rate +
                        serCostObj.project_management_hourly_rate +
                        serviceCost.hourlyLaborTotalRate
                    )}
                  </div>
                </div>
              </div>
            </div>
            <div className="calculations-table">
              <div className="calculations-table-header">
                <div className="calculations-header-title">Description</div>
                <div className="calculations-header-title">Revenue</div>
                <div className="calculations-header-title">Internal Cost</div>
                <div className="calculations-header-title">GP $</div>
                <div className="calculations-header-title">GP %</div>
              </div>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Engineering Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.engineeringHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.engineeringHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Engineering After Hours Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.afterHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.afterHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Integration Technician Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serviceCost.integrationTechnicianCustomerCost
                    )}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(
                      serviceCost.integrationTechnicianInternalCost
                    )}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.integrationTechnicianMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.integrationTechnicianMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Project Management Revenue
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.pmHoursMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.pmHoursMarginPercent + "%"}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Hourly Contract Labor
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborCustomerCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborInternalCost)}
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.hourlyLaborMargin)}
                  </div>
                  <div className="calculations-table-col">
                    {serviceCost.hourlyLaborMarginPercent + "%"}
                  </div>
                </div>
              </div>
            </div>
            {!isTMDoc && (
              <div className="calculations-table">
                <h4>Project Summary</h4>
                <div className="calculations-table-header">
                  <div className="calculations-header-title">Description</div>
                  <div className="calculations-header-title">Revenue</div>
                  <div className="calculations-header-title">Internal Cost</div>
                  <div className="calculations-header-title">GP $</div>
                  <div className="calculations-header-title">GP %</div>
                </div>
                <div className="calculations-table-body">
                  <div className="calculations-table-row">
                    <div className="calculations-table-col">
                      {isTMDoc
                        ? "Estimated Professional Services"
                        : "Professional Services"}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.proSerCustomerCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.proSerInternalCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.proSerMargin)}
                    </div>
                    <div className="calculations-table-col">
                      {serviceCost.proSerMarginPercent + "%"}
                    </div>
                  </div>
                  <div className="calculations-table-row">
                    <div className="calculations-table-col">Risk Budget</div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.riskBudgetCustomerCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.riskBudgetInternalCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.riskBudgetMargin)}
                    </div>
                    <div className="calculations-table-col">
                      {serviceCost.riskBudgetMarginPercent + "%"}
                    </div>
                  </div>
                  <div className="calculations-table-row">
                    <div className="calculations-table-col">
                      {" "}
                      {isTMDoc ? "Fee Based Service or Product" : "Contractors"}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.contractorCustomerCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.contractorInternalCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.contractorMargin)}
                    </div>
                    <div className="calculations-table-col">
                      {serviceCost.contractorMarginPercent + "%"}
                    </div>
                  </div>
                  <div className="calculations-table-row">
                    <div className="calculations-table-col">
                      {isTMDoc ? "Estimated Travel Budget" : "Travel Budget"}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.travelCustomerCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.travelInternalCost)}
                    </div>
                    <div className="calculations-table-col">
                      {formatter.format(serviceCost.travelMargin)}
                    </div>
                    <div className="calculations-table-col">
                      {serviceCost.travelMarginPercent + "%"}
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div className="calculations-table project-total-section">
              <h4>Project Total</h4>
              <div className="calculations-table-body">
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Internal Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalInternalCost)}
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">Total Margin</div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalMargin)}
                    <span className="total-margin-percent">
                      @ {serviceCost.totalMarginPercent}% GP
                    </span>
                  </div>
                </div>
                <div className="calculations-table-row">
                  <div className="calculations-table-col">
                    Total Customer Cost
                  </div>
                  <div className="calculations-table-col">
                    {formatter.format(serviceCost.totalCustomerCost)}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="notes col-md-12">
            <div className="sub-heading-notes col-md-12">
              <div className="text">NOTES</div>
            </div>
            <div className="col-md-12">
              <QuillEditorAcela
                scrollingContainer=".app-body"
                onChange={(e) => this.handleChangeNoteMarkDown(e)}
                value={serCostObj.notes}
                wrapperClass="ql-cr-notes"
                isDisabled={readOnly}
              />
            </div>
          </div>
        </div>
      </div>
    );
  };

  getProjectManagementRecommendedHours = (
    service_cost: IServiceCost
  ): number => {
    return parseInt(
      getRecommendedHours(
        service_cost.engineering_hours +
          service_cost.after_hours +
          service_cost.integration_technician_hours +
          (service_cost.hourly_resources
            ? commonFunctions.arrSum(
                service_cost.hourly_resources.map((el) => el.hours)
              )
            : 0)
      )
    );
  };

  handleChangeZeroDollar = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.is_zero_dollar_change_request =
      e.target.checked;
    this.setState(newState);
  };

  handleChangeStaticHide = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    if (!isNil(idx)) {
      newState.changeRequest.json_config.service_cost.hourly_resources[
        idx
      ].is_hidden = e.target.checked;
    } else {
      newState.changeRequest.json_config.service_cost[e.target.name] =
        e.target.checked;
    }
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  handleChangeStaticOverride = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost[e.target.name] =
      e.target.checked;

    if (
      e.target.name === "project_management_hours_override" &&
      !e.target.checked
    ) {
      newState.changeRequest.json_config.service_cost.project_management_hours = this.getProjectManagementRecommendedHours(
        this.state.changeRequest.json_config.service_cost
      );
    }
    newState.changeRequest.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.changeRequest.json_config.service_cost
    );
    this.setState(newState);
    this.debouncedCalculations();
  };

  handleClickEditDescription = (
    resource: IHourlyResource | IContractor,
    index: number,
    type: "Hourly Resource" | "Contractor"
  ) => {
    let mapping: IVendorAliasMapping;
    if (type === "Hourly Resource")
      mapping = {
        index,
        resource_type: type,
        vendor_crm_id: (resource as IHourlyResource).resource_id,
        vendor_name: (resource as IHourlyResource).resource_name,
        resource_description: (resource as IHourlyResource)
          .resource_description,
      };
    else
      mapping = {
        index,
        resource_type: type,
        vendor_crm_id: (resource as IContractor).vendor_id,
        vendor_name: (resource as IContractor).name,
        resource_description: (resource as IContractor).description,
      };

    this.setState({
      currentVendorMapping: mapping,
      showVendorMappingModal: true,
    });
  };

  handleChangeStatic = (
    e: React.ChangeEvent<HTMLInputElement>,
    idx?: number
  ) => {
    const newState = cloneDeep(this.state);
    const serviceCost = newState.changeRequest.json_config.service_cost;
    const value = this.getValues(
      e,
      [
        "hours",
        "after_hours",
        "engineering_hours",
        "integration_technician_hours",
        "project_management_hours",
      ].includes(e.target.name)
    );

    // For hourly resource, index will be passed
    if (!isNil(idx)) {
      let updatedResource: IHourlyResource;
      if (e.target.name === "resource_id") {
        const resource_id = value as number;
        const resource_name = this.props.vendorOptions.find(
          (el) => el.value === value
        ).label as string;
        const resource_description = this.state.vendorDescriptionMapping.has(
          resource_id
        )
          ? this.state.vendorDescriptionMapping.get(resource_id)
              .resource_description
          : "";
        updatedResource = {
          ...serviceCost.hourly_resources[idx],
          resource_id,
          resource_name,
          resource_description,
        };
      } else {
        updatedResource = getCalculatedHourlyResource({
          ...serviceCost.hourly_resources[idx],
          [e.target.name]: value,
        });
      }
      serviceCost.hourly_resources[idx] = updatedResource;
    } else serviceCost[e.target.name] = value;

    if (!serviceCost.project_management_hours_override)
      serviceCost.project_management_hours = this.getProjectManagementRecommendedHours(
        serviceCost
      );
    newState.changeRequest.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.changeRequest.json_config.service_cost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState);
    this.debouncedCalculations();
  };

  setDefaultStatic = () => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.engineering_hourly_rate = this.props.docSetting.engineering_hourly_rate;
    newState.changeRequest.json_config.service_cost.after_hours_rate = this.props.docSetting.after_hours_rate;
    newState.changeRequest.json_config.service_cost.integration_technician_hourly_rate = this.props.docSetting.integration_technician_hourly_rate;
    newState.changeRequest.json_config.service_cost.project_management_hourly_rate = this.props.docSetting.project_management_hourly_rate;
    newState.changeRequest.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.changeRequest.json_config.service_cost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doCRCalculations);
  };

  getValues = (
    e: React.ChangeEvent<HTMLInputElement>,
    integer: boolean = false
  ) => {
    const value =
      e.target.type === "number"
        ? round(Number(e.target.value), integer ? 0 : 2)
        : e.target.value;

    return value;
  };

  handleChangeContractors = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    (newState.unsaved as boolean) = true;
    const currentContractor =
      newState.changeRequest.json_config.service_cost.contractors[index];

    currentContractor[e.target.name] = this.getValues(e);

    if (e.target.name === "vendor_id") {
      const vendorId = Number(e.target.value);
      if (vendorId) {
        currentContractor.name = this.props.vendorOptions.find(
          (el) => el.value == vendorId
        ).label as string;
        currentContractor.description = this.state.vendorDescriptionMapping.has(
          vendorId
        )
          ? this.state.vendorDescriptionMapping.get(vendorId)
              .resource_description
          : "";
      } else {
        currentContractor.name = "";
        currentContractor.description = "";
      }
    }
    if (e.target.type === "number")
      currentContractor.customer_cost = getCustomerCost(currentContractor);

    this.setState(newState, () => {
      if (e.target.type === "number") this.debouncedCalculations();
    });
  };

  handleChangeContractorsProduct = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.contractors[index].type = e
      .target.checked
      ? "Product"
      : "Service";
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  addNewContractor = () => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.contractors.push({
      ...ChangeRequestDetails.EmptyContractor,
    });
    (newState.unsaved as boolean) = true;

    this.setState(newState);
  };

  removeContractor = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.contractors.splice(idx, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doCRCalculations);
  };

  addNewTravel = () => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.travels.push({
      cost: 0,
      description: "",
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeTravel = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.travels.splice(idx, 1);
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doCRCalculations);
  };

  addNewHourlyResource = () => {
    const newState = cloneDeep(this.state);
    if (!newState.changeRequest.json_config.service_cost.hourly_resources)
      newState.changeRequest.json_config.service_cost.hourly_resources = [];
    newState.changeRequest.json_config.service_cost.hourly_resources.push({
      ...ChangeRequestDetails.EmptyHourlyResource,
    });
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  removeHourlyResource = (idx: number) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.hourly_resources.splice(
      idx,
      1
    );
    newState.changeRequest.json_config.service_cost.total_hours = this.calculateTotalHours(
      newState.changeRequest.json_config.service_cost
    );
    (newState.unsaved as boolean) = true;
    this.setState(newState, this.doCRCalculations);
  };

  handleChangeTravels = (
    e: React.ChangeEvent<HTMLInputElement>,
    index: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.travels[index][
      e.target.name
    ] = this.getValues(e);
    this.setState(newState);
    if (e.target.type === "number") this.debouncedCalculations();
  };

  handleChangeRiskVariable = (e: React.ChangeEvent<HTMLInputElement>) => {
    const newRiskValue: number = Number(e.target.value);
    const changeRequest = cloneDeep(this.state.changeRequest);
    const error = cloneDeep(this.state.error);
    changeRequest.json_config.service_cost.default_ps_risk = newRiskValue;
    error.default_ps_risk =
      newRiskValue < this.props.docSetting.risk_low_watermark * 100
        ? {
            errorState: "error",
            errorMessage: `Risk variable should not go below ${this.props
              .docSetting.risk_low_watermark * 100}% (Risk Low Watermark)`,
          }
        : { ...ChangeRequestDetails.emptyErrorState };
    this.setState({ changeRequest, error }, this.doCRCalculations);
  };

  handleChangeRiskVarOverride = (e: React.ChangeEvent<HTMLInputElement>) => {
    const changeRequest = cloneDeep(this.state.changeRequest);
    if (!e.target.checked) {
      changeRequest.json_config.service_cost.default_ps_risk =
        this.props.docSetting.default_ps_risk * 100;
    }
    this.setState(
      (prevState) => ({
        changeRequest,
        riskVarOverride: e.target.checked,
        error: {
          ...prevState.error,
          error: { ...ChangeRequestDetails.emptyErrorState },
        },
      }),
      this.doCRCalculations
    );
  };

  handleChangeRemoveRisk = (e: React.ChangeEvent<HTMLInputElement>) => {
    const changeRequest = cloneDeep(this.state.changeRequest);
    changeRequest.json_config.service_cost.default_ps_risk = !e.target.checked
      ? this.props.docSetting.default_ps_risk * 100
      : 0;
    this.setState(
      {
        changeRequest,
        riskVarRemove: e.target.checked,
      },
      this.doCRCalculations
    );
  };

  handleChangeNoteMarkDown = (html: string) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config.service_cost.notes = html;
    this.setState(newState);
  };

  checkValidaBoards = () => {
    const error = this.getEmptyState().error;

    const changeRequest = this.state.changeRequest.json_config;
    let isValid = true;
    const isCollapsed = Object.keys(changeRequest).map((key) => false);
    const errorsLabel = {};
    const errorsValue = {};

    if (
      !this.state.changeRequest.name ||
      this.state.changeRequest.name.trim().length === 0
    ) {
      error.name.errorState = "error";
      error.name.errorMessage = "Enter a valid template name";

      isValid = false;
    } else if (this.state.changeRequest.name.length > 300) {
      error.name.errorState = "error";
      error.name.errorMessage = "name should be less than 300 chars.";

      isValid = false;
    }
    if (!this.state.changeRequest.requested_by) {
      error.requested_by.errorState = "error";
      error.requested_by.errorMessage = "Please select user";
      isValid = false;
    }
    if (!this.state.changeRequest.change_request_type) {
      error.change_request_type.errorState = "error";
      error.change_request_type.errorMessage =
        "Please select the type of change request";
      isValid = false;
    }

    if (changeRequest) {
      changeRequest.service_cost.contractors.forEach((contractor) => {
        contractor.errorName = commonFunctions.getErrorState();
        contractor.errorRate = commonFunctions.getErrorState();
      });

      changeRequest.service_cost.contractors.forEach((contractor) => {
        if (
          changeRequest.service_cost.contractors.length > 1 ||
          contractor.vendor_id ||
          contractor.partner_cost > 0
        ) {
          if (!contractor.vendor_id) {
            contractor.errorName = commonFunctions.getErrorState(
              "Please select a contractor"
            );
            isValid = false;
          }
          if (contractor.partner_cost === 0) {
            contractor.errorRate = commonFunctions.getErrorState(
              "Positive No. Required"
            );
            isValid = false;
          }
        }
      });

      changeRequest.service_cost.travels &&
        changeRequest.service_cost.travels.map((travel) => {
          travel.errorCost = commonFunctions.getErrorState();
          travel.errorDescription = commonFunctions.getErrorState();
        });

      changeRequest.service_cost.travels &&
        changeRequest.service_cost.travels.map((travel) => {
          if (
            changeRequest.service_cost.travels.length > 1 ||
            travel.description.trim() ||
            travel.cost > 0
          ) {
            if (travel.description.trim() === "") {
              travel.errorDescription = commonFunctions.getErrorState(
                "Please enter description"
              );
              isValid = false;
            }
            if (travel.cost === 0) {
              travel.errorCost = commonFunctions.getErrorState(
                "Please enter cost"
              );
              isValid = false;
            }
          }
        });

      if (changeRequest.service_cost.hourly_resources) {
        changeRequest.service_cost.hourly_resources.forEach((resource) => {
          resource.errorName = commonFunctions.getErrorState();
          resource.errorRate = commonFunctions.getErrorState();
          resource.errorCost = commonFunctions.getErrorState();
        });

        let uniqueHourlyResources: Set<string> = new Set();
        changeRequest.service_cost.hourly_resources.forEach((resource) => {
          const resourceIdentifier = resource.resource_description
            ? resource.resource_description
            : resource.resource_name;
          if (!resource.resource_id) {
            resource.errorName = commonFunctions.getErrorState(
              "Please select resource"
            );
            isValid = false;
          }
          if (uniqueHourlyResources.has(resourceIdentifier)) {
            resource.errorName = commonFunctions.getErrorState(
              "Duplicate resource"
            );
            isValid = false;
          }
          if (resource.hourly_rate === 0) {
            resource.errorRate = commonFunctions.getErrorState("Required");
            isValid = false;
          }
          if (resource.hourly_cost === 0) {
            resource.errorCost = commonFunctions.getErrorState("Required");
            isValid = false;
          }
          if (resourceIdentifier) uniqueHourlyResources.add(resourceIdentifier);
        });
      }
      if (
        !this.state.riskVarRemove &&
        changeRequest.service_cost.default_ps_risk <
          this.props.docSetting.risk_low_watermark * 100
      ) {
        error.default_ps_risk = {
          errorState: "error",
          errorMessage: `Risk variable should not go below ${this.props
            .docSetting.risk_low_watermark * 100}% (Risk Low Watermark)`,
        };
        isValid = false;
      }

      let count = 1;

      Object.keys(changeRequest)
        .filter((el) => el !== "service_cost")
        .sort((a, b) => changeRequest[a].ordering - changeRequest[b].ordering)
        .map((key, index) => {
          const boardContainerData = changeRequest[key];
          {
            Object.keys(boardContainerData)
              .sort(
                (a, b) =>
                  ((changeRequest[key][a] && changeRequest[key][a].ordering) ||
                    0) -
                  ((changeRequest[key][b] && changeRequest[key][b].ordering) ||
                    0)
              )
              .map((k) => {
                if (
                  k !== "section_label" &&
                  k !== "ordering" &&
                  k !== "visible_in" &&
                  k !== "default_hidden"
                ) {
                  const hasOptions =
                    typeof boardContainerData[k] === "object" &&
                    Object.keys(boardContainerData[k]).findIndex(
                      (v) => v === "options"
                    ) !== -1;
                  if (
                    boardContainerData[k] &&
                    boardContainerData[k].type !== "TEXTBOX" &&
                    boardContainerData[k] &&
                    boardContainerData[k].type !== "MARKDOWN"
                  ) {
                    if (hasOptions) {
                      if (boardContainerData[k].is_required) {
                        if (changeRequest[key] && changeRequest[key][k]) {
                          if (
                            !changeRequest[key][k].options.reduce(
                              (prev, cur) => prev || cur.active,
                              false
                            )
                          ) {
                            isValid = false;
                            isCollapsed[index] = true;
                            errorsValue[count] = {
                              errorState: "error",
                              errorMessage: `Required`,
                            };
                          }
                        } else {
                          isValid = false;
                        }
                      }
                    } else {
                      Object.keys(boardContainerData[k]).map((l, j) => {
                        if (boardContainerData[k][l].is_required) {
                          if (
                            changeRequest[key] &&
                            changeRequest[key][k] &&
                            changeRequest[key][k][l]
                          ) {
                            if (changeRequest[key][k][l].length === 0) {
                              isValid = false;
                            }
                          } else {
                            isValid = false;
                          }
                        }
                      });
                    }
                  } else if (
                    boardContainerData[k] &&
                    boardContainerData[k].type === "MARKDOWN" &&
                    boardContainerData[k].is_required
                  ) {
                    if (changeRequest[key] && changeRequest[key][k]) {
                      if (
                        commonFunctions.isEditorEmpty(
                          changeRequest[key][k].value
                        )
                      ) {
                        isValid = false;
                        isCollapsed[index] = true;
                        errorsValue[count] = {
                          errorState: "error",
                          errorMessage: `Required`,
                        };
                      }
                    } else {
                      isValid = false;
                    }
                  } else {
                    if (
                      boardContainerData[k] &&
                      boardContainerData[k].is_required
                    ) {
                      if (changeRequest[key] && changeRequest[key][k]) {
                        if (
                          !changeRequest[key][k].value ||
                          (changeRequest[key][k].value &&
                            changeRequest[key][k].value.length === 0)
                        ) {
                          isValid = false;
                          isCollapsed[index] = true;
                          errorsValue[count] = {
                            errorState: "error",
                            errorMessage: `Required`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  }
                  if (boardContainerData[k]) {
                    if (changeRequest[key] && changeRequest[key][k]) {
                      if (changeRequest[key][k].label === "") {
                        isValid = false;
                        isCollapsed[index] = true;
                        errorsLabel[count] = {
                          errorState: "error",
                          errorMessage: `label is required`,
                        };
                      }
                    } else {
                      isValid = false;
                    }
                  }

                  if (
                    boardContainerData[k] &&
                    boardContainerData[k].input_type === "FLOAT"
                  ) {
                    if (boardContainerData[k].is_required) {
                      if (changeRequest[key] && changeRequest[key][k]) {
                        if (
                          changeRequest[key][k].value.length > 0 &&
                          !AppValidators.isValidPositiveFloat(
                            changeRequest[key][k].value
                          )
                        ) {
                          isValid = false;
                          isCollapsed[index] = true;
                          errorsValue[count] = {
                            errorState: "error",

                            errorMessage: `Please enter numeric values only`,
                          };
                        }
                      } else {
                        isValid = false;
                      }
                    }
                  }
                  if (
                    boardContainerData.sections &&
                    boardContainerData.sections.length > 0
                  ) {
                    boardContainerData.sections.map((keyS, indexS) => {
                      Object.keys(keyS).map((secKey, x) => {
                        if (keyS[secKey].is_required) {
                          delete changeRequest[key].sections[indexS][secKey]
                            .error;
                        }
                      });
                    });
                  }
                  if (
                    boardContainerData.sections &&
                    boardContainerData.sections.length > 0
                  ) {
                    boardContainerData.sections.map((keyS, indexS) => {
                      Object.keys(keyS).map((secKey, x) => {
                        if (keyS[secKey].is_required) {
                          if (keyS[secKey].value === "") {
                            isValid = false;
                            isCollapsed[index] = true;
                            (changeRequest[key].sections[indexS][secKey]
                              .error as any) = {
                              errorState: "error",
                              errorMessage: `is required`,
                            };
                          }
                        }
                      });
                    });
                  }
                  count++;
                }
              });
          }
        });
    }

    this.setState((prevState) => ({
      error,
      errorsLabel,
      errorsValue,
      isValid,
      isCollapsed,
      showError: !isValid,
      changeRequest: {
        ...prevState.changeRequest,
        json_config: changeRequest,
      },
    }));

    return isValid;
  };

  setValidationErrors = (errorList: object) => {
    const newState: IChangeRequestDetailsState = cloneDeep(this.state);
    this.setState(commonFunctions.errorStateHandle(errorList, newState));
  };

  closeVendorMappingModal = (
    save: boolean,
    vendorMapping?: IVendorAliasMapping
  ) => {
    const newState = cloneDeep(this.state);
    if (save) {
      if (vendorMapping.resource_type === "Hourly Resource") {
        // Update the resource description for hourly resource
        const prevResource = this.state.changeRequest.json_config.service_cost
          .hourly_resources[vendorMapping.index];
        newState.changeRequest.json_config.service_cost.hourly_resources[
          vendorMapping.index
        ] = {
          ...ChangeRequestDetails.EmptyHourlyResource,
          resource_id: prevResource.resource_id,
          resource_name: prevResource.resource_name,
          resource_description: vendorMapping.resource_description,
        };
      } else {
        // Update description for contractor
        newState.changeRequest.json_config.service_cost.contractors[
          vendorMapping.index
        ].description = vendorMapping.resource_description;
      }
      (newState.unsaved as boolean) = true;
    }
    (newState.showVendorMappingModal as boolean) = false;
    (newState.currentVendorMapping as IVendorAliasMapping) = {
      vendor_name: "",
      resource_type: null,
      vendor_crm_id: null,
      resource_description: "",
    };

    this.setState(newState, this.doCRCalculations);
  };

  handleChangeCheckBox = (
    e: React.ChangeEvent<HTMLInputElement>,
    key: string,
    field: string,
    type: string,
    optionIndex: number
  ) => {
    const newState = cloneDeep(this.state);
    newState.changeRequest.json_config[key][field][type][optionIndex].active =
      e.target.checked;
    (newState.unsaved as boolean) = true;
    this.setState(newState);
  };

  previewDoc = () => {
    if (this.checkValidaBoards()) {
      this.setState({ loading: true });
      const changeRequest = cloneDeep(this.state.changeRequest);
      const json_config = changeRequest.json_config;
      if (changeRequest && json_config && json_config.service_cost) {
        json_config.service_cost.customer_cost = this.state.sowCalculations.totalCustomerCost;
        json_config.service_cost.internal_cost = this.state.sowCalculations.totalInternalCost;
      }
      if (
        json_config.change_affects &&
        json_config.change_affects[CRConfigSectionKeys.change_affects]
      ) {
        json_config.change_affects[
          CRConfigSectionKeys.change_affects
        ].value = json_config.change_affects[
          CRConfigSectionKeys.change_affects
        ].options
          .filter((op) => op.active === true)
          .map((op) => op.label);
      }
      json_config.service_cost.default_ps_risk /= 100;
      changeRequest.doc_type = "Change Request";
      changeRequest.category = this.state.categories;
      changeRequest.user = changeRequest.requested_by;
      changeRequest.project = this.state.project;
      changeRequest.customer =
        this.state.project && this.state.project.customer_id;
      changeRequest.project = this.state.project.id;
      this.props.previewSOW(changeRequest).then((a) => {
        if (a.type === CREATE_SOW_SUCCESS) {
          this.setState({
            openPreview: true,
            previewHTML: a.response,
            loading: false,
          });
        }
        if (a.type === CREATE_SOW_FAILURE) {
          this.setValidationErrors(a.errorList.data);
          this.setState({ loading: false });
        }
      });
    }
  };

  toggleOpenPreview = () => {
    this.setState({
      openPreview: !this.state.openPreview,
      previewHTML: null,
    });
  };

  render() {
    const project = this.state.project;
    const readOnly = this.state.changeRequest.status === "Approved";
    const existingCR: boolean =
      this.props.match.params.ChangeRequestID &&
      this.props.match.params.ChangeRequestID !== "0";

    return (
      <div className="project-details meeting-details create-change-request">
        {this.state.loading && (
          <div className="loader">
            <Spinner show={true} />
          </div>
        )}
        <div className="header">
          <div className="left">
            <div className="meeting-name">{`${
              existingCR ? "Edit" : "New"
            } Change Request`}</div>
            <div className="separator" />
            <div className="project-name">
              {project.title} - {project.customer}
            </div>
          </div>
          <div className="right">
            {!this.state.view && (
              <Checkbox
                isChecked={this.state.sendEmail}
                name="sendEmail"
                disabled={readOnly}
                onChange={(e) => {
                  this.setState({ sendEmail: e.target.checked });
                }}
              >
                Send to Requested by and Account Manager
              </Checkbox>
            )}
            {!this.state.loading && (
              <SquareButton
                content="Preview"
                onClick={this.previewDoc}
                className="preview-sow-doc"
                bsStyle={"default"}
              />
            )}
            {!this.state.view && !readOnly && (
              <SquareButton
                content={this.state.changeRequest.id ? "Update" : "Save"}
                bsStyle={"primary"}
                onClick={this.onSaveClick}
              />
            )}
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.props.history.goBack}
            />
          </div>
        </div>
        {this.state.showError && (
          <div className="board-error">Please enter all required fields.</div>
        )}
        <div className="first-row">
          <div className="action-second status-actions-row">
            <div className="change-request-new">
              <div className="change-rquest-static">
                <Input
                  field={{
                    label: "Name",
                    type: "TEXT",
                    value: this.state.changeRequest.name,
                    isRequired: true,
                  }}
                  width={8}
                  labelIcon={"info"}
                  name="name"
                  onChange={(e) => this.handleChangeChangeRequest(e)}
                  placeholder={`Enter name`}
                  disabled={this.state.view || readOnly}
                  error={this.state.error.name}
                />
                <Input
                  field={{
                    label: "Type",
                    type: "PICKLIST",
                    value: this.state.changeRequest.change_request_type,
                    isRequired: true,
                    options: ["Fixed Fee", "T & M"],
                  }}
                  width={4}
                  labelIcon={"info"}
                  name="change_request_type"
                  onChange={(e) => this.handleChangeChangeRequest(e)}
                  placeholder={`Select Type`}
                  disabled={this.state.view || readOnly}
                  error={this.state.error.change_request_type}
                />
                <Input
                  field={{
                    label: "Requested By",
                    type: "PICKLIST",
                    value: this.state.changeRequest.requested_by,
                    isRequired: true,
                    options:
                      this.state.projectCustomerContacts &&
                      this.state.projectCustomerContacts
                        .filter((user) => user.user_id)
                        .map((user, i) => ({
                          value: user.user_id,
                          label: (
                            <div key={i} className="label-user-dropdown">
                              <div
                                style={{
                                  backgroundImage: `url(${
                                    user && user.profile_url
                                      ? `${user.profile_url}`
                                      : "/assets/icons/user-filled-shape.svg"
                                  })`,
                                }}
                                className="pm-image-circle"
                              />
                              <div className="full-name" title={user.name}>
                                {user.name}
                              </div>
                            </div>
                          ),
                        })),
                  }}
                  width={6}
                  labelIcon={"info"}
                  name="requested_by"
                  onChange={(e) => this.handleChangeChangeRequest(e)}
                  placeholder={"Select User"}
                  className={`image-user-dropdown`}
                  disabled={readOnly}
                  error={this.state.error.requested_by}
                  loading={this.state.contactLoading}
                />

                <Input
                  field={{
                    label: "Assigned to",
                    type: "PICKLIST",
                    value: this.state.changeRequest.assigned_to,
                    isRequired: false,
                    options:
                      this.state.usersList &&
                      this.state.usersList.map((user, i) => ({
                        value: user.id,
                        label: (
                          <div key={i} className="label-user-dropdown">
                            <div
                              style={{
                                backgroundImage: `url(${
                                  user && user.profile_url
                                    ? `${user.profile_url}`
                                    : "/assets/icons/user-filled-shape.svg"
                                })`,
                              }}
                              className="pm-image-circle"
                            />
                            <div className="full-name" title={user.name}>
                              {user.name}
                            </div>
                          </div>
                        ),
                      })),
                  }}
                  width={6}
                  labelIcon={"info"}
                  name="assigned_to"
                  onChange={(e) => this.handleChangeChangeRequest(e)}
                  placeholder={"Select User"}
                  className={`image-user-dropdown`}
                  disabled={readOnly}
                  error={this.state.error.assigned_to}
                  loading={this.state.userLoading}
                />
              </div>
              {this.state.changeRequest.json_config &&
                this.renderTemplateByJson(this.state.changeRequest.json_config)}
              {this.state.changeRequest.json_config &&
                this.props.docSetting &&
                this.renderStaticFields()}
            </div>
          </div>
          <div className="status-overall-accordians">
            <Accordian label={"Project Details"} isOpen={true}>
              <div className="details-box">
                <div className="status-overall">
                  <div className="action">
                    <div className="status-text">Overall Status:</div>
                    <div className="field-section color-preview" title={""}>
                      <div
                        style={{
                          backgroundColor: project.overall_status_color,
                          borderColor: project.overall_status_color,
                        }}
                        className="left-column"
                      />
                      <div
                        style={{
                          background: `${getConvertedColorWithOpacity(
                            project.overall_status_color
                          )}`,
                        }}
                        className="text"
                      >
                        {project.overall_status}
                      </div>
                    </div>
                  </div>
                </div>
                <div className="label-details">Project Type</div>
                {
                  <div className="project-type">
                    <div className="label-content">{project.type}</div>
                  </div>
                }
              </div>
              <div className="details-box">
                <div className="label-details">Customer Contacts</div>
                {this.state.projectCustomerContacts.map((contact, i) => (
                  <div key={i} className="contact-container">
                    <div className={`label-content ${contact.contact_crm_id}`}>
                      <UsershortInfo
                        name={contact.name}
                        url={contact.profile_pic}
                      />
                    </div>
                    {
                      <div className="label-sub-content">
                        {contact.project_role}
                      </div>
                    }
                  </div>
                ))}
              </div>
              <div className="details-box">
                <div className="label-details">Project Manager</div>
                <div className="label-content">
                  <UsershortInfo
                    name={project.project_manager}
                    url={project.project_manager_profile_pic}
                  />
                </div>
              </div>
              <div className="details-box">
                <div className="label-details">Engineer Name</div>
                {this.state.projectEngineers &&
                  this.state.projectEngineers.length === 0 && (
                    <div className="label-content">-</div>
                  )}
                {this.state.projectEngineers &&
                  this.state.projectEngineers.map((engineer, i) => (
                    <div key={i} className="label-content">
                      <UsershortInfo
                        name={`${engineer.first_name} ${engineer.last_name}`}
                        url={engineer.profile_url}
                      />
                    </div>
                  ))}
              </div>
              <div className="details-box">
                <div className="label-details">Customer</div>
                <div className="label-content">{project.customer}</div>
              </div>
              <div className="details-box">
                <div className="label-details">Account Manager</div>
                <div className="label-content">
                  {(this.state.accountManager &&
                    this.state.accountManager.id && (
                      <>
                        <UsershortInfo
                          name={`${this.state.accountManager.first_name} ${this.state.accountManager.last_name}`}
                          url={this.state.accountManager.profile_url}
                        />
                      </>
                    )) ||
                    "-"}
                </div>
              </div>
            </Accordian>
          </div>
        </div>
        <VendorMappingModal
          show={this.state.showVendorMappingModal}
          closeModal={this.closeVendorMappingModal}
          mapping={this.state.currentVendorMapping}
        />
        <AddVendor
          show={this.state.showVendorModal}
          onClose={() => this.toggleVendorModal(false)}
          onSubmit={() => this.onSubmitVendorModal()}
        />
        <PDFViewer
          show={this.state.openPreview}
          onClose={this.toggleOpenPreview}
          titleElement={`View SOW Preview`}
          previewHTML={this.state.previewHTML}
          footerElement={
            <SquareButton
              content="Close"
              bsStyle={"default"}
              onClick={this.toggleOpenPreview}
            />
          }
          className=""
        />
        <PromptUnsaved
          when={this.state.unsaved}
          navigate={(path) => this.props.history.push(path)}
          shouldBlockNavigation={(location) => {
            if (this.state.unsaved) {
              return true;
            }
            return false;
          }}
          onSaveClick={(e) => this.onSaveClick()}
        />
      </div>
    );
  }
}

const mapStateToProps = (state: IReduxStore) => ({
  baseTemplates: state.sow.baseTemplates,
  docSetting: state.setting.docSetting,
  vendorOptions: state.sow.vendorOptions,
  vendorMapping: state.sow.vendorMapping,
  isFetchingVendors: state.sow.isFetchingVendors,
});

const mapDispatchToProps = (dispatch: any) => ({
  getProjectDetails: (projectID: number) =>
    dispatch(getProjectDetails(projectID)),
  getVendorsList: () => dispatch(getVendorsList()),
  getVendorMapping: () => dispatch(getVendorMappingList()),
  createChangeRequests: (projectID: number, data: IChangeRequest) =>
    dispatch(createChangeRequests(projectID, data)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  getProjectCustomerContactsByID: (projectID: number) =>
    dispatch(getProjectCustomerContactsByID(projectID)),
  getProjectEngineersByID: (projectID: number) =>
    dispatch(getProjectEngineersByID(projectID)),
  getActionItemList: (projectId: number, api: string) =>
    dispatch(getActionItemList(projectId, api)),
  getChangeRequests: (id: number, changeRequestID: number) =>
    dispatch(getChangeRequests(id, changeRequestID)),
  getBaseTemplate: () => dispatch(getBaseTemplate()),
  fetchSOWDOCSetting: () => dispatch(fetchSOWDOCSetting()),
  previewSOW: (changeRequest: IChangeRequest) =>
    dispatch(previewSOW(changeRequest)),
  getCategoryList: () => dispatch(getCategoryList()),
  pmoCommonAPI: (api: string, type: string) =>
    dispatch(pmoCommonAPI(api, type)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChangeRequestDetails);
