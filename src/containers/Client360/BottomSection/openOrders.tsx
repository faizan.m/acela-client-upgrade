import React from "react";
import InfiniteListing from "../../../components/InfiniteList/infiniteList";

interface OpenOrdersProps {
  customerCrmId: number;
}

const OpenOrders: React.FC<OpenOrdersProps> = (props) => {
  const columns: IColumnInfinite<{
    id: number;
    po_number_id: string;
    vendor_company_name: string;
    purchase_order_status_name: string;
    customer_po: string;
    opportunity_name: string;
    customer_company_name: string;
    customer_company_id: number;
  }>[] = [
    {
      name: "Customer PO",
      id: "customer_po",
      ordering: "customer_po",
      className: "customer-po-width",
    },
    {
      name: "Opportunity",
      id: "opportunity_name",
      ordering: "opportunity_name",
    },
    {
      name: "Vendor",
      id: "vendor_company_name",
      ordering: "vendor_company_name",
      className: "width-20",
    },
    {
      name: "Status",
      id: "purchase_order_status_name",
      className: "width-15",
    },
  ];

  return (
    <div className="open-orders-outer-div">
      <h3 className="client-bottom-subheading">Open Orders</h3>
      <div className="client-bottom-table client-table">
        <InfiniteListing
          url={`providers/client360/open-purchase-orders?customer_crm_id=${props.customerCrmId}`}
          key={props.customerCrmId}
          columns={columns}
          showSearch={false}
          id="OpenOrders"
          height={"calc(100% - 23px)"}
          className="open-orders-listing"
        />
      </div>
    </div>
  );
};

export default OpenOrders;
