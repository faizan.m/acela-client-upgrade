import React, { useMemo } from "react";
import { connect } from "react-redux";
import ActiveProjects from "./activeProjects";
import ManagedTickets from "./managedTickets";
import OpenOrders from "./openOrders";

// can remove this (if unnecessary)
interface ClientServicesProps extends ICommonProps {
  customerId: number;
  customers: ICustomerShort[];
}

const ClientServices: React.FC<ClientServicesProps> = (props) => {
  const commonProps: ICommonProps = {
    match: props.match,
    history: props.history,
    location: props.location,
  };

  const customerCrmId: number = useMemo(
    () =>
      props.customers && props.customers.length
        ? props.customers.find((el) => el.id === props.customerId).crm_id
        : null,
    [props.customers, props.customerId]
  );

  return (
    <div className="client-bottom-row">
      <ActiveProjects {...commonProps} />
      {customerCrmId && (
        <>
          <ManagedTickets customerCrmId={customerCrmId} />
          <OpenOrders customerCrmId={customerCrmId} />
        </>
      )}
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customerId: Number(state.customer.customerId),
  customers: state.customer.customersShort,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(ClientServices);
