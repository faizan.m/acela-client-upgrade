import React, { useCallback, useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import {
  fetchManagedTickets,
  FETCH_MANAGED_TICKETS_SUCCESS,
  FETCH_MANAGED_TICKETS_FAILURE,
} from "../../../actions/customer";
import Spinner from "../../../components/Spinner";
import { gt } from "lodash";

interface ManagedTicketsProps {
  customerCrmId: number;
  fetchTickets: (customerCrmId: number) => Promise<any>;
}

interface IManagedTicket {
  id: number;
  board: string;
  owner: string;
  status: string;
  summary: string;
  board_id: number;
  owner_id: number;
  status_id: number;
  company_id: number;
  record_type: string;
  company_name: string;
  last_updated_on: string;
  owner_identifier: string;
}

const ManagedTickets: React.FC<ManagedTicketsProps> = (props) => {
  const [tickets, setTickets] = useState<IManagedTicket[]>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [ordering, setOrdering] = useState<string>("");

  const orderedTickets = useMemo(
    () =>
      tickets && ordering
        ? [...tickets].sort((a, b) => {
            let multiplier = ordering[0] === "-" ? -1 : 1;
            let orderKey =
              ordering[0] === "-" ? ordering.substring(1) : ordering;
            return multiplier * (gt(a[orderKey], b[orderKey]) ? 1 : -1);
          })
        : tickets,
    [tickets, ordering]
  );

  const orderData = (field: string) => {
    let orderBy = "";
    switch (ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;
      default:
        orderBy = field;
        break;
    }
    setOrdering(orderBy);
  };

  const getOrderClass = useCallback(
    (field: string) => {
      let currentClassName = "";
      switch (ordering) {
        case `-${field}`:
          currentClassName = "asc-order";
          break;
        case field:
          currentClassName = "desc-order";
          break;

        default:
          currentClassName = "";
          break;
      }
      return currentClassName;
    },
    [ordering]
  );

  useEffect(() => {
    if (props.customerCrmId) fetchTickets();
  }, [props.customerCrmId]);

  const fetchTickets = () => {
    setLoading(true);
    props
      .fetchTickets(props.customerCrmId)
      .then((action) => {
        if (action.type === FETCH_MANAGED_TICKETS_SUCCESS) {
          setTickets(action.response);
        } else if (action.type === FETCH_MANAGED_TICKETS_FAILURE) {
          setTickets([]);
        }
      })
      .finally(() => setLoading(false));
  };

  return (
    <div className="managed-tickets-outer-div">
      <h3 className="client-bottom-subheading">Managed Tickets</h3>
      <div className="client-bottom-table client-table">
        <div className="infinite-list-component">
          <div className="top-action">
            <div className="total-count">
              <span>Total : </span>
              {tickets && tickets.length ? tickets.length : 0}
            </div>
          </div>
          <div className="header">
            <div
              className={`cell ticket-no-width cursor-pointer ${getOrderClass(
                "id"
              )}`}
              onClick={() => orderData("id")}
            >
              Ticket #
            </div>
            <div
              className={`cell width-20 cursor-pointer ${getOrderClass(
                "status"
              )}`}
              onClick={() => orderData("status")}
            >
              Status
            </div>
            <div className="cell">Description</div>
          </div>
          <div
            id="ManagedTickets"
            style={{
              position: "relative",
              height: "calc(100% - 23px)",
              overflow: "auto",
            }}
          >
            {loading && (
              <div className="loader-list">
                <Spinner show={true} />
              </div>
            )}
            {orderedTickets && orderedTickets.length ? (
              <div
                className="infinite-scroll-component"
                style={{ overflow: "auto !important" }}
              >
                {orderedTickets.map((ticket, index) => (
                  <div
                    className={`row-panel ${index % 2 !== 0 ? "odd" : "even"}`}
                    key={`row-${index}`}
                  >
                    <div
                      className="cell ticket-no-width"
                      title={String(ticket.id)}
                    >
                      {ticket.id}
                    </div>
                    <div className="cell width-20" title={ticket.status}>
                      {ticket.status}
                    </div>
                    <div className="cell" title={ticket.summary}>
                      {ticket.summary}
                    </div>
                  </div>
                ))}
              </div>
            ) : (
              <div className="no-data-inf">No data available</div>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchTickets: (customerCrmId: number) =>
    dispatch(fetchManagedTickets(customerCrmId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ManagedTickets);
