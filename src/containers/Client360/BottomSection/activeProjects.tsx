import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { connect } from "react-redux";
import store from "../../../store";
import {
  FETCH_PROJECT_BOARD_LIST_SUCCESS,
  LAST_VISITED_PMO_SITE,
  fetchPMODashboardReview,
  getProjectBoardList,
} from "../../../actions/pmo";
import Spinner from "../../../components/Spinner";
import { getConvertedColorWithOpacity } from "../../../utils/CommonUtils";

interface ActiveProjectsProps extends ICommonProps {
  customerId: number;
  getProjectBoardList: () => Promise<any>;
  fetchPMODashboardReview: (
    params: IServerPaginationParams & IPMODashboardFilterParams
  ) => Promise<any>;
}

const EmptyPagination: IServerPaginationParams & IPMODashboardFilterParams = {
  page: 1,
  page_size: 5,
  ordering: "",
  search: "",
};

const ActiveProjects: React.FC<ActiveProjectsProps> = (props) => {
  const [nextPage, setNextPage] = useState<number>(1);
  const [noData, setNoData] = useState<boolean>(false);
  const [loading, setLoading] = useState<boolean>(false);
  const [projects, setProjects] = useState<IProjectReview[]>([]);
  const [openStatusIds, setOpenStatusIds] = useState<number[]>([]);
  const [pagination, setPagination] = useState<
    IServerPaginationParams & IPMODashboardFilterParams
  >({
    ...EmptyPagination,
  });

  useEffect(() => {
    fetchOpenStatusIds();
    return () => {
      const lastVisitedSite: string = props.location.pathname;
      store.dispatch({
        type: LAST_VISITED_PMO_SITE,
        response: lastVisitedSite,
      });
    };
  }, []);

  useEffect(() => {
    if (openStatusIds.length && props.customerId) fetchMoreData(true);
  }, [openStatusIds, props.customerId, pagination.ordering]);

  const fetchOpenStatusIds = () => {
    setLoading(true);
    props.getProjectBoardList().then((action) => {
      if (action.type === FETCH_PROJECT_BOARD_LIST_SUCCESS) {
        const statusList: number[] = action.response
          ? action.response
              .map((a) => a.statuses)
              .flat()
              .filter((el) => el.is_open)
              .map((el) => el.id)
          : [];
        setOpenStatusIds(Array.from(new Set(statusList)));
      }
    });
  };

  const getOrderClass = (field: string) => {
    let currentClassName = "";
    switch (pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "";
        break;
    }
    return currentClassName;
  };

  const fetchByOrder = (field: string) => {
    let orderBy = "";
    switch (pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    setPagination((prevState) => ({
      ...prevState,
      ordering: orderBy,
    }));
  };

  const fetchMoreData = (clearData: boolean = false) => {
    setLoading(true);
    const filterParams: IPMODashboardFilterParams = {
      customer: props.customerId,
      overall_status: openStatusIds.join(","),
    };
    const newPagination = { ...pagination, ...filterParams };
    if (clearData || nextPage) {
      let newParams: IServerPaginationParams & IPMODashboardFilterParams = {
        ...newPagination,
        page: clearData ? 1 : nextPage,
      };
      props.fetchPMODashboardReview(newParams).then((action) => {
        if (action.response) {
          let newProjects: IProjectReview[];
          if (clearData) {
            newProjects = [...action.response.results];
          } else {
            newProjects = [...projects, ...action.response.results];
          }

          setLoading(false);
          setPagination(newParams);
          setProjects(newProjects);
          setNoData(newProjects.length === 0);
          setNextPage(action.response.links.next_page_number);
        } else {
          setNoData(true);
          setLoading(false);
        }
      });
    }
  };

  return (
    <div className="active-projects-outer-div">
      <h3 className="client-bottom-subheading">Active Projects</h3>
      <div className="project-list">
        <Spinner show={loading} className="pmo-review-spinner" />
        <div
          id="scrollableDiv"
          style={{ height: "auto", maxHeight: "100%", overflow: "auto" }}
        >
          <InfiniteScroll
            dataLength={projects.length}
            next={fetchMoreData}
            hasMore={nextPage ? true : false}
            loader={<h4 className="no-data">Loading...</h4>}
            scrollableTarget="scrollableDiv"
          >
            <div className="project-list-table col-md-12">
              <div className={`header-dashboard`}>
                <div
                  className={`project-name ellipsis-text ${getOrderClass(
                    "title"
                  )}`}
                  onClick={() => fetchByOrder("title")}
                >
                  Project Name
                </div>
                <div
                  className={`pm ${getOrderClass("project_manager_name")}`}
                  onClick={() => fetchByOrder("project_manager_name")}
                >
                  PM
                </div>
                <div
                  className={`status ${getOrderClass("status")}`}
                  onClick={() => fetchByOrder("status")}
                >
                  Status
                </div>
              </div>
              {projects.map((project, index) => (
                <div className={`row-panel-dashboard`} key={index}>
                  <div
                    className="project-details"
                    onClick={() => {
                      props.history.push(`Projects/${project.id}`);
                    }}
                  >
                    <div
                      className="project-name"
                      style={{ borderColor: "#60c2e1" }}
                    >
                      <div
                        className="name-bold ellipsis-text"
                        title={`Project Name : ${project.title}`}
                      >
                        {project.title}
                      </div>
                    </div>
                    <div className="manager user-image-component">
                      <div
                        style={{
                          backgroundImage: `url(${
                            project.project_manager_pic
                              ? `${project.project_manager_pic}`
                              : "/assets/icons/user-filled-shape.svg"
                          })`,
                        }}
                        title={project.project_manager_name}
                        className="pm-image-circle"
                      />
                    </div>
                  </div>
                  <div
                    className="status color-preview-dashboard"
                    title={project.overall_status_title}
                  >
                    <div
                      style={{
                        backgroundColor: project.overall_status_color,
                        borderColor: project.overall_status_color,
                      }}
                      className="left-column"
                    />
                    <div
                      style={{
                        background: `${getConvertedColorWithOpacity(
                          project.overall_status_color
                        )}`,
                      }}
                      className="text"
                    >
                      {project.overall_status_title}
                    </div>
                  </div>
                </div>
              ))}
            </div>
            {!loading && nextPage && projects.length > 0 && (
              <div
                className="load-more"
                onClick={() => {
                  fetchMoreData(false);
                }}
              >
                Load more projects...
              </div>
            )}
          </InfiniteScroll>
          {noData && projects.length === 0 && (
            <div className="no-data">No Projects Available</div>
          )}
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customerId: Number(state.customer.customerId),
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchPMODashboardReview: (
    params: IServerPaginationParams & IPMODashboardFilterParams
  ) => dispatch(fetchPMODashboardReview(params)),
  getProjectBoardList: () => dispatch(getProjectBoardList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(ActiveProjects);
