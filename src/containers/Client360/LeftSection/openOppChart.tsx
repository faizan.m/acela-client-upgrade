import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Chart,
  Legend,
  Tooltip,
  ArcElement,
  DoughnutController,
} from "chart.js";
import { Doughnut } from "react-chartjs-2";
import { getOpenOppChart, GET_OPEN_OPP_SUCCESS } from "../../../actions/sow";
import Spinner from "../../../components/Spinner";
import { commonFunctions } from "../../../utils/commonFunctions";
import ChartDataLabels from "chartjs-plugin-datalabels";

// TODO: RPU - Check after react-select support is added
Chart.register(
  Legend,
  Tooltip,
  ArcElement,
  ChartDataLabels,
  DoughnutController
);

interface OpenOppChartProps {
  customerCrmId: number;
  getChartData: (customerCrmId: number) => Promise<any>;
}

// const options = {
//   plugins: {
//     datalabels: {
//       display: false,
//     },
//     legend: {
//       position: "bottom",
//       labels: {
//         boxWidth: 13,
//         fontSize: 11,
//       },
//     },
//   },
//   maintainAspectRatio: false,
//   layout: {
//     padding: {
//       left: 0,
//       right: 0,
//       top: 5,
//       bottom: 5,
//     },
//   },
//   cutoutPercentage: 80,
// };

interface IOpenOppChartData {
  stage_name: string;
  count: number;
}

const colorMap: string[] = [
  "#f0b65c",
  "#9e3d9a",
  "#7f7fb1",
  "#4ba2c1",
  "#e55b7a",
  "#fac64d",
  "#5b9950",
];

const OpenOppChart: React.FC<OpenOppChartProps> = (props) => {
  const [fetching, setFetching] = useState<boolean>(true);
  const [data, setData] = useState<IOpenOppChartData[]>([]);

  useEffect(() => {
    if (props.customerCrmId) {
      setFetching(true);
      props
        .getChartData(props.customerCrmId)
        .then((action) => {
          if (action.type === GET_OPEN_OPP_SUCCESS) {
            setData(action.response);
          }
        })
        .finally(() => setFetching(false));
    }
  }, [props.customerCrmId]);

  return (
    <div className="open-opp-chart-container">
      <h3 className={`graph-heading ${fetching ? "loading" : ""}`}>
        Open Opportunities
      </h3>
      {fetching && (
        <div className="loader">
          <Spinner show={true} className="opp-chart-loader" />
        </div>
      )}
      <div className={`dashboard-graph-img ${fetching ? "loading" : ""}`}>
        <Doughnut
          data={{
            labels: data.map((el) => el.stage_name),
            datasets: [
              {
                data: data.map((el) => el.count),
                backgroundColor: colorMap,
                hoverBackgroundColor: colorMap,
                borderWidth: 1,
              },
            ],
          }}
          options={{
            events: [
              "mousemove",
              "mouseout",
              "click",
              "touchstart",
              "touchmove",
            ],
            plugins: {
              datalabels: {
                display: false,
              },
              legend: {
                position: "bottom",
                labels: {
                  boxWidth: 13,
                  font: { size: 11 },
                },
              },
            },
            maintainAspectRatio: false,
            layout: {
              padding: {
                left: 0,
                right: 0,
                top: 5,
                bottom: 5,
              },
            },
            cutout: "80%",
          }}
        />
        <div className="donut-inner">
          <span>
            {data.length
              ? commonFunctions.arrSum(data.map((el) => el.count))
              : "No Data"}
          </span>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({});

const mapDispatchToProps = (dispatch: any) => ({
  getChartData: (customerCrmId: number) =>
    dispatch(getOpenOppChart(customerCrmId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OpenOppChart);
