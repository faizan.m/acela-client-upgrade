import React, { useEffect, useMemo, useState } from "react";
import { connect } from "react-redux";
import { split } from "lodash";
import {
  getSalesActivityStatusSetting,
  SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
} from "../../../actions/sales";
import InfiniteListing from "../../../components/InfiniteList/infiniteList";
import ActivityDetailsModal from "./activityDetailsModal";

interface OpenActivitiesProps {
  user: ISuperUser;
  customerId: number;
  getSalesActivityStatusSetting: () => Promise<any>;
}

const getAbbreviatedName = (name: string): string => {
  if (!name) return "-";
  let nameArr: string[] = split(name.trim(), " ");
  return nameArr.map((str) => str[0]).join("");
};

const OpenActivities: React.FC<OpenActivitiesProps> = (props) => {
  const [openModal, setOpenModal] = useState<boolean>(false);
  const [openStatusIds, setStatusIds] = useState<number[]>([]);
  const [refreshKey, setRefreshKey] = useState<number>(undefined);
  const [editActivity, setEditActivity] = useState<ISalesActivity>();
  const [fetchingStatus, setFetchingStatus] = useState<boolean>(true);
  const url: string = useMemo(
    () =>
      `providers/sales/sales-activities?status=${openStatusIds.join(
        ","
      )}&customer=${props.customerId}`,
    [openStatusIds, props.customerId]
  );

  const columns: IColumnInfinite<ISalesActivity>[] = [
    {
      name: "Description",
      id: "description",
      className: "activity-desc activity-cell-wrap",
      Cell: (activity) => {
        return (
          <div
            className="activity-internal-cell"
            title={activity.description}
            onClick={() => onEditClick(activity)}
          >
            {activity.description}
          </div>
        );
      },
    },
    {
      name: "Assigned",
      className: "activity-cell-wrap",
      Cell: (activity) => {
        return (
          <div
            className="activity-assigned activity-internal-cell"
            title={(activity.assigned_to as IUserProfilePic).name}
            onClick={() => onEditClick(activity)}
          >
            {getAbbreviatedName((activity.assigned_to as IUserProfilePic).name)}
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    fetchOpenStatuses();
  }, []);

  useEffect(() => {
    if (props.customerId) setRefreshKey(Math.random() * props.customerId);
  }, [props.customerId]);

  const onEditClick = (activity: ISalesActivity) => {
    setEditActivity(activity);
    setOpenModal(true);
  };

  const onClose = (saved?: boolean) => {
    setOpenModal(false);
    setEditActivity(undefined);
    if (saved) setRefreshKey(Math.random() * props.customerId);
  };

  const fetchOpenStatuses = () => {
    props
      .getSalesActivityStatusSetting()
      .then((action) => {
        if (action.type === SALES_ACTIVITY_STATUS_SETTING_SUCCESS) {
          // Get open statuses (ask to Saurabh about more info)
          const openStatuses: number[] = action.response
            .filter((val) => {
              return !val.is_closed;
            })
            .map((obj) => {
              return obj.id;
            });
          setStatusIds(openStatuses);
        }
      })
      .finally(() => setFetchingStatus(false));
  };

  return (
    <>
      {openModal && (
        <ActivityDetailsModal onClose={onClose} activity={editActivity} />
      )}
      <div className="client-left-table client-table">
        <div className="client-left-table-header">
          <h3>Open Activities</h3>
          <div
            className="add-activity-btn"
            title="Create New Activity"
            onClick={() => setOpenModal(true)}
          >
            +
          </div>
        </div>
        {fetchingStatus ? (
          <div className="loading-data">Loading...</div>
        ) : (
          <InfiniteListing
            url={url}
            key={refreshKey}
            columns={columns}
            showSearch={false}
            id="OpenActivities"
            height={"calc(100% - 23px)"}
            className="open-activity-listing"
          />
        )}
      </div>
    </>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  user: state.profile.user,
  customerId: Number(state.customer.customerId),
});

const mapDispatchToProps = (dispatch: any) => ({
  getSalesActivityStatusSetting: () =>
    dispatch(getSalesActivityStatusSetting()),
});

export default connect(mapStateToProps, mapDispatchToProps)(OpenActivities);
