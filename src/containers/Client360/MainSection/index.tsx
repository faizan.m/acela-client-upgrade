import React from "react";
import { connect } from "react-redux";
import HorizontalTabSlider, {
  Tab,
} from "../../../components/HorizontalTabSlider/HorizontalTabSlider";
import SowList from "../../SOW/Sow/sowList";
import NotesList from "../../SOW/Notes/NotesList";
import AgreementList from "../../SOW/Agreement/agreementList";
import Assets from "../../InventoryManagement/inventoryManagement";
import PurchaseHistory from "../../PurchaseOrder/purchaseOrderList";

interface Client360MainProps extends ICommonProps {
  customerId: number;
  lastVisitedTab: string;
}

const Client360Main: React.FC<Client360MainProps> = (props) => {
  const commonProps: ICommonProps = {
    match: props.match,
    history: props.history,
    location: props.location,
  };

  // Currently customer filter is not working for Agreement and Notes, also remove customer column from agreement and notes
  return (
    <div className="client-main-section-container">
      <HorizontalTabSlider defaultTab={props.lastVisitedTab}>
        <Tab title="Documents">
          <SowList customerId={props.customerId} {...commonProps} />
        </Tab>
        <Tab title="Agreements">
          <AgreementList customerId={props.customerId} {...commonProps} />
        </Tab>
        <Tab title="Purchase History">
          <PurchaseHistory {...commonProps} />
        </Tab>
        <Tab title="Assets">
          <Assets {...commonProps} />
        </Tab>
        <Tab title="Notes">
          <NotesList customerId={props.customerId} {...commonProps} />
        </Tab>
      </HorizontalTabSlider>
    </div>
  );
};

const mapStateToProps = (state: IReduxStore) => ({
  customerId: Number(state.customer.customerId),
  lastVisitedTab: state.customer.lastVisitedTab,
});

const mapDispatchToProps = (dispatch: any) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Client360Main);
