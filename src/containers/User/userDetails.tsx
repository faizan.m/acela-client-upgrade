import React from "react";

import SquareButton from "../../components/Button/button";
import ModalBase from "../../components/ModalBase/modalBase";
import { phoneNumberInFormat } from "../../utils/CalendarUtil";

import "./style.scss";

interface IUserDetailsProps {
  show: boolean;
  onClose: (e: any) => void;
  user: ISuperUser;
  title?: string;
  fetchUserForCustomer?: any;
  customerUsers?: ISuperUser[];
  customers?: ICustomerShort[];
  fetchTerritoryMembers?: any;
  terretoryMembers?: any;
}

interface IUserDetailsState {
  user: ISuperUser;
}

export default class UserDetails extends React.Component<
  IUserDetailsProps,
  IUserDetailsState
> {
  static defaultProps: IUserDetailsProps = {
    show: false,
    onClose: (e) => null,
    user: null,
    title: "User Details",
  };

  constructor(props: IUserDetailsProps) {
    super(props);

    this.state = {
      user: null,
    };
  }

  componentDidUpdate(prevProps: IUserDetailsProps) {
    if (this.props.show !== prevProps.show && this.props.user) {
      this.setState({
        user: this.props.user,
      });
      if (
        this.props.user &&
        this.props.user.profile &&
        this.props.user.profile.customer_instance_id
      ) {
        this.props.fetchUserForCustomer(
          this.props.user.profile.customer_instance_id
        );
      }
    }
  }

  componentDidMount() {
    this.props.fetchTerritoryMembers && this.props.fetchTerritoryMembers();
  }

  onClose = (e) => {
    this.props.onClose(e);
  };

  getTitle = () => {
    return this.props.title;
  };

  getCustomerName = (id) => {
    let user;
    user =
      this.props.customers &&
      this.props.customers.length > 0 &&
      this.props.customers.filter((cus) => cus.id === id);

    return user && user.length > 0 ? `${user[0].name}` : "N.A.";
  };

  getCustomerUserName = (id: number) => {
    let user: ISuperUser[];
    user =
      this.props.customerUsers &&
      this.props.customerUsers.length > 0 &&
      this.props.customerUsers.filter((cus) => cus.crm_id === id);

    return user && user.length > 0
      ? `${user[0].first_name} ${user[0].last_name}`
      : "N.A.";
  };

  getMemberUserName = (id: number) => {
    let user;
    user =
      this.props.terretoryMembers &&
      this.props.terretoryMembers.length > 0 &&
      this.props.terretoryMembers.find((cus) => cus.id === id);

    return user ? `${user.first_name} ${user.last_name}` : "N.A.";
  };

  getBody = () => {
    const { user } = this.state;

    if (user) {
      const profile = user.profile;

      return (
        <div className="user-details-modal__body">
          <div className="user-details-modal__body-field">
            <label>NAME</label>
            <label>{`${user.first_name} ${user.last_name}`}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>EMAIL ADDRESS</label>
            <label>{user.email ? user.email : "N/A"}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>DEPARTMENT</label>
            <label>{profile.department ? profile.department : "N/A"}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>TITLE</label>
            <label>{profile.title ? profile.title : "N/A"}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>OFFICE PHONE NUMBER</label>
            <label>
              {phoneNumberInFormat(
                profile.office_phone_country_code,
                profile.office_phone
              )}
            </label>
          </div>
          <div className="user-details-modal__body-field">
            <label>CELL PHONE NUMBER</label>
            <label>
              {phoneNumberInFormat(
                profile.country_code,
                profile.cell_phone_number
              )}
            </label>
          </div>
          <div className="user-details-modal__body-field">
            <label>ROLE</label>
            <label>{user.role_display_name}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>CCO ID</label>
            <label>{user.profile.cco_id ? user.profile.cco_id : "N/A"}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>OUTLOOK EMAIL</label>
            <label>
              {user.profile.outlook_email ? user.profile.outlook_email : "N/A"}
            </label>
          </div>
          <div className="user-details-modal__body-field">
            <label>TWITTER PROFILE URL</label>
            <label>
              {profile.twitter_profile_url
                ? profile.twitter_profile_url
                : "N/A"}
            </label>
          </div>
          <div className="user-details-modal__body-field">
            <label>LINKEDIN PROFILE URL</label>
            <label>
              {profile.linkedin_profile_url
                ? profile.linkedin_profile_url
                : "N/A"}
            </label>
          </div>
          <div className="user-details-modal__body-field">
            <label>STATUS</label>
            <label>{user.is_active ? "Enabled" : "Disabled"}</label>
          </div>
          <div className="user-details-modal__body-field">
            <label>CUSTOMER</label>
            <label>
              {(user.profile &&
                user.profile.customer_instance_id &&
                this.getCustomerName(user.profile.customer_instance_id)) ||
                "N.A."}
            </label>
          </div>
          <div className="user-details-modal__body-field">
            <label>CUSTOME USER</label>
            {(user.profile &&
              user.profile.customer_user_instance_id &&
              this.getCustomerUserName(
                user.profile.customer_user_instance_id
              )) ||
              "N.A."}
          </div>
          <div className="user-details-modal__body-field">
            <label>SYSTEM MEMBER </label>
            {(user.profile &&
              user.profile.system_member_crm_id &&
              this.getMemberUserName(user.profile.system_member_crm_id)) ||
              "N.A."}
          </div>
          {
            <div className="user-details-modal__body-field">
              <label>PROJECT ROLE </label>
              {user.project_rate_role_name
                ? user.project_rate_role_name
                : "N.A."}
            </div>
          }
        </div>
      );
    } else {
      return null;
    }
  };

  getFooter = () => {
    return (
      <div className="user-details-modal__footer">
        <SquareButton
          onClick={this.onClose}
          content="Close"
          bsStyle={"primary"}
        />
      </div>
    );
  };

  render() {
    return (
      <ModalBase
        show={this.props.show}
        onClose={this.onClose}
        titleElement={this.getTitle()}
        bodyElement={this.getBody()}
        footerElement={this.getFooter()}
        className="user-details-modal"
      />
    );
  }
}
