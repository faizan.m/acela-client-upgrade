import React from "react";
import { debounce } from "lodash";
import { connect } from "react-redux";
import InfiniteScroll from "react-infinite-scroll-component";
import Spinner from "../../components/Spinner";
import Input from "../../components/Input/input";
import { saveFileFromURL } from "../../utils/download";
import SquareButton from "../../components/Button/button";
import RightMenu from "../../components/RighMenuBase/rightMenuBase";
import { fromISOStringToFormattedDate } from "../../utils/CalendarUtil";
import {
  fetchEmails,
  FETCH_EMAILS_FAILURE,
  FETCH_EMAILS_SUCCESS,
  resendEmail,
} from "../../actions/email";
import { addErrorMessage, addSuccessMessage } from "../../actions/appState";
import "./style.scss";

interface IEmailLogProps extends ICommonProps {
  fetchEmails: (params: IScrollPaginationFilters) => Promise<any>;
  resendEmail: (id: number) => Promise<any>;
  addErrorMessage: TShowErrorMessage;
  addSuccessMessage: TShowSuccessMessage;
}

interface IEmailLog {
  id: number;
  email_data: {
    cc: string[];
    to: string[];
    bcc: string[];
    body: string;
    html?: string;
    files: string[];
    images: string[];
    subject: string;
    user_id: string;
    reply_to: string;
    body_type?: string;
    from_email: string;
    delivered_by: "GRAPH_API" | "SMTP";
    template_name: string;
    remote_attachments: string[][];
  };
  created: string;
}

interface IEmailLogState {
  emails: IEmailLog[];
  pagination: IScrollPaginationFilters;
  inputValue: string;
  noData: boolean;
  loading: boolean;
  viewEmail: number;
  sendingEmails: number[];
}

class EmailLog extends React.Component<IEmailLogProps, IEmailLogState> {
  constructor(props: IEmailLogProps) {
    super(props);
    this.state = this.getEmptyState();
    this.updateMessage = debounce(this.updateMessage, 1000);
  }

  getEmptyState = () => ({
    emails: [],
    pagination: {
      currentPage: 0,
      nextPage: 1,
      page_size: 25,
      ordering: "",
      search: "",
    },
    inputValue: "",
    noData: false,
    loading: true,
    viewEmail: null,
    sendingEmails: [],
  });

  componentDidMount() {
    this.fetchMoreData(true);
  }

  onSearchStringChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const search = e.target.value;
    this.setState({ inputValue: search });
    this.updateMessage(search);
  };

  updateMessage = (search: string) => {
    this.setState((prevState) => ({
      pagination: {
        ...prevState.pagination,
        search,
      },
    }));
    this.fetchMoreData(true);
  };

  fetchMoreData = (clearData: boolean = false) => {
    let prevParams = this.state.pagination;
    this.setState({
      loading: true,
    });
    if (clearData) {
      prevParams = {
        ...this.getEmptyState().pagination,
        ordering: this.state.pagination.ordering,
        search: this.state.pagination.search,
      };
    }
    if (prevParams.nextPage !== null) {
      this.props.fetchEmails(prevParams).then((action) => {
        if (action.response) {
          let emails = [];
          if (clearData) {
            emails = action.response.results;
          } else {
            emails = [...this.state.emails, ...action.response.results];
          }
          const newPagination = {
            currentPage: action.response.links.page_number,
            nextPage: action.response.links.next_page_number,
            page_size: this.state.pagination.page_size,
            ordering: this.state.pagination.ordering,
            search: this.state.pagination.search,
          };

          this.setState({
            pagination: newPagination,
            emails,
            noData: emails.length === 0 ? true : false,
            loading: false,
          });
        } else {
          this.setState({
            noData: true,
            loading: false,
          });
        }
      });
    }
  };

  fetchByOrder = (field: string) => {
    let orderBy = "";

    switch (this.state.pagination.ordering) {
      case `-${field}`:
        orderBy = field;
        break;
      case field:
        orderBy = `-${field}`;
        break;
      case "":
        orderBy = field;
        break;

      default:
        orderBy = field;
        break;
    }
    this.setState(
      (prevState) => ({
        pagination: {
          ...prevState.pagination,
          ordering: orderBy,
        },
      }),
      () => {
        this.fetchMoreData(true);
      }
    );
  };

  getOrderClass = (field: string) => {
    let currentClassName = "";
    switch (this.state.pagination.ordering) {
      case `-${field}`:
        currentClassName = "asc-order";
        break;
      case field:
        currentClassName = "desc-order";
        break;

      default:
        currentClassName = "ordering";
        break;
    }
    return currentClassName;
  };

  renderTopBar = () => {
    return (
      <div className="emails-action-tabs">
        <Input
          field={{
            value: this.state.inputValue,
            label: "",
            type: "SEARCH",
          }}
          width={10}
          name="searchString"
          onChange={this.onSearchStringChange}
          placeholder="Search"
          className="custom-search custom-search-outside"
        />
      </div>
    );
  };

  emailListingRows = (email: IEmailLog, index: number) => {
    return (
      <div
        className="row-panel"
        key={index}
        onClick={() => {
          this.setState({ viewEmail: index });
        }}
      >
        <div className="email-name">{email.email_data.subject}</div>
        <div className="emails-text">{email.email_data.from_email}</div>
        <div className="emails-text">{email.email_data.to.join(", ")}</div>
        <div className="">
          {fromISOStringToFormattedDate(email.created, "MM/DD/YYYY hh:mm A") ||
            "-"}
        </div>
        {this.getPDFDownloadMarkUp(email)}
      </div>
    );
  };

  resendEmail = (id: number) => {
    this.setState({
      sendingEmails: [...this.state.sendingEmails, id],
    });
    this.props.resendEmail(id).then((action) => {
      if (action.type === FETCH_EMAILS_SUCCESS) {
        this.props.addSuccessMessage("Email sent");
      } else if (action.type === FETCH_EMAILS_FAILURE) {
        this.props.addErrorMessage("Error while sending email");
      }
      const sendingEmails = this.state.sendingEmails.filter((x) => x !== id);
      this.setState({
        sendingEmails,
      });
    });
  };

  getPDFDownloadMarkUp = (email: IEmailLog) => {
    const isSentViaGraph = email.email_data.delivered_by === "GRAPH_API";
    if (this.state.sendingEmails.includes(email.id)) {
      return (
        <img
          className="icon__loading"
          src="/assets/icons/loading.gif"
          alt="Downloading File"
        />
      );
    } else {
      return (
        <SquareButton
          content={"Resend"}
          bsStyle={"primary"}
          onClick={(e) => {
            e.stopPropagation();
            this.resendEmail(email.id);
          }}
          className="process-cr"
          title={
            isSentViaGraph
              ? "Unable to Resend as this email is sent via Graph API"
              : ""
          }
          disabled={isSentViaGraph}
        />
      );
    }
  };

  render() {
    return (
      <div className="email-logs">
        <div className="header-actions">
          <div className="left">{this.renderTopBar()}</div>
        </div>
        <div className="email-list">
          <div className={`header  column-7`}>
            <div className={`email-name title`}> Subject</div>
            <div className={`email-name title`}> From</div>
            <div className={`email-name title`}> To</div>
            <div
              className={`title  ${this.getOrderClass("created")}`}
              onClick={() => this.fetchByOrder("created")}
            >
              Created
            </div>
            <div className={`email-name title`}> Action</div>
          </div>

          <div id="scrollableDiv" style={{ height: "72vh", overflow: "auto" }}>
            <InfiniteScroll
              dataLength={this.state.emails.length}
              next={this.fetchMoreData}
              hasMore={this.state.pagination.nextPage ? true : false}
              loader={<h4 className="no-data"> Loading...</h4>}
              scrollableTarget="scrollableDiv"
            >
              {this.state.emails.map((email, index) =>
                this.emailListingRows(email, index)
              )}
              {!this.state.loading &&
                this.state.pagination.nextPage &&
                this.state.emails.length > 0 && (
                  <div
                    className="load-more"
                    onClick={() => {
                      this.fetchMoreData(false);
                    }}
                  >
                    load more data...
                  </div>
                )}
            </InfiniteScroll>
            {this.state.noData && this.state.emails.length === 0 && (
              <div className="no-data">No Emails Available</div>
            )}
            {this.state.loading && (
              <div className="loader">
                <Spinner show={true} />
              </div>
            )}
          </div>
          {this.state.viewEmail !== null && (
            <RightMenu
              show={this.state.viewEmail !== null}
              onClose={() => this.setState({ viewEmail: null })}
              titleElement={<div className="">Email</div>}
              bodyElement={
                <div className="Email-data ">
                  <div className="top">
                    <div className="left">
                      <div className="emails-text">
                        <div className="email-label">To :</div>
                        {this.state.emails[
                          this.state.viewEmail
                        ].email_data.to.join(", ")}
                      </div>
                      <div className="emails-text">
                        <div className="email-label">CC :</div>
                        {this.state.emails[
                          this.state.viewEmail
                        ].email_data.cc.join(", ")}
                      </div>
                      <div className="emails-text">
                        <div className="email-label">BCC :</div>
                        {this.state.emails[
                          this.state.viewEmail
                        ].email_data.bcc.join(", ")}
                      </div>
                      <div className="emails-text">
                        <div className="email-label">From :</div>
                        {
                          this.state.emails[this.state.viewEmail].email_data
                            .from_email
                        }
                      </div>
                    </div>
                    <div className="right">
                      <div className="created">
                        <div className="email-label">Created :</div>
                        {fromISOStringToFormattedDate(
                          this.state.emails[this.state.viewEmail].created,
                          "MMM DD, YYYY"
                        ) || "-"}
                      </div>
                      {this.getPDFDownloadMarkUp(
                        this.state.emails[this.state.viewEmail]
                      )}
                    </div>
                  </div>

                  <div className="emails-text">
                    <div className="email-label">Subject :</div>
                    {this.state.emails[this.state.viewEmail].email_data.subject}
                  </div>
                  <div
                    className={`html-data reset-this`}
                    dangerouslySetInnerHTML={{
                      __html: this.state.emails[this.state.viewEmail].email_data
                        .body,
                    }}
                  />
                  <div className="emails-files">
                    <div className="email-label">Attachments :</div>
                    {this.state.emails[
                      this.state.viewEmail
                    ].email_data.remote_attachments.map((x, index) => {
                      return (
                        <div
                          className="files"
                          onClick={() => {
                            saveFileFromURL(x[1]);
                          }}
                        >
                          {`${index + 1}. `} {x[0]}
                        </div>
                      );
                    })}
                  </div>
                  <div className="emails-files">
                    <div className="email-label">Files :</div>
                    {this.state.emails[
                      this.state.viewEmail
                    ].email_data.files.map((x, index) => {
                      return (
                        <div
                          className="files"
                          onClick={() => {
                            saveFileFromURL(x[1]);
                          }}
                        >
                          {`${index + 1}. `} {x[0]}
                        </div>
                      );
                    })}
                  </div>
                </div>
              }
              footerElement={<div />}
              className="view-email-details"
            />
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch: any) => ({
  fetchEmails: (params?: IScrollPaginationFilters) =>
    dispatch(fetchEmails(params)),
  resendEmail: (id: number) => dispatch(resendEmail(id)),
  addErrorMessage: (message: string) => dispatch(addErrorMessage(message)),
  addSuccessMessage: (message: string) => dispatch(addSuccessMessage(message)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailLog);
