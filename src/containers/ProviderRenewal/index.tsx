import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchRenewalCustomers } from '../../actions/provider';

import Input from '../../components/Input/input';
import Spinner from '../../components/Spinner';
import Table from '../../components/Table/table';

import { setRenewalCustomerId } from '../../actions/renewal';

import './style.scss';

interface IProviderRenewalProps extends ICommonProps {
  fetchRenewalCustomers: TFetchProviderRenewals;
  setRenewalCustomerId: any;
  renewalCustomerId: any;
  isProviderRenwalFetching: boolean;
}

interface IProviderRenewalState {
  search: string;
  rows: IProviderRenewalRequest[];
}

class ProviderRenewal extends Component<
  IProviderRenewalProps,
  IProviderRenewalState
> {
  constructor(props: IProviderRenewalProps) {
    super(props);
    this.state = {
      search: '',
      rows: [],
    };
  }

  componentDidMount() {
    this.props.fetchRenewalCustomers().then(action => {
      this.setRows(action.response);
    });
  }

  handleInput = (e: any) => {
    this.setState({ search: e.target.value });
  };

  renderTopBar = () => {
    return (
      <div className="renewal__header">
        <div>
          <Input
            field={{
              label: '',
              type: "TEXT",
              isRequired: false,
              value: this.state.search,
            }}
            width={4}
            name="search"
            placeholder="Search"
            onChange={this.handleInput}
          />
        </div>
      </div>
    );
  };

  setRows = result => {
    this.setState({ rows: result });
  };

  filterRows = req => {
    const searchValue = this.state.search.toLowerCase();
    if (req.customer_name.toLowerCase().includes(searchValue)) {
      return req;
    }
  };

  onRowClick = rowInfo => {
    this.props.setRenewalCustomerId(rowInfo.original.customer_id);
    this.props.history.push(`/renewal/${rowInfo.original.customer_id}`);
  };

  render() {
    const columns = [
      {
        accessor: 'customer_name',
        Header: 'Customer Name',
        Cell: name => <div className="pl-15">{name.value}</div>,
      },
      {
        accessor: 'pending_renewals_count',
        Header: 'Pending Requests',
      },
      {
        accessor: 'completed_renewals_count',
        Header: 'Completed Requests',
      },
    ];
    const rows = this.state.rows.filter(this.filterRows);

    return (
      <div className="provider-renewal-container">
        <h3>Customers Renewal Requests</h3>
        <div className="loader">
          <Spinner show={this.props.isProviderRenwalFetching} />
        </div>
        <Table
          columns={columns}
          rows={rows}
          customTopBar={this.renderTopBar()}
          onRowClick={this.onRowClick}
          className={`${this.props.isProviderRenwalFetching ? 'loading' : ''}`}
          loading={this.props.isProviderRenwalFetching}
        />
      </div>
    );
  }
}
const mapStateToProps = (state: IReduxStore) => ({
  renewalCustomerId: state.renewal.renewalCustomerId,
  isProviderRenwalFetching: state.renewal.isProviderRenwalFetching,
});

const mapDispatchToProps = (dispatch: any) => ({
  fetchRenewalCustomers: () => dispatch(fetchRenewalCustomers()),
  setRenewalCustomerId: (id: number) => dispatch(setRenewalCustomerId(id)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProviderRenewal);
