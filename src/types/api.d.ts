declare type USER_TYPE = "ACCELAVAR" | "PROVIDER" | "CUSTOMER";

declare type USER_ROLE = "OWNER" | "ADMIN" | "STAFF";

declare type SowDocType = "Fixed Fee" | "T & M" | "Change Request";

declare type JSONConfigSectionKeys =
  | "terms"
  | "project_management"
  | "project_summary"
  | "generated_terms";

declare type CRConfigSectionKeys =
  | "risk"
  | "reason"
  | "alternates"
  | "change_impact"
  | "change_affects"
  | "technical_changes"
  | "change_description";

declare type HTTPMethods = "get" | "post" | "put" | "delete" | "patch";

declare interface IUserProfile {
  user_id: string;
  email: string;
  scopes: IAuthScope;
  default_landing_page: string;
}

declare interface IAuthScope {
  type: USER_TYPE;
  role: USER_ROLE;
}

declare interface IUser {
  id?: number;
  crm_id?: number;
  divisionId: number;
  username: string;
  password?: string;
  confirmPassword?: string;
  firstName: string;
  middleName?: string;
  lastName: string;
  email: string;
  phone: string;
  title: string;
  activeDate: string;
  inactiveDate?: string;
  signature?: string;
  preferences?: object;
  isActive?: boolean;
  lastLogin?: string;
  createdOn?: string;
}

declare interface Icustomer {
  name: string;
  address?: {
    id?: number;
    created_on?: string;
    updated_on?: string;
    address_1: string;
    address_2: string;
    city: string;
    state: string;
    zip_code: string;
  };
  crm_id?: string;
  phone: string;
  country_code?: string;
  primary_contact: string;
  users_count: number;
  devices_count: number;
  status?: string;
  id?: number;
  provider?: number;
  is_active?: boolean;
  index?: any;
  created_on?: string;
  updated_on?: string;
  renewal_service_allowed: boolean;
  collector_service_allowed: boolean;
  config_compliance_service_allowed: boolean;
  purchase_order_history_visibility_allowed: boolean;
  config_compliance_service_write_allowed: boolean;
  customer_order_visibility_allowed: boolean;
  last_logged_in_user: any;
  is_ms_customer?: boolean;
  logo?: string;
}

declare interface ICustomerPaginated extends IPaginatedDefault {
  results: Icustomer[];
}

declare interface ICustomerUser {
  activation_mails_sent_count?: number;
  created_on?: string;
  customer?: number;
  email: string;
  first_name: string;
  id?: string;
  is_active: string;
  role_display_name?: string;
  is_password_set?: boolean;
  last_login?: string;
  previous_login?: string;
  last_name: string;
  country_code?: string;
  office_phone_country_code?: string;
  phone_number?: string;
  profile: any;
  provider?: number;
  role: string;
  type?: string;
  updated_on?: string;
  user_role?: number;
  user_type?: number;
  can_edit?: boolean;
  crm_id?: number;
}

declare interface ICustomerUserPaginated extends IPaginatedDefault {
  results: ICustomerUser[];
}

declare interface IPaginatedDefault {
  count: number;
  page_size: number;
  links: {
    next_page_number: number;
    page_number: number;
    previous_page_number: number;
  };
  next: string;
  prev: string;
}

declare interface ICustomerShort {
  name: string;
  id: number;
  crm_id?: number;
  account_manager_id?: number;
  account_manager_name?: string;
}

declare interface IInventory {
  category_id: number;
  category_name: string;
  contract_number: string;
  device_name: string;
  id: number;
  location_id: number;
  model_number: string;
  notes: string;
  serial_number: string;
  site?: string;
  site_id?: number;
  status?: string;
  status_id?: number;
  contract_status?: string;
  contract_status_id?: number;
  manufacturer_name?: string;
  EOL_date?: string;
  LDOS_date?: string;
  EOS_date?: string;
  device_type?: string;
  product_bulletin_url?: string;
  service_contract_number?: string;
  expiration_date?: string;
  monitoring_data: {
    is_managed?: boolean;
    os_version?: string;
    host_name?: string;
    sys_name?: string;
    up_time?: any;
    is_created_by_lm?: string;
  };
  name?: string;
  migration_info?: string;
  customer_notes?: string;
  asset_tag?: string;
  non_cisco?: boolean;
}

declare interface ISubscription {
  category_id: number;
  category_name?: string;
  contract_number?: string;
  product?: string;
  product_id?: any;
  product_name?: string;
  type_name?: string;
  purchase_date?: string;
  id?: number;
  location_id?: number;
  model?: string;
  notes?: string;
  serial_number: string;
  site?: string;
  site_id?: number;
  status?: string;
  status_id?: number;
  contract_status?: any;
  contract_status_id?: number;
  manufacturer_name?: string;
  manufacturer_id?: number;
  EOL_date?: string;
  LDOS_date?: string;
  EOS_date?: string;
  device_type?: string;
  device_name?: string;
  product_bulletin_url?: string;
  service_contract_number?: number;
  expiration_date?: string;
  installation_date?: string;
  true_forward_date?: string;
  monitoring_data?: {
    is_managed?: boolean;
    os_version?: string;
    host_name?: string;
    sys_name?: string;
    up_time?: any;
    is_created_by_lm?: string;
  };
  name?: string;
  migration_info?: string;
  customer_notes?: string;
  asset_tag?: string;
  licence_qty?: any;
  auto_renewal_term?: number;
  term_length?: number;
  billing_model?: string;
}

declare interface IDevice {
  id?: number;
  device_name: string;
  category?: string;
  category_id?: number;
  site_id?: number;
  status_id?: number;
  status?: string;
  location?: string;
  serial_number?: string;
  model_number?: string;
  tag_number?: string;
  notes?: string;
  manufacturer_id?: number;
  manufacturer_name?: string;
  decommissioned_on?: string;
  site?: string;
  address?: string;
  EOL_date?: string;
  EOSA_date?: string;
  EOSU_date?: string;
  EOSWS_date?: string;
  EOS_date?: string;
  LDOS_date?: string;
  contract_number?: string;
  category_name?: string;
  location_id?: number;
  product_id?: number;
  is_managed_devices?: string;
  purchased_date?: string;
  CE_date?: string;
  contract_status?: string;
  product_bulletin_url?: string;
  updated_on?: string;
  item_description?: string;
  expiration_date?: string;
  service_contract_number?: string;
  purchase_date?: string;
  instance_id?: string;
  retry_count?: number;
  max_retry_reached?: boolean;
  is_serial_number_invalid?: boolean;
  migration_info?: string;
  migration_product_id?: string;
  migration_product_name?: string;
  monitoring_data?: {
    created_by: string;
    last_updated_by: string;
    logicmonitor?: IMonitoringSourceData;
    solarwinds?: IMonitoringSourceData;
    is_managed?: boolean;
  };
  name?: string;
  circuit_info_ids?: any;
  renewal_history_is_present?: boolean;
  customer_notes?: string;
  asset_tag?: string;
  installation_date?: string;
  expiration_date?: string;
  service_sku?: string;
  service_level?: string;
  service_description?: string;
  release_version?: string;
  image_name?: string;
  release_date?: string;
  image_download_url?: string;
  suggested_software_update?: string;
  non_cisco?: boolean;
}

declare interface ISoWServiceDetailPayload {
  customer: number;
  name: string;
  json_config: IJSONConfig;
  user: string;
  author: string;
  updated_by: string;
  quote_id: number;
  doc_type: SowDocType;
  pm_type_t_and_m: boolean;
}

declare interface IMonitoringSourceData {
  device_name?: string;
  is_managed?: boolean;
  os_version?: string;
  host_name?: string;
  sys_name?: string;
  ios_image?: string;
  updated_on?: string;
  up_time?: any;
}
declare interface IConfiguration {
  id: number;
  device_name: string;
  category_id: number;
  category_name: string;
  status_id: number;
  status: string;
  customer_id: number;
  customer_name: string;
  site_id: number;
  site?: string;
  name: string;
  location_id: number;
  serial_number: string;
  model_number: string;
  service_contract_number: string;
  purchase_date: string;
  expiration_date: string;
  notes: string;
  manufacturer_id: number;
  manufacturer_name: string;
  configuration_data: IConfigurationData;
  device_type?: string;
  config?: string;
  config_last_updated_on?: string;
  config_last_synced_date: string;
  invalid_compliance_statistics?: any[];
  valid_compliance_statistics?: any[];
  compliance_statistics?: IRule[];
  stack_count: number;
  serial_numbers: any[];
  host_name: string;
  violations_count_by_levels?: any;
  rules_violations_by_priority: IRulesViolationsPriority;
  checked?: boolean; // Used by UI
}
declare interface IConfigurationData {
  config: string;
  config_last_updated_on: string;
  config_last_synced_date: string;
  config_data: Iconfig;
}

declare interface Iconfig {
  config: string;
  config_last_updated_on: string;
  config_last_synced_date: string;
}
declare interface ISite {
  id?: number;
  name?: string;
  address_line_1?: string;
  address_line_2?: string;
  city?: string;
  state?: string;
  zip?: string;
  site_id?: string;
  site?: string;
  country?: string;
  state_id?: number;
  country_id?: number;
  state_crm_id?: number;
  country_crm_id?: number;
  phone_number?: string;
  is_custom_site?: boolean;
  business_unit?: string;
  crm_id?: number;
}

declare interface IPickListOptions {
  value: any;
  label: string | JSX.Element;
  disabled?: boolean;
  data?: any;
}

declare interface ICustomFieldConfig {
  [key: string]: IInputField;
}

declare interface IForcedAbatementSectionConfig {
  id: number;
  name: string;
  label: string;
  abatementSectionFields: ICustomFieldConfig[];
}
declare interface IBulkModify {
  activate?: number[];
  inactivate?: number[];
}

declare interface ISetPasswordRequest {
  uid: string;
  token: string;
  new_password: string;
}

declare interface IResetPasswordRequest {
  new_password: string;
  current_password: string;
}

declare interface IForgotPasswordRequest {
  email: string;
}

declare interface IProvider {
  id?: number;
  address: {
    id?: number;
    created_on?: string;
    updated_on?: string;
    address_1: string;
    address_2: string;
    city: string;
    state: string;
    zip_code: string;
  };
  accounting_contact: {
    id?: number;
    created_on?: string;
    updated_on?: string;
    first_name: string;
    last_name: string;
    email: string;
    phone: string;
    website: string;
  };
  timezone?: string;
  name: string;
  url: string;
  email: string;
  is_active: boolean;
  logo?: string;
  support_contact: string;
  // TODO
  // Make this an enum
  http_protocol: string;
  sub_domain?: string;
  created_on?: string;
  updated_on?: string;
  role?: string;
  is_configured?: boolean;
  is_two_fa_enabled?: boolean;
}

declare interface IProviderPaginated extends IPaginatedDefault {
  results: IProvider[];
}

declare interface IProviderAdminUser {
  id?: string;
  email: string;
  first_name: string;
  last_name: string;
  phone_number: string;
  type?: string;
  role?: string;
  user_type?: number;
  user_role?: number;
  provider?: number;
  customer?: number;
  is_active?: boolean;
  profile: {
    department: string;
    title: string;
    office_phone: string;
    cell_phone_number: string;
    twitter_profile_url: string;
    linkedin_profile_url: string;
  };
  created_on?: string;
  updated_on?: string;
  last_login?: string;
  previous_login?: string;
}

declare interface IProviderAdminPaginated extends IPaginatedDefault {
  results: ISuperUser[];
}

declare interface IProviderRenewalRequest {
  customer_id: number;
  customer_name: string;
  pending_renewals_count: number;
  completed_renewals_count: number;
}

declare interface ISuperUser {
  id?: string;
  email: string;
  first_name?: string;
  last_name?: string;
  phone_number?: string;
  is_password_set?: boolean;
  role?: string;
  role_display_name?: string;
  user_role?: number;
  is_active: boolean;
  crm_id?: number;
  profile: {
    cco_id?: any;
    country_code?: string;
    department?: string;
    title?: string;
    office_phone_country_code?: string;
    office_phone?: string;
    cell_phone_number?: string;
    twitter_profile_url?: string;
    linkedin_profile_url?: string;
    profile_pic?: string;
    outlook_email?: string;
    customer_instance_id?: number;
    customer_user_instance_id?: number;
    system_member_crm_id?: number;
    default_landing_page?: string;
    email_signature?: {
      email_signature?: string;
      email_signature_markdown?: string;
    };
    cco_id_acknowledged?: boolean;
    feature?: IFeatureAccess[];
  };
  is_two_fa_enabled?: boolean;
  created_on?: string;
  updated_on?: string;
  last_login?: string;
  previous_login?: string;
  type?: string;
  can_edit?: boolean;
  can_delete?: boolean;
  provider?: number;
  customer?: number;
  project_rate_role?: number;
  project_rate_role_name?: string;
}

declare interface Iservice {
  open_ps_ticket: number;
  open_ms_ticket: number;
  open_projects: number;
}
declare interface IserviceTechnologyTypes {
  id?: number;
  name: string;
  is_disabled: boolean;
}

declare interface IserviceCatalog {
  id?: number;
  service_type: any;
  service_category: any;
  service_name: string;
  service_technology_types?: any[];
  service_technology_types_list?: IserviceTechnologyTypes[];
  service_technology_types_names?: any[];
  is_template_present?: boolean;
  is_disabled: boolean;
  created_on?: string;
  updated_on?: string;
  notes?: string;
  linked_template: any;
  linked_template_name?: any;
}
declare interface IserviceCatalogShort {
  id?: number;
  service_name: string;
  service_category: any;
  service_type: any;
}
declare interface Ilifecycle {
  expiring_support: number;
  expired_support: number;
  end_of_life: number;
  supported_devices: number;
}

declare interface IVendorSettings {
  territories: { territory_crm_id: number; territory_name: string }[];
  customer_type: { customer_type_crm_id: number; customer_type_name: string };
  vendor_onboarding_email_alias: string;
  vendor_onboarding_email_subject: string;
  vendor_onboarding_email_body: string;
  vendor_list_statuses: {
    customer_status_crm_id: number;
    customer_status_name: string;
  }[];
  vendor_create_status: {
    customer_status_crm_id: number;
    customer_status_name: string;
  };
}

declare interface ICustomerListDashBoard {
  name: string;
  customers_count?: number;
  users_count?: number;
}

declare interface ISupeUserDashboard {
  providers_count: number;
  top_five_providers_by_customer_count: ICustomerListDashBoard[];
  top_five_providers_by_users_activity: ICustomerListDashBoard[];
}

declare interface Iorders {
  open: number;
  closed: number;
  open_quotes: number;
}

declare interface IOrderTrackingDashboardSetting {
  business_unit_id: number;
  created_on?: string;
  id?: number;
  po_custom_field_id: number;
  purchase_order_status_ids: number[];
  service_boards: number[];
  service_ticket_create_date: string;
  ticket_status_ids: number[];
  updated_on?: string;
}

declare interface IProviderProfile {
  url_alias: string;
  ticket_note: string;
  colour_code: string;
  support_contact: string;
  company_url: string;
  enable_url: boolean;
  logo: any;
  timezone: string;
  customer_instance_id: string;
}

declare interface ISuperUserPaginated extends IPaginatedDefault {
  results: ISuperUser[];
}

declare interface IRoleType {
  id: number;
  role_name: string;
  display_name: string;
}

declare interface IUserTypeRole {
  id: number;
  user_type: string;
  roles: IRoleType[];
}

declare interface IIntegrationType {
  id: number;
  name: string;
  authentication_config: ICustomFieldConfig;
  device_manu_id?: number;
}

declare interface IIntegration {
  id: number;
  name: string;
  types: IIntegrationType[];
}

declare interface IProviderIntegration {
  id?: number;
  provider?: number;
  integration_type: number;
  authentication_info: ICustomFieldConfig;
  other_config: any; // TODO;
  type?: string;
  category?: string;
}

declare interface IProviderIntegrationRequest {
  integration_type: number;
  authentication_info: Array<{ [name: string]: string }>;
}

declare interface IConfigStatus {
  crm_authentication_configured: boolean;
  crm_board_mapping_configured: boolean;
  crm_device_categories_configured: boolean;
  device_manufacturer_api_configured: boolean;
}

declare interface IBoardMappingObj {
  type?: string;
  label?: string;
  value?: string;
  is_required?: boolean;
  disabled?: boolean;
  options: IDLabelObject[];
}

declare interface IFieldMapping {
  "Custom Fields": {
    ms_customer: IBoardMappingObj;
  };
  Orders: {
    open_status: IBoardMappingObj;
    closed_status: IBoardMappingObj;
  };
  "Service Board Mapping": ServiceBoardMapping;
  Opportunities: {
    status: IBoardMappingObj;
    stages: IBoardMappingObj;
  };
  "SOW Opportunity Mapping": {
    open_status: IBoardMappingObj;
    business_unit: IBoardMappingObj;
  };
  "Change Request Mapping": {
    won_status: IBoardMappingObj;
    won_stage: IBoardMappingObj;
  };
  "SOW Quote Status Mapping": {
    won_status: IBoardMappingObj;
    open_status: IBoardMappingObj;
    lost_status: IBoardMappingObj;
  };
}

declare interface IServiceBoardMapping {
  "Professional Services": ProfessionalServices;
  "Managed Services": ManagedServices;
  "Smartnet Services": SmartnetServices;
  Projects: Projects;
}

declare interface IServiceBoardObj {
  board: IBoardMappingObj;
  open_status: IBoardMappingObj;
  closed_status: IBoardMappingObj;
  smartnet_complete_status?: IBoardMappingObj;
  ticket_closed_status?: IBoardMappingObj;
}

declare interface ICustomerNote {
  note_id?: string;
  text: string;
  note_type_id?: number;
  note_type_name?: string;
  flagged?: boolean;
  entered_by?: string;
  id?: number;
  identifier?: string;
  last_updated?: string;
  updated_by?: string;
}

declare interface ISalesNote {
  id?: number;
  last_updated_by?: string;
  opportunity_ids: number[];
  contact_ids: number[];
  site_ids: number[];
  technology_ids: number[];
  created_on?: string;
  updated_on?: string;
  body: string;
  description: string;
  notes_object: string;
  is_deleted?: boolean;
  provider?: number;
  customer?: number;
  markDownText?: string;
}

declare interface IClient360Settings {
  won_opp_status_crm_ids: number[];
  open_po_status_crm_ids: number[];
}

declare interface ICancelledCRSetting {
  opp_status_crm_id: number;
  opp_stage_crm_id: number;
}

declare interface IProjectHistory {
  actor: string;
  action: string;
  timestamp: string;
  remote_addr: string;
  changes_str: string;
  object_repr: string;
  additional_data: {
    project_id: number;
    cr_processing_details?: IProjectActionDetails[];
    meeting_close_details?: IProjectActionDetails[];
  };
  model_display_name: string;
  changes_dict: {
    [fieldName: string]: [string, string];
  };
  changes_display_dict: {
    [fieldName: string]: [string, string];
  };
}

declare interface IProjectActionDetails {
  message: string;
  errors: string[];
}

declare interface ICustomerEscalation {
  id?: string;
  escalation_priority: string;
  escalation_type: string;
  user?: any;
  userId?: any;
  contact: string;
  role?: string;
  email?: string;
  location?: string;
  cell_phone?: string;
  work_phone?: string;
  authorize_charges: boolean;
  authorize_changes: boolean;
  cw_site_id?: string;
}

declare interface ICircuitInfo {
  id?: string;
  site: string;
  cw_site_id?: string;
  circuit_type: string;
  provider: string;
  circuit_id: string;
  contact_phone?: string;
  email?: string;
  note?: string;
  loa?: boolean;
  device_crm_id?: string;
  cw_site_name?: string;
  contract_end_date?: string;
  attachment?: string;
  ip_address?: string;
  circuit_type_name?: string;
  bandwidth?: string;
  bandwidth_frequency?: string;
  account_number?: string;
}
declare interface IDeviceCategory {
  category_id: number;
  category_name: string;
}

declare interface ICategoryManufaturerMapping {
  device_category_id: number;
  device_category_name: string;
  manufacturer_id: number;
  cw_mnf_name: string;
  acela_integration_type_id: number;
}
declare interface ICategorySubscriptioMapping {
  subscription_category_id: number;
  subscription_category_name: string;
  manufacturer_id: number;
  cw_mnf_name: string;
  acela_integration_type_id: number;
}

declare interface ImanufacturerMapping {
  cw_manu_id: number;
  device_manu_id: number;
}

declare interface IFinanceReportFilters {
  status: string;
  customer_id?: number;
  po_date_after?: string;
  po_date_before?: string;
  created_date_after?: string;
  created_date_before?: string;
  payment_date_after?: string;
  payment_date_before?: string;
  received_date_after?: string;
  received_date_before?: string;
  expected_ship_date_after?: string;
  expected_ship_date_before?: string;
  opportunity_won_date_after?: string;
  opportunity_won_date_before?: string;
}

declare interface IExtraConfig {
  board_mapping: IFieldMapping;
  device_categories: IDeviceCategory[];
  manufacturer_map: any;
  empty_serial_mapping: {
    device_category_ids: numbers[];
  };
}

declare interface IServerPaginationParams {
  page?: number;
  page_size?: number;
  ordering?: string;
  search?: string;
  pagination?: boolean;
}

declare interface ISowDocListFilters {
  id?: number;
  customer_id?: number;
  doc_type?: string;
  quote_id?: string;
}

declare interface ProjectPhase {
  id: number;
  title: string;
  order: number;
}

declare interface ProjectPhaseParent {
  id: number;
  updated_on: string;
  estimated_completion_date: string;
  phase: ProjectPhase;
  status: {
    id: number;
    title: string;
    color: string;
  };
}

declare interface IOperationsDashboardFilterParams {
  ingram?: boolean;
  eta_after?: string;
  eta_before?: string;
  eta_is_null?: "True" | "False";
  connectwise_last_data_update_after?: string;
  connectwise_last_data_update_before?: string;
  "shipped-not-received"?: boolean;
  vendor_company_name?: string;
}

declare interface IPMODashboardFilterParams {
  type?: string;
  customer?: number;
  sow_type?: string;
  customer?: string;
  unmapped_opp?: boolean;
  unmapped_sow?: boolean;
  overall_status?: string;
  project_manager?: string;
  estimated_end_date_after?: string;
  estimated_end_date_before?: string;
}

declare interface IScrollPagination {
  page?: number;
  currentPage?: number;
  totalPages?: number;
  nextPage?: number;
  page_size?: number;
  ordering?: string;
  search?: string;
}

declare interface IPMONotesPagination extends IScrollPagination {
  author_id: string;
}

declare interface IPMONote {
  id: number;
  note: string;
  provider_id: number;
  author_id: string;
  author_first_name: string;
  author_last_name: string;
  author_pic: string;
  created_on: string;
  updated_on: string;
  is_archived: boolean;
}

declare interface IScrollPaginationFilters extends IScrollPagination {
  project_manager?: string;
  assigned_to?: string;
  priority?: string;
  type?: string;
  customer?: string;
  manager?: string;
  status?: string;
  created_by?: string;
  activity_type?: string;
  closed_start_date?: string;
  closed_end_date?: string;
  action_start_date?: string;
  action_end_date?: string;
  due_date_before?: string;
  due_date_after?: string;
  showGroupBy?: boolean;
  groupBy?: string;
  showSearch?: boolean;
  count?: number;
  projects_closing_this_week?: any;
  overbudget_projects?: any;
  projects_with_overdue_action_items_or_overdue_critical_path_items?: any;
  project_engineer_or_manager?: any;
  project_engineer?: any;
  unmapped_sow?: boolean;
  unmapped_opp?: boolean;
}
declare interface ISPRulesFilterParams {
  page?: number;
  page_size?: number;
  ordering?: string;
  search?: string;
  level?: string;
  classification?: string;
  //applies_to?: string;
  status?: string;
  is_global?: string;
}

declare interface IProductCatalogFilters {
  product_type?: string[] | string;
}

declare interface IServerPaginationServiceCatlogFilterParams {
  page?: number;
  page_size?: number;
  ordering?: string;
  search?: string;
  service_type?: string;
  service_category?: string;
  service_technology_types?: string;
}

declare interface ISoWMetricsFilterParams {
  id?: number;
  quote_id?: string;
  "doc-status"?: string;
  updated_after?: string;
  updated_before?: string;
  created_after?: string;
  created_before?: string;
  technology_type?: string;
  author_id?: string;
}

declare interface AgreementTemplatesQueryParams
  extends IServerPaginationParams,
    ITemplateFilters {}

declare interface AgreementQueryParams
  extends IServerPaginationParams,
    IAgreementFilters {}

declare interface IContractStatus {
  contract_status_id: number;
  contract_status: string;
}

declare interface IStatusImpactItem {
  id: number;
  name?: string;
  title?: string;
  color: string;
  is_default?: boolean;
}

declare interface IComment {
  contact_id: number;
  contact_name: string;
  created_by: string;
  created_on: string;
  id: number;
  last_updated_on: string;
  text: string;
  alternate_created_by?: string;
  is_internal_note?: boolean;
}

declare interface ISOPOMapping {
  opportunity_crm_id: number;
  detail?: string;
  opportunity_name?: string;
  sales_order_crm_id?: number;
  purchase_order_crm_ids?: number[];
  purchase_order_numbers?: string[];
  customer_name?: string;
}

declare interface IServiceTicket {
  id: number;
  summary: string;
  record_type: string;
  board: string;
  board_id: number;
  status: string;
  status_id: number;
  company_id: number;
  company_name?: string;
  priority: string;
  priority_id: number;
  severity: string;
  impact: string;
  owner: string;
  resource: string;
  resource_phone_number: string;
  created_on: string;
  last_updated_on: string;
  notes?: IComment[];
  documents?: ITicketDocument[];
  description: string;
  can_close: boolean;
  opportunity_id?: number;
  opportunity_name?: string;
  owner_id?: number;
}

declare interface IServiceTicketNew {
  summary: string;
  description?: string;
  primary_contact?: string;
  created_by?: string;
}

declare interface ITicketDocument {
  file_name: string;
  id: number;
  server_file_name: string;
  title: string;
}

declare interface IInventoryFilters {
  site?: number[];
  type?: any[];
  status?: number[];
  is_managed?: string[];
  created_by?: string[];
  manufacturer?: any[];
  replacement?: string[];
  contractStatus?: number[];
  non_cisco?: string[];
}
declare interface ISubscriptionFilters {
  productMappings?: any[];
  contractStatus?: number[];
  status?: number[];
  manufacturer?: any[];
}

declare interface ISiteFilters {
  site?: number[];
}
declare interface ICircuitFilters {
  site?: number[];
  type?: number[];
  is_exist?: any[];
}
declare interface IConfigurationFilters {
  site?: number[];
  type?: any[];
  status?: number[];
  level?: any[];
  classification?: any[];
}

declare interface SystemCategory {
  key: string;
  name: string;
  value: string[];
  not_value: string[];
  active: boolean;
}

declare interface ILogicMonitorMapping {
  group_ids: number[];
  system_categories: SystemCategory[];
  cw_mnf_name: string;
  cw_mnf_id: number;
  key_mappings: any;
  non_cisco_devices?: string;
}

declare interface IDocumentFilters {
  customer?: number[];
  author?: string[];
  is_quote_exist?: any[];
}

declare interface IServiceCatalogFilters {
  service_type?: number[];
  service_category?: number[];
  service_technology_types?: number[];
  is_template_present?: string;
  linked_service_catalog?: number[];
}

declare interface ITemplateFilters {
  author_id?: string;
  updated_before?: string;
  updated_after?: string;
}

declare interface IAgreementFilters {
  author_id?: string;
  customer?: number;
  updated_before?: string;
  updated_after?: string;
}

declare interface IRulesFilter {
  level?: string[];
  classification?: string[];
  customers_assigned?: number[];
  is_enabled?: any[];
  is_global?: any[];
  applies_to?: string[];
}
declare interface IVariableFilter {
  is_global?: any[];
  type?: any[];
  option?: any[];
}
declare interface ISOWDashboardField {
  value: number;
  hover_text: string;
  label: string;
  unit: string;
  bg_color: string;
  text_color: string;
  documents?: any[];
}

declare interface ICollectorQuery {
  id: any;
  payload: string;
  status?: string;
  description?: string;
  updated_on?: string;
  created_on?: string;
  result?: any;
  collector?: string;
}

declare interface INotes {
  name: string;
  notes: string;
}

declare interface INotesFilters {
  opportunity_ids?: number[];
  contact_ids?: number[];
  site_ids?: number[];
  technology_ids?: number[];
  customer_ids?: number[];
  updated_on_0?: any;
  updated_on_1?: any;
  dateValue?: any;
}

declare interface IProjectMeeting {
  id?: any;
  name: string;
  project_update?: string;
  meeting_id?: string;
  schedule_start_datetime: any;
  schedule_end_datetime: any;
  conducted_on?: any;
  status?: string;
  meeting_type: string;
  agenda_topics?: IAgendaTopic[];
  action_items?: IProjectDetailItem[];
  critical_path_items?: IProjectDetailItem[];
  meeting_notes?: any[];
  risk_items?: IProjectDetailItem[];
  email_body: string;
  email_subject: string;
  email_body_text?: string;
  email_body_replaced_text?: string;
  is_internal?: boolean;
  project_acceptance?: string;
  project_acceptance_markdown?: string;
  project_acceptance_text?: string;
  project_acceptance_replaced_text?: string;
  project_acceptance_email_subject?: string;
  owner?: string;
  is_archived?: boolean;
  updated_on?: string;
  created_on?: string;
  preview_ordering?: Ordering;
}
interface ItemType {
  ordering_field: string;
}

interface Items {
  CriticalPathItem: ItemType;
  ActionItem: ItemType;
  RiskItem: ItemType;
}

interface Ordering {
  [key: string]: ItemType;
}
declare interface MeetingTimeEntry {
  pm_time_entry: TimeEntry;
  engineer_time_entries: TimeEntry[];
}

declare interface IAgendaTopic {
  id?: number;
  topic: string;
  notes: string;
}

declare interface IProjectDetailItem {
  id?: number;
  description?: string;
  owner: string;
  owner_name?: string;
  owner_profile_pic?: string | null;
  resource?: any;
  impact?: number;
  impact_name?: string;
  completed_on?: string | null;
  created_on?: any;
  notes?: string | null;
  due_date?: any;
  status?: number;
  status_title?: string;
  status_color?: string;
  updated_on?: string;
}

declare interface IProjectUpdates {
  updates: string;
  note: string;
  estimated_hours_to_complete: number;
}

declare interface IProjectDetails {
  id?: number;
  title?: string;
  description?: string;
  crm_id?: number;
  hours_budget?: number;
  actual_hours?: number;
  estimated_end_date?: string;
  closed_date?: string;
  type?: string | number;
  customer?: string;
  project_manager?: string;
  overall_status?: string;
  primary_contact?: string;
  account_manager?: string;
  created_on?: string;
  updated_on?: string;
  sow_document?: number;
  opportunity_crm_ids?: number[];
  billing_amount?: number;
  project_updates?: IProjectUpdates;
  type_id?: number;
  customer_id?: number;
  project_manager_id?: string;
  project_manager_profile_pic?: string;
  overall_status_id?: number;
  overall_status_color?: string;
  primary_contact_id?: string;
  last_updated_by?: string;
  last_updated_by_id?: string;
  time_status_value?: number;
  time_status_color?: string;
  is_time_status_critical?: boolean;
  date_status_value?: number;
  date_status_color?: string;
  progress_percent?: number;
  overdue_critical_path_items?: number;
  overdue_action_items?: number;
  last_action_date?: string;
  expiring_critical_path_items?: number;
  expiring_action_items?: number;
  action_status_color?: string;
  linked_sow_doc_name?: string;
}

const projectData: ProjectData = {
  // Fill in the data here based on the JSON structure you provided
};

declare interface IProjectType {
  id?: string;
  title: string;
  edited: boolean;
  phases: IProjectPhases[];
  titleError?: string;
  newTag?: boolean;
  in_use?: boolean;
}

declare interface IProjectPhases {
  title: string;
  order: boolean;
  id: string;
  tag?: IProjectPhaseTag;
}

declare interface IProjectPhaseTag {
  title: string;
  id: string;
}

declare interface IMarkDownVariables {
  value: string | number;
  id: string | number;
}

declare interface ISoW {
  author?: string;
  author_name?: string;
  category: number;
  category_name?: string;
  change_request?: IChangeRequest;
  created_on?: string;
  customer?: any;
  customer_id?: number;
  pm_type_t_and_m?: boolean;
  doc_type: SowDocType;
  forecast_id?: number;
  id: number;
  json_config: IJSONConfig;
  major_version: number;
  minor_version: number;
  name: string;
  quote_id?: number;
  updated_by?: string;
  updated_by_name?: string;
  updated_on?: string;
  user: string;
  user_name?: string;
  version?: string;
  template?: number;
  version_description?: string;
  update_version: boolean;
  update_author: boolean;
  project?: number;
  change_request_type?: string;
  requested_by?: string;
  change_number?: number;
  include_cover_page: boolean;
  include_customer_logo: boolean;
}

declare interface ISOWShort {
  id: number;
  category_name: string;
  author_name: string;
  user_name: string;
  updated_by_name: string;
  change_request: ICRShort[];
  version: string;
  name: string;
  author: string;
  updated_by: string;
  quote_id: number;
  forecast_id: number;
  category: number;
  doc_type: SowDocType;
  created_on: string;
  updated_on: string;
  quote: IQuote;
  quote_exist: boolean;
  forecast_exception: {
    message: string;
    exception_in_forecast_creation: boolean;
  };
  customer_name: string;
  customer_id: number;
}

interface IAgreementShort {
  id: number;
  name: string;
  index?: number;
  version: string;
  updated_on: string;
  author_name: string;
  is_disabled: boolean;
  customer_name: string;
  updated_by_name: string;
  is_current_version?: boolean;
  forecast_data: {
    forecast_id: string;
    forecast_error_details: {
      error_message: string;
      error_in_forecast_creation: boolean;
    };
  };
}

declare interface ICRShort {
  id: number;
  name: string;
  change_number: number;
  change_request_type: SowDocType;
  project: number;
  project_name: string;
  requested_by: string;
  requested_by_name: string;
  status: string;
  assigned_to_name: string;
}

declare interface ICRListItem {
  id: number;
  name: string;
  requested_by: string;
  assigned_to: string;
  change_request_type: string;
  status: string;
  sow_document?: number;
  sow_document_id?: number;
  json_config: IChangeRequestConfig;
  change_number: number;
  last_updated_by: string;
  created_on: string;
  updated_on: string;
  project_id?: number;
  project_title?: string;
  customer_id?: number;
  customer_name?: string;
  customer_cost?: number;
  requested_by_id?: string;
  requested_by_name?: string;
  assigned_to_id?: string;
  assigned_to_name?: string;
  sow_document_id?: number;
  project_manager?: string;
}

declare interface ICRFilters {
  customer_id?: number[] | string;
  project_id?: number;
  updated_after?: string;
  updated_before?: string;
  status?: string;
}

declare interface IVRWarehouseFilters {
  customer_id?: number;
  status?: string[] | string;
  warehouse?: string[] | string;
  include_shipped?: boolean;
}

declare interface IVRWarehouseSettings {
  in_transit_status: {
    status: string;
    label: string;
  };
  in_stock_status: {
    status: string;
    label: string;
  };
  in_configuration_status: {
    status: string;
    label: string;
  };
  shipped_status: {
    status: string;
    label: string;
  };
  extra_statuses: {
    [status_name: string]: {
      status: string;
      label: string;
    };
  }[];
}

declare interface IQuickbaseSettings {
  past_days: number;
  frequency?: string;
  acela_webhook_url?: string;
  qb_customer_pipeline_url: string;
  qb_parts_pipeline_url: string;
  carrier_exclude_filter: string[];
}

declare interface IVRWHDeviceDetail {
  index: number;
  notes?: string;
  location: string;
  status: string;
  po_number: string;
  warehouse?: string;
  part_number: string;
  customer_po: string;
  updated_on?: string;
  customer_id?: string;
  manufacturer: string;
  customer_name: string;
  serial_number: string;
  consigned_date?: string;
  part_description: string;
  opportunity_name: string;
  shipping_address: string;
  opportunity_number: number;
  sales_order_crm_id: number;
  carrier?: string;
  carrier_tracking_url_mapping?: string;
  tracking_number?: string;
  shipping_date?: string;
}

declare interface ISowTableRow {
  id: number;
  name: string;
  category_name: string;
  category: number;
  quote_id: number;
  forecast_id: number;
  doc_type: SowDocType;
  customer: ICustomerShort;
  user: string;
  user_name: string;
  author: string;
  author_name: string;
  updated_by: string;
  opp_won_date?: string;
  updated_by_name: string;
  version: string;
  change_request?: [ICRShort];
  forecast_exception: { forecast_exception: object };
  budget_hours: number;
  margin: number;
  created_on: string;
  updated_on: string;
  quote: IQuote;
  quote_exist: boolean;
  engineering_hours: number;
  engineering_risk_hours: number;
  engineering_revenue: number;
  engineering_cost: number;
  engineering_margin: number;
  engineering_margin_percent: number;
  after_hours: number;
  after_hours_risk_hours: number;
  after_hours_revenue: number;
  after_hours_cost: number;
  after_hours_margin: number;
  after_hours_margin_percent: number;
  integration_technician_hours: number;
  integration_technician_risk_hours: number;
  integration_technician_revenue: number;
  integration_technician_cost: number;
  integration_technician_margin: number;
  integration_technician_margin_percent: number;
  project_management_hours: number;
  project_management_risk_hours: number;
  project_management_revenue: number;
  project_management_cost: number;
  project_management_margin: number;
  project_management_margin_percent: number;
  total_hourly_resource_hours: number;
  total_hourly_resource_risk_hours: number;
  hourly_resources_revenue: number;
  hourly_resources_cost: number;
  hourly_resources_margin: number;
  hourly_resources_margin_percent: number;
  professional_service_hours: number;
  professional_service_risk_hours: number;
  professional_service_revenue: number;
  professional_service_cost: number;
  professional_service_margin: number;
  professional_service_margin_percent: number;
  contractors_revenue: number;
  contractors_cost: number;
  contractors_margin: number;
  contractors_margin_percent: number;
  risk_budget_revenue: number;
  risk_budget_cost: number;
  risk_budget_margin: number;
  risk_budget_margin_percent: number;
  travel_cost: number;
  revenue: number;
  cost: number;
  margin_percent: number;
}

declare interface ISowDocListRow {
  id: number;
  margin: number;
  quote_id: number;
  customer_id: number;
  name: string;
  doc_type: string;
  customer_name: string;
  author_name: string;
  user_name: string;
  updated_by_name: string;
  created_on: string;
  updated_on: string;
  quote: { expected_close_date: string } & IQuote;
}

declare interface IProjectReview {
  id: number;
  crm_id: number;
  title: string;
  customer_id: number;
  customer_name: string;
  project_manager_id: string;
  project_manager_name: string;
  project_manager_pic: string;
  type_id: number;
  overall_status_id: number;
  overall_status_title: string;
  overall_status_color: string;
  progress_percent: number;
  closed_date: string;
  estimated_end_date: string;
  opportunity_crm_ids: number[];
  sow_document_id: number;
  project_updates: {
    estimated_hours_to_complete: number;
    note: string;
    updates: string;
  };
}

declare interface IChangeRequest {
  name: string;
  change_request_type: SowDocType;
  id?: number;
  requested_by: string;
  created_on?: string;
  assigned_to: string;
  status: string;
  json_config: IChangeRequestConfig;
  doc_type?: string;
  customer?: string;
  user?: string;
  category?: number;
  project?: any;
  sow_document?: number;
  updated_on?: string;
  change_number?: number;
}

declare interface ISoWTemplate {
  id: number;
  name: string;
  version?: string;
  category: number;
  updated_on?: string;
  updated_by?: string;
  created_on?: string;
  author_name?: string;
  major_version: number;
  minor_version: number;
  category_name?: string;
  updated_by_name?: string;
  update_version?: boolean;
  json_config: IJSONConfig;
  version_description?: string;
  service_catalog_category?: any;
  linked_service_catalog?: any;
  provider?: number;
}

declare interface IJSONConfig {
  implementation_logistics?: IImplementationLogistics;
  project_details: {
    out_of_scope: IJSONConfigSectionInfo;
    project_assumptions: IJSONConfigSectionInfo;
    request_description: IJSONConfigSectionInfo;
    task_deliverables: IJSONConfigSectionInfo;
  } & IJSONConfigSection;
  change_affects?: IJSONConfigSectionGeneric<CRConfigSectionKeys.change_affects>;
  project_management_fixed_fee: IJSONConfigSectionGeneric<"project_management">;
  project_management_t_and_m: IJSONConfigSectionGeneric<"project_management">;
  project_summary: IJSONConfigSectionGeneric<"project_summary">;
  terms_fixed_fee: {
    ["terms"]: IJSONConfigSectionInfo;
    ["generated_terms"]: IJSONConfigSectionInfo;
  } & IJSONConfigSection;
  terms_t_and_m: IJSONConfigSectionGeneric<"terms">;
  service_cost: IServiceCost;
  forecase_exception?: {
    exception_in_forecast_creation: boolean;
    message: string;
  };
}

declare interface IChangeRequestConfig {
  alternates: IJSONConfigSectionGeneric<"alternates">;
  change_affects: IJSONConfigSectionGeneric<"change_affects">;
  change_description: IJSONConfigSectionGeneric<"change_description">;
  change_impact: IJSONConfigSectionGeneric<"change_impact">;
  reason: IJSONConfigSectionGeneric<"reason">;
  risk: IJSONConfigSectionGeneric<"risk">;
  service_cost: IServiceCost;
  technical_changes: IJSONConfigSectionGeneric<"technical_changes">;
}

declare interface IServiceCost {
  staging?: boolean;
  after_hours: number;
  after_hours_override: boolean;
  default_ps_risk: number;
  after_hours_rate: number;
  contractors: IContractor[];
  customer_cost_fixed_fee?: number;
  customer_cost_t_and_m_fee?: number;
  engineering_hourly_rate: number;
  engineering_hours: number;
  integration_technician_hours: number;
  integration_technician_hours_override: boolean;
  integration_technician_hourly_rate: number;
  hide_engineering_hours: boolean;
  hide_after_hours: boolean;
  hide_travel_expenses?: boolean;
  hide_project_management_hours: boolean;
  hide_integration_technician_hours: boolean;
  total_revenue_rounding_offset?: number;
  engineering_hours_breakup?: {
    total_cutovers: number;
    total_sites: number;
    total_sites_override: boolean;
    total_cutovers_override: boolean;
    phases: IPhase[];
  };
  bill_travel_expenses?: boolean;
  milestones?: IMilestone[];
  hourly_resources?: IHourlyResource[];
  engineering_hours_override: boolean;
  internal_cost_fixed_fee?: number;
  internal_cost_t_and_m_fee?: number;
  preview_travel_costs?: boolean;
  notes: string;
  notesMD: string;
  project_management_hourly_rate: number;
  project_management_hours: number;
  project_management_hours_override: boolean;
  total_hours: number;
  static_fields?: boolean;
  customer_cost?: number;
  internal_cost?: number;
  travels: {
    description: string;
    cost: number;
    errorDescription?: IFieldValidation;
    errorCost?: IFieldValidation;
  }[];
  is_zero_dollar_change_request?: boolean;
}

declare interface IContractor {
  customer_cost: number;
  margin_percentage: number;
  name: string;
  partner_cost: number;
  type: "Service" | "Product";
  vendor_id?: number;
  description?: string;
  errorName?: IFieldValidation;
  errorRate?: IFieldValidation;
  errorMargin?: IFieldValidation;
}

declare interface IHourlyResource {
  id?: number;
  hours: number;
  override: boolean;
  hourly_rate: number;
  hourly_cost: number;
  resource_id: number;
  resource_name: string;
  internal_cost: number;
  customer_cost: number;
  margin: number;
  margin_percentage: number;
  is_hidden: boolean;
  resource_description?: string;
  errorName?: IFieldValidation;
  errorRate?: IFieldValidation;
  errorCost?: IFieldValidation;
}

declare interface IMilestone {
  id?: number;
  name: string;
  terms: string;
  milestone_percent: number;
  errorName?: IFieldValidation;
  errorTerms?: IFieldValidation;
  errorMargin?: IFieldValidation;
}

declare interface IPhase {
  id?: number;
  content?: JSX.Element;
  name: string;
  after_hours: boolean;
  default?: boolean;
  hours: number;
  override?: boolean;
  resource?: number;
  resource_name?: string;
  errorName?: IFieldValidation;
  errorResource?: IFieldValidation;
}

declare interface IJSONConfigSection {
  default_hidden: boolean;
  ordering: number;
  section_label: string;
  visible_in: SowDocType[];
}

declare interface IImplementationLogistics extends IJSONConfigSection {
  hide_in_sow?: boolean;
  sections: {
    column1?: IJSONConfigSectionInfo;
    column2?: IJSONConfigSectionInfo;
    column3?: IJSONConfigSectionInfo;
    column4?: IJSONConfigSectionInfo;
    delivery_model: IJSONConfigSectionInfo;
    detail: IJSONConfigSectionInfo;
  }[];
}

declare interface IJSONConfigSectionGeneric<T extends JSONConfigSectionKeys>
  extends IJSONConfigSection {
  [T]: IJSONConfigSectionInfo;
}

declare interface IJSONConfigSectionInfo {
  input_type: string;
  is_label_editable: string | boolean;
  is_required?: string | boolean;
  reset?: boolean;
  type?: string;
  options?: string[] | { label: string; active: boolean }[];
  label: string;
  ordering: number;
  section_label: string;
  value: string;
  value_markdown?: string;
}

declare interface ICategoryList {
  id: number;
  name: string;
  technology_types: {
    id?: number;
    name: string;
    is_disabled?: boolean;
  }[];
  technology_types_list?: string[];
}

declare interface IOutlookEmailItem {
  id: string;
  mail: string;
  jobTitle: string;
  displayName: string;
}
