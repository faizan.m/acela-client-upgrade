declare type AgreementProductType = "Service" | "Hardware" | "Comment";

declare type AgreementBillingTerm = "Monthly" | "Annually" | "One Time" | "NA";

declare type MeetingType =
  | "Kickoff"
  | "Internal Kickoff"
  | "Status"
  | "Close Out"
  | "Customer Touch";

declare type PAX8BillingTerm =
  | "Monthly"
  | "Annual"
  | "One-Time"
  | "3-Year"
  | "2-Year"
  | "Trial"
  | "Not Set";

declare type CatalogBillingTerm = "Not Set" | "Monthly" | "Annual" | "One Time";

declare type CatalogContractTerm =
  | "Monthly"
  | "12 Months"
  | "24 Months"
  | "36 Months"
  | "One Time";

declare type CatalogProductType =
  | "Service Contract"
  | "Consumption"
  | "One Time Service"
  | "Not Set";

declare type CompliancePriority = "LOW" | "MEDIUM" | "HIGH";

interface IReduxStore {
  appState: IAppStateStore;
  routing: any;
  auth: IAuthStore;
  sales: ISalesStore;
  user: IUserStore;
  customer: ICustomerStore;
  customerUser: ICustomerUserStore;
  provider: IProviderStore;
  providerUser: IProviderUserStore;
  providerIntegration: IProviderIntegrationStore;
  providerAdminUser: IProviderAdminUserStore;
  serviceRequest: IServiceRequestStore;
  superuser: ISuperUserStore;
  integration: IIntegrationStore;
  userType: IUserTypeStore;
  profile: IProfilestore;
  pmo: IPMOStore;
  setting: ISettingsStore;
  inventory: IInventoryStore;
  report: IReportStore;
  renewal: IRenewalStore;
  dashboard: IDashboardStore;
  documentation: IDocumentationstore;
  sow: ISOWStore;
  subscription: ISubscriptionStore;
  configuration: IConfigurationStore;
  sowDashboard: ISOWDashboardStore;
  orderTracking: IOrderTrackingStore;
  collector: ICollectorStore;
  agreement: IAgreementStore;
  virtualWarehouse: IVRWarehouseStore;
}

declare interface IAuthStore {
  isLoggingIn: boolean;
  isAuthenticated: boolean;
  isProductAdmin: boolean;
  isSiteAdmin: boolean;
  isaccelaAdmin: boolean;
  userProfile: IUserProfile;
  error: any;
  fetchingToken: boolean;
}

declare interface IAppStateStore {
  loadingComponents: number;
  errors: IHash;
  warnings: IHash;
  successMessage: IHash;
  infoMessage: IHash;
  prevLocation: string;
  currentLocation: string;
}

declare interface IColumnInfinite<T> {
  id?: string | number;
  name: string;
  ordering?: string;
  className?: string;
  Cell?: (row: T, index: number) => void;
}

declare interface IListColumn<T> {
  id?: keyof T;
  name: string;
  ordering?: string;
  className?: string;
  Cell?: (row: T, index?: number) => JSX.Element | string | number;
}

declare interface IUserStore {
  currentUser: IUser;
  users: IUser[];
  productUsersViewACL: IUser[];
  productUsersCreateACL: IUser[];
  productUsersModifyACL: IUser[];
}

declare interface IProfilestore {
  user: ISuperUser;
  isFetching: boolean;
  customerUsers: ISuperUser[];
  isUsersFetching: boolean;
}
declare interface IPMOStore {
  pmoDashboardFilters: {
    inputValue: string;
    projectsByPM: IDoughnutChartObject[];
    projectsByStatus: IDoughnutChartObject[];
    projectsByClosingDate: IDoughnutChartObject[];
    projectsByType: IDoughnutChartObject[];
    pagination: IServerPaginationParams & IPMODashboardFilterParams;
  };
  projectTickets: IPickListOptions[];
  meetingTemplates: IMeetingEmailTemplate[];
  lastVisitedSite: string;
  projectDetailsFilters: object;
  isFetching: boolean;
  additionalSetting: IAdditionSettingPMO;
}

declare interface ISalesStore {
  error?: string;
  isFetching: boolean;
  productValidationInfo: IProductValidation;
}

declare interface IMeetingEmailTemplate {
  id?: number;
  isCreated?: boolean;
  email_subject: string;
  email_body_text: string;
  email_body_markdown: string;
  meeting_type?: MeetingType;
  project_update?: string;
  add_internal_contacts?: boolean;
  add_customer_contacts?: boolean;
  add_additional_contacts?: boolean;
}

declare interface IMeetingTemplate {
  id?: number;
  template_name: string;
  action_items?: string[];
  agenda_topics: IAgendaTopic[];
  edited?: boolean;
  open?: boolean;
  titleError?: string;
  hasError?: boolean;
  meeting_type?: string;
}

declare interface ProjectContextValue extends ICommonProps {
  projectID: number;
  loadingProject: boolean;
  project: IProjectDetails;
  projectTeam: IProjectTeamMember[];
  customerContacts: IProjectCustomerContact[];
  additionalContacts: IProjectAdditionalContact[];
  meetingList: IProjectMeeting[];
  projectStatusList: IStatusImpactItem[];
  updateProject: (project: Partial<IProjectDetails>) => void;
}

declare interface IAdditionSettingPMO {
  default_project_manager?: string;
}

declare interface IDoughnutChartObject {
  value: number;
  color?: string;
  label: string;
  filter: boolean;
  id?: string;
  count?: number;
}

declare interface IDLabelObject {
  id: number;
  label: string;
  name?: string;
}
declare interface ISettingsStore {
  publicKey: string;
  serviceCatalogCategories: any;
  agreementMargin: any;
  serviceType: ICategoryList[];
  isFetching: boolean;
  isFetchingType: boolean;
  isFetchingShipments: boolean;
  isFetchingDocSet: boolean;
  isFetchingCWTeamRoles: boolean;
  isFetchingCustomerTypes: boolean;
  isFetchingCustomerStatuses: boolean;
  isFetchingPAX8Credentials: boolean;
  error: string;
  docSetting: IDOCSetting;
  smartNetSetting: ISmartNetSetting;
  purchaseOrderSetting: IPurchaseOrderSetting;
  attachmentsList: any;
  ingramCredentials: IIngramCredentials;
  pax8Credentials: IPAX8Credentials;
  receivedStatusList: IPickListOptions[];
  billingStatusOptions: IPickListOptions[];
  customerTypes: IPickListOptions[];
  customerStatuses: IPickListOptions[];
  cwTeamRoles: IPickListOptions[];
  shipments: IPickListOptions[];
}
declare interface ISOWStore {
  template: any;
  templates: any[];
  baseTemplates: {
    base_config: IJSONConfig;
    change_request_config: IChangeRequestConfig;
  };
  isFetching: boolean;
  isFetchingTemplate: boolean;
  isFetchingTemplates: boolean;
  isFetchingVendors: boolean;
  categoryList: ICategoryList[];
  isFetchingCategory: boolean;
  sow: any;
  sowList: ISOWShort[];
  qStageList: IDLabelObject[];
  qTypeList: IDLabelObject[];
  qStatusList: IDLabelObject[];
  isFetchingQTypeList: boolean;
  isFetchingQStageList: boolean;
  isFetchingQStatusList: boolean;
  isFetchingSow: boolean;
  isFetchingSowList: boolean;
  quote: IQuote;
  isFetchingSingleQuote: boolean;
  createtingCR: boolean;
  uploadingImages: boolean;
  businessUnits: any;
  fetchingBusinessUnits: boolean;
  isFetchingSC: boolean;
  isFetchingSCList: boolean;
  serviceCatalogList: IserviceCatalog[];
  serviceCatalog: IserviceCatalog;
  serviceCatalogShortList: IserviceCatalogShort[];
  serviceCatalogCategories: IserviceTechnologyTypes[];
  documentTypes: string[];
  sowHistory: any;
  isFetchingHistory: boolean;
  sowCreateHistory: any[];
  isFetchingCreateHistory: boolean;
  sowDeleteHistory: any[];
  isFetchingDeleteHistory: boolean;
  NotesList: IPaginatedDefault & { results: ISalesNote[] };
  SOWMetricsTable: any;
  contactTypes: IPickListOptions[];
  vendorOptions: IPickListOptions[];
  vendorMapping: IVendorAliasMapping[];
  isFetchingVendorMapping: boolean;
}

declare interface IAgreementStore {
  template: any;
  templates: any[];
  isFetching: boolean;
  isFetchingTemplate: boolean;
  isFetchingTemplates: boolean;
  agreementList: IPaginatedDefault & { results: IAgreementShort[] };
  isFetchingAgreements: boolean;
}

declare interface ISOWDashboardStore {
  service_activity: ISOWDashboardField[];
  booked_service_mixed: ISOWDashboardField[];
  rawData: any;
  sowCreated: any;
  expectedWon: any;
  isFetchingActivity: boolean;
  isFetchingSMixed: boolean;
  isFetchingSOWCreated: boolean;
  isFetchingExpected: boolean;
  isFetchingSRawData: boolean;
}

declare interface IDocumentationstore {
  noteType: any[];
  customerNotes: ICustomerNote[];
  isFetching: boolean;
  networkNote: string;
  customerEscalations: any[];
  escalationFields: IEscalationFields;
  distributionList: IDistributionList;
  users: ISuperUser[];
  circuitInfoList: ICircuitInfo[];
  circuitTypes: any[];
  documentsList: any[];
  deviceList: any[];
  isFetchingTypes: boolean;
  isFetchingDevices: boolean;
}

declare interface ICollectorStore {
  collectors: ICollector[];
  customerCollectors: ICustomerCollector[];
  customerCollector: ICustomerCollector;
  isFetchingCollectors: boolean;
  isFetchingCustomerCollectors: boolean;
  isFetchingCustomerCollector: boolean;
  isFetching: boolean;
  isFetchingURL: boolean;
  downloadURL: string;
  serviceTypes: any[];
  isFetchingServiceTypes: boolean;
  settings: any;
  queryList: any;
  query: any;
  isFetchingQueryList: boolean;
  isReportDownloading: boolean;
  isFetchingQuery: boolean;
}
declare interface ICollector {
  id?: number;
  version: any;
  download_link?: string;
  created_on?: Date;
}
declare interface ICustomerCollector {
  id?: number;
  name: string;
  collector: any;
  customer: any;
  token: string;
  status?: string;
  version?: any;
  last_heart_beat?: any;
  created_on?: Date;
}
declare interface ICollectorPlatform {
  id: number;
  platform: string;
}

declare interface ICollectorVersion {
  id: number;
  version: string;
}
declare interface IEscalationFields {
  escalation_priority_types: any;
  escalation_types: any;
}

declare interface IDistributionList {
  alerts_and_notifications: string[];
  monitoring_alerts: string[];
  scheduled_reports: string[];
  staffing_changes: string[];
}
declare interface IInventoryStore {
  devices: any;
  manufacturers: IDLabelObject[];
  contractStatuses: IContractStatus[];
  sites: any[];
  types: string[];
  error: string;
  device: IDevice;
  xlsxData: any;
  site: any;
  deviceSuccess: any;
  countries: any;
  dateFormats: any[];
  xlsxFile: any;
  availableAssociationList: any[];
  existingAssociationList: any[];
  isFetching: boolean;
  isFetchingDevice: boolean;
  isDeviceFetching: boolean;
  isCountriesFetching: boolean;
  isPostingBatch: boolean;
  fetchingXlsx: boolean;
  deviceCategoryList: IDeviceCategory[];
  providerIntegration: any;
  statusList: IDeviceStatus[];
  purchaseOrders: any[];
  purchaseOrdersAll: any[];
  purchaseOrderCorrection: any;
  isFetchingAllDevice: boolean;
  allDevices: any;
  lastVisitedSite: string;
}

declare interface IDeviceCategory {
  category_id: number;
  category_name: string;
}

declare interface MeetingCloseProps {
  show: boolean;
  meetingId: number;
  engineers: IProjectEngMan[];
  attendees: IMeetingAttendee[];
  projectDetails: IProjectDetails;
  meetingDetails: IProjectMeeting;
  onClose: (save?: boolean) => void;
}

declare interface ILineItem {
  id: number;
  tax: number;
  quantity: number;
  unitCost: number;
  shipDate?: string;
  vendorSku: string;
  lineNumber: number;
  closedFlag: boolean;
  description: string;
  canceledFlag: boolean;
  internalNotes: string;
  dateReceived?: string;
  serialNumbers?: string;
  trackingNumber?: string;
  receivedStatus: string;
  purchaseOrderId: number;
  total_quantity?: number;
  backorderedFlag: boolean;
  expectedShipDate?: string;
  vendorOrderNumber: string;
  displayInternalNotesFlag: boolean;
  received_serial_numbers: string[];
  partially_received_quantity: number;
  received_tracking_numbers: string[];
  product: {
    id: number;
    identifier: string;
    _info: {
      catalog_href: string;
    };
  };
  shipmentMethod: {
    id: number;
    name: string;
  };
  unitOfMeasure: {
    id: number;
    name: string;
    _info: {
      uom_href: string;
    };
  };
  _info: {
    updatedBy: string;
    enteredBy: string;
    lastUpdated: string;
    dateEntered: string;
  };
  customFields: {
    id: number;
    type: string;
    caption: string;
    entryMethod: string;
    numberOfDecimals: number;
  }[];
}

declare interface IDeviceStatus {
  id?: number;
  description: string;
  closedFlag: boolean;
  defaultFlag: boolean;
  _info: {
    lastUpdated: string;
    updatedBy: string;
  };
}

declare interface IMappingInfo {
  id?: number;
  opportunity_crm_id?: number;
  opportunity_name?: string;
  po_custom_field_present?: {
    po_custom_field_present: {
      reason: string;
      po_custom_field_present: boolean;
    };
  };
  po_number?: string;
  sales_order_crm_id?: number;
  so_linked_pos?: string[];
  purchase_order_crm_ids?: number[];
  ticket_summary?: string;
  ticket_crm_id?: number;
}

declare interface IVendorDetails {
  customer_identifier: string;
  vendor_name: string;
  address_line_1: string;
  address_line_2: string;
  city: string;
  state: string;
  zip: string;
  customer_site_name: string;
  territory_crm_id: number;
  website: string;
}

declare interface IVendorContact {
  first_name: string;
  last_name: string;
  title: string;
  office_phone_number: string;
  email: string;
  customer_crm_id: number;
  contact_type_crm_id: number;
}

declare interface IRenewalStore {
  newRenewals: any;
  pending: any;
  completed: any;
  renewalTypes: any;
  serviceLevelTypes: any;
  requestResponce: any;
  renewalCustomerList: any;
  renewalCustomerId: any;
  isProviderRenwalFetching: boolean;
  isCompletedFetching: boolean;
  isPendingFetching: boolean;
  isNewDevicesFetching: boolean;
}

declare interface IReportStore {
  devices: any;
  reportError: any;
  isFetching: boolean;
  reportTypes: any[];
  cuircuitInfoReportTypes: any[];
  reportCategories: any;
  scheduledReportByCat: any[];
  reportFrequencies: any;
  scheduledReportList: any[];
  isFetchingSH: boolean;
  customerAnalysisReportTypes: any[];
  reportDownloadStats: any[];
  financeStatuses: IPickListOptions[];
}

declare interface IDeviceCreateRes {
  total_request_items_count: number;
  successfully_updated_items_count: number;
  successfully_created_items_count: number;
  failed_updated_items_count: number;
  failed_created_items_count: number;
  import_warnings?: string[];
  import_warnings_file_info: IFileResponse;
}

declare interface ICircuitCreateRes {
  total_requested_item_count: number;
  failed_item_count: number;
  created_item_count: number;
  updated_count: number;
  error_file: IFileResponse;
}

declare interface IFileResponse {
  file_name?: string;
  file_path: string;
}

declare interface TimeEntry {
  start_time: Moment;
  end_time: Moment;
  notes: string;
  user_id: string;
  ticket_id: number;
  additionalInfo?: {
    success: boolean;
    errors: string[];
    user_name: string;
    ticket_name: string;
  };
}

declare interface IProjectTeamMember {
  id: string;
  system_member_crm_id: number;
  profile_url?: string;
  full_name: string;
  first_name: string;
  last_name: string;
  project_role: string;
  email: string;
  cell_phone_number?: string;
  office_phone_number?: string;
  full_cell_phone_number?: string;
  full_office_phone_number?: string;
}

declare interface IProjectCustomerContact {
  id?: string;
  user_id: string;
  email: string;
  contact_crm_id: number;
  name: string;
  profile_url: string;
  contact_record_id: number;
  project_role: string;
}

declare interface ICustomerContactItem {
  id: string;
  full_name: string;
  first_name: string;
  last_name: string;
  email: string;
  crm_id: number;
  contact_record_id: number;
  role: string;
  role_id: number;
  cell_phone_number?: string;
  office_phone_number?: string;
  full_cell_phone_number?: string;
  full_office_phone_number?: string;
  customer_name: string;
  profile_pic_url: string;
}

declare interface IProjectAdditionalContact {
  id?: number;
  name: string;
  email: string;
  phone: string;
  role: string;
}

declare interface IProjectStatus {
  id?: number;
  title: string;
  color: string;
  is_default: boolean;
}

declare interface IDRoleObject {
  id: number;
  role: string;
}

declare interface IProjectEngMan {
  id: string;
  member_id: number;
  first_name: string;
  last_name: string;
  name: string;
  profile_url: string;
}

interface IMeetingAttendee {
  id?: number;
  attendee?: string;
  external_contact?: string;
}

declare interface CloseMeetingEmail {
  email_body: string;
  recipients: string[];
  email_subject: string;
  project_update?: string;
  attachment?: File;
  user_attachments?: string[];
}

declare interface IErrorValidation {
  [fieldName: string]: IFieldValidation;
}

declare interface TimeEntrySetting {
  default_pm_note: string;
  default_engineer_note: string;
}

declare interface TimeEntryTickets {
  id?: number;
  project?: number;
  default_pm_ticket_id: number;
  default_engineer_ticket_id: number;
}

declare interface IProjectWorkPhase {
  id?: number;
  phase?: number;
  project?: number;
  role_type: number;
  actual_hours?: number;
  budget_hours?: number;
  description?: string;
  role_type_name?: string;
  error?: IFieldValidation;
}

declare interface IDOCSetting {
  default_ps_risk: number;
  risk_low_watermark: number;
  pm_hourly_cost: number;
  staging_text: string;
  project_management_hourly_rate: number;
  engineering_hourly_cost: number;
  engineering_hourly_rate: number;
  after_hours_rate: number;
  after_hours_cost: number;
  exclude_bill_travel_text: string;
  include_bill_travel_text: string;
  hidden_bill_travel_text: string;
  integration_technician_hourly_rate: number;
  integration_technician_hourly_cost: number;
  project_management_t_and_m: string;
  project_management_fixed_fee: string;
  terms_fixed_fee: string;
  terms_t_and_m: string;
  pm_type_t_and_m_terms: string;
  pm_type_t_and_m_name: string;
  engineering_hours_description: string;
  after_hours_description: string;
  project_management_hours_description: string;
  integration_technician_description: string;
  total_no_of_sites_statement?: string;
  total_no_of_cutovers_statement?: string;
  change_request_t_and_m_terms?: string;
  change_request_fixed_fee_terms?: string;
  change_request_t_and_m_terms_markdown?: string;
  change_request_fixed_fee_terms_markdown?: string;
}
declare interface ISmartNetSetting {
  id?: number;
  created_on?: any;
  updated_on?: any;
  order_status_alias_email: string;
  email_subject: string;
  email_body: string;
  provider?: number;
  tac_email: string;
  tac_email_subject: string;
  tac_email_body: string;
  smartnet_receiving_ticket_closing_note: string;
  email_notifications_from_email: string;
  opportunity_default_inside_sales_rep: string;
  shipment_method_id: any;
  purchase_order_open_statuses: any[];
  opportunity_business_unit_id?: number;
  opportunity_type_id?: number;
  opportunity_status_id?: number;
  email_body_markdown: string;
  partial_receive_status: string;
}

declare interface IEmailSettings {
  email_intro: string;
  email_intro_markdown: string;
  smartnet_intro: string;
  smartnet_intro_markdown: string;
  ending_paragraph: string;
  ending_paragraph_markdown: string;
}
declare interface ISection {
  name: string;
  content: string;
  value_html: string;
  is_locked: boolean;
  edited?: boolean;
  nameError?: string;
  id?: number;
  error?: IFieldValidation;
  markdownKey?: number;
}

declare interface IUserProfilePic {
  id: string;
  name: string;
  profile_pic: string;
}

declare interface IColorStatus {
  id: number;
  title: string;
  color: string;
  rank?: number;
}

declare interface ICustomerAccShort {
  id: number;
  name: string;
  account_manager_name?: string;
  account_manager_id?: number;
}

declare interface ISalesActivity {
  id?: number;
  age?: number;
  notes: string;
  due_date: any;
  updated_on?: string;
  description: string;
  opportunity_id: number;
  activity_type: string[];
  customer_contact: string;
  created_by?: IUserProfilePic;
  assigned_to: string | IUserProfilePic;
  last_updated_by?: IUserProfilePic;
  status: number | IColorStatus;
  priority: number | IColorStatus;
  customer: number | ICustomerAccShort;
}

declare interface IProductBundle {
  id?: number;
  billing_option: AgreementBillingTerm;
  product_type: AgreementProductType;
  is_bundle: boolean; // Not used in any functionality
  hasError?: boolean;
  products: IAgreementProduct[];
  is_checked: boolean;
  selected_product: number;
  customer_cost_total?: number;
  margin_total?: number;
  internal_cost_total?: number;
  discount?: number;
  is_required: boolean;
  comment: string;
}

declare interface IPAX8Product {
  id: number;
  product_crm_id: string;
  sku: string;
  product_name: string;
  short_description: string;
  vendor_name: string;
  billing_term: PAX8BillingTerm;
  commitment_term: string;
  commitment_term_in_months: number;
  pricing_type: string;
  unit_of_measurement: string;
  partner_buy_rate: number;
  suggested_retail_price: number;
  start_quantity_range: number;
  end_quantity_range: number;
  charge_type: string;
  internal_cost: number;
  customer_cost: number;
  margin: number;
  revenue: number;
  total_margin: number;
  quantity: number;
  is_comment: boolean;
  comment: string;
  min_quantity?: number;
  max_quantity?: number;
  error?: boolean;
}

declare interface IPAX8ProductDetail {
  id: number;
  product_crm_id: string;
  product_name: string;
  vendor_name: string;
  short_description: string;
  sku: string;
  vendorSku: string;
  pricing_data: IPricingData[];
}
declare interface IPricingData {
  billing_term: PAX8BillingTerm;
  commitment_term: string;
  commitment_term_in_months: number;
  pricing_rates: IPricingRates[];
  pricing_type: string;
  unit_of_measurement: string;
}

declare interface IPricingRates {
  charge_type: string;
  end_quantity_range: number;
  partner_buy_rate: number;
  start_quantity_range: number;
  suggested_retail_price: number;
}
declare interface IAgreementProduct {
  name: string;
  description: string;
  internal_cost: number;
  customer_cost: number;
  margin: number;
  quantity: number;
  error?: IFieldValidation;
}

declare interface IProductValidation {
  file_errors?: string[];
  product_errors?: {
    [productId: string]: {
      row_errors: IProductRowError[];
      config_errors: string[];
    };
  };
  valid_products?: IValidProducts;
}

declare interface IProductRowError {
  row_number: number;
  data: IProductRowItem;
  is_valid: boolean;
  errors: string[];
}

declare interface IValidProducts {
  products_to_create: IProductCatalogItem[];
  products_to_update: IProductCatalogItem[];
}

declare interface IProductCatalogItem {
  id?: number;
  product_id: string;
  name: string;
  description?: string;
  revenue_rounding: boolean;
  measurement_unit: string;
  no_of_units: number;
  product_type: CatalogProductType;
  pricing_data?: {
    ["Monthly"]?: {
      [Monthly]?: IProductPricing;
      ["12 Months"]?: IProductPricing;
      ["24 Months"]?: IProductPricing;
      ["36 Months"]?: IProductPricing;
    };
    ["Annual"]?: {
      [Monthly]?: IProductPricing;
      ["12 Months"]?: IProductPricing;
      ["24 Months"]?: IProductPricing;
      ["36 Months"]?: IProductPricing;
    };
    ["One Time"]?: {
      ["One Time"]?: IProductPricing;
    };
  };
}

declare interface IProductRowItem extends IProductCatalogItem, IProductPricing {
  billing_option: CatalogBillingTerm;
  contract_term: CatalogContractTerm;
}

declare interface IQuotedCatalogProduct extends IProductCatalogItem {
  billing_term: CatalogBillingTerm;
  contract_term: CatalogContractTerm;
  internal_cost: number;
  customer_cost: number;
  margin: number;
  revenue: number;
  total_margin: number;
  quantity: number;
  is_comment: boolean;
  is_required: boolean;
  vendor_name: string;
  comment?: string;
  error?: boolean;
}

declare interface IProductPricing {
  customer_cost: number;
  internal_cost: number;
  margin: number;
  margin_percent: number;
  annual_customer_cost: number;
}

declare interface IAgreementTemplate {
  created_on?: Date;
  updated_on?: Date;
  provider?: number;
  name: string;
  id?: any;
  author?: string;
  updated_by?: string;
  is_disabled?: boolean;
  major_version?: number;
  minor_version?: number;
  update_major_version?: boolean;
  set_user_as_author?: boolean;
  sections?: ISection[];
  products: IProductBundle[];
  pax8_products?: {
    name: string;
    products: IPAX8Product[];
  };
  catalog_products_name?: string;
  catalog_products?: IQuotedCatalogProduct[];
  min_discount?: number;
  max_discount?: number;
  updated_by_name?: string;
  author_name?: string;
  version_description?: string;
  version?: string;
}

declare interface IAgreement {
  created_on?: Date;
  updated_on?: Date;
  provider?: number;
  name: string;
  id?: any;
  author?: string;
  updated_by?: string;
  is_disabled?: boolean;
  major_version?: number;
  minor_version?: number;
  update_major_version?: boolean;
  set_user_as_author?: boolean;
  sections?: ISection[];
  products: IProductBundle[];
  min_discount?: number;
  max_discount?: number;
  updated_by_name?: string;
  author_name?: string;
  version_description?: string;
  user?: string;
  customer?: number;
  stage_id?: number;
  quote_id?: number;
  template?: number;
  customer_cost?: number;
  internal_cost?: number;
  margin_percentage?: number;
  margin?: number;
  total_discount?: number;
  user_name?: string;
  version?: string;
  discount_percentage: number;
  template_name?: string;
  pax8_products: {
    name: string;
    products: IPAX8Product[];
  };
  catalog_products_name?: string;
  catalog_products: IQuotedCatalogProduct[];
  is_current_version?: boolean;
  parent_agreement?: number;
  root_agreement?: number;
  forecast_data?: {
    forecast_error_details: {
      error_in_forecast_creation: boolean;
      error_message: string;
    };
    forecast_id: number;
  };
}

declare interface ProductCategoryMapping {
  licences: string[];
  contracts: string[];
}

declare interface IOrderTrackingExtraConfig {
  email_settings: IEmailSettings;
  product_category_mapping: any;
  carrier_tracking_url: ICarrierTrackingUrl;
  part_number_exclusion?: string[];
  received_status_mapping?: {
    received_status_id: number;
    mapped_status: string;
  }[];
}

declare interface ICarrierTrackingUrl {
  [carrierName: string]: {
    name: string;
    tracking_url: string;
    connectwise_shipper: number;
  };
}

declare interface CustomLineItem extends ILineItem {
  carrier?: string;
  error?: string;
  opportunity?: any;
  selected?: boolean;
  isIngram?: boolean;
  ship_date?: string;
  configuration?: any;
  received_qty?: number;
  configAdded?: boolean;
  received_status?: string;
  serial_numbers: string[];
  tracking_number: string[];
  recievedQtyOption?: string;
  shipped_quantity?: number;
  shipping_method_id: number;
  estimated_delivery_date?: string;
}

declare interface IIngramData {
  carrier: string;
  ship_date: string;
  tracking_number: string[];
  serial_numbers: string[];
  shipped_quantity: number;
  estimated_delivery_date: string;
}

declare interface IQuote {
  company_id: number;
  company_name: string;
  created_on: string;
  id: number;
  name: string;
  stage_id: number;
  stage_name: string;
  status_id: number;
  status_name: string;
  type_id: number;
  type_name: string;
  closed_date?: string;
}

declare interface ISoWCalculationFields {
  engRiskHours?: number;
  ahRiskHours?: number;
  pmRiskHours?: number;
  itRiskHours?: number;
  hourlyLaborRiskHours?: number;
  totalHours?: number;
  totalRiskHours?: number;
  hourlyLaborTotalHours?: number;
  engineeringHoursInternalCost: number;
  engineeringHoursCustomerCost: number;
  engineeringHoursMargin: number;
  engineeringHoursMarginPercent: number;
  afterHoursInternalCost: number;
  afterHoursCustomerCost: number;
  afterHoursMargin: number;
  afterHoursMarginPercent: number;
  integrationTechnicianInternalCost: number;
  integrationTechnicianCustomerCost: number;
  integrationTechnicianMargin: number;
  integrationTechnicianMarginPercent: number;
  pmHoursInternalCost: number;
  pmHoursCustomerCost: number;
  pmHoursMargin: number;
  pmHoursMarginPercent: number;
  riskBudgetInternalCost?: number;
  riskBudgetCustomerCost?: number;
  riskBudgetMargin?: number;
  riskBudgetMarginPercent?: number;
  contractorInternalCost?: number;
  contractorCustomerCost?: number;
  contractorMargin?: number;
  contractorMarginPercent?: number;
  totalCustomerCost: number;
  totalInternalCost: number;
  totalMargin: number;
  totalMarginPercent: number;
  proSerInternalCost?: number;
  proSerCustomerCost?: number;
  proSerMargin?: number;
  proSerMarginPercent?: number;
  hourlyLaborTotalRate?: number;
  hourlyLaborInternalCost: number;
  hourlyLaborCustomerCost: number;
  hourlyLaborMargin: number;
  hourlyLaborMarginPercent: number;
  travelCustomerCost?: number;
  travelInternalCost?: number;
  travelMargin?: number;
  travelMarginPercent?: number;
}

declare interface IServiceTicketBoardMapping {
  board_id: number;
  board_name: string;
  ticket_statuses: { ticket_status_id: number; ticket_status_name: string }[];
}

declare interface IOpportunityStatusSetting {
  status_id: number;
  status_name: string;
}

declare interface IOpportunityStageSetting {
  stage_id: number;
  stage_name: string;
}

declare interface IPurchaseOrderSetting {
  id?: number;
  created_on?: any;
  updated_on?: any;
  purchase_order_status_ids: any[];
  contact_type_connectwise_id: any;
  extra_config?: IOrderTrackingExtraConfig;
  default_extra_config?: IOrderTrackingExtraConfig;
  ticket_board_and_status_mapping: IServiceTicketBoardMapping[];
  opportunity_statuses: IOpportunityStatusSetting[];
  opportunity_stages: IOpportunityStageSetting[];
}

//Service Requests
declare interface IServiceRequestStore {
  requests: IServiceTicket[];
  requestDetail: IServiceTicket;
  contactList: Array<{ name: string }>;
  note: string;
  isTicketsFetching: boolean;
  isTicketFetching: boolean;
  isNotePosting: boolean;
  completedTickets: boolean;
  statusList: IDLabelObject[];
}
declare type TFetchTickets = (closed?: boolean) => void;
declare type TFetchTicket = (id: number) => void;
declare type TPostNote = (id: number, text: string) => Promise<any>;
declare type TFetchContacts = () => void;
declare type TPostTicket = (ticket: IServiceTicketNew) => Promise<any>;
declare type TFetchDocument = (
  ticketId: number,
  documentId: number
) => Promise<any>;
declare type TPostDocument = (ticketId: number, file: any) => Promise<any>;
declare type TFetchNote = () => void;

// Login
declare type TLoginRequest = (
  username: string,
  password: string
) => Promise<any>;

declare type TLoginSuccess = (UserProfile: IUserProfile) => Promise<any>;

declare type TLogout = () => void;

declare type TSetPasswordRequest = (
  setRequest: ISetPasswordRequest
) => Promise<any>;

declare type TResetPasswordRequest = (
  resetPasswordRequest: IResetPasswordRequest
) => Promise<any>;

declare type TShowSuccessMessage = (message: string) => void;
declare type TShowErrorMessage = (message: string) => void;
declare type TShowWarningMessage = (message: string) => void;
declare type TShowInfoMessage = (message: string) => void;

declare interface ICustomerStore {
  customers: ICustomerPaginated;
  users: ISuperUserPaginated;
  customer: Icustomer;
  user: ISuperUser;
  error: string;
  customersShort: ICustomerShort[];
  customerId: number;
  isFetchingCustomers: boolean;
  isFetchingCustomer: boolean;
  isFetchingUsers: boolean;
  lastVisitedTab: string;
  customersShortFetching: boolean;
}

declare interface ICustomerUserStore {
  users: ISuperUserPaginated;
  error: string;
  isFetchingUsers: boolean;
  isPostingUser: boolean;
  customerProfile: Icustomer;
}

declare interface ISuperUserStore {
  superusers: ISuperUserPaginated;
  currentSuperUser: ISuperUser;
  isFetchingUsers: boolean;
}
declare interface ISubscriptionStore {
  productMappings: ISuperUserPaginated;
  modelMappings: any;
  isFetching: boolean;
  isFetchingSubscriptions: boolean;
  isFetchingSubscription: boolean;
  isFetchingModelList: boolean;
  isLoadingModel: boolean;
  subscription: ISubscription;
  subscriptions: ISubscription[];
}

declare interface IConfigurationStore {
  isFetching: boolean;
  isFetchingList: boolean;
  isFetchingConfigList: boolean;
  isFetchingTypes: boolean;
  configuration: IConfiguration;
  configurations: IConfiguration[];
  rules: IRule[];
  rule: IRule;
  globalVariables: IVariable[];
  globalVariable: IVariable;
  customerVariables: any[];
  customerVariablesAll: any[];
  customerVariable: any;
  types: any;
  levels: string[];
  classifications: IDLabelObject[];
  isFetchingTasks: boolean;
  isTaskPolling: boolean;
  ruleHistory: any;
  isFetchingHistory: boolean;
  variableHistory: any;
  deletedHistory: any;
  isFetchingDeleted: boolean;
  createdHistory: any;
  isFetchingCreated: boolean;
  isFetchingComplianceDashboardData: boolean;
  complianceDashboardData: IComplianceDashboardData;
  detailConfiguration: any;
  isFetchingConfigDetail: boolean;
}

declare interface IComplianceDashboardData {
  rules: { [id as number]: IRule };
  top_rules_ids: number[];
  top_devices_ids: number[];
  devices: { [id as number]: IConfiguration };
  rules_violations_by_priority_obj: IRulesViolationsPriority;
}

declare interface IRulesViolationsPriority {
  ["HIGH"]?: number;
  ["MEDIUM"]?: number;
  ["LOW"]?: number;
}

declare interface IOrderTrackingStore {
  isFetching: boolean;
  orderTrackingDashboardData: any;
  serviceTickets: IServiceTicket[];
  unmappedServiceTickets: IServiceTicket[];
  isTicketsFetching: boolean;
  isFetchingLinkedPOs: boolean;
  salesOrders: IPickListOptions[];
  purchaseOrders: IPickListOptions[];
  isFetchingSalesOrders: boolean;
  isFetchingPurchaseOrders: boolean;
  linkedPOs: IPickListOptions[];
}

declare interface ICollectionsSettings {
  weekly_send_schedule: string[];
  send_to_email: string[];
  send_from_email: string;
  invoice_exclude_patterns: string[];
  approaching_due_date_ageing: number;
  email_body_template: string;
  subject: string;
  contact_type_crm_id: number;
  billing_statuses: (
    | number
    | {
        billing_status_id: number;
        billing_status_name: string;
      }
  )[];
}

declare interface ISubscriptionNotificationSettings {
  receiver_email_ids: string[];
  email_subject: string;
  email_body: string;
  renewal_type_contact_crm_id: number;
  weekly_send_schedule: string[];
  approaching_expiry_date_range: string[];
  sender: string;
  config_type_crm_ids: number[];
  config_status_crm_ids: number[];
  manufacturer_crm_ids: number[];
}

declare interface IWeeklyOrderTrackingSettings {
  receiver_email_ids: string[];
  email_subject: string;
  email_body_text: string;
  contact_type_crm_id: number;
  weekly_schedule: string[];
  sender: string;
  is_enabled: boolean;
}

declare interface IConfigMapping {
  id?: number;
  name: string;
  config_type_crm_ids: number[];
}

declare interface IFIREReportSettings {
  receiver_email_ids: string[];
  subject: string;
  frequency: string;
  past_days: number;
}

declare interface IVendorSettingsState {
  territories: number[];
  customer_type: number;
  vendor_onboarding_email_alias: string;
  vendor_onboarding_email_subject: string;
  vendor_onboarding_email_body: string;
  vendor_list_statuses?: number[];
  vendor_create_status?: number;
}

declare interface IVendorAliasMapping {
  id?: number;
  index?: number;
  vendor_name: string;
  vendor_crm_id: number;
  resource_description: string;
  resource_type?: "Hourly Resource" | "Contractor";
}

declare interface IRule {
  id?: number;
  name: string;
  level: string;
  detail: string;
  exceptions?: any;
  function?: string;
  updated_on?: string;
  description: string;
  is_global?: boolean;
  apply_not?: boolean;
  applies_to: string[];
  is_enabled?: boolean;
  created_on?: boolean;
  is_assigned?: boolean;
  valid_commands?: {
    command: string;
    line_number: number;
    detail?: string;
  }[][];
  classification: number;
  is_valid_rule?: boolean;
  deivces_list?: string[];
  to_ignore_devices?: any;
  to_include_devices?: any;
  violations_count?: number;
  classification_name?: string;
  multi_section_data?: string[];
  applies_to_ios_version?: string | string[];
  applies_to_manufacturers: string[];
  variable_values?: {
    id: number;
    is_global: boolean;
    name: string;
    type: string;
    option: string;
    description: string;
    variable_code: string;
    variable_values: string[];
  };
  invalid_commands?: {
    command: string;
    line_number?: number;
    partial_matching_commands: {
      line_number: number;
      command_diff: string[];
      found_command: string;
      match_percentage: number;
    }[];
  }[][];
}
declare interface IVariable {
  name: string;
  description: string;
  variable_code: string;
  variable_values: string[];
  type: string;
  option?: string;
  id?: number;
  is_edited?: boolean;
  is_global?: boolean;
  created_on?: string;
  updated_on?: string;
  customer_variables?: {
    variable_values: string[];
    option: string;
  };
}
declare interface IIntegrationStore {
  integrations: IIntegrationType[];
  integrationVerified: boolean;
  integrationsCatgories: any;
  manufacturerIntegrations: any;
  isFetchingGroupList: boolean;
  defaultKeyMapping: any;
  terretories: any;
  isFetchingTerretories: boolean;
  terretoryMembers: any;
  isFetchingMembers: boolean;
}

declare interface IFeatureAccess {
  id?: number;
  name?: string;
  email?: string;
  user?: string;
  users?: string[];
  feature: string;
  enabled: boolean;
}

declare interface IPurchaseHistory {
  customer_po_number: string;
  description: string;
  id: number;
  opportunity_crm_id: number;
  opportunity_name: string;
  product_id?: string;
  product?: { identifier: string };
  purchaseOrderDate: string;
  purchaseOrderId: number;
  purchaseOrderNumber: string;
  quantity: number;
  received: string;
  sales_order_crm_id: number;
  ticket_crm_id: number;
  ticket_summary: string;
  vendorInvoiceNumber: string[];
  receivedQuantity?: number;
  serialNumbers: string;
  shipDate: string;
  trackingNumber: string;
  dateReceived: string;
  shipmentMethod: { id: number; name: string };
}

declare interface IDashboardStore {
  service: Iservice;
  orders: Iorders;
  lifecycle: Ilifecycle;
  superUsers: ISupeUserDashboard;
  isServiceFetching: boolean;
  isLifeCycleFetching: boolean;
  isOrderFetching: boolean;
  isSuperUserFetching: boolean;
  quoteList: IQuote[];
  quoteFetching: boolean;
  quoteAction: boolean;
  userActivity: any[];
  userActivityFetching: boolean;
}

declare interface IUserTypeStore {
  userTypes: IUserTypeRole[];
}

declare interface IProviderUserStore {
  providerUsers: ISuperUserPaginated;
  providerUsersAll: ISuperUser[];
  currentProviderUser: ISuperUser;
  isFetchingUsers: boolean;
  isAction: boolean;
}

declare interface IProviderIntegrationStore {
  configStatus: IConfigStatus;
  providerIntegration: IProviderIntegration;
  providerExtraConfigs: IExtraConfig;
  allProviderIntegrations: IProviderIntegration[];
  boardStatusList: IDLabelObject[];
  boardManagedStatusList: any;
  isFetching: number;
  isFetchingExtraConfig: boolean;
  isFetchingStatus: boolean;
}

declare type TCreateCustomer = (currentCusomer: Icustomer) => Promise<any>;

declare type TFetchUser = (agencyId: number, userId: number) => Promise<any>;
declare type TCreateUser = (agencyId: number, user: IUser) => Promise<any>;
declare type TUpdateUser = (
  agencyId: number,
  userId: number,
  user: IUser
) => Promise<any>;

declare interface IIngramCredentials {
  client_id: string;
  client_secret: string;
  ingram_customer_id: string;
}

declare interface IPAX8Credentials {
  client_id: string;
  client_secret: string;
  vendors: string[];
}

declare interface IGraphCredentials {
  client_id: string;
  client_secret: string;
}

declare interface IIngramCredentials extends IGraphCredentials {
  ingram_customer_id: string;
  customer_number?: string;
}

declare interface IProviderStore {
  provider: IProvider;
  providers: IProviderPaginated;
  error: string;
  isProvidersFetching: boolean;
  isProviderFetching: boolean;
  themeColor: string;
  logo_url: string;
  updated_on: string;
  customer_instance_id: string;
  company_url: string;
  enable_url: boolean;
  timezones: any[];
}

declare type TFetchProvider = (providerId: number) => Promise<any>;
declare type TFetchProviderList = (
  params?: IServerPaginationParams
) => Promise<any>;
declare type TCreateProvider = (provider: IProvider) => Promise<any>;

declare type TFetchUserTypes = () => Promise<any>;

declare type TFetchSuperUsers = (
  params?: IServerPaginationParams
) => Promise<any>;

declare type TCreateSuperUser = (superUser: ISuperUser) => Promise<any>;

declare type TEditSuperUser = (
  superUserId: string,
  superUser: ISuperUser
) => Promise<any>;

declare type TActivateSuperUser = (providerUser: ISuperUser) => Promise<any>;

declare type TFetchProviderUsers = (
  params?: IServerPaginationParams
) => Promise<any>;

declare type TCreateProviderUser = (providerUser: ISuperUser) => Promise<any>;

declare type TEditProviderUser = (
  providerUserId: string,
  providerUser: ISuperUser
) => Promise<any>;

declare interface IProviderAdminUserStore {
  providerAdminUser: IProviderAdminUser;
  providerAdminUsers: IProviderAdminPaginated;
  error: string;
  isFetchingAdmins: boolean;
  isAction: boolean;
  accessFeatures: string[];
}

declare interface IVRWarehouseStore {
  error: string;
  isFetchingStatus: boolean;
  quickbaseStatus: string[];
  isFetchingWarehouse: boolean;
  quickbaseWareHouses: string[];
}

declare type TFetchProviderAdminUserList = (
  providerId: number,
  params?: IServerPaginationParams
) => Promise<any>;

declare type TAddProviderAdminUser = (
  providerAdminUser: IProviderAdminUser
) => Promise<any>;

declare type TFetchUserProfile = () => Promise<any>;

declare type TEditUserProfile = (user: ISuperUser) => Promise<any>;

declare type TFetchConfigStatus = () => Promise<any>;

declare type TFetchIntegrationsCategories = () => Promise<any>;

declare type TFetchIntegrations = () => Promise<any>;

declare type TFetchIntegrationsCategoriesTypes = (id: number) => Promise<any>;

declare type TTestIntegration = (
  catId: number,
  typeId: number,
  integration: Array<{ [name: string]: string }>
) => Promise<any>;

declare type TCreateProviderIntegrations = (
  providerIntegration: IProviderIntegrationRequest
) => Promise<any>;

declare type TFetchProviderIntegrations = (providerId: number) => Promise<any>;

declare type TFetchProviderExtraConfigs = (providerId: number) => Promise<any>;

declare type TFetchProviderProfile = () => Promise<any>;
declare type TEditProviderProfile = (data: any) => Promise<any>;

declare type TFetchCustomers = (
  params?: IServerPaginationParams
) => Promise<any>;

declare type TFetchCustomerUsers = (
  params?: IServerPaginationParams
) => Promise<any>;

declare type TPostSendMail = (user: ICustomerUser) => Promise<any>;

declare type TFetchSingleCustomer = (
  id: any,
  params?: IServerPaginationParams
) => Promise<any>;

declare type TEditCustomerUser = (id: string, user: ISuperUser) => Promise<any>;

declare type TactivateCustomerUser = (
  id: number,
  user: ISuperUser
) => Promise<any>;

declare type TEditCustomer = (id: string, customer: Icustomer) => Promise<any>;

declare type TFetchCustomerProfile = () => Promise<any>;

declare type TAddDevice = (
  customerId: number,
  newDevice: IDevice
) => Promise<any>;

declare type TAddDeviceCustomerUser = (newDevice: IDevice) => Promise<any>;

declare type TEditDevice = (deviceId: number, devide: IDevice) => Promise<any>;

declare type TUpdateContract = (
  customerId: number,
  contractNo: any,
  deviceIds: number[]
) => Promise<any>;

declare type TFetchDevices = (id: number, show: boolean) => Promise<any>;
declare type TFetchDevicesCustomerUser = (show: boolean) => Promise<any>;
declare type TFetchManufacturers = () => Promise<any>;
declare type TFetchContractStatuses = () => Promise<any>;
declare type TFetchSingleDevice = (id: number) => Promise<any>;
declare type TFetchSites = (id: number) => Promise<any>;
declare type TFetchSitesCustomerUser = () => Promise<any>;
declare type TFetchTypes = () => Promise<any>;
declare type TFetchProviderRenewals = () => Promise<any>;
declare type TRejectRenewal = (
  id: number,
  note: string,
  cust_id: number
) => Promise<any>;
declare type TFetchProviderPendingRenwals = (
  id: number,
  params?: IServerPaginationParams
) => void;
declare type TFetchProviderCompletedRenwals = (
  id: number,
  params?: IServerPaginationParams
) => void;

declare type TfetchServiceDashboardData = () => Promise<any>;
declare type TfetchLifeCycleDashboardData = () => Promise<any>;
declare type TfetchServiceDashboardDataCustomer = () => Promise<any>;
declare type TfetchLifeCycleDashboardDataCustomer = () => Promise<any>;
declare type TfetchOrdersDashboardData = () => Promise<any>;
declare type TfetchOrdersDashboardDataCustomer = () => Promise<any>;
declare type TfetchSuperUserDashboardData = () => Promise<any>;

declare type TPostEmailActivation = (id: string) => Promise<any>;

declare type TPostCustomerUser = (user: ICustomerUser) => Promise<any>;
