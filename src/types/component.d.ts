declare interface IHash {
  [key: string]: any;
}

declare interface ICommonProps {
  history: any;
  match: any;
  location: any;
}

declare type TButtonOnClick = (event: any) => void;

declare interface ISearchItem {
  name: string;
  value: any;
}
declare interface ISearchOptions {
  title?: string;
  items: ISearchItem[];
}

declare interface IInputField {
  label: any;
  value: any;
  isRequired?: boolean;
  is_required?: boolean;
  type: InputFieldType;
  options?: Array<IPickListOptions | string>;
  disabled?: string;
  id?: string;
  help_text?: string;
}

declare interface IFieldValidation {
  errorState: IValidationState;
  errorMessage: string;
}

declare interface ISideMenuOption {
  name: string;
  path: string;
  campare: string;
  icon?: string;
  childs?: ISideMenuOption[];
  showChild?: boolean;
  isAllowed?: any;
  accessLevel?: any;
  search?: any;
  isAllow?: boolean;
  condition?: any;
  isAvailable?: string;
  defaultHide?: boolean;
}

declare type ButtonStyle =
  | "primary"
  | "link"
  | "warning"
  | "danger"
  | "default"
  | "success"
  | "info";

declare type InputFieldType =
  | "DATE"
  | "SEARCH"
  | "TEXT"
  | "NUMBER"
  | "TEXTAREA"
  | "PICKLIST"
  | "PASSWORD"
  | "RADIO"
  | "FILE"
  | "COLOR"
  | "CUSTOM";

declare type IValidationState = "success" | "warning" | "error";

declare interface ITableColumn {
  id?: string;
  accessor: string;
  Header: any;
  sortable?: boolean;
  Cell?: (row: any) => any;
  show?: boolean;
  width?: number;
  resizable?: boolean;
}

declare type IIntegrationsCatgories =
  | "CRM"
  | "DEVICE_INFO"
  | "DEVICE_MONITOR"
  | "STORAGE_SERVICE";

declare type ISetingsType = "CISCO" | "CONNECTWISE";
