import { CALL_API } from '../../middleware/ApiMiddleware';

export const FETCH_DEVICE_MANUFACTURER_LIST_REQUEST =
  'FETCH_DEVICE_MANUFACTURER_LIST_REQUEST';
export const FETCH_DEVICE_MANUFACTURER_LIST_SUCCESS =
  'FETCH_DEVICE_MANUFACTURER_LIST_SUCCESS';
export const FETCH_DEVICE_MANUFACTURER_LIST_FAILURE =
  'FETCH_DEVICE_MANUFACTURER_LIST_FAILURE';

export const fetchProviderConfigStatus = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/integrations/config-status',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_DEVICE_MANUFACTURER_LIST_REQUEST,
      FETCH_DEVICE_MANUFACTURER_LIST_SUCCESS,
      FETCH_DEVICE_MANUFACTURER_LIST_FAILURE,
    ],
  },
});
