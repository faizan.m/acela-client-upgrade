import { CALL_API } from '../../middleware/ApiMiddleware';

export const FETCH_PROVIDER_USERS_REQUEST = 'FETCH_PROVIDER_USERS_REQUEST';
export const FETCH_PROVIDER_USERS_SUCCESS = 'FETCH_PROVIDER_USERS_SUCCESS';
export const FETCH_PROVIDER_USERS_FAILURE = 'FETCH_PROVIDER_USERS_FAILURE';

export const FETCH_PROVIDER_USERS_ALL_REQUEST =
  'FETCH_PROVIDER_USERS_ALL_REQUEST';
export const FETCH_PROVIDER_USERS_ALL_SUCCESS =
  'FETCH_PROVIDER_USERS_ALL_SUCCESS';
export const FETCH_PROVIDER_USERS_ALL_FAILURE =
  'FETCH_PROVIDER_USERS_ALL_FAILURE';

export const PROVIDER_USER_CREATE_REQUEST = 'PROVIDER_USER_CREATE_REQUEST';
export const PROVIDER_USER_CREATE_SUCCESS = 'PROVIDER_USER_CREATE_SUCCESS';
export const PROVIDER_USER_CREATE_FAILURE = 'PROVIDER_USER_CREATE_FAILURE';

export const PROVIDER_USER_EDIT_REQUEST = 'PROVIDER_USER_EDIT_REQUEST';
export const PROVIDER_USER_EDIT_SUCCESS = 'PROVIDER_USER_EDIT_SUCCESS';
export const PROVIDER_USER_EDIT_FAILURE = 'PROVIDER_USER_EDIT_FAILURE';

export const PROVIDER_USER_DELETE_REQUEST = 'PROVIDER_USER_DELETE_REQUEST';
export const PROVIDER_USER_DELETE_SUCCESS = 'PROVIDER_USER_DELETE_SUCCESS';
export const PROVIDER_USER_DELETE_FAILURE = 'PROVIDER_USER_DELETE_FAILURE';

export const fetchProviderUsers = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/users',
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_PROVIDER_USERS_REQUEST,
      FETCH_PROVIDER_USERS_SUCCESS,
      FETCH_PROVIDER_USERS_FAILURE,
    ],
  },
});

export const fetchAllProviderUsers = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/users?pagination=false',
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_PROVIDER_USERS_ALL_REQUEST,
      FETCH_PROVIDER_USERS_ALL_SUCCESS,
      FETCH_PROVIDER_USERS_ALL_FAILURE,
    ],
  },
});

export const fetchProviderUsersForAdmin = (providerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/${providerId}/users?pagination=false`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_USERS_ALL_REQUEST,
      FETCH_PROVIDER_USERS_ALL_SUCCESS,
      FETCH_PROVIDER_USERS_ALL_FAILURE,
    ],
  },
});

export const createProviderUser = (user: ISuperUser) => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/users',
    method: 'post',
    authenticated: true,
    body: user,
    types: [
      PROVIDER_USER_CREATE_REQUEST,
      PROVIDER_USER_CREATE_SUCCESS,
      PROVIDER_USER_CREATE_FAILURE,
    ],
  },
});

export const editProviderUser = (providerUserId: string, user: ISuperUser) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/users/${providerUserId}`,
    method: 'put',
    authenticated: true,
    body: user,
    types: [
      PROVIDER_USER_EDIT_REQUEST,
      PROVIDER_USER_EDIT_SUCCESS,
      PROVIDER_USER_EDIT_FAILURE,
    ],
  },
});

export const deleteProviderUser = (userId: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/users/${userId}`,
    method: 'delete',
    authenticated: true,
    types: [
      PROVIDER_USER_DELETE_REQUEST,
      PROVIDER_USER_DELETE_SUCCESS,
      PROVIDER_USER_DELETE_FAILURE,
    ],
  },
});
