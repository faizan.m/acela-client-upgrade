import { CALL_API } from '../../middleware/ApiMiddleware';

export const FETCH_PROVIDER_CONFIG_STATUS_REQUEST =
  'FETCH_PROVIDER_CONFIG_STATUS_REQUEST';
export const FETCH_PROVIDER_CONFIG_STATUS_SUCCESS =
  'FETCH_PROVIDER_CONFIG_STATUS_SUCCESS';
export const FETCH_PROVIDER_CONFIG_STATUS_FAILURE =
  'FETCH_PROVIDER_CONFIG_STATUS_FAILURE';
export const CREATE_PROVIDER_INTEGRATIONS_REQUEST =
  'CREATE_PROVIDER_INTEGRATIONS_REQUEST';
export const CREATE_PROVIDER_INTEGRATIONS_SUCCESS =
  'CREATE_PROVIDER_INTEGRATIONS_SUCCESS';
export const CREATE_PROVIDER_INTEGRATIONS_FAILURE =
  'CREATE_PROVIDER_INTEGRATIONS_FAILURE';
export const FETCH_PROVIDER_INTEGRATIONS_REQUEST =
  'FETCH_PROVIDER_INTEGRATIONS_REQUEST';
export const FETCH_PROVIDER_INTEGRATIONS_SUCCESS =
  'FETCH_PROVIDER_INTEGRATIONS_SUCCESS';
export const FETCH_PROVIDER_INTEGRATIONS_FAILURE =
  'FETCH_PROVIDER_INTEGRATIONS_FAILURE';
export const FETCH_PROVIDER_EXTRA_CONFIGS_REQUEST =
  'FETCH_PROVIDER_EXTRA_CONFIGS_REQUEST';
export const FETCH_PROVIDER_EXTRA_CONFIGS_SUCCESS =
  'FETCH_PROVIDER_EXTRA_CONFIGS_SUCCESS';
export const FETCH_PROVIDER_EXTRA_CONFIGS_FAILURE =
  'FETCH_PROVIDER_EXTRA_CONFIGS_FAILURE';
export const FETCH_ALL_PROVIDER_INTEGRATIONS_REQUEST =
  'FETCH_ALL_PROVIDER_INTEGRATIONS_REQUEST';
export const FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS =
  'FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS';
export const FETCH_ALL_PROVIDER_INTEGRATIONS_FAILURE =
  'FETCH_ALL_PROVIDER_INTEGRATIONS_FAILURE';
export const UPDATE_PROVIDER_INTEGRATIONS_REQUEST =
  'UPDATE_PROVIDER_INTEGRATIONS_REQUEST';
export const UPDATE_PROVIDER_INTEGRATIONS_SUCCESS =
  'UPDATE_PROVIDER_INTEGRATIONS_SUCCESS';
export const UPDATE_PROVIDER_INTEGRATIONS_FAILURE =
  'UPDATE_PROVIDER_INTEGRATIONS_FAILURE';
  export const FETCH_PROVIDER_BOARD_STATUS_REQUEST =
  'FETCH_PROVIDER_BOARD_STATUS_REQUEST';
export const FETCH_PROVIDER_BOARD_STATUS_SUCCESS =
  'FETCH_PROVIDER_BOARD_STATUS_SUCCESS';
export const FETCH_PROVIDER_BOARD_STATUS_FAILURE =
  'FETCH_PROVIDER_BOARD_STATUS_FAILURE';
export const FETCH_CUSTOM_FS_REQUEST =
  'FETCH_CUSTOM_FS_REQUEST';
export const FETCH_CUSTOM_FS_SUCCESS =
  'FETCH_CUSTOM_FS_SUCCESS';
export const FETCH_CUSTOM_FS_FAILURE =
  'FETCH_CUSTOM_FS_FAILURE';
export const FETCH_PROVIDER_MANAGED_BOARD_STATUS_REQUEST =
  'FETCH_PROVIDER_MANAGED_BOARD_STATUS_REQUEST';
export const FETCH_PROVIDER_MANAGED_BOARD_STATUS_SUCCESS =
  'FETCH_PROVIDER_MANAGED_BOARD_STATUS_SUCCESS';
export const FETCH_PROVIDER_MANAGED_BOARD_STATUS_FAILURE =
  'FETCH_PROVIDER_MANAGED_BOARD_STATUS_FAILURE';
export const SYNC_CW_DATA_REQUEST = 'SYNC_CW_DATA_REQUEST';
export const SYNC_CW_DATA_SUCCESS = 'SYNC_CW_DATA_SUCCESS';
export const SYNC_CW_DATA_FAILURE = 'SYNC_CW_DATA_FAILURE';

export const FETCH_TER_MEMBERS_REQUEST = 'FETCH_TER_MEMBERS_REQUEST';
export const FETCH_TER_MEMBERS_SUCCESS = 'FETCH_TER_MEMBERS_SUCCESS';
export const FETCH_TER_MEMBERS_FAILURE = 'FETCH_TER_MEMBERS_FAILURE';

export const FETCH_TERRITORIES_REQUEST = 'FETCH_TERRITORIES_REQUEST';
export const FETCH_TERRITORIES_SUCCESS = 'FETCH_TERRITORIES_SUCCESS';
export const FETCH_TERRITORIES_FAILURE = 'FETCH_TERRITORIES_FAILURE';

export const SAVE_MANAGER_REQUEST = 'SAVE_MANAGER_REQUEST';
export const SAVE_MANAGER_SUCCESS = 'SAVE_MANAGER_SUCCESS';
export const SAVE_MANAGER_FAILURE = 'SAVE_MANAGER_FAILURE';

export const fetchProviderConfigStatus = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/integrations/config-status',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_CONFIG_STATUS_REQUEST,
      FETCH_PROVIDER_CONFIG_STATUS_SUCCESS,
      FETCH_PROVIDER_CONFIG_STATUS_FAILURE,
    ],
  },
});

export const createProviderIntegrations = (
  providerIntegration: IProviderIntegrationRequest
) => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/integrations',
    method: 'post',
    body: providerIntegration,
    authenticated: true,
    types: [
      CREATE_PROVIDER_INTEGRATIONS_REQUEST,
      CREATE_PROVIDER_INTEGRATIONS_SUCCESS,
      CREATE_PROVIDER_INTEGRATIONS_FAILURE,
    ],
  },
});

export const fetchProviderIntegrations = (providerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/${providerId}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_INTEGRATIONS_REQUEST,
      FETCH_PROVIDER_INTEGRATIONS_SUCCESS,
      FETCH_PROVIDER_INTEGRATIONS_FAILURE,
    ],
  },
});

export const editProviderIntegrations = (
  providerId: number,
  providerIntegration: IProviderIntegrationRequest
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/${providerId}`,
    method: 'put',
    body: providerIntegration,
    authenticated: true,
    types: [
      CREATE_PROVIDER_INTEGRATIONS_REQUEST,
      CREATE_PROVIDER_INTEGRATIONS_SUCCESS,
      CREATE_PROVIDER_INTEGRATIONS_FAILURE,
    ],
  },
});

export const fetchProviderExtraConfigs = (providerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/${providerId}/extra-configs`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_EXTRA_CONFIGS_REQUEST,
      FETCH_PROVIDER_EXTRA_CONFIGS_SUCCESS,
      FETCH_PROVIDER_EXTRA_CONFIGS_FAILURE,
    ],
  },
});

export const fetchAllProviderIntegrations = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_ALL_PROVIDER_INTEGRATIONS_REQUEST,
      FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS,
      FETCH_ALL_PROVIDER_INTEGRATIONS_FAILURE,
    ],
  },
});

export const fetchAllProviderIntegrationsWithFilter = (type: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_ALL_PROVIDER_INTEGRATIONS_REQUEST,
      FETCH_ALL_PROVIDER_INTEGRATIONS_SUCCESS,
      FETCH_ALL_PROVIDER_INTEGRATIONS_FAILURE,
    ],
  },
});

export const updateProviderIntegration = (
  providerId: number,
  providerIntegration: IProviderIntegration
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/${providerId}`,
    body: providerIntegration,
    method: 'put',
    authenticated: true,
    types: [
      UPDATE_PROVIDER_INTEGRATIONS_REQUEST,
      UPDATE_PROVIDER_INTEGRATIONS_SUCCESS,
      UPDATE_PROVIDER_INTEGRATIONS_FAILURE,
    ],
  },
});

export const updateEmptySerialNumber = (
  providerId: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/${providerId}/empty-serial-mapping`,
    body: data,
    method: 'put',
    authenticated: true,
    types: [
      'UPDATE_EMPTY_SN_REQUEST',
      'UPDATE_EMPTY_SN_SUCCESS',
      'UPDATE_EMPTY_SN_FAILURE',
    ],
  },
});

export const fetchProviderBoardStatus = (boardId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/boards/${boardId}/statuses`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_BOARD_STATUS_REQUEST,
      FETCH_PROVIDER_BOARD_STATUS_SUCCESS,
      FETCH_PROVIDER_BOARD_STATUS_FAILURE,
    ],
  },
});

export const fetchProviderBoardStatusManaged = boardId => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/boards/${boardId}/statuses`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_MANAGED_BOARD_STATUS_REQUEST,
      FETCH_PROVIDER_MANAGED_BOARD_STATUS_SUCCESS,
      FETCH_PROVIDER_MANAGED_BOARD_STATUS_FAILURE,
    ],
  },
});

export const syncCWData = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/actions/cw-device-data-sync',
    method: 'post',
    authenticated: true,
    types: [SYNC_CW_DATA_REQUEST, SYNC_CW_DATA_SUCCESS, SYNC_CW_DATA_FAILURE],
  },
});

export const syncExternal = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/actions/sync-external-services-data',
    method: 'post',
    authenticated: true,
    types: [SYNC_CW_DATA_REQUEST, SYNC_CW_DATA_SUCCESS, SYNC_CW_DATA_FAILURE],
  },
});

export const fetchTerritoryMembers = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/members',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_TER_MEMBERS_REQUEST,
      FETCH_TER_MEMBERS_SUCCESS,
      FETCH_TER_MEMBERS_FAILURE,
    ],
  },
});
export const fetchTerritories = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/territories',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_TERRITORIES_REQUEST,
      FETCH_TERRITORIES_SUCCESS,
      FETCH_TERRITORIES_FAILURE,
    ],
  },
});

export const saveTerritoryManager = (managerId: any, id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/territories/${id}`,
    method: 'put',
    authenticated: true,
    body: { manager_id: managerId },
    types: [SAVE_MANAGER_REQUEST, SAVE_MANAGER_SUCCESS, SAVE_MANAGER_FAILURE],
  },
});

export const fetchCustomFieldsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/custom-fields`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUSTOM_FS_REQUEST,
      FETCH_CUSTOM_FS_SUCCESS,
      FETCH_CUSTOM_FS_FAILURE,
    ],
  },
});