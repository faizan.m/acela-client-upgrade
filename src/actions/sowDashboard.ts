import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_S_ACTIVITY_REQUEST = 'FETCH_S_ACTIVITY_REQUEST';
export const FETCH_S_ACTIVITY_SUCCESS = 'FETCH_S_ACTIVITY_SUCCESS';
export const FETCH_S_ACTIVITY_FAILURE = 'FETCH_S_ACTIVITY_FAILURE';

export const FETCH_S_H_CREATED_REQUEST = 'FETCH_S_H_CREATED_REQUEST';
export const FETCH_S_H_CREATED_SUCCESS = 'FETCH_S_H_CREATED_SUCCESS';
export const FETCH_S_H_CREATED_FAILURE = 'FETCH_S_H_CREATED_FAILURE';

export const FETCH_SH_EXPECTED_REQUEST = 'FETCH_SH_EXPECTED_REQUEST';
export const FETCH_SH_EXPECTED_SUCCESS = 'FETCH_SH_EXPECTED_SUCCESS';
export const FETCH_SH_EXPECTED_FAILURE = 'FETCH_SH_EXPECTED_FAILURE';

export const FETCH_RAW_DATA_REQUEST = 'FETCH_RAW_DATA_REQUEST';
export const FETCH_RAW_DATA_SUCCESS = 'FETCH_RAW_DATA_SUCCESS';
export const FETCH_RAW_DATA_FAILURE = 'FETCH_RAW_DATA_FAILURE';

export const EXPORT_RAW_DATA_REQUEST = 'EXPORT_RAW_DATA_REQUEST';
export const EXPORT_RAW_DATA_SUCCESS = 'EXPORT_RAW_DATA_SUCCESS';
export const EXPORT_RAW_DATA_FAILURE = 'EXPORT_RAW_DATA_FAILURE';

export const getServiceActivity = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/services-dashboards/service-activity`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_S_ACTIVITY_REQUEST,
      FETCH_S_ACTIVITY_SUCCESS,
      FETCH_S_ACTIVITY_FAILURE,
    ],
  },
});

export const getServiceHostoryWon = () => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/services-dashboards/service-history-created-won`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_S_H_CREATED_REQUEST,
      FETCH_S_H_CREATED_SUCCESS,
      FETCH_S_H_CREATED_FAILURE,
    ],
  },
});

export const getServiceHistoryExpected = () => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/services-dashboards/service-history-expected-vs-won`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SH_EXPECTED_REQUEST,
      FETCH_SH_EXPECTED_SUCCESS,
      FETCH_SH_EXPECTED_FAILURE,
    ],
  },
});

export const getRawData = () => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/services-dashboards/raw-data`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_RAW_DATA_REQUEST,
      FETCH_RAW_DATA_SUCCESS,
      FETCH_RAW_DATA_FAILURE,
    ],
  },
});

export const exportRawData = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/services-dashboards/raw-data-export`,
    method: 'get',
    authenticated: true,
    types: [
      EXPORT_RAW_DATA_REQUEST,
      EXPORT_RAW_DATA_SUCCESS,
      EXPORT_RAW_DATA_FAILURE,
    ],
  },
});
