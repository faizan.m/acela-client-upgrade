export const IS_FETCHING = 'IS_FETCHING';
export const FETCHING_COMPLETE = 'FETCHING_COMPLETE';
export const ERROR = 'ERROR';
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const SHOW_INFO_MESSAGE = 'SHOW_INFO_MESSAGE';
export const SHOW_SUCCESS_MESSAGE = 'SHOW_SUCCESS_MESSAGE';
export const LOCATION_CHANGE = '@@router/LOCATION_CHANGE';
export const SHOW_WARNING_MESSAGE = 'SHOW_WARNING_MESSAGE';

// TODO
// Add a method to delete the error
export const removeError = (errorKey: string) => {
  return {
    type: CLEAR_ERROR,
    errorKey,
  };
};

export const addSuccessMessage = (message: string) => {
  return {
    type: SHOW_SUCCESS_MESSAGE,
    errorKey: new Date().getTime(),
    message,
  };
};

export const addWarningMessage = (message: string) => {
  return {
    type: SHOW_WARNING_MESSAGE,
    errorKey: new Date().getTime(),
    message
  }
}

export const addInfoMessage = (message: string) => {
  return {
    type: SHOW_INFO_MESSAGE,
    errorKey: new Date().getTime(),
    message
  }
}

export const addErrorMessage = (errorMessage: string) => {
  return {
    type: ERROR,
    errorKey: new Date().getTime(),
    errorMessage,
  };
};
