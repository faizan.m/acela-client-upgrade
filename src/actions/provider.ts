import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_PROVIDERS_REQUEST = 'FETCH_PROVIDERS_REQUEST';
export const FETCH_PROVIDERS_SUCCESS = 'FETCH_PROVIDERS_SUCCESS';
export const FETCH_PROVIDERS_FAILURE = 'FETCH_PROVIDERS_FAILURE';
export const FETCH_PROVIDER_REQUEST = 'FETCH_PROVIDER_REQUEST';
export const FETCH_PROVIDER_SUCCESS = 'FETCH_PROVIDER_SUCCESS';
export const FETCH_PROVIDER_FAILURE = 'FETCH_PROVIDER_FAILURE';
export const PROVIDER_CREATE_REQUEST = 'PROVIDER_CREATE_REQUEST';
export const PROVIDER_CREATE_SUCCESS = 'PROVIDER_CREATE_SUCCESS';
export const PROVIDER_CREATE_FAILURE = 'PROVIDER_CREATE_FAILURE';
export const PROVIDER_EDIT_REQUEST = 'PROVIDER_EDIT_REQUEST';
export const PROVIDER_EDIT_SUCCESS = 'PROVIDER_EDIT_SUCCESS';
export const PROVIDER_EDIT_FAILURE = 'PROVIDER_EDIT_FAILURE';
export const FETCH_PROVIDER_PROFILE_REQUEST = 'FETCH_PROVIDER_PROFILE_REQUEST';
export const FETCH_PROVIDER_PROFILE_SUCCESS = 'FETCH_PROVIDER_PROFILE_SUCCESS';
export const FETCH_PROVIDER_PROFILE_FAILURE = 'FETCH_PROVIDER_PROFILE_FAILURE';
export const EDIT_PROVIDER_PROFILE_REQUEST = 'EDIT_PROVIDER_PROFILE_REQUEST';
export const EDIT_PROVIDER_PROFILE_SUCCESS = 'EDIT_PROVIDER_PROFILE_SUCCESS';
export const EDIT_PROVIDER_PROFILE_FAILURE = 'EDIT_PROVIDER_PROFILE_FAILURE';
export const PROVIDER_RENEWAL_FETCH_REQUEST = 'PROVIDER_RENEWAL_FETCH_REQUEST';
export const PROVIDER_RENEWAL_FETCH_SUCCESS = 'PROVIDER_RENEWAL_FETCH_SUCCESS';
export const PROVIDER_RENEWAL_FETCH_FAILURE = 'PROVIDER_RENEWAL_FETCH_FAILURE';

export const FETCH_LOGO_REQUEST = 'FETCH_LOGO_REQUEST';
export const FETCH_LOGO_SUCCESS = 'FETCH_LOGO_SUCCESS';
export const FETCH_LOGO_FAILURE = 'FETCH_LOGO_FAILURE';

export const FETCH_TIMEZONES_REQUEST = 'FETCH_TIMEZONES_REQUEST';
export const FETCH_TIMEZONES_SUCCESS = 'FETCH_TIMEZONES_SUCCESS';
export const FETCH_TIMEZONES_FAILURE = 'FETCH_TIMEZONES_FAILURE';

export const fetchProviders = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: '/api/v1/superusers/providers',
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_PROVIDERS_REQUEST,
      FETCH_PROVIDERS_SUCCESS,
      FETCH_PROVIDERS_FAILURE,
    ],
  },
});

export const fetchProvider = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/providers/${id}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_REQUEST,
      FETCH_PROVIDER_SUCCESS,
      FETCH_PROVIDER_FAILURE,
    ],
  },
});

export const createProvider = (provider: IProvider) => ({
  [CALL_API]: {
    endpoint: '/api/v1/superusers/providers',
    method: 'post',
    authenticated: true,
    body: provider,
    types: [
      PROVIDER_CREATE_REQUEST,
      PROVIDER_CREATE_SUCCESS,
      PROVIDER_CREATE_FAILURE,
    ],
  },
});

export const editProvider = (provider: IProvider) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/providers/${provider.id}`,
    method: 'put',
    authenticated: true,
    body: provider,
    types: [
      PROVIDER_EDIT_REQUEST,
      PROVIDER_EDIT_SUCCESS,
      PROVIDER_EDIT_FAILURE,
    ],
  },
});

export const fetchProviderProfile = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/profile',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PROVIDER_PROFILE_REQUEST,
      FETCH_PROVIDER_PROFILE_SUCCESS,
      FETCH_PROVIDER_PROFILE_FAILURE,
    ],
  },
});

export const updateProviderProfile = (req: IProviderProfile) => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/profile',
    method: 'put',
    authenticated: true,
    body: req,
    types: [
      EDIT_PROVIDER_PROFILE_REQUEST,
      EDIT_PROVIDER_PROFILE_SUCCESS,
      EDIT_PROVIDER_PROFILE_FAILURE,
    ],
  },
});

export const fetchRenewalCustomers = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/renewal-requests-summary`,
    method: 'get',
    authenticated: true,
    types: [
      PROVIDER_RENEWAL_FETCH_REQUEST,
      PROVIDER_RENEWAL_FETCH_SUCCESS,
      PROVIDER_RENEWAL_FETCH_FAILURE,
    ],
  },
});

export const fetchLogoColor = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/profile-info',
    method: 'get',
    authenticated: false,
    types: [FETCH_LOGO_REQUEST, FETCH_LOGO_SUCCESS, FETCH_LOGO_FAILURE],
  },
});

export const fetchTimezones = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/timezones',
    method: 'get',
    authenticated: true,
    types: [FETCH_TIMEZONES_REQUEST, FETCH_TIMEZONES_SUCCESS, FETCH_TIMEZONES_FAILURE],
  },
});
