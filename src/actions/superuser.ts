import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_SUPERUSERS_REQUEST = 'FETCH_SUPERUSERS_REQUEST';
export const FETCH_SUPERUSERS_SUCCESS = 'FETCH_SUPERUSERS_SUCCESS';
export const FETCH_SUPERUSERS_FAILURE = 'FETCH_SUPERUSERS_FAILURE';
export const SUPERUSER_CREATE_REQUEST = 'SUPERUSER_CREATE_REQUEST';
export const SUPERUSER_CREATE_SUCCESS = 'SUPERUSER_CREATE_SUCCESS';
export const SUPERUSER_CREATE_FAILURE = 'SUPERUSER_CREATE_FAILURE';
export const SUPERUSER_EDIT_REQUEST = 'SUPERUSER_EDIT_REQUEST';
export const SUPERUSER_EDIT_SUCCESS = 'SUPERUSER_EDIT_SUCCESS';
export const SUPERUSER_EDIT_FAILURE = 'SUPERUSER_EDIT_FAILURE';

export const fetchSuperUsers = (params?: IServerPaginationParams) => {
  return {
    [CALL_API]: {
      endpoint: '/api/v1/superusers/users',
      method: 'get',
      authenticated: true,
      params,
      types: [
        FETCH_SUPERUSERS_REQUEST,
        FETCH_SUPERUSERS_SUCCESS,
        FETCH_SUPERUSERS_FAILURE,
      ],
    },
  };
};

export const createSuperUser = (superUser: ISuperUser) => ({
  [CALL_API]: {
    endpoint: '/api/v1/superusers/users',
    method: 'post',
    authenticated: true,
    body: superUser,
    types: [
      SUPERUSER_CREATE_REQUEST,
      SUPERUSER_CREATE_SUCCESS,
      SUPERUSER_CREATE_FAILURE,
    ],
  },
});

export const editSuperUser = (superUserId: number, superUser: ISuperUser) => ({
  [CALL_API]: {
    endpoint: `/api/v1/superusers/users/${superUserId}`,
    method: 'put',
    authenticated: true,
    body: superUser,
    types: [
      SUPERUSER_EDIT_REQUEST,
      SUPERUSER_EDIT_SUCCESS,
      SUPERUSER_EDIT_FAILURE,
    ],
  },
});
