import { CALL_API } from '../middleware/ApiMiddleware';
import { BATCH_CREATE_REQUEST, BATCH_CREATE_SUCCESS, BATCH_CREATE_FAILURE } from './inventory';

export const FETCH_NOTE_TYPES_REQUEST = 'FETCH_NOTE_TYPES_REQUEST';
export const FETCH_NOTE_TYPES_SUCCESS = 'FETCH_NOTE_TYPES_SUCCESS';
export const FETCH_NOTE_TYPES_FAILURE = 'FETCH_NOTE_TYPES_FAILURE';

export const FETCH_CUSTOMER_NOTES_REQUEST = 'FETCH_CUSTOMER_NOTES_REQUEST';
export const FETCH_CUSTOMER_NOTES_SUCCESS = 'FETCH_CUSTOMER_NOTES_SUCCESS';
export const FETCH_CUSTOMER_NOTES_FAILURE = 'FETCH_CUSTOMER_NOTES_FAILURE';

export const ADD_CUSTOMER_NOTE_REQUEST = 'ADD_CUSTOMER_NOTE_REQUEST';
export const ADD_CUSTOMER_NOTE_SUCCESS = 'ADD_CUSTOMER_NOTE_SUCCESS';
export const ADD_CUSTOMER_NOTE_FAILURE = 'ADD_CUSTOMER_NOTE_FAILURE';

export const FETCH_NETWORK_NOTES_REQUEST = 'FETCH_NETWORK_NOTES_REQUEST';
export const FETCH_NETWORK_NOTES_SUCCESS = 'FETCH_NETWORK_NOTES_SUCCESS';
export const FETCH_NETWORK_NOTES_FAILURE = 'FETCH_NETWORK_NOTES_FAILURE';

export const ADD_NETWORK_NOTE_REQUEST = 'ADD_NETWORK_NOTE_REQUEST';
export const ADD_NETWORK_NOTE_SUCCESS = 'ADD_NETWORK_NOTE_SUCCESS';
export const ADD_NETWORK_NOTE_FAILURE = 'ADD_NETWORK_NOTE_FAILURE';

export const ADD_ESCALATION_FIELDS_REQUEST = 'ADD_ESCALATION_FIELDS_REQUEST';
export const ADD_ESCALATION_FIELDS_SUCCESS = 'ADD_ESCALATION_FIELDS_SUCCESS';
export const ADD_ESCALATION_FIELDS_FAILURE = 'ADD_ESCALATION_FIELDS_FAILURE';

export const ADD_CIRCUIT_INFO_REQUEST = 'ADD_CIRCUIT_INFO_REQUEST';
export const ADD_CIRCUIT_INFO_SUCCESS = 'ADD_CIRCUIT_INFO_SUCCESS';
export const ADD_CIRCUIT_INFO_FAILURE = 'ADD_CIRCUIT_INFO_FAILURE';

export const ADD_ESCALATION_REQUEST = 'ADD_ESCALATION_REQUEST';
export const ADD_ESCALATION_SUCCESS = 'ADD_ESCALATION_SUCCESS';
export const ADD_ESCALATION_FAILURE = 'ADD_ESCALATION_FAILURE';

export const DELETE_ESCALATION_REQUEST = 'DELETE_ESCALATION_REQUEST';
export const DELETE_ESCALATION_SUCCESS = 'DELETE_ESCALATION_SUCCESS';
export const DELETE_ESCALATION_FAILURE = 'DELETE_ESCALATION_FAILURE';

export const FETCH_ALL_CUST_USERS_REQUEST = 'FETCH_ALL_CUST_USERS_REQUEST';
export const FETCH_ALL_CUST_USERS_SUCCESS = 'FETCH_ALL_CUST_USERS_SUCCESS';
export const FETCH_ALL_CUST_USERS_FAILURE = 'FETCH_ALL_CUST_USERS_FAILURE';

export const FETCH_CIRCUIT_INFO_REQUEST = 'FETCH_CIRCUIT_INFO_REQUEST';
export const FETCH_CIRCUIT_INFO_SUCCESS = 'FETCH_CIRCUIT_INFO_SUCCESS';
export const FETCH_CIRCUIT_INFO_FAILURE = 'FETCH_CIRCUIT_INFO_FAILURE';

export const FETCH_CIRCUIT_TYPE_REQUEST = 'FETCH_CIRCUIT_TYPE_REQUEST';
export const FETCH_CIRCUIT_TYPE_SUCCESS = 'FETCH_CIRCUIT_TYPE_SUCCESS';
export const FETCH_CIRCUIT_TYPE_FAILURE = 'FETCH_CIRCUIT_TYPE_FAILURE';

export const CREATE_CIRCUIT_TYPE_REQUEST = 'CREATE_CIRCUIT_TYPE_REQUEST';
export const CREATE_CIRCUIT_TYPE_SUCCESS = 'CREATE_CIRCUIT_TYPE_SUCCESS';
export const CREATE_CIRCUIT_TYPE_FAILURE = 'CREATE_CIRCUIT_TYPE_FAILURE';

export const FETCH_CUSTOMER_ESCALATION_REQUEST =
  'FETCH_CUSTOMER_ESCALATION_REQUEST';
export const FETCH_CUSTOMER_ESCALATION_SUCCESS =
  'FETCH_CUSTOMER_ESCALATION_SUCCESS';
export const FETCH_CUSTOMER_ESCALATION_FAILURE =
  'FETCH_CUSTOMER_ESCALATION_FAILURE';

export const FETCH_ESCALATION_FIELDS_REQUEST =
  'FETCH_ESCALATION_FIELDS_REQUEST';
export const FETCH_ESCALATION_FIELDS_SUCCESS =
  'FETCH_ESCALATION_FIELDS_SUCCESS';
export const FETCH_ESCALATION_FIELDS_FAILURE =
  'FETCH_ESCALATION_FIELDS_FAILURE';

export const FETCH_PATH_LISTING_REQUEST = 'FETCH_PATH_LISTING_REQUEST';
export const FETCH_PATH_LISTING_SUCCESS = 'FETCH_PATH_LISTING_SUCCESS';
export const FETCH_PATH_LISTING_FAILURE = 'FETCH_PATH_LISTING_FAILURE';

export const FETCH_FOLDER_LISTING_REQUEST = 'FETCH_FOLDER_LISTING_REQUEST';
export const FETCH_FOLDER_LISTING_SUCCESS = 'FETCH_FOLDER_LISTING_SUCCESS';
export const FETCH_FOLDER_LISTING_FAILURE = 'FETCH_FOLDER_LISTING_FAILURE';

export const DOWNLOAD_FILE_REQUEST = 'DOWNLOAD_FILE_REQUEST';
export const DOWNLOAD_FILE_SUCCESS = 'DOWNLOAD_FILE_SUCCESS';
export const DOWNLOAD_FILE_FAILURE = 'DOWNLOAD_FILE_FAILURE';

export const FETCH_DEVICES_SHORT_REQUEST = 'FETCH_DEVICES_SHORT_REQUEST';
export const FETCH_DEVICES_SHORT_SUCCESS = 'FETCH_DEVICES_SHORT_SUCCESS';
export const FETCH_DEVICES_SHORT_FAILURE = 'FETCH_DEVICES_SHORT_FAILURE';

export const fetchNoteTypes = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/customers/note-types',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_NOTE_TYPES_REQUEST,
      FETCH_NOTE_TYPES_SUCCESS,
      FETCH_NOTE_TYPES_FAILURE,
    ],
  },
});

export const fetchCustomerNotes = (customerId: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/notes`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUSTOMER_NOTES_REQUEST,
      FETCH_CUSTOMER_NOTES_SUCCESS,
      FETCH_CUSTOMER_NOTES_FAILURE,
    ],
  },
});

export const addCustomerNote = (
  customerId: number,
  customerNote: ICustomerNote
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/notes`,
    method: 'post',
    body: customerNote,
    authenticated: true,
    types: [
      ADD_CUSTOMER_NOTE_REQUEST,
      ADD_CUSTOMER_NOTE_SUCCESS,
      ADD_CUSTOMER_NOTE_FAILURE,
    ],
  },
});

export const editCustomerNote = (
  customerId: number,
  customerNote: ICustomerNote
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/notes/${
      customerNote.note_id
    }`,
    method: 'put',
    body: customerNote,
    authenticated: true,
    types: [
      ADD_CUSTOMER_NOTE_REQUEST,
      ADD_CUSTOMER_NOTE_SUCCESS,
      ADD_CUSTOMER_NOTE_FAILURE,
    ],
  },
});

export const editNetworkNote = (customerId: number, networkNote: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}`,
    method: 'put',
    body: { network_note: networkNote },
    authenticated: true,
    types: [
      ADD_NETWORK_NOTE_REQUEST,
      ADD_NETWORK_NOTE_SUCCESS,
      ADD_NETWORK_NOTE_FAILURE,
    ],
  },
});

export const getNetworkNote = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_NETWORK_NOTES_REQUEST,
      FETCH_NETWORK_NOTES_SUCCESS,
      FETCH_NETWORK_NOTES_FAILURE,
    ],
  },
});

export const fetchCustomerEscalation = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/customer-escalation`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUSTOMER_ESCALATION_REQUEST,
      FETCH_CUSTOMER_ESCALATION_SUCCESS,
      FETCH_CUSTOMER_ESCALATION_FAILURE,
    ],
  },
});

export const addCustomerEscalation = (
  customerId: number,
  customerEscalation: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/customer-escalation`,
    method: 'post',
    body: customerEscalation,
    authenticated: true,
    types: [
      ADD_ESCALATION_REQUEST,
      ADD_ESCALATION_SUCCESS,
      ADD_ESCALATION_FAILURE,
    ],
  },
});

export const editCustomerEscalation = (
  customerId: number,
  customerEscalation: any,
  id: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/customer-escalation/${id}`,
    method: 'put',
    body: customerEscalation,
    authenticated: true,
    types: [
      ADD_ESCALATION_REQUEST,
      ADD_ESCALATION_SUCCESS,
      ADD_ESCALATION_FAILURE,
    ],
  },
});

export const deleteCustomerEscalation = (customerId: number, id: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/customer-escalation/${id}`,
    method: 'delete',
    authenticated: true,
    types: [
      DELETE_ESCALATION_REQUEST,
      DELETE_ESCALATION_SUCCESS,
      DELETE_ESCALATION_FAILURE,
    ],
  },
});

export const editDistributionList = (
  customerId: number,
  distributionList: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}`,
    method: 'put',
    body: { customer_distribution: distributionList },
    authenticated: true,
    types: [
      ADD_NETWORK_NOTE_REQUEST,
      ADD_NETWORK_NOTE_SUCCESS,
      ADD_NETWORK_NOTE_FAILURE,
    ],
  },
});

export const fetchEscalationFields = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customer-escalation-fields`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_ESCALATION_FIELDS_REQUEST,
      FETCH_ESCALATION_FIELDS_SUCCESS,
      FETCH_ESCALATION_FIELDS_FAILURE,
    ],
  },
});

export const fetchAllCustomerUsers = (
  id: number,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${parseInt(
      String(id),
      10
    )}/users?pagination=false`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_ALL_CUST_USERS_REQUEST,
      FETCH_ALL_CUST_USERS_SUCCESS,
      FETCH_ALL_CUST_USERS_FAILURE,
    ],
  },
});

export const fetchCircuitTypes = (show: boolean = false) => ({
  [CALL_API]: {
    endpoint: `/api/v1/circuit-info-types?show-associated-circuits=${show}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CIRCUIT_TYPE_REQUEST,
      FETCH_CIRCUIT_TYPE_SUCCESS,
      FETCH_CIRCUIT_TYPE_FAILURE,
    ],
  },
});
export const createCircuitTypes = (name: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/circuit-info-types`,
    method: 'post',
    authenticated: true,
    body: { name },
    types: [
      CREATE_CIRCUIT_TYPE_REQUEST,
      CREATE_CIRCUIT_TYPE_SUCCESS,
      CREATE_CIRCUIT_TYPE_FAILURE,
    ],
  },
});

export const editCircuitTypes = (name: string, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/circuit-info-types/${id}`,
    method: 'put',
    authenticated: true,
    body: { name },
    types: [
      CREATE_CIRCUIT_TYPE_REQUEST,
      CREATE_CIRCUIT_TYPE_SUCCESS,
      CREATE_CIRCUIT_TYPE_FAILURE,
    ],
  },
});

export const deleteCircuitTypes = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/circuit-info-types/${id}`,
    method: 'delete',
    authenticated: true,
    body: { name },
    types: [
      CREATE_CIRCUIT_TYPE_REQUEST,
      CREATE_CIRCUIT_TYPE_SUCCESS,
      CREATE_CIRCUIT_TYPE_FAILURE,
    ],
  },
});

export const fetchCircuitInfo = (
  customerId: number,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-info?pagination=false`,
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_CIRCUIT_INFO_REQUEST,
      FETCH_CIRCUIT_INFO_SUCCESS,
      FETCH_CIRCUIT_INFO_FAILURE,
    ],
  },
});

export const addCircuitInfo = (customerId: number, circuitInfo: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-info`,
    method: 'post',
    body: circuitInfo,
    authenticated: true,
    types: [
      ADD_CIRCUIT_INFO_REQUEST,
      ADD_CIRCUIT_INFO_SUCCESS,
      ADD_CIRCUIT_INFO_FAILURE,
    ],
  },
});

export const editCircuitInfo = (
  customerId: number,
  circuitInfo: any,
  id: any
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-info/${id}`,
    method: 'put',
    body: circuitInfo,
    authenticated: true,
    types: [
      ADD_CIRCUIT_INFO_REQUEST,
      ADD_CIRCUIT_INFO_SUCCESS,
      ADD_CIRCUIT_INFO_FAILURE,
    ],
  },
});

export const deleteCircuitInfo = (customerId: number, id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/circuit-info/${id}`,
    method: 'delete',
    authenticated: true,
    types: [
      ADD_CIRCUIT_INFO_REQUEST,
      ADD_CIRCUIT_INFO_SUCCESS,
      ADD_CIRCUIT_INFO_FAILURE,
    ],
  },
});
// customerUser

export const fetchCustomerNotesCU = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/notes`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUSTOMER_NOTES_REQUEST,
      FETCH_CUSTOMER_NOTES_SUCCESS,
      FETCH_CUSTOMER_NOTES_FAILURE,
    ],
  },
});

export const addCustomerNoteCU = (customerNote: ICustomerNote) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/notes`,
    method: 'post',
    body: customerNote,
    authenticated: true,
    types: [
      ADD_CUSTOMER_NOTE_REQUEST,
      ADD_CUSTOMER_NOTE_SUCCESS,
      ADD_CUSTOMER_NOTE_FAILURE,
    ],
  },
});

export const editCustomerNoteCU = (customerNote: ICustomerNote) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/notes/${customerNote.note_id}`,
    method: 'put',
    body: customerNote,
    authenticated: true,
    types: [
      ADD_CUSTOMER_NOTE_REQUEST,
      ADD_CUSTOMER_NOTE_SUCCESS,
      ADD_CUSTOMER_NOTE_FAILURE,
    ],
  },
});

export const editNetworkNoteCU = (networkNote: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers`,
    method: 'put',
    body: { network_note: networkNote },
    authenticated: true,
    types: [
      ADD_NETWORK_NOTE_REQUEST,
      ADD_NETWORK_NOTE_SUCCESS,
      ADD_NETWORK_NOTE_FAILURE,
    ],
  },
});

export const getNetworkNoteCU = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/profile`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_NETWORK_NOTES_REQUEST,
      FETCH_NETWORK_NOTES_SUCCESS,
      FETCH_NETWORK_NOTES_FAILURE,
    ],
  },
});

export const fetchCustomerEscalationCU = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/customer-escalation`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUSTOMER_ESCALATION_REQUEST,
      FETCH_CUSTOMER_ESCALATION_SUCCESS,
      FETCH_CUSTOMER_ESCALATION_FAILURE,
    ],
  },
});

export const addCustomerEscalationCU = (customerEscalation: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/customer-escalation`,
    method: 'post',
    body: customerEscalation,
    authenticated: true,
    types: [
      ADD_ESCALATION_REQUEST,
      ADD_ESCALATION_SUCCESS,
      ADD_ESCALATION_FAILURE,
    ],
  },
});

export const editCustomerEscalationCU = (
  customerEscalation: any,
  id: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/customer-escalation/${id}`,
    method: 'put',
    body: customerEscalation,
    authenticated: true,
    types: [
      ADD_ESCALATION_REQUEST,
      ADD_ESCALATION_SUCCESS,
      ADD_ESCALATION_FAILURE,
    ],
  },
});

export const deleteCustomerEscalationCU = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/customer-escalation/${id}`,
    method: 'delete',
    authenticated: true,
    types: [
      DELETE_ESCALATION_REQUEST,
      DELETE_ESCALATION_SUCCESS,
      DELETE_ESCALATION_FAILURE,
    ],
  },
});

export const editDistributionListCU = (distributionList: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/profile`,
    method: 'put',
    body: { customer_distribution: distributionList },
    authenticated: true,
    types: [
      ADD_NETWORK_NOTE_REQUEST,
      ADD_NETWORK_NOTE_SUCCESS,
      ADD_NETWORK_NOTE_FAILURE,
    ],
  },
});

export const fetchAllCustomerUsersCU = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/users?pagination=false`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_ALL_CUST_USERS_REQUEST,
      FETCH_ALL_CUST_USERS_SUCCESS,
      FETCH_ALL_CUST_USERS_FAILURE,
    ],
  },
});

export const fetchCircuitInfoCU = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/circuit-info?pagination=false`,
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_CIRCUIT_INFO_REQUEST,
      FETCH_CIRCUIT_INFO_SUCCESS,
      FETCH_CIRCUIT_INFO_FAILURE,
    ],
  },
});

export const addCircuitInfoCU = (circuitInfo: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/circuit-info`,
    method: 'post',
    body: circuitInfo,
    authenticated: true,
    types: [
      ADD_CIRCUIT_INFO_REQUEST,
      ADD_CIRCUIT_INFO_SUCCESS,
      ADD_CIRCUIT_INFO_FAILURE,
    ],
  },
});

export const editCircuitInfoCU = (circuitInfo: any, id: any) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/circuit-info/${id}`,
    method: 'put',
    body: circuitInfo,
    authenticated: true,
    types: [
      ADD_CIRCUIT_INFO_REQUEST,
      ADD_CIRCUIT_INFO_SUCCESS,
      ADD_CIRCUIT_INFO_FAILURE,
    ],
  },
});

export const fetchPathListing = (
  customerId: string,
  foldeName: string = ''
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/file-storage?path=${foldeName}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PATH_LISTING_REQUEST,
      FETCH_PATH_LISTING_SUCCESS,
      FETCH_PATH_LISTING_FAILURE,
    ],
  },
});

export const downloadFilefromDropBox = (
  customerId: number,
  filePath: string
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/file-storage/download?path=${encodeURI(
      filePath
    )}`,

    method: 'get',
    authenticated: true,
    responseType: 'blob',
    types: [
      DOWNLOAD_FILE_REQUEST,
      DOWNLOAD_FILE_SUCCESS,
      DOWNLOAD_FILE_FAILURE,
    ],
  },
});
export const uploadFiletoDropBox = (
  customerId: number,
  filePath: string,
  data:any
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/file-storage/upload?path=${encodeURI(
      filePath
    )}`,
    method: 'post',
    body: data,
    authenticated: true,
    types: [
      DOWNLOAD_FILE_REQUEST,
      DOWNLOAD_FILE_SUCCESS,
      DOWNLOAD_FILE_FAILURE,
    ],
  },
});

export const fetchPathListingCU = (foldeName: string = '') => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/file-storage?path=${foldeName}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PATH_LISTING_REQUEST,
      FETCH_PATH_LISTING_SUCCESS,
      FETCH_PATH_LISTING_FAILURE,
    ],
  },
});

export const downloadFilefromDropBoxCU = (filePath: string = '') => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/file-storage/download?path=${encodeURI(
      filePath
    )}`,
    method: 'get',
    authenticated: true,
    responseType: 'blob',
    types: [
      DOWNLOAD_FILE_REQUEST,
      DOWNLOAD_FILE_SUCCESS,
      DOWNLOAD_FILE_FAILURE,
    ],
  },
});

export const fetchDropboxFolders = (key: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/file-storage/folders`,
    method: 'post',
    authenticated: true,
    body: { access_token: key },
    types: [
      FETCH_FOLDER_LISTING_REQUEST,
      FETCH_FOLDER_LISTING_SUCCESS,
      FETCH_FOLDER_LISTING_FAILURE,
    ],
  },
});

export const fetchDevicesShort = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/devices?full-info=false`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_DEVICES_SHORT_REQUEST,
      FETCH_DEVICES_SHORT_SUCCESS,
      FETCH_DEVICES_SHORT_FAILURE,
    ],
  },
});

export const fetchDevicesShortCU = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/devices?full-info=false`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_DEVICES_SHORT_REQUEST,
      FETCH_DEVICES_SHORT_SUCCESS,
      FETCH_DEVICES_SHORT_FAILURE,
    ],
  },
});

export const importCircuits = (id: any,devices: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/circuit-info/import`,
    method: 'post',
    body: devices,
    authenticated: true,
    types: [BATCH_CREATE_REQUEST, BATCH_CREATE_SUCCESS, BATCH_CREATE_FAILURE],
  },
});
