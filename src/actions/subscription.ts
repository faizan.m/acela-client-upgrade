import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_PRODUCT_MAPPINGS_REQUEST = 'FETCH_PRODUCT_MAPPINGS_REQUEST';
export const FETCH_PRODUCT_MAPPINGS_SUCCESS = 'FETCH_PRODUCT_MAPPINGS_SUCCESS';
export const FETCH_PRODUCT_MAPPINGS_FAILURE = 'FETCH_PRODUCT_MAPPINGS_FAILURE';

export const UPDATE_PRODUCTS_REQUEST = 'UPDATE_PRODUCTS_REQUEST';
export const UPDATE_PRODUCTS_SUCCESS = 'UPDATE_PRODUCTS_SUCCESS';
export const UPDATE_PRODUCTS_FAILURE = 'UPDATE_PRODUCTS_FAILURE';

export const FETCH_SUBSCRIPTIONS_REQUEST = 'FETCH_SUBSCRIPTIONS_REQUEST';
export const FETCH_SUBSCRIPTIONS_SUCCESS = 'FETCH_SUBSCRIPTIONS_SUCCESS';
export const FETCH_SUBSCRIPTIONS_FAILURE = 'FETCH_SUBSCRIPTIONS_FAILURE';

export const EDIT_SUBSCRIPTION_REQUEST = 'EDIT_SUBSCRIPTION_REQUEST';
export const EDIT_SUBSCRIPTION_SUCCESS = 'EDIT_SUBSCRIPTION_SUCCESS';
export const EDIT_SUBSCRIPTION_FAILURE = 'EDIT_SUBSCRIPTION_FAILURE';

export const FETCH_MODEL_MAPPINGS_REQUEST = 'FETCH_MODEL_MAPPINGS_REQUEST';
export const FETCH_MODEL_MAPPINGS_SUCCESS = 'FETCH_MODEL_MAPPINGS_SUCCESS';
export const FETCH_MODEL_MAPPINGS_FAILURE = 'FETCH_MODEL_MAPPINGS_FAILURE';

export const UPDATE_MODELS_REQUEST = 'UPDATE_MODELS_REQUEST';
export const UPDATE_MODELS_SUCCESS = 'UPDATE_MODELS_SUCCESS';
export const UPDATE_MODELS_FAILURE = 'UPDATE_MODELS_FAILURE';

export const SERVICE_PROVIDER_SETTINGS_REQUEST = 'SERVICE_PROVIDER_SETTINGS_REQUEST';
export const SERVICE_PROVIDER_SETTINGS_SUCCESS = 'SERVICE_PROVIDER_SETTINGS_SUCCESS';
export const SERVICE_PROVIDER_SETTINGS_FAILURE = 'SERVICE_PROVIDER_SETTINGS_FAILURE';

export const fetchProductMappings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription/product-mappings`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PRODUCT_MAPPINGS_REQUEST,
      FETCH_PRODUCT_MAPPINGS_SUCCESS,
      FETCH_PRODUCT_MAPPINGS_FAILURE,
    ],
  },
});

export const fetchProductMappingsCU = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/subscription/product-mappings`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_PRODUCT_MAPPINGS_REQUEST,
      FETCH_PRODUCT_MAPPINGS_SUCCESS,
      FETCH_PRODUCT_MAPPINGS_FAILURE,
    ],
  },
});

export const saveProductMapping = (
  typeId: any,
  name: string,
  mappings: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription/product-mappings/${typeId}`,
    method: 'put',
    authenticated: true,
    body: { name, products: mappings },
    types: [
      UPDATE_PRODUCTS_REQUEST,
      UPDATE_PRODUCTS_SUCCESS,
      UPDATE_PRODUCTS_FAILURE,
    ],
  },
});

export const addProductMapping = (name: string, mappings: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription/product-mappings`,
    method: 'post',
    authenticated: true,
    body: { name, products: mappings },
    types: [
      UPDATE_PRODUCTS_REQUEST,
      UPDATE_PRODUCTS_SUCCESS,
      UPDATE_PRODUCTS_FAILURE,
    ],
  },
});

export const deleteProductMapping = (typeId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription/product-mappings/${typeId}`,
    method: 'delete',
    authenticated: true,
    types: [
      UPDATE_PRODUCTS_REQUEST,
      UPDATE_PRODUCTS_SUCCESS,
      UPDATE_PRODUCTS_FAILURE,
    ],
  },
});

export const fetchSubscriptions = (id: number, no: boolean = false) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${id}/subscriptions?show-inactive=${no}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SUBSCRIPTIONS_REQUEST,
      FETCH_SUBSCRIPTIONS_SUCCESS,
      FETCH_SUBSCRIPTIONS_FAILURE,
    ],
  },
});

export const fetchSubscriptionsCustomerUser = (
  showInactive: boolean = false
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/subscriptions?show-inactive=${showInactive}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SUBSCRIPTIONS_REQUEST,
      FETCH_SUBSCRIPTIONS_SUCCESS,
      FETCH_SUBSCRIPTIONS_FAILURE,
    ],
  },
});

export const editSubscriptions = (
  custId: number,
  id: number,
  subscription: ISubscription,
  no: boolean = false
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${custId}/subscriptions/${id}`,
    method: 'put',
    body: subscription,
    authenticated: true,
    types: [
      EDIT_SUBSCRIPTION_REQUEST,
      EDIT_SUBSCRIPTION_SUCCESS,
      EDIT_SUBSCRIPTION_FAILURE,
    ],
  },
});

export const editSubscriptionsCustomerUser = (
  subscription: ISubscription,
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/subscriptions/${subscription.id}`,
    method: 'put',
    body: subscription,
    authenticated: true,
    types: [
      EDIT_SUBSCRIPTION_REQUEST,
      EDIT_SUBSCRIPTION_SUCCESS,
      EDIT_SUBSCRIPTION_FAILURE,
    ],
  },
});


export const fetchModelMappings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/model-type-mapping`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_MODEL_MAPPINGS_REQUEST,
      FETCH_MODEL_MAPPINGS_SUCCESS,
      FETCH_MODEL_MAPPINGS_FAILURE,
    ],
  },
});

export const fetchModelMappingsCU = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/model-type-mapping`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_MODEL_MAPPINGS_REQUEST,
      FETCH_MODEL_MAPPINGS_SUCCESS,
      FETCH_MODEL_MAPPINGS_FAILURE,
    ],
  },
});

export const saveModelMapping = (
  typeId: any,
  type: any,  
  mappings: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/model-type-mapping/${typeId}`,
    method: 'patch',
    authenticated: true,
    body: { model_ids : mappings },
    types: [
      UPDATE_MODELS_REQUEST,
      UPDATE_MODELS_SUCCESS,
      UPDATE_MODELS_FAILURE,
    ],
  },
});

export const addModelMapping = (type: string, mappings: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/model-type-mapping`,
    method: 'post',
    authenticated: true,
    body: { type, model_ids : mappings },
    types: [
      UPDATE_MODELS_REQUEST,
      UPDATE_MODELS_SUCCESS,
      UPDATE_MODELS_FAILURE,
    ],
  },
});

export const deleteModelMapping = (typeId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/model-type-mapping/${typeId}`,
    method: 'delete',
    authenticated: true,
    types: [
      UPDATE_MODELS_REQUEST,
      UPDATE_MODELS_SUCCESS,
      UPDATE_MODELS_FAILURE,
    ],
  },
});

export const fetchSubscriptionServiceProviderSettings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-service-provider-mapping`,
    method: 'get',
    authenticated: true,
    types: [
      SERVICE_PROVIDER_SETTINGS_REQUEST,
      SERVICE_PROVIDER_SETTINGS_SUCCESS,
      SERVICE_PROVIDER_SETTINGS_FAILURE
    ],
  },
});

export const saveSubscriptionServiceProviderSettings = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-service-provider-mapping`,
    method: 'post',
    body: data,
    authenticated: true,
    types: [
      SERVICE_PROVIDER_SETTINGS_REQUEST,
      SERVICE_PROVIDER_SETTINGS_SUCCESS,
      SERVICE_PROVIDER_SETTINGS_FAILURE
    ],
  },
});

export const updateSubscriptionServiceProviderSettings = (mappingId: any, data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-service-provider-mapping/${mappingId}`,
    method: 'put',
    body: data,
    authenticated: true,
    types: [
      SERVICE_PROVIDER_SETTINGS_REQUEST,
      SERVICE_PROVIDER_SETTINGS_SUCCESS,
      SERVICE_PROVIDER_SETTINGS_FAILURE
    ],
  },
});

export const deleteSubscriptionServiceProviderSettings = (mappingId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-service-provider-mapping/${mappingId}`,
    method: 'delete',
    authenticated: true,
    types: [
      SERVICE_PROVIDER_SETTINGS_REQUEST,
      SERVICE_PROVIDER_SETTINGS_SUCCESS,
      SERVICE_PROVIDER_SETTINGS_FAILURE
    ],
  },
});
