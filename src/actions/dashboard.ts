import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_SUPERUSER_DASHBOARD_REQUEST =
  'FETCH_SUPERUSER_DASHBOARD_REQUEST';
export const FETCH_SUPERUSER_DASHBOARD_SUCCESS =
  'FETCH_SUPERUSER_DASHBOARD_SUCCESS';
export const FETCH_SUPERUSER_DASHBOARD_FAILURE =
  'FETCH_SUPERUSER_DASHBOARD_FAILURE';

export const FETCH_SERVICE_DASHBOARD_DATA_REQUEST =
  'FETCH_SERVICE_DASHBOARD_DATA_REQUEST';
export const FETCH_SERVICE_DASHBOARD_DATA_SUCCESS =
  'FETCH_SERVICE_DASHBOARD_DATA_SUCCESS';
export const FETCH_SERVICE_DASHBOARD_DATA_FAILURE =
  'FETCH_SERVICE_DASHBOARD_DATA_FAILURE';

export const FETCH_LIFECYCLE_DASHBOARD_DATA_REQUEST =
  'FETCH_LIFECYCLE_DASHBOARD_DATA_REQUEST';
export const FETCH_LIFECYCLE_DASHBOARD_DATA_SUCCESS =
  'FETCH_LIFECYCLE_DASHBOARD_DATA_SUCCESS';
export const FETCH_LIFECYCLE_DASHBOARD_DATA_FAILURE =
  'FETCH_LIFECYCLE_DASHBOARD_DATA_FAILURE';

export const FETCH_ORDER_DASHBOARD_DATA_REQUEST =
  'FETCH_ORDER_DASHBOARD_DATA_REQUEST';
export const FETCH_ORDER_DASHBOARD_DATA_SUCCESS =
  'FETCH_ORDER_DASHBOARD_DATA_SUCCESS';
export const FETCH_ORDER_DASHBOARD_DATA_FAILURE =
  'FETCH_ORDER_DASHBOARD_DATA_FAILURE';

export const FETCH_QUOTE_LIST_REQUEST = 'FETCH_QUOTE_LIST_REQUEST';
export const FETCH_QUOTE_LIST_SUCCESS = 'FETCH_QUOTE_LIST_SUCCESS';
export const FETCH_QUOTE_LIST_FAILURE = 'FETCH_QUOTE_LIST_FAILURE';

export const GET_DOCUMENT_REQUEST = 'GET_DOCUMENT_REQUEST';
export const GET_DOCUMENT_SUCCESS = 'GET_DOCUMENT_SUCCESS';
export const GET_DOCUMENT_FAILURE = 'GET_DOCUMENT_FAILURE';

export const DOWNLOAD_DOCUMENT_REQUEST = 'DOWNLOAD_DOCUMENT_REQUEST';
export const DOWNLOAD_DOCUMENT_SUCCESS = 'DOWNLOAD_DOCUMENT_SUCCESS';
export const DOWNLOAD_DOCUMENT_FAILURE = 'DOWNLOAD_DOCUMENT_FAILURE';

export const FETCH_ACTIVITY_REQUEST = 'FETCH_ACTIVITY_REQUEST';
export const FETCH_ACTIVITY_SUCCESS = 'FETCH_ACTIVITY_SUCCESS';
export const FETCH_ACTIVITY_FAILURE = 'FETCH_ACTIVITY_FAILURE';

export const fetchSuperUserDashboardData = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/superusers/dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SUPERUSER_DASHBOARD_REQUEST,
      FETCH_SUPERUSER_DASHBOARD_SUCCESS,
      FETCH_SUPERUSER_DASHBOARD_FAILURE,
    ],
  },
});

export const fetchServiceDashboardData = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/service-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SERVICE_DASHBOARD_DATA_REQUEST,
      FETCH_SERVICE_DASHBOARD_DATA_SUCCESS,
      FETCH_SERVICE_DASHBOARD_DATA_FAILURE,
    ],
  },
});

export const fetchServiceDashboardDataCustomer = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/customers/service-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SERVICE_DASHBOARD_DATA_REQUEST,
      FETCH_SERVICE_DASHBOARD_DATA_SUCCESS,
      FETCH_SERVICE_DASHBOARD_DATA_FAILURE,
    ],
  },
});

export const fetchLifeCycleDashboardData = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/lifecycle-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_LIFECYCLE_DASHBOARD_DATA_REQUEST,
      FETCH_LIFECYCLE_DASHBOARD_DATA_SUCCESS,
      FETCH_LIFECYCLE_DASHBOARD_DATA_FAILURE,
    ],
  },
});

export const fetchLifeCycleDashboardDataCustomer = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/customers/lifecycle-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_LIFECYCLE_DASHBOARD_DATA_REQUEST,
      FETCH_LIFECYCLE_DASHBOARD_DATA_SUCCESS,
      FETCH_LIFECYCLE_DASHBOARD_DATA_FAILURE,
    ],
  },
});

export const fetchOrdersDashboardData = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/orders-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_ORDER_DASHBOARD_DATA_REQUEST,
      FETCH_ORDER_DASHBOARD_DATA_SUCCESS,
      FETCH_ORDER_DASHBOARD_DATA_FAILURE,
    ],
  },
});

export const fetchOrdersDashboardDataCustomer = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/customers/orders-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_ORDER_DASHBOARD_DATA_REQUEST,
      FETCH_ORDER_DASHBOARD_DATA_SUCCESS,
      FETCH_ORDER_DASHBOARD_DATA_FAILURE,
    ],
  },
});

export const fetchQuoteDashboardListing = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/customers/quotes-dashboard',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_QUOTE_LIST_REQUEST,
      FETCH_QUOTE_LIST_SUCCESS,
      FETCH_QUOTE_LIST_FAILURE,
    ],
  },
});
export const fetchCustomerActivityDashboard = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/customer-user-metrics',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_ACTIVITY_REQUEST,
      FETCH_ACTIVITY_SUCCESS,
      FETCH_ACTIVITY_FAILURE,
    ],
  },
});
export const fetchQuoteDashboardListingPU = (id: number, openOnly?: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/quotes`,
    method: 'get',
    params: { 'open-only': openOnly },
    authenticated: true,
    types: [
      FETCH_QUOTE_LIST_REQUEST,
      FETCH_QUOTE_LIST_SUCCESS,
      FETCH_QUOTE_LIST_FAILURE,
    ],
  },
});

export const getAttachment = id => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/quotes/${id}/documents`,
    method: 'get',
    authenticated: true,
    types: [GET_DOCUMENT_REQUEST, GET_DOCUMENT_SUCCESS, GET_DOCUMENT_FAILURE],
  },
});

export const downloadAttachment = documentId => ({
  [CALL_API]: {
    endpoint: `/api/v1/documents/${documentId}/download`,
    method: 'get',
    authenticated: true,
    types: [
      DOWNLOAD_DOCUMENT_REQUEST,
      DOWNLOAD_DOCUMENT_SUCCESS,
      DOWNLOAD_DOCUMENT_FAILURE,
    ],
  },
});
