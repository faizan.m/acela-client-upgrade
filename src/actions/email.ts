import { CALL_API } from "../middleware/ApiMiddleware";

export const ACTIVATE_USER_EMAIL_REQUEST = "ACTIVATE_USER_EMAIL_REQUEST";
export const ACTIVATE_USER_EMAIL_SUCCESS = "ACTIVATE_USER_EMAIL_SUCCESS";
export const ACTIVATE_USER_EMAIL_FAILURE = "ACTIVATE_USER_EMAIL_FAILURE";

export const FETCH_EMAILS_REQUEST = "FETCH_EMAILS_REQUEST";
export const FETCH_EMAILS_SUCCESS = "FETCH_EMAILS_SUCCESS";
export const FETCH_EMAILS_FAILURE = "FETCH_EMAILS_FAILURE";

export const FETCH_OUTLOOK_EMAIL_REQUEST = "FETCH_OUTLOOK_EMAIL_REQUEST";
export const FETCH_OUTLOOK_EMAIL_SUCCESS = "FETCH_OUTLOOK_EMAIL_SUCCESS";
export const FETCH_OUTLOOK_EMAIL_FAILURE = "FETCH_OUTLOOK_EMAIL_FAILURE";

export const sendActivationEmail = (id: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/users/${id}/send-activation-email`,
    method: "put",
    authenticated: true,
    types: [
      ACTIVATE_USER_EMAIL_REQUEST,
      ACTIVATE_USER_EMAIL_SUCCESS,
      ACTIVATE_USER_EMAIL_FAILURE,
    ],
  },
});

export const fetchEmails = (params?: IScrollPaginationFilters) => ({
  [CALL_API]: {
    endpoint: `/api/v1/email-logs`,
    method: "get",
    params: {
      page: params.nextPage,
      page_size: params.page_size,
      ordering: params.ordering,
      search: params.search,
    },
    authenticated: true,
    types: [FETCH_EMAILS_REQUEST, FETCH_EMAILS_SUCCESS, FETCH_EMAILS_FAILURE],
  },
});

export const resendEmail = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/email-logs/${id}/resend`,
    method: "post",
    authenticated: true,
    types: [FETCH_EMAILS_REQUEST, FETCH_EMAILS_SUCCESS, FETCH_EMAILS_FAILURE],
  },
});

export const fetchOutlookEmails = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/aad-users`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_OUTLOOK_EMAIL_REQUEST,
      FETCH_OUTLOOK_EMAIL_SUCCESS,
      FETCH_OUTLOOK_EMAIL_FAILURE,
    ],
  },
});
