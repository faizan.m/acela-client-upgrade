import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_USERTYPES_REQUEST = 'FETCH_USERTYPES_REQUEST';
export const FETCH_USERTYPES_SUCCESS = 'FETCH_USERTYPES_SUCCESS';
export const FETCH_USERTYPES_FAILURE = 'FETCH_USERTYPES_FAILURE';

export const fetchUserTypes = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/user-types-roles',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_USERTYPES_REQUEST,
      FETCH_USERTYPES_SUCCESS,
      FETCH_USERTYPES_FAILURE,
    ],
  },
});
