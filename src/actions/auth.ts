import { CALL_API } from "../middleware/ApiMiddleware";
import { LOGIN_API, LOGOUT_API } from "../middleware/AuthMiddleware";
import { getLocalRefreshToken } from "../utils/AuthUtil";

export const LOGIN_REQUEST = "LOGIN_REQUEST";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_FAILURE = "LOGIN_FAILURE";
export const LOGOUT_REQUEST = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";

export const FORGOT_PASSWORD_REQUEST = "FORGOT_PASSWORD_REQUEST";
export const FORGOT_PASSWORD_SUCCESS = "FORGOT_PASSWORD_SUCCESS";
export const FORGOT_PASSWORD_FAILURE = "FORGOT_PASSWORD_FAILURE";

export const TWOFA_REQUEST = "TWOFA_REQUEST";
export const TWOFA_SUCCESS = "TWOFA_SUCCESS";
export const TWOFA_FAILURE = "TWOFA_FAILURE";

export const TWOFA_REQUEST_REQUEST = "TWOFA_REQUEST_REQUEST";
export const TWOFA_REQUEST_SUCCESS = "TWOFA_REQUEST_SUCCESS";
export const TWOFA_REQUEST_FAILURE = "TWOFA_REQUEST_FAILURE";

export const REQUEST_PHONE_VALID_REQUEST = "REQUEST_PHONE_VALID_REQUEST";
export const REQUEST_PHONE_VALID_SUCCESS = "REQUEST_PHONE_VALID_SUCCESS";
export const REQUEST_PHONE_VALID_FAILURE = "REQUEST_PHONE_VALID_FAILURE";

export const PHONE_VALIDATE_REQUEST = "PHONE_VALIDATE_REQUEST";
export const PHONE_VALIDATE_SUCCESS = "PHONE_VALIDATE_SUCCESS";
export const PHONE_VALIDATE_FAILURE = "PHONE_VALIDATE_FAILURE";

export const SET_PASSWORD_REQUEST = "SET_PASSWORD_REQUEST";
export const SET_PASSWORD_SUCCESS = "SET_PASSWORD_SUCCESS";
export const SET_PASSWORD_FAILURE = "SET_PASSWORD_FAILURE";

export const RESET_PASSWORD_REQUEST = "RESET_PASSWORD_REQUEST";
export const RESET_PASSWORD_FAILURE = "RESET_PASSWORD_FAILURE";
export const RESET_PASSWORD_SUCCESS = "RESET_PASSWORD_SUCCESS";

export const VALIDATE_LINK_REQUEST = "VALIDATE_LINK_REQUEST";
export const VALIDATE_LINK_SUCCESS = "VALIDATE_LINK_SUCCESS";
export const VALIDATE_LINK_FAILURE = "VALIDATE_LINK_FAILURE";

export const REFRESH_TOKEN_REQUEST = "REFRESH_TOKEN_REQUEST";
export const REFRESH_TOKEN_SUCCESS = "REFRESH_TOKEN_SUCCESS";
export const REFRESH_TOKEN_FAILURE = "REFRESH_TOKEN_FAILURE";

export const loginRequest = (email: string, password: string) => ({
  [LOGIN_API]: {
    email,
    password,
    types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
  },
});

export const loginSuccess = (userProfile: IUserProfile) => ({
  type: LOGIN_SUCCESS,
  userProfile,
});

export const logOutRequest = () => ({
  [LOGOUT_API]: {
    success: LOGOUT_SUCCESS,
  },
});

export const logOut = () => ({
  type: LOGOUT_SUCCESS,
});

export const loginStaffRequest = (
  agencyId: number,
  username: string,
  password: string
) => ({
  [LOGIN_API]: {
    username,
    password,
    agencyId,
    types: [LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE],
  },
});

export const forgotPasswordRequest = (
  forgotRequest: IForgotPasswordRequest
) => ({
  [CALL_API]: {
    endpoint: "/api/v1/forgot-password",
    method: "post",
    authenticated: false,
    body: forgotRequest,
    types: [
      FORGOT_PASSWORD_REQUEST,
      FORGOT_PASSWORD_SUCCESS,
      FORGOT_PASSWORD_FAILURE,
    ],
  },
});

export const twoFARequestAuth = (data: any) => ({
  [CALL_API]: {
    endpoint: "/api/v1/request-authentication",
    method: "post",
    tempAuthenticated: true,
    body: data,
    types: [
      TWOFA_REQUEST_REQUEST,
      TWOFA_REQUEST_SUCCESS,
      TWOFA_REQUEST_FAILURE,
    ],
  },
});

export const twoFAValidateAuth = (data: any) => ({
  [CALL_API]: {
    endpoint: "/api/v1/validate-authentication",
    method: "post",
    tempAuthenticated: true,
    body: data,
    types: [TWOFA_REQUEST, TWOFA_SUCCESS, TWOFA_FAILURE],
  },
});

export const requestPhoneVerification = (data: {
  country_code: string;
  phone: string;
}) => ({
  [CALL_API]: {
    endpoint: "/api/v1/request-phone-verification",
    method: "post",
    tempAuthenticated: true,
    body: data,
    types: [
      REQUEST_PHONE_VALID_REQUEST,
      REQUEST_PHONE_VALID_SUCCESS,
      REQUEST_PHONE_VALID_FAILURE,
    ],
  },
});

export const validatePhone = (data: {
  code: string;
  phone: string;
  country_code: string;
}) => ({
  [CALL_API]: {
    endpoint: "/api/v1/validate-phone-validation",
    method: "post",
    tempAuthenticated: true,
    body: data,
    types: [
      PHONE_VALIDATE_REQUEST,
      PHONE_VALIDATE_SUCCESS,
      PHONE_VALIDATE_FAILURE,
    ],
  },
});

export const userTwoFAStatus = (userId: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/user/${userId}/two-fa-status`,
    method: "get",
    authenticated: true,
    types: [
      PHONE_VALIDATE_REQUEST,
      PHONE_VALIDATE_SUCCESS,
      PHONE_VALIDATE_FAILURE,
    ],
  },
});

export const verifyAuthyToken = (otp: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/verify-otp`,
    method: "post",
    authenticated: true,
    body: { otp },
    types: [
      TWOFA_REQUEST_REQUEST,
      TWOFA_REQUEST_SUCCESS,
      TWOFA_REQUEST_FAILURE,
    ],
  },
});

export const setPasswordRequest = (setRequest: ISetPasswordRequest) => ({
  [CALL_API]: {
    endpoint: "/api/v1/set-password",
    method: "post",
    authenticated: false,
    body: setRequest,
    types: [SET_PASSWORD_REQUEST, SET_PASSWORD_SUCCESS, SET_PASSWORD_FAILURE],
  },
});

export const resetPasswordRequest = (resetRequest: IResetPasswordRequest) => ({
  [CALL_API]: {
    endpoint: "/api/v1/change-password",
    method: "post",
    authenticated: true,
    body: resetRequest,
    types: [
      RESET_PASSWORD_REQUEST,
      RESET_PASSWORD_SUCCESS,
      RESET_PASSWORD_FAILURE,
    ],
  },
});

export const getRefreshToken = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/token-refresh",
    method: "post",
    authenticated: false,
    body: { token: getLocalRefreshToken() },
    types: [
      REFRESH_TOKEN_REQUEST,
      REFRESH_TOKEN_SUCCESS,
      REFRESH_TOKEN_FAILURE,
    ],
  },
});
export const validatePasswordRequest = (data: any) => ({
  [CALL_API]: {
    endpoint: "/api/v1/validate-uid-token",
    method: "post",
    authenticated: false,
    body: data,
    types: [
      VALIDATE_LINK_REQUEST,
      VALIDATE_LINK_SUCCESS,
      VALIDATE_LINK_FAILURE,
    ],
  },
});
