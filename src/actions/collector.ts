import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_CUS_COLLTS_REQUEST = 'FETCH_CUS_COLLTS_REQUEST';
export const FETCH_CUS_COLLTS_SUCCESS = 'FETCH_CUS_COLLTS_SUCCESS';
export const FETCH_CUS_COLLTS_FAILURE = 'FETCH_CUS_COLLTS_FAILURE';

export const FETCH_COLLECTOR_REQUEST = 'FETCH_COLLECTOR_REQUEST';
export const FETCH_COLLECTOR_SUCCESS = 'FETCH_COLLECTOR_SUCCESS';
export const FETCH_COLLECTOR_FAILURE = 'FETCH_COLLECTOR_FAILURE';

export const FETCH_COLLECTORS_REQUEST = 'FETCH_COLLECTORS_REQUEST';
export const FETCH_COLLECTORS_SUCCESS = 'FETCH_COLLECTORS_SUCCESS';
export const FETCH_COLLECTORS_FAILURE = 'FETCH_COLLECTORS_FAILURE';

export const UPDATE_CUS_COLLTS_REQUEST = 'UPDATE_CUS_COLLTS_REQUEST';
export const UPDATE_CUS_COLLTS_SUCCESS = 'UPDATE_CUS_COLLTS_SUCCESS';
export const UPDATE_CUS_COLLTS_FAILURE = 'UPDATE_CUS_COLLTS_FAILURE';

export const DELETE_COLLECTOR_REQUEST = 'DELETE_COLLECTOR_REQUEST';
export const DELETE_COLLECTOR_SUCCESS = 'DELETE_COLLECTOR_SUCCESS';
export const DELETE_COLLECTOR_FAILURE = 'DELETE_COLLECTOR_FAILURE';

export const GET_CLT_TOKEN_REQUEST = 'GET_CLT_TOKEN_REQUEST';
export const GET_CLT_TOKEN_SUCCESS = 'GET_CLT_TOKEN_SUCCESS';
export const GET_CLT_TOKEN_FAILURE = 'GET_CLT_TOKEN_FAILURE';

export const FETCH_C_SERVICE_TYPE_REQUEST = 'FETCH_C_SERVICE_TYPE_REQUEST';
export const FETCH_C_SERVICE_TYPE_SUCCESS = 'FETCH_C_SERVICE_TYPE_SUCCESS';
export const FETCH_C_SERVICE_TYPE_FAILURE = 'FETCH_C_SERVICE_TYPE_FAILURE';

export const UPDATE_CL_MAPPING_REQUEST = 'UPDATE_CL_MAPPING_REQUEST';
export const UPDATE_CL_MAPPING_SUCCESS = 'UPDATE_CL_MAPPING_SUCCESS';
export const UPDATE_CL_MAPPING_FAILURE = 'UPDATE_CL_MAPPING_FAILURE';

export const FETCH_D_CL_MAPPING_REQUEST = 'FETCH_D_CL_MAPPING_REQUEST';
export const FETCH_D_CL_MAPPING_SUCCESS = 'FETCH_D_CL_MAPPING_SUCCESS';
export const FETCH_D_CL_MAPPING_FAILURE = 'FETCH_D_CL_MAPPING_FAILURE';

export const SAVE_COLL_MAPPING_REQUEST = 'SAVE_COLL_MAPPING_REQUEST';
export const SAVE_COLL_MAPPING_SUCCESS = 'SAVE_COLL_MAPPING_SUCCESS';
export const SAVE_COLL_MAPPING_FAILURE = 'SAVE_COLL_MAPPING_FAILURE';


export const fetchCollectorToken = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/collector-tokens`,
    method: 'get',
    authenticated: true,
    types: [
      GET_CLT_TOKEN_REQUEST,
      GET_CLT_TOKEN_SUCCESS,
      GET_CLT_TOKEN_FAILURE,
    ],
  },
});

export const fetchCustomerCollectors = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/collectors`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_CUS_COLLTS_REQUEST,
      FETCH_CUS_COLLTS_SUCCESS,
      FETCH_CUS_COLLTS_FAILURE,
    ],
  },
});

export const fetchSingleCollectorCU = (collectorId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/collectors/${collectorId}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_COLLECTOR_REQUEST,
      FETCH_COLLECTOR_SUCCESS,
      FETCH_COLLECTOR_FAILURE,
    ],
  },
});


export const fetchSingleCollectorPU = (id: any, collectorId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/collectors/${collectorId}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_COLLECTOR_REQUEST,
      FETCH_COLLECTOR_SUCCESS,
      FETCH_COLLECTOR_FAILURE,
    ],
  },
});
export const saveCustomerCollector = (collector: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/collectors`,
    method: 'post',
    body: { collector: collector.collector, name: collector.name, token: collector.token },
    authenticated: true,
    types: [
      UPDATE_CUS_COLLTS_REQUEST,
      UPDATE_CUS_COLLTS_SUCCESS,
      UPDATE_CUS_COLLTS_FAILURE,
    ],
  },
});


export const deleteCollector = (collectorId: number, customerId?: number) => ({
  [CALL_API]: {
    endpoint: `${customerId ? 
      `/api/v1/providers/customers/${customerId}/collectors/${collectorId}` :
      `/api/v1/customers/collectors/${collectorId}`}`,
    method: 'delete',
    authenticated: true,
    types: [
      DELETE_COLLECTOR_REQUEST,
      DELETE_COLLECTOR_SUCCESS,
      DELETE_COLLECTOR_FAILURE,
    ],
  },
});

export const fetchCustomerCollectorsPU = (
  id: any,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/collectors`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_CUS_COLLTS_REQUEST,
      FETCH_CUS_COLLTS_SUCCESS,
      FETCH_CUS_COLLTS_FAILURE,
    ],
  },
});
export const saveCollectorsMapping = (
  custId: any,
  collectortId: any,
  intId: any,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${custId}/collectors/${collectortId}/integrations/${intId}`,
    method: 'patch',
    authenticated: true,
    body: data,
    types: [
      SAVE_COLL_MAPPING_REQUEST,
      SAVE_COLL_MAPPING_SUCCESS,
      SAVE_COLL_MAPPING_FAILURE,
    ],
  },
});
export const saveCustomerCollectorPU = (id: any, collector: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/collectors`,
    method: 'post',
    body: { name: collector.name, token: collector.token },
    authenticated: true,
    types: [
      UPDATE_CUS_COLLTS_REQUEST,
      UPDATE_CUS_COLLTS_SUCCESS,
      UPDATE_CUS_COLLTS_FAILURE,
    ],
  },
});

export const disableCollectors = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/collectors`,
    method: 'delete',
    authenticated: true,
    types: [
      UPDATE_CUS_COLLTS_REQUEST,
      UPDATE_CUS_COLLTS_SUCCESS,
      UPDATE_CUS_COLLTS_FAILURE,
    ],
  },
});

export const fetchCollectorServiceTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/collector-service-types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_C_SERVICE_TYPE_REQUEST,
      FETCH_C_SERVICE_TYPE_SUCCESS,
      FETCH_C_SERVICE_TYPE_FAILURE,
    ],
  },
});
export const updateCollectorMapping = (data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/collector-settings`,
    method: 'put',
    body: { settings: data },
    authenticated: true,
    types: [
      UPDATE_CL_MAPPING_REQUEST,
      UPDATE_CL_MAPPING_SUCCESS,
      UPDATE_CL_MAPPING_FAILURE,
    ],
  },
});
export const fetchCollectorMapping = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/collector-settings`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_D_CL_MAPPING_REQUEST,
      FETCH_D_CL_MAPPING_SUCCESS,
      FETCH_D_CL_MAPPING_FAILURE,
    ],
  },
});



export const FETCH_QUERIES_REQUEST = 'FETCH_QUERIES_REQUEST';
export const FETCH_QUERIES_SUCCESS = 'FETCH_QUERIES_SUCCESS';
export const FETCH_QUERIES_FAILURE = 'FETCH_QUERIES_FAILURE';


export const FETCH_QUERY_REQUEST = 'FETCH_QUERY_REQUEST';
export const FETCH_QUERY_SUCCESS = 'FETCH_QUERY_SUCCESS';
export const FETCH_QUERY_FAILURE = 'FETCH_QUERY_FAILURE';

export const GET_URL_REQUEST = 'GET_URL_REQUEST';
export const GET_URL_SUCCESS = 'GET_URL_SUCCESS';
export const GET_URL_FAILURE = 'GET_URL_FAILURE';

export const RERUN_COMPLIANCE_REQUEST = 'RERUN_COMPLIANCE_REQUEST';
export const RERUN_COMPLIANCE_SUCCESS = 'RERUN_COMPLIANCE_SUCCESS';
export const RERUN_COMPLIANCE_FAILURE = 'RERUN_COMPLIANCE_FAILURE';

export const REPORT_COMPLIANCE_REQUEST = 'REPORT_COMPLIANCE_REQUEST';
export const REPORT_COMPLIANCE_SUCCESS = 'REPORT_COMPLIANCE_SUCCESS';
export const REPORT_COMPLIANCE_FAILURE = 'REPORT_COMPLIANCE_FAILURE';

export const fetchQueryList = (customerId: any, collectorId: any, params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/collectors/${collectorId}/queries`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_QUERIES_REQUEST,
      FETCH_QUERIES_SUCCESS,
      FETCH_QUERIES_FAILURE,
    ],
  },
});

export const fetchQuery = (customerId: any, collectorId: any, queryId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/collectors/${collectorId}/queries/${queryId}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_QUERY_REQUEST,
      FETCH_QUERY_SUCCESS,
      FETCH_QUERY_FAILURE,
    ],
  },
});

export const saveQuery = (customerId: any, collectorId: any, data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/collectors/${collectorId}/queries`,
    method: 'post',
    body: data,
    authenticated: true,
    types: [
      FETCH_QUERY_REQUEST,
      FETCH_QUERY_SUCCESS,
      FETCH_QUERY_FAILURE,
    ],
  },
});
export const deleteQuery = (customerId: any, collectorId: any, queryId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/collectors/${collectorId}/queries/${queryId}`,
    method: 'delete',
    authenticated: true,
    types: [
      FETCH_QUERY_REQUEST,
      FETCH_QUERY_SUCCESS,
      FETCH_QUERY_FAILURE,
    ],
  },
});

export const getDownloadURL = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/collector-download-url`,
    method: 'get',
    authenticated: true,
    types: [
      GET_URL_REQUEST,
      GET_URL_SUCCESS,
      GET_URL_FAILURE,
    ],
  },
});


export const rerunConfigCompliance = (data?: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rerun-compliance`,
    method: 'post',
    body: data,
    authenticated: true,
    types: [
      RERUN_COMPLIANCE_REQUEST,
      RERUN_COMPLIANCE_SUCCESS,
      RERUN_COMPLIANCE_FAILURE,
    ],
  },
});

export const reportConfigCompliance = (customerId: any) => ({
  [CALL_API]: {
    endpoint: customerId? `/api/v1/providers/customers/${customerId}/compliance-report` : `/api/v1/customers/compliance-report`,
    method: 'get',
    authenticated: true,
    types: [
      REPORT_COMPLIANCE_REQUEST,
      REPORT_COMPLIANCE_SUCCESS,
      REPORT_COMPLIANCE_FAILURE,
    ],
  },
});
