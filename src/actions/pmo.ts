import { CALL_API } from "../middleware/ApiMiddleware";

export const SAVE_PROJECT_DETAILS_FILTER = "SAVE_PROJECT_DETAILS_FILTER";

export const SAVE_PMO_DASHBOARD_FILTERS = "SAVE_PMO_DASHBOARD_FILTERS";

export const LAST_VISITED_PMO_SITE = "LAST_VISITED_PMO_SITE";

export const NEW_TIME_S_REQUEST = "NEW_TIME_S_REQUEST";
export const NEW_TIME_S_SUCCESS = "NEW_TIME_S_SUCCESS";
export const NEW_TIME_S_FAILURE = "NEW_TIME_S_FAILURE";

export const FETCH_ACTIVITY_S_LIST_REQUEST = "FETCH_ACTIVITY_S_LIST_REQUEST";
export const FETCH_ACTIVITY_S_LIST_SUCCESS = "FETCH_ACTIVITY_S_LIST_SUCCESS";
export const FETCH_ACTIVITY_S_LIST_FAILURE = "FETCH_ACTIVITY_S_LIST_FAILURE";

export const FETCH_DATE_S_LIST_REQUEST = "FETCH_DATE_S_LIST_REQUEST";
export const FETCH_DATE_S_LIST_SUCCESS = "FETCH_DATE_S_LIST_SUCCESS";
export const FETCH_DATE_S_LIST_FAILURE = "FETCH_DATE_S_LIST_FAILURE";

export const FETCH_TIME_S_LIST_REQUEST = "FETCH_TIME_S_LIST_REQUEST";
export const FETCH_TIME_S_LIST_SUCCESS = "FETCH_TIME_S_LIST_SUCCESS";
export const FETCH_TIME_S_LIST_FAILURE = "FETCH_TIME_S_LIST_FAILURE";

export const NEW_DATE_S_REQUEST = "NEW_DATE_S_REQUEST";
export const NEW_DATE_S_SUCCESS = "NEW_DATE_S_SUCCESS";
export const NEW_DATE_S_FAILURE = "NEW_DATE_S_FAILURE";

export const NEW_ACTIVITY_STATUS_REQUEST = "NEW_ACTIVITY_STATUS_REQUEST";
export const NEW_ACTIVITY_STATUS_SUCCESS = "NEW_ACTIVITY_STATUS_SUCCESS";
export const NEW_ACTIVITY_STATUS_FAILURE = "NEW_ACTIVITY_STATUS_FAILURE";

export const GET_DATA_REQUEST = "GET_DATA_REQUEST";
export const GET_DATA_SUCCESS = "GET_DATA_SUCCESS";
export const GET_DATA_FAILURE = "GET_DATA_FAILURE";

export const NEW_PROJECT_TYPE_REQUEST = "NEW_PROJECT_TYPE_REQUEST";
export const NEW_PROJECT_TYPE_SUCCESS = "NEW_PROJECT_TYPE_SUCCESS";
export const NEW_PROJECT_TYPE_FAILURE = "NEW_PROJECT_TYPE_FAILURE";

export const NEW_PROJECT_PHASE_REQUEST = "NEW_PROJECT_PHASE_REQUEST";
export const NEW_PROJECT_PHASE_SUCCESS = "NEW_PROJECT_PHASE_SUCCESS";
export const NEW_PROJECT_PHASE_FAILURE = "NEW_PROJECT_PHASE_FAILURE";

export const FETCH_PROJECTS_REQUEST = "FETCH_PROJECTS_REQUEST";
export const FETCH_PROJECTS_SUCCESS = "FETCH_PROJECTS_SUCCESS";
export const FETCH_PROJECTS_FAILURE = "FETCH_PROJECTS_FAILURE";

export const FETCH_PMO_REVIEW_REQUEST = "FETCH_PMO_REVIEW_REQUEST";
export const FETCH_PMO_REVIEW_SUCCESS = "FETCH_PMO_REVIEW_SUCCESS";
export const FETCH_PMO_REVIEW_FAILURE = "FETCH_PMO_REVIEW_FAILURE";

export const FETCH_PROJECT_TYPE_LIST_REQUEST =
  "FETCH_PROJECT_TYPE_LIST_REQUEST";
export const FETCH_PROJECT_TYPE_LIST_SUCCESS =
  "FETCH_PROJECT_TYPE_LIST_SUCCESS";
export const FETCH_PROJECT_TYPE_LIST_FAILURE =
  "FETCH_PROJECT_TYPE_LIST_FAILURE";

export const FETCH_PROJECT_BOARD_LIST_REQUEST =
  "FETCH_PROJECT_BOARD_LIST_REQUEST";
export const FETCH_PROJECT_BOARD_LIST_SUCCESS =
  "FETCH_PROJECT_BOARD_LIST_SUCCESS";
export const FETCH_PROJECT_BOARD_LIST_FAILURE =
  "FETCH_PROJECT_BOARD_LIST_FAILURE";

export const FETCH_PROJECT_TEAM_REQUEST = "FETCH_PROJECT_TEAM_REQUEST";
export const FETCH_PROJECT_TEAM_SUCCESS = "FETCH_PROJECT_TEAM_SUCCESS";
export const FETCH_PROJECT_TEAM_FAILURE = "FETCH_PROJECT_TEAM_FAILURE";

export const PROJECT_SETUP_REQUEST = "PROJECT_SETUP_REQUEST";
export const PROJECT_SETUP_SUCCESS = "PROJECT_SETUP_SUCCESS";
export const PROJECT_SETUP_FAILURE = "PROJECT_SETUP_FAILURE";

export const DEFAULT_TIME_ENTRY_TICKET_REQUEST =
  "DEFAULT_TIME_ENTRY_TICKET_REQUEST";
export const DEFAULT_TIME_ENTRY_TICKET_SUCCESS =
  "DEFAULT_TIME_ENTRY_TICKET_SUCCESS";
export const DEFAULT_TIME_ENTRY_TICKET_FAILURE =
  "DEFAULT_TIME_ENTRY_TICKET_FAILURE";

export const PROJECT_WORK_PHASE_REQUEST = "PROJECT_WORK_PHASE_REQUEST";
export const PROJECT_WORK_PHASE_SUCCESS = "PROJECT_WORK_PHASE_SUCCESS";
export const PROJECT_WORK_PHASE_FAILURE = "PROJECT_WORK_PHASE_FAILURE";

export const PROJECT_RESOURCE_SUMMARY_REQUEST =
  "PROJECT_RESOURCE_SUMMARY_REQUEST";
export const PROJECT_RESOURCE_SUMMARY_SUCCESS =
  "PROJECT_RESOURCE_SUMMARY_SUCCESS";
export const PROJECT_RESOURCE_SUMMARY_FAILURE =
  "PROJECT_RESOURCE_SUMMARY_FAILURE";

export const NEW_PROJECT_BOARD_REQUEST = "NEW_PROJECT_BOARD_REQUEST";
export const NEW_PROJECT_BOARD_SUCCESS = "NEW_PROJECT_BOARD_SUCCESS";
export const NEW_PROJECT_BOARD_FAILURE = "NEW_PROJECT_BOARD_FAILURE";

export const NEW_KICK_OFF_MEET_REQUEST = "NEW_KICK_OFF_MEET_REQUEST";
export const NEW_KICK_OFF_MEET_SUCCESS = "NEW_KICK_OFF_MEET_SUCCESS";
export const NEW_KICK_OFF_MEET_FAILURE = "NEW_KICK_OFF_MEET_FAILURE";

export const MEETING_TEMPLATE_REQUEST = "MEETING_TEMPLATE_REQUEST";
export const MEETING_TEMPLATE_SUCCESS = "MEETING_TEMPLATE_SUCCESS";
export const MEETING_TEMPLATE_FAILURE = "MEETING_TEMPLATE_FAILURE";

export const ADDITIONAL_SETTING_REQUEST = "ADDITIONAL_SETTING_REQUEST";
export const ADDITIONAL_SETTING_SUCCESS = "ADDITIONAL_SETTING_SUCCESS";
export const ADDITIONAL_SETTING_FAILURE = "ADDITIONAL_SETTING_FAILURE";

export const FETCH__BOARD_LIST_REQUEST_REQUEST =
  "FETCH__BOARD_LIST_REQUEST_REQUEST";
export const FETCH__BOARD_LIST_REQUEST_SUCCESS =
  "FETCH__BOARD_LIST_REQUEST_SUCCESS";
export const FETCH__BOARD_LIST_REQUEST_FAILURE =
  "FETCH__BOARD_LIST_REQUEST_FAILURE";

export const FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST =
  "FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST";
export const FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS =
  "FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS";
export const FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE =
  "FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE";

export const CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST =
  "CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST";
export const CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS =
  "CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS";
export const CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE =
  "CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE";

export const UPDATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST =
  "UPDATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST";
export const UPDATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS =
  "UPDATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS";
export const UPDATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE =
  "UPDATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE";

export const PROJECT_PHASES_REQUEST = "PROJECT_PHASES_REQUEST";
export const PROJECT_PHASES_SUCCESS = "PROJECT_PHASES_SUCCESS";
export const PROJECT_PHASES_FAILURE = "PROJECT_PHASES_FAILURE";

export const PROJECT_DETAILS_REQUEST = "PROJECT_DETAILS_REQUEST";
export const PROJECT_DETAILS_SUCCESS = "PROJECT_DETAILS_SUCCESS";
export const PROJECT_DETAILS_FAILURE = "PROJECT_DETAILS_FAILURE";

export const SET_PROJECT_DETAILS_REQUEST = "SET_PROJECT_DETAILS_REQUEST";
export const SET_PROJECT_DETAILS_SUCCESS = "SET_PROJECT_DETAILS_SUCCESS";
export const SET_PROJECT_DETAILS_FAILURE = "SET_PROJECT_DETAILS_FAILURE";

export const PROJECT_NOTES_REQUEST = "PROJECT_NOTES_REQUEST";
export const PROJECT_NOTES_SUCCESS = "PROJECT_NOTES_SUCCESS";
export const PROJECT_NOTES_FAILURE = "PROJECT_NOTES_FAILURE";

export const PROJECT_MEETING_NOTES_REQUEST = "PROJECT_MEETING_NOTES_REQUEST";
export const PROJECT_MEETING_NOTES_SUCCESS = "PROJECT_MEETING_NOTES_SUCCESS";
export const PROJECT_MEETING_NOTES_FAILURE = "PROJECT_MEETING_NOTES_FAILURE";

export const TEMPLATES_REQUEST = "TEMPLATES_REQUEST";
export const TEMPLATES_SUCCESS = "TEMPLATES_SUCCESS";
export const TEMPLATES_FAILURE = "TEMPLATES_FAILURE";

export const PROJECT_TICKETS_REQUEST = "PROJECT_TICKETS_REQUEST";
export const PROJECT_TICKETS_SUCCESS = "PROJECT_TICKETS_SUCCESS";
export const PROJECT_TICKETS_FAILURE = "PROJECT_TICKETS_FAILURE";

export const ACTION_ITEM_REQUEST = "ACTION_ITEM_REQUEST";
export const ACTION_ITEM_SUCCESS = "ACTION_ITEM_SUCCESS";
export const ACTION_ITEM_FAILURE = "ACTION_ITEM_FAILURE";

export const STATUS_BY_PROJECT_REQUEST = "STATUS_BY_PROJECT_REQUEST";
export const STATUS_BY_PROJECT_SUCCESS = "STATUS_BY_PROJECT_SUCCESS";
export const STATUS_BY_PROJECT_FAILURE = "STATUS_BY_PROJECT_FAILURE";

export const PROJECT_DETAILS_UPDATE_REQUEST = "PROJECT_DETAILS_UPDATE_REQUEST";
export const PROJECT_DETAILS_UPDATE_SUCCESS = "PROJECT_DETAILS_UPDATE_SUCCESS";
export const PROJECT_DETAILS_UPDATE_FAILURE = "PROJECT_DETAILS_UPDATE_FAILURE";

export const PROJECT_TYPES_REQUEST = "PROJECT_TYPES_REQUEST";
export const PROJECT_TYPES_SUCCESS = "PROJECT_TYPES_SUCCESS";
export const PROJECT_TYPES_FAILURE = "PROJECT_TYPES_FAILURE";

export const PROJECT_ENGINEERS_REQUEST = "PROJECT_ENGINEERS_REQUEST";
export const PROJECT_ENGINEERS_SUCCESS = "PROJECT_ENGINEERS_REQUEST";
export const PROJECT_ENGINEERS_FAILURE = "PROJECT_ENGINEERS_REQUEST";

export const PROJECT_DOCUMENTS_REQUEST = "PROJECT_DOCUMENTS_REQUEST";
export const PROJECT_DOCUMENTS_SUCCESS = "PROJECT_DOCUMENTS_SUCCESS";
export const PROJECT_DOCUMENTS_FAILURE = "PROJECT_DOCUMENTS_FAILURE";

export const PROJECT_CUSTOMER_CONTACTS_REQUEST =
  "PROJECT_CUSTOMER_CONTACTS_REQUEST";
export const PROJECT_CUSTOMER_CONTACTS_SUCCESS =
  "PROJECT_CUSTOMER_CONTACTS_SUCCESS";
export const PROJECT_CUSTOMER_CONTACTS_FAILURE =
  "PROJECT_CUSTOMER_CONTACTS_FAILURE";

export const PROJECT_MANAGER_REQUEST = "PROJECT_MANAGER_REQUEST";
export const PROJECT_MANAGER_SUCCESS = "PROJECT_MANAGER_SUCCESS";
export const PROJECT_MANAGER_FAILURE = "PROJECT_MANAGER_FAILURE";

export const MEETING_REQUEST = "MEETING_REQUEST";
export const MEETING_SUCCESS = "MEETING_SUCCESS";
export const MEETING_FAILURE = "MEETING_FAILURE";

export const MEETING_COMPLETE_REQUEST = "MEETING_COMPLETE_REQUEST";
export const MEETING_COMPLETE_SUCCESS = "MEETING_COMPLETE_SUCCESS";
export const MEETING_COMPLETE_FAILURE = "MEETING_COMPLETE_FAILURE";

export const MEETING_ACTION_REQUEST = "MEETING_ACTION_REQUEST";
export const MEETING_ACTION_SUCCESS = "MEETING_ACTION_SUCCESS";
export const MEETING_ACTION_FAILURE = "MEETING_ACTION__FAILURE";

export const PROJECT_MEETING_DOCUMENTS_REQUEST =
  "PROJECT_MEETING_DOCUMENTS_REQUEST";
export const PROJECT_MEETING_DOCUMENTS_SUCCESS =
  "PROJECT_MEETING_DOCUMENTS_SUCCESS";
export const PROJECT_MEETING_DOCUMENTS_FAILURE =
  "PROJECT_MEETING_DOCUMENTS_FAILURE";

export const FETCHING_REQUEST = "FETCHING_REQUEST";
export const FETCHING_SUCCESS = "FETCHING_SUCCESS";
export const FETCHING_FAILURE = "FETCHING_FAILURE";

export const MEETING_ATTENDEES_REQUEST = "MEETING_ATTENDEES_REQUEST";
export const MEETING_ATTENDEES_SUCCESS = "MEETING_ATTENDEES_SUCCESS";
export const MEETING_ATTENDEES_FAILURE = "MEETING_ATTENDEES_FAILURE";

export const CLOSE_OUT_MEETING_TEMPLATE_REQUEST =
  "CLOSE_OUT_MEETING_TEMPLATE_REQUEST";
export const CLOSE_OUT_MEETING_TEMPLATE_SUCCESS =
  "CLOSE_OUT_MEETING_TEMPLATE_SUCCESS";
export const CLOSE_OUT_MEETING_TEMPLATE_FAILURE =
  "CLOSE_OUT_MEETING_TEMPLATE_FAILURE";

export const DOWNLOAD_MEETING_DOCUMENT_REQUEST =
  "DOWNLOAD_MEETING_DOCUMENT_REQUEST";
export const DOWNLOAD_MEETING_DOCUMENT_SUCCESS =
  "DOWNLOAD_MEETING_DOCUMENT_SUCCESS";
export const DOWNLOAD_MEETING_DOCUMENT_FAILURE =
  "DOWNLOAD_MEETING_DOCUMENT_FAILURE";

export const FETCH_MEETING_MAIL_TEMPLATE_SETTING_REQUEST =
  "FETCH_MEETING_MAIL_TEMPLATE_SETTING_REQUEST";
export const FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS =
  "FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS";
export const FETCH_MEETING_MAIL_TEMPLATE_SETTING_FAILURE =
  "FETCH_MEETING_MAIL_TEMPLATE_SETTING_FAILURE";

export const CREATE_MEETING_MAIL_TEMPLATE_SETTING_REQUEST =
  "CREATE_MEETING_MAIL_TEMPLATE_SETTING_REQUEST";
export const CREATE_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS =
  "CREATE_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS";
export const CREATE_MEETING_MAIL_TEMPLATE_SETTING_FAILURE =
  "CREATE_MEETING_MAIL_TEMPLATE_SETTING_FAILURE";

export const UPDATE_MEETING_MAIL_TEMPLATE_SETTING_REQUEST =
  "UPDATE_MEETING_MAIL_TEMPLATE_SETTING_REQUEST";
export const UPDATE_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS =
  "UPDATE_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS";
export const UPDATE_MEETING_MAIL_TEMPLATE_SETTING_FAILURE =
  "UPDATE_MEETING_MAIL_TEMPLATE_SETTING_FAILURE";

export const UPDATE_CHANGE_REQ_REQUEST = "UPDATE_CHANGE_REQ_REQUEST";
export const UPDATE_CHANGE_REQ_SUCCESS = "UPDATE_CHANGE_REQSUCCESS";
export const UPDATE_CHANGE_REQ_FAILURE = "UPDATE_CHANGE_REQFAILURE";

export const FETCH_CHANGE_REQ_REQUEST = "FETCH_CHANGE_REQ_REQUEST";
export const FETCH_CHANGE_REQ_SUCCESS = "FETCH_CHANGE_REQ_SUCCESS";
export const FETCH_CHANGE_REQ_FAILURE = "FETCH_CHANGE_REQ_FAILURE";

export const REQUEST = "REQUEST";
export const SUCCESS = "SUCCESS";
export const FAILURE = "FAILURE";

export const FETCH_MEETING_PDF_PREVIEW_REQUEST =
  "FETCH_MEETING_PDF_PREVIEW_REQUEST";
export const FETCH_MEETING_PDF_PREVIEW_SUCCESS =
  "FETCH_MEETING_PDF_PREVIEW_SUCCESS";
export const FETCH_MEETING_PDF_PREVIEW_FAILURE =
  "FETCH_MEETING_PDF_PREVIEW_FAILURE";

export const MEETING_EMAIL_RECEPIENTS_REQUEST =
  "MEETING_EMAIL_RECEPIENTS_REQUEST";
export const MEETING_EMAIL_RECEPIENTS_SUCCESS =
  "MEETING_EMAIL_RECEPIENTS_SUCCESS";
export const MEETING_EMAIL_RECEPIENTS_FAILURE =
  "MEETING_EMAIL_RECEPIENTS_FAILURE";

export const FETCH_MEETING_EMAIL_PREVIEW_REQUEST =
  "FETCH_MEETING_EMAIL_PREVIEW_REQUEST";
export const FETCH_MEETING_EMAIL_PREVIEW_SUCCESS =
  "FETCH_MEETING_EMAIL_PREVIEW_SUCCESS";
export const FETCH_MEETING_EMAIL_PREVIEW_FAILURE =
  "FETCH_MEETING_EMAIL_PREVIEW_FAILURE";

export const PROJECT_ADDITIONAL_CONTACTS_REQUEST =
  "PROJECT_ADDITIONAL_CONTACTS_REQUEST";
export const PROJECT_ADDITIONAL_CONTACTS_SUCCESS =
  "PROJECT_ADDITIONAL_CONTACTS_SUCCESS";
export const PROJECT_ADDITIONAL_CONTACTS_FAILURE =
  "PROJECT_ADDITIONAL_CONTACTS_FAILURE";

export const EXTERNAL_MEETING_ATTENDEES_REQUEST =
  "EXTERNAL_MEETING_ATTENDEES_REQUEST";
export const EXTERNAL_MEETING_ATTENDEES_SUCCESS =
  "EXTERNAL_MEETING_ATTENDEES_SUCCESS";
export const EXTERNAL_MEETING_ATTENDEES_FAILURE =
  "EXTERNAL_MEETING_ATTENDEES_FAILURE";

export const FETCH_PROJECTS_LIST_REQUEST = "FETCH_PROJECTS_LIST_REQUEST";
export const FETCH_PROJECTS_LIST_SUCCESS = "FETCH_PROJECTS_LIST_SUCCESS";
export const FETCH_PROJECTS_LIST_FAILURE = "FETCH_PROJECTS_LIST_FAILURE";

export const FETCH_PROJECT_MANAGER_GRAPH_REQUEST =
  "FETCH_PROJECT_MANAGER_GRAPH_REQUEST";
export const FETCH_PROJECT_MANAGER_GRAPH_SUCCESS =
  "FETCH_PROJECT_MANAGER_GRAPH_SUCCESS";
export const FETCH_PROJECT_MANAGER_GRAPH_FAILURE =
  "FETCH_PROJECT_MANAGER_GRAPH_FAILURE";

export const FETCH_PROJECT_TYPE_GRAPH_REQUEST =
  "FETCH_PROJECT_TYPE_GRAPH_REQUEST";
export const FETCH_PROJECT_TYPE_GRAPH_SUCCESS =
  "FETCH_PROJECT_TYPE_GRAPH_SUCCESS";
export const FETCH_PROJECT_TYPE_GRAPH_FAILURE =
  "FETCH_PROJECT_TYPE_GRAPH_FAILURE";

export const FETCH_PROJECTS_CLOSING_GRAPH_REQUEST =
  "FETCH_PROJECTS_CLOSING_GRAPH_REQUEST";
export const FETCH_PROJECTS_CLOSING_GRAPH_SUCCESS =
  "FETCH_PROJECTS_CLOSING_GRAPH_SUCCESS";
export const FETCH_PROJECTS_CLOSING_GRAPH_FAILURE =
  "FETCH_PROJECTS_CLOSING_GRAPH_FAILURE";

export const FETCH_PROJECT_STATUS_GRAPH_REQUEST =
  "FETCH_PROJECT_STATUS_GRAPH_REQUEST";
export const FETCH_PROJECT_STATUS_GRAPH_SUCCESS =
  "FETCH_PROJECT_STATUS_GRAPH_SUCCESS";
export const FETCH_PROJECT_STATUS_GRAPH_FAILURE =
  "FETCH_PROJECT_STATUS_GRAPH_FAILURE";

export const TIME_ENTRY_SETTING_REQUEST = "TIME_ENTRY_SETTING_REQUEST";
export const TIME_ENTRY_SETTING_SUCCESS = "TIME_ENTRY_SETTING_SUCCESS";
export const TIME_ENTRY_SETTING_FAILURE = "TIME_ENTRY_SETTING_FAILURE";

export const TIME_ENTRY_REQUEST = "TIME_ENTRY_REQUEST";
export const TIME_ENTRY_SUCCESS = "TIME_ENTRY_SUCCESS";
export const TIME_ENTRY_FAILURE = "TIME_ENTRY_FAILURE";

export const COMPLETE_MEETING_REQUEST = "COMPLETE_MEETING_REQUEST";
export const COMPLETE_MEETING_SUCCESS = "COMPLETE_MEETING_SUCCESS";
export const COMPLETE_MEETING_FAILURE = "COMPLETE_MEETING_FAILURE";

export const FETCH_PMO_NOTES_REQUEST = "FETCH_PMO_NOTES_REQUEST";
export const FETCH_PMO_NOTES_SUCCESS = "FETCH_PMO_NOTES_SUCCESS";
export const FETCH_PMO_NOTES_FAILURE = "FETCH_PMO_NOTES_FAILURE";

export const EDIT_PMO_NOTES_REQUEST = "EDIT_PMO_NOTES_REQUEST";
export const EDIT_PMO_NOTES_SUCCESS = "EDIT_PMO_NOTES_SUCCESS";
export const EDIT_PMO_NOTES_FAILURE = "EDIT_PMO_NOTES_FAILURE";

export const UPDATE_TIMEENTRY_REQUEST = "UPDATE_TIMEENTRY_REQUEST";
export const UPDATE_TIMEENTRY_SUCCESS = "UPDATE_TIMEENTRY_SUCCESS";
export const UPDATE_TIMEENTRY_FAILURE = "UPDATE_TIMEENTRY_FAILURE";

export const CANCELLED_CR_SETTING_REQUEST = "CANCELLED_CR_SETTING_REQUEST";
export const CANCELLED_CR_SETTING_SUCCESS = "CANCELLED_CR_SETTING_SUCCESS";
export const CANCELLED_CR_SETTING_FAILURE = "CANCELLED_CR_SETTING_FAILURE";

export const postActivityStatus = (data: any, url: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/${url ? url : "pmo/settings/activity-status"}`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      NEW_ACTIVITY_STATUS_REQUEST,
      NEW_ACTIVITY_STATUS_SUCCESS,
      NEW_ACTIVITY_STATUS_FAILURE,
    ],
  },
});

export const editActivityStatus = (data: any, url: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/${
      url ? url : "pmo/settings/activity-status"
    }/${data.id}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      NEW_ACTIVITY_STATUS_REQUEST,
      NEW_ACTIVITY_STATUS_SUCCESS,
      NEW_ACTIVITY_STATUS_FAILURE,
    ],
  },
});

export const deleteActivityStatus = (data: any, url: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/${
      url ? url : "pmo/settings/activity-status"
    }/${data.id}`,
    method: "delete",
    authenticated: true,
    body: data,
    types: [
      NEW_ACTIVITY_STATUS_REQUEST,
      NEW_ACTIVITY_STATUS_SUCCESS,
      NEW_ACTIVITY_STATUS_FAILURE,
    ],
  },
});

export const getActivityStatusList = (url?: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/${url ? url : "pmo/settings/activity-status"}`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      FETCH_ACTIVITY_S_LIST_REQUEST,
      FETCH_ACTIVITY_S_LIST_SUCCESS,
      FETCH_ACTIVITY_S_LIST_FAILURE,
    ],
  },
});

export const getDateStatusList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/date-status`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      FETCH_DATE_S_LIST_REQUEST,
      FETCH_DATE_S_LIST_SUCCESS,
      FETCH_DATE_S_LIST_FAILURE,
    ],
  },
});

export const getTimeStatusList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/time-status`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      FETCH_TIME_S_LIST_REQUEST,
      FETCH_TIME_S_LIST_SUCCESS,
      FETCH_TIME_S_LIST_FAILURE,
    ],
  },
});

export const postTimeStatusList = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/time-status`,
    method: "post",
    body: data,
    authenticated: true,
    params: { pagination: false },
    types: [NEW_TIME_S_REQUEST, NEW_TIME_S_SUCCESS, NEW_TIME_S_FAILURE],
  },
});

export const postDateStatusList = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/date-status`,
    method: "post",
    body: data,
    authenticated: true,
    params: { pagination: false },
    types: [NEW_DATE_S_REQUEST, NEW_DATE_S_SUCCESS, NEW_DATE_S_FAILURE],
  },
});

//------------Project Types

export const postProjectType = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      NEW_PROJECT_TYPE_REQUEST,
      NEW_PROJECT_TYPE_SUCCESS,
      NEW_PROJECT_TYPE_FAILURE,
    ],
  },
});

export const editProjectType = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types/${data.id}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      NEW_PROJECT_TYPE_REQUEST,
      NEW_PROJECT_TYPE_SUCCESS,
      NEW_PROJECT_TYPE_FAILURE,
    ],
  },
});

export const deleteProjectType = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types/${data.id}`,
    method: "delete",
    authenticated: true,
    body: data,
    types: [
      NEW_PROJECT_TYPE_REQUEST,
      NEW_PROJECT_TYPE_SUCCESS,
      NEW_PROJECT_TYPE_FAILURE,
    ],
  },
});

export const getProjectTypeList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      FETCH_PROJECT_TYPE_LIST_REQUEST,
      FETCH_PROJECT_TYPE_LIST_SUCCESS,
      FETCH_PROJECT_TYPE_LIST_FAILURE,
    ],
  },
});

export const editProjectPhase = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types/${data.id}/phases`,
    method: "put",
    body: data.phases,
    authenticated: true,
    types: [
      NEW_PROJECT_TYPE_REQUEST,
      NEW_PROJECT_TYPE_SUCCESS,
      NEW_PROJECT_TYPE_FAILURE,
    ],
  },
});

export const postProjectPhase = (data: any, phase: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types/${data.id}/phases`,
    method: "post",
    body: phase,
    authenticated: true,
    types: [
      NEW_PROJECT_PHASE_REQUEST,
      NEW_PROJECT_PHASE_SUCCESS,
      NEW_PROJECT_PHASE_FAILURE,
    ],
  },
});

export const deleteProjectPhase = (data: any, phase: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types/${data.id}/phases/${phase.id}`,
    method: "delete",
    authenticated: true,
    types: [
      NEW_PROJECT_PHASE_REQUEST,
      NEW_PROJECT_PHASE_SUCCESS,
      NEW_PROJECT_PHASE_FAILURE,
    ],
  },
});

export const getProjectBoardList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-boards`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      FETCH_PROJECT_BOARD_LIST_REQUEST,
      FETCH_PROJECT_BOARD_LIST_SUCCESS,
      FETCH_PROJECT_BOARD_LIST_FAILURE,
    ],
  },
});

export const getProjectTeamMembers = (
  projectId: number,
  params: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/team-member-info`,
    method: "get",
    authenticated: true,
    params,
    types: [
      FETCH_PROJECT_TEAM_REQUEST,
      FETCH_PROJECT_TEAM_SUCCESS,
      FETCH_PROJECT_TEAM_FAILURE,
    ],
  },
});

export const updateProjectTeamMember = (
  projectId: number,
  memberId: string,
  data: IProjectTeamMember
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/team-member-info/${memberId}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      FETCH_PROJECT_TEAM_REQUEST,
      FETCH_PROJECT_TEAM_SUCCESS,
      FETCH_PROJECT_TEAM_FAILURE,
    ],
  },
});

export const getProjectBoardNames = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/project-boards`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      FETCH__BOARD_LIST_REQUEST_REQUEST,
      FETCH__BOARD_LIST_REQUEST_SUCCESS,
      FETCH__BOARD_LIST_REQUEST_FAILURE,
    ],
  },
});

export const postProjectBoard = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-boards`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const editProjectBoard = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-boards/${data.id}`,
    method: "put",
    body: data,
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const deleteProjectBoardStatus = (data: any, StatusId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-boards/${data.id}/overall-statuses/${StatusId}`,
    method: "delete",
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const deleteProjectBoard = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-boards/${data.id}`,
    method: "delete",
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const getProjectRoles = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-roles`,
    method: "get",
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const getProjectRolesMapping = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/engineering-roles`,
    method: "get",
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});
export const getPreSalesRolesMapping = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/pre-sales-engineering-roles`,
    method: "get",
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const updateProjectPreSalesRolesMapping = (
  data,
  method: string = "post"
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/pre-sales-engineering-roles`,
    method,
    body: { role_ids: data },
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const updateProjectRolesMapping = (data, method: string = "post") => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/engineering-roles`,
    method,
    body: { role_ids: data },
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const createContactRole = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/customer-contact-roles`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const getContactRoles = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/customer-contact-roles`,
    method: "get",
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const deleteContactRole = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/customer-contact-roles`,
    method: "delete",
    body: { deleted: [data.id] },
    authenticated: true,
    types: [
      NEW_PROJECT_BOARD_REQUEST,
      NEW_PROJECT_BOARD_SUCCESS,
      NEW_PROJECT_BOARD_FAILURE,
    ],
  },
});

export const getCustomerTouchMeetingTemplate = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/customer-touch-meeting-templates`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST,
      FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS,
      FETCH_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE,
    ],
  },
});

export const createCustomerTouchMeetingTemplate = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/customer-touch-meeting-templates`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST,
      CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS,
      CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE,
    ],
  },
});

export const udpateCustomerTouchMeetingTemplate = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/customer-touch-meeting-templates`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_REQUEST,
      CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_SUCCESS,
      CREATE_CUSTOMER_TOUCH_MAIL_TEMPLATE_FAILURE,
    ],
  },
});

export const meetingTemplateCRUD = (
  method: HTTPMethods,
  data?: IMeetingTemplate,
  type?: "Status" | "Kickoff"
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/meeting-templates${
      data && data.id ? `/${data.id}` : ""
    }`,
    method,
    body: data,
    params:
      method === "get"
        ? { meeting_type: type, pagination: false }
        : undefined,
    authenticated: true,
    types: [
      MEETING_TEMPLATE_REQUEST,
      MEETING_TEMPLATE_SUCCESS,
      MEETING_TEMPLATE_FAILURE,
    ],
  },
});

export const getProjectStatusList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/project-statuses`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      NEW_KICK_OFF_MEET_REQUEST,
      NEW_KICK_OFF_MEET_SUCCESS,
      NEW_KICK_OFF_MEET_FAILURE,
    ],
  },
});

export const GetAdditionalSetting = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/additional-settings`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      ADDITIONAL_SETTING_REQUEST,
      ADDITIONAL_SETTING_SUCCESS,
      ADDITIONAL_SETTING_FAILURE,
    ],
  },
});

export const updteAdditionalSetting = (data: any, method: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/additional-settings`,
    method: method,
    authenticated: true,
    body: data,
    params: { pagination: false },
    types: [
      ADDITIONAL_SETTING_REQUEST,
      ADDITIONAL_SETTING_SUCCESS,
      ADDITIONAL_SETTING_FAILURE,
    ],
  },
});

export const fetchProjects = (params?: IScrollPaginationFilters) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects`,
    method: "get",
    params: {
      page: params.nextPage,
      page_size: params.page_size,
      ordering: params.ordering,
      search: params.search,
      project_engineer_or_manager: params.project_engineer_or_manager,
      project_engineer: params.project_engineer,
      project_manager: params.project_manager,
      type: params.type,
      customer: params.customer,
      status: params.status,
      unmapped_opp: params.unmapped_opp,
      unmapped_sow: params.unmapped_sow,
      estimated_end_date_after: params.closed_start_date,
      estimated_end_date_before: params.closed_end_date,
      last_action_date_after: params.action_start_date,
      last_action_date_before: params.action_end_date,
      overbudget_projects: params.overbudget_projects,
      projects_closing_this_week: params.projects_closing_this_week,
      projects_with_overdue_action_items_or_overdue_critical_path_items:
        params.projects_with_overdue_action_items_or_overdue_critical_path_items,
    },
    authenticated: true,
    types: [
      FETCH_PROJECTS_REQUEST,
      FETCH_PROJECTS_SUCCESS,
      FETCH_PROJECTS_FAILURE,
    ],
  },
});

export const getProjectPhasesByID = (
  projectId: number,
  excludeCompleted?: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/phases`,
    method: "get",
    authenticated: true,
    params: { pagination: false, exclude_completed: excludeCompleted },
    types: [
      PROJECT_PHASES_REQUEST,
      PROJECT_PHASES_SUCCESS,
      PROJECT_PHASES_FAILURE,
    ],
  },
});

export const getProjectDetails = (projectId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}`,
    method: "get",
    authenticated: true,
    types: [
      PROJECT_DETAILS_REQUEST,
      PROJECT_DETAILS_SUCCESS,
      PROJECT_DETAILS_FAILURE,
    ],
  },
});

export const setProjectDetails = (projectId: number, data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}`,
    method: "patch",
    authenticated: true,
    body: data,
    types: [
      SET_PROJECT_DETAILS_REQUEST,
      SET_PROJECT_DETAILS_SUCCESS,
      SET_PROJECT_DETAILS_FAILURE,
    ],
  },
});

export const updateProjectDetails = (
  data: Partial<IProjectDetails>,
  projectId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}`,
    method: "patch",
    authenticated: true,
    body: data,
    types: [
      PROJECT_DETAILS_UPDATE_REQUEST,
      PROJECT_DETAILS_UPDATE_SUCCESS,
      PROJECT_DETAILS_UPDATE_FAILURE,
    ],
  },
});

export const getProjectTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-types`,
    method: "get",
    authenticated: true,
    types: [
      PROJECT_TYPES_REQUEST,
      PROJECT_TYPES_SUCCESS,
      PROJECT_TYPES_FAILURE,
    ],
  },
});

export const updateProjectPhasesStatusByID = (
  data: any,
  projectId: number,
  phaseId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/phases/${phaseId}`,
    method: "put",
    authenticated: true,
    body: data,
    params: { pagination: false },
    types: [
      PROJECT_PHASES_REQUEST,
      PROJECT_PHASES_SUCCESS,
      PROJECT_PHASES_FAILURE,
    ],
  },
});

export const getProjectNotesByID = (projectId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/notes`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const saveProjectNotesByID = (projectId: number, note: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/notes`,
    method: "post",
    body: { note },
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const editProjectNotesByID = (
  projectId: number,
  noteId: number,
  note: string,
  project_notes_crm_id?: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/notes/${noteId}`,
    method: "put",
    authenticated: true,
    body: { note, project_notes_crm_id },
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const deleteProjectNotesByID = (
  projectId: number,
  noteId: number,
  note_crm_id?: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/notes/${noteId}`,
    method: "delete",
    body: { project_notes_crm_id: note_crm_id },
    authenticated: true,
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const getProjectCWNotesByID = (projectId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/cw-notes`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const saveCWProjectNotesByID = (projectId: number, note: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/cw-notes`,
    method: "post",
    body: { note },
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const editCWProjectNotesByID = (
  projectId: number,
  note: string,
  note_crm_id: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/cw-notes`,
    method: "put",
    authenticated: true,
    body: { note, note_crm_id },
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const deleteCWProjectNotesByID = (
  projectId: number,
  note_crm_id?: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/cw-notes`,
    method: "delete",
    body: { note_crm_id },
    authenticated: true,
    types: [
      PROJECT_NOTES_REQUEST,
      PROJECT_NOTES_SUCCESS,
      PROJECT_NOTES_FAILURE,
    ],
  },
});

export const getMeetingNotesByProjectID = (
  projectId: number,
  meetingId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/notes`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_MEETING_NOTES_REQUEST,
      PROJECT_MEETING_NOTES_SUCCESS,
      PROJECT_MEETING_NOTES_FAILURE,
    ],
  },
});

export const saveMeetingNotesByProjectID = (
  projectId: number,
  meetingId: number,
  note: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/notes`,
    method: "post",
    body: { note },
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_MEETING_NOTES_REQUEST,
      PROJECT_MEETING_NOTES_SUCCESS,
      PROJECT_MEETING_NOTES_FAILURE,
    ],
  },
});

export const updateMeetingNotesByProjectID = (
  projectId: number,
  meetingId: number,
  noteId: number,
  note: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/notes/${noteId}`,
    method: "put",
    body: { note },
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_MEETING_NOTES_REQUEST,
      PROJECT_MEETING_NOTES_SUCCESS,
      PROJECT_MEETING_NOTES_FAILURE,
    ],
  },
});

export const deletetMeetingNotesByProjectID = (
  projectId: number,
  meetingId: number,
  noteId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/notes/${noteId}`,
    method: "delete",
    authenticated: true,
    types: [
      PROJECT_MEETING_NOTES_REQUEST,
      PROJECT_MEETING_NOTES_SUCCESS,
      PROJECT_MEETING_NOTES_FAILURE,
    ],
  },
});

export const postActionItem = (projectId: number, data: any, api: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}`,
    method: "post",
    authenticated: true,
    body: data,
    types: [ACTION_ITEM_REQUEST, ACTION_ITEM_SUCCESS, ACTION_ITEM_FAILURE],
  },
});

export const editActionItem = (projectId: number, data: any, api: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}/${data.id}`,
    method: data.id ? "put" : "post",
    authenticated: true,
    body: data,
    types: [ACTION_ITEM_REQUEST, ACTION_ITEM_SUCCESS, ACTION_ITEM_FAILURE],
  },
});

export const deleteActionItem = (
  projectId: number,
  itemId: any,
  api: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}/${itemId}`,
    method: "delete",
    authenticated: true,
    types: [ACTION_ITEM_REQUEST, ACTION_ITEM_SUCCESS, ACTION_ITEM_FAILURE],
  },
});

export const getActionItemList = (projectId: number, api: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [ACTION_ITEM_REQUEST, ACTION_ITEM_SUCCESS, ACTION_ITEM_FAILURE],
  },
});

export const getImpactList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/risk-item-impacts`,
    method: "get",
    authenticated: true,
    types: [
      STATUS_BY_PROJECT_REQUEST,
      STATUS_BY_PROJECT_SUCCESS,
      STATUS_BY_PROJECT_FAILURE,
    ],
  },
});

export const getProjectStatusByProjectID = (projectId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/statuses`,
    method: "get",
    authenticated: true,
    types: [
      STATUS_BY_PROJECT_REQUEST,
      STATUS_BY_PROJECT_SUCCESS,
      STATUS_BY_PROJECT_FAILURE,
    ],
  },
});

export const getProjectEngineersByID = (projectId: number, type?: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${
      type ? type : "engineers"
    }`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_ENGINEERS_REQUEST,
      PROJECT_ENGINEERS_SUCCESS,
      PROJECT_ENGINEERS_FAILURE,
    ],
  },
});

export const getProjectDocumentsByID = (projectId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/documents`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_DOCUMENTS_REQUEST,
      PROJECT_DOCUMENTS_SUCCESS,
      PROJECT_DOCUMENTS_FAILURE,
    ],
  },
});

export const saveProjectDocumentsByID = (projectId: number, data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/documents`,
    method: "post",
    body: data,
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_DOCUMENTS_REQUEST,
      PROJECT_DOCUMENTS_SUCCESS,
      PROJECT_DOCUMENTS_FAILURE,
    ],
  },
});

export const deleteProjectDocumentsByID = (
  projectId: number,
  documentId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/documents/${documentId}/delete`,
    method: "delete",
    authenticated: true,
    types: [
      PROJECT_DOCUMENTS_REQUEST,
      PROJECT_DOCUMENTS_SUCCESS,
      PROJECT_DOCUMENTS_FAILURE,
    ],
  },
});

export const fetchPMODashboardReview = (
  params: IServerPaginationParams & IPMODashboardFilterParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/review`,
    method: "get",
    params: params,
    authenticated: true,
    types: [
      FETCH_PMO_REVIEW_REQUEST,
      FETCH_PMO_REVIEW_SUCCESS,
      FETCH_PMO_REVIEW_FAILURE,
    ],
  },
});

export const getProjectCustomerContactsByID = (
  projectId: number,
  params: IServerPaginationParams = { pagination: false }
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/customer-contacts`,
    method: "get",
    authenticated: true,
    params,
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const fetchProjectCustomerContactsV2 = (
  projectId: number,
  params: IServerPaginationParams = { pagination: false }
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/customer-contacts-list`,
    method: "get",
    authenticated: true,
    params,
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const createCustomerContactV2 = (projectID: number, data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/customer-contacts-list`,
    authenticated: true,
    method: "post",
    body: data,
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const customerContactUD = (
  method: HTTPMethods,
  projectID: number,
  userId: string,
  data?: ICustomerContactItem
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/customer-contact/${userId}${
      method === "delete"
        ? `?contact_record_id=${data.contact_record_id}`
        : ""
    }`,
    method,
    authenticated: true,
    body: data,
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const saveProjectCustomerContactsByID = (
  projectId: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/customer-contacts`,
    method: "post",
    body: data,
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const updateCustomerContact = (
  projectId: number,
  contactCrmID: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/customer-contacts/${contactCrmID}`,
    method: "put",
    body: data,
    authenticated: true,
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const deleteCustomerContact = (
  projectId: number,
  contactCrmID: number,
  contactRecordId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/customer-contacts/${contactCrmID}`,
    method: "post",
    body: { contact_record_id: contactRecordId },
    authenticated: true,
    types: [
      PROJECT_CUSTOMER_CONTACTS_REQUEST,
      PROJECT_CUSTOMER_CONTACTS_SUCCESS,
      PROJECT_CUSTOMER_CONTACTS_FAILURE,
    ],
  },
});

export const getProjectManagers = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/managers`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_MANAGER_REQUEST,
      PROJECT_MANAGER_SUCCESS,
      PROJECT_MANAGER_FAILURE,
    ],
  },
});

export const getProjectEngineers = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/engineers`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_ENGINEERS_REQUEST,
      PROJECT_ENGINEERS_SUCCESS,
      PROJECT_ENGINEERS_FAILURE,
    ],
  },
});

export const getTeamMembers = (id) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${id}/team-members`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [FETCHING_REQUEST, FETCHING_SUCCESS, FETCHING_FAILURE],
  },
});

export const saveMeetingDetails = (
  projectId: number,
  data: IProjectMeeting,
  api: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}`,
    method: data.id ? "put" : "post",
    authenticated: true,
    body: data,
    types: [MEETING_REQUEST, MEETING_SUCCESS, MEETING_FAILURE],
  },
});

export const meetingComplete = (
  projectId: number,
  method: HTTPMethods,
  api: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}`,
    method: method,
    authenticated: true,
    types: [
      MEETING_COMPLETE_REQUEST,
      MEETING_COMPLETE_SUCCESS,
      MEETING_COMPLETE_FAILURE,
    ],
  },
});

export const editStatusMeeting = (
  projectId: number,
  data: any,
  api: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}/${data.id}`,
    method: data.id ? "put" : "post",
    authenticated: true,
    body: data,
    types: [MEETING_REQUEST, MEETING_SUCCESS, MEETING_FAILURE],
  },
});

export const deleteStatusMeeting = (
  projectId: number,
  itemId: any,
  api: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}/${itemId}`,
    method: "delete",
    authenticated: true,
    types: [MEETING_REQUEST, MEETING_SUCCESS, MEETING_FAILURE],
  },
});

export const getMeetingTemplates = (url: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/${url}`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [TEMPLATES_REQUEST, TEMPLATES_SUCCESS, TEMPLATES_FAILURE],
  },
});

export const getServiceBoardPMO = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/project-boards`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [TEMPLATES_REQUEST, TEMPLATES_SUCCESS, TEMPLATES_FAILURE],
  },
});

export const boardStatusListPMO = (id: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/boards/${id}/statuses`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [TEMPLATES_REQUEST, TEMPLATES_SUCCESS, TEMPLATES_FAILURE],
  },
});

export const getProjectTickets = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${id}/tickets`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_TICKETS_REQUEST,
      PROJECT_TICKETS_SUCCESS,
      PROJECT_TICKETS_FAILURE,
    ],
  },
});

export const meetingActionUpdate = (
  projectId: number,
  data: any,
  api: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${api}`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      MEETING_ACTION_REQUEST,
      MEETING_ACTION_SUCCESS,
      MEETING_ACTION_FAILURE,
    ],
  },
});

export const getMeetingDocumentsByProjectID = (
  projectId: number,
  meetingId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/documents`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_MEETING_DOCUMENTS_REQUEST,
      PROJECT_MEETING_DOCUMENTS_SUCCESS,
      PROJECT_MEETING_DOCUMENTS_FAILURE,
    ],
  },
});

export const saveMeetingDocumentsByProjectID = (
  projectId: number,
  meetingId: number,
  data
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/documents`,
    method: "post",
    body: data,
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_MEETING_DOCUMENTS_REQUEST,
      PROJECT_MEETING_DOCUMENTS_SUCCESS,
      PROJECT_MEETING_DOCUMENTS_FAILURE,
    ],
  },
});

export const deleteMeetingDocumentsByProjectID = (
  projectId: number,
  meetingId: number,
  documentId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/documents/${documentId}`,
    method: "delete",
    authenticated: true,
    types: [
      PROJECT_MEETING_DOCUMENTS_REQUEST,
      PROJECT_MEETING_DOCUMENTS_SUCCESS,
      PROJECT_MEETING_DOCUMENTS_FAILURE,
    ],
  },
});

export const checkSettings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/check`,
    method: "get",
    authenticated: true,
    types: [FETCHING_REQUEST, FETCHING_SUCCESS, FETCHING_FAILURE],
  },
});

export const getMeetingAttendeesByProjectID = (
  projectId: number,
  meetingId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/attendees`,
    method: "get",
    authenticated: true,
    types: [
      MEETING_ATTENDEES_REQUEST,
      MEETING_ATTENDEES_SUCCESS,
      MEETING_ATTENDEES_FAILURE,
    ],
  },
});

export const getCloseOutMeetingTemplate = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/close-out-meeting-templates`,
    method: "get",
    authenticated: true,
    types: [
      CLOSE_OUT_MEETING_TEMPLATE_REQUEST,
      CLOSE_OUT_MEETING_TEMPLATE_SUCCESS,
      CLOSE_OUT_MEETING_TEMPLATE_FAILURE,
    ],
  },
});

export const saveMeetingAttendeesByProjectID = (
  projectId: number,
  meetingId: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/attendees`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      MEETING_ATTENDEES_REQUEST,
      MEETING_ATTENDEES_SUCCESS,
      MEETING_ATTENDEES_FAILURE,
    ],
  },
});

export const createCloseOutMeetingTemplate = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/close-out-meeting-templates`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CLOSE_OUT_MEETING_TEMPLATE_REQUEST,
      CLOSE_OUT_MEETING_TEMPLATE_SUCCESS,
      CLOSE_OUT_MEETING_TEMPLATE_FAILURE,
    ],
  },
});

export const deleteMeetingAttendeesByProjectID = (
  projectId: number,
  meetingId: number,
  attendeeId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/attendees/${attendeeId}`,
    method: "delete",
    authenticated: true,
    types: [
      MEETING_ATTENDEES_REQUEST,
      MEETING_ATTENDEES_SUCCESS,
      MEETING_ATTENDEES_FAILURE,
    ],
  },
});

export const udpateCloseOutMeetingTemplate = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/close-out-meeting-templates`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      CLOSE_OUT_MEETING_TEMPLATE_REQUEST,
      CLOSE_OUT_MEETING_TEMPLATE_SUCCESS,
      CLOSE_OUT_MEETING_TEMPLATE_FAILURE,
    ],
  },
});

export const downloadMeetingDocument = (projectId, meetingId, documentId) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/documents/${documentId}/download`,
    method: "get",
    authenticated: true,
    types: [
      DOWNLOAD_MEETING_DOCUMENT_REQUEST,
      DOWNLOAD_MEETING_DOCUMENT_SUCCESS,
      DOWNLOAD_MEETING_DOCUMENT_FAILURE,
    ],
  },
});

export const getMeetingTemplateSetting = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/meeting-mail-templates`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_MEETING_MAIL_TEMPLATE_SETTING_REQUEST,
      FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS,
      FETCH_MEETING_MAIL_TEMPLATE_SETTING_FAILURE,
    ],
  },
});

export const createMeetingTemplateSetting = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/meeting-mail-templates`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_MEETING_MAIL_TEMPLATE_SETTING_REQUEST,
      CREATE_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS,
      CREATE_MEETING_MAIL_TEMPLATE_SETTING_FAILURE,
    ],
  },
});

export const updateMeetingTemplateSetting = (
  meetingTypeID: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/meeting-mail-templates/${meetingTypeID}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      UPDATE_MEETING_MAIL_TEMPLATE_SETTING_REQUEST,
      UPDATE_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS,
      UPDATE_MEETING_MAIL_TEMPLATE_SETTING_FAILURE,
    ],
  },
});

export const fetchChangeRequests = (
  ProjectId: number,
  params?: IScrollPaginationFilters
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${ProjectId}/change-requests`,
    method: "get",
    params: {
      page: params.nextPage,
      page_size: params.page_size,
      ordering: params.ordering,
      search: params.search,
      type: params.type,
      customer: params.customer,
      status: params.status,
    },
    authenticated: true,
    types: [
      FETCH_CHANGE_REQ_REQUEST,
      FETCH_CHANGE_REQ_SUCCESS,
      FETCH_CHANGE_REQ_FAILURE,
    ],
  },
});

export const fetchChangeRequestsAll = (
  params: IServerPaginationParams & ICRFilters
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/change-requests`,
    method: "get",
    params: params,
    authenticated: true,
    types: [
      FETCH_CHANGE_REQ_REQUEST,
      FETCH_CHANGE_REQ_SUCCESS,
      FETCH_CHANGE_REQ_FAILURE,
    ],
  },
});

export const createChangeRequests = (
  ProjectId: number,
  data: IChangeRequest
) => ({
  [CALL_API]: {
    endpoint: data.id
      ? `/api/v1/providers/pmo/projects/${ProjectId}/change-requests/${data.id}`
      : `/api/v1/providers/pmo/projects/${ProjectId}/change-requests`,
    method: data.id ? "put" : "post",
    body: data,
    authenticated: true,
    types: [
      UPDATE_CHANGE_REQ_REQUEST,
      UPDATE_CHANGE_REQ_SUCCESS,
      UPDATE_CHANGE_REQ_FAILURE,
    ],
  },
});

export const getChangeRequests = (
  ProjectId: number,
  changeRequestID: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${ProjectId}/change-requests/${changeRequestID}`,
    method: "get",
    authenticated: true,
    types: [
      UPDATE_CHANGE_REQ_REQUEST,
      UPDATE_CHANGE_REQ_SUCCESS,
      UPDATE_CHANGE_REQ_FAILURE,
    ],
  },
});

export const GetWorkTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/work-types`,
    method: "get",
    authenticated: true,
    types: ["FETCHING", "SUCCESS", "ERROR"],
  },
});
export const GetWorkRoles = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/work-roles`,
    method: "get",
    authenticated: true,
    types: ["FETCHING", "SUCCESS", "ERROR"],
  },
});

export const uploadProcessCR = (
  ProjectId: number,
  changeRequestID: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${ProjectId}/change-requests/${changeRequestID}/process`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      "PROCES_CR_UPLOAD_REQUEST",
      "PROCES_CR_UPLOAD_SUCCESS",
      "PROCES_CR_UPLOAD_FAILURE",
    ],
  },
});

export const pmoCommonAPI = (
  api: string,
  type: string,
  data?: any,
  params?: IScrollPaginationFilters
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/${api}`,
    method: type,
    authenticated: true,
    body: data,
    params: {
      page: params && params.nextPage,
      page_size: params && params.page_size,
      ordering: params && params.ordering,
      search: params && params.search,
    },
    types: [REQUEST, SUCCESS, FAILURE],
  },
});

export const getMeetingPDFPreview = (
  projectId: number,
  meetingId: number,
  completed: boolean,
  cc?: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/${
      completed ? "completed-meetings" : "meetings"
    }/${meetingId}/preview-pdf`,
    method: "get",
    authenticated: true,
    params: { add_customer_contacts: cc, pagination: false },
    types: [
      FETCH_MEETING_PDF_PREVIEW_REQUEST,
      FETCH_MEETING_PDF_PREVIEW_SUCCESS,
      FETCH_MEETING_PDF_PREVIEW_FAILURE,
    ],
  },
});

export const getMeetingEmailRecipients = (
  projectId: number,
  meetingId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/email/recipients`,
    method: "get",
    authenticated: true,
    types: [
      MEETING_EMAIL_RECEPIENTS_REQUEST,
      MEETING_EMAIL_RECEPIENTS_SUCCESS,
      MEETING_EMAIL_RECEPIENTS_FAILURE,
    ],
  },
});

export const getMeetingPDFPreviewV2 = (
  projectId: number,
  meetingId: number,
  project_update: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/preview-pdf/v2`,
    method: "put",
    body: { project_update },
    authenticated: true,
    types: [
      FETCH_MEETING_PDF_PREVIEW_REQUEST,
      FETCH_MEETING_PDF_PREVIEW_SUCCESS,
      FETCH_MEETING_PDF_PREVIEW_FAILURE,
    ],
  },
});

export const getMeetingEmailPreview = (
  projectId: number,
  meetingId: number,
  email_data: CloseMeetingEmail
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/email/preview`,
    method: "put",
    body: email_data,
    authenticated: true,
    types: [
      FETCH_MEETING_EMAIL_PREVIEW_REQUEST,
      FETCH_MEETING_EMAIL_PREVIEW_SUCCESS,
      FETCH_MEETING_EMAIL_PREVIEW_FAILURE,
    ],
  },
});

export const timeEntrySetting = (
  method: HTTPMethods,
  data?: TimeEntrySetting
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/meeting-closure/time-entry`,
    method,
    body: data,
    authenticated: true,
    types: [
      TIME_ENTRY_SETTING_REQUEST,
      TIME_ENTRY_SETTING_SUCCESS,
      TIME_ENTRY_SETTING_FAILURE,
    ],
  },
});

export const createTimeEntries = (
  projectID: number,
  meetingID: number,
  data: MeetingTimeEntry
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/meetings/${meetingID}/time-entries`,
    method: "put",
    body: data,
    authenticated: true,
    types: [TIME_ENTRY_REQUEST, TIME_ENTRY_SUCCESS, TIME_ENTRY_FAILURE],
  },
});

export const fetchTimeEntries = (meetingID: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/meetings/${meetingID}/time-entries?pagination=false`,
    method: "get",
    authenticated: true,
    types: [TIME_ENTRY_REQUEST, TIME_ENTRY_SUCCESS, TIME_ENTRY_FAILURE],
  },
});

export const makeMeetingComplete = (
  projectID: number,
  meetingType: string,
  meetingID: number,
  data: FormData
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/${meetingType}/${meetingID}/actions/complete/v2`,
    method: "put",
    body: data,
    authenticated: true,
    types: [
      COMPLETE_MEETING_REQUEST,
      COMPLETE_MEETING_SUCCESS,
      COMPLETE_MEETING_FAILURE,
    ],
  },
});

export const fetchProjectsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects`,
    method: "get",
    params: { pagination: false },
    authenticated: true,
    types: [
      "FETCH_PROJECTS_LIST_REQUEST",
      "FETCH_PROJECTS_LIST_SUCCESS",
      "FETCH_PROJECTS_LIST_FAILURE",
    ],
  },
});

export const fetchProjectsForCustomer = (ids) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/sow-linked-closed-projects`,
    method: "get",
    params: {
      customer: ids && ids.length ? ids.join(",") : "",
      status: "closed",
    },
    authenticated: true,
    types: [
      "FETCH_PROJECTS_LIST_REQUEST",
      "FETCH_PROJECTS_LIST_SUCCESS",
      "FETCH_PROJECTS_LIST_FAILURE",
    ],
  },
});

export const reportProjectAudit = (data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/audit/report`,
    method: "post",
    body: data,
    authenticated: true,
    responseType: "blob",
    types: [
      "PROJECT_AUDIT_REQUEST",
      "PROJECT_AUDIT_SUCCESS",
      "PROJECT_AUDIT_FAILURE",
    ],
  },
});

export const getTimesheetPeriod = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/timesheet-periods`,
    method: "get",
    authenticated: true,
    types: ["FETCHING", "SUCCESS", "ERROR"],
  },
});

export const getDraft = (data: any, method: string, rangeId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/drafts`,
    method,
    authenticated: true,
    params: { time_period_id: rangeId },
    body: data,
    types: [
      "DRAFT_TIMESHEET_REQUEST",
      "DRAFT_TIMESHEET_SUCCESS",
      "DRAFT_TIMESHEET_FAILURE",
    ],
  },
});

export const saveAsDraft = (data: any, method: string, draftId: any) => ({
  [CALL_API]: {
    endpoint: draftId
      ? `/api/v1/providers/pmo/timesheet-entries/drafts/${draftId}`
      : `/api/v1/providers/pmo/timesheet-entries/drafts`,
    method,
    authenticated: true,
    body: data,
    // params:{"time_period_id" : rangeId},
    types: [
      "SAVE_DRAFT_TIMESHEET_REQUEST",
      "SAVE_DRAFT_TIMESHEET_SUCCESS",
      "SAVE_DRAFT_TIMESHEET_FAILURE",
    ],
  },
});
export const getTimeEntries = (data: any, method: string, rangeId: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries`,
    method,
    authenticated: true,
    params: { time_sheet_id: rangeId },
    types: ["TIMESHEET_REQUEST", "TIMESHEET_SUCCESS", "TIMESHEET_FAILURE"],
  },
});

export const deleteTimeEntry = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/update-delete`,
    method: "delete",
    authenticated: true,
    params: { time_entry_id: id },
    types: [
      UPDATE_TIMEENTRY_REQUEST,
      UPDATE_TIMEENTRY_SUCCESS,
      UPDATE_TIMEENTRY_FAILURE,
    ],
  },
});

export const updateTimeEntry = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/update-delete`,
    method: "PUT",
    body: data,
    authenticated: true,
    params: { time_entry_id: data.time_entry_id },
    types: [
      UPDATE_TIMEENTRY_REQUEST,
      UPDATE_TIMEENTRY_SUCCESS,
      UPDATE_TIMEENTRY_FAILURE,
    ],
  },
});

export const submitTimesheet = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/submit`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      "SUBMIT_TIMESHEET_REQUEST",
      "SUBMIT_TIMESHEET_SUCCESS",
      "SUBMIT_TIMESHEET_FAILURE",
    ],
  },
});

export const fetchTimesheetProjectsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/projects`,
    method: "get",
    authenticated: true,
    types: [
      "FETCH_PROJECTS_TS_REQUEST",
      "FETCH_PROJECTS_TS_SUCCESS",
      "FETCH_PROJECTS_TS_FAILURE",
    ],
  },
});

export const timeSheetSettings = (data: any, method: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/settings`,
    method,
    authenticated: true,
    body: data,
    types: [
      "TIMESHEET_SETTING_REQUEST",
      "TIMESHEET_SETTING_SUCCESS",
      "TIMESHEET_SETTING_FAILURE",
    ],
  },
});
export const getChargeCodes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/timesheet-entries/charge-codes`,
    method: "get",
    authenticated: true,
    types: [
      "FETCH_CHARGE_CODE_REQUEST",
      "FETCH_CHARGE_CODE_SUCCESS",
      "FETCH_CHARGE_CODE_FAILURE",
    ],
  },
});

export const getProjectData = (url: string, data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/${url}`,
    method: "get",
    params: data,
    authenticated: true,
    types: [GET_DATA_REQUEST, GET_DATA_SUCCESS, GET_DATA_FAILURE],
  },
});

// Merge this 4 APIs into one
export const getProjectAdditionalContactsByID = (
  projectId: number,
  params: IServerPaginationParams = { pagination: false }
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/additional-contacts`,
    method: "get",
    authenticated: true,
    params,
    types: [
      PROJECT_ADDITIONAL_CONTACTS_REQUEST,
      PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
      PROJECT_ADDITIONAL_CONTACTS_FAILURE,
    ],
  },
});

export const saveProjectAdditionalContactsByID = (
  projectId: number,
  data: IProjectAdditionalContact
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/additional-contacts`,
    method: "post",
    body: data,
    authenticated: true,
    params: { pagination: false },
    types: [
      PROJECT_ADDITIONAL_CONTACTS_REQUEST,
      PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
      PROJECT_ADDITIONAL_CONTACTS_FAILURE,
    ],
  },
});

export const updateAdditionalContact = (
  projectId: number,
  contactId: number,
  data: IProjectAdditionalContact
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/additional-contacts/${contactId}`,
    method: "put",
    body: data,
    authenticated: true,
    types: [
      PROJECT_ADDITIONAL_CONTACTS_REQUEST,
      PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
      PROJECT_ADDITIONAL_CONTACTS_FAILURE,
    ],
  },
});

export const deleteAdditionalContact = (
  projectId: number,
  contactId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/additional-contacts/${contactId}`,
    method: "delete",
    authenticated: true,
    types: [
      PROJECT_ADDITIONAL_CONTACTS_REQUEST,
      PROJECT_ADDITIONAL_CONTACTS_SUCCESS,
      PROJECT_ADDITIONAL_CONTACTS_FAILURE,
    ],
  },
});

export const getExternalMeetingAttendeesByProjectID = (
  projectId: number,
  meetingId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/external_attendees`,
    method: "get",
    authenticated: true,
    types: [
      EXTERNAL_MEETING_ATTENDEES_REQUEST,
      EXTERNAL_MEETING_ATTENDEES_SUCCESS,
      EXTERNAL_MEETING_ATTENDEES_FAILURE,
    ],
  },
});

export const cancelledCRSettingCRU = (
  method: HTTPMethods,
  data?: ICancelledCRSetting
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/cancelled-change-requests/setting`,
    method: method,
    body: data,
    authenticated: true,
    types: [
      CANCELLED_CR_SETTING_REQUEST,
      CANCELLED_CR_SETTING_SUCCESS,
      CANCELLED_CR_SETTING_FAILURE,
    ],
  },
});

export const saveExternalMeetingAttendeesByProjectID = (
  projectId: number,
  meetingId: number,
  data: any
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/external_attendees`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      EXTERNAL_MEETING_ATTENDEES_REQUEST,
      EXTERNAL_MEETING_ATTENDEES_SUCCESS,
      EXTERNAL_MEETING_ATTENDEES_FAILURE,
    ],
  },
});

export const deleteExternalMeetingAttendeesByProjectID = (
  projectId: number,
  meetingId: number,
  attendeeId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectId}/meetings/${meetingId}/external_attendees/${attendeeId}/delete`,
    method: "delete",
    authenticated: true,
    types: [
      EXTERNAL_MEETING_ATTENDEES_REQUEST,
      EXTERNAL_MEETING_ATTENDEES_SUCCESS,
      EXTERNAL_MEETING_ATTENDEES_FAILURE,
    ],
  },
});

export const getProjectAfterHoursSetting = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/after-hours-mapping`,
    method: "get",
    authenticated: true,
    params: { pagination: false },
    types: [REQUEST, SUCCESS, FAILURE],
  },
});

export const saveProjectAfterHoursSetting = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/after-hours-mapping`,
    method: "patch",
    authenticated: true,
    body: data,
    params: { pagination: false },
    types: [REQUEST, SUCCESS, FAILURE],
  },
});

export const getProjectsByProjectManager = (
  params: IPMODashboardFilterParams
) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/pmo/projects/metrics/manager-projects-count",
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_PROJECT_MANAGER_GRAPH_REQUEST,
      FETCH_PROJECT_MANAGER_GRAPH_SUCCESS,
      FETCH_PROJECT_MANAGER_GRAPH_FAILURE,
    ],
  },
});

export const getProjectsByType = (params: IPMODashboardFilterParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/project-type-count`,
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_PROJECT_TYPE_GRAPH_REQUEST,
      FETCH_PROJECT_TYPE_GRAPH_SUCCESS,
      FETCH_PROJECT_TYPE_GRAPH_FAILURE,
    ],
  },
});

export const getProjectsByClosingDate = (
  params: IPMODashboardFilterParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/project-date-range-count`,
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_PROJECTS_CLOSING_GRAPH_REQUEST,
      FETCH_PROJECTS_CLOSING_GRAPH_SUCCESS,
      FETCH_PROJECTS_CLOSING_GRAPH_FAILURE,
    ],
  },
});

export const getProjectsByStatus = (params: IPMODashboardFilterParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/project-status-count`,
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_PROJECT_STATUS_GRAPH_REQUEST,
      FETCH_PROJECT_STATUS_GRAPH_SUCCESS,
      FETCH_PROJECT_STATUS_GRAPH_FAILURE,
    ],
  },
});

export const fetchPMODashboardNotes = (
  params: IPMONotesPagination,
  archived: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/notes`,
    method: "get",
    params: { ...params, archived },
    authenticated: true,
    types: [
      FETCH_PMO_NOTES_REQUEST,
      FETCH_PMO_NOTES_SUCCESS,
      FETCH_PMO_NOTES_FAILURE,
    ],
  },
});

export const createPMONote = (note: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/notes`,
    method: "post",
    body: { note },
    authenticated: true,
    types: [
      FETCH_PMO_NOTES_REQUEST,
      FETCH_PMO_NOTES_SUCCESS,
      FETCH_PMO_NOTES_FAILURE,
    ],
  },
});

export const editPMONote = (
  noteId: number,
  method: "put" | "delete",
  data: IPMONote
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/metrics/notes/${noteId}`,
    method,
    body: method !== "delete" ? data : undefined,
    authenticated: true,
    types: [
      EDIT_PMO_NOTES_REQUEST,
      EDIT_PMO_NOTES_SUCCESS,
      EDIT_PMO_NOTES_FAILURE,
    ],
  },
});

// Project Setup APIs
export const fetchProjectSetupInfo = (projectID: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/setup/details`,
    method: "get",
    authenticated: true,
    types: [
      PROJECT_SETUP_REQUEST,
      PROJECT_SETUP_SUCCESS,
      PROJECT_SETUP_FAILURE,
    ],
  },
});

export const projectDefaultTETicketCRU = (
  method: HTTPMethods,
  projectID: number,
  data?: TimeEntryTickets
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/setup/time-entry-settings`,
    method,
    body: data,
    authenticated: true,
    types: [
      DEFAULT_TIME_ENTRY_TICKET_REQUEST,
      DEFAULT_TIME_ENTRY_TICKET_SUCCESS,
      DEFAULT_TIME_ENTRY_TICKET_FAILURE,
    ],
  },
});

export const getWorkplanPhases = (projectID: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/setup/workplan-phases`,
    method: "get",
    params: { pagination: false },
    authenticated: true,
    types: [
      PROJECT_WORK_PHASE_REQUEST,
      PROJECT_WORK_PHASE_SUCCESS,
      PROJECT_WORK_PHASE_FAILURE,
    ],
  },
});

export const getProjectResourceSummary = (projectID: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/resource-summary`,
    method: "get",
    authenticated: true,
    types: [
      PROJECT_RESOURCE_SUMMARY_REQUEST,
      PROJECT_RESOURCE_SUMMARY_SUCCESS,
      PROJECT_RESOURCE_SUMMARY_FAILURE,
    ],
  },
});

export const updateWorkplanPhases = (
  projectID: number,
  data: IProjectWorkPhase[]
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/projects/${projectID}/setup/workplan-phases/bulk-update`,
    method: "put",
    body: { mappings: data },
    authenticated: true,
    types: [
      PROJECT_WORK_PHASE_REQUEST,
      PROJECT_WORK_PHASE_SUCCESS,
      PROJECT_WORK_PHASE_FAILURE,
    ],
  },
});
