import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_BASE_TEMPLATE_REQUEST = "FETCH_BASE_TEMPLATE_REQUEST";
export const FETCH_BASE_TEMPLATE_SUCCESS = "FETCH_BASE_TEMPLATE_SUCCESS";
export const FETCH_BASE_TEMPLATE_FAILURE = "FETCH_BASE_TEMPLATE_FAILURE";

export const FETCH_DOC_TYPE_REQUEST = "FETCH_DOC_TYPE_REQUEST";
export const FETCH_DOC_TYPE_SUCCESS = "FETCH_DOC_TYPE_SUCCESS";
export const FETCH_DOC_TYPE_FAILURE = "FETCH_DOC_TYPE_FAILURE";

export const FETCH_TEMPLATES_REQUEST = "FETCH_TEMPLATES_REQUEST";
export const FETCH_TEMPLATES_SUCCESS = "FETCH_TEMPLATES_SUCCESS";
export const FETCH_TEMPLATES_FAILURE = "FETCH_TEMPLATES_FAILURE";

export const CREATE_TEMPLATE_REQUEST = "CREATE_TEMPLATE_REQUEST";
export const CREATE_TEMPLATE_SUCCESS = "CREATE_TEMPLATE_SUCCESS";
export const CREATE_TEMPLATE_FAILURE = "CREATE_TEMPLATE_FAILURE";

export const EDIT_TEMPLATE_REQUEST = "EDIT_TEMPLATE_REQUEST";
export const EDIT_TEMPLATE_SUCCESS = "EDIT_TEMPLATE_SUCCESS";
export const EDIT_TEMPLATE_FAILURE = "EDIT_TEMPLATE_FAILURE";

export const FETCH_TEMPLATE_REQUEST = "FETCH_TEMPLATE_REQUEST";
export const FETCH_TEMPLATE_SUCCESS = "FETCH_TEMPLATE_SUCCESS";
export const FETCH_TEMPLATE_FAILURE = "FETCH_TEMPLATE_FAILURE";

export const FETCH_CATEGORIES_REQUEST = "FETCH_CATEGORIES_REQUEST";
export const FETCH_CATEGORIES_SUCCESS = "FETCH_CATEGORIES_SUCCESS";
export const FETCH_CATEGORIES_FAILURE = "FETCH_CATEGORIES_FAILURE";

export const FETCH_SOWS_REQUEST = "FETCH_SOWS_REQUEST";
export const FETCH_SOWS_SUCCESS = "FETCH_SOWS_SUCCESS";
export const FETCH_SOWS_FAILURE = "FETCH_SOWS_FAILURE";

export const CREATE_SOW_REQUEST = "CREATE_SOW_REQUEST";
export const CREATE_SOW_SUCCESS = "CREATE_SOW_SUCCESS";
export const CREATE_SOW_FAILURE = "CREATE_SOW_FAILURE";

export const PREVIEW_SERVICE_DETAIL_REQUEST = "PREVIEW_SERVICE_DETAIL_REQUEST";
export const PREVIEW_SERVICE_DETAIL_SUCCESS = "PREVIEW_SERVICE_DETAIL_SUCCESS";
export const PREVIEW_SERVICE_DETAIL_FAILURE = "PREVIEW_SERVICE_DETAIL_FAILURE";

export const FETCH_SOW_DOC_LIST_REQUEST = "FETCH_SOW_DOC_LIST_REQUEST";
export const FETCH_SOW_DOC_LIST_SUCCESS = "FETCH_SOW_DOC_LIST_SUCCESS";
export const FETCH_SOW_DOC_LIST_FAILURE = "FETCH_SOW_DOC_LIST_FAILURE";

export const EDIT_SOW_REQUEST = "EDIT_SOW_REQUEST";
export const EDIT_SOW_SUCCESS = "EDIT_SOW_SUCCESS";
export const EDIT_SOW_FAILURE = "EDIT_SOW_FAILURE";

export const GET_STATUSES_REQUEST = "GET_STATUSES_REQUEST";
export const GET_STATUSES_SUCCESS = "GET_STATUSES_SUCCESS";
export const GET_STATUSES_FAILURE = "GET_STATUSES_FAILURE";

export const GET_BUSINESS_REQUEST = "GET_BUSINESS_REQUEST";
export const GET_BUSINESS_SUCCESS = "GET_BUSINESS_SUCCESS";
export const GET_BUSINESS_FAILURE = "GET_BUSINESS_FAILURE";

export const GET_SOW_DLT_HTR_REQUEST = "GET_SOW_DLT_HTR_REQUEST";
export const GET_SOW_DLT_HTR_SUCCESS = "GET_SOW_DLT_HTR_SUCCESS";
export const GET_SOW_DLT_HTR_FAILURE = "GET_SOW_DLT_HTR_FAILURE";

export const GET_SOW_CREATE_HTR_REQUEST = "GET_SOW_CREATE_HTR_REQUEST";
export const GET_SOW_CREATE_HTR_SUCCESS = "GET_SOW_CREATE_HTR_SUCCESS";
export const GET_SOW_CREATE_HTR_FAILURE = "GET_SOW_CREATE_HTR_FAILURE";

export const GET_SOW_HISTORY_REQUEST = "GET_SOW_HISTORY_REQUEST";
export const GET_SOW_HISTORY_SUCCESS = "GET_SOW_HISTORY_SUCCESS";
export const GET_SOW_HISTORY_FAILURE = "GET_SOW_HISTORY_FAILURE";

export const FETCH_SOW_REQUEST = "FETCH_SOW_REQUEST";
export const FETCH_SOW_SUCCESS = "FETCH_SOW_SUCCESS";
export const FETCH_SOW_FAILURE = "FETCH_SOW_FAILURE";

export const DOWNLOAD_SOW_REQUEST = "DOWNLOAD_SOW_REQUEST";
export const DOWNLOAD_SOW_SUCCESS = "DOWNLOAD_SOW_SUCCESS";
export const DOWNLOAD_SOW_FAILURE = "DOWNLOAD_SOW_FAILURE";

export const FETCH_Q_TYPES_REQUEST = "FETCH_Q_TYPES_REQUEST";
export const FETCH_Q_TYPES_SUCCESS = "FETCH_Q_TYPES_SUCCESS";
export const FETCH_Q_TYPES_FAILURE = "FETCH_Q_TYPES_FAILURE";

export const FETCH_Q_STAGE_REQUEST = "FETCH_Q_STAGE_REQUEST";
export const FETCH_Q_STAGE_SUCCESS = "FETCH_Q_STAGE_SUCCESS";
export const FETCH_Q_STAGE_FAILURE = "FETCH_Q_STAGE_FAILURE";

export const GET_QUOTES_REQUEST = "GET_QUOTES_REQUEST";
export const GET_QUOTES_SUCCESS = "GET_QUOTES_SUCCESS";
export const GET_QUOTES_FAILURE = "GET_QUOTES_FAILURE";

export const CREATE_QUOTES_REQUEST = "CREATE_QUOTES_REQUEST";
export const CREATE_QUOTES_SUCCESS = "CREATE_QUOTES_SUCCESS";
export const CREATE_QUOTES_FAILURE = "CREATE_QUOTES_FAILURE";

export const UPLOAD_IMAGE_CR_REQUEST = "UPLOAD_IMAGE_CR_REQUEST";
export const UPLOAD_IMAGE_CR_SUCCESS = "UPLOAD_IMAGE_CR_SUCCESS";
export const UPLOAD_IMAGE_CR_FAILURE = "UPLOAD_IMAGE_CR_FAILURE";

export const UPLOAD_IMAGE_REQUEST = "UPLOAD_IMAGE_REQUEST";
export const UPLOAD_IMAGE_SUCCESS = "UPLOAD_IMAGE_SUCCESS";
export const UPLOAD_IMAGE_FAILURE = "UPLOAD_IMAGE_FAILURE";

export const FETCH_BUSINESS_UNITS_REQUEST = "FETCH_BUSINESS_UNITS_REQUEST";
export const FETCH_BUSINESS_UNITS_SUCCESS = "FETCH_BUSINESS_UNITS_SUCCESS";
export const FETCH_BUSINESS_UNITS_FAILURE = "FETCH_BUSINESS_UNITS_FAILURE";

export const CREATE_CR_REQUEST = "CREATE_CR_REQUEST";
export const CREATE_CR_SUCCESS = "CREATE_CR_SUCCESS";
export const CREATE_CR_FAILURE = "CREATE_CR_FAILURE";

export const FETCH_SCL_REQUEST = "FETCH_SCL_REQUEST";
export const FETCH_SCL_SUCCESS = "FETCH_SCL_SUCCESS";
export const FETCH_SCL_FAILURE = "FETCH_SCL_FAILURE";

export const EDIT_SCL_REQUEST = "EDIT_SCL_REQUEST";
export const EDIT_SCL_SUCCESS = "EDIT_SCL_SUCCESS";
export const EDIT_SCL_FAILURE = "EDIT_SCL_FAILURE";

export const CREATE_SCL_REQUEST = "CREATE_SCL_REQUEST";
export const CREATE_SCL_SUCCESS = "CREATE_SCL_SUCCESS";
export const CREATE_SCL_FAILURE = "CREATE_SCL_FAILURE";

export const FETCH_SCLIST_SHORT_REQUEST = "FETCH_SCLIST_SHORT_REQUEST";
export const FETCH_SCLIST_SHORT_SUCCESS = "FETCH_SCLIST_SHORT_SUCCESS";
export const FETCH_SCLIST_SHORT_FAILURE = "FETCH_SCLIST_SHORT_FAILURE";

export const FETCH_SCLIST_REQUEST = "FETCH_SCLIST_REQUEST";
export const FETCH_SCLIST_SUCCESS = "FETCH_SCLIST_SUCCESS";
export const FETCH_SCLIST_FAILURE = "FETCH_SCLIST_FAILURE";

export const GET_SCC_REQUEST = "GET_SCC_REQUEST";
export const GET_SCC_SUCCESS = "GET_SCC_SUCCESS";
export const GET_SCC_FAILURE = "GET_SCC_FAILURE";

export const FETCH_SOW_DOC_LAST_UPDATED_REQUEST =
  "FETCH_SOW_DOC_LAST_UPDATED_REQUEST";
export const FETCH_SOW_DOC_LAST_UPDATED_SUCCESS =
  "FETCH_SOW_DOC_LAST_UPDATED_SUCCESS";
export const FETCH_SOW_DOC_LAST_UPDATED_FAILURE =
  "FETCH_SOW_DOC_LAST_UPDATED_FAILURE";

export const FETCH_SOW_AUTHORS_REQUEST = "FETCH_SOW_AUTHORS_REQUEST";
export const FETCH_SOW_AUTHORS_SUCCESS = "FETCH_SOW_AUTHORS_SUCCESS";
export const FETCH_SOW_AUTHORS_FAILURE = "FETCH_SOW_AUTHORS_FAILURE";

export const FETCH_SOW_BY_TECH_REQUEST = "FETCH_SOW_BY_TECH_REQUEST";
export const FETCH_SOW_BY_TECH_SUCCESS = "FETCH_SOW_BY_TECH_SUCCESS";
export const FETCH_SOW_BY_TECH_FAILURE = "FETCH_SOW_BY_TECH_FAILURE";

export const FETCH_SOW_MARGIN_REQUEST = "FETCH_SOW_MARGIN_REQUEST";
export const FETCH_SOW_MARGIN_SUCCESS = "FETCH_SOW_MARGIN_SUCCESS";
export const FETCH_SOW_MARGIN_FAILURE = "FETCH_SOW_MARGIN_FAILURE";

export const FETCH_TEMPLATE_PREVIEW_REQUEST = "FETCH_TEMPLATE_PREVIEW_REQUEST";
export const FETCH_TEMPLATE_PREVIEW_SUCCESS = "FETCH_TEMPLATE_PREVIEW_SUCCESS";
export const FETCH_TEMPLATE_PREVIEW_FAILURE = "FETCH_TEMPLATE_PREVIEW_FAILURE";

export const sowActionTypes = {
  CREATE_NOTE_REQUEST: "CREATE_NOTE_REQUEST",
  CREATE_NOTE_SUCCESS: "CREATE_NOTE_SUCCESS",
  CREATE_NOTE_FAILURE: "CREATE_NOTE_FAILURE",
  UPDATE_NOTE_REQUEST: "UPDATE_NOTE_REQUEST",
  UPDATE_NOTE_SUCCESS: "UPDATE_NOTE_SUCCESS",
  UPDATE_NOTE_FAILURE: "UPDATE_NOTE_FAILURE",
  GET_NOTES_REQUEST: "GET_NOTES_REQUEST",
  GET_NOTES_SUCCESS: "GET_NOTES_SUCCESS",
  GET_NOTES_FAILURE: "GET_NOTES_FAILURE",
  GET_ALL_NOTES_REQUEST: "GET_ALL_NOTES_REQUEST",
  GET_ALL_NOTES_SUCCESS: "GET_ALL_NOTES_SUCCESS",
  GET_ALL_NOTES_FAILURE: "GET_ALL_NOTES_FAILURE",
};

export const FETCH_SERVICE_BOARDS_REQUEST = "FETCH_SERVICE_BOARDS_REQUEST";
export const FETCH_SERVICE_BOARDS_SUCCESS = "FETCH_SERVICE_BOARDS_SUCCESS";
export const FETCH_SERVICE_BOARDS_FAILURE = "FETCH_SERVICE_BOARDS_FAILURE";

export const GET_TABLE_REQUEST = "GET_TABLE_REQUEST";
export const GET_TABLE_SUCCESS = "GET_TABLE_SUCCESS";
export const GET_TABLE_FAILURE = "GET_TABLE_FAILURE";

export const GET_FULL_TABLE_REQUEST = "GET_FULL_TABLE_REQUEST";
export const GET_FULL_TABLE_SUCCESS = "GET_FULL_TABLE_SUCCESS";
export const GET_FULL_TABLE_FAILURE = "GET_FULL_TABLE_FAILURE";

export const GET_SOW_GP_REQUEST = "GET_SOW_GP_REQUEST";
export const GET_SOW_GP_SUCCESS = "GET_SOW_GP_SUCCESS";
export const GET_SOW_GP_FAILURE = "GET_SOW_GP_FAILURE";

export const SAVE_VENDOR_REQUEST = "SAVE_VENDOR_REQUEST";
export const SAVE_VENDOR_SUCCESS = "SAVE_VENDOR_SUCCESS";
export const SAVE_VENDOR_FAILURE = "SAVE_VENDOR_FAILURE";

export const GET_VENDORS_REQUEST = "GET_VENDORS_REQUEST";
export const GET_VENDORS_SUCCESS = "GET_VENDORS_SUCCESS";
export const GET_VENDORS_FAILURE = "GET_VENDORS_FAILURE";

export const GET_OPEN_OPP_REQUEST = "GET_OPEN_OPP_REQUEST";
export const GET_OPEN_OPP_SUCCESS = "GET_OPEN_OPP_SUCCESS";
export const GET_OPEN_OPP_FAILURE = "GET_OPEN_OPP_FAILURE";

export const GET_VENDOR_MAPPING_REQUEST = "GET_VENDOR_MAPPING_REQUEST";
export const GET_VENDOR_MAPPING_SUCCESS = "GET_VENDOR_MAPPING_SUCCESS";
export const GET_VENDOR_MAPPING_FAILURE = "GET_VENDOR_MAPPING_FAILURE";

export const SAVE_VENDOR_CONTACT_REQUEST = "SAVE_VENDOR_CONTACT_REQUEST";
export const SAVE_VENDOR_CONTACT_SUCCESS = "SAVE_VENDOR_CONTACT_SUCCESS";
export const SAVE_VENDOR_CONTACT_FAILURE = "SAVE_VENDOR_CONTACT_FAILURE";

export const GET_CONTACT_TYPES_REQUEST = "GET_CONTACT_TYPES_REQUEST";
export const GET_CONTACT_TYPES_SUCCESS = "GET_CONTACT_TYPES_SUCCESS";
export const GET_CONTACT_TYPES_FAILURE = "GET_CONTACT_TYPES_FAILURE";

export const getBaseTemplate = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/base-templates",
    method: "get",
    authenticated: true,
    types: [
      FETCH_BASE_TEMPLATE_REQUEST,
      FETCH_BASE_TEMPLATE_SUCCESS,
      FETCH_BASE_TEMPLATE_FAILURE,
    ],
  },
});

export const getDocumentType = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/types",
    method: "get",
    authenticated: true,
    types: [
      FETCH_DOC_TYPE_REQUEST,
      FETCH_DOC_TYPE_SUCCESS,
      FETCH_DOC_TYPE_FAILURE,
    ],
  },
});

export const getTemplate = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/${id}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_TEMPLATE_REQUEST,
      FETCH_TEMPLATE_SUCCESS,
      FETCH_TEMPLATE_FAILURE,
    ],
  },
});
export const getTemplateList = (show: boolean = false) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates?is_disabled=${show}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_TEMPLATES_REQUEST,
      FETCH_TEMPLATES_SUCCESS,
      FETCH_TEMPLATES_FAILURE,
    ],
  },
});

export const saveTemplate = (data: ISoWTemplate) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/templates",
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_TEMPLATE_REQUEST,
      CREATE_TEMPLATE_SUCCESS,
      CREATE_TEMPLATE_FAILURE,
    ],
  },
});

export const updateTemplate = (data: ISoWTemplate) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/${data.id}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      EDIT_TEMPLATE_REQUEST,
      EDIT_TEMPLATE_SUCCESS,
      EDIT_TEMPLATE_FAILURE,
    ],
  },
});

export const deleteTemplate = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/${id}`,
    method: "delete",
    authenticated: true,
    types: [
      EDIT_TEMPLATE_REQUEST,
      EDIT_TEMPLATE_SUCCESS,
      EDIT_TEMPLATE_FAILURE,
    ],
  },
});

export const getCategoryList = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/category",
    method: "get",
    authenticated: true,
    types: [
      FETCH_CATEGORIES_REQUEST,
      FETCH_CATEGORIES_SUCCESS,
      FETCH_CATEGORIES_FAILURE,
    ],
  },
});

export const getSOW = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/${id}`,
    method: "get",
    authenticated: true,
    types: [FETCH_SOW_REQUEST, FETCH_SOW_SUCCESS, FETCH_SOW_FAILURE],
  },
});
export const getSOWList = (
  show: boolean = false,
  closed: boolean = false,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/document?show-older=${show}&show-closed=${closed}`,
    method: "get",
    params,
    authenticated: true,
    types: [FETCH_SOWS_REQUEST, FETCH_SOWS_SUCCESS, FETCH_SOWS_FAILURE],
  },
});

export const saveSOW = (
  data: ISoW,
  sendEmail: boolean,
  sendEmailViaGraph: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document?email-account-manager=${sendEmail}&email-account-manager-via-graph=${sendEmailViaGraph}`,
    method: "post",
    authenticated: true,
    body: data,
    types: [CREATE_SOW_REQUEST, CREATE_SOW_SUCCESS, CREATE_SOW_FAILURE],
  },
});

export const getSOWGrossProfit = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/calculation/gross-profit`,
    method: "post",
    authenticated: true,
    body: data,
    types: [GET_SOW_GP_REQUEST, GET_SOW_GP_SUCCESS, GET_SOW_GP_FAILURE],
  },
});
export const getSOWGrossProfitCalculations = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/calculation/calculation-details`,
    method: "post",
    authenticated: true,
    body: data,
    types: [GET_SOW_GP_REQUEST, GET_SOW_GP_SUCCESS, GET_SOW_GP_FAILURE],
  },
});
export const getSOWHistory = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/${id}/change-history?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_SOW_HISTORY_REQUEST,
      GET_SOW_HISTORY_SUCCESS,
      GET_SOW_HISTORY_FAILURE,
    ],
  },
});

export const getDeletedSOWHistory = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/delete-history`,
    method: "get",
    params,
    authenticated: true,
    types: [
      GET_SOW_DLT_HTR_REQUEST,
      GET_SOW_DLT_HTR_SUCCESS,
      GET_SOW_DLT_HTR_FAILURE,
    ],
  },
});

export const getCreatedSOWHistory = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/create-history`,
    method: "get",
    params,
    authenticated: true,
    types: [
      GET_SOW_CREATE_HTR_REQUEST,
      GET_SOW_CREATE_HTR_SUCCESS,
      GET_SOW_CREATE_HTR_FAILURE,
    ],
  },
});
export const getTemplateHistory = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/${id}/change-history?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_SOW_HISTORY_REQUEST,
      GET_SOW_HISTORY_SUCCESS,
      GET_SOW_HISTORY_FAILURE,
    ],
  },
});

export const getCreatedTemplateCDHistory = (
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/create-history`,
    method: "get",
    params,
    authenticated: true,
    types: [
      GET_SOW_CREATE_HTR_REQUEST,
      GET_SOW_CREATE_HTR_SUCCESS,
      GET_SOW_CREATE_HTR_FAILURE,
    ],
  },
});

export const getDeletedTemplateCDHistory = (
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/delete-history`,
    method: "get",
    params,
    authenticated: true,
    types: [
      GET_SOW_DLT_HTR_REQUEST,
      GET_SOW_DLT_HTR_SUCCESS,
      GET_SOW_DLT_HTR_FAILURE,
    ],
  },
});

export const revertDeletedTemplate = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/templates/delete-history`,
    method: "post",
    body: { version_id: id },
    authenticated: true,
    types: [
      FETCH_TEMPLATE_REQUEST,
      FETCH_TEMPLATE_SUCCESS,
      FETCH_TEMPLATE_FAILURE,
    ],
  },
});
export const previewServiceDetail = (data: ISoWServiceDetailPayload) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/service-detail/preview`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      PREVIEW_SERVICE_DETAIL_REQUEST,
      PREVIEW_SERVICE_DETAIL_SUCCESS,
      PREVIEW_SERVICE_DETAIL_FAILURE,
    ],
  },
});

export const downloadServiceDetail = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/${id}/service-detail/download`,
    method: "get",
    authenticated: true,
    types: [
      PREVIEW_SERVICE_DETAIL_REQUEST,
      PREVIEW_SERVICE_DETAIL_SUCCESS,
      PREVIEW_SERVICE_DETAIL_FAILURE,
    ],
  },
});

export const fetchSowDocs = (params: IServerPaginationParams & ISowDocListFilters) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sow-documents`,
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_SOW_DOC_LIST_REQUEST,
      FETCH_SOW_DOC_LIST_SUCCESS,
      FETCH_SOW_DOC_LIST_FAILURE,
    ],
  },
});

export const previewSOW = (data: ISoW | IChangeRequest) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/preview`,
    method: "post",
    authenticated: true,
    body: data,
    types: [CREATE_SOW_REQUEST, CREATE_SOW_SUCCESS, CREATE_SOW_FAILURE],
  },
});

export const updateSOW = (
  data: ISoW,
  sendEmail: boolean,
  sendEmailViaGraph: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/${data.id}?email-account-manager=${sendEmail}&email-account-manager-via-graph=${sendEmailViaGraph}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [EDIT_SOW_REQUEST, EDIT_SOW_SUCCESS, EDIT_SOW_FAILURE],
  },
});

export const deleteSOW = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/${id}`,
    method: "delete",
    authenticated: true,
    types: [EDIT_SOW_REQUEST, EDIT_SOW_SUCCESS, EDIT_SOW_FAILURE],
  },
});
export const sendEmailToAccountManager = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/${id}/send-account-manager-email`,
    method: "post",
    authenticated: true,
    types: [EDIT_SOW_REQUEST, EDIT_SOW_SUCCESS, EDIT_SOW_FAILURE],
  },
});

export const sendEmailToAccountManagerViaGraph = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/graph-test/document/${id}/send-account-manager-email`,
    method: "post",
    authenticated: true,
    types: [EDIT_SOW_REQUEST, EDIT_SOW_SUCCESS, EDIT_SOW_FAILURE],
  },
});

export const downloadSOWDocumnet = (
  id: number,
  type: string,
  name: string = ""
) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/document/${id}/download?type=${type}&name=${name}`,
    method: "get",
    authenticated: true,
    types: [DOWNLOAD_SOW_REQUEST, DOWNLOAD_SOW_SUCCESS, DOWNLOAD_SOW_FAILURE],
  },
});

export const getQuoteTypeList = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quotes/types",
    method: "get",
    authenticated: true,
    types: [
      FETCH_Q_TYPES_REQUEST,
      FETCH_Q_TYPES_SUCCESS,
      FETCH_Q_TYPES_FAILURE,
    ],
  },
});

export const getQuoteStatusesList = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quotes/statuses",
    method: "get",
    authenticated: true,
    types: [GET_STATUSES_REQUEST, GET_STATUSES_SUCCESS, GET_STATUSES_FAILURE],
  },
});

export const getQuoteBusinessList = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quotes/business-units",
    method: "get",
    authenticated: true,
    types: [GET_BUSINESS_REQUEST, GET_BUSINESS_SUCCESS, GET_BUSINESS_FAILURE],
  },
});

export const getQuoteStageList = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quotes/stages",
    method: "get",
    authenticated: true,
    types: [
      FETCH_Q_STAGE_REQUEST,
      FETCH_Q_STAGE_SUCCESS,
      FETCH_Q_STAGE_FAILURE,
    ],
  },
});

export const createQuote = (customerId: number, data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/quotes`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_QUOTES_REQUEST,
      CREATE_QUOTES_SUCCESS,
      CREATE_QUOTES_FAILURE,
    ],
  },
});

export const getCustomerQuotes = (customerId: number, params: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/quotes`,
    method: "get",
    params,
    authenticated: true,
    types: [GET_QUOTES_REQUEST, GET_QUOTES_SUCCESS, GET_QUOTES_FAILURE],
  },
});

export const createQuoteSmartnet = (customerId: number, data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/workflows/smartnet-receiving/opportunity`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_QUOTES_REQUEST,
      CREATE_QUOTES_SUCCESS,
      CREATE_QUOTES_FAILURE,
    ],
  },
});

export const getquote = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quotes/stages",
    method: "get",
    authenticated: true,
    types: [
      CREATE_QUOTES_REQUEST,
      CREATE_QUOTES_SUCCESS,
      CREATE_QUOTES_FAILURE,
    ],
  },
});

export const uploadImage = (fileReq: FormData, name: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/image-upload?filename=${name}`,
    method: "post",
    body: fileReq,
    authenticated: true,
    types: [UPLOAD_IMAGE_REQUEST, UPLOAD_IMAGE_SUCCESS, UPLOAD_IMAGE_FAILURE],
  },
});

export const getSingleQuote = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/quotes/${id}`,
    method: "get",
    authenticated: true,
    types: [GET_QUOTES_REQUEST, GET_QUOTES_SUCCESS, GET_QUOTES_FAILURE],
  },
});

export const updateQuoteStage = (id: number, stageId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/quotes/${id}`,
    method: "put",
    body: { stage_id: stageId },
    authenticated: true,
    types: [GET_QUOTES_REQUEST, GET_QUOTES_SUCCESS, GET_QUOTES_FAILURE],
  },
});

export const uploadImagesForCR = (files: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/upload-attachments`,
    method: "post",
    body: files,
    authenticated: true,
    types: [
      UPLOAD_IMAGE_CR_REQUEST,
      UPLOAD_IMAGE_CR_SUCCESS,
      UPLOAD_IMAGE_CR_FAILURE,
    ],
  },
});

export const getBusinessUnits = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/business-units",
    method: "get",
    authenticated: true,
    types: [
      FETCH_BUSINESS_UNITS_REQUEST,
      FETCH_BUSINESS_UNITS_SUCCESS,
      FETCH_BUSINESS_UNITS_FAILURE,
    ],
  },
});

export const CreateCR = (data: any) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/change-request",
    method: "post",
    body: data,
    authenticated: true,
    types: [CREATE_CR_REQUEST, CREATE_CR_SUCCESS, CREATE_CR_FAILURE],
  },
});

export const getserviceCatalogList = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/service-catalogs`,
    method: "get",
    params,
    authenticated: true,
    types: [FETCH_SCLIST_REQUEST, FETCH_SCLIST_SUCCESS, FETCH_SCLIST_FAILURE],
  },
});

export const getShortServiceCatalogList = () => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/service-catalogs?fields=id,service_name,service_category,service_type&no_page`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_SCLIST_SHORT_REQUEST,
      FETCH_SCLIST_SHORT_SUCCESS,
      FETCH_SCLIST_SHORT_FAILURE,
    ],
  },
});

export const getserviceCatalog = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/service-catalogs/${id}`,
    method: "get",
    authenticated: true,
    types: [FETCH_SCL_REQUEST, FETCH_SCL_SUCCESS, FETCH_SCL_FAILURE],
  },
});

export const createServiceCatalog = (serviceCatalog: IserviceCatalog) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/service-catalogs`,
    method: "post",
    body: serviceCatalog,
    authenticated: true,
    types: [CREATE_SCL_REQUEST, CREATE_SCL_SUCCESS, CREATE_SCL_FAILURE],
  },
});

export const updateServiceCatalog = (
  id: any,
  serviceCatalog: IserviceCatalog
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/service-catalogs/${id}`,
    method: "put",
    body: serviceCatalog,
    authenticated: true,
    types: [EDIT_SCL_REQUEST, EDIT_SCL_SUCCESS, EDIT_SCL_FAILURE],
  },
});

export const deleteserviceCatalog = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/service-catalogs/${id}`,
    method: "delete",
    authenticated: true,
    types: [EDIT_SCL_REQUEST, EDIT_SCL_SUCCESS, EDIT_SCL_FAILURE],
  },
});

export const fetchServiceCategoriesFull = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/service-categories?get_ids=true&get_all=true",
    method: "get",
    authenticated: true,
    types: [GET_SCC_REQUEST, GET_SCC_SUCCESS, GET_SCC_FAILURE],
  },
});

export const createNotes = (customersId: number, note: ISalesNote) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-notes`,
    method: "post",
    body: { ...note, customer: customersId },
    authenticated: true,
    types: [
      sowActionTypes.CREATE_NOTE_REQUEST,
      sowActionTypes.CREATE_NOTE_SUCCESS,
      sowActionTypes.CREATE_NOTE_FAILURE,
    ],
  },
});

export const updateNotes = (customersId, note: ISalesNote) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-notes/${note.id}`,
    method: "put",
    body: { ...note, customer: customersId },
    authenticated: true,
    types: [
      sowActionTypes.UPDATE_NOTE_REQUEST,
      sowActionTypes.UPDATE_NOTE_SUCCESS,
      sowActionTypes.UPDATE_NOTE_FAILURE,
    ],
  },
});

export const deleteNotes = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-notes/${id}`,
    method: "delete",
    authenticated: true,
    types: [
      sowActionTypes.CREATE_NOTE_REQUEST,
      sowActionTypes.CREATE_NOTE_SUCCESS,
      sowActionTypes.CREATE_NOTE_FAILURE,
    ],
  },
});
export const getNote = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-notes/${id}`,
    method: "get",
    authenticated: true,
    types: [
      sowActionTypes.CREATE_NOTE_REQUEST,
      sowActionTypes.CREATE_NOTE_SUCCESS,
      sowActionTypes.CREATE_NOTE_FAILURE,
    ],
  },
});
export const getNotesList = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-notes`,
    method: "get",
    params,
    authenticated: true,
    types: [
      sowActionTypes.GET_NOTES_REQUEST,
      sowActionTypes.GET_NOTES_SUCCESS,
      sowActionTypes.GET_NOTES_FAILURE,
    ],
  },
});

export const getNotesListAll = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customer-notes`,
    method: "get",
    params,
    authenticated: true,
    types: [
      sowActionTypes.GET_ALL_NOTES_REQUEST,
      sowActionTypes.GET_ALL_NOTES_SUCCESS,
      sowActionTypes.GET_ALL_NOTES_FAILURE,
    ],
  },
});
export const getTemplateByType = (
  template_payload: ISoWTemplate,
  template_type: SowDocType
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/template/preview`,
    method: "post",
    body: { template_payload, template_type },
    authenticated: true,
    types: [DOWNLOAD_SOW_REQUEST, DOWNLOAD_SOW_SUCCESS, DOWNLOAD_SOW_FAILURE],
  },
});

export const getTemplateByTypeWithId = (
  templateId: number,
  template_type: string
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/template/${templateId}/preview`,
    method: "post",
    body: { template_type },
    authenticated: true,
    types: [
      FETCH_TEMPLATE_PREVIEW_REQUEST,
      FETCH_TEMPLATE_PREVIEW_SUCCESS,
      FETCH_TEMPLATE_PREVIEW_FAILURE,
    ],
  },
});

export const getServiceBoards = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/pmo/settings/service-boards",
    method: "get",
    authenticated: true,
    types: [
      FETCH_SERVICE_BOARDS_REQUEST,
      FETCH_SERVICE_BOARDS_SUCCESS,
      FETCH_SERVICE_BOARDS_FAILURE,
    ],
  },
});

export const getSOWDocumentsLastUpdated = (
  params: ISoWMetricsFilterParams
) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/sow-dashboard/sow-doc-update-stats",
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_SOW_DOC_LAST_UPDATED_REQUEST,
      FETCH_SOW_DOC_LAST_UPDATED_SUCCESS,
      FETCH_SOW_DOC_LAST_UPDATED_FAILURE,
    ],
  },
});

export const getSOWAuthors = (params: ISoWMetricsFilterParams) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/sow-dashboard/sow-doc-authors-list",
    method: "get",
    authenticated: true,
    params,
    types: [
      FETCH_SOW_AUTHORS_REQUEST,
      FETCH_SOW_AUTHORS_SUCCESS,
      FETCH_SOW_AUTHORS_FAILURE,
    ],
  },
});

export const getSOWByTechnologies = (params: ISoWMetricsFilterParams) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/sow-dashboard/sow-doc-count-by-type",
    method: "get",
    authenticated: true,
    params,
    types: [
      FETCH_SOW_BY_TECH_REQUEST,
      FETCH_SOW_BY_TECH_SUCCESS,
      FETCH_SOW_BY_TECH_FAILURE,
    ],
  },
});

export const getMarginData = (params: ISoWMetricsFilterParams) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/sow-dashboard/sow-doc-margin-chart",
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_SOW_MARGIN_REQUEST,
      FETCH_SOW_MARGIN_SUCCESS,
      FETCH_SOW_MARGIN_FAILURE,
    ],
  },
});

export const getTabledataSOWMetrics = (
  params?: IServerPaginationParams & ISoWMetricsFilterParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/sow-dashboard/sow-doc-table`,
    method: "get",
    params,
    authenticated: true,
    types: [GET_TABLE_REQUEST, GET_TABLE_SUCCESS, GET_TABLE_FAILURE],
  },
});

export const getFullSOWMetricsTable = (
  params?: IServerPaginationParams & ISoWMetricsFilterParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/sow-dashboard/sow-doc-table`,
    method: "get",
    params,
    authenticated: true,
    types: [
      GET_FULL_TABLE_REQUEST,
      GET_FULL_TABLE_SUCCESS,
      GET_FULL_TABLE_FAILURE,
    ],
  },
});

export const saveVendor = (data: IVendorDetails) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/vendors`,
    method: "post",
    body: data,
    authenticated: true,
    types: [SAVE_VENDOR_REQUEST, SAVE_VENDOR_SUCCESS, SAVE_VENDOR_FAILURE],
  },
});

export const saveVendorPrimaryContact = (data: IVendorContact) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/vendors/contact`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      SAVE_VENDOR_CONTACT_REQUEST,
      SAVE_VENDOR_CONTACT_SUCCESS,
      SAVE_VENDOR_CONTACT_FAILURE,
    ],
  },
});

export const getVendorContactTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/contact-types`,
    method: "get",
    authenticated: true,
    types: [
      GET_CONTACT_TYPES_REQUEST,
      GET_CONTACT_TYPES_SUCCESS,
      GET_CONTACT_TYPES_FAILURE,
    ],
  },
});

export const getOpenOppChart = (customerCrmId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/client360/open-opp-chart?customer_crm_id=${customerCrmId}`,
    method: "get",
    authenticated: true,
    types: [GET_OPEN_OPP_REQUEST, GET_OPEN_OPP_SUCCESS, GET_OPEN_OPP_FAILURE],
  },
});

export const getVendorsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/vendors`,
    method: "get",
    authenticated: true,
    types: [GET_VENDORS_REQUEST, GET_VENDORS_SUCCESS, GET_VENDORS_FAILURE],
  },
});

export const getVendorMappingList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/sow/resource-description?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_VENDOR_MAPPING_REQUEST,
      GET_VENDOR_MAPPING_SUCCESS,
      GET_VENDOR_MAPPING_FAILURE,
    ],
  },
});
