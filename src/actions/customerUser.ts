import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_CUSTOMER_USERS_REQUEST = 'FETCH_CUSTOMER_USERS_REQUEST';
export const FETCH_CUSTOMER_USERS_SUCCESS = 'FETCH_CUSTOMER_USERS_SUCCESS';
export const FETCH_CUSTOMER_USERS_FAILURE = 'FETCH_CUSTOMER_USERS_FAILURE';
export const CUSTOMER_USERS_CREATE_REQUEST = 'CUSTOMER_USERS_CREATE_REQUEST';
export const CUSTOMER_USERS_CREATE_SUCCESS = 'CUSTOMER_USERS_CREATE_SUCCESS';
export const CUSTOMER_USERS_CREATE_FAILURE = 'CUSTOMER_USERS_CREATE_FAILURE';
export const EDIT_CUSTOMER_USERS_REQUEST = 'EDIT_CUSTOMER_USERS_REQUEST';
export const EDIT_CUSTOMER_USERS_SUCCESS = 'EDIT_CUSTOMER_USERS_SUCCESS';
export const EDIT_CUSTOMER_USERS_FAILURE = 'EDIT_CUSTOMER_USERS_FAILURE';

export const FETCH_CUSTOMER_PROFILE_REQUEST = 'FETCH_CUSTOMER_PROFILE_REQUEST';
export const FETCH_CUSTOMER_PROFILE_SUCCESS = 'FETCH_CUSTOMER_PROFILE_SUCCESS';
export const FETCH_CUSTOMER_PROFILE_FAILURE = 'FETCH_CUSTOMER_PROFILE_FAILURE';
export const CREATE_CUSTOMERS_USER_REQUEST = 'CREATE_CUSTOMERS_USER_REQUEST';
export const CREATE_CUSTOMERS_USER_SUCCESS = 'CREATE_CUSTOMERS_USER_SUCCESS';
export const CREATE_CUSTOMERS_USER_FAILURE = 'CREATE_CUSTOMERS_USER_FAILURE';

export const PU_CU_CREATE_REQUEST = 'PU_CU_CREATE_REQUEST';
export const PU_CU_CREATE_SUCCESS = 'PU_CU_CREATE_SUCCESS';
export const PU_CU_CREATE_FAILURE = 'PU_CU_CREATE_FAILURE';

export const fetchCustomerUsers = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: '/api/v1/customers/users',
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_CUSTOMER_USERS_REQUEST,
      FETCH_CUSTOMER_USERS_SUCCESS,
      FETCH_CUSTOMER_USERS_FAILURE,
    ],
  },
});

export const createUserCustomer = (user: ICustomerUser) => ({
  [CALL_API]: {
    endpoint: '/api/v1/customers/users',
    method: 'post',
    authenticated: true,
    body: user,
    types: [
      CUSTOMER_USERS_CREATE_REQUEST,
      CUSTOMER_USERS_CREATE_SUCCESS,
      CUSTOMER_USERS_CREATE_FAILURE,
    ],
  },
});

export const createUserCustomerPU = (id: any, user: ICustomerUser) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/users`,
    method: 'post',
    authenticated: true,
    body: user,
    types: [PU_CU_CREATE_REQUEST, PU_CU_CREATE_SUCCESS, PU_CU_CREATE_FAILURE],
  },
});

export const editUserCustomer = (id: string, user: ICustomerUser) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/users/${id}`,
    method: 'put',
    authenticated: true,
    body: user,
    types: [
      EDIT_CUSTOMER_USERS_REQUEST,
      EDIT_CUSTOMER_USERS_SUCCESS,
      EDIT_CUSTOMER_USERS_FAILURE,
    ],
  },
});

export const fetchCustomerProfile = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/profile`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUSTOMER_PROFILE_REQUEST,
      FETCH_CUSTOMER_PROFILE_SUCCESS,
      FETCH_CUSTOMER_PROFILE_FAILURE,
    ],
  },
});

export const sendMailactivateCustomerUser = (user: ICustomerUser) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/users/${user.id}/activate`,
    method: 'put',
    authenticated: true,
    body: { user_role: user.user_role },
    types: [
      CREATE_CUSTOMERS_USER_REQUEST,
      CREATE_CUSTOMERS_USER_SUCCESS,
      CREATE_CUSTOMERS_USER_FAILURE,
    ],
  },
});
