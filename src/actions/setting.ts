import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_SERVICE_CATALOG_CAT_REQUEST =
  "FETCH_SERVICE_CATALOG_CAT_REQUEST";
export const FETCH_SERVICE_CATALOG_CAT_SUCCESS =
  "FETCH_SERVICE_CATALOG_CAT_SUCCESS";
export const FETCH_SERVICE_CATALOG_CAT_FAILURE =
  "FETCH_SERVICE_CATALOG_CAT_FAILURE";

export const EDIT_SERVICE_CATALOG_REQUEST = "EDIT_SERVICE_CATALOG_REQUEST";
export const EDIT_SERVICE_CATALOG_SUCCESS = "EDIT_SERVICE_CATALOG_SUCCESS";
export const EDIT_SERVICE_CATALOG_FAILURE = "EDIT_SERVICE_CATALOG_FAILURE";

export const EDIT_SERVICE_TYPE_REQUEST = "EDIT_SERVICE_TYPE_REQUEST";
export const EDIT_SERVICE_TYPE_SUCCESS = "EDIT_SERVICE_TYPE_SUCCESS";
export const EDIT_SERVICE_TYPE_FAILURE = "EDIT_SERVICE_TYPE_FAILURE";

export const FETCH_SERVICE_TYPE_REQUEST = "FETCH_SERVICE_TYPE_REQUEST";
export const FETCH_SERVICE_TYPE_SUCCESS = "FETCH_SERVICE_TYPE_SUCCESS";
export const FETCH_SERVICE_TYPE_FAILURE = "FETCH_SERVICE_TYPE_FAILURE";

export const EDIT_DOC_SETTING_REQUEST = "EDIT_DOC_SETTING_REQUEST";
export const EDIT_DOC_SETTING_SUCCESS = "EDIT_DOC_SETTING_SUCCESS";
export const EDIT_DOC_SETTING_FAILURE = "EDIT_DOC_SETTING_FAILURE";

export const FETCH_DOC_SETTING_REQUEST = "FETCH_DOC_SETTING_REQUEST";
export const FETCH_DOC_SETTING_SUCCESS = "FETCH_DOC_SETTING_SUCCESS";
export const FETCH_DOC_SETTING_FAILURE = "FETCH_DOC_SETTING_FAILURE";

export const UPDATE_TEMPLATE_TERMS_REQUEST = "UPDATE_TEMPLATE_TERMS_REQUEST";
export const UPDATE_TEMPLATE_TERMS_SUCCESS = "UPDATE_TEMPLATE_TERMS_SUCCESS";
export const UPDATE_TEMPLATE_TERMS_FAILURE = "UPDATE_TEMPLATE_TERMS_FAILURE";

export const GET_SMART_NET_ATTACH_REQUEST = "GET_SMART_NET_ATTACH_REQUEST";
export const GET_SMART_NET_ATTACH_SUCCESS = "GET_SMART_NET_ATTACH_SUCCESS";
export const GET_SMART_NET_ATTACH_FAILURE = "GET_SMART_NET_ATTACH_FAILURE";

export const GET_SMART_NET_ATTACH_LIST_REQUEST =
  "GET_SMART_NET_ATTACH_LIST_REQUEST";
export const GET_SMART_NET_ATTACH_LIST_SUCCESS =
  "GET_SMART_NET_ATTACH_LIST_SUCCESS";
export const GET_SMART_NET_ATTACH_LIST_FAILURE =
  "GET_SMART_NET_ATTACH_LIST_FAILURE";

export const GET_SMART_NET_SETTING_REQUEST = "GET_SMART_NET_SETTING_REQUEST";
export const GET_SMART_NET_SETTING_SUCCESS = "GET_SMART_NET_SETTING_SUCCESS";
export const GET_SMART_NET_SETTING_FAILURE = "GET_SMART_NET_SETTING_FAILURE";

export const FIRE_REPORT_SETTING_REQUEST = "FIRE_REPORT_SETTING_REQUEST";
export const FIRE_REPORT_SETTING_SUCCESS = "FIRE_REPORT_SETTING_SUCCESS";
export const FIRE_REPORT_SETTING_FAILURE = "FIRE_REPORT_SETTING_FAILURE";

export const FIRE_REPORT_SITES_REQUEST = "FIRE_REPORT_SITES_REQUEST";
export const FIRE_REPORT_SITES_SUCCESS = "FIRE_REPORT_SITES_SUCCESS";
export const FIRE_REPORT_SITES_FAILURE = "FIRE_REPORT_SITES_FAILURE";

export const FIRE_REPORT_SITES_SYNC_REQUEST = "FIRE_REPORT_SITES_SYNC_REQUEST";
export const FIRE_REPORT_SITES_SYNC_SUCCESS = "FIRE_REPORT_SITES_SYNC_SUCCESS";
export const FIRE_REPORT_SITES_SYNC_FAILURE = "FIRE_REPORT_SITES_SYNC_FAILURE";

export const GET_P_O_SETTING_REQUEST = "GET_P_O_SETTING_REQUEST";
export const GET_P_O_SETTING_SUCCESS = "GET_P_O_SETTING_SUCCESS";
export const GET_P_O_SETTING_FAILURE = "GET_P_O_SETTING_FAILURE";

export const TEST_EMAIL_SNT_REQUEST = "TEST_EMAIL_SNT_REQUEST";
export const TEST_EMAIL_SNT_SUCCESS = "TEST_EMAIL_SNT_SUCCESS";
export const TEST_EMAIL_SNT_FAILURE = "TEST_EMAIL_SNT_FAILURE";

export const GET_SHIPMENTS_REQUEST = "GET_SHIPMENTS_REQUEST";
export const GET_SHIPMENTS_SUCCESS = "GET_SHIPMENTS_SUCCESS";
export const GET_SHIPMENTS_FAILURE = "GET_SHIPMENTS_FAILURE";

export const FETCH_AGREEMENT_MARGIN_REQUEST = "FETCH_AGREEMENT_MARGIN_REQUEST";
export const FETCH_AGREEMENT_MARGIN_SUCCESS = "FETCH_AGREEMENT_MARGIN_SUCCESS";
export const FETCH_AGREEMENT_MARGIN_FAILURE = "FETCH_AGREEMENT_MARGIN_FAILURE";

export const GET_PO_STATUS_REQUEST = "GET_PO_STATUS_REQUEST";
export const GET_PO_STATUS_SUCCESS = "GET_PO_STATUS_SUCCESS";
export const GET_PO_STATUS_FAILURE = "GET_PO_STATUS_FAILURE";

export const TEST_INGRAM_CRED_REQUEST = "TEST_INGRAM_CRED_REQUEST";
export const TEST_INGRAM_CRED_SUCCESS = "TEST_INGRAM_CRED_SUCCESS";
export const TEST_INGRAM_CRED_FAILURE = "TEST_INGRAM_CRED_FAILURE";

export const CLIENT_360_SETTINGS_CRU_REQUEST =
  "CLIENT_360_SETTINGS_CRU_REQUEST";
export const CLIENT_360_SETTINGS_CRU_SUCCESS =
  "CLIENT_360_SETTINGS_CRU_SUCCESS";
export const CLIENT_360_SETTINGS_CRU_FAILURE =
  "CLIENT_360_SETTINGS_CRU_FAILURE";

export const GET_INGRAM_CRED_REQUEST = "GET_INGRAM_CRED_REQUEST";
export const GET_INGRAM_CRED_SUCCESS = "GET_INGRAM_CRED_SUCCESS";
export const GET_INGRAM_CRED_FAILURE = "GET_INGRAM_CRED_FAILURE";

export const TEST_PAX8_CRED_REQUEST = "TEST_PAX8_CRED_REQUEST";
export const TEST_PAX8_CRED_SUCCESS = "TEST_PAX8_CRED_SUCCESS";
export const TEST_PAX8_CRED_FAILURE = "TEST_PAX8_CRED_FAILURE";

export const TEST_GRAPH_CRED_REQUEST = "TEST_GRAPH_CRED_REQUEST";
export const TEST_GRAPH_CRED_SUCCESS = "TEST_GRAPH_CRED_SUCCESS";
export const TEST_GRAPH_CRED_FAILURE = "TEST_GRAPH_CRED_FAILURE";

export const GRAPH_CRED_REQUEST = "GRAPH_CRED_REQUEST";
export const GRAPH_CRED_SUCCESS = "GRAPH_CRED_SUCCESS";
export const GRAPH_CRED_FAILURE = "GRAPH_CRED_FAILURE";

export const GET_PAX8_CRED_REQUEST = "GET_PAX8_CRED_REQUEST";
export const GET_PAX8_CRED_SUCCESS = "GET_PAX8_CRED_SUCCESS";
export const GET_PAX8_CRED_FAILURE = "GET_PAX8_CRED_FAILURE";

export const GET_VENDOR_LIST_REQUEST = "GET_VENDOR_LIST_REQUEST";
export const GET_VENDOR_LIST_SUCCESS = "GET_VENDOR_LIST_SUCCESS";
export const GET_VENDOR_LIST_FAILURE = "GET_VENDOR_LIST_FAILURE";

export const GET_CUSTOMER_TYPES_REQUEST = "GET_CUSTOMER_TYPES_REQUEST";
export const GET_CUSTOMER_TYPES_SUCCESS = "GET_CUSTOMER_TYPES_SUCCESS";
export const GET_CUSTOMER_TYPES_FAILURE = "GET_CUSTOMER_TYPES_FAILURE";

export const GET_CUSTOMER_STATUSES_REQUEST = "GET_CUSTOMER_STATUSES_REQUEST";
export const GET_CUSTOMER_STATUSES_SUCCESS = "GET_CUSTOMER_STATUSES_SUCCESS";
export const GET_CUSTOMER_STATUSES_FAILURE = "GET_CUSTOMER_STATUSES_FAILURE";

export const GET_CW_TEAM_ROLES_REQUEST = "GET_CW_TEAM_ROLES_REQUEST";
export const GET_CW_TEAM_ROLES_SUCCESS = "GET_CW_TEAM_ROLES_SUCCESS";
export const GET_CW_TEAM_ROLES_FAILURE = "GET_CW_TEAM_ROLES_FAILURE";

export const CREATE_CW_TEAM_ROLE_REQUEST = "CREATE_CW_TEAM_ROLE_REQUEST";
export const CREATE_CW_TEAM_ROLE_SUCCESS = "CREATE_CW_TEAM_ROLE_SUCCESS";
export const CREATE_CW_TEAM_ROLE_FAILURE = "CREATE_CW_TEAM_ROLE_FAILURE";

export const VENDOR_ATTACHMENTS_REQUEST = "VENDOR_ATTACHMENTS_REQUEST";
export const VENDOR_ATTACHMENTS_SUCCESS = "VENDOR_ATTACHMENTS_SUCCESS";
export const VENDOR_ATTACHMENTS_FAILURE = "VENDOR_ATTACHMENTS_FAILURE";

export const SOW_ROLE_SETTING_REQUEST = "SOW_ROLE_SETTING_REQUEST";
export const SOW_ROLE_SETTING_SUCCESS = "SOW_ROLE_SETTING_SUCCESS";
export const SOW_ROLE_SETTING_FAILURE = "SOW_ROLE_SETTING_FAILURE";

export const FETCH_PUBLIC_KEY_REQUEST = "FETCH_PUBLIC_KEY_REQUEST";
export const FETCH_PUBLIC_KEY_SUCCESS = "FETCH_PUBLIC_KEY_SUCCESS";
export const FETCH_PUBLIC_KEY_FAILURE = "FETCH_PUBLIC_KEY_FAILURE";

export const VENDOR_ALIAS_MAPPING_REQUEST = "VENDOR_ALIAS_MAPPING_REQUEST";
export const VENDOR_ALIAS_MAPPING_SUCCESS = "VENDOR_ALIAS_MAPPING_SUCCESS";
export const VENDOR_ALIAS_MAPPING_FAILURE = "VENDOR_ALIAS_MAPPING_FAILURE";

export const FETCH_ORDER_TRACKING_SETTING_REQUEST =
  "FETCH_ORDER_TRACKING_SETTING_REQUEST";
export const FETCH_ORDER_TRACKING_SETTING_SUCCESS =
  "FETCH_ORDER_TRACKING_SETTING_SUCCESS";
export const FETCH_ORDER_TRACKING_SETTING_FAILURE =
  "FETCH_ORDER_TRACKING_SETTING_FAILURE";

export const FETCH_COMM_TYPES_REQUEST = "FETCH_COMM_TYPES_REQUEST";
export const FETCH_COMM_TYPES_SUCCESS = "FETCH_COMM_TYPES_SUCCESS";
export const FETCH_COMM_TYPES_FAILURE = "FETCH_COMM_TYPES_FAILURE";

export const SAVE_COMM_REQUEST = "SAVE_COMM_REQUEST";
export const SAVE_COMM_SUCCESS = "SAVE_COMM_SUCCESS";
export const SAVE_COMM_FAILURE = "SAVE_COMM_FAILURE";

export const GET_RECIEVING_STATUS_REQUEST = "GET_RECIEVING_STATUS_REQUEST";
export const GET_RECIEVING_STATUS_SUCCESS = "GET_RECIEVING_STATUS_SUCCESS";
export const GET_RECIEVING_STATUS_FAILURE = "GET_RECIEVING_STATUS_FAILURE";

export const GET_MS_AGENT_ACC_REQUEST = "GET_MS_AGENT_ACC_REQUEST";
export const GET_MS_AGENT_ACC_SUCCESS = "GET_MS_AGENT_ACC_SUCCESS";
export const GET_MS_AGENT_ACC_FAILURE = "GET_MS_AGENT_ACC_FAILURE";

export const ORDER_TRACKING_DASHBOARD_SETTING_REQUEST =
  "ORDER_TRACKING_DASHBOARD_SETTING_REQUEST";
export const ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS =
  "ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS";
export const ORDER_TRACKING_DASHBOARD_SETTING_FAILURE =
  "ORDER_TRACKING_DASHBOARD_SETTING_FAILURE";

export const GET_PO_SETTING_REQUEST = "GET_PO_SETTING_REQUEST";
export const GET_PO_SETTING_SUCCESS = "GET_PO_SETTING_SUCCESS";
export const GET_PO_SETTING_FAILURE = "GET_PO_SETTING_FAILURE";

export const ROLE_TYPE_REQUEST = "ROLE_TYPE_REQUEST";
export const ROLE_TYPE_SUCCESS = "ROLE_TYPE_SUCCESS";
export const ROLE_TYPE_FAILURE = "ROLE_TYPE_FAILURE";

export const GET_QUOTE_STAGES_REQUEST = "GET_QUOTE_STAGES_REQUEST";
export const GET_QUOTE_STAGES_SUCCESS = "GET_QUOTE_STAGES_SUCCESS";
export const GET_QUOTE_STAGES_FAILURE = "GET_QUOTE_STAGES_FAILURE";

export const GET_BILLING_STATUSES_REQUEST = "GET_BILLING_STATUSES_REQUEST";
export const GET_BILLING_STATUSES_SUCCESS = "GET_BILLING_STATUSES_SUCCESS";
export const GET_BILLING_STATUSES_FAILURE = "GET_BILLING_STATUSES_FAILURE";

export const GET_COLLECTIONS_SETTINGS_REQUEST =
  "GET_COLLECTIONS_SETTINGS_REQUEST";
export const GET_COLLECTIONS_SETTINGS_SUCCESS =
  "GET_COLLECTIONS_SETTINGS_SUCCESS";
export const GET_COLLECTIONS_SETTINGS_FAILURE =
  "GET_COLLECTIONS_SETTINGS_FAILURE";

export const GET_VENDOR_SETTINGS_REQUEST = "GET_VENDOR_SETTINGS_REQUEST";
export const GET_VENDOR_SETTINGS_SUCCESS = "GET_VENDOR_SETTINGS_SUCCESS";
export const GET_VENDOR_SETTINGS_FAILURE = "GET_VENDOR_SETTINGS_FAILURE";

export const SEND_COLLECTIONS_NOTICE_REQUEST =
  "SEND_COLLECTIONS_NOTICE_REQUEST";
export const SEND_COLLECTIONS_NOTICE_SUCCESS =
  "SEND_COLLECTIONS_NOTICE_SUCCESS";
export const SEND_COLLECTIONS_NOTICE_FAILURE =
  "SEND_COLLECTIONS_NOTICE_FAILURE";

export const GET_COLLECTIONS_PREVIEW_REQUEST =
  "GET_COLLECTIONS_PREVIEW_REQUEST";
export const GET_COLLECTIONS_PREVIEW_SUCCESS =
  "GET_COLLECTIONS_PREVIEW_SUCCESS";
export const GET_COLLECTIONS_PREVIEW_FAILURE =
  "GET_COLLECTIONS_PREVIEW_FAILURE";

export const SUBSCRIPTIONS_NOTICE_REQUEST = "SUBSCRIPTIONS_NOTICE_REQUEST";
export const SUBSCRIPTIONS_NOTICE_SUCCESS = "SUBSCRIPTIONS_NOTICE_SUCCESS";
export const SUBSCRIPTIONS_NOTICE_FAILURE = "SUBSCRIPTIONS_NOTICE_FAILURE";

export const WEEKLY_TRACKING_REQUEST = "WEEKLY_TRACKING_REQUEST";
export const WEEKLY_TRACKING_SUCCESS = "WEEKLY_TRACKING_SUCCESS";
export const WEEKLY_TRACKING_FAILURE = "WEEKLY_TRACKING_FAILURE";

export const CONFIG_MAPPING_REQUEST = "CONFIG_MAPPING_REQUEST";
export const CONFIG_MAPPING_SUCCESS = "CONFIG_MAPPING_SUCCESS";
export const CONFIG_MAPPING_FAILURE = "CONFIG_MAPPING_FAILURE";

export const SEND_WEEKLY_TRACKING_REQUEST = "SEND_WEEKLY_TRACKING_REQUEST";
export const SEND_WEEKLY_TRACKING_SUCCESS = "SEND_WEEKLY_TRACKING_SUCCESS";
export const SEND_WEEKLY_TRACKING_FAILURE = "SEND_WEEKLY_TRACKING_FAILURE";

export const GET_WEEKLY_TRACKING_PREVIEW_REQUEST =
  "GET_WEEKLY_TRACKING_PREVIEW_REQUEST";
export const GET_WEEKLY_TRACKING_PREVIEW_SUCCESS =
  "GET_WEEKLY_TRACKING_PREVIEW_SUCCESS";
export const GET_WEEKLY_TRACKING_PREVIEW_FAILURE =
  "GET_WEEKLY_TRACKING_PREVIEW_FAILURE";

export const SEND_SUBSCRIPTIONS_NOTICE_REQUEST =
  "SEND_SUBSCRIPTIONS_NOTICE_REQUEST";
export const SEND_SUBSCRIPTIONS_NOTICE_SUCCESS =
  "SEND_SUBSCRIPTIONS_NOTICE_SUCCESS";
export const SEND_SUBSCRIPTIONS_NOTICE_FAILURE =
  "SEND_SUBSCRIPTIONS_NOTICE_FAILURE";

export const GET_SUBSCRIPTIONS_PREVIEW_REQUEST =
  "GET_SUBSCRIPTIONS_PREVIEW_REQUEST";
export const GET_SUBSCRIPTIONS_PREVIEW_SUCCESS =
  "GET_SUBSCRIPTIONS_PREVIEW_SUCCESS";
export const GET_SUBSCRIPTIONS_PREVIEW_FAILURE =
  "GET_SUBSCRIPTIONS_PREVIEW_FAILURE";

export const fetchServiceCatalogCategories = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/service-categories",
    method: "get",
    authenticated: true,
    types: [
      FETCH_SERVICE_CATALOG_CAT_REQUEST,
      FETCH_SERVICE_CATALOG_CAT_SUCCESS,
      FETCH_SERVICE_CATALOG_CAT_FAILURE,
    ],
  },
});

export const editServiceCatalog = (data) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/service-categories",
    method: "post",
    body: data,
    authenticated: true,
    types: [
      EDIT_SERVICE_CATALOG_REQUEST,
      EDIT_SERVICE_CATALOG_SUCCESS,
      EDIT_SERVICE_CATALOG_FAILURE,
    ],
  },
});

export const fetchServiceTypeSTT = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/document/category",
    method: "get",
    authenticated: true,
    types: [
      FETCH_SERVICE_TYPE_REQUEST,
      FETCH_SERVICE_TYPE_SUCCESS,
      FETCH_SERVICE_TYPE_FAILURE,
    ],
  },
});

export const agreementMarginSettingsCRUD = (
  data?: { id?: number; margin_limit: number },
  method: string = "get"
) => ({
  [CALL_API]: {
    endpoint:
      "/api/v1/providers/sales/agreement-margin-settings" +
      (method === "put" ? `/${data.id}` : "?pagination=false"),
    method: method,
    body: data,
    authenticated: true,
    types: [
      FETCH_AGREEMENT_MARGIN_REQUEST,
      FETCH_AGREEMENT_MARGIN_SUCCESS,
      FETCH_AGREEMENT_MARGIN_FAILURE,
    ],
  },
});

export const editServiceTypeSTT = (data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/category/${data.id}`,
    method: "put",
    body: data,
    authenticated: true,
    types: [
      EDIT_SERVICE_TYPE_REQUEST,
      EDIT_SERVICE_TYPE_SUCCESS,
      EDIT_SERVICE_TYPE_FAILURE,
    ],
  },
});

export const fetchSOWDOCSetting = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sow-doc-settings`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_DOC_SETTING_REQUEST,
      FETCH_DOC_SETTING_SUCCESS,
      FETCH_DOC_SETTING_FAILURE,
    ],
  },
});

export const updateSowTemplateTerms = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sow-doc-settings/template-terms/update`,
    method: "put",
    authenticated: true,
    types: [
      UPDATE_TEMPLATE_TERMS_REQUEST,
      UPDATE_TEMPLATE_TERMS_SUCCESS,
      UPDATE_TEMPLATE_TERMS_FAILURE,
    ],
  },
});

export const editSOWDOCSetting = (data: IDOCSetting) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sow-doc-settings`,
    method: "put",
    body: data,
    authenticated: true,
    types: [
      EDIT_DOC_SETTING_REQUEST,
      EDIT_DOC_SETTING_SUCCESS,
      EDIT_DOC_SETTING_FAILURE,
    ],
  },
});

export const getSmartNetSettings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings`,
    method: "get",
    authenticated: true,
    types: [
      GET_SMART_NET_SETTING_REQUEST,
      GET_SMART_NET_SETTING_SUCCESS,
      GET_SMART_NET_SETTING_FAILURE,
    ],
  },
});

export const getShipmentMethods = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/shipmentmethods`,
    method: "get",
    authenticated: true,
    types: [
      GET_SHIPMENTS_REQUEST,
      GET_SHIPMENTS_SUCCESS,
      GET_SHIPMENTS_FAILURE,
    ],
  },
});

export const getRecievingStatuses = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/partial-receive-status`,
    method: "get",
    authenticated: true,
    types: [
      GET_RECIEVING_STATUS_REQUEST,
      GET_RECIEVING_STATUS_SUCCESS,
      GET_RECIEVING_STATUS_FAILURE,
    ],
  },
});

export const getPurchaseOrderStatusList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/purchase-order-statuses`,
    method: "get",
    authenticated: true,
    types: [
      GET_PO_STATUS_REQUEST,
      GET_PO_STATUS_SUCCESS,
      GET_PO_STATUS_FAILURE,
    ],
  },
});

export const fireReportSettingsCRU = (
  method: HTTPMethods,
  data?: IFIREReportSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/fire-report/setting`,
    method,
    body: data,
    authenticated: true,
    types: [
      FIRE_REPORT_SETTING_REQUEST,
      FIRE_REPORT_SETTING_SUCCESS,
      FIRE_REPORT_SETTING_FAILURE,
    ],
  },
});

export const fireReportPartnerSitesCRUD = (
  method: HTTPMethods,
  site?: ISite
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/fire-report/partner-sites${
      method === "get" || method === "post"
        ? "?pagination=false"
        : "/" + site.id
    }`,
    method,
    body:
      method === "put" || method === "post"
        ? site
        : undefined,
    authenticated: true,
    types: [
      FIRE_REPORT_SITES_REQUEST,
      FIRE_REPORT_SITES_SUCCESS,
      FIRE_REPORT_SITES_FAILURE,
    ],
  },
});

export const fireReportPartnerSitesSync = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/fire-report/partner-sites/sync`,
    method: "post",
    authenticated: true,
    types: [
      FIRE_REPORT_SITES_SYNC_REQUEST,
      FIRE_REPORT_SITES_SYNC_SUCCESS,
      FIRE_REPORT_SITES_SYNC_FAILURE,
    ],
  },
});

export const editSmartNetSettings = (settings: ISmartNetSetting) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings`,
    method: "put",
    body: settings,
    authenticated: true,
    types: [
      GET_SMART_NET_SETTING_REQUEST,
      GET_SMART_NET_SETTING_SUCCESS,
      GET_SMART_NET_SETTING_FAILURE,
    ],
  },
});

export const sendTestEmailSmartNet = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/test-snt-notification-email`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      TEST_EMAIL_SNT_REQUEST,
      TEST_EMAIL_SNT_SUCCESS,
      TEST_EMAIL_SNT_FAILURE,
    ],
  },
});

export const getSmartNetSettingsAttachments = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings/attachments?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_SMART_NET_ATTACH_LIST_REQUEST,
      GET_SMART_NET_ATTACH_LIST_SUCCESS,
      GET_SMART_NET_ATTACH_LIST_FAILURE,
    ],
  },
});

export const deleteSmartNetSettingsAttachments = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings/attachments/${id}`,
    method: "delete",
    authenticated: true,
    types: [
      GET_SMART_NET_ATTACH_REQUEST,
      GET_SMART_NET_ATTACH_SUCCESS,
      GET_SMART_NET_ATTACH_FAILURE,
    ],
  },
});

export const addSmartNetSettingsAttachments = (attachments) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings/attachments`,
    method: "post",
    body: attachments,
    authenticated: true,
    types: [
      GET_SMART_NET_ATTACH_REQUEST,
      GET_SMART_NET_ATTACH_SUCCESS,
      GET_SMART_NET_ATTACH_FAILURE,
    ],
  },
});

export const testIngramAPICredentials = (data: IIngramCredentials) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/ingram-api/validate`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      TEST_INGRAM_CRED_REQUEST,
      TEST_INGRAM_CRED_SUCCESS,
      TEST_INGRAM_CRED_FAILURE,
    ],
  },
});

export const getIngramAPICredentials = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/ingram-api/settings`,
    method: "get",
    authenticated: true,
    types: [
      GET_INGRAM_CRED_REQUEST,
      GET_INGRAM_CRED_SUCCESS,
      GET_INGRAM_CRED_FAILURE,
    ],
  },
});
export const saveIngramAPICredentials = (data: IIngramCredentials) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/ingram-api/settings`,
    method: "patch",
    body: data,
    authenticated: true,
    types: [
      TEST_INGRAM_CRED_REQUEST,
      TEST_INGRAM_CRED_SUCCESS,
      TEST_INGRAM_CRED_FAILURE,
    ],
  },
});

export const testPAX8APICredentials = (data: IPAX8Credentials) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pax8-settings/validate`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      TEST_PAX8_CRED_REQUEST,
      TEST_PAX8_CRED_SUCCESS,
      TEST_PAX8_CRED_FAILURE,
    ],
  },
});

export const getPAX8APICredentials = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pax8-settings`,
    method: "get",
    authenticated: true,
    types: [
      GET_PAX8_CRED_REQUEST,
      GET_PAX8_CRED_SUCCESS,
      GET_PAX8_CRED_FAILURE,
    ],
  },
});
export const savePAX8APICredentials = (
  data: IPAX8Credentials,
  method: "put" | "post"
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pax8-settings`,
    method,
    body: data,
    authenticated: true,
    types: [
      TEST_PAX8_CRED_REQUEST,
      TEST_PAX8_CRED_SUCCESS,
      TEST_PAX8_CRED_FAILURE,
    ],
  },
});

export const testGraphAPICredentials = (data: IGraphCredentials) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/graph/setting/validate`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      TEST_GRAPH_CRED_REQUEST,
      TEST_GRAPH_CRED_SUCCESS,
      TEST_GRAPH_CRED_FAILURE,
    ],
  },
});

export const graphAPICredCRU = (
  method: HTTPMethods,
  data?: IGraphCredentials
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/graph/setting`,
    method,
    body: data,
    authenticated: true,
    types: [GRAPH_CRED_REQUEST, GRAPH_CRED_SUCCESS, GRAPH_CRED_FAILURE],
  },
});

export const getPurchaseOrderSettings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/email-template/settings`,
    method: "get",
    authenticated: true,
    types: [
      GET_P_O_SETTING_REQUEST,
      GET_P_O_SETTING_SUCCESS,
      GET_P_O_SETTING_FAILURE,
    ],
  },
});

export const editPurchaseOrderSettings = (settings: ISmartNetSetting) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/email-template/settings`,
    method: "patch",
    body: { ...settings },
    authenticated: true,
    types: [
      GET_P_O_SETTING_REQUEST,
      GET_P_O_SETTING_SUCCESS,
      GET_P_O_SETTING_FAILURE,
    ],
  },
});

export const fetchOrderTrackingSettings = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking-settings`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_ORDER_TRACKING_SETTING_REQUEST,
      FETCH_ORDER_TRACKING_SETTING_SUCCESS,
      FETCH_ORDER_TRACKING_SETTING_FAILURE,
    ],
  },
});

export const fetCommunicationTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/phone-communication-types`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_COMM_TYPES_REQUEST,
      FETCH_COMM_TYPES_SUCCESS,
      FETCH_COMM_TYPES_FAILURE,
    ],
  },
});
export const saveCommunication = (data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/communication-types/settings`,
    method: "put",
    body: data,
    authenticated: true,
    types: [SAVE_COMM_REQUEST, SAVE_COMM_SUCCESS, SAVE_COMM_FAILURE],
  },
});

export const getCommunicationData = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/communication-types/settings`,
    method: "get",
    authenticated: true,
    types: [SAVE_COMM_REQUEST, SAVE_COMM_SUCCESS, SAVE_COMM_FAILURE],
  },
});

export const getMSAgentAssociation = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings/ms-agent-provider-association`,
    method: "get",
    authenticated: true,
    types: [
      GET_MS_AGENT_ACC_REQUEST,
      GET_MS_AGENT_ACC_SUCCESS,
      GET_MS_AGENT_ACC_FAILURE,
    ],
  },
});

export const postMSAgentAssociation = (data, method) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/workflows/smartnet-receiving/settings/ms-agent-provider-association`,
    method,
    body: data,
    authenticated: true,
    types: [
      GET_MS_AGENT_ACC_REQUEST,
      GET_MS_AGENT_ACC_SUCCESS,
      GET_MS_AGENT_ACC_FAILURE,
    ],
  },
});

export const fetchOrderTrackingDashboardSettings = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/operations/dashboard",
    method: "get",
    authenticated: true,
    types: [
      ORDER_TRACKING_DASHBOARD_SETTING_REQUEST,
      ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS,
      ORDER_TRACKING_DASHBOARD_SETTING_FAILURE,
    ],
  },
});

export const postOrderTrackingDashboardSettings = (
  data: IOrderTrackingDashboardSetting
) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/operations/dashboard",
    method: "post",
    body: data,
    authenticated: true,
    types: [
      ORDER_TRACKING_DASHBOARD_SETTING_REQUEST,
      ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS,
      ORDER_TRACKING_DASHBOARD_SETTING_FAILURE,
    ],
  },
});

export const updateOrderTrackingDashboardSettings = (
  data: IOrderTrackingDashboardSetting
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/dashboard`,
    method: "patch",
    body: data,
    authenticated: true,
    types: [
      ORDER_TRACKING_DASHBOARD_SETTING_REQUEST,
      ORDER_TRACKING_DASHBOARD_SETTING_SUCCESS,
      ORDER_TRACKING_DASHBOARD_SETTING_FAILURE,
    ],
  },
});

export const poStatusSetting = (method, custId, data?: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/operations/${custId}/pos-status-settings`,
    method: method,
    body: data,
    authenticated: true,
    types: [
      GET_PO_SETTING_REQUEST,
      GET_PO_SETTING_SUCCESS,
      GET_PO_SETTING_FAILURE,
    ],
  },
});

export const createRoleType = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-role-types`,
    method: "post",
    body: data,
    authenticated: true,
    types: [ROLE_TYPE_REQUEST, ROLE_TYPE_SUCCESS, ROLE_TYPE_FAILURE],
  },
});

export const getRoleTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-role-types`,
    method: "get",
    authenticated: true,
    types: [ROLE_TYPE_REQUEST, ROLE_TYPE_SUCCESS, ROLE_TYPE_FAILURE],
  },
});

export const deleteRoleType = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/pmo/settings/project-role-types`,
    method: "delete",
    body: { deleted: [data.id] },
    authenticated: true,
    types: [ROLE_TYPE_REQUEST, ROLE_TYPE_SUCCESS, ROLE_TYPE_FAILURE],
  },
});

export const getQuoteAllStages = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/quotes/all-stages`,
    method: "get",
    authenticated: true,
    types: [
      GET_QUOTE_STAGES_REQUEST,
      GET_QUOTE_STAGES_SUCCESS,
      GET_QUOTE_STAGES_FAILURE,
    ],
  },
});

export const getBillingStatusesOptions = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/collections/billing-statuses`,
    method: "get",
    authenticated: true,
    types: [
      GET_BILLING_STATUSES_REQUEST,
      GET_BILLING_STATUSES_SUCCESS,
      GET_BILLING_STATUSES_FAILURE,
    ],
  },
});

export const collectionsSettingsCRUD = (
  request: "get" | "post" | "put",
  data?: ICollectionsSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/collections/automated-notices-setting`,
    method: request,
    authenticated: true,
    body: data,
    types: [
      GET_COLLECTIONS_SETTINGS_REQUEST,
      GET_COLLECTIONS_SETTINGS_SUCCESS,
      GET_COLLECTIONS_SETTINGS_FAILURE,
    ],
  },
});

export const vendorSettingsCRU = (
  request: "get" | "post" | "put",
  data?: IVendorSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/sow/vendor-onboarding-setting`,
    method: request,
    authenticated: true,
    body: data,
    types: [
      GET_VENDOR_SETTINGS_REQUEST,
      GET_VENDOR_SETTINGS_SUCCESS,
      GET_VENDOR_SETTINGS_FAILURE,
    ],
  },
});

export const sendCollectionsNoticeEmail = (
  crm_ids: number[],
  all_customers: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/collections/send-collection-notices`,
    method: "post",
    authenticated: true,
    body: { customer_crm_ids: crm_ids, all_customers },
    types: [
      SEND_COLLECTIONS_NOTICE_REQUEST,
      SEND_COLLECTIONS_NOTICE_SUCCESS,
      SEND_COLLECTIONS_NOTICE_FAILURE,
    ],
  },
});

export const getCollectionsEmailPreview = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/collections/notice-email-preview`,
    method: "get",
    authenticated: true,
    types: [
      GET_COLLECTIONS_PREVIEW_REQUEST,
      GET_COLLECTIONS_PREVIEW_SUCCESS,
      GET_COLLECTIONS_PREVIEW_FAILURE,
    ],
  },
});

export const subscriptionNoticesCRUD = (
  request: HTTPMethods,
  data?: ISubscriptionNotificationSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-notices/setting`,
    method: request,
    authenticated: true,
    body: data,
    types: [
      SUBSCRIPTIONS_NOTICE_REQUEST,
      SUBSCRIPTIONS_NOTICE_SUCCESS,
      SUBSCRIPTIONS_NOTICE_FAILURE,
    ],
  },
});

export const weeklyTrackingCRU = (
  request: HTTPMethods,
  data?: IWeeklyOrderTrackingSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/weekly-recap/setting`,
    method: request,
    authenticated: true,
    body: data,
    types: [
      WEEKLY_TRACKING_REQUEST,
      WEEKLY_TRACKING_SUCCESS,
      WEEKLY_TRACKING_FAILURE,
    ],
  },
});

export const configMappingCRUD = (
  request: HTTPMethods,
  data?: IConfigMapping
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-notices/config-mapping${
      request === "put" || request === "delete"
        ? "/" + data.id
        : "?pagination=false"
    }`,
    method: request,
    authenticated: true,
    body: data,
    types: [
      CONFIG_MAPPING_REQUEST,
      CONFIG_MAPPING_SUCCESS,
      CONFIG_MAPPING_FAILURE,
    ],
  },
});

export const sendSubscriptionsNoticeEmail = (
  crm_ids: number[],
  all_customers: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-notices/send`,
    method: "post",
    authenticated: true,
    body: { customer_crm_ids: crm_ids, all_customers },
    types: [
      SEND_SUBSCRIPTIONS_NOTICE_REQUEST,
      SEND_SUBSCRIPTIONS_NOTICE_SUCCESS,
      SEND_SUBSCRIPTIONS_NOTICE_FAILURE,
    ],
  },
});

export const getSubscriptionsEmailPreview = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/subscription-notices/email-preview`,
    authenticated: true,
    types: [
      GET_SUBSCRIPTIONS_PREVIEW_REQUEST,
      GET_SUBSCRIPTIONS_PREVIEW_SUCCESS,
      GET_SUBSCRIPTIONS_PREVIEW_FAILURE,
    ],
  },
});

export const sendWeeklyTrackingEmail = (
  crm_ids: number[],
  all_customers: boolean
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/weekly-recap/email/trigger`,
    method: "post",
    authenticated: true,
    body: { customer_crm_ids: crm_ids, all_customers },
    types: [
      SEND_WEEKLY_TRACKING_REQUEST,
      SEND_WEEKLY_TRACKING_SUCCESS,
      SEND_WEEKLY_TRACKING_FAILURE,
    ],
  },
});

export const getWeeklyTrackingEmailPreview = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/weekly-recap/email/preview`,
    authenticated: true,
    types: [
      GET_WEEKLY_TRACKING_PREVIEW_REQUEST,
      GET_WEEKLY_TRACKING_PREVIEW_SUCCESS,
      GET_WEEKLY_TRACKING_PREVIEW_FAILURE,
    ],
  },
});

export const fetchVendorList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement/pax8/vendors?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_VENDOR_LIST_REQUEST,
      GET_VENDOR_LIST_SUCCESS,
      GET_VENDOR_LIST_FAILURE,
    ],
  },
});

export const fetchCustomerTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/types`,
    method: "get",
    authenticated: true,
    types: [
      GET_CUSTOMER_TYPES_REQUEST,
      GET_CUSTOMER_TYPES_SUCCESS,
      GET_CUSTOMER_TYPES_FAILURE,
    ],
  },
});

export const fetchCustomerStatuses = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/statuses`,
    method: "get",
    authenticated: true,
    types: [
      GET_CUSTOMER_STATUSES_REQUEST,
      GET_CUSTOMER_STATUSES_SUCCESS,
      GET_CUSTOMER_STATUSES_FAILURE,
    ],
  },
});

export const vendorAttachmentsEdit = (
  request: "get" | "post" | "delete",
  data?: FormData,
  id?: number
) => ({
  [CALL_API]: {
    endpoint:
      `/api/v1/providers/document/sow/vendor-onboarding-setting/attachments` +
      (request === "get"
        ? "?pagination=false"
        : request === "delete"
        ? `/${id}`
        : ""),
    method: request,
    body: data,
    authenticated: true,
    types: [
      VENDOR_ATTACHMENTS_REQUEST,
      VENDOR_ATTACHMENTS_SUCCESS,
      VENDOR_ATTACHMENTS_FAILURE,
    ],
  },
});

export const fetchCWTeamRoleOptions = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/team-roles`,
    method: "get",
    authenticated: true,
    types: [
      GET_CW_TEAM_ROLES_REQUEST,
      GET_CW_TEAM_ROLES_SUCCESS,
      GET_CW_TEAM_ROLES_FAILURE,
    ],
  },
});

export const createCWTeamRole = (role_name: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/team-roles`,
    method: "post",
    body: { role_name },
    authenticated: true,
    types: [
      CREATE_CW_TEAM_ROLE_REQUEST,
      CREATE_CW_TEAM_ROLE_SUCCESS,
      CREATE_CW_TEAM_ROLE_FAILURE,
    ],
  },
});

export const sowCreatorSettingCRU = (
  request: "get" | "post" | "put",
  data?: {
    default_sow_creator_role_id: number;
    default_change_request_creator_role_id: number;
  }
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/creator-role-setting`,
    method: request,
    body: data,
    authenticated: true,
    types: [
      SOW_ROLE_SETTING_REQUEST,
      SOW_ROLE_SETTING_SUCCESS,
      SOW_ROLE_SETTING_FAILURE,
    ],
  },
});

export const fetchEncryptionPublicKey = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/integrations/public-key`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_PUBLIC_KEY_REQUEST,
      FETCH_PUBLIC_KEY_SUCCESS,
      FETCH_PUBLIC_KEY_FAILURE,
    ],
  },
});

export const client360SettingCRU = (
  request: HTTPMethods,
  data?: IClient360Settings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/client360/setting`,
    method: request,
    body: data,
    authenticated: true,
    types: [
      CLIENT_360_SETTINGS_CRU_REQUEST,
      CLIENT_360_SETTINGS_CRU_SUCCESS,
      CLIENT_360_SETTINGS_CRU_FAILURE,
    ],
  },
});

export const vendorAliasMappingCRUD = (
  request: HTTPMethods,
  data?: IVendorAliasMapping
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/document/sow/resource-description${
      request === "put" || request === "delete"
        ? "/" + data.id
        : ""
    }`,
    method: request,
    body:
      "put" || request === "post"
        ? { ...data, id: undefined }
        : undefined,
    authenticated: true,
    types: [
      VENDOR_ALIAS_MAPPING_REQUEST,
      VENDOR_ALIAS_MAPPING_SUCCESS,
      VENDOR_ALIAS_MAPPING_FAILURE,
    ],
  },
});
