import { CALL_API } from "../middleware/ApiMiddleware";

export const GET_TEMPLATE_HISTORY_REQUEST = "GET_TEMPLATE_HISTORY_REQUEST";
export const GET_TEMPLATE_HISTORY_SUCCESS = "GET_TEMPLATE_HISTORY_SUCCESS";
export const GET_TEMPLATE_HISTORY_FAILURE = "GET_TEMPLATE_HISTORY_FAILURE";

export const GET_AGREEMENT_HISTORY_REQUEST = "GET_AGREEMENT_HISTORY_REQUEST";
export const GET_AGREEMENT_HISTORY_SUCCESS = "GET_AGREEMENT_HISTORY_SUCCESS";
export const GET_AGREEMENT_HISTORY_FAILURE = "GET_AGREEMENT_HISTORY_FAILURE";

export const FETCH_AGREEMENT_REQUEST = "FETCH_AGREEMENT_REQUEST";
export const FETCH_AGREEMENT_SUCCESS = "FETCH_AGREEMENT_SUCCESS";
export const FETCH_AGREEMENT_FAILURE = "FETCH_AGREEMENT_FAILURE";

export const CREATE_AGREEMENT_REQUEST = "CREATE_AGREEMENT_REQUEST";
export const CREATE_AGREEMENT_SUCCESS = "CREATE_AGREEMENT_SUCCESS";
export const CREATE_AGREEMENT_FAILURE = "CREATE_AGREEMENT_FAILURE";

export const FETCH_TEMPLATES_REQUEST = "FETCH_TEMPLATES_REQUEST";
export const FETCH_TEMPLATES_SUCCESS = "FETCH_TEMPLATES_SUCCESS";
export const FETCH_TEMPLATES_FAILURE = "FETCH_TEMPLATES_FAILURE";

export const FETCH_TEMPLATES_AUTHORS_REQUEST =
  "FETCH_TEMPLATES_AUTHORS_REQUEST";
export const FETCH_TEMPLATES_AUTHORS_SUCCESS =
  "FETCH_TEMPLATES_AUTHORS_SUCCESS";
export const FETCH_TEMPLATES_AUTHORS_FAILURE =
  "FETCH_TEMPLATES_AUTHORS_FAILURE";

export const FETCH_AGREEMENTS_AUTHORS_REQUEST =
  "FETCH_AGREEMENTS_AUTHORS_REQUEST";
export const FETCH_AGREEMENTS_AUTHORS_SUCCESS =
  "FETCH_AGREEMENTS_AUTHORS_SUCCESS";
export const FETCH_AGREEMENTS_AUTHORS_FAILURE =
  "FETCH_AGREEMENTS_AUTHORS_FAILURE";

export const CREATE_TEMPLATE_REQUEST = "CREATE_TEMPLATE_REQUEST";
export const CREATE_TEMPLATE_SUCCESS = "CREATE_TEMPLATE_SUCCESS";
export const CREATE_TEMPLATE_FAILURE = "CREATE_TEMPLATE_FAILURE";

export const DOWNLOAD_AGREEMENT_REQUEST = "DOWNLOAD_AGREEMENT_REQUEST";
export const DOWNLOAD_AGREEMENT_SUCCESS = "DOWNLOAD_AGREEMENT_SUCCESS";
export const DOWNLOAD_AGREEMENT_FAILURE = "DOWNLOAD_AGREEMENT_FAILURE";

export const SEND_AGR_EMAIL_REQUEST = "SEND_AGR_EMAIL_REQUEST";
export const SEND_AGR_EMAIL_SUCCESS = "SEND_AGR_EMAIL_SUCCESS";
export const SEND_AGR_EMAIL_FAILURE = "SEND_AGR_EMAIL_FAILURE";

export const FETCH_PAX8_PRODUCT_REQUEST = "FETCH_PAX8_PRODUCT_REQUEST";
export const FETCH_PAX8_PRODUCT_SUCCESS = "FETCH_PAX8_PRODUCT_SUCCESS";
export const FETCH_PAX8_PRODUCT_FAILURE = "FETCH_PAX8_PRODUCT_FAILURE";

export const getAgreementTemplateHistory = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement-templates/${id}/version-history?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_TEMPLATE_HISTORY_REQUEST,
      GET_TEMPLATE_HISTORY_SUCCESS,
      GET_TEMPLATE_HISTORY_FAILURE,
    ],
  },
});

export const getAgreementHistory = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreements/${id}/version-history?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_AGREEMENT_HISTORY_REQUEST,
      GET_AGREEMENT_HISTORY_SUCCESS,
      GET_AGREEMENT_HISTORY_FAILURE,
    ],
  },
});

export const getAgreementTemplateList = (
  show: boolean = false,
  params?: AgreementTemplatesQueryParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement-templates`,
    method: "get",
    params: { show_disabled: show, ...params },
    authenticated: true,
    types: [
      FETCH_TEMPLATES_REQUEST,
      FETCH_TEMPLATES_SUCCESS,
      FETCH_TEMPLATES_FAILURE,
    ],
  },
});

export const getAgreementTemplateAuthorsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement-template/authors`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_TEMPLATES_AUTHORS_REQUEST,
      FETCH_TEMPLATES_AUTHORS_SUCCESS,
      FETCH_TEMPLATES_AUTHORS_FAILURE,
    ],
  },
});

export const getAgreementAuthorsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement/authors`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_AGREEMENTS_AUTHORS_REQUEST,
      FETCH_AGREEMENTS_AUTHORS_SUCCESS,
      FETCH_AGREEMENTS_AUTHORS_FAILURE,
    ],
  },
});

export const createAgreementTemplate = (
  method: string = "post",
  data: IAgreementTemplate,
  params: object = {}
) => ({
  [CALL_API]: {
    endpoint:
      method === "post"
        ? `/api/v1/providers/sales/agreement-templates`
        : `/api/v1/providers/sales/agreement-templates/${data.id}`,
    method,
    body: data,
    params,
    authenticated: true,
    types: [
      CREATE_TEMPLATE_REQUEST,
      CREATE_TEMPLATE_SUCCESS,
      CREATE_TEMPLATE_FAILURE,
    ],
  },
});

export const getAgreementTemplate = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement-templates/${id}`,
    method: "get",
    authenticated: true,
    types: [
      CREATE_TEMPLATE_REQUEST,
      CREATE_TEMPLATE_SUCCESS,
      CREATE_TEMPLATE_FAILURE,
    ],
  },
});

export const getAgreementList = (
  show: boolean = false,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreements`,
    method: "get",
    params: { show_disabled: show, ...params },
    authenticated: true,
    types: [
      FETCH_AGREEMENT_REQUEST,
      FETCH_AGREEMENT_SUCCESS,
      FETCH_AGREEMENT_FAILURE,
    ],
  },
});

export const agreementCRUD = (
  method: HTTPMethods = "post",
  data: IAgreement,
  params = {}
) => ({
  [CALL_API]: {
    endpoint:
      method === "post"
        ? `/api/v1/providers/sales/agreements`
        : `/api/v1/providers/sales/agreements/${data.id}`,
    method,
    body: data,
    params,
    authenticated: true,
    types: [
      CREATE_AGREEMENT_REQUEST,
      CREATE_AGREEMENT_SUCCESS,
      CREATE_AGREEMENT_FAILURE,
    ],
  },
});

export const getAgreement = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreements/${id}`,
    method: "get",
    authenticated: true,
    types: [
      CREATE_AGREEMENT_REQUEST,
      CREATE_AGREEMENT_SUCCESS,
      CREATE_AGREEMENT_FAILURE,
    ],
  },
});

export const getTemplatePreview = (
  template_payload: IAgreementTemplate,
  id: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement-templates/preview`,
    method: "post",
    body:
      id !== 0
        ? {
            existing_template_preview: true,
            template_id: id,
          }
        : {
            pre_template_create_preview: true,
            template_payload,
          },
    authenticated: true,
    types: [
      DOWNLOAD_AGREEMENT_REQUEST,
      DOWNLOAD_AGREEMENT_SUCCESS,
      DOWNLOAD_AGREEMENT_FAILURE,
    ],
  },
});

export const getAgreementPreview = (
  agreement_payload: IAgreement,
  id: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreements/preview`,
    method: "post",
    body:
      id !== 0
        ? {
            existing_agreement_preview: true,
            agreement_id: id,
          }
        : {
            pre_agreement_create_preview: true,
            agreement_payload,
          },
    authenticated: true,
    types: [
      DOWNLOAD_AGREEMENT_REQUEST,
      DOWNLOAD_AGREEMENT_SUCCESS,
      DOWNLOAD_AGREEMENT_FAILURE,
    ],
  },
});

export const fetchPAX8ProductDetails = (id: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreement/pax8/products/${id}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_PAX8_PRODUCT_REQUEST,
      FETCH_PAX8_PRODUCT_SUCCESS,
      FETCH_PAX8_PRODUCT_FAILURE,
    ],
  },
});

export const sendEmailToAccountManagerAgreement = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/agreements/${id}/send-to-account-manager`,
    method: "post",
    authenticated: true,
    types: [
      SEND_AGR_EMAIL_REQUEST,
      SEND_AGR_EMAIL_SUCCESS,
      SEND_AGR_EMAIL_FAILURE,
    ],
  },
});
