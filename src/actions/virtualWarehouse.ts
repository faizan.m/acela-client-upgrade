import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_VRWH_DEVICE_DATA_REQUEST = "FETCH_VRWH_DEVICE_DATA_REQUEST";
export const FETCH_VRWH_DEVICE_DATA_SUCCESS = "FETCH_VRWH_DEVICE_DATA_SUCCESS";
export const FETCH_VRWH_DEVICE_DATA_FAILURE = "FETCH_VRWH_DEVICE_DATA_FAILURE";

export const FETCH_VRWH_DEVICE_DETAIL_REQUEST =
  "FETCH_VRWH_DEVICE_DETAIL_REQUEST";
export const FETCH_VRWH_DEVICE_DETAIL_SUCCESS =
  "FETCH_VRWH_DEVICE_DETAIL_SUCCESS";
export const FETCH_VRWH_DEVICE_DETAIL_FAILURE =
  "FETCH_VRWH_DEVICE_DETAIL_FAILURE";

export const FETCH_QUICKBASE_STATUS_REQUEST = "FETCH_QUICKBASE_STATUS_REQUEST";
export const FETCH_QUICKBASE_STATUS_SUCCESS = "FETCH_QUICKBASE_STATUS_SUCCESS";
export const FETCH_QUICKBASE_STATUS_FAILURE = "FETCH_QUICKBASE_STATUS_FAILURE";

export const FETCH_QUICKBASE_WH_REQUEST = "FETCH_QUICKBASE_WH_REQUEST";
export const FETCH_QUICKBASE_WH_SUCCESS = "FETCH_QUICKBASE_WH_SUCCESS";
export const FETCH_QUICKBASE_WH_FAILURE = "FETCH_QUICKBASE_WH_FAILURE";

export const TEST_QUICKBASE_URL_REQUEST = "TEST_QUICKBASE_URL_REQUEST";
export const TEST_QUICKBASE_URL_SUCCESS = "TEST_QUICKBASE_URL_SUCCESS";
export const TEST_QUICKBASE_URL_FAILURE = "TEST_QUICKBASE_URL_FAILURE";

export const VRWH_SETTING_REQUEST = "VRWH_SETTING_REQUEST";
export const VRWH_SETTING_SUCCESS = "VRWH_SETTING_SUCCESS";
export const VRWH_SETTING_FAILURE = "VRWH_SETTING_FAILURE";

export const QUICKBASE_SETTING_REQUEST = "QUICKBASE_SETTING_REQUEST";
export const QUICKBASE_SETTING_SUCCESS = "QUICKBASE_SETTING_SUCCESS";
export const QUICKBASE_SETTING_FAILURE = "QUICKBASE_SETTING_FAILURE";

export const QUICKBASE_SYNC_TRIGGER_REQUEST = "QUICKBASE_SYNC_TRIGGER_REQUEST";
export const QUICKBASE_SYNC_TRIGGER_SUCCESS = "QUICKBASE_SYNC_TRIGGER_SUCCESS";
export const QUICKBASE_SYNC_TRIGGER_FAILURE = "QUICKBASE_SYNC_TRIGGER_FAILURE";

export const fetchVRWarehouseDataListing = (
  params: IServerPaginationParams & IVRWarehouseFilters
) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quick-base-fire-data",
    method: "get",
    params,
    authenticated: true,
    types: [
      FETCH_VRWH_DEVICE_DATA_REQUEST,
      FETCH_VRWH_DEVICE_DATA_SUCCESS,
      FETCH_VRWH_DEVICE_DATA_FAILURE,
    ],
  },
});

export const fetchVRWarehouseDeviceDetail = (deviceId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/quick-base-fire-data/${deviceId}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_VRWH_DEVICE_DETAIL_REQUEST,
      FETCH_VRWH_DEVICE_DETAIL_SUCCESS,
      FETCH_VRWH_DEVICE_DETAIL_FAILURE,
    ],
  },
});

export const getQuickbaseStatuses = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quick-base-statuses",
    method: "get",
    authenticated: true,
    types: [
      FETCH_QUICKBASE_STATUS_REQUEST,
      FETCH_QUICKBASE_STATUS_SUCCESS,
      FETCH_QUICKBASE_STATUS_FAILURE,
    ],
  },
});

export const getQuickbaseWarehouses = () => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quick-base-warehouses",
    method: "get",
    authenticated: true,
    types: [
      FETCH_QUICKBASE_WH_REQUEST,
      FETCH_QUICKBASE_WH_SUCCESS,
      FETCH_QUICKBASE_WH_FAILURE,
    ],
  },
});

export const testQuickbaseURL = (url: string) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/quick-base-pipeline/validate",
    method: "post",
    body: {
      quick_base_pipeline_url: url,
    },
    authenticated: true,
    types: [
      TEST_QUICKBASE_URL_REQUEST,
      TEST_QUICKBASE_URL_SUCCESS,
      TEST_QUICKBASE_URL_FAILURE,
    ],
  },
});

export const quickbaseSyncTrigger = (fullSync?: boolean) => ({
  [CALL_API]: {
    endpoint: "/api/v1/providers/actions/cw-sites-fire-data-quick-base-sync",
    method: "post",
    params: { full_sync: fullSync },
    authenticated: true,
    types: [
      QUICKBASE_SYNC_TRIGGER_REQUEST,
      QUICKBASE_SYNC_TRIGGER_SUCCESS,
      QUICKBASE_SYNC_TRIGGER_FAILURE,
    ],
  },
});

export const fetchOrUpdateVRWHSettings = (
  method: HTTPMethods,
  data?: IVRWarehouseSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/virtual-warehouse-settings`,
    method: method,
    body: data,
    authenticated: true,
    types: [VRWH_SETTING_REQUEST, VRWH_SETTING_SUCCESS, VRWH_SETTING_FAILURE],
  },
});

export const fetchOrUpdateQuickbaseSettings = (
  method: HTTPMethods,
  data?: IQuickbaseSettings
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/quick-base-data-sync-settings`,
    method: method,
    body: data,
    authenticated: true,
    types: [
      QUICKBASE_SETTING_REQUEST,
      QUICKBASE_SETTING_SUCCESS,
      QUICKBASE_SETTING_FAILURE,
    ],
  },
});
