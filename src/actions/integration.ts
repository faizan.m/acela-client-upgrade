import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_INTEGRATIONS_REQUEST = 'FETCH_INTEGRATIONS_REQUEST';
export const FETCH_INTEGRATIONS_SUCCESS = 'FETCH_INTEGRATIONS_SUCCESS';
export const FETCH_INTEGRATIONS_FAILURE = 'FETCH_INTEGRATIONS_FAILURE';

export const FETCH_INTEGRATIONS_CAT_REQUEST = 'FETCH_INTEGRATIONS_CAT_REQUEST';
export const FETCH_INTEGRATIONS_CAT_SUCCESS = 'FETCH_INTEGRATIONS_CAT_SUCCESS';
export const FETCH_INTEGRATIONS_CAT_FAILURE = 'FETCH_INTEGRATIONS_CAT_FAILURE';

export const TEST_INTEGRATION_REQUEST = 'TEST_INTEGRATION_REQUEST';
export const TEST_INTEGRATION_SUCCESS = 'TEST_INTEGRATION_SUCCESS';
export const TEST_INTEGRATION_FAILURE = 'TEST_INTEGRATION_FAILURE';

export const FETCH_INTEGRATIONS_CISCO_REQUEST =
  'FETCH_INTEGRATIONS_CISCO_REQUEST';
export const FETCH_INTEGRATIONS_CISCO_SUCCESS =
  'FETCH_INTEGRATIONS_CISCO_SUCCESS';
export const FETCH_INTEGRATIONS_CISCO_FAILURE =
  'FETCH_INTEGRATIONS_CISCO_FAILURE';

export const FETCH_GROUPS_REQUEST = 'FETCH_GROUPS_REQUEST';
export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';
export const FETCH_GROUPS_FAILURE = 'FETCH_GROUPS_FAILURE';

export const FETCH_GROUPS_CAT_REQUEST = 'FETCH_GROUPS_CAT_REQUEST';
export const FETCH_GROUPS_CAT_SUCCESS = 'FETCH_GROUPS_CAT_SUCCESS';
export const FETCH_GROUPS_CAT_FAILURE = 'FETCH_GROUPS_CAT_FAILURE';

export const FETCH_DEFAULT_MAPPING_REQUEST = 'FETCH_DEFAULT_MAPPING_REQUEST';
export const FETCH_DEFAULT_MAPPING_SUCCESS = 'FETCH_DEFAULT_MAPPING_SUCCESS';
export const FETCH_DEFAULT_MAPPING_FAILURE = 'FETCH_DEFAULT_MAPPING_FAILURE';

export const fetchIntegrations = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/integrations',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_INTEGRATIONS_REQUEST,
      FETCH_INTEGRATIONS_SUCCESS,
      FETCH_INTEGRATIONS_FAILURE,
    ],
  },
});

export const fetchIntegrationsCategories = () => ({
  [CALL_API]: {
    endpoint: '/api/v1/integration-categories',
    method: 'get',
    authenticated: true,
    types: [
      FETCH_INTEGRATIONS_CAT_REQUEST,
      FETCH_INTEGRATIONS_CAT_SUCCESS,
      FETCH_INTEGRATIONS_CAT_FAILURE,
    ],
  },
});

export const fetchIntegrationsCategoriesTypes = (categoryId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/integration-categories/${categoryId}/types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_INTEGRATIONS_REQUEST,
      FETCH_INTEGRATIONS_SUCCESS,
      FETCH_INTEGRATIONS_FAILURE,
    ],
  },
});

export const fetchManufacturerIntegrations = (categoryId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/integration-categories/${categoryId}/types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_INTEGRATIONS_CISCO_REQUEST,
      FETCH_INTEGRATIONS_CISCO_SUCCESS,
      FETCH_INTEGRATIONS_CISCO_FAILURE,
    ],
  },
});

export const testIntegration = (
  catId: number,
  typeId: number,
  integration: Array<{ [name: string]: string }>
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/integration-categories/${catId}/types/${typeId}/auth`,
    method: 'post',
    body: integration,
    authenticated: true,
    types: [
      TEST_INTEGRATION_REQUEST,
      TEST_INTEGRATION_SUCCESS,
      TEST_INTEGRATION_FAILURE,
    ],
  },
});

export const fetchDeviceMonitoringGroups = (
  integration: Array<{ [name: string]: string }>
) => ({
  [CALL_API]: {
    endpoint: '/api/v1/providers/device-monitoring/groups',
    method: 'post',
    body: integration,
    authenticated: true,
    types: [FETCH_GROUPS_REQUEST, FETCH_GROUPS_SUCCESS, FETCH_GROUPS_FAILURE],
  },
});

export const fetchSystemCategoriesByGroupId = (
  id,
  integration: Array<{ [name: string]: string }>
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/device-monitoring/groups/${id}/system-categories`,
    method: 'post',
    body: integration,
    authenticated: true,
    types: [
      FETCH_GROUPS_CAT_REQUEST,
      FETCH_GROUPS_CAT_SUCCESS,
      FETCH_GROUPS_CAT_FAILURE,
    ],
  },
});

export const fetchDefaultKeyMapping = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/device-monitoring/default-key-mappings`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_DEFAULT_MAPPING_REQUEST,
      FETCH_DEFAULT_MAPPING_SUCCESS,
      FETCH_DEFAULT_MAPPING_FAILURE,
    ],
  },
});
