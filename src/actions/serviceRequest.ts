import { CALL_API } from '../middleware/ApiMiddleware';

export const ADD_COMMENT = 'ADD_COMMENT';
export const ADD_TICKET = 'ADD_TICKET';
export const FETCH_TICKETS_REQUEST = 'FETCH_TICKETS_REQUEST';
export const FETCH_TICKETS_SUCCESS = 'FETCH_TICKETS_SUCCESS';
export const FETCH_TICKETS_FAILURE = 'FETCH_TICKETS_FAILURE';
export const FETCH_TICKET_REQUEST = 'FETCH_TICKET_REQUEST';
export const FETCH_TICKET_SUCCESS = 'FETCH_TICKET_SUCCESS';
export const FETCH_TICKET_FAILURE = 'FETCH_TICKET_FAILURE';
export const POST_NOTE_REQUEST = 'POST_NOTE_REQUEST';
export const POST_NOTE_SUCCESS = 'POST_NOTE_SUCCESS';
export const POST_NOTE_FAILURE = 'POST_NOTE_FAILURE';
export const FETCH_CONTACTS_REQUEST = 'FETCH_CONTACTS_REQUEST';
export const FETCH_CONTACTS_SUCCESS = 'FETCH_CONTACTS_SUCCESS';
export const FETCH_CONTACTS_FAILURE = 'FETCH_CONTACTS_FAILURE';
export const POST_TICKET_REQUEST = 'POST_TICKET_REQUEST';
export const POST_TICKET_SUCCESS = 'POST_TICKET_SUCCESS';
export const POST_TICKET_FAILURE = 'POST_TICKET_FAILURE';
export const FETCH_DOCUMENT_REQUEST = 'FETCH_DOCUMENT_REQUEST';
export const FETCH_DOCUMENT_SUCCESS = 'FETCH_DOCUMENT_SUCCESS';
export const FETCH_DOCUMENT_FAILURE = 'FETCH_DOCUMENT_FAILURE';
export const POST_DOCUMENT_REQUEST = 'POST_DOCUMENT_REQUEST';
export const POST_DOCUMENT_SUCCESS = 'POST_DOCUMENT_SUCCESS';
export const POST_DOCUMENT_FAILURE = 'POST_DOCUMENT_FAILURE';
export const FETCH_NOTE_REQUEST = 'FETCH_NOTE_REQUEST';
export const FETCH_NOTE_SUCCESS = 'FETCH_NOTE_SUCCESS';
export const FETCH_NOTE_FAILURE = 'FETCH_NOTE_FAILURE';
export const SET_COMPLETED_TICKETS = 'SET_COMPLETED_TICKETS';
export const UPDATE_TICKET_REQUEST = 'UPDATE_TICKET_REQUEST';
export const UPDATE_TICKET_SUCCESS = 'UPDATE_TICKET_SUCCESS';
export const UPDATE_TICKET_FAILURE = 'UPDATE_TICKET_FAILURE';
export const GET_STATUS_LIST_REQUEST = 'GET_STATUS_LIST_REQUEST';
export const GET_STATUS_LIST_SUCCESS = 'GET_STATUS_LIST_SUCCESS';
export const GET_STATUS_LIST_FAILURE = 'GET_STATUS_LIST_FAILURE';
export const CLOSE_TICKET = 'CLOSE_TICKET';

export const addComment = (comment: any, index: number) => ({
  type: ADD_COMMENT,
  comment,
  index,
});

export const addTicket = (ticket: IServiceTicketNew) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-tickets`,
    method: 'post',
    body: ticket,
    authenticated: true,
    types: [POST_TICKET_REQUEST, POST_TICKET_SUCCESS, POST_TICKET_FAILURE],
  },
});

export const getTickets = (closed?: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-tickets`,
    method: 'get',
    params: { closed },
    authenticated: true,
    types: [
      FETCH_TICKETS_REQUEST,
      FETCH_TICKETS_SUCCESS,
      FETCH_TICKETS_FAILURE,
    ],
  },
});

export const getTicket = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-tickets/${id}`,
    method: 'get',
    authenticated: true,
    types: [FETCH_TICKET_REQUEST, FETCH_TICKET_SUCCESS, FETCH_TICKET_FAILURE],
  },
});

export const sendNote = (id: number, text: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-tickets/${id}/notes`,
    method: 'post',
    body: { text },
    authenticated: true,
    types: [POST_NOTE_REQUEST, POST_NOTE_SUCCESS, POST_NOTE_FAILURE],
  },
});

export const getContacts = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/users`,
    method: 'get',
    params: { 'full-info': false },
    authenticated: true,
    types: [
      FETCH_CONTACTS_REQUEST,
      FETCH_CONTACTS_SUCCESS,
      FETCH_CONTACTS_FAILURE,
    ],
  },
});

export const getDocument = (ticketId: number, documentId: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/service-tickets/${ticketId}/documents/${documentId}`,
    method: 'get',
    authenticated: true,
    responseType: 'blob',
    types: [
      FETCH_DOCUMENT_REQUEST,
      FETCH_DOCUMENT_SUCCESS,
      FETCH_DOCUMENT_FAILURE,
    ],
  },
});

export const postDocument = (ticketId: number, document: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-tickets/${ticketId}/documents`,
    method: 'post',
    body: document,
    authenticated: true,
    types: [
      POST_DOCUMENT_REQUEST,
      POST_DOCUMENT_SUCCESS,
      POST_DOCUMENT_FAILURE,
    ],
  },
});

export const getNote = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-ticket-note`,
    method: 'get',
    authenticated: true,
    types: [FETCH_NOTE_REQUEST, FETCH_NOTE_SUCCESS, FETCH_NOTE_FAILURE],
  },
});

export const setIsCompletedTickets = (is: boolean) => ({
  type: SET_COMPLETED_TICKETS,
  is,
});

//---Provider User---------

export const addTicketPU = (customerId: number, ticket: IServiceTicketNew) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets`,
    method: 'post',
    body: ticket,
    authenticated: true,
    types: [POST_TICKET_REQUEST, POST_TICKET_SUCCESS, POST_TICKET_FAILURE],
  },
});

export const getTicketsPU = (customerId: number, closed?: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets`,
    method: 'get',
    params: { closed },
    authenticated: true,
    types: [
      FETCH_TICKETS_REQUEST,
      FETCH_TICKETS_SUCCESS,
      FETCH_TICKETS_FAILURE,
    ],
  },
});

export const getTicketPU = (customerId: number, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets/${id}`,
    method: 'get',
    authenticated: true,
    types: [FETCH_TICKET_REQUEST, FETCH_TICKET_SUCCESS, FETCH_TICKET_FAILURE],
  },
});

export const sendNotePU = (
  customerId: number,
  id: number,
  text: string,
  internal: boolean
) => ({ 
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets/${id}/notes`,
    method: "post",
    body: { text, is_internal_note: internal },
    authenticated: true,
    types: [POST_NOTE_REQUEST, POST_NOTE_SUCCESS, POST_NOTE_FAILURE],
  },
});

export const getContactsPU = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/users`,
    method: 'get',
    params: { 'full-info': false },
    authenticated: true,
    types: [
      FETCH_CONTACTS_REQUEST,
      FETCH_CONTACTS_SUCCESS,
      FETCH_CONTACTS_FAILURE,
    ],
  },
});

export const getDocumentPU = (
  customerId: number,
  ticketId: number,
  documentId: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets/${ticketId}/documents/${documentId}`,
    method: 'get',
    authenticated: true,
    responseType: 'blob',
    types: [
      FETCH_DOCUMENT_REQUEST,
      FETCH_DOCUMENT_SUCCESS,
      FETCH_DOCUMENT_FAILURE,
    ],
  },
});

export const postDocumentPU = (
  customerId: number,
  ticketId: number,
  document: any
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets/${ticketId}/documents`,
    method: 'post',
    body: document,
    authenticated: true,
    types: [
      POST_DOCUMENT_REQUEST,
      POST_DOCUMENT_SUCCESS,
      POST_DOCUMENT_FAILURE,
    ],
  },
});

export const getNotePU = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/service-ticket-note`,
    method: 'get',
    authenticated: true,
    types: [FETCH_NOTE_REQUEST, FETCH_NOTE_SUCCESS, FETCH_NOTE_FAILURE],
  },
});

export const getServiceTicketStatusList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/service-ticket-status`,
    method: 'get',
    authenticated: true,
    types: [
      GET_STATUS_LIST_REQUEST,
      GET_STATUS_LIST_SUCCESS,
      GET_STATUS_LIST_FAILURE,
    ],
  },
});
export const updateTicket = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/service-tickets/${id}?action=close`,
    method: 'put',
    authenticated: true,
    types: [
      UPDATE_TICKET_REQUEST,
      UPDATE_TICKET_SUCCESS,
      UPDATE_TICKET_FAILURE,
    ],
  },
});

export const updateTicketPU = (customerId: number, id: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/service-tickets/${id}?action=close`,
    method: 'put',
    authenticated: true,
    types: [
      UPDATE_TICKET_REQUEST,
      UPDATE_TICKET_SUCCESS,
      UPDATE_TICKET_FAILURE,
    ],
  },
});

export const closeTicket = (id: string) => ({
  type: CLOSE_TICKET,
  id,
});
