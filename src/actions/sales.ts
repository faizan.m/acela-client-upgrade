import { CALL_API } from "../middleware/ApiMiddleware";

export const SALES_ACTIVITY_STATUS_SETTING_REQUEST =
  "SALES_ACTIVITY_STATUS_SETTING_REQUEST";
export const SALES_ACTIVITY_STATUS_SETTING_SUCCESS =
  "SALES_ACTIVITY_STATUS_SETTING_SUCCESS";
export const SALES_ACTIVITY_STATUS_SETTING_FAILURE =
  "SALES_ACTIVITY_STATUS_SETTING_FAILURE";

export const SALES_ACTIVITY_PRIORITY_SETTING_REQUEST =
  "SALES_ACTIVITY_PRIORITY_SETTING_REQUEST";
export const SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS =
  "SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS";
export const SALES_ACTIVITY_PRIORITY_SETTING_FAILURE =
  "SALES_ACTIVITY_PRIORITY_SETTING_FAILURE";

export const FETCH_SALES_ACTIVITY_REQUEST = "FETCH_SALES_ACTIVITY_REQUEST";
export const FETCH_SALES_ACTIVITY_SUCCESS = "FETCH_SALES_ACTIVITY_SUCCESS";
export const FETCH_SALES_ACTIVITY_FAILURE = "FETCH_SALES_ACTIVITY_FAILURE";

export const CREATE_SALES_ACTIVITY_REQUEST = "CREATE_SALES_ACTIVITY_REQUEST";
export const CREATE_SALES_ACTIVITY_SUCCESS = "CREATE_SALES_ACTIVITY_SUCCESS";
export const CREATE_SALES_ACTIVITY_FAILURE = "CREATE_SALES_ACTIVITY_FAILURE";

export const UPDATE_SALES_ACTIVITY_REQUEST = "UPDATE_SALES_ACTIVITY_REQUEST";
export const UPDATE_SALES_ACTIVITY_SUCCESS = "UPDATE_SALES_ACTIVITY_SUCCESS";
export const UPDATE_SALES_ACTIVITY_FAILURE = "UPDATE_SALES_ACTIVITY_FAILURE";

export const SALES_ACTIVITY_CUSTOM_VIEW_SETTING_REQUEST =
  "SALES_ACTIVITY_CUSTOM_VIEW_SETTING_REQUEST";
export const SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS =
  "SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS";
export const SALES_ACTIVITY_CUSTOM_VIEW_SETTING_FAILURE =
  "SALES_ACTIVITY_CUSTOM_VIEW_SETTING_FAILURE";

export const PURCHASE_HISTORY_REQUEST = "PURCHASE_HISTORY_REQUEST";
export const PURCHASE_HISTORY_SUCCESS = "PURCHASE_HISTORY_SUCCESS";
export const PURCHASE_HISTORY_FAILURE = "PURCHASE_HISTORY_FAILURE";

export const PRODUCT_CATALOG_REQUEST = "PRODUCT_CATALOG_REQUEST";
export const PRODUCT_CATALOG_SUCCESS = "PRODUCT_CATALOG_SUCCESS";
export const PRODUCT_CATALOG_FAILURE = "PRODUCT_CATALOG_FAILURE";

export const PC_IMPORT_CSV_REQUEST = "PC_IMPORT_CSV_REQUEST";
export const PC_IMPORT_CSV_SUCCESS = "PC_IMPORT_CSV_SUCCESS";
export const PC_IMPORT_CSV_FAILURE = "PC_IMPORT_CSV_FAILURE";

export const PC_EXPORT_CSV_REQUEST = "PC_EXPORT_CSV_REQUEST";
export const PC_EXPORT_CSV_SUCCESS = "PC_EXPORT_CSV_SUCCESS";
export const PC_EXPORT_CSV_FAILURE = "PC_EXPORT_CSV_FAILURE";

export const BULK_EDIT_PRODUCT_REQUEST = "BULK_EDIT_PRODUCT_REQUEST";
export const BULK_EDIT_PRODUCT_SUCCESS = "BULK_EDIT_PRODUCT_SUCCESS";
export const BULK_EDIT_PRODUCT_FAILURE = "BULK_EDIT_PRODUCT_FAILURE";

export const getSalesActivityStatusSetting = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/activity-statuses?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      SALES_ACTIVITY_STATUS_SETTING_REQUEST,
      SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
      SALES_ACTIVITY_STATUS_SETTING_FAILURE,
    ],
  },
});

export const postSalesActivityStatusSetting = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/activity-statuses`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_STATUS_SETTING_REQUEST,
      SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
      SALES_ACTIVITY_STATUS_SETTING_FAILURE,
    ],
  },
});

export const editSalesActivityStatusSetting = (data: any, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/activity-status/${id}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_STATUS_SETTING_REQUEST,
      SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
      SALES_ACTIVITY_STATUS_SETTING_FAILURE,
    ],
  },
});

export const deleteSalesActivityStatusSetting = (data: any, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/activity-status/${id}`,
    method: "delete",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_STATUS_SETTING_REQUEST,
      SALES_ACTIVITY_STATUS_SETTING_SUCCESS,
      SALES_ACTIVITY_STATUS_SETTING_FAILURE,
    ],
  },
});

export const getSalesActivityPrioritySetting = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/priority-statuses?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      SALES_ACTIVITY_PRIORITY_SETTING_REQUEST,
      SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
      SALES_ACTIVITY_PRIORITY_SETTING_FAILURE,
    ],
  },
});

export const postSalesActivityPrioritySetting = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/priority-statuses`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_PRIORITY_SETTING_REQUEST,
      SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
      SALES_ACTIVITY_PRIORITY_SETTING_FAILURE,
    ],
  },
});

export const editSalesActivityPrioritySetting = (data: any, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/priority-status/${id}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_PRIORITY_SETTING_REQUEST,
      SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
      SALES_ACTIVITY_PRIORITY_SETTING_FAILURE,
    ],
  },
});

export const deleteSalesActivityPrioritySetting = (data: any, id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/settings/priority-status/${id}`,
    method: "delete",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_PRIORITY_SETTING_REQUEST,
      SALES_ACTIVITY_PRIORITY_SETTING_SUCCESS,
      SALES_ACTIVITY_PRIORITY_SETTING_FAILURE,
    ],
  },
});

export const fetchSalesActivity = (params?: IScrollPaginationFilters) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities`,
    method: "get",
    params: {
      page: params.nextPage,
      page_size: params.page_size,
      ordering: params.ordering,
      search: params.search,
      customer: params.customer,
      status: params.status,
      assigned_to: params.assigned_to,
      created_by: params.created_by,
      priority: params.priority,
      due_date_after: params.due_date_after,
      due_date_before: params.due_date_before,
      activity_type: params.activity_type,
    },
    authenticated: true,
    types: [
      FETCH_SALES_ACTIVITY_REQUEST,
      FETCH_SALES_ACTIVITY_SUCCESS,
      FETCH_SALES_ACTIVITY_FAILURE,
    ],
  },
});

export const postActivityStatus = (data: ISalesActivity) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      CREATE_SALES_ACTIVITY_REQUEST,
      CREATE_SALES_ACTIVITY_SUCCESS,
      CREATE_SALES_ACTIVITY_FAILURE,
    ],
  },
});

export const updateSalesActivity = (
  data: ISalesActivity,
  activityId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/${activityId}`,
    method: "put",
    authenticated: true,
    body: data,
    types: [
      UPDATE_SALES_ACTIVITY_REQUEST,
      UPDATE_SALES_ACTIVITY_SUCCESS,
      UPDATE_SALES_ACTIVITY_FAILURE,
    ],
  },
});

export const getSalesActivityCustomView = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/custom-view`,
    method: "get",
    authenticated: true,
    types: [
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_REQUEST,
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS,
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_FAILURE,
    ],
  },
});

export const postSalesActivityCustomView = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/custom-view`,
    method: "post",
    authenticated: true,
    body: data,
    types: [
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_REQUEST,
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS,
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_FAILURE,
    ],
  },
});

export const deleteSalesActivityCustomView = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/sales-activities/custom-view/${id}`,
    method: "delete",
    authenticated: true,
    types: [
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_REQUEST,
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_SUCCESS,
      SALES_ACTIVITY_CUSTOM_VIEW_SETTING_FAILURE,
    ],
  },
});

export const getPurchaseHistories = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/${customerId}/purchase-orders-history`,
    method: "get",
    authenticated: true,
    types: [
      PURCHASE_HISTORY_REQUEST,
      PURCHASE_HISTORY_SUCCESS,
      PURCHASE_HISTORY_FAILURE,
    ],
  },
});

export const getPurchaseOrderDetail = (
  orderId: number,
  lineItemId: number,
  po_number: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/${orderId}/purchase-order-history-details/${lineItemId}`,
    method: "get",
    params: { po_number },
    authenticated: true,
    types: [
      PURCHASE_HISTORY_REQUEST,
      PURCHASE_HISTORY_SUCCESS,
      PURCHASE_HISTORY_FAILURE,
    ],
  },
});

export const exportPurchaseOrderData = (customer_id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/purchase-orders-history/export`,
    method: "post",
    body: { customer_id },
    authenticated: true,
    responseType: "blob",
    types: [
      PURCHASE_HISTORY_REQUEST,
      PURCHASE_HISTORY_SUCCESS,
      PURCHASE_HISTORY_FAILURE,
    ],
  },
});

export const productCatalogCRUD = (
  method: HTTPMethods,
  params?: IServerPaginationParams & IProductCatalogFilters,
  data?: IProductCatalogItem
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/products${
      data && data.id ? `/${data.id}` : ""
    }`,
    method,
    params,
    body:
      method === "post" || method === "put"
        ? data
        : undefined,
    authenticated: true,
    types: [
      PRODUCT_CATALOG_REQUEST,
      PRODUCT_CATALOG_SUCCESS,
      PRODUCT_CATALOG_FAILURE,
    ],
  },
});

// verify these APIs
export const uploadProductImportCSV = (data: FormData) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/products/import/validate`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      PC_IMPORT_CSV_REQUEST,
      PC_IMPORT_CSV_SUCCESS,
      PC_IMPORT_CSV_FAILURE,
    ],
  },
});

export const fetchProductsToExport = (
  params: IServerPaginationParams & IProductCatalogFilters
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/products/export`,
    method: "get",
    params,
    authenticated: true,
    types: [
      PC_EXPORT_CSV_REQUEST,
      PC_EXPORT_CSV_SUCCESS,
      PC_EXPORT_CSV_FAILURE,
    ],
  },
});

export const bulkCreateProducts = (data: IProductCatalogItem[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/products/bulk-create`,
    method: "post", 
    body: { products_to_create: data },
    authenticated: true,
    types: [
      BULK_EDIT_PRODUCT_REQUEST,
      BULK_EDIT_PRODUCT_SUCCESS,
      BULK_EDIT_PRODUCT_FAILURE,
    ],
  },
});

export const bulkUpdateProducts = (data: IProductCatalogItem[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales/products/bulk-update`,
    method: "put", 
    body: { products_to_update: data },
    authenticated: true,
    types: [
      BULK_EDIT_PRODUCT_REQUEST,
      BULK_EDIT_PRODUCT_SUCCESS,
      BULK_EDIT_PRODUCT_FAILURE,
    ],
  },
});
