import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_NEW_RENEWALS_REQUEST = 'FETCH_NEW_RENEWALS_REQUEST';
export const FETCH_NEW_RENEWALS_SUCCESS = 'FETCH_NEW_RENEWALS_SUCCESS';
export const FETCH_NEW_RENEWALS_FAILURE = 'FETCH_NEW_RENEWALS_FAILURE';

export const FETCH_SERVICE_LEVEL_REQUEST = 'FETCH_SERVICE_LEVEL_REQUEST';
export const FETCH_SERVICE_LEVEL_SUCCESS = 'FETCH_SERVICE_LEVEL_SUCCESS';
export const FETCH_SERVICE_LEVEL_FAILURE = 'FETCH_SERVICE_LEVEL_FAILURE';

export const FETCH_RENEWAL_TYPE_REQUEST = 'FETCH_RENEWAL_TYPE_REQUEST';
export const FETCH_RENEWAL_TYPE_SUCCESS = 'FETCH_RENEWAL_TYPE_SUCCESS';
export const FETCH_RENEWAL_TYPE_FAILURE = 'FETCH_RENEWAL_TYPE_FAILURE';

export const FETCH_PENDING_DEVICES_REQUEST = 'FETCH_PENDING_REQUEST';
export const FETCH_PENDING_DEVICES_SUCCESS = 'FETCH_PENDING_SUCCESS';
export const FETCH_PENDING_DEVICES_FAILURE = 'FETCH_CUSTOMERS_FAILURE';

export const FETCH_COMPLETED_DEVICES_REQUEST = 'FETCH_COMPLETED_REQUEST';
export const FETCH_COMPLETED_DEVICES_SUCCESS = 'FETCH_COMPLETED_SUCCESS';
export const FETCH_COMPLETED_DEVICES_FAILURE = 'FETCH_CUSTOMERS_FAILURE';

export const POST_RENEWAL_REQUEST = 'POST_RENEWAL_REQUEST';
export const POST_RENEWAL_SUCCESS = 'POST_RENEWAL_SUCCESS';
export const POST_RENEWAL_FAILURE = 'POST_RENEWAL_FAILURE';

export const SET_RENEWAL_CUSTOMER_ID = 'SET_RENEWAL_CUSTOMER_ID';

export const DATA_RENEWAL_REQUEST = 'DATA_RENEWAL_REQUEST';
export const DATA_RENEWAL_SUCCESS = 'DATA_RENEWAL_SUCCESS';
export const DATA_RENEWAL_FAILURE = 'DATA_RENEWAL_FAILURE';

export const DOWNLOAD_RENEWAL_REQUEST = 'DOWNLOAD_RENEWAL_REQUEST';
export const DOWNLOAD_RENEWAL_SUCCESS = 'DOWNLOAD_RENEWAL_SUCCESS';
export const DOWNLOAD_RENEWAL_FAILURE = 'DOWNLOAD_RENEWAL_FAILURE';

export const REJECT_PROVIDER_RENEWAL_REQUEST =
  'REJECT_PROVIDER_RENEWAL_REQUEST';
export const REJECT_PROVIDER_RENEWAL_SUCCESS =
  'REJECT_PROVIDER_RENEWAL_SUCCESS';
export const REJECT_PROVIDER_RENEWAL_FAILURE =
  'REJECT_PROVIDER_RENEWAL_FAILURE';

export const getNewRenewalsDevicesForCustomers = (show: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/new-renewals?show_uncovered_only=${show}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_NEW_RENEWALS_REQUEST,
      FETCH_NEW_RENEWALS_SUCCESS,
      FETCH_NEW_RENEWALS_FAILURE,
    ],
  },
});

export const getNewRenewalsDevicesForProviders = (
  show: boolean,
  customerId: number
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/new-renewals?show_uncovered_only=${show}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_NEW_RENEWALS_REQUEST,
      FETCH_NEW_RENEWALS_SUCCESS,
      FETCH_NEW_RENEWALS_FAILURE,
    ],
  },
});

export const submitRenewalRequest = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/renewal-requests`,
    method: 'post',
    body: data,
    authenticated: true,
    types: [POST_RENEWAL_REQUEST, POST_RENEWAL_SUCCESS, POST_RENEWAL_FAILURE],
  },
});

export const submitRenewalRequestProvider = (
  data: any,
  customerId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/renewal-requests`,
    method: 'post',
    body: data,
    authenticated: true,
    types: [POST_RENEWAL_REQUEST, POST_RENEWAL_SUCCESS, POST_RENEWAL_FAILURE],
  },
});

export const getPendingDevicesForCustomers = (
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/pending-renewal-requests`,
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_PENDING_DEVICES_REQUEST,
      FETCH_PENDING_DEVICES_SUCCESS,
      FETCH_PENDING_DEVICES_FAILURE,
    ],
  },
});

export const getCompletedDevicesForCustomers = (
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/customers/completed-renewal-requests`,
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_COMPLETED_DEVICES_REQUEST,
      FETCH_COMPLETED_DEVICES_SUCCESS,
      FETCH_COMPLETED_DEVICES_FAILURE,
    ],
  },
});

export const getRenewalTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/renewal-types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_RENEWAL_TYPE_REQUEST,
      FETCH_RENEWAL_TYPE_SUCCESS,
      FETCH_RENEWAL_TYPE_FAILURE,
    ],
  },
});

export const getServiceLevelTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/service-level-types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_SERVICE_LEVEL_REQUEST,
      FETCH_SERVICE_LEVEL_SUCCESS,
      FETCH_SERVICE_LEVEL_FAILURE,
    ],
  },
});
export const setRenewalCustomerId = (id: number) => ({
  type: SET_RENEWAL_CUSTOMER_ID,
  id,
});

export const uploadRenewalData = (fileReq: any, customerId: number) => ({
  [CALL_API]: {
    //endpoint: `/api/v1/providers/renew`,
    endpoint: `/api/v1/providers/customers/${customerId}/actions/renew`,
    method: 'post',
    body: fileReq,
    authenticated: true,
    types: [DATA_RENEWAL_REQUEST, DATA_RENEWAL_SUCCESS, DATA_RENEWAL_FAILURE],
  },
});

export const exportPendingRenewalRequests = (customerId: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${customerId}/export-pending-renewal-requests`,
    method: 'get',
    authenticated: true,
    types: [
      DOWNLOAD_RENEWAL_REQUEST,
      DOWNLOAD_RENEWAL_SUCCESS,
      DOWNLOAD_RENEWAL_FAILURE,
    ],
  },
});

export const getPendingDevicesForProvider = (
  id: number,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/pending-renewal-requests`,
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_PENDING_DEVICES_REQUEST,
      FETCH_PENDING_DEVICES_SUCCESS,
      FETCH_PENDING_DEVICES_FAILURE,
    ],
  },
});

export const getCompletedDevicesForProviders = (
  id: number,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/completed-renewal-requests`,
    method: 'get',
    authenticated: true,
    params,
    types: [
      FETCH_COMPLETED_DEVICES_REQUEST,
      FETCH_COMPLETED_DEVICES_SUCCESS,
      FETCH_COMPLETED_DEVICES_FAILURE,
    ],
  },
});

export const rejectRenewal = (
  id: number,
  notes: string,
  customerId: number
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${customerId}/renewals/${id}/reject`,
    method: 'post',
    authenticated: true,
    body: { notes },
    types: [
      REJECT_PROVIDER_RENEWAL_REQUEST,
      REJECT_PROVIDER_RENEWAL_SUCCESS,
      REJECT_PROVIDER_RENEWAL_SUCCESS,
    ],
  },
});
