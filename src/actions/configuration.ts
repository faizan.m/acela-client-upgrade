import { CALL_API } from '../middleware/ApiMiddleware';

export const FETCH_CONFIGURATION_REQUEST = 'FETCH_CONFIGURATION_REQUEST';
export const FETCH_CONFIGURATION_SUCCESS = 'FETCH_CONFIGURATION_SUCCESS';
export const FETCH_CONFIGURATION_FAILURE = 'FETCH_CONFIGURATION_FAILURE';

export const FETCH_RULES_REQUEST = 'FETCH_RULES_REQUEST';
export const FETCH_RULES_SUCCESS = 'FETCH_RULES_SUCCESS';
export const FETCH_RULES_FAILURE = 'FETCH_RULES_FAILURE';

export const FETCH_RULE_REQUEST = 'FETCH_RULE_REQUEST';
export const FETCH_RULE_SUCCESS = 'FETCH_RULE_SUCCESS';
export const FETCH_RULE_FAILURE = 'FETCH_RULE_FAILURE';

export const FETCH_RULE_LEVEL_REQUEST = 'FETCH_RULE_LEVEL_REQUEST';
export const FETCH_RULE_LEVEL_SUCCESS = 'FETCH_RULE_LEVEL_SUCCESS';
export const FETCH_RULE_LEVEL_FAILURE = 'FETCH_RULE_LEVEL_FAILURE';

export const FETCH_COMP_CLASS_REQUEST = 'FETCH_COMP_CLASS_REQUEST';
export const FETCH_COMP_CLASS_SUCCESS = 'FETCH_COMP_CLASS_SUCCESS';
export const FETCH_COMP_CLASS_FAILURE = 'FETCH_COMP_CLASS_FAILURE';

export const FETCH_G_VARS_REQUEST = 'FETCH_G_VARS_REQUEST';
export const FETCH_G_VARS_SUCCESS = 'FETCH_G_VARS_SUCCESS';
export const FETCH_G_VARS_FAILURE = 'FETCH_G_VARS_FAILURE';

export const FETCH_G_VAR_REQUEST = 'FETCH_G_VAR_REQUEST';
export const FETCH_G_VAR_SUCCESS = 'FETCH_G_VAR_SUCCESS';
export const FETCH_G_VAR_FAILURE = 'FETCH_G_VAR_FAILURE';

export const FETCH_RULE_FUNCTION_REQUEST = 'FETCH_RULE_FUNCTION_REQUEST';
export const FETCH_RULE_FUNCTION_SUCCESS = 'FETCH_RULE_FUNCTION_SUCCESS';
export const FETCH_RULE_FUNCTION_FAILURE = 'FETCH_RULE_FUNCTION_FAILURE';

export const FETCH_C_VARS_REQUEST = 'FETCH_C_VARS_REQUEST';
export const FETCH_C_VARS_SUCCESS = 'FETCH_C_VARS_SUCCESS';
export const FETCH_C_VARS_FAILURE = 'FETCH_C_VARS_FAILURE';

export const DOWNLOD_DEVICE_REP_REQUEST = 'DOWNLOD_DEVICE_REP_REQUEST';
export const DOWNLOD_DEVICE_REP_SUCCESS = 'DOWNLOD_DEVICE_REP_SUCCESS';
export const DOWNLOD_DEVICE_REP_FAILURE = 'DOWNLOD_DEVICE_REP_FAILURE';

export const FETCH_C_VARS_ALL_REQUEST = 'FETCH_C_VARS_ALL_REQUEST';
export const FETCH_C_VARS_ALL_SUCCESS = 'FETCH_C_VARS_ALL_SUCCESS';
export const FETCH_C_VARS_ALL_FAILURE = 'FETCH_C_VARS_ALL_FAILURE';

export const FETCH_C_VAR_REQUEST = 'FETCH_C_VAR_REQUEST';
export const FETCH_C_VAR_SUCCESS = 'FETCH_C_VAR_SUCCESS';
export const FETCH_C_VAR_FAILURE = 'FETCH_C_VAR_FAILURE';

export const FETCH_VAR_TYPE_REQUEST = 'FETCH_VAR_TYPE_REQUEST';
export const FETCH_VAR_TYPE_SUCCESS = 'FETCH_VAR_TYPE_SUCCESS';
export const FETCH_VAR_TYPE_FAILURE = 'FETCH_VAR_TYPE_FAILURE';

export const FETCH_CUS_RULES_REQUEST = 'FETCH_CUS_RULES_REQUEST';
export const FETCH_CUS_RULES_SUCCESS = 'FETCH_CUS_RULES_SUCCESS';
export const FETCH_CUS_RULES_FAILURE = 'FETCH_CUS_RULES_FAILURE';

export const FETCH_CUS_RULE_REQUEST = 'FETCH_CUS_RULE_REQUEST';
export const FETCH_CUS_RULE_SUCCESS = 'FETCH_CUS_RULE_SUCCESS';
export const FETCH_CUS_RULE_FAILURE = 'FETCH_CUS_RULE_FAILURE';

export const SYNC_CONFIG_REQUEST = 'SYNC_CONFIG_REQUEST';
export const SYNC_CONFIG_SUCCESS = 'SYNC_CONFIG_SUCCESS';
export const SYNC_CONFIG_FAILURE = 'SYNC_CONFIG_FAILURE';

export const CONFIG_TASK_STATUS_REQUEST = "CONFIG_TASK_STATUS_REQUEST";
export const CONFIG_TASK_STATUS_SUCCESS = "CONFIG_TASK_STATUS_SUCCESS";
export const CONFIG_TASK_STATUS_FAILURE = "CONFIG_TASK_STATUS_FAILURE";

export const GET_VAR_HISTORY_REQUEST = "GET_VAR_HISTORY_REQUEST";
export const GET_VAR_HISTORY_SUCCESS = "GET_VAR_HISTORY_SUCCESS";
export const GET_VAR_HISTORY_FAILURE = "GET_VAR_HISTORY_FAILURE";

export const GET_RULE_HISTORY_REQUEST = "GET_RULE_HISTORY_REQUEST";
export const GET_RULE_HISTORY_SUCCESS = "GET_RULE_HISTORY_SUCCESS";
export const GET_RULE_HISTORY_FAILURE = "GET_RULE_HISTORY_FAILURE";

export const GET_DELETE_HTR_REQUEST = 'GET_DELETE_HTR_REQUEST';
export const GET_DELETE_HTR_SUCCESS = 'GET_DELETE_HTR_SUCCESS';
export const GET_DELETE_HTR_FAILURE = 'GET_DELETE_HTR_FAILURE';

export const GET_CREATE_HTR_REQUEST = 'GET_CREATE_HTR_REQUEST';
export const GET_CREATE_HTR_SUCCESS = 'GET_CREATE_HTR_SUCCESS';
export const GET_CREATE_HTR_FAILURE = 'GET_CREATE_HTR_FAILURE';

export const GET_COMPLIANCE_DASHBOARD_REQUEST = 'GET_COMPLIANCE_DASHBOARD_REQUEST';
export const GET_COMPLIANCE_DASHBOARD_SUCCESS = 'GET_COMPLIANCE_DASHBOARD_SUCCESS';
export const GET_COMPLIANCE_DASHBOARD_FAILURE = 'GET_COMPLIANCE_DASHBOARD_FAILURE';

export const GET_COMPLIANCE_CONFIGURATION_SEARCH_REQUEST = 'GET_COMPLIANCE_CONFIGURATION_SEARCH_REQUEST';
export const GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS = 'GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS';
export const GET_COMPLIANCE_CONFIGURATION_SEARCH_FAILURE = 'GET_COMPLIANCE_CONFIGURATION_SEARCH_FAILURE';

export const GET_DEVICE_CONFIGURATION_DETAIL_REQUEST = 'GET_DEVICE_CONFIGURATION_DETAIL_REQUEST';
export const GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS = 'GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS';
export const GET_DEVICE_CONFIGURATION_DETAIL_FAILURE = 'GET_DEVICE_CONFIGURATION_DETAIL_FAILURE';

export const fetchConfigurations = (id: number, no: boolean = false) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    // mockAPI: 'https://10a90497-984e-48f7-84a6-dbbe40b5fd79.mock.pstmn.io/api/v1/providers/customers/10242/monitoring-configs?show-inactive=false',
    endpoint: `/api/v1/providers/customers/${id}/monitoring-configs?show-inactive=false`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CONFIGURATION_REQUEST,
      FETCH_CONFIGURATION_SUCCESS,
      FETCH_CONFIGURATION_FAILURE,
    ],
  },
});

export const fetchConfigurationsCustomerUser = (
  showInactive: boolean = false
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/monitoring-configs?show-inactive=${showInactive}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CONFIGURATION_REQUEST,
      FETCH_CONFIGURATION_SUCCESS,
      FETCH_CONFIGURATION_FAILURE,
    ],
  },
});

export const fetchRules = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules`,
    method: 'get',
    params,
    authenticated: true,
    types: [FETCH_RULES_REQUEST, FETCH_RULES_SUCCESS, FETCH_RULES_FAILURE],
  },
});


export const fetchRulesLevels = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rule-levels`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_RULE_LEVEL_REQUEST,
      FETCH_RULE_LEVEL_SUCCESS,
      FETCH_RULE_LEVEL_FAILURE,
    ],
  },
});

export const fetchClassifications = (user_type: string = 'provider') => ({
  [CALL_API]: {
    endpoint: `${user_type === 'provider' ? `/api/v1/providers/config-compliance/rule-classifications` : `/api/v1/customers/config-compliance/rule-classifications`}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_COMP_CLASS_REQUEST,
      FETCH_COMP_CLASS_SUCCESS,
      FETCH_COMP_CLASS_FAILURE,
    ],
  },
});

export const createClassifications = (text: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rule-classifications`,
    method: 'post',
    body: { name: text },
    authenticated: true,
    types: [
      FETCH_COMP_CLASS_REQUEST,
      FETCH_COMP_CLASS_SUCCESS,
      FETCH_COMP_CLASS_FAILURE,
    ],
  },
});

export const fetchSingleRule = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/${id}`,
    method: 'get',
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});

export const saveRule = (rule: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules`,
    method: 'post',
    body: rule,
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});

export const editRule = (rule: IRule) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/${rule.id}`,
    method: 'put',
    body: rule,
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});

export const deleteRule = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/${id}`,
    method: 'delete',
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});

export const fetchVariableTypes = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variable-types`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_VAR_TYPE_REQUEST,
      FETCH_VAR_TYPE_SUCCESS,
      FETCH_VAR_TYPE_FAILURE,
    ],
  },
});

export const fetchGlobalVariables = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables`,
    method: 'get',
    params,
    authenticated: true,
    types: [FETCH_G_VARS_REQUEST, FETCH_G_VARS_SUCCESS, FETCH_G_VARS_FAILURE],
  },
});
export const fetchSingleGlobalVariable = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/${id}`,
    method: 'get',
    authenticated: true,
    types: [FETCH_G_VAR_REQUEST, FETCH_G_VAR_SUCCESS, FETCH_G_VAR_FAILURE],
  },
});

export const fetchRuleFunctions = () => ({
  [CALL_API]: {
    // mockAPI: `https://e162284f-4b90-4678-bc8c-b3048a540bcd.mock.pstmn.io/api/v1/providers/config-compliance/rule-functions`,
    endpoint: `/api/v1/providers/config-compliance/rule-functions`,
    method: 'get',
    authenticated: true,
    types: [FETCH_RULE_FUNCTION_REQUEST, FETCH_RULE_FUNCTION_SUCCESS, FETCH_RULE_FUNCTION_FAILURE],
  },
});

export const saveGlobalVariable = (rule: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables`,
    method: 'post',
    body: rule,
    authenticated: true,
    types: [FETCH_G_VAR_REQUEST, FETCH_G_VAR_SUCCESS, FETCH_G_VAR_FAILURE],
  },
});

export const editGlobalVariable = (rule: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/${rule.id}`,
    method: 'put',
    body: rule,
    authenticated: true,
    types: [FETCH_G_VAR_REQUEST, FETCH_G_VAR_SUCCESS, FETCH_G_VAR_FAILURE],
  },
});

export const deleteGlobalVariable = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/${id}`,
    method: 'delete',
    authenticated: true,
    types: [FETCH_G_VAR_REQUEST, FETCH_G_VAR_SUCCESS, FETCH_G_VAR_FAILURE],
  },
});

export const checkDeleteGlobalVariable = (id: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/${id}?action=delete`,
    method: 'get',
    authenticated: true,
    types: [FETCH_G_VAR_REQUEST, FETCH_G_VAR_SUCCESS, FETCH_G_VAR_FAILURE],
  },
});

export const fetchCustomerVars = (
  custId: number,
  params?: ISPRulesFilterParams
) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: custId ? `/api/v1/providers/customers/${custId}/config-compliance/variables` : `/api/v1/customers/config-compliance/variables`,
    method: 'get',
    params,
    authenticated: true,
    types: [FETCH_C_VARS_REQUEST, FETCH_C_VARS_SUCCESS, FETCH_C_VARS_FAILURE],
  },
});
export const fetchCustomerVarsAll = (
  CustId: any,
  params?: ISPRulesFilterParams
) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/variables?pagination=false` : `/api/v1/customers/config-compliance/variables?pagination=false`,
    method: 'get',
    params,
    authenticated: true,
    types: [FETCH_C_VARS_ALL_REQUEST, FETCH_C_VARS_ALL_SUCCESS, FETCH_C_VARS_ALL_FAILURE],
  },
});
export const fetchSingleCustomerVar = (CustId: any, id: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/customers/${CustId}/config-compliance/variables/${id}`,
    method: 'get',
    authenticated: true,
    types: [FETCH_C_VAR_REQUEST, FETCH_C_VAR_SUCCESS, FETCH_C_VAR_FAILURE],
  },
});

export const saveCustomerVar = (CustId: any, rule: any) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/variables` : `/api/v1/customers/config-compliance/variables`,
    method: 'post',
    body: rule,
    authenticated: true,
    types: [FETCH_C_VAR_REQUEST, FETCH_C_VAR_SUCCESS, FETCH_C_VAR_FAILURE],
  },
});

export const editCustomerVar = (CustId: any, rule: any) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/variables/${rule.id}` : `/api/v1/customers/config-compliance/variables/${rule.id}`,
    method: 'put',
    body: rule,
    authenticated: true,
    types: [FETCH_C_VAR_REQUEST, FETCH_C_VAR_SUCCESS, FETCH_C_VAR_FAILURE],
  },
});

export const deleteCustomerVar = (CustId: any, id: any) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/variables/${id}` : `/api/v1/customers/config-compliance/variables/${id}`,

    method: 'delete',
    authenticated: true,
    types: [FETCH_C_VAR_REQUEST, FETCH_C_VAR_SUCCESS, FETCH_C_VAR_FAILURE],
  },
});
export const checkDeleteCustomerVar = (CustId: any, id: any) => ({
  [CALL_API]: {
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/variables/${id}?action=delete` : `/api/v1/customers/config-compliance/variables/${id}?action=delete`,
    method: 'get',
    authenticated: true,
    types: [FETCH_C_VAR_REQUEST, FETCH_C_VAR_SUCCESS, FETCH_C_VAR_FAILURE],
  },
});

export const getCustomerRules = (
  CustId: any,
  params?: IServerPaginationParams
) => ({
  [CALL_API]: {
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/rules` : `/api/v1/customers/config-compliance/rules`,
    method: 'get',
    params,
    authenticated: true,
    types: [
      FETCH_CUS_RULES_REQUEST,
      FETCH_CUS_RULES_SUCCESS,
      FETCH_CUS_RULES_FAILURE,
    ],
  },
});

export const fetchSingleCustomerRule = (CustId: any, id: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/customers/${CustId}/config-compliance/rules/${id}`,
    method: 'get',
    authenticated: true,
    types: [
      FETCH_CUS_RULE_REQUEST,
      FETCH_CUS_RULE_SUCCESS,
      FETCH_CUS_RULE_FAILURE,
    ],
  },
});

export const saveCustomerRule = (CustId: any, rule: any) => ({
  [CALL_API]: {
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/rules` : `/api/v1/customers/config-compliance/rules`,
    method: 'post',
    body: rule,
    authenticated: true,
    types: [
      FETCH_CUS_RULE_REQUEST,
      FETCH_CUS_RULE_SUCCESS,
      FETCH_CUS_RULE_FAILURE,
    ],
  },
});

export const editCustomerRule = (CustId: number, rule: IRule) => ({
  [CALL_API]: {
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/rules/${rule.id}` : `/api/v1/customers/config-compliance/rules/${rule.id}`,
    method: 'put',
    body: rule,
    authenticated: true,
    types: [
      FETCH_CUS_RULE_REQUEST,
      FETCH_CUS_RULE_SUCCESS,
      FETCH_CUS_RULE_FAILURE,
    ],
  },
});

export const deleteCustomerRule = (CustId: number, id: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/rules/${id}` : `/api/v1/customers/config-compliance/rules/${id}`,
    method: 'delete',
    authenticated: true,
    types: [
      FETCH_CUS_RULE_REQUEST,
      FETCH_CUS_RULE_SUCCESS,
      FETCH_CUS_RULE_FAILURE,
    ],
  },
});

export const customerRuleAssign = (CustId: any, id: any, status: string) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: CustId ? `/api/v1/providers/customers/${CustId}/config-compliance/rules/${id}/${status}` : `/api/v1/customers/config-compliance/rules/${id}/${status}`,
    method: 'put',
    authenticated: true,
    types: [
      FETCH_CUS_RULE_REQUEST,
      FETCH_CUS_RULE_SUCCESS,
      FETCH_CUS_RULE_FAILURE,
    ],
  },
});


export const fetchTaskStatusConfig = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/task-status/${id}`,
    method: 'get',
    authenticated: true,
    types: [CONFIG_TASK_STATUS_REQUEST, CONFIG_TASK_STATUS_SUCCESS, CONFIG_TASK_STATUS_FAILURE],
  },
});


export const syncConfigCompliance = (customerId: any) => ({
  [CALL_API]: {
    endpoint: customerId ? `/api/v1/providers/customers/${customerId}/config-sync` : `/api/v1/customers/config-sync`,
    method: 'post',
    authenticated: true,
    types: [
      SYNC_CONFIG_REQUEST,
      SYNC_CONFIG_SUCCESS,
      SYNC_CONFIG_FAILURE,
    ],
  },
});

export const getRuleHistory = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/${id}/change-history?pagination=false`,
    method: 'get',
    authenticated: true,
    types: [GET_RULE_HISTORY_REQUEST, GET_RULE_HISTORY_SUCCESS, GET_RULE_HISTORY_FAILURE],
  },
});


export const getVariableHistory = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/${id}/change-history?pagination=false`,
    method: 'get',
    authenticated: true,
    types: [GET_VAR_HISTORY_REQUEST, GET_VAR_HISTORY_SUCCESS, GET_VAR_HISTORY_FAILURE],
  },
});


export const getCustomerVariableHistory = (custId: number, id: number) => ({
  [CALL_API]: {
    endpoint: custId ? `/api/v1/providers/customers/${custId}/config-compliance/variables/${id}/change-history?pagination=false` : `/api/v1/providers/config-compliance/variables/${id}/change-history?pagination=false`,
    method: 'get',
    authenticated: true,
    types: [GET_VAR_HISTORY_REQUEST, GET_VAR_HISTORY_SUCCESS, GET_VAR_HISTORY_FAILURE],
  },
});

export const getCreatedRuleCDHistory = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/create-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_CREATE_HTR_REQUEST, GET_CREATE_HTR_SUCCESS, GET_CREATE_HTR_FAILURE],
  },
});

export const getDeletedRuleCDHistory = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/delete-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_DELETE_HTR_REQUEST, GET_DELETE_HTR_SUCCESS, GET_DELETE_HTR_FAILURE],
  },
});

export const revertDeletedRule = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/rules/delete-history`,
    method: 'post',
    body: { 'version_id': id },
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});


export const getCustomerCreatedRuleCDHistory = (id: number, params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/config-compliance/rules/create-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_CREATE_HTR_REQUEST, GET_CREATE_HTR_SUCCESS, GET_CREATE_HTR_FAILURE],
  },
});

export const getCustomerDeletedRuleCDHistory = (id: number, params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/config-compliance/rules/delete-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_DELETE_HTR_REQUEST, GET_DELETE_HTR_SUCCESS, GET_DELETE_HTR_FAILURE],
  },
});

export const revertCustomerDeletedRule = (id: number, custId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${custId}/config-compliance/rules/delete-history/${id}`,
    method: 'put',
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});

//---variable


export const getCreatedVariableCDHistory = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/create-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_CREATE_HTR_REQUEST, GET_CREATE_HTR_SUCCESS, GET_CREATE_HTR_FAILURE],
  },
});

export const getDeletedVariableCDHistory = (params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/delete-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_DELETE_HTR_REQUEST, GET_DELETE_HTR_SUCCESS, GET_DELETE_HTR_FAILURE],
  },
});

export const revertDeletedVariable = (id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/config-compliance/variables/delete-history`,
    method: 'post',
    body: { 'version_id': id },
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});


export const getCustomerCreatedVariableCDHistory = (id: number, params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/config-compliance/variables/create-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_CREATE_HTR_REQUEST, GET_CREATE_HTR_SUCCESS, GET_CREATE_HTR_FAILURE],
  },
});

export const getCustomerDeletedVariableCDHistory = (id: number, params?: IServerPaginationParams) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${id}/config-compliance/variables/delete-history`,
    method: 'get',
    params,
    authenticated: true,
    types: [GET_DELETE_HTR_REQUEST, GET_DELETE_HTR_SUCCESS, GET_DELETE_HTR_FAILURE],
  },
});

export const revertCustomerDeletedVariable = (id: number, custId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/customers/${custId}/config-compliance/variables/delete-history/${id}`,
    method: 'put',
    authenticated: true,
    types: [FETCH_RULE_REQUEST, FETCH_RULE_SUCCESS, FETCH_RULE_FAILURE],
  },
});

export const fetchComplianceDashboardData = (id: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    // mockAPI: 'https://10a90497-984e-48f7-84a6-dbbe40b5fd79.mock.pstmn.io/api/v1/providers/customers/10242/monitoring-configs?show-inactive=false',
    endpoint: `/api/v1/providers/customers/${id}/rules/dashboard?show-inactive=false`,
    method: 'get',
    authenticated: true,
    types: [
      GET_COMPLIANCE_DASHBOARD_REQUEST,
      GET_COMPLIANCE_DASHBOARD_SUCCESS,
      GET_COMPLIANCE_DASHBOARD_FAILURE,
    ],
  },
});

export const fetchCustomerComplianceDashboardData = (
) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/rules/dashboard`,
    method: 'get',
    authenticated: true,
    types: [
      GET_COMPLIANCE_DASHBOARD_REQUEST,
      GET_COMPLIANCE_DASHBOARD_SUCCESS,
      GET_COMPLIANCE_DASHBOARD_FAILURE,
    ],
  },
});

export const downloadDeviceReportSingle = (
  id: number,
  deviceId: number,
) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/providers/customers/${id}/devices/${deviceId}/compliance-report`,
    method: 'get',
    authenticated: true,
    types: [
      DOWNLOD_DEVICE_REP_REQUEST,
      DOWNLOD_DEVICE_REP_SUCCESS,
      DOWNLOD_DEVICE_REP_FAILURE
    ],
  },
});

export const downloadCustomerDeviceReportSingle = (
  deviceId: any,
) => ({
  [CALL_API]: {
    // tslint:disable-next-line: max-line-length
    endpoint: `/api/v1/customers/devices/${deviceId}/compliance-report`,
    method: 'get',
    authenticated: true,
    types: [
      DOWNLOD_DEVICE_REP_REQUEST,
      DOWNLOD_DEVICE_REP_SUCCESS,
      DOWNLOD_DEVICE_REP_FAILURE
    ],
  },
});

export const fetchConfigurationSearchResults = (id: number, searchQuery: string, deviceIds: number[]) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/providers/customers/${id}/configurations?q=${searchQuery}`,
    method: 'post',
    body: { device_ids: deviceIds },
    authenticated: true,
    types: [
      GET_COMPLIANCE_CONFIGURATION_SEARCH_REQUEST,
      GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS,
      GET_COMPLIANCE_CONFIGURATION_SEARCH_FAILURE
    ],
  },
});

export const fetchCustomerConfigurationSearchResults = (
  searchQuery: string, deviceIds: number[]) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/configurations?q=${searchQuery}`,
    method: 'post',
    body: { device_ids: deviceIds },
    authenticated: true,
    types: [
      GET_COMPLIANCE_CONFIGURATION_SEARCH_REQUEST,
      GET_COMPLIANCE_CONFIGURATION_SEARCH_SUCCESS,
      GET_COMPLIANCE_CONFIGURATION_SEARCH_FAILURE
    ],
  },
});

export const fetchDeviceConfigurationDetail = (customerId: number, deviceId: number) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    // mockAPI: 'https://10a90497-984e-48f7-84a6-dbbe40b5fd79.mock.pstmn.io/api/v1/providers/customers/10242/monitoring-configs?show-inactive=false',
    endpoint: `/api/v1/providers/customers/${customerId}/devices/${deviceId}/compliance`,
    method: 'get',
    authenticated: true,
    types: [
      GET_DEVICE_CONFIGURATION_DETAIL_REQUEST,
      GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS,
      GET_DEVICE_CONFIGURATION_DETAIL_FAILURE
    ],
  },
});

export const fetchCustomerDeviceConfigurationDetail = (
  deviceId: number ) => ({
  [CALL_API]: {
    // tslint:disable-next-line:max-line-length
    endpoint: `/api/v1/customers/devices/${deviceId}/compliance`,
    method: 'get',
    authenticated: true,
    types: [
      GET_DEVICE_CONFIGURATION_DETAIL_REQUEST,
      GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS,
      GET_DEVICE_CONFIGURATION_DETAIL_FAILURE
    ],
  },
});
