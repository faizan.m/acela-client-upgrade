import { CALL_API } from "../middleware/ApiMiddleware";

export const FETCH_OPEN_PO_REQUEST = "FETCH_OPEN_PO_REQUEST";
export const FETCH_OPEN_PO_SUCCESS = "FETCH_OPEN_PO_SUCCESS";
export const FETCH_OPEN_PO_FAILURE = "FETCH_OPEN_PO_FAILURE";

export const FETCH_ESTIMATED_REQUEST = "FETCH_ESTIMATED_REQUEST";
export const FETCH_ESTIMATED_SUCCESS = "FETCH_ESTIMATED_SUCCESS";
export const FETCH_ESTIMATED_FAILURE = "FETCH_ESTIMATED_FAILURE";

export const FETCH_SHIPPED_REQUEST = "FETCH_SHIPPED_REQUEST";
export const FETCH_SHIPPED_SUCCESS = "FETCH_SHIPPED_SUCCESS";
export const FETCH_SHIPPED_FAILURE = "FETCH_SHIPPED_FAILURE";

export const FETCH_LAST_CUST_DATA_REQUEST = "FETCH_LAST_CUST_DATA_REQUEST";
export const FETCH_LAST_CUST_DATA_SUCCESS = "FETCH_LAST_CUST_DATA_SUCCESS";
export const FETCH_LAST_CUST_DATA_FAILURE = "FETCH_LAST_CUST_DATA_FAILURE";

export const GET_INF_LISTE_REQUEST = "GET_INF_LISTE_REQUEST";
export const GET_INF_LISTE_SUCCESS = "GET_INF_LISTE_SUCCESS";
export const GET_INF_LISTE_FAILURE = "GET_INF_LISTE_FAILURE";

export const GET_OT_TABLE_REQUEST = "GET_OT_TABLE_REQUEST";
export const GET_OT_TABLE_SUCCESS = "GET_OT_TABLE_SUCCESS";
export const GET_OT_TABLE_FAILURE = "GET_OT_TABLE_FAILURE";

export const GET_DATA_REQUEST = "GET_DATA_REQUEST";
export const GET_DATA_SUCCESS = "GET_DATA_SUCCESS";
export const GET_DATA_FAILURE = "GET_DATA_FAILURE";

export const FETCH_STATUS_PO_REQUEST = "FETCH_STATUS_PO_REQUEST";
export const FETCH_STATUS_PO_SUCCESS = "FETCH_STATUS_PO_SUCCESS";
export const FETCH_STATUS_PO_FAILURE = "FETCH_STATUS_PO_FAILURE";

export const GET_EMAIL_TEMPLATE_OPPORTUNITY_REQUEST =
  "GET_EMAIL_TEMPLATE_OPPORTUNITY_REQUEST";
export const GET_EMAIL_TEMPLATE_OPPORTUNITY_SUCCESS =
  "GET_EMAIL_TEMPLATE_OPPORTUNITY_SUCCESS";
export const GET_EMAIL_TEMPLATE_OPPORTUNITY_FAILURE =
  "GET_EMAIL_TEMPLATE_OPPORTUNITY_FAILURE";

export const GET_CUSTOMER_SERVICE_TICKETS_REQUEST =
  "GET_CUSTOMER_SERVICE_TICKETS_REQUEST";
export const GET_CUSTOMER_SERVICE_TICKETS_SUCCESS =
  "GET_CUSTOMER_SERVICE_TICKETS_SUCCESS";
export const GET_CUSTOMER_SERVICE_TICKETS_FAILURE =
  "GET_CUSTOMER_SERVICE_TICKETS_FAILURE";

export const GET_SERVICE_TICKETS_REQUEST = "GET_SERVICE_TICKETS_REQUEST";
export const GET_SERVICE_TICKETS_SUCCESS = "GET_SERVICE_TICKETS_SUCCESS";
export const GET_SERVICE_TICKETS_FAILURE = "GET_SERVICE_TICKETS_FAILURE";

export const GET_UNMAPPED_SERVICE_TICKETS_REQUEST =
  "GET_UNMAPPED_SERVICE_TICKETS_REQUEST";
export const GET_UNMAPPED_SERVICE_TICKETS_SUCCESS =
  "GET_UNMAPPED_SERVICE_TICKETS_SUCCESS";
export const GET_UNMAPPED_SERVICE_TICKETS_FAILURE =
  "GET_UNMAPPED_SERVICE_TICKETS_FAILURE";

export const GET_CUSTOMER_QUOTES_REQUEST = "GET_CUSTOMER_QUOTES_REQUEST";
export const GET_CUSTOMER_QUOTES_SUCCESS = "GET_CUSTOMER_QUOTES_SUCCESS";
export const GET_CUSTOMER_QUOTES_FAILURE = "GET_CUSTOMER_QUOTES_FAILURE";

export const GET_SALES_ORDERS_REQUEST = "GET_SALES_ORDERS_REQUEST";
export const GET_SALES_ORDERS_SUCCESS = "GET_SALES_ORDERS_SUCCESS";
export const GET_SALES_ORDERS_FAILURE = "GET_SALES_ORDERS_FAILURE";

export const GET_PURCHASE_ORDERS_REQUEST = "GET_PURCHASE_ORDERS_REQUEST";
export const GET_PURCHASE_ORDERS_SUCCESS = "GET_PURCHASE_ORDERS_SUCCESS";
export const GET_PURCHASE_ORDERS_FAILURE = "GET_PURCHASE_ORDERS_FAILURE";

export const GET_LINKED_PO_REQUEST = "GET_LINKED_PO_REQUEST";
export const GET_LINKED_PO_SUCCESS = "GET_LINKED_PO_SUCCESS";
export const GET_LINKED_PO_FAILURE = "GET_LINKED_PO_FAILURE";

export const GET_SO_PO_LINKING_REQUEST = "GET_SO_PO_LINKING_REQUEST";
export const GET_SO_PO_LINKING_SUCCESS = "GET_SO_PO_LINKING_SUCCESS";
export const GET_SO_PO_LINKING_FAILURE = "GET_SO_PO_LINKING_FAILURE";

export const POST_IGNORE_TICKETS_REQUEST = "POST_IGNORE_TICKETS_REQUEST";
export const POST_IGNORE_TICKETS_SUCCESS = "POST_IGNORE_TICKETS_SUCCESS";
export const POST_IGNORE_TICKETS_FAILURE = "POST_IGNORE_TICKETS_FAILURE";

export const POST_IGNORE_SO_REQUEST = "POST_IGNORE_SO_REQUEST";
export const POST_IGNORE_SO_SUCCESS = "POST_IGNORE_SO_SUCCESS";
export const POST_IGNORE_SO_FAILURE = "POST_IGNORE_SO_FAILURE";

export const LINK_TICKET_OPP_REQUEST = "LINK_TICKET_OPP_REQUEST";
export const LINK_TICKET_OPP_SUCCESS = "LINK_TICKET_OPP_SUCCESS";
export const LINK_TICKET_OPP_FAILURE = "LINK_TICKET_OPP_FAILURE";

export const FETCH_LINKING_INFO_REQUEST = "FETCH_LINKING_INFO_REQUEST";
export const FETCH_LINKING_INFO_SUCCESS = "FETCH_LINKING_INFO_SUCCESS";
export const FETCH_LINKING_INFO_FAILURE = "FETCH_LINKING_INFO_FAILURE";

export const getOpenPOByPOBYDistributor = (showLoggedInUserData: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/open-po-by-distributer?show-all-data=${!showLoggedInUserData}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_OPEN_PO_REQUEST,
      FETCH_OPEN_PO_SUCCESS,
      FETCH_OPEN_PO_FAILURE,
    ],
  },
});

export const getTabledata = (
  showLoggedInUserData: boolean,
  params?: IServerPaginationParams & IOperationsDashboardFilterParams
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/table-data?show-all-data=${!showLoggedInUserData}`,
    method: "get",
    params,
    authenticated: true,
    types: [GET_OT_TABLE_REQUEST, GET_OT_TABLE_SUCCESS, GET_OT_TABLE_FAILURE],
  },
});

export const getLastCustomerUpdatedData = (showLoggedInUserData: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/last-customer-updated-data?show-all-data=${!showLoggedInUserData}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_LAST_CUST_DATA_REQUEST,
      FETCH_LAST_CUST_DATA_SUCCESS,
      FETCH_LAST_CUST_DATA_FAILURE,
    ],
  },
});

export const getOrderShippedNotReceived = (showLoggedInUserData: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/orders-shipped-not-received?show-all-data=${!showLoggedInUserData}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_SHIPPED_REQUEST,
      FETCH_SHIPPED_SUCCESS,
      FETCH_SHIPPED_FAILURE,
    ],
  },
});
export const getOrderEstimatedShipDate = (showLoggedInUserData: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/orders-by-est-shipped-date?show-all-data=${!showLoggedInUserData}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_ESTIMATED_REQUEST,
      FETCH_ESTIMATED_SUCCESS,
      FETCH_ESTIMATED_FAILURE,
    ],
  },
});

export const getOrderEstimatedShipDateCW = (showLoggedInUserData: boolean) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/orders-by-cw-est-shipped-date?show-all-data=${!showLoggedInUserData}`,
    method: "get",
    authenticated: true,
    types: [
      FETCH_ESTIMATED_REQUEST,
      FETCH_ESTIMATED_SUCCESS,
      FETCH_ESTIMATED_FAILURE,
    ],
  },
});

export const fetchInfiniteList = (
  url: string,
  params?: IScrollPaginationFilters
) => ({
  [CALL_API]: {
    endpoint: `/api/v1/${url}`,
    method: "get",
    params: {
      page: params.nextPage,
      page_size: params.page_size,
      ordering: params.ordering,
      search: params.search,
      ...params,
    },
    authenticated: true,
    types: [
      GET_INF_LISTE_REQUEST,
      GET_INF_LISTE_SUCCESS,
      GET_INF_LISTE_FAILURE,
    ],
  },
});

export const salesOrderStatusSetting = (method, data) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-orders/status-settings`,
    method: method || "get",
    authenticated: true,
    body: data,
    types: [
      FETCH_STATUS_PO_REQUEST,
      FETCH_STATUS_PO_SUCCESS,
      FETCH_STATUS_PO_FAILURE,
    ],
  },
});

export const getDataCommon = (url: string) => ({
  [CALL_API]: {
    endpoint: `/api/v1/${url}`,
    method: "get",
    authenticated: true,
    types: [GET_DATA_REQUEST, GET_DATA_SUCCESS, GET_DATA_FAILURE],
  },
});

export const getServiceTicketsListForCustomer = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/customers/${customerId}/service-tickets`,
    method: "get",
    authenticated: true,
    types: [
      GET_CUSTOMER_SERVICE_TICKETS_REQUEST,
      GET_CUSTOMER_SERVICE_TICKETS_SUCCESS,
      GET_CUSTOMER_SERVICE_TICKETS_FAILURE,
    ],
  },
});

export const getServiceTicketsList = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/service-tickets`,
    method: "get",
    authenticated: true,
    types: [
      GET_SERVICE_TICKETS_REQUEST,
      GET_SERVICE_TICKETS_SUCCESS,
      GET_SERVICE_TICKETS_FAILURE,
    ],
  },
});

export const getUnmappedServiceTickets = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/unmapped-service-tickets-from-sos`,
    method: "get",
    authenticated: true,
    types: [
      GET_UNMAPPED_SERVICE_TICKETS_REQUEST,
      GET_UNMAPPED_SERVICE_TICKETS_SUCCESS,
      GET_UNMAPPED_SERVICE_TICKETS_FAILURE,
    ],
  },
});

export const getEmailTemplateUsingOpportunity = (quote: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/so-email-template/generation`,
    method: "post",
    body: {
      opportunity_crm_id: quote,
    },
    authenticated: true,
    types: [
      GET_EMAIL_TEMPLATE_OPPORTUNITY_REQUEST,
      GET_EMAIL_TEMPLATE_OPPORTUNITY_SUCCESS,
      GET_EMAIL_TEMPLATE_OPPORTUNITY_FAILURE,
    ],
  },
});

export const getOperationsCustomerQuotes = (customerId: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/operations/customers/${customerId}/quotes`,
    method: "get",
    authenticated: true,
    types: [
      GET_CUSTOMER_QUOTES_REQUEST,
      GET_CUSTOMER_QUOTES_SUCCESS,
      GET_CUSTOMER_QUOTES_FAILURE,
    ],
  },
});

export const getSalesOrdersListing = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/umapped-sos-from-tickets`,
    method: "get",
    authenticated: true,
    types: [
      GET_SALES_ORDERS_REQUEST,
      GET_SALES_ORDERS_SUCCESS,
      GET_SALES_ORDERS_FAILURE,
    ],
  },
});

export const getUnlinkedPurchaseOrdersListing = () => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/unlinked-purchase-orders?pagination=false`,
    method: "get",
    authenticated: true,
    types: [
      GET_PURCHASE_ORDERS_REQUEST,
      GET_PURCHASE_ORDERS_SUCCESS,
      GET_PURCHASE_ORDERS_FAILURE,
    ],
  },
});

export const getLinkedPurchaseOrders = (sales_order_crm_id: number) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/mapped-pos-with-so/${sales_order_crm_id}`,
    method: "get",
    authenticated: true,
    types: [
      GET_LINKED_PO_REQUEST,
      GET_LINKED_PO_SUCCESS,
      GET_LINKED_PO_FAILURE,
    ],
  },
});

export const postSOPOLinking = (data: {
  sales_order_crm_id: number;
  opportunity_crm_id: number;
  purchase_order_crm_ids: number[];
}) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/po-link`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      GET_SO_PO_LINKING_REQUEST,
      GET_SO_PO_LINKING_SUCCESS,
      GET_SO_PO_LINKING_FAILURE,
    ],
  },
});

export const postIgnoredTickets = (tickets: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/order-tracking/dashboard/ignore-failed-service-tickets`,
    method: "put",
    body: { ticket_crm_ids: tickets },
    authenticated: true,
    types: [
      POST_IGNORE_TICKETS_REQUEST,
      POST_IGNORE_TICKETS_SUCCESS,
      POST_IGNORE_TICKETS_FAILURE,
    ],
  },
});

export const postIgnoredSOs = (ids: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/ignore-sos`,
    method: "put",
    body: { sales_order_crm_ids: ids },
    authenticated: true,
    types: [
      POST_IGNORE_SO_REQUEST,
      POST_IGNORE_SO_SUCCESS,
      POST_IGNORE_SO_FAILURE,
    ],
  },
});

export const postUnignoredSOs = (ids: number[]) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/revoke-ignored-sos`,
    method: "put",
    body: { sales_order_crm_ids: ids },
    authenticated: true,
    types: [
      POST_IGNORE_SO_REQUEST,
      POST_IGNORE_SO_SUCCESS,
      POST_IGNORE_SO_FAILURE,
    ],
  },
});

export const linkServiceTicketOpportunity = (data: any) => ({
  [CALL_API]: {
    endpoint: `/api/v1/providers/sales-order/ticket-oppo-so-linking`,
    method: "post",
    body: data,
    authenticated: true,
    types: [
      LINK_TICKET_OPP_REQUEST,
      LINK_TICKET_OPP_SUCCESS,
      LINK_TICKET_OPP_FAILURE,
    ],
  },
});
