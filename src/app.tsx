import React from "react";
import { Provider } from "react-redux";
import { Route, Switch } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";

// import * as Sentry from "@sentry/react";
// import { BrowserTracing } from "@sentry/tracing";
import PrivateRoute from "./components/PrivateRoute/privateRoute";
import Home from "./containers/Home/home";
import TWOFA from "./containers/Login/2fa";
import ForgotPassword from "./containers/Login/forgotPassword";
import Login from "./containers/Login/login";
import ResetPassword from "./containers/Login/resetPassword";
import { setAPIBaseUrl as initAPIMiddleware } from "./middleware/ApiMiddleware";
import { setAPIBaseUrl as initAuthMiddleware } from "./middleware/AuthMiddleware";
import store from "./store";

import "./main.scss";
// Sentry.init({
//   dsn: "https://8f47f08dee61410790e080401eecd243@o288293.ingest.sentry.io/4504286056218624",
//   integrations: [new BrowserTracing()],
//   // Set tracesSampleRate to 1.0 to capture 100%
//   // of transactions for performance monitoring.
//   // We recommend adjusting this value in production
//   tracesSampleRate: 1.0,
// });
const initApp = () => {
  initAPIMiddleware(import.meta.env.VITE_API_URL);
  initAuthMiddleware(import.meta.env.VITE_API_URL);
};
initApp();
// function FallbackComponent() {
//   return <div>An error has occurred</div>;
// }
// const myFallback = <FallbackComponent />;

function App() {
  return (
    // <Sentry.ErrorBoundary fallback={myFallback} showDialog>
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exact path="/login" component={Login} />
          <Route exact path="/eyJ0eXAiOiJKdV1QiLCJ2fa" component={TWOFA} />
          <Route exact path="/forgot-password" component={ForgotPassword} />
          <Route
            exact
            path="/set-password/:uid/:token/"
            component={ResetPassword}
          />
          <PrivateRoute path="/" component={Home} />
        </Switch>
      </Router>
    </Provider>
    // </Sentry.ErrorBoundary>
  );
}

export default App;
