import {
  CLEAR_ERROR,
  ERROR,
  FETCHING_COMPLETE,
  IS_FETCHING,
  LOCATION_CHANGE,
  SHOW_INFO_MESSAGE,
  SHOW_SUCCESS_MESSAGE,
  SHOW_WARNING_MESSAGE,
} from "../actions/appState";
import { userLogOut } from "../utils/AuthUtil";

const initialState: IAppStateStore = {
  loadingComponents: 0,
  errors: {},
  warnings: {},
  infoMessage: {},
  successMessage: {},
  prevLocation: "",
  currentLocation: "",
};

export default function applicationState(
  state: IAppStateStore = initialState,
  action: any
): IAppStateStore {
  const newState = Object.assign({}, state);

  switch (action.type) {
    case IS_FETCHING:
      if (!action.hideLoadingWidget) {
        newState.loadingComponents++;
      }

      return newState;

    case FETCHING_COMPLETE:
      if (!action.hideLoadingWidget) {
        newState.loadingComponents--;
      }

      return newState;

    case ERROR:
      if (!action.hideLoadingWidget) {
        newState.loadingComponents--;
      }

      const errors = { ...newState.errors };
      const warnings = { ...newState.warnings };
      const newErrorKey = new Date().getTime();
      const details = "detail";

      switch (action.errorDetails && action.errorDetails.status) {
        case 401:
        case 402:
        case 403:
          errors[newErrorKey] = action.errorDetails.data[details];
          setTimeout(() => userLogOut(), 3000);
          break;
        case 412:
          warnings[newErrorKey] = action.errorDetails.data[details];
          break;
        case 400:
          break;

        case 404:
          localStorage.setItem("redirectnotfound", "redirect");
          break;

        default:
          errors[newErrorKey] = action.errorMessage;
      }

      newState.warnings = warnings;
      newState.errors = errors;

      return newState;

    case CLEAR_ERROR:
      const errors1 = { ...newState.errors };
      delete errors1[action.errorKey];
      newState.errors = errors1;

      return newState;

    case SHOW_SUCCESS_MESSAGE:
      if (!action.hideLoadingWidget) {
        newState.loadingComponents--;
      }
      const successMessage = { ...newState.successMessage };
      const newErrorKey2 = new Date().getTime();
      successMessage[newErrorKey2] = action.message;
      newState.successMessage = successMessage;

      return newState;

    case SHOW_INFO_MESSAGE:
      const infoMessage = { ...newState.infoMessage };
      const newErrorKey4 = new Date().getTime();
      infoMessage[newErrorKey4] = action.message;
      newState.infoMessage = infoMessage;
      return newState;

    case SHOW_WARNING_MESSAGE:
      if (!action.hideLoadingWidget) {
        newState.loadingComponents--;
      }
      const warningsMsg = { ...newState.warnings };
      const newErrorKey3 = new Date().getTime();
      warningsMsg[newErrorKey3] = action.message;
      newState.warnings = warningsMsg;

      return newState;

    case LOCATION_CHANGE:
      newState.prevLocation = state.currentLocation;
      newState.currentLocation = action.payload.pathname;

      return newState;

    default:
      return state;
  }
}
