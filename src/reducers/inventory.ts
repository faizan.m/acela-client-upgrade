import {
  ADD_SITE_FAILURE,
  ADD_SITE_REQUEST,
  ADD_SITE_SUCCESS,
  BATCH_CREATE_FAILURE,
  BATCH_CREATE_REQUEST,
  BATCH_CREATE_SUCCESS,
  BATCH_EXPORT_SUCCESS,
  EDIT_SINGLE_DEVICE_FAILURE,
  EDIT_SINGLE_DEVICE_SUCCESS,
  FETCH_AVAILABLE_DEVICE_ASSOCIATION_FAILURE,
  FETCH_AVAILABLE_DEVICE_ASSOCIATION_REQUEST,
  FETCH_AVAILABLE_DEVICE_ASSOCIATION_SUCCESS,
  FETCH_CONTRACT_STATUSES_SUCCESS,
  FETCH_COUNTRIES_FAILURE,
  FETCH_COUNTRIES_REQUEST,
  FETCH_COUNTRIES_SUCCESS,
  FETCH_DATE_FORMAT_SUCCESS,
  FETCH_DEVICE_CATEGORIES_FAILURE,
  FETCH_DEVICE_CATEGORIES_REQUEST,
  FETCH_DEVICE_CATEGORIES_SUCCESS,
  FETCH_DEVICES_FAILURE,
  FETCH_DEVICES_REQUEST,
  FETCH_DEVICES_SUCCESS,
  FETCH_EXISTING_DEVICE_ASSOCIATION_FAILURE,
  FETCH_EXISTING_DEVICE_ASSOCIATION_REQUEST,
  FETCH_EXISTING_DEVICE_ASSOCIATION_SUCCESS,
  FETCH_MANUFACTURERS_SUCCESS,
  FETCH_P_INTEGRATIONS_FAILURE,
  FETCH_P_INTEGRATIONS_REQUEST,
  FETCH_P_INTEGRATIONS_SUCCESS,
  FETCH_SINGLE_DEVICE_FAILURE,
  FETCH_SINGLE_DEVICE_REQUEST,
  FETCH_SINGLE_DEVICE_SUCCESS,
  FETCH_SITES_FAILURE,
  FETCH_SITES_REQUEST,
  FETCH_SITES_SUCCESS,
  FETCH_STATUSES_FAILURE,
  FETCH_STATUSES_REQUEST,
  FETCH_STATUSES_SUCCESS,
  FETCH_TYPES_SUCCESS,
  FETCH_XLSX__SUCCESS,
  FETCH_XLSX_REQUEST,
  SET_XLSX_DATA,
  TASK_STATUS_FAILURE,
  TASK_STATUS_REQUEST,
  TASK_STATUS_SUCCESS,
  GET_P_O_REQUEST,
  GET_P_O_SUCCESS,
  GET_P_O_FAILURE,
  SMARTNET_VALIDATE_REQUEST,
  SMARTNET_VALIDATE_SUCCESS,
  SMARTNET_VALIDATE_FAILURE,
  GET_P_O_ALL_REQUEST,
  GET_P_O_ALL_FAILURE,
  GET_P_O_ALL_SUCCESS,
  FETCH_ALL_DEVICES_REQUEST,
  FETCH_ALL_DEVICES_SUCCESS,
  FETCH_ALL_DEVICES_FAILURE,
  LAST_VISITED_ASSET_SITE
} from '../actions/inventory';

const initialState: IInventoryStore = {
  devices: null,
  manufacturers: null,
  contractStatuses: null,
  sites: null,
  error: null,
  device: null,
  xlsxData: null,
  site: null,
  deviceSuccess: null,
  types: null,
  countries: null,
  isFetching: false,
  isFetchingDevice: false,
  isDeviceFetching: false,
  isCountriesFetching: false,
  isPostingBatch: false,
  fetchingXlsx: false,
  dateFormats: null,
  xlsxFile: null,
  availableAssociationList: null,
  existingAssociationList: null,
  deviceCategoryList: null,
  providerIntegration: null,
  statusList: [],
  purchaseOrders: [],
  purchaseOrdersAll: [],
  purchaseOrderCorrection: null,
  isFetchingAllDevice: false,
  allDevices: null,
  lastVisitedSite: "/inventory-management"
};

export default function customer(
  state: IInventoryStore = initialState,
  action: any
): IInventoryStore {
  switch (action.type) {
    case FETCH_DEVICES_REQUEST:
      return Object.assign({}, state, {
        isFetchingDevice: true,
      });
    case FETCH_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        devices: action.response,
        isFetchingDevice: false,
      });

    case FETCH_DEVICES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingDevice: false,
      });
    case FETCH_ALL_DEVICES_REQUEST:
      return Object.assign({}, state, {
        isFetchingAllDevice: true,
      });
    case FETCH_ALL_DEVICES_SUCCESS:
      return Object.assign({}, state, {
        allDevices: action.response,
        isFetchingAllDevice: false,
      });

    case FETCH_ALL_DEVICES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingAllDevice: false,
      });
    case FETCH_COUNTRIES_REQUEST:
      return Object.assign({}, state, {
        isCountriesFetching: true,
      });
    case FETCH_COUNTRIES_SUCCESS:
      return Object.assign({}, state, {
        countries: action.response,
        isCountriesFetching: false,
      });

    case FETCH_COUNTRIES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isCountriesFetching: false,
      });

    case FETCH_MANUFACTURERS_SUCCESS:
      return Object.assign({}, state, {
        manufacturers: action.response,
      });

    case FETCH_SITES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_SITES_SUCCESS:
      return Object.assign({}, state, {
        sites: action.response,
        isFetching: false,
      });
    case FETCH_SITES_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });
    case FETCH_TYPES_SUCCESS:
      return Object.assign({}, state, {
        types: action.response,
      });

    case FETCH_CONTRACT_STATUSES_SUCCESS:
      return Object.assign({}, state, {
        contractStatuses: action.response,
      });

    case FETCH_SINGLE_DEVICE_REQUEST:
      return Object.assign({}, state, {
        isDeviceFetching: true,
      });

    case FETCH_SINGLE_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        device: action.response,
        isDeviceFetching: false,
      });

    case FETCH_SINGLE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isDeviceFetching: false,
      });

    case FETCH_STATUSES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_STATUSES_SUCCESS:
      return Object.assign({}, state, {
        statusList: action.response,
        isFetching: false,
      });

    case FETCH_STATUSES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    case EDIT_SINGLE_DEVICE_SUCCESS:
      return Object.assign({}, state, {
        device: action.response,
      });

    case EDIT_SINGLE_DEVICE_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
      });

    case ADD_SITE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case ADD_SITE_SUCCESS:
      return Object.assign({}, state, {
        site: action.response,
        isFetching: false,
      });

    case ADD_SITE_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
        isFetching: false,
      });

    case SET_XLSX_DATA:
      return Object.assign({}, state, {
        xlsxData: action.data,
        xlsxFile: action.file,
      });

    case BATCH_CREATE_REQUEST:
      return Object.assign({}, state, {
        isPostingBatch: true,
      });

    case BATCH_CREATE_SUCCESS:
      return Object.assign({}, state, {
        //deviceSuccess: action.response,
        isPostingBatch: false,
      });

    case BATCH_CREATE_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
        isPostingBatch: false,
      });
    case SMARTNET_VALIDATE_REQUEST:
      return Object.assign({}, state, {
        isPostingBatch: true,
      });

    case SMARTNET_VALIDATE_SUCCESS:
      return Object.assign({}, state, {
        purchaseOrderCorrection: action.response,
        isPostingBatch: false,
      });

    case SMARTNET_VALIDATE_FAILURE:
      return Object.assign({}, state, {
        purchaseOrderCorrection: action.errorList,
        isPostingBatch: false,
      });
    case GET_P_O_REQUEST:
      return Object.assign({}, state, {
        isPostingBatch: true,
      });

    case GET_P_O_SUCCESS:
      return Object.assign({}, state, {
        purchaseOrders: action.response,
        isPostingBatch: false,
      });

    case GET_P_O_FAILURE:
      return Object.assign({}, state, {
        isPostingBatch: false,
        purchaseOrders: [],
      });
      case GET_P_O_ALL_REQUEST:
        return Object.assign({}, state, {
          isPostingBatch: true,
        });
  
      case GET_P_O_ALL_SUCCESS:
        return Object.assign({}, state, {
          purchaseOrdersAll: action.response,
          isPostingBatch: false,
        });
  
      case GET_P_O_ALL_FAILURE:
        return Object.assign({}, state, {
          isPostingBatch: false,
          purchaseOrdersAll: [],
        });
    case TASK_STATUS_REQUEST:
      return Object.assign({}, state, {
        isPostingBatch: true,
      });

    case TASK_STATUS_SUCCESS:
      return Object.assign({}, state, {
        deviceSuccess: action.response.result,
        isPostingBatch:
          action.response && action.response.status === 'PENDING'
            ? true
            : false,
      });

    case TASK_STATUS_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
        isPostingBatch: false,
      });

    case FETCH_XLSX_REQUEST:
      return Object.assign({}, state, {
        fetchingXlsx: true,
      });

    case FETCH_XLSX__SUCCESS:
      return Object.assign({}, state, {
        deviceSuccess: action.response,
        fetchingXlsx: false,
      });

    case BATCH_EXPORT_SUCCESS:
      const url = window.URL.createObjectURL(new Blob([action.response]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'Devices.csv');
      document.body.appendChild(link);
      link.click();

      return Object.assign({}, state, {
        error: 'some error',
      });

    case FETCH_DATE_FORMAT_SUCCESS:
      return Object.assign({}, state, {
        dateFormats: action.response,
      });

    case FETCH_AVAILABLE_DEVICE_ASSOCIATION_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_AVAILABLE_DEVICE_ASSOCIATION_SUCCESS:
      return Object.assign({}, state, {
        availableAssociationList: action.response,
        isFetching: false,
      });
    case FETCH_AVAILABLE_DEVICE_ASSOCIATION_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case FETCH_EXISTING_DEVICE_ASSOCIATION_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_EXISTING_DEVICE_ASSOCIATION_SUCCESS:
      return Object.assign({}, state, {
        existingAssociationList: action.response,
        isFetching: false,
      });
    case FETCH_EXISTING_DEVICE_ASSOCIATION_FAILURE:
      return Object.assign({}, state, {
        isFetching: false,
      });
    case FETCH_DEVICE_CATEGORIES_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_DEVICE_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        deviceCategoryList: action.response,
        isFetching: false,
      });

    case FETCH_DEVICE_CATEGORIES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    case FETCH_P_INTEGRATIONS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_P_INTEGRATIONS_SUCCESS:
      return Object.assign({}, state, {
        providerIntegration: action.response,
        isFetching: false,
      });

    case FETCH_P_INTEGRATIONS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    case LAST_VISITED_ASSET_SITE:
      return Object.assign({}, state, {
        lastVisitedSite: action.response,
      });
    default:
      return state;
  }
}
