import {
  FETCH_QUICKBASE_WH_REQUEST,
  FETCH_QUICKBASE_WH_SUCCESS,
  FETCH_QUICKBASE_WH_FAILURE,
  FETCH_QUICKBASE_STATUS_REQUEST,
  FETCH_QUICKBASE_STATUS_SUCCESS,
  FETCH_QUICKBASE_STATUS_FAILURE,
} from "../actions/virtualWarehouse";

const initialState: IVRWarehouseStore = {
  error: "",
  isFetchingStatus: false,
  quickbaseStatus: [],
  isFetchingWarehouse: false,
  quickbaseWareHouses: [],
};

export default function providerAdminUser(
  state: IVRWarehouseStore = initialState,
  action: any
): IVRWarehouseStore {
  switch (action.type) {
    case FETCH_QUICKBASE_STATUS_REQUEST:
      return Object.assign({}, state, {
        isFetchingStatus: true,
      });

    case FETCH_QUICKBASE_STATUS_SUCCESS:
      return Object.assign({}, state, {
        quickbaseStatus: action.response,
        isFetchingStatus: false,
      });

    case FETCH_QUICKBASE_STATUS_FAILURE:
      return Object.assign({}, state, {
        quickbaseStatus: [],
        error: "Error fetching Quickbase statuses",
        isFetchingStatus: false,
      });

    case FETCH_QUICKBASE_WH_REQUEST:
      return Object.assign({}, state, {
        isFetchingWarehouse: true,
      });

    case FETCH_QUICKBASE_WH_SUCCESS:
      return Object.assign({}, state, {
        quickbaseWareHouses: action.response,
        isFetchingWarehouse: false,
      });

    case FETCH_QUICKBASE_WH_FAILURE:
      return Object.assign({}, state, {
        quickbaseWareHouses: [],
        error: "Error fetching Quickbase statuses",
        isFetchingWarehouse: false,
      });

    default:
      return state;
  }
}
