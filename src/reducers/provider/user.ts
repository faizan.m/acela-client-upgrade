import {
  FETCH_PROVIDER_USERS_ALL_FAILURE,
  FETCH_PROVIDER_USERS_ALL_REQUEST,
  FETCH_PROVIDER_USERS_ALL_SUCCESS,
  FETCH_PROVIDER_USERS_FAILURE,
  FETCH_PROVIDER_USERS_REQUEST,
  FETCH_PROVIDER_USERS_SUCCESS,
  PROVIDER_USER_CREATE_FAILURE,
  PROVIDER_USER_CREATE_SUCCESS,
  PROVIDER_USER_DELETE_FAILURE,
  PROVIDER_USER_DELETE_REQUEST,
  PROVIDER_USER_DELETE_SUCCESS,
  PROVIDER_USER_EDIT_FAILURE,
  PROVIDER_USER_EDIT_SUCCESS,
} from '../../actions/provider/user';

const initialState: IProviderUserStore = {
  providerUsers: null,
  providerUsersAll: null,
  currentProviderUser: null,
  isFetchingUsers: false,
  isAction: false,
};

export default function customer(
  state: IProviderUserStore = initialState,
  action: any
): IProviderUserStore {
  switch (action.type) {
    case FETCH_PROVIDER_USERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingUsers: true,
      });

    case FETCH_PROVIDER_USERS_SUCCESS:
      return Object.assign({}, state, {
        providerUsers: action.response,
        isFetchingUsers: false,
      });

    case FETCH_PROVIDER_USERS_FAILURE:
      return Object.assign({}, state, {
        isFetchingUsers: false,
      });

    case FETCH_PROVIDER_USERS_ALL_REQUEST:
      return Object.assign({}, state, {
        isFetchingUsers: true,
      });

    case FETCH_PROVIDER_USERS_ALL_SUCCESS:
      return Object.assign({}, state, {
        providerUsersAll: action.response,
        isFetchingUsers: false,
      });

    case FETCH_PROVIDER_USERS_ALL_FAILURE:
      return Object.assign({}, state, {
        isFetchingUsers: false,
      });
    case PROVIDER_USER_CREATE_SUCCESS:
      return Object.assign({}, state, {
        currentProviderUser: action.response,
      });

    case PROVIDER_USER_EDIT_SUCCESS:
      return Object.assign({}, state, {
        currentProviderUser: action.response,
      });

    case PROVIDER_USER_EDIT_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
      });

    case PROVIDER_USER_CREATE_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
      });

    case PROVIDER_USER_DELETE_REQUEST:
      return Object.assign({}, state, {
        isAction: true,
      });

    case PROVIDER_USER_DELETE_SUCCESS:
      return Object.assign({}, state, {
        isAction: false,
      });

    case PROVIDER_USER_DELETE_FAILURE:
      return Object.assign({}, state, {
        isAction: false,
      });
    default:
      return state;
  }
}
