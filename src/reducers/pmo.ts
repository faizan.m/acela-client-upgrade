import {
  LAST_VISITED_PMO_SITE,
  ADDITIONAL_SETTING_REQUEST,
  ADDITIONAL_SETTING_SUCCESS,
  ADDITIONAL_SETTING_FAILURE,
  SAVE_PMO_DASHBOARD_FILTERS,
  SAVE_PROJECT_DETAILS_FILTER,
  PROJECT_TICKETS_REQUEST,
  PROJECT_TICKETS_SUCCESS,
  PROJECT_TICKETS_FAILURE,
  FETCH_MEETING_MAIL_TEMPLATE_SETTING_REQUEST,
  FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS,
  FETCH_MEETING_MAIL_TEMPLATE_SETTING_FAILURE,
} from "../actions/pmo";

const initialState: IPMOStore = {
  meetingTemplates: [],
  projectTickets: [],
  lastVisitedSite: "/ProjectManagement",
  pmoDashboardFilters: null,
  projectDetailsFilters: null,
  additionalSetting: null,
  isFetching: false,
};

export default function customer(state: any = initialState, action: any): any {
  switch (action.type) {
    case ADDITIONAL_SETTING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case ADDITIONAL_SETTING_SUCCESS:
      return Object.assign({}, state, {
        additionalSetting: action.response,
        isFetching: false,
      });

    case ADDITIONAL_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case PROJECT_TICKETS_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case PROJECT_TICKETS_SUCCESS:
      return Object.assign({}, state, {
        projectTickets: action.response.map((s) => ({
          value: s.id,
          label: `${s.id} - ${s.summary}`,
        })),
        isFetching: false,
      });

    case PROJECT_TICKETS_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    case FETCH_MEETING_MAIL_TEMPLATE_SETTING_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_MEETING_MAIL_TEMPLATE_SETTING_SUCCESS:
      return Object.assign({}, state, {
        meetingTemplates: action.response.results,
        isFetching: false,
      });

    case FETCH_MEETING_MAIL_TEMPLATE_SETTING_FAILURE:
      return Object.assign({}, state, {
        error: "Error fetching meeting templates",
        isFetching: false,
      });

    case SAVE_PROJECT_DETAILS_FILTER:
      return Object.assign({}, state, {
        projectDetailsFilters: action.response,
      });

    case SAVE_PMO_DASHBOARD_FILTERS:
      return Object.assign({}, state, {
        pmoDashboardFilters: action.response,
      });

    case LAST_VISITED_PMO_SITE:
      return Object.assign({}, state, {
        lastVisitedSite: action.response,
      });

    default:
      return state;
  }
}
