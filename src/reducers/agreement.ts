import {
  CREATE_TEMPLATE_FAILURE,
  CREATE_TEMPLATE_REQUEST,
  CREATE_TEMPLATE_SUCCESS,
  FETCH_AGREEMENT_FAILURE,
  FETCH_AGREEMENT_REQUEST,
  FETCH_AGREEMENT_SUCCESS,
  FETCH_TEMPLATES_FAILURE,
  FETCH_TEMPLATES_REQUEST,
  FETCH_TEMPLATES_SUCCESS,
} from "../actions/agreement";
import {
  FETCH_TEMPLATE_FAILURE,
  FETCH_TEMPLATE_REQUEST,
  FETCH_TEMPLATE_SUCCESS,
} from "../actions/sow";

const initialState: IAgreementStore = {
  template: null,
  templates: null,
  isFetching: false,
  isFetchingTemplate: false,
  isFetchingTemplates: false,
  agreementList: null,
  isFetchingAgreements: false,
};

export default function provider(
  state: IAgreementStore = initialState,
  action: any
): IAgreementStore {
  switch (action.type) {
    case FETCH_AGREEMENT_REQUEST:
      return Object.assign({}, state, {
        isFetchingAgreements: true,
      });

    case FETCH_AGREEMENT_SUCCESS:
      return Object.assign({}, state, {
        agreementList: action.response,
        isFetchingAgreements: false,
      });

    case FETCH_AGREEMENT_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingAgreements: false,
      });
    case FETCH_TEMPLATES_REQUEST:
      return Object.assign({}, state, {
        isFetchingTemplates: true,
      });

    case FETCH_TEMPLATES_SUCCESS:
      return Object.assign({}, state, {
        templates: action.response,
        isFetchingTemplates: false,
      });

    case FETCH_TEMPLATES_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingTemplates: false,
      });
    case FETCH_TEMPLATE_REQUEST:
      return Object.assign({}, state, {
        isFetchingTemplate: true,
      });

    case FETCH_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        template: action.response,
        isFetchingTemplate: false,
      });

    case FETCH_TEMPLATE_FAILURE:
      return Object.assign({}, state, {
        isFetchingCategory: true,
      });

    case CREATE_TEMPLATE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case CREATE_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case CREATE_TEMPLATE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });

    default:
      return state;
  }
}
