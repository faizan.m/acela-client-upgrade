import {
  FETCH_SUPERUSERS_FAILURE,
  FETCH_SUPERUSERS_REQUEST,
  FETCH_SUPERUSERS_SUCCESS,
  SUPERUSER_CREATE_FAILURE,
  SUPERUSER_CREATE_SUCCESS,
  SUPERUSER_EDIT_FAILURE,
  SUPERUSER_EDIT_SUCCESS,
} from '../actions/superuser';

const initialState: ISuperUserStore = {
  superusers: null,
  currentSuperUser: null,
  isFetchingUsers: false,
};

export default function customer(
  state: ISuperUserStore = initialState,
  action: any
): ISuperUserStore {
  switch (action.type) {
    case FETCH_SUPERUSERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingUsers: true,
      });

    case FETCH_SUPERUSERS_SUCCESS:
      return Object.assign({}, state, {
        superusers: action.response,
        isFetchingUsers: false,
      });

    case FETCH_SUPERUSERS_FAILURE:
      return Object.assign({}, state, {
        isFetchingUsers: false,
      });

    case SUPERUSER_CREATE_SUCCESS:
      return Object.assign({}, state, {
        currentSuperUser: action.response,
      });

    case SUPERUSER_EDIT_SUCCESS:
      return Object.assign({}, state, {
        currentSuperUser: action.response,
      });

    case SUPERUSER_EDIT_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
      });

    case SUPERUSER_CREATE_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
      });

    default:
      return state;
  }
}
