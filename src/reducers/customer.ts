import {
  CREATE_CUSTOMERS_USER_FAILURE,
  CREATE_CUSTOMERS_USER_SUCCESS,
  EDIT_CUSTOMER_FAILURE,
  EDIT_CUSTOMER_SUCCESS,
  EDIT_CUSTOMER_USER_FAILURE,
  EDIT_CUSTOMER_USER_SUCCESS,
  FETCH_CUSTOMER_USERS_FAILURE,
  FETCH_CUSTOMER_USERS_REQUEST,
  FETCH_CUSTOMER_USERS_SUCCESS,
  FETCH_CUSTOMERS_FAILURE,
  FETCH_CUSTOMERS_REQUEST,
  FETCH_CUSTOMERS_SHORT_FAILURE,
  FETCH_CUSTOMERS_SHORT_SUCCESS,
  FETCH_CUSTOMERS_SUCCESS,
  FETCH_SINGLE_CUSTOMER_FAILURE,
  FETCH_SINGLE_CUSTOMER_REQUEST,
  FETCH_SINGLE_CUSTOMER_SUCCESS,
  SET_CUSTOMER_ID,
  FETCH_CUSTOMERS_SHORT_REQUEST,
  EDIT_CUSTOMER_USER_REQUEST,
  LAST_VISITED_CLIENT_360_TAB
} from '../actions/customer';
const initialState: ICustomerStore = {
  customers: null,
  customer: null,
  error: null,
  users: null,
  user: null,
  customersShort: null,
  customersShortFetching: false,
  customerId: null,
  isFetchingCustomers: false,
  isFetchingCustomer: false,
  isFetchingUsers: false,
  lastVisitedTab: "Documents"
};

export default function customer(
  state: ICustomerStore = initialState,
  action: any
): ICustomerStore {
  switch (action.type) {
    case FETCH_CUSTOMERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingCustomers: true,
      });

    case FETCH_CUSTOMERS_SUCCESS:
      return Object.assign({}, state, {
        customers: action.response,
        isFetchingCustomers: false,
      });

    case FETCH_CUSTOMERS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCustomers: false,
      });
    case FETCH_CUSTOMERS_SHORT_REQUEST:
      return Object.assign({}, state, {
        customersShortFetching: true,
      });
    case FETCH_CUSTOMERS_SHORT_SUCCESS:
      return Object.assign({}, state, {
        customersShortFetching: false,
        customersShort: action.response,
      });

    case FETCH_CUSTOMERS_SHORT_FAILURE:
      return Object.assign({}, state, {
        customersShortFetching: false,
        error: 'some error',
      });

    case FETCH_CUSTOMER_USERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingUsers: true,
      });

    case FETCH_CUSTOMER_USERS_SUCCESS:
      return Object.assign({}, state, {
        users: action.response,
        isFetchingUsers: false,
      });

    case FETCH_CUSTOMER_USERS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingUsers: false,
      });

    case FETCH_SINGLE_CUSTOMER_REQUEST:
      return Object.assign({}, state, {
        isFetchingCustomer: true,
      });

    case FETCH_SINGLE_CUSTOMER_SUCCESS:
      return Object.assign({}, state, {
        customer: action.response,
        isFetchingCustomer: false,
      });

    case FETCH_SINGLE_CUSTOMER_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCustomer: false,
      });

    case EDIT_CUSTOMER_USER_REQUEST:
      return Object.assign({}, state, {
        isFetchingCustomer: true,
      });
      
    case EDIT_CUSTOMER_USER_SUCCESS:
      return Object.assign({}, state, {
        user: action.response,
        isFetchingCustomer: false,
      });

    case EDIT_CUSTOMER_USER_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
        isFetchingCustomer: false,
      });

    case EDIT_CUSTOMER_SUCCESS:
      return Object.assign({}, state, {
        customer: action.response,
      });

    case EDIT_CUSTOMER_FAILURE:
      return Object.assign({}, state, {
        errorList: action.errorList,
      });

    case SET_CUSTOMER_ID:
      return Object.assign({}, state, {
        customerId: action.id,
      });

    case CREATE_CUSTOMERS_USER_SUCCESS:
      return Object.assign({}, state, {
        user: action.response,
      });

    case CREATE_CUSTOMERS_USER_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
      });

    case LAST_VISITED_CLIENT_360_TAB:
      return Object.assign({}, state, {
        lastVisitedTab: action.response,
      });

    default:
      return state;
  }
}
