import {
  CREATE_CR_FAILURE,
  CREATE_CR_REQUEST,
  CREATE_CR_SUCCESS,
  CREATE_SCL_FAILURE,
  CREATE_SCL_REQUEST,
  CREATE_SCL_SUCCESS,
  CREATE_SOW_FAILURE,
  CREATE_SOW_REQUEST,
  CREATE_SOW_SUCCESS,
  CREATE_TEMPLATE_FAILURE,
  CREATE_TEMPLATE_REQUEST,
  CREATE_TEMPLATE_SUCCESS,
  EDIT_SCL_FAILURE,
  EDIT_SCL_REQUEST,
  EDIT_SCL_SUCCESS,
  EDIT_SOW_FAILURE,
  EDIT_SOW_REQUEST,
  EDIT_SOW_SUCCESS,
  EDIT_TEMPLATE_FAILURE,
  EDIT_TEMPLATE_REQUEST,
  EDIT_TEMPLATE_SUCCESS,
  FETCH_BASE_TEMPLATE_FAILURE,
  FETCH_BASE_TEMPLATE_REQUEST,
  FETCH_BASE_TEMPLATE_SUCCESS,
  FETCH_BUSINESS_UNITS_FAILURE,
  FETCH_BUSINESS_UNITS_REQUEST,
  FETCH_BUSINESS_UNITS_SUCCESS,
  FETCH_CATEGORIES_FAILURE,
  FETCH_CATEGORIES_REQUEST,
  FETCH_CATEGORIES_SUCCESS,
  FETCH_DOC_TYPE_FAILURE,
  FETCH_DOC_TYPE_REQUEST,
  FETCH_DOC_TYPE_SUCCESS,
  FETCH_Q_STAGE_FAILURE,
  FETCH_Q_STAGE_REQUEST,
  FETCH_Q_STAGE_SUCCESS,
  FETCH_Q_TYPES_FAILURE,
  FETCH_Q_TYPES_REQUEST,
  FETCH_Q_TYPES_SUCCESS,
  FETCH_SCL_FAILURE,
  FETCH_SCL_REQUEST,
  FETCH_SCL_SUCCESS,
  FETCH_SCLIST_FAILURE,
  FETCH_SCLIST_REQUEST,
  FETCH_SCLIST_SHORT_FAILURE,
  FETCH_SCLIST_SHORT_REQUEST,
  FETCH_SCLIST_SHORT_SUCCESS,
  FETCH_SCLIST_SUCCESS,
  FETCH_SOW_FAILURE,
  FETCH_SOW_REQUEST,
  FETCH_SOW_SUCCESS,
  FETCH_SOWS_FAILURE,
  FETCH_SOWS_REQUEST,
  FETCH_SOWS_SUCCESS,
  FETCH_TEMPLATE_FAILURE,
  FETCH_TEMPLATE_REQUEST,
  FETCH_TEMPLATE_SUCCESS,
  FETCH_TEMPLATES_FAILURE,
  FETCH_TEMPLATES_REQUEST,
  FETCH_TEMPLATES_SUCCESS,
  GET_QUOTES_FAILURE,
  GET_QUOTES_REQUEST,
  GET_QUOTES_SUCCESS,
  GET_SCC_FAILURE,
  GET_SCC_REQUEST,
  GET_SCC_SUCCESS,
  UPLOAD_IMAGE_CR_FAILURE,
  UPLOAD_IMAGE_CR_REQUEST,
  UPLOAD_IMAGE_CR_SUCCESS,
  GET_SOW_HISTORY_REQUEST,
  GET_SOW_HISTORY_SUCCESS,
  GET_SOW_HISTORY_FAILURE,
  GET_SOW_CREATE_HTR_REQUEST,
  GET_SOW_CREATE_HTR_SUCCESS,
  GET_SOW_CREATE_HTR_FAILURE,
  GET_SOW_DLT_HTR_REQUEST,
  GET_SOW_DLT_HTR_SUCCESS,
  GET_SOW_DLT_HTR_FAILURE,
  sowActionTypes,
  GET_TABLE_FAILURE,
  GET_TABLE_REQUEST,
  GET_TABLE_SUCCESS,
  GET_CONTACT_TYPES_REQUEST,
  GET_CONTACT_TYPES_SUCCESS,
  GET_CONTACT_TYPES_FAILURE,
  GET_VENDORS_REQUEST,
  GET_VENDORS_SUCCESS,
  GET_VENDORS_FAILURE,
  GET_VENDOR_MAPPING_REQUEST,
  GET_VENDOR_MAPPING_SUCCESS,
  GET_VENDOR_MAPPING_FAILURE,
  GET_STATUSES_REQUEST,
  GET_STATUSES_SUCCESS,
  GET_STATUSES_FAILURE,
} from '../actions/sow';

const initialState: ISOWStore = {
  template: null,
  templates: null,
  categoryList: null,
  contactTypes: [],
  vendorOptions: [],
  isFetching: false,
  isFetchingTemplate: false,
  isFetchingTemplates: false,
  isFetchingVendors: false,
  isFetchingCategory: false,
  baseTemplates: null,
  sow: null,
  sowList: null,
  isFetchingSow: false,
  isFetchingSowList: false,
  qStageList: null,
  qTypeList: null,
  qStatusList: null,
  isFetchingQTypeList: false,
  isFetchingQStageList: false,
  isFetchingQStatusList: false,
  quote: null,
  isFetchingSingleQuote: false,
  createtingCR: false,
  businessUnits: null,
  fetchingBusinessUnits: false,
  uploadingImages: false,
  isFetchingSC: false,
  isFetchingSCList: false,
  serviceCatalogList: null,
  serviceCatalogShortList: null,
  serviceCatalog: null,
  serviceCatalogCategories: null,
  documentTypes: null,
  sowHistory: [],
  isFetchingHistory: false,
  sowCreateHistory: [],
  isFetchingCreateHistory: false,
  sowDeleteHistory: [],
  isFetchingDeleteHistory: false,
  NotesList: undefined,
  SOWMetricsTable: [],
  vendorMapping: [],
  isFetchingVendorMapping: false
};

export default function provider(
  state: ISOWStore = initialState,
  action: any
): ISOWStore {
  switch (action.type) {
    case FETCH_BASE_TEMPLATE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_BASE_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        baseTemplates: action.response,
        isFetching: false,
      });

    case FETCH_BASE_TEMPLATE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    case FETCH_DOC_TYPE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case FETCH_DOC_TYPE_SUCCESS:
      return Object.assign({}, state, {
        documentTypes: action.response,
        isFetching: false,
      });

    case FETCH_DOC_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    case FETCH_TEMPLATES_REQUEST:
      return Object.assign({}, state, {
        isFetchingTemplates: true,
      });

    case FETCH_TEMPLATES_SUCCESS:
      return Object.assign({}, state, {
        templates: action.response,
        isFetchingTemplates: false,
      });

    case FETCH_TEMPLATES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingTemplates: false,
      });
    case FETCH_TEMPLATE_REQUEST:
      return Object.assign({}, state, {
        isFetchingTemplate: true,
      });

    case FETCH_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        template: action.response,
        isFetchingTemplate: false,
      });

    case FETCH_TEMPLATE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingTemplate: false,
      });
    case FETCH_CATEGORIES_REQUEST:
      return Object.assign({}, state, {
        isFetchingCategory: true,
      });

    case FETCH_CATEGORIES_SUCCESS:
      return Object.assign({}, state, {
        categoryList: action.response,
        isFetchingCategory: false,
      });

    case FETCH_CATEGORIES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCategory: false,
      });

    case GET_CONTACT_TYPES_REQUEST:
      return state;

    case GET_CONTACT_TYPES_SUCCESS:
      return Object.assign({}, state, {
        contactTypes: action.response.map(el => ({
          value: el.id,
          label: el.description
        })),
      });

    case GET_CONTACT_TYPES_FAILURE:
      return Object.assign({}, state, {
        error: 'Error fetching contact types',
      });

    case GET_VENDORS_REQUEST:
      return Object.assign({}, state, {
        isFetchingVendors: true,
      });

    case GET_VENDORS_SUCCESS:
      return Object.assign({}, state, {
        vendorOptions: action.response.map(el => ({
          value: el.id,
          label: el.name
        })),
        isFetchingVendors: false,
      });

    case GET_VENDORS_FAILURE:
      return Object.assign({}, state, {
        error: 'Error fetching vendors',
        isFetchingVendors: false,
      });

    case GET_VENDOR_MAPPING_REQUEST:
      return Object.assign({}, state, {
        isFetchingVendorMapping: true,
      });

    case GET_VENDOR_MAPPING_SUCCESS:
      return Object.assign({}, state, {
        vendorMapping: action.response,
        isFetchingVendorMapping: false,
      });

    case GET_VENDOR_MAPPING_FAILURE:
      return Object.assign({}, state, {
        error: 'Error fetching vendor mapping',
        isFetchingVendorMapping: false,
      });

    case FETCH_SOWS_REQUEST:
      return Object.assign({}, state, {
        isFetchingSowList: true,
      });

    case FETCH_SOWS_SUCCESS:
      return Object.assign({}, state, {
        sowList: action.response,
        isFetchingSowList: false,
      });

    case FETCH_SOWS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSowList: false,
      });
    case FETCH_SOW_REQUEST:
      return Object.assign({}, state, {
        isFetchingSow: true,
        quote: null,
      });

    case FETCH_SOW_SUCCESS:
      return Object.assign({}, state, {
        sow: action.response,
        isFetchingSow: false,
      });

    case FETCH_SOW_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSow: false,
      });
    case GET_SOW_HISTORY_REQUEST:
      return Object.assign({}, state, {
        isFetchingHistory: true,
        sowHistory: [],
      });

    case GET_SOW_HISTORY_SUCCESS:
      return Object.assign({}, state, {
        sowHistory: action.response,
        isFetchingHistory: false,
      });

    case GET_SOW_HISTORY_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingHistory: false,
      });

    case GET_SOW_CREATE_HTR_REQUEST:
      return Object.assign({}, state, {
        isFetchingCreateHistory: true,
      });

    case GET_SOW_CREATE_HTR_SUCCESS:
      return Object.assign({}, state, {
        sowCreateHistory: action.response,
        isFetchingCreateHistory: false,
      });

    case GET_SOW_CREATE_HTR_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingCreateHistory: false,
      });
    case GET_SOW_HISTORY_REQUEST:
      return Object.assign({}, state, {
        isFetchingDeleteHistory: true,
      });

    case GET_SOW_HISTORY_SUCCESS:
      return Object.assign({}, state, {
        sowDeleteHistory: action.response,
        isFetchingDeleteHistory: false,
      });

    case GET_SOW_HISTORY_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingDeleteHistory: false,
      });

    case GET_SOW_DLT_HTR_REQUEST:
      return Object.assign({}, state, {
        isFetchingDeleteHistory: true,
      });

    case GET_SOW_DLT_HTR_SUCCESS:
      return Object.assign({}, state, {
        sowDeleteHistory: action.response,
        isFetchingDeleteHistory: false,
      });

    case GET_SOW_DLT_HTR_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingDeleteHistory: false,
      });

    case CREATE_SOW_REQUEST:
    case EDIT_SOW_REQUEST:
      return Object.assign({}, state, {
        isFetchingSow: true,
      });

    case CREATE_SOW_SUCCESS:
    case EDIT_SOW_SUCCESS:
      return Object.assign({}, state, {
        isFetchingSow: false,
      });

    case CREATE_SOW_FAILURE:
    case EDIT_SOW_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSow: false,
      });

    case CREATE_TEMPLATE_REQUEST:
    case EDIT_TEMPLATE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case CREATE_TEMPLATE_SUCCESS:
    case EDIT_TEMPLATE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case CREATE_TEMPLATE_FAILURE:
    case EDIT_TEMPLATE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    case FETCH_Q_TYPES_REQUEST:
      return Object.assign({}, state, {
        isFetchingQTypeList: true,
      });

    case FETCH_Q_TYPES_SUCCESS:
      return Object.assign({}, state, {
        qTypeList: action.response,
        isFetchingQTypeList: false,
      });

    case FETCH_Q_TYPES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingQTypeList: false,
      });

    case FETCH_Q_STAGE_REQUEST:
      return Object.assign({}, state, {
        isFetchingQStageList: true,
      });

    case FETCH_Q_STAGE_SUCCESS:
      return Object.assign({}, state, {
        qStageList: action.response,
        isFetchingQStageList: false,
      });

    case FETCH_Q_STAGE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingQStageList: false,
      });

    case GET_STATUSES_REQUEST:
      return Object.assign({}, state, {
        isFetchingQStatusList: true,
      });

    case GET_STATUSES_SUCCESS:
      return Object.assign({}, state, {
        qStatusList: action.response,
        isFetchingQStatusList: false,
      });

    case GET_STATUSES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingQStatusList: false,
      });

    case GET_QUOTES_REQUEST:
      return Object.assign({}, state, {
        isFetchingSingleQuote: true,
      });

    case GET_QUOTES_SUCCESS:
      return Object.assign({}, state, {
        quote: action.response,
        isFetchingSingleQuote: false,
      });

    case GET_QUOTES_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSingleQuote: false,
      });

    case CREATE_CR_REQUEST:
      return Object.assign({}, state, {
        createtingCR: true,
      });

    case CREATE_CR_SUCCESS:
      return Object.assign({}, state, {
        createtingCR: false,
      });

    case CREATE_CR_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        createtingCR: false,
      });

    case FETCH_BUSINESS_UNITS_REQUEST:
      return Object.assign({}, state, {
        fetchingBusinessUnits: true,
      });

    case FETCH_BUSINESS_UNITS_SUCCESS:
      return Object.assign({}, state, {
        businessUnits: action.response,
        fetchingBusinessUnits: false,
      });

    case FETCH_BUSINESS_UNITS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        fetchingBusinessUnits: false,
      });
    case UPLOAD_IMAGE_CR_REQUEST:
      return Object.assign({}, state, {
        uploadingImages: true,
      });

    case UPLOAD_IMAGE_CR_SUCCESS:
      return Object.assign({}, state, {
        uploadingImages: false,
      });

    case UPLOAD_IMAGE_CR_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        uploadingImages: false,
      });
    case CREATE_SCL_REQUEST:
    case EDIT_SCL_REQUEST:
      return Object.assign({}, state, {
        isFetchingSC: true,
      });

    case CREATE_SCL_SUCCESS:
    case EDIT_SCL_SUCCESS:
      return Object.assign({}, state, {
        isFetchingSC: false,
      });

    case CREATE_SCL_FAILURE:
    case EDIT_SCL_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSC: false,
      });

    case FETCH_SCLIST_REQUEST:
      return Object.assign({}, state, {
        isFetchingSCList: true,
      });

    case FETCH_SCLIST_SUCCESS:
      return Object.assign({}, state, {
        serviceCatalogList: action.response,
        isFetchingSCList: false,
      });

    case FETCH_SCLIST_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSCL: false,
      });

    case FETCH_SCLIST_SHORT_REQUEST:
      return Object.assign({}, state, {
        isFetchingSCList: true,
      });

    case FETCH_SCLIST_SHORT_SUCCESS:
      return Object.assign({}, state, {
        serviceCatalogShortList: action.response,
        isFetchingSCList: false,
      });

    case FETCH_SCLIST_SHORT_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSCL: false,
      });

    case FETCH_SCL_REQUEST:
      return Object.assign({}, state, {
        isFetchingSCL: true,
      });

    case FETCH_SCL_SUCCESS:
      return Object.assign({}, state, {
        serviceCatalog: action.response,
        isFetchingSCL: false,
      });

    case FETCH_SCL_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSCL: false,
      });
    case GET_SCC_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case GET_SCC_SUCCESS:
      return Object.assign({}, state, {
        serviceCatalogCategories: action.response,
        isFetching: false,
      });

    case GET_SCC_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
    case sowActionTypes.CREATE_NOTE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });

    case sowActionTypes.CREATE_NOTE_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
      });

    case sowActionTypes.CREATE_NOTE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });
      case sowActionTypes.GET_NOTES_REQUEST:
        return Object.assign({}, state, {
          isFetchingSCList: true,
        });
  
      case sowActionTypes.GET_NOTES_SUCCESS:
        return Object.assign({}, state, {
          NotesList: action.response,
          isFetchingSCList: false,
        });
  
      case sowActionTypes.GET_NOTES_FAILURE:
        return Object.assign({}, state, {
          error: 'some error',
          isFetchingSCList: false,
        });
          
    case GET_TABLE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
        SOWMetricsTable: [],
      });
    case GET_TABLE_SUCCESS:
      return Object.assign({}, state, {
        SOWMetricsTable: action.response,
        isFetching: false,
      });
    case GET_TABLE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetching: false,
      });

    default:
      return state;
  }
}
