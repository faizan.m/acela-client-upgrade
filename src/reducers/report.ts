import {
  FETCH_CONTRACT_STATUSES_FAILURE,
  FETCH_CONTRACT_STATUSES_REQUEST,
  FETCH_CONTRACT_STATUSES_SUCCESS,
  FETCH_SITES_FAILURE,
  FETCH_SITES_REQUEST,
  FETCH_SITES_SUCCESS,
  FETCH_TYPES_FAILURE,
  FETCH_TYPES_REQUEST,
  FETCH_TYPES_SUCCESS,
} from '../actions/inventory';
import {
  BATCH_REPORT_FAILURE,
  BATCH_REPORT_REQUEST,
  BATCH_REPORT_SUCCESS_CUSTOMER,
  BATCH_REPORT_SUCCESS_PROVIDER,
  CIRCUIT_REPORT_FAILURE,
  CIRCUIT_REPORT_REQUEST,
  CIRCUIT_REPORT_SUCCESS,
  CUSTOMER_ANALYSIS_REPORT_FAILURE,
  CUSTOMER_ANALYSIS_REPORT_REQUEST,
  CUSTOMER_ANALYSIS_REPORT_SUCCESS,
  CUSTOMER_ANALYSIS_RT_FAILURE,
  CUSTOMER_ANALYSIS_RT_REQUEST,
  CUSTOMER_ANALYSIS_RT_SUCCESS,
  FETCH_CIRCUIT_INFO_RT_FAILURE,
  FETCH_CIRCUIT_INFO_RT_REQUEST,
  FETCH_CIRCUIT_INFO_RT_SUCCESS,
  FETCH_REP_CATEGORIES_FAILURE,
  FETCH_REP_CATEGORIES_REQUEST,
  FETCH_REP_CATEGORIES_SUCCESS,
  FETCH_REPORT_TYPE_FAILURE,
  FETCH_REPORT_TYPE_REQUEST,
  FETCH_REPORT_TYPE_SUCCESS,
  FETCH_SHE_RE_FREQUENCY_FAILURE,
  FETCH_SHE_RE_FREQUENCY_REQUEST,
  FETCH_SHE_RE_FREQUENCY_SUCCESS,
  FETCH_SHE_RE_TYPE_FAILURE,
  FETCH_SHE_RE_TYPE_REQUEST,
  FETCH_SHE_RE_TYPE_SUCCESS,
  GET_REPORT_D_STATS_FAILURE,
  GET_REPORT_D_STATS_REQUEST,
  GET_REPORT_D_STATS_SUCCESS,
  GET_SH_REPORT_FAILURE,
  GET_SH_REPORT_REQUEST,
  GET_SH_REPORT_SUCCESS,
  POST_SH_REPORT_FAILURE,
  POST_SH_REPORT_REQUEST,
  POST_SH_REPORT_SUCCESS,
  GET_FINANCE_STATUS_REQUEST,
  GET_FINANCE_STATUS_SUCCESS,
  GET_FINANCE_STATUS_FAILURE,
} from '../actions/report';

const initialState: IReportStore = {
  devices: null,
  reportError: null,
  isFetching: false,
  reportTypes: null,
  cuircuitInfoReportTypes: null,
  reportCategories: null,
  scheduledReportByCat: null,
  reportFrequencies: null,
  isFetchingSH: false,
  scheduledReportList: null,
  customerAnalysisReportTypes: null,
  reportDownloadStats: null,
  financeStatuses: []
};

export default function customer(state: any = initialState, action: any): any {
  switch (action.type) {
    case FETCH_SITES_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case FETCH_SITES_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case FETCH_SITES_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case FETCH_TYPES_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case FETCH_TYPES_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case FETCH_TYPES_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case FETCH_REPORT_TYPE_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case FETCH_REPORT_TYPE_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        reportTypes: action.response,
      });
    }

    case FETCH_REPORT_TYPE_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }
    case FETCH_CIRCUIT_INFO_RT_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case FETCH_CIRCUIT_INFO_RT_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        cuircuitInfoReportTypes: action.response,
      });
    }

    case FETCH_CIRCUIT_INFO_RT_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case CUSTOMER_ANALYSIS_RT_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case CUSTOMER_ANALYSIS_RT_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        customerAnalysisReportTypes: action.response,
      });
    }

    case CUSTOMER_ANALYSIS_RT_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case GET_FINANCE_STATUS_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case GET_FINANCE_STATUS_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        financeStatuses: action.response.map((el) => ({
          value: el.id,
          label: el.status,
        })),
      });
    }

    case GET_FINANCE_STATUS_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case CUSTOMER_ANALYSIS_REPORT_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case CUSTOMER_ANALYSIS_REPORT_SUCCESS: {
      return Object.assign({}, state, {
        reportError: new Date().getTime(),
        isFetching: false,
      });
    }

    case CUSTOMER_ANALYSIS_REPORT_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case FETCH_CONTRACT_STATUSES_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case FETCH_CONTRACT_STATUSES_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case FETCH_CONTRACT_STATUSES_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }
    case BATCH_REPORT_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case BATCH_REPORT_SUCCESS_PROVIDER: {
      // if (action.response && action.response !== '') {
      //   const url = window.URL.createObjectURL(new Blob([action.response]));
      //   const link = document.createElement('a');
      //   link.href = url;
      //   link.setAttribute('download', 'Device-Report.csv');
      //   document.body.appendChild(link);
      //   link.click();
      //   return Object.assign({}, state, {
      //     isFetching: false,
      //   });
      // } else {
      return Object.assign({}, state, {
        reportError: new Date().getTime(),
        isFetching: false,
      });
      // }
    }

    case BATCH_REPORT_SUCCESS_CUSTOMER: {
      // if (action.response && action.response !== '') {
      //   const url = window.URL.createObjectURL(new Blob([action.response]));
      //   const link = document.createElement('a');
      //   link.href = url;
      //   link.setAttribute('download', 'Device-Report.pdf');
      //   document.body.appendChild(link);
      //   link.click();
      //   return Object.assign({}, state, {
      //     isFetching: false,
      //   });
      // } else {
      return Object.assign({}, state, {
        reportError: new Date().getTime(),
        isFetching: false,
      });
      // }
    }

    case BATCH_REPORT_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case CIRCUIT_REPORT_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    case CIRCUIT_REPORT_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }
    case CIRCUIT_REPORT_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case FETCH_REP_CATEGORIES_REQUEST: {
      return Object.assign({}, state, {
        isFetchingSH: true,
        scheduledReportByCat: null,
      });
    }

    case FETCH_REP_CATEGORIES_SUCCESS: {
      return Object.assign({}, state, {
        isFetchingSH: false,
        reportCategories: action.response,
      });
    }
    case FETCH_REP_CATEGORIES_FAILURE: {
      return Object.assign({}, state, {
        isFetchingSH: false,
      });
    }

    case FETCH_SHE_RE_TYPE_REQUEST: {
      return Object.assign({}, state, {
        isFetchingSH: true,
      });
    }

    case FETCH_SHE_RE_TYPE_SUCCESS: {
      return Object.assign({}, state, {
        isFetchingSH: false,
        scheduledReportByCat: action.response,
      });
    }
    case FETCH_SHE_RE_TYPE_FAILURE: {
      return Object.assign({}, state, {
        isFetchingSH: false,
      });
    }

    case FETCH_SHE_RE_FREQUENCY_REQUEST: {
      return Object.assign({}, state, {
        isFetchingSH: true,
      });
    }

    case FETCH_SHE_RE_FREQUENCY_SUCCESS: {
      return Object.assign({}, state, {
        isFetchingSH: false,
        reportFrequencies: action.response,
      });
    }
    case FETCH_SHE_RE_FREQUENCY_FAILURE: {
      return Object.assign({}, state, {
        isFetchingSH: false,
      });
    }

    case POST_SH_REPORT_REQUEST: {
      return Object.assign({}, state, {
        isFetchingSH: true,
      });
    }

    case POST_SH_REPORT_SUCCESS: {
      return Object.assign({}, state, {
        isFetchingSH: false,
      });
    }
    case POST_SH_REPORT_FAILURE: {
      return Object.assign({}, state, {
        isFetchingSH: false,
      });
    }

    case GET_SH_REPORT_REQUEST: {
      return Object.assign({}, state, {
        isFetchingSH: true,
      });
    }

    case GET_SH_REPORT_SUCCESS: {
      return Object.assign({}, state, {
        isFetchingSH: false,
        scheduledReportList: action.response,
      });
    }
    case GET_SH_REPORT_FAILURE: {
      return Object.assign({}, state, {
        isFetchingSH: false,
      });
    }
    case GET_REPORT_D_STATS_REQUEST: {
      return Object.assign({}, state, {
        isFetching: true,
      });
    }

    case GET_REPORT_D_STATS_SUCCESS: {
      return Object.assign({}, state, {
        isFetching: false,
        reportDownloadStats: action.response,
      });
    }
    case GET_REPORT_D_STATS_FAILURE: {
      return Object.assign({}, state, {
        isFetching: false,
      });
    }

    default:
      return state;
  }
}
