import {
  FETCH_ACTIVITY_FAILURE,
  FETCH_ACTIVITY_REQUEST,
  FETCH_ACTIVITY_SUCCESS,
  FETCH_LIFECYCLE_DASHBOARD_DATA_FAILURE,
  FETCH_LIFECYCLE_DASHBOARD_DATA_REQUEST,
  FETCH_LIFECYCLE_DASHBOARD_DATA_SUCCESS,
  FETCH_ORDER_DASHBOARD_DATA_FAILURE,
  FETCH_ORDER_DASHBOARD_DATA_REQUEST,
  FETCH_ORDER_DASHBOARD_DATA_SUCCESS,
  FETCH_QUOTE_LIST_FAILURE,
  FETCH_QUOTE_LIST_REQUEST,
  FETCH_QUOTE_LIST_SUCCESS,
  FETCH_SERVICE_DASHBOARD_DATA_FAILURE,
  FETCH_SERVICE_DASHBOARD_DATA_REQUEST,
  FETCH_SERVICE_DASHBOARD_DATA_SUCCESS,
  FETCH_SUPERUSER_DASHBOARD_FAILURE,
  FETCH_SUPERUSER_DASHBOARD_REQUEST,
  FETCH_SUPERUSER_DASHBOARD_SUCCESS,
} from '../actions/dashboard';

const initialState: IDashboardStore = {
  service: null,
  orders: null,
  lifecycle: null,
  superUsers: null,
  isServiceFetching: false,
  isLifeCycleFetching: false,
  isOrderFetching: false,
  isSuperUserFetching: false,
  quoteList: [],
  quoteFetching: false,
  quoteAction: false,
  userActivity: [],
  userActivityFetching: false,
};

export default function dashboard(
  state: IDashboardStore = initialState,
  action: any
): IDashboardStore {
  switch (action.type) {
    case FETCH_SERVICE_DASHBOARD_DATA_REQUEST:
      return Object.assign({}, state, {
        isServiceFetching: true,
      });

    case FETCH_SERVICE_DASHBOARD_DATA_SUCCESS:
      return Object.assign({}, state, {
        service: action.response,
        isServiceFetching: false,
      });

    case FETCH_SERVICE_DASHBOARD_DATA_FAILURE:
      return Object.assign({}, state, {
        isServiceFetching: false,
      });

    case FETCH_ORDER_DASHBOARD_DATA_REQUEST:
      return Object.assign({}, state, {
        isOrderFetching: true,
      });

    case FETCH_ORDER_DASHBOARD_DATA_SUCCESS:
      return Object.assign({}, state, {
        orders: action.response,
        isOrderFetching: false,
      });

    case FETCH_ORDER_DASHBOARD_DATA_FAILURE:
      return Object.assign({}, state, {
        isOrderFetching: false,
      });

    case FETCH_LIFECYCLE_DASHBOARD_DATA_REQUEST:
      return Object.assign({}, state, {
        isLifeCycleFetching: true,
      });

    case FETCH_LIFECYCLE_DASHBOARD_DATA_SUCCESS:
      return Object.assign({}, state, {
        lifecycle: action.response,
        isLifeCycleFetching: false,
      });

    case FETCH_LIFECYCLE_DASHBOARD_DATA_FAILURE:
      return Object.assign({}, state, {
        isLifeCycleFetching: false,
      });

    case FETCH_SUPERUSER_DASHBOARD_REQUEST:
      return Object.assign({}, state, {
        isSuperUserFetching: true,
      });

    case FETCH_SUPERUSER_DASHBOARD_SUCCESS:
      return Object.assign({}, state, {
        superUsers: action.response,
        isSuperUserFetching: false,
      });

    case FETCH_SUPERUSER_DASHBOARD_FAILURE:
      return Object.assign({}, state, {
        isSuperUserFetching: false,
      });

    case FETCH_QUOTE_LIST_REQUEST:
      return Object.assign({}, state, {
        quoteFetching: true,
      });

    case FETCH_QUOTE_LIST_SUCCESS:
      return Object.assign({}, state, {
        quoteList: action.response,
        quoteFetching: false,
      });

    case FETCH_QUOTE_LIST_FAILURE:
      return Object.assign({}, state, {
        quoteFetching: false,
      });

    case FETCH_ACTIVITY_REQUEST:
      return Object.assign({}, state, {
        userActivityFetching: true,
      });

    case FETCH_ACTIVITY_SUCCESS:
      return Object.assign({}, state, {
        userActivity: action.response,
        userActivityFetching: false,
      });

    case FETCH_ACTIVITY_FAILURE:
      return Object.assign({}, state, {
        userActivityFetching: false,
      });

    default:
      return state;
  }
}
