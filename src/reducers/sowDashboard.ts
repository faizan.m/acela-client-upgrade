import {
  FETCH_RAW_DATA_FAILURE,
  FETCH_RAW_DATA_REQUEST,
  FETCH_RAW_DATA_SUCCESS,
  FETCH_S_ACTIVITY_FAILURE,
  FETCH_S_ACTIVITY_REQUEST,
  FETCH_S_ACTIVITY_SUCCESS,
  FETCH_S_H_CREATED_FAILURE,
  FETCH_S_H_CREATED_REQUEST,
  FETCH_S_H_CREATED_SUCCESS,
  FETCH_SH_EXPECTED_FAILURE,
  FETCH_SH_EXPECTED_REQUEST,
  FETCH_SH_EXPECTED_SUCCESS,
  EXPORT_RAW_DATA_REQUEST,
  EXPORT_RAW_DATA_SUCCESS,
  EXPORT_RAW_DATA_FAILURE,
} from '../actions/sowDashboard';

const initialState: ISOWDashboardStore = {
  service_activity: null,
  booked_service_mixed: null,
  isFetchingActivity: false,
  isFetchingSMixed: false,
  rawData: null,
  sowCreated: null,
  expectedWon: null,
  isFetchingSOWCreated: false,
  isFetchingExpected: false,
  isFetchingSRawData: false,
};

export default function provider(
  state: ISOWDashboardStore = initialState,
  action: any
): ISOWDashboardStore {
  switch (action.type) {
    case FETCH_S_ACTIVITY_REQUEST:
      return Object.assign({}, state, {
        isFetchingActivity: true,
      });

    case FETCH_S_ACTIVITY_SUCCESS:
      return Object.assign({}, state, {
        service_activity: action.response,
        isFetchingActivity: false,
      });

    case FETCH_S_ACTIVITY_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingActivity: false,
      });
    case FETCH_S_H_CREATED_REQUEST:
      return Object.assign({}, state, {
        isFetchingSOWCreated: true,
      });

    case FETCH_S_H_CREATED_SUCCESS:
      return Object.assign({}, state, {
        sowCreated: action.response,
        isFetchingSOWCreated: false,
      });

    case FETCH_S_H_CREATED_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSOWCreated: false,
      });
    case FETCH_SH_EXPECTED_REQUEST:
      return Object.assign({}, state, {
        isFetchingExpected: true,
      });

    case FETCH_SH_EXPECTED_SUCCESS:
      return Object.assign({}, state, {
        expectedWon: action.response,
        isFetchingExpected: false,
      });

    case FETCH_SH_EXPECTED_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingExpected: false,
      });

    case FETCH_RAW_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetchingSRawData: true,
      });

    case FETCH_RAW_DATA_SUCCESS:
      return Object.assign({}, state, {
        rawData: action.response,
        isFetchingSRawData: false,
      });

    case FETCH_RAW_DATA_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSRawData: false,
      });

    case EXPORT_RAW_DATA_REQUEST:
      return Object.assign({}, state, {
        isFetchingSRawData: true,
      });

    case EXPORT_RAW_DATA_SUCCESS:
      const url = window.URL.createObjectURL(new Blob([action.response]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', 'row-data.csv');
      document.body.appendChild(link);
      link.click();

      return Object.assign({}, state, {
        isFetchingSRawData: false,
      });

    case EXPORT_RAW_DATA_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingSRawData: false,
      });

    default:
      return state;
  }
}
