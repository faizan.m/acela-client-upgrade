import { cloneDeep } from 'lodash';
import {
  CUSTOMER_USERS_CREATE_FAILURE,
  CUSTOMER_USERS_CREATE_REQUEST,
  CUSTOMER_USERS_CREATE_SUCCESS,
  EDIT_CUSTOMER_USERS_FAILURE,
  EDIT_CUSTOMER_USERS_REQUEST,
  EDIT_CUSTOMER_USERS_SUCCESS,
  FETCH_CUSTOMER_PROFILE_SUCCESS,
  FETCH_CUSTOMER_USERS_FAILURE,
  FETCH_CUSTOMER_USERS_REQUEST,
  FETCH_CUSTOMER_USERS_SUCCESS,
  PU_CU_CREATE_FAILURE,
  PU_CU_CREATE_REQUEST,
  PU_CU_CREATE_SUCCESS,
} from '../actions/customerUser';

const initialState: ICustomerUserStore = {
  users: null,
  error: null,
  isFetchingUsers: false,
  isPostingUser: false,
  customerProfile: null,
};

export default function customerUser(
  state: ICustomerUserStore = initialState,
  action: any
): ICustomerUserStore {
  switch (action.type) {
    case FETCH_CUSTOMER_USERS_REQUEST:
      return Object.assign({}, state, {
        isFetchingUsers: true,
      });

    case FETCH_CUSTOMER_USERS_SUCCESS:
      return Object.assign({}, state, {
        users: action.response,
        isFetchingUsers: false,
      });

    case FETCH_CUSTOMER_USERS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isFetchingUsers: false,
      });

    case CUSTOMER_USERS_CREATE_REQUEST:
      return Object.assign({}, state, {
        isPostingUser: true,
      });

    case CUSTOMER_USERS_CREATE_SUCCESS:
      const newStates = cloneDeep(state);
      action.response.can_edit = true;
      newStates.users.results[0] = action.response;
      newStates.isPostingUser = false;

      return newStates;

    case CUSTOMER_USERS_CREATE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isPostingUser: false,
      });

    case PU_CU_CREATE_REQUEST:
      return Object.assign({}, state, {
        isPostingUser: true,
      });

    case PU_CU_CREATE_SUCCESS:
      return Object.assign({}, state, {
        error: 'some error',
        isPostingUser: false,
      });

    case PU_CU_CREATE_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isPostingUser: false,
      });

    case EDIT_CUSTOMER_USERS_REQUEST:
      return Object.assign({}, state, {
        isPostingUser: true,
      });

    case EDIT_CUSTOMER_USERS_SUCCESS:
      const newState = cloneDeep(state);
      let index;
      newState.users.results.map((res, i) => {
        if (res.id === action.response.id) {
          index = i;
          action.response.can_edit = true;
        }
      });
      newState.users.results[index] = action.response;
      newState.isPostingUser = false;

      return newState;

    case EDIT_CUSTOMER_USERS_FAILURE:
      return Object.assign({}, state, {
        error: 'some error',
        isPostingUser: false,
      });

    case FETCH_CUSTOMER_PROFILE_SUCCESS:
      return Object.assign({}, state, {
        customerProfile: action.response,
      });

    default:
      return state;
  }
}
