import {
  FETCH_C_VAR_FAILURE,
  FETCH_C_VAR_REQUEST,
  FETCH_C_VAR_SUCCESS,
  FETCH_C_VARS_FAILURE,
  FETCH_C_VARS_REQUEST,
  FETCH_C_VARS_SUCCESS,
  FETCH_COMP_CLASS_FAILURE,
  FETCH_COMP_CLASS_REQUEST,
  FETCH_COMP_CLASS_SUCCESS,
  FETCH_CONFIGURATION_FAILURE,
  FETCH_CONFIGURATION_REQUEST,
  FETCH_CONFIGURATION_SUCCESS,
  FETCH_CUS_RULE_FAILURE,
  FETCH_CUS_RULE_REQUEST,
  FETCH_CUS_RULE_SUCCESS,
  FETCH_CUS_RULES_FAILURE,
  FETCH_CUS_RULES_REQUEST,
  FETCH_CUS_RULES_SUCCESS,
  FETCH_G_VAR_FAILURE,
  FETCH_G_VAR_REQUEST,
  FETCH_G_VAR_SUCCESS,
  FETCH_G_VARS_FAILURE,
  FETCH_G_VARS_REQUEST,
  FETCH_G_VARS_SUCCESS,
  FETCH_RULE_FAILURE,
  FETCH_RULE_LEVEL_FAILURE,
  FETCH_RULE_LEVEL_REQUEST,
  FETCH_RULE_LEVEL_SUCCESS,
  FETCH_RULE_REQUEST,
  FETCH_RULE_SUCCESS,
  FETCH_RULES_FAILURE,
  FETCH_RULES_REQUEST,
  FETCH_RULES_SUCCESS,
  FETCH_VAR_TYPE_FAILURE,
  FETCH_VAR_TYPE_REQUEST,
  FETCH_VAR_TYPE_SUCCESS,
  FETCH_C_VARS_ALL_FAILURE,
  FETCH_C_VARS_ALL_REQUEST,
  FETCH_C_VARS_ALL_SUCCESS,
  SYNC_CONFIG_FAILURE,
  SYNC_CONFIG_REQUEST,
  SYNC_CONFIG_SUCCESS,
  CONFIG_TASK_STATUS_FAILURE,
  CONFIG_TASK_STATUS_REQUEST,
  CONFIG_TASK_STATUS_SUCCESS,
  GET_RULE_HISTORY_FAILURE,
  GET_RULE_HISTORY_REQUEST,
  GET_RULE_HISTORY_SUCCESS,
  GET_VAR_HISTORY_REQUEST,
  GET_VAR_HISTORY_SUCCESS,
  GET_VAR_HISTORY_FAILURE,
  GET_CREATE_HTR_REQUEST,
  GET_CREATE_HTR_SUCCESS,
  GET_DELETE_HTR_REQUEST,
  GET_DELETE_HTR_SUCCESS,
  GET_CREATE_HTR_FAILURE,
  GET_DELETE_HTR_FAILURE,
  GET_COMPLIANCE_DASHBOARD_REQUEST,
  GET_COMPLIANCE_DASHBOARD_SUCCESS,
  GET_COMPLIANCE_DASHBOARD_FAILURE,
  GET_DEVICE_CONFIGURATION_DETAIL_REQUEST,
  GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS,
  GET_DEVICE_CONFIGURATION_DETAIL_FAILURE,
} from "../actions/configuration";
import {
  RERUN_COMPLIANCE_FAILURE,
  RERUN_COMPLIANCE_REQUEST,
  RERUN_COMPLIANCE_SUCCESS,
} from "../actions/collector";

const initialState: IConfigurationStore = {
  isFetching: false,
  isFetchingTypes: false,
  isFetchingList: false,
  isFetchingConfigList: false,
  configuration: null,
  detailConfiguration: null,
  isFetchingConfigDetail: false,
  configurations: [],
  rules: [],
  rule: null,
  levels: [],
  classifications: [],
  globalVariables: [],
  globalVariable: null,
  customerVariables: [],
  customerVariablesAll: [],
  customerVariable: null,
  types: [],
  isFetchingTasks: false,
  isTaskPolling: false,
  isFetchingHistory: false,
  ruleHistory: [],
  variableHistory: [],
  deletedHistory: [],
  isFetchingDeleted: false,
  createdHistory: [],
  isFetchingCreated: false,
  isFetchingComplianceDashboardData: false,
  complianceDashboardData: {
    rules_violations_by_priority_obj: {},
    top_rules_ids: [],
    top_devices_ids: [],
    rules: null,
    devices: null,
  },
};

export default function configuration(
  state: IConfigurationStore = initialState,
  action: any
): IConfigurationStore {
  switch (action.type) {
    case GET_COMPLIANCE_DASHBOARD_REQUEST:
      return Object.assign({}, state, {
        isFetchingComplianceDashboardData: true,
      });
    case GET_COMPLIANCE_DASHBOARD_SUCCESS:
      return Object.assign({}, state, {
        complianceDashboardData: action.response,
        isFetchingComplianceDashboardData: false,
      });
    case GET_COMPLIANCE_DASHBOARD_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingComplianceDashboardData: false,
      });

    case FETCH_CONFIGURATION_REQUEST:
      return Object.assign({}, state, {
        isFetchingConfigList: true,
      });
    case FETCH_CONFIGURATION_SUCCESS:
      return Object.assign({}, state, {
        configurations: action.response,
        isFetchingConfigList: false,
      });
    case FETCH_CONFIGURATION_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingConfigList: false,
      });

    case GET_DEVICE_CONFIGURATION_DETAIL_REQUEST:
      return Object.assign({}, state, {
        isFetchingConfigDetail: true,
      });
    case GET_DEVICE_CONFIGURATION_DETAIL_SUCCESS:
      return Object.assign({}, state, {
        detailConfiguration: action.response,
        isFetchingConfigDetail: false,
      });
    case GET_DEVICE_CONFIGURATION_DETAIL_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingConfigDetail: false,
      });

    case FETCH_RULES_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingList: false,
      });
    case FETCH_RULES_REQUEST:
      return Object.assign({}, state, {
        isFetchingList: true,
      });
    case FETCH_RULES_SUCCESS:
      return Object.assign({}, state, {
        rules: action.response,
        isFetchingList: false,
      });

    case FETCH_RULE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });
    case FETCH_RULE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_RULE_SUCCESS:
      return Object.assign({}, state, {
        rule: action.response,
        isFetching: false,
      });

    case FETCH_RULE_LEVEL_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });
    case FETCH_RULE_LEVEL_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_RULE_LEVEL_SUCCESS:
      return Object.assign({}, state, {
        levels: action.response,
        isFetching: false,
      });

    case FETCH_COMP_CLASS_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingList: false,
      });
    case FETCH_COMP_CLASS_REQUEST:
      return Object.assign({}, state, {
        isFetchingList: true,
      });
    case FETCH_COMP_CLASS_SUCCESS:
      return Object.assign({}, state, {
        classifications: action.response,
        isFetchingList: false,
      });

    case FETCH_G_VARS_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingList: false,
      });
    case FETCH_G_VARS_REQUEST:
      return Object.assign({}, state, {
        isFetchingList: true,
        globalVariables: [],
      });
    case FETCH_G_VARS_SUCCESS:
      return Object.assign({}, state, {
        globalVariables: action.response,
        isFetchingList: false,
      });

    case FETCH_G_VAR_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });
    case FETCH_G_VAR_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_G_VAR_SUCCESS:
      return Object.assign({}, state, {
        globalVariable: action.response,
        isFetching: false,
      });

    case FETCH_VAR_TYPE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingTypes: false,
      });
    case FETCH_VAR_TYPE_REQUEST:
      return Object.assign({}, state, {
        isFetchingTypes: true,
      });
    case FETCH_VAR_TYPE_SUCCESS:
      return Object.assign({}, state, {
        types: action.response,
        isFetchingTypes: false,
      });
    case FETCH_C_VARS_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingList: false,
      });
    case FETCH_C_VARS_REQUEST:
      return Object.assign({}, state, {
        isFetchingList: true,
        customerVariables: [],
      });
    case FETCH_C_VARS_SUCCESS:
      return Object.assign({}, state, {
        customerVariables: action.response,
        isFetchingList: false,
      });
    case FETCH_C_VARS_ALL_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingList: false,
      });
    case FETCH_C_VARS_ALL_REQUEST:
      return Object.assign({}, state, {
        isFetchingList: true,
        customerVariablesAll: [],
      });
    case FETCH_C_VARS_ALL_SUCCESS:
      return Object.assign({}, state, {
        customerVariablesAll: action.response,
        isFetchingList: false,
      });

    case FETCH_C_VAR_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });
    case FETCH_C_VAR_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_C_VAR_SUCCESS:
      return Object.assign({}, state, {
        customerVariable: action.response,
        isFetching: false,
      });

    case FETCH_CUS_RULES_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingList: false,
      });
    case FETCH_CUS_RULES_REQUEST:
      return Object.assign({}, state, {
        isFetchingList: true,
      });
    case FETCH_CUS_RULES_SUCCESS:
      return Object.assign({}, state, {
        rules: action.response,
        isFetchingList: false,
      });

    case SYNC_CONFIG_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingTasks: false,
      });
    case SYNC_CONFIG_REQUEST:
      return Object.assign({}, state, {
        isFetchingTasks: true,
      });
    case SYNC_CONFIG_SUCCESS:
      return Object.assign({}, state, {
        configSyncTaskId: action.response,
        isFetchingTasks: false,
      });

    case RERUN_COMPLIANCE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingTasks: false,
      });
    case RERUN_COMPLIANCE_REQUEST:
      return Object.assign({}, state, {
        isFetchingTasks: true,
      });
    case RERUN_COMPLIANCE_SUCCESS:
      return Object.assign({}, state, {
        ComplianceRunTaskId: action.response,
        isFetchingTasks: false,
      });

    case CONFIG_TASK_STATUS_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isTaskPolling: false,
      });
    case CONFIG_TASK_STATUS_REQUEST:
      return Object.assign({}, state, {
        isTaskPolling: true,
      });
    case CONFIG_TASK_STATUS_SUCCESS:
      return Object.assign({}, state, {
        isTaskPolling:
          action.response && action.response.status === "PENDING"
            ? true
            : false,
      });

    case GET_RULE_HISTORY_REQUEST:
      return Object.assign({}, state, {
        isFetchingHistory: true,
        ruleHistory: [],
      });
    case GET_RULE_HISTORY_SUCCESS:
      return Object.assign({}, state, {
        isFetchingHistory: false,
        ruleHistory: action.response,
      });
    case GET_RULE_HISTORY_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingHistory: false,
      });

    case GET_VAR_HISTORY_REQUEST:
      return Object.assign({}, state, {
        isFetchingHistory: true,
        ruleHistory: [],
      });
    case GET_VAR_HISTORY_SUCCESS:
      return Object.assign({}, state, {
        isFetchingHistory: false,
        variableHistory: action.response,
      });
    case GET_VAR_HISTORY_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingHistory: false,
      });
    case FETCH_CUS_RULE_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetching: false,
      });
    case FETCH_CUS_RULE_REQUEST:
      return Object.assign({}, state, {
        isFetching: true,
      });
    case FETCH_CUS_RULE_SUCCESS:
      return Object.assign({}, state, {
        customerVariable: action.response,
        isFetching: false,
      });
    case GET_CREATE_HTR_FAILURE:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingCreated: false,
      });
    case GET_CREATE_HTR_REQUEST:
      return Object.assign({}, state, {
        isFetchingCreated: true,
      });
    case GET_CREATE_HTR_SUCCESS:
      return Object.assign({}, state, {
        createdHistory: action.response,
        isFetchingCreated: false,
      });
    case GET_DELETE_HTR_REQUEST:
      return Object.assign({}, state, {
        error: "some error",
        isFetchingDeleted: true,
      });
    case GET_DELETE_HTR_FAILURE:
      return Object.assign({}, state, {
        isFetchingDeleted: false,
      });
    case GET_DELETE_HTR_SUCCESS:
      return Object.assign({}, state, {
        deletedHistory: action.response,
        isFetchingDeleted: false,
      });

    default:
      return state;
  }
}
