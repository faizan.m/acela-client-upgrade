import {
  PC_IMPORT_CSV_REQUEST,
  PC_IMPORT_CSV_SUCCESS,
  PC_IMPORT_CSV_FAILURE,
} from "../actions/sales";

const initialState: ISalesStore = {
  isFetching: false,
  productValidationInfo: null,
};

export default function customer(state: any = initialState, action: any): any {
  switch (action.type) {
    case PC_IMPORT_CSV_REQUEST:
      return Object.assign({}, state, {
        productValidationInfo: null,
        isFetching: true,
      });

    case PC_IMPORT_CSV_SUCCESS:
      return Object.assign({}, state, {
        productValidationInfo: action.response,
        isFetching: false,
      });

    case PC_IMPORT_CSV_FAILURE:
      return Object.assign({}, state, {
        error: "Error uploading CSV file",
        isFetching: false,
      });
    default:
      return state;
  }
}
