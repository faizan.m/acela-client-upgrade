import { FETCH_USERTYPES_SUCCESS } from '../actions/userType';

const initialState: IUserTypeStore = {
  userTypes: null,
};

export default function customer(
  state: IUserTypeStore = initialState,
  action: any
): IUserTypeStore {
  switch (action.type) {
    case FETCH_USERTYPES_SUCCESS:
      // action.response.results due to pagination.
      return Object.assign({}, state, {
        userTypes: action.response.results,
      });

    default:
      return state;
  }
}
