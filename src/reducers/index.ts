import { combineReducers } from 'redux';

import { LOGOUT_SUCCESS } from '../actions/appState';
import appState from './appState';
import auth from './auth';
import collector from './collector';
import configuration from './configuration';
import customer from './customer';
import customerUser from './customerUser';
import dashboard from './dashboard';
import documentation from './documentation';
import integration from './integration';
import inventory from './inventory';
import profile from './profile';
import provider from './provider';
import providerIntegration from './provider/integration';
import providerUser from './provider/user';
import providerAdminUser from './providerAdminUser';
import renewal from './renewal';
import report from './report';
import serviceRequest from './serviceRequest';
import setting from './setting';
import sow from './sow';
import sowDashboard from './sowDashboard';
import subscription from './subscription';
import superuser from './superuser';
import user from './user';
import userType from './userType';
import pmo from './pmo';
import orderTracking from './orderTracking';
import agreement from './agreement';
import sales from './sales';
import virtualWarehouse from './virtualWarehouse';

const appReducers = combineReducers({
  auth,
  user,
  sales,
  superuser,
  customer,
  customerUser,
  provider,
  integration,
  providerUser,
  providerAdminUser,
  providerIntegration,
  serviceRequest,
  userType,
  appState,
  profile,
  inventory,
  report,
  renewal,
  dashboard,
  documentation,
  subscription,
  sow,
  agreement,
  sowDashboard,
  setting,
  configuration,
  collector,
  pmo,
  orderTracking,
  virtualWarehouse
});

export const reducers = (state, action) => {
  let newState = Object.assign({}, state);

  switch (action.type) {
    case LOGOUT_SUCCESS:
      newState = undefined;
      break;
  }

  return appReducers(newState, action);
};
