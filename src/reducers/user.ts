import {
  PRODUCT_USERS_CREATE_ACL_FETCH_SUCCESS,
  PRODUCT_USERS_MODIFY_ACL_FETCH_SUCCESS,
  PRODUCT_USERS_VIEW_ACL_FETCH_SUCCESS,
  USER_FETCH_SUCCESS,
  USERS_FETCH_SUCCESS,
} from '../actions/user';

const initialState: IUserStore = {
  currentUser: null,
  users: null,
  productUsersViewACL: null,
  productUsersCreateACL: null,
  productUsersModifyACL: null,
};

export default function user(
  state: IUserStore = initialState,
  action: any
): IUserStore {
  switch (action.type) {
    case USERS_FETCH_SUCCESS:
      return Object.assign({}, state, {
        users: action.response,
      });

    case USER_FETCH_SUCCESS:
      return Object.assign({}, state, {
        currentUser: action.response,
      });

    case PRODUCT_USERS_VIEW_ACL_FETCH_SUCCESS:
      return Object.assign({}, state, {
        productUsersViewACL: action.response,
      });

    case PRODUCT_USERS_CREATE_ACL_FETCH_SUCCESS:
      return Object.assign({}, state, {
        productUsersCreateACL: action.response,
      });

    case PRODUCT_USERS_MODIFY_ACL_FETCH_SUCCESS:
      return Object.assign({}, state, {
        productUsersModifyACL: action.response,
      });

    default:
      return state;
  }
}
