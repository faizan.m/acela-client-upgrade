import {
  ADD_TICKET,
  CLOSE_TICKET,
  FETCH_CONTACTS_SUCCESS,
  FETCH_NOTE_SUCCESS,
  FETCH_TICKET_FAILURE,
  FETCH_TICKET_REQUEST,
  FETCH_TICKET_SUCCESS,
  FETCH_TICKETS_FAILURE,
  FETCH_TICKETS_REQUEST,
  FETCH_TICKETS_SUCCESS,
  GET_STATUS_LIST_FAILURE,
  GET_STATUS_LIST_REQUEST,
  GET_STATUS_LIST_SUCCESS,
  POST_NOTE_FAILURE,
  POST_NOTE_REQUEST,
  POST_NOTE_SUCCESS,
  SET_COMPLETED_TICKETS,
  UPDATE_TICKET_FAILURE,
  UPDATE_TICKET_REQUEST,
  UPDATE_TICKET_SUCCESS,
} from '../actions/serviceRequest';

const initialState: IServiceRequestStore = {
  requests: [],
  requestDetail: null,
  contactList: [],
  note: '',
  isTicketsFetching: false,
  isTicketFetching: true,
  isNotePosting: false,
  completedTickets: false,
  statusList: [],
};

export default function(
  state: IServiceRequestStore = initialState,
  action: any
): IServiceRequestStore {
  switch (action.type) {
    case FETCH_TICKETS_REQUEST: {
      return Object.assign({}, state, {
        isTicketsFetching: true,
      });
    }

    case FETCH_TICKETS_SUCCESS: {
      return Object.assign({}, state, {
        requests: action.response,
        isTicketsFetching: false,
      });
    }
    case FETCH_TICKETS_FAILURE: {
      return Object.assign({}, state, {
        isTicketsFetching: false,
      });
    }
    case FETCH_TICKET_REQUEST: {
      return Object.assign({}, state, {
        isTicketFetching: true,
      });
    }
    case FETCH_TICKET_SUCCESS: {
      return Object.assign({}, state, {
        requestDetail: action.response,
        isTicketFetching: false,
      });
    }
    case FETCH_TICKET_FAILURE: {
      return Object.assign({}, state, {
        isTicketFetching: false,
      });
    }
    case ADD_TICKET: {
      return Object.assign({}, state, {
        requests: [...state.requests, action.request],
      });
    }
    case FETCH_CONTACTS_SUCCESS: {
      return Object.assign({}, state, {
        contactList: action.response,
      });
    }
    case FETCH_NOTE_SUCCESS: {
      return Object.assign({}, state, {
        note: action.response.ticket_note,
      });
    }
    case POST_NOTE_REQUEST: {
      return Object.assign({}, state, {
        isNotePosting: true,
      });
    }
    case POST_NOTE_SUCCESS: {
      return Object.assign({}, state, {
        isNotePosting: false,
      });
    }
    case POST_NOTE_FAILURE: {
      return Object.assign({}, state, {
        isNotePosting: false,
      });
    }

    case SET_COMPLETED_TICKETS:
      return Object.assign({}, state, {
        completedTickets: action.is,
      });

    case GET_STATUS_LIST_REQUEST: {
      return Object.assign({}, state, {
        isNotePosting: true,
      });
    }
    case GET_STATUS_LIST_SUCCESS: {
      return Object.assign({}, state, {
        statusList: action.response,
        isNotePosting: false,
      });
    }
    case GET_STATUS_LIST_FAILURE: {
      return Object.assign({}, state, {
        isNotePosting: false,
      });
    }
    case UPDATE_TICKET_REQUEST: {
      return Object.assign({}, state, {
        isNotePosting: true,
      });
    }
    case UPDATE_TICKET_SUCCESS: {
      return Object.assign({}, state, {
        isNotePosting: false,
      });
    }
    case UPDATE_TICKET_FAILURE: {
      return Object.assign({}, state, {
        isNotePosting: false,
      });
    }

    case CLOSE_TICKET:
      return Object.assign({}, state, {
        requests: state.requests.map(request => {
          if (request.id === action.id) {
            return {};
          }

          return request;
        }),
      });
    default:
      return state;
  }
}
