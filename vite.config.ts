import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

const APP_DIR = path.resolve(__dirname, "src");
const STATIC_DIR = path.resolve(__dirname, "static", "app");
const BUILD_DIR = path.resolve(__dirname, "dist", "app");

export default defineConfig({
  plugins: [react()],
  build: {
    outDir: BUILD_DIR,
    emptyOutDir: true,
    sourcemap: true,
    target: "es2020",
    rollupOptions: {
      input: `${APP_DIR}/app.tsx`,
      output: {
        entryFileNames: "bundle.js",
        chunkFileNames: "[name].js",
        assetFileNames: "[name][extname]",
        manualChunks(id) {
          if (id.includes("node_modules")) {
            return "vendor";
          }
        },
      },
    },
  },
  publicDir: STATIC_DIR,
  server: {
    port: 4001,
    strictPort: false,
    open: true,
    hmr: true,
    host: "0.0.0.0",
    proxy: {},
  },
});
